<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_jadwal_ujian extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_jadwal_ujian';
    }

    public function single($where)
    {
        $this->db->select("t.id_jadwal_ujian,t.waktu_mulai,t.waktu_selesai,t.durasi,t.total_peserta,t.doc_status,k.id_kelas, k.kelas, u.judul,u.id_ujian,u.tipe,t.tampilkan_jawaban");
        $this->db->join("asc_ujian u", "u.id_ujian=t.id_ujian");
        $this->db->join("tabel_kelas k", "k.id_kelas=t.id_kelas");
        return $this->get($where, true);
    }

    public function guru($where, $key = null)
    {
        $this->db->select("u.kode,j.id_jadwal_ujian, u.id_ujian, j.id_kelas, k.kelas, m.id_mapel, m.mapel , u.judul, u.deskripsi, j.doc_status, j.waktu_mulai, j.waktu_selesai, j.tampilkan_jawaban")
                    ->from("asc_jadwal_ujian j")
                    ->join("asc_ujian u","u.id_ujian=j.id_ujian","inner")
                    ->join("tabel_mapel m","m.id_mapel=u.id_mapel","inner")
                    ->join("tabel_kelas k","k.id_kelas=j.id_kelas","inner");
        $this->db->order_by("j.waktu_mulai", "DESC");
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( $key != null )
        {
            $this->db->like("u.judul", $key, "both");
            $this->db->or_like("u.deskripsi", $key, "both");
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function list_by($where, $key = null)
    {
        $this->db->select("p.id_peserta_ujian, u.kode, u.tipe, DATE_FORMAT(`j`.`waktu_mulai`, '%Y-%m-%d %H:%i:%s') AS waktu_mulai, DATE_FORMAT(j.waktu_selesai, '%Y-%m-%d %H:%i:%s') AS waktu_selesai, p.id_siswa, p.doc_status, p.waktu_join,p.waktu_submit, j.id_jadwal_ujian, u.id_ujian, m.id_mapel, m.mapel , u.judul, u.deskripsi, g.nama as guru, j.durasi")
                    ->from("asc_peserta_ujian p")
                    ->join("asc_jadwal_ujian j","j.id_jadwal_ujian=p.id_jadwal_ujian","inner")
                    ->join("asc_ujian u","u.id_ujian=j.id_ujian","inner")
                    ->join("tabel_guru g","g.id_guru=u.id_guru","inner")
                    ->join("tabel_mapel m","m.id_mapel=u.id_mapel","inner")
                    ->order_by("j.waktu_mulai");
        $this->db->where('j.doc_status', $this->config->item('doc_status_publish'));
        $this->db->where('j.waktu_selesai>', date('Y-m-d H:i:s'));
        $this->db->order_by("j.created", "DESC");
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( $key != null )
        {
            $this->db->like("u.judul", $key, "both");
            $this->db->or_like("u.deskripsi", $key, "both");
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_all_ujian($param) {
        // get peserta lalu delete
        $where = array(
            "id_jadwal_ujian" => $param['id_jadwal_ujian']
        );
        $this->db->from('asc_peserta_ujian');
        $this->db->where($where);
        $query = $this->db->get();
        if ( $query == null )
        {
            return false;
        }
        $pesertaList = $query->result_array();
        // delete submission
        foreach ($pesertaList as $peserta)
        {
            $where = array(
                "id_peserta_ujian" => $peserta['id_peserta_ujian']
            );
            $this->db->where($where);
            if( !$this->db->delete('asc_submission') )
            {
                return false;
            }
        }
        // delete peserta
        $where = array(
            "id_jadwal_ujian" => $param['id_jadwal_ujian']
        );
        $this->db->where($where);
        if ( !$this->db->delete('asc_peserta_ujian') )
            return false;
        // delete jadwal ujian
        if ( !$this->delete($param) )
            return false;

        return true;
    }
    
    protected function init_rule()
    {
        $this->form_validation->set_rules('tampilkan_jawaban', 'Tampilkan Jawaban', 'required');
    }
}
