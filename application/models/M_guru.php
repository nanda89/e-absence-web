<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_guru extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id_general($table, $id, $val)
    {
        $query = $this->db->where($id, $val)->get($table);
        return $query->result();
    }

    public function get_all()
    {
        $sql = "SELECT * FROM tabel_guru
				ORDER BY tabel_guru.id_guru asc";
        return $this->db->query($sql);
    }

    public function get_one($id)
    {
        $sql = "SELECT * FROM tabel_guru
				WHERE id_guru = '$id'";
        return $this->db->query($sql);
    }

    public function sync_rekap_absen_guru()
    {
        return $this->db->query('SELECT tm.*,tj.jam_awal,tj.jam_akhir,tj.id_jadwal,tam.*,tk.*,tg.nama,tg.id_guru FROM
			tabel_guru tg, tabel_mapel tm, tabel_jadwal tj, tabel_jenjang tjg,
			tabel_tahunajaran ta, tabel_absen_mapel tam, tabel_kelas tk
			where tj.id_mapel = tm.id_mapel and tj.id_jenjang = tjg.id_jenjang
			and tj.id_kelas = tk.id_kelas  and
			tj.id_guru = tg.id_guru  and tj.id_ta = ta.id_ta and
			tj.id_jadwal = tam.id_jadwal')->result_array();
    }

    public function get_one_guru($id)
    {
        $sql = "SELECT * FROM tabel_guru INNER JOIN tabel_jenjang
				ON tabel_guru.id_jenjang=tabel_jenjang.id_jenjang
				WHERE tabel_guru.id_jenjang = '$id'";
        return $this->db->query($sql);
    }

    public function get_guru_by_jenjang_mapel($id_jenjang, $id_mapel)
    {
        $sql = "SELECT * FROM tabel_guru
				INNER JOIN tabel_jenjang
				ON tabel_guru.id_jenjang=tabel_jenjang.id_jenjang
				INNER JOIN tabel_pengampu on tabel_pengampu.nik = tabel_guru.nik
				WHERE tabel_guru.id_jenjang = '$id_jenjang' and tabel_pengampu.id_mapel='$id_mapel'";
        return $this->db->query($sql);

    }

    public function tambah($data)
    {
        $query = $this->db->insert('tabel_guru', $data);
    }

    public function edit($id, $data)
    {
        $this->db->where('id_guru', $id);
        $this->db->update('tabel_guru', $data);
    }

    public function hapus($id)
    {
        $this->db->where('id_guru', $id);
        $this->db->delete('tabel_guru');
    }
    public function remove_checked()
    {
        $delete = $this->input->post('item');
        for ($i = 0; $i < count($delete); $i++) {
            $this->db->where('id_guru', $delete[$i]);
            $this->db->delete('tabel_guru');
        }
    }

    public function get_kelas_by_guru($id_guru)
    {
        $sql = "SELECT DISTINCT tabel_kelas.* FROM tabel_kelas
			INNER JOIN tabel_jadwal ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas
			INNER JOIN tabel_guru ON tabel_guru.id_guru  = tabel_jadwal.id_guru
			WHERE tabel_guru.id_guru = '$id_guru'";

        return $this->db->query($sql);
    }
}
