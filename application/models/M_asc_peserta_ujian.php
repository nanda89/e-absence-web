<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_peserta_ujian extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_peserta_ujian';
    }

    public function list_by($where = null, $key = null)
    {
        $this->db->select("p.id_peserta_ujian, s.id_siswa, s.nisn, s.nama_lengkap, p.nilai, p.doc_status");
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( $key != null )
        {
            $this->db->like("s.nisn", $key, "both");
            $this->db->or_like("s.nama_lengkap", $key, "both");
        }
        $this->db->from("asc_peserta_ujian p");
        $this->db->join("tabel_siswa s", "s.id_siswa=p.id_siswa");
        $q = $this->db->get();
        return $q->result_array();
    }

    public function generate_peserta($jadwal_ujian)
    {
        $query = $this->db->from("tabel_siswakelas")
                            ->where("id_kelas", $jadwal_ujian["id_kelas"])
                            ->get();
        if ( $query->num_rows () > 0 )
        {
            $result = array();
            $list = array();
            $list_siswa = $query->result_array();
            $i=0;
            foreach ($list_siswa as $siswa)
            {
                $param = array();
                $param["doc_status"] = $this->config->item('doc_status_draft');
                $param["id_siswa"] = $siswa["id_siswa"];
                $param["id_jadwal_ujian"] = $jadwal_ujian["id_jadwal_ujian"];
                if ( $this->save($param, null, true) )
                {
                    $list[] = array(
                        "id_siswa" => $siswa["id_siswa"],
                        "id_peserta" => $this->db->insert_id()
                    );
                    $i++;
                }
            }
            $result["total_peserta"] = $i;
            $result["peserta"] = $list;
            return $result;
        }
        return null;
    }
}