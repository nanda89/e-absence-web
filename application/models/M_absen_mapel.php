<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_absen_mapel extends CI_Model {
	protected $_table = 'tabel_absen_mapel';
	
	public function __construct()
	{
		parent::__construct();
	}

	function get_by_penempatan_jadwal_tgl($id_penempatan, $id_jadwal, $tgl){
        $query = $this->db->get_where($this->_table, 
				array('tabel_absen_mapel.id_penempatan' => $id_penempatan,
					'tabel_absen_mapel.id_jadwal' => $id_jadwal, 
					'tabel_absen_mapel.tgl' => $tgl));
        return $query->row_array();
    }

	function add($data)
    {
        $this->db->insert($this->_table, $data);
		return true;

    }

	function update($id, $data)
    {
        $this->db->where('id_absen_mapel', $id);
        $this->db->update($this->_table, $data); 
        return true;
    }

}