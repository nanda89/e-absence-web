<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cabang extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_cabang
				ORDER BY nama_cabang asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_cabang
				WHERE id_cabang = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_cabang',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_cabang', $id);
		$this->db->update('tabel_cabang', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_cabang', $id);
		$this->db->delete('tabel_cabang'); 
	}
}