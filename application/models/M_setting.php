<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_setting extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_one()
    {
        $sql = "SELECT * FROM tabel_setting LIMIT 1";
        return $this->db->query($sql);
    }
}
