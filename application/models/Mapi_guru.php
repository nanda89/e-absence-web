<?php

	class Mapi_guru extends CI_Model
	{
		function get($id = NULL)
		{
			$this->db->select('*');
			$this->db->from('tabel_guru');
			if($id != NULL) $this->db->where('id_guru',$id);
			$query = $this->db->get();

			return ($id == NULL) ? $query->result_array() : $query->row_array();
		}

		function save($data = NULL,$id = NULL,$gambar = NULL)
		{
			if($id == NULL){
				$this->db->insert('tabel_guru',$data);
			}else{
				$this->db->where('id_guru',$id);
				$this->db->update('tabel_guru',$data);
			}
			if ($gambar != null) {
				$nik = $data['nik'];
					$decoded=base64_decode($gambar);
					do {
						$avatar = '';
						$keys = array_merge(range(0, 9), range('a', 'z'));

						for ($i = 0; $i < 10; $i++) {
							$avatar .= $keys[array_rand($keys)];
						}
						$avatar .= '.png';
					} while (file_exists('assets/panel/images/'.$avatar));
					file_put_contents('assets/panel/images/'.$avatar,$decoded);
					$this->db->where('nik', $nik);
					$this->db->update('tabel_guru', array('gambar' => $avatar));
				
			}
		}
	}