<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kategori extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_kategori
				ORDER BY kategori asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_kategori
				WHERE id_kategori = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_kategori',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_kategori', $id);
		$this->db->update('tabel_kategori', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_kategori', $id);
		$this->db->delete('tabel_kategori'); 
	}
}