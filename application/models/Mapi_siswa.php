<?php

	class Mapi_siswa extends CI_Model
	{
		function get($id = NULL)
		{
			$this->db->select('*');
			$this->db->from('tabel_siswa');
			if($id != NULL) $this->db->where('id_siswa',$id);
			$query = $this->db->get();

			return ($id == NULL) ? $query->result_array() : $query->row_array();
		}

		function save($data = NULL,$id = NULL,$gambar = NULL)
		{
			if($id == NULL){
				$this->db->insert('tabel_siswa',$data);
			}else{
				$this->db->where('id_siswa',$id);
				$this->db->update('tabel_siswa',$data);
			}
			if ($gambar != null) {
				$nisn = $data['nisn'];
					$decoded=base64_decode($gambar);
					do {
						$avatar = '';
						$keys = array_merge(range(0, 9), range('a', 'z'));

						for ($i = 0; $i < 10; $i++) {
							$avatar .= $keys[array_rand($keys)];
						}
						$avatar .= '.png';
					} while (file_exists('assets/panel/images/'.$avatar));
					file_put_contents('assets/panel/images/'.$avatar,$decoded);
					$this->db->where('nisn', $nisn);
					$this->db->update('tabel_siswa', array('gambar' => $avatar));
				
			}
		}
	}