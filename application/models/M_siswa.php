<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_siswa extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_siswa
				ORDER BY tabel_siswa.id_siswa asc";
		return $this->db->query($sql);
	}
	public function get_siswa($a,$b)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE id_jenjang = '$a' AND right(tgl_daftar,4) = '$b' 
				ORDER BY id_siswa asc";
		return $this->db->query($sql);
	}
	public function get_siswa_b($a)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE id_jenjang = '$a'
				ORDER BY id_siswa asc";
		return $this->db->query($sql);
	}
	public function get_all_calon()
	{
		$sql = "SELECT * FROM tabel_daf_jenis INNER JOIN tabel_daf_jadwal
        		ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis) INNER JOIN tabel_calon_siswa 
		        ON (tabel_daf_jadwal.id_daf_jadwal = tabel_calon_siswa.id_daf_jadwal) LEFT JOIN tabel_cabang
		        ON (tabel_calon_siswa.id_cabang = tabel_cabang.id_cabang)
		        WHERE tabel_calon_siswa.status <> 'aktif'
				ORDER BY id_daftar asc";
		return $this->db->query($sql);
	}
	public function sinkron()
	{
		$sql = "SELECT * FROM tabel_calon_siswa INNER JOIN tabel_c_siswa_detail
				ON tabel_calon_siswa.id_daftar = tabel_c_siswa_detail.id_daftar
				WHERE tabel_calon_siswa.status = 'lulus'
				ORDER BY tabel_calon_siswa.id_daftar asc";
		return $this->db->query($sql);
	}
	public function get_calon($a,$b,$c)
	{
		$sql = "SELECT * FROM tabel_daf_jenis INNER JOIN tabel_daf_jadwal
        		ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis) INNER JOIN tabel_calon_siswa 
		        ON (tabel_daf_jadwal.id_daf_jadwal = tabel_calon_siswa.id_daf_jadwal)
				WHERE tabel_daf_jenis.id_daf_jenis = '$a' AND right(tabel_calon_siswa.tgl_daftar,4) = '$b' 
				AND tabel_calon_siswa.id_daf_jadwal = '$c'
				ORDER BY id_daftar asc";
		return $this->db->query($sql);
	}
	public function get_calon_b($a,$b)
	{
		$sql = "SELECT * FROM tabel_daf_jenis
    			INNER JOIN tabel_daf_jadwal
        		ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis)
	    		INNER JOIN tabel_calon_siswa 
		        ON (tabel_daf_jadwal.id_daf_jadwal = tabel_calon_siswa.id_daf_jadwal)
				WHERE tabel_daf_jenis.id_daf_jenis = '$a' AND right(tabel_calon_siswa.tgl_daftar,4) = '$b' 				
				ORDER BY id_daftar asc";
		return $this->db->query($sql);
	}

	public function get_one_prestasi($id)
	{
		$sql = "SELECT * FROM tabel_prestasi
				WHERE id_siswa = '$id'
				ORDER BY id_prestasi asc";
		return $this->db->query($sql);
	}

	public function get_one_beasiswa($id)
	{
		$sql = "SELECT * FROM tabel_beasiswa
				WHERE id_siswa = '$id'
				ORDER BY id_beasiswa asc";
		return $this->db->query($sql);
	}
	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE id_siswa = '$id'";
		return $this->db->query($sql);
	}
	public function cek_lulus($id)
	{
		$sql = "SELECT * FROM tabel_calon_siswa
				WHERE id_daftar = '$id'";
		return $this->db->query($sql);
	}
	public function cek_lulus_detail($id)
	{
		$sql = "SELECT * FROM tabel_c_siswa_detail
				WHERE id_daftar = '$id'";
		return $this->db->query($sql);
	}
	public function get_nisn($id)
	{
		$sql = "SELECT tabel_siswa.nama_lengkap,tabel_siswa.id_jenjang as kode, tabel_siswakelas.id_penempatan,
				tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran FROM tabel_siswa INNER JOIN tabel_jenjang
				ON tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang
				INNER JOIN tabel_siswakelas ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
				INNER JOIN tabel_tahunajaran ON tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta
				INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
				WHERE tabel_siswa.id_siswa = '$id' AND tabel_siswakelas.status = 'aktif'";
		return $this->db->query($sql);
	}
	public function get_one_detail($id)
	{
		$sql = "SELECT * FROM tabel_siswa_detail
				WHERE id_siswa = '$id'";
		return $this->db->query($sql);
	}

	public function get_end($jen)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE id_jenjang = '$jen'
				ORDER BY id_siswa DESC LIMIT 0,1";
		return $this->db->query($sql);
	}	

	public function get_end_calon($jen)
	{
		$sql = "SELECT * FROM tabel_calon_siswa
				WHERE id_jenjang = '$jen'
				ORDER BY id_daftar DESC LIMIT 0,1";
		return $this->db->query($sql);
	}

	public function cek_by_id_or_nisn($id,$nisn)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE id_siswa = '$id' OR nisn = '$nisn' LIMIT 1";
		var_dump($sql);
		return $this->db->query($sql);
	}

	public function cek_by_nisn($nisn)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE nisn = '$nisn' LIMIT 1";
		return $this->db->query($sql);
	}
	public function cek_by_nisn_new($nisn)
	{
		$sql = "SELECT * FROM tabel_siswa
				WHERE nisn = '$nisn' LIMIT 1";
		return $this->db->query($sql)->row();
	}
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_siswa',$data);
	}
	public function tambah_calon($data)
	{
		$query=$this->db->insert('tabel_calon_siswa',$data);
	}
	public function tambah_c_detail($detail)
	{
		$query=$this->db->insert('tabel_c_siswa_detail',$detail);
	}
	public function tambah_detail($detail)
	{
		$query=$this->db->insert('tabel_siswa_detail',$detail);
	}
	public function tambah_prestasi($data)
	{
		$query=$this->db->insert('tabel_prestasi',$data);
	}
	public function tambah_beasiswa($data)
	{
		$query=$this->db->insert('tabel_beasiswa',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_siswa', $id);
		$this->db->update('tabel_siswa', $data); 
	}
	function edit_by_id_or_nisn($id,$nisn, $data)
	{
		$this->db->where('id_siswa', $id);
		$this->db->or_where('nisn', $nisn);
		$this->db->update('tabel_siswa', $data); 
	}
	function edit_detail($id, $detail)
	{
		$this->db->where('id_siswa', $id);
		$this->db->update('tabel_siswa_detail', $detail); 
	}
	function edit_calon($id, $detail)
	{
		$this->db->where('id_daftar', $id);
		$this->db->update('tabel_calon_siswa', $detail); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_siswa', $id);
		$this->db->delete('tabel_siswa'); 
	}
	function hapus_calon($id)
	{
		$this->db->where('id_daftar', $id);
		$this->db->delete('tabel_calon_siswa'); 

		$this->db->where('id_daftar', $id);
		$this->db->delete('tabel_c_siswa_detail'); 
	}
	function hapus_prestasi($id,$id_pres)
	{
		$sql = "DELETE FROM tabel_prestasi
				WHERE id_siswa = '$id' AND id_prestasi = '$id_pres'";
		return $this->db->query($sql);
	}
	function hapus_beasiswa($id,$id_bea)
	{
		$sql = "DELETE FROM tabel_beasiswa
				WHERE id_siswa = '$id' AND id_beasiswa = '$id_bea'";
		return $this->db->query($sql);
	}
	function insert_csv($data) {
        $this->db->insert('tabel_siswa', $data);
    }
	function insert_csv_by_return($data) {
        $this->db->insert('tabel_siswa', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
		
    }
    function insert_csv_p($data) {
        $this->db->insert('tabel_siswakelas', $data);
    }
    public function edit_csv_p($id, $data)
	{
		$this->db->where('id_siswa', $id);
		$this->db->update('tabel_siswakelas', $data); 
	}
}