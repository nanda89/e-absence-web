<?php

class M_siswakelas extends CI_Model
{
    protected $_table = 'tabel_siswakelas';

    public function hari_apa($hari)
    {
        if ($hari == "0 Minggu") {
            return "Sunday";
        } else if ($hari == "1 Senin") {
            return "Monday";
        } else if ($hari == "2 Selasa") {
            return "Tuesday";
        } else if ($hari == "3 Rabu") {
            return "Wednesday";
        } else if ($hari == "4 Kamis") {
            return "Thursday";
        } else if ($hari == "5 Jumat") {
            return "Friday";
        } else if ($hari == "6 Sabtu") {
            return "Saturday";
        }
    }
    public function dt_wali_kelas($kondisi = null)
    {
        return $this->db->query("
				SELECT
					tabel_siswakelas.id_penempatan,
					tabel_siswa.nama_lengkap,
					tabel_siswa.id_siswa,
					tabel_siswa.nisn,
					tabel_jenjang.jenjang,
					tabel_kelas.kelas,
					tabel_tahunajaran.tahun_ajaran,
					tabel_absen_mapel.tgl,
					tabel_absen_mapel.absen,
					tabel_guru.nama,
					tabel_mapel.mapel
				FROM tabel_siswakelas
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
				INNER JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				INNER JOIN tabel_jenjang ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang)
				INNER JOIN tabel_guru ON (tabel_kelas.id_guru = tabel_guru.id_guru)
				LEFT JOIN tabel_tahunajaran ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
				INNER JOIN tabel_absen_mapel ON (tabel_siswakelas.id_penempatan = tabel_absen_mapel.id_penempatan)
				INNER JOIN tabel_jadwal ON (tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal)
				INNER JOIN tabel_mapel ON (tabel_jadwal.id_mapel = tabel_mapel.id_mapel)
				" . $kondisi . "
				ORDER BY tabel_absen_mapel.id_absen_mapel DESC
			");
    }

    public function hapus_siswa($id)
    {
        $this->db->where('id_siswa', $id);
        $this->db->delete('tabel_siswakelas');
    }

    public function json_filter($start, $length, $search, $count, $sorting, $colsorting, $tgl_awal, $tgl_akhir, $status, $kondisi = null)
    {
        $where = '';
        $limit = '';

        if ($search != '') {
            $where .= "AND (
                    tabel_siswakelas.id_penempatan LIKE '%{$search}%'
                    OR tabel_siswa.nama_lengkap LIKE '%{$search}%'
                    OR tabel_siswa.id_siswa LIKE '%{$search}%'
                    OR tabel_siswa.nisn LIKE '%{$search}%'
                    OR tabel_jenjang.jenjang LIKE '%{$search}%'
					OR tabel_kelas.kelas LIKE '%{$search}%'
					OR tabel_tahunajaran.tahun_ajaran LIKE '%{$search}%'
					OR tabel_guru.nama LIKE '%{$search}%'
					OR tabel_mapel.mapel LIKE '%{$search}%'
                )";
        }

        if ($count == false) {
            $limit .= ' LIMIT ' . $start . ',' . $length . '';
        }

        $query = $this->db->query("
					SELECT
						tabel_siswakelas.id_penempatan,
						tabel_siswa.nama_lengkap,
						tabel_siswa.id_siswa,
						tabel_siswa.nisn,
						tabel_jenjang.jenjang,
						tabel_kelas.kelas,
						tabel_tahunajaran.tahun_ajaran,
						tabel_guru.nama,
						tabel_mapel.mapel,
						tabel_jadwal.id_jadwal,
						tabel_jadwal.hari
					FROM tabel_siswakelas
					LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
					INNER JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
					INNER JOIN tabel_jenjang ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang)
					INNER JOIN tabel_guru ON (tabel_kelas.id_guru = tabel_guru.id_guru)
					LEFT JOIN tabel_tahunajaran ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
					INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)
					INNER JOIN tabel_mapel ON (tabel_jadwal.id_mapel = tabel_mapel.id_mapel)
					 " . $kondisi . "
					 " . $where . "
				");

        if ($count == false) {
            $data = array();
            $no = 1;

            $begin = new DateTime($tgl_awal);
            $end = new DateTime($tgl_akhir);
            $end->modify('+1 day');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $tgl = $dt->format("d-m-Y");
                $hari = $dt->format("l");

                foreach ($query->result_array() as $row) {
                    $hariapa = $this->hari_apa($row['hari']);
                    if ($hariapa != $hari) {
                        continue;
                    }

                    $s = "SELECT * FROM tabel_absen_mapel WHERE tabel_absen_mapel.id_penempatan='" . $row['id_penempatan'] . "' and tabel_absen_mapel.tgl = '$tgl' and tabel_absen_mapel.id_jadwal='" . $row['id_jadwal'] . "' ";
                    // if($status=='semua'){
                    //     $s .= "";
                    // }elseif($status=='hadir'){
                    //     $s .= " and tabel_absen_mapel.absen='hadir'";
                    // }elseif($status=='tidak_hadir'){
                    //     $s .= " and tabel_absen_mapel.absen<>'hadir'";
                    // }elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
                    //     $s .= " and tabel_absen_mapel.absen='$status'";
                    // }
                    $d = $this->db->query($s);
                    $ambil = $d->row();
                    if ($status != 'semua') {
                        if ($d->num_rows() == 0) {
                            continue;
                        } else {
                            if ($status != $ambil->absen) {
                                continue;
                            }
                        }
                    }
                    if ($d->num_rows() > 0) {
                        if ($ambil->absen == '') {
                            $absen = "-";
                            $jam = '';
                        } else {
                            $absen_m = $ambil->absen;
                            if ($absen_m == 'alpha') {
                                $absen = "
									<a href='#' class='edit-status-kehadiran' data-type='select'
									data-url='adm/sinkron_kehadiran_guru/absen_manual'
									data-absenmapel='$ambil->id_absen_mapel' data-absen='$absen_m' data-title='Pilih keterangan'>
									<span class='label label-danger'>$absen_m</span>
									</a>";
                            } elseif ($absen_m == 'izin') {
                                $absen = "
									<a href='#' class='edit-status-kehadiran' data-type='select' data-url='adm/sinkron_kehadiran_guru/absen_manual'
									data-absenmapel='$ambil->id_absen_mapel'
									data-absen='$absen_m' data-title='Pilih keterangan'>
									<span class='label label-warning'>$absen_m</span></a>";
                            } elseif ($absen_m == 'sakit') {
                                $absen = "
									<a href='#' class='edit-status-kehadiran' data-type='select' data-url='adm/sinkron_kehadiran_guru/absen_manual'
									data-absenmapel='$ambil->id_absen_mapel'
									data-absen='$absen_m' data-title='Pilih keterangan'>
									<font class='label label-primary'>$absen_m</span></a>";
                            } elseif ($absen_m == 'hadir') {
                                $absen = "
									<a href='#' class='edit-status-kehadiran' data-type='select' data-url='adm/sinkron_kehadiran_guru/absen_manual'
									 data-absenmapel='$ambil->id_absen_mapel'
									 data-absen='$absen_m' data-title='Pilih keterangan'>
									<font class='label label-success'>$absen_m</span></a>";
                            }
                            $jam = $ambil->jam . " ";
                        }
                    } else {
                        $id_penempatan = $row['id_penempatan'];
                        $id_jadwal = $row['id_jadwal'];
                        $absen = "<a href='#' class='edit-status-kehadiran' data-type='select' data-url='adm/sinkron_kehadiran_guru/absen_manual'
									 data-presensi='tidak' data-absen='kosong' data-jadwal='$id_jadwal' data-tgl='$tgl'
									 data-penempatan='$id_penempatan' data-title='Pilih keterangan'>
									<font class='label label-default'>Tidak Absen</span></a>";
                        $jam = '';
                    }

                    $row['no'] = $no;
                    $row['absen'] = $absen;
                    $row['jam'] = $jam;
                    $row['tgl'] = $tgl;
                    $data[] = $row;

                    $no++;
                }
            }

            return $data;
        } else {
            $no = 1;

            $begin = new DateTime($tgl_awal);
            $end = new DateTime($tgl_akhir);
            $end->modify('+1 day');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $tgl = $dt->format("d-m-Y");
                $hari = $dt->format("l");

                foreach ($query->result_array() as $row) {
                    $hariapa = $this->hari_apa($row['hari']);
                    if ($hariapa != $hari) {
                        continue;
                    }

                    $s = "SELECT * FROM tabel_absen_mapel WHERE tabel_absen_mapel.id_penempatan='" . $row['id_penempatan'] . "' and tabel_absen_mapel.tgl = '$tgl' and tabel_absen_mapel.id_jadwal='" . $row['id_jadwal'] . "' ";
                    // if($status=='semua'){
                    //     $s .= "";
                    // }elseif($status=='hadir'){
                    //     $s .= " and tabel_absen_mapel.absen='hadir'";
                    // }elseif($status=='tidak_hadir'){
                    //     $s .= " and tabel_absen_mapel.absen<>'hadir'";
                    // }elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
                    //     $s .= " and tabel_absen_mapel.absen='$status'";
                    // }
                    $d = $this->db->query($s);
                    $ambil = $d->row();
                    if ($status != 'semua') {
                        if ($d->num_rows() == 0) {
                            continue;
                        } else {
                            if ($status != $ambil->absen) {
                                continue;
                            }
                        }
                    }

                    $no++;
                }
            }
            return $no - 1;
        }
    }

    public function countKelas($id_kelas)
    {
        $sql = "SELECT * FROM tabel_siswakelas WHERE id_kelas ='$id_kelas'";
        $count = $this->db->query($sql);
        return $count->num_rows();
    }

    public function countKelasByTAAndJenjang($id_ta, $id_jenjang)
    {
        $sql = "SELECT * FROM tabel_kelas WHERE id_ta ='$id_ta' AND id_jenjang = '$id_jenjang'";
        $count = $this->db->query($sql);
        return $count->num_rows();
    }

    public function getDataByIdKelas($id_kelas)
    {
        $sql = "SELECT * FROM tabel_siswakelas WHERE id_kelas ='$id_kelas'";
        return $this->db->query($sql);

    }

    function get_absensi_mapel_by_id_kelas($id_kelas, $id_jadwal, $tgl)
    {   
        $response = $this->db->query("SELECT sk.id_penempatan,s.id_siswa, s.id_jenjang, s.nama_lengkap,
            am.id_jadwal, am.tgl, am.jam, am.absen
            FROM tabel_siswakelas sk 
            JOIN tabel_siswa s ON sk.id_siswa = s.id_siswa
            LEFT JOIN tabel_absen_mapel am ON am.id_penempatan=sk.id_penempatan
            AND am.id_jadwal= $id_jadwal AND am.tgl='$tgl' 
            WHERE sk.id_kelas = $id_kelas")->result_array();

        return $response;
    }
}
