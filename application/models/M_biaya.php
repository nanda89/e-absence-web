<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_biaya extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_biaya INNER JOIN tabel_jenjang
				ON tabel_biaya.id_jenjang = tabel_jenjang.id_jenjang
				ORDER BY biaya asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_biaya
				WHERE id_biaya = '$id'";
		return $this->db->query($sql);
	}	
	public function get_biaya($id)
	{
		$sql = "SELECT * FROM tabel_biaya
				WHERE id_jenjang = '$id' ORDER BY id_biaya ASC";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_biaya',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_biaya', $id);
		$this->db->update('tabel_biaya', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_biaya', $id);
		$this->db->delete('tabel_biaya'); 
	}
}