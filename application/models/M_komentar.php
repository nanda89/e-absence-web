<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_komentar extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_artikel INNER JOIN tabel_komentar
				ON tabel_artikel.id_komentar = tabel_komentar.id_komentar
				ORDER BY tabel_komentar.id_komentar asc";
		return $this->db->query($sql);
	}
	public function get_sub($id)
	{
		$sql = "SELECT * FROM tabel_komentar INNER JOIN tabel_sub_komentar
				ON tabel_komentar.id_komentar = tabel_sub_komentar.id_komentar
				WHERE tabel_komentar.id_komentar = '$id'
				ORDER BY tabel_sub_komentar.id_sub_komentar asc";
		return $this->db->query($sql);
	}

	public function get_jum_komen($id)
	{
		$sql = "SELECT COUNT(*) AS jum FROM  tabel_komentar 
			INNER JOIN tabel_artikel
			ON tabel_komentar.id_artikel = tabel_artikel.id_artikel
			WHERE tabel_komentar.id_artikel='$id'";    		
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_artikel INNER JOIN tabel_komentar
				ON tabel_artikel.id_komentar = tabel_komentar.id_komentar
				WHERE tabel_komentar.id_komentar = '$id'";
		return $this->db->query($sql);
	}
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_komentar',$data);
	}
	public function tambah_k($data)
	{
		$query=$this->db->insert('tabel_sub_komentar',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update('tabel_artikel', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_komentar', $id);
		$this->db->delete('tabel_komentar'); 
	}
}