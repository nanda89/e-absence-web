<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_pilihan extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_pilihan';
    }

    protected function process($param = null, $use_param_only = false)
    {
        $data = $param;
        if ( $use_param_only == false )
        {
        }
        return $data;
    }

    public function get($where = null , $single = false)
    {
        $this->db->order_by('t.huruf');
        return parent::get($where, $single);
    }
}