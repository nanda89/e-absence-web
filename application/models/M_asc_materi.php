<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_materi extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_materi';
    }

    protected function init_rule()
    {
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('materi', 'materi', 'required');
        $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required');
    }

    public function list_by($where)
    {
        $this->db->from($this->table . ' t');
        $this->db->join('tabel_mapel m', 'm.id_mapel=t.id_mapel', 'inner');
        if ( $where != null )
        {
            foreach ($where as $key => $value)
            {
                $this->db->where('t.'.$key, $value);
            }
        }
        $this->db->order_by("t.created", "DESC");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete($where)
    {
        $materi = $this->single($where);
        if ( $materi == null)
            return false;

        if ( parent::delete($where) )
        {
            $this->remove_materi_file($materi, "gambar");
            $this->remove_materi_file($materi, "suara");
            $this->remove_materi_file($materi, "file");
            return true;
        }
        return false;
    }

    private function remove_materi_file($materi, $column)
    {
        if ( !empty($materi[$column]) || $materi[$column] != null)
        {
            $file = $this->config->item("upload_path") . $materi[$column];
            $this->remove_file($file);
        }
    }

    public function single($where)
    {
        $this->db->from($this->table);
        $this->db->where($where);
        $q = $this->db->get();
        if ( $q->num_rows() > 0 )
            return $q->row_array();
        return null;
    }

    public function get($where = null , $single = false)
    {
        $this->db->select('t.id_materi, t.file, t.judul, t.id_mapel, m.mapel, t.materi, t.gambar, t.suara, t.doc_status,t.created, t.updated');
        $this->db->join('tabel_mapel m', 'm.id_mapel=t.id_mapel');
        return parent::get($where, $single);
    }

    protected function process($param = null, $use_param_only = false)
    {
        $data = $param;
        if ( $use_param_only == false )
        {
            $data['judul'] = $this->input->post('judul');
            $data['materi'] = $this->input->post('materi');
            $data['id_mapel'] = $this->input->post('mapel');
            $data['doc_status'] = $this->input->post('status');
            if ( $param != null )
            {
                $data = array_merge($data, $param);
            }
        }
        if ( empty($data['id_mapel']) || $data['id_mapel'] == null )
        {
            $this->set_error("harap masukan mapel");
            return null;
        }
        if ( empty($data['doc_status']) || $data['doc_status'] == null )
        {
            $data['doc_status'] = $this->config->item("doc_status_draft");
        }
        return $data;
    }
}
