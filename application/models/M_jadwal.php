<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_jadwal extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_kelas
    	INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)
    	INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
    	INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
    	INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)";
		return $this->db->query($sql);
	}

	public function get_one_kelas($id_jenjang)
	{
		$sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang' 
				ORDER BY tabel_kelas.kelas ASC";
		return $this->db->query($sql);
	}
	public function get_jadwal($id_tahun, $id_jenjang, $id_kelas, $id_guru)
	{
		$kondisi = "";
		$pertama = true;
		if ($id_tahun != "" && $id_tahun != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal.id_ta = '$id_tahun'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal.id_ta = '$id_tahun'";
			}
		}
		if ($id_jenjang != "" && $id_jenjang != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal.id_jenjang = '$id_jenjang'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal.id_jenjang = '$id_jenjang'";
			}
		}
		if ($id_kelas != "" && $id_kelas != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal.id_kelas = '$id_kelas'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal.id_kelas = '$id_kelas'";
			}
		}
		if ($id_guru != "" && $id_guru != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal.id_guru = '$id_guru'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal.id_guru = '$id_guru'";
			}
		}
		$sql = "SELECT * FROM tabel_kelas
	    	INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)
	    	INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
	    	INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
    		INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)";
		$sql .= $kondisi;
		$sql .= " ORDER BY tabel_jadwal.hari,tabel_jadwal.jam_awal ASC";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_mapel
				WHERE id_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function get_one_kat($id)
	{
		$sql = "SELECT * FROM tabel_kat_mapel
				WHERE id_kat_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function tambah($data)
	{
		$query = $this->db->insert('tabel_jadwal', $data);
	}

	public function tambah_kat($data)
	{
		$query = $this->db->insert('tabel_kat_mapel', $data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_mapel', $id);
		$this->db->update('tabel_mapel', $data);
	}

	function edit_kat($id, $data)
	{
		$this->db->where('id_kat_mapel', $id);
		$this->db->update('tabel_kat_mapel', $data);
	}

	function hapus($id)
	{
		$this->db->where('id_mapel', $id);
		$this->db->delete('tabel_mapel');
	}
	function hapus_all()
	{
		$sql = "TRUNCATE TABLE tabel_jadwal";
		return $this->db->query($sql);
	}
	function hapus_jadwal($id)
	{
		$this->db->where('id_jadwal', $id);
		$this->db->delete('tabel_jadwal');
	}

	function get_jadwal_by_id($id)
	{
		$sql = "SELECT * FROM tabel_jadwal
				WHERE id_jadwal = '$id'";
		return $this->db->query($sql);
	}
}
