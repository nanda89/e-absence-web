<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pesan extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_pesan
				ORDER BY id_pesan asc";
		return $this->db->query($sql);
	}
	public function get_unread()
	{
		$sql = "SELECT * FROM tabel_pesan
				WHERE status = 'unread'
				ORDER BY id_pesan asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_pesan
				WHERE id_pesan = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_pesan',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_pesan', $id);
		$this->db->update('tabel_pesan', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_pesan', $id);
		$this->db->delete('tabel_pesan'); 
	}
}