<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_absen_server extends CI_Model {

	var $table = <<<EOT
 (
    SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_siswa.nisn,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_absen_mapel.id_absen_mapel,
			tabel_tahunajaran.tahun_ajaran,tabel_absen_mapel.tgl,tabel_absen_mapel.absen,tabel_guru.nama,tabel_mapel.mapel
		FROM tabel_siswakelas LEFT JOIN tabel_siswa
        ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa INNER JOIN tabel_kelas        
		ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas INNER JOIN tabel_jenjang
        ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_guru      	
        ON tabel_kelas.id_guru = tabel_guru.id_guru LEFT JOIN tabel_tahunajaran
        ON tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta INNER JOIN tabel_absen_mapel
        ON tabel_siswakelas.id_penempatan = tabel_absen_mapel.id_penempatan INNER JOIN tabel_jadwal
        ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal INNER JOIN tabel_mapel
        ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
 ) temp
EOT;

	var $column_order = array(null,'id_absen_mapel','nisn','id_siswa','nama_lengkap','mapel','kelas','tgl','absen'); //set column field database for datatable orderable
	var $column_search = array('id_absen_mapel','nisn','id_siswa','nama_lengkap','mapel','kelas','tgl','absen'); //set column field database for datatable searchable
	var $order = array('id_absen_mapel' => 'desc'); // default order

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{

		$this->db->from($this->table);

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{

				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

}
