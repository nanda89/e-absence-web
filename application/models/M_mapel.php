<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_mapel extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_mapel LEFT JOIN tabel_kat_mapel
				ON tabel_mapel.id_kat_mapel = tabel_kat_mapel.id_kat_mapel 
				LEFT JOIN tabel_jenjang
				ON tabel_mapel.id_jenjang = tabel_jenjang.id_jenjang
				ORDER BY tabel_kat_mapel.kategori_mapel,tabel_mapel.mapel asc";
		return $this->db->query($sql);
	}

	public function get_all_kat()
	{
		$sql = "SELECT * FROM tabel_kat_mapel
				ORDER BY id_kat_mapel asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_mapel
				WHERE id_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function get_one_kat($id)
	{
		$sql = "SELECT * FROM tabel_kat_mapel
				WHERE id_kat_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function get_all_jen()
	{
		$sql = "SELECT * FROM tabel_mapel";
		return $this->db->query($sql);
	}

	public function get_one_jen($id)
	{
		$sql = "SELECT * FROM tabel_mapel
				WHERE id_jenjang = '$id' AND jenis = 'reguler'";
		return $this->db->query($sql);
	}

	public function get_one_jen_tambah($id)
	{
		$sql = "SELECT * FROM tabel_mapel
				WHERE id_jenjang = '$id' AND jenis = 'tambahan'";
		return $this->db->query($sql);
	}

	public function tambah($data)
	{
		$query = $this->db->insert('tabel_mapel', $data);
	}

	public function tambah_kat($data)
	{
		$query = $this->db->insert('tabel_kat_mapel', $data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_mapel', $id);
		$this->db->update('tabel_mapel', $data);
	}

	function edit_kat($id, $data)
	{
		$this->db->where('id_kat_mapel', $id);
		$this->db->update('tabel_kat_mapel', $data);
	}

	function hapus($id)
	{
		$this->db->where('id_mapel', $id);
		$this->db->delete('tabel_mapel');
	}
	function hapus_kat($id)
	{
		$this->db->where('id_kat_mapel', $id);
		$this->db->delete('tabel_kat_mapel');
	}
}
