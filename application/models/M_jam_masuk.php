<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jam_masuk extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_jam_masuk
				ORDER BY tabel_jam_masuk.hari asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_jam_masuk
				WHERE id_jam_masuk = '$id'";
		return $this->db->query($sql);
	}		
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_jam_masuk',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_jam_masuk', $id);
		$this->db->update('tabel_jam_masuk', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_jam_masuk', $id);
		$this->db->delete('tabel_jam_masuk'); 
	}
	function remove_checked() 
    {
	 	$delete = $this->input->post('item');
 		for ($i=0; $i < count($delete) ; $i++) 
 		{ 
 			$this->db->where('id_jam_masuk', $delete[$i]);
 			$this->db->delete('tabel_jam_masuk');
 		}
	}
	 
}