<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_penempatan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all($id)
    {
        $sql = "SELECT * FROM tabel_siswa LEFT JOIN tabel_jenjang ON
			tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang
	        WHERE tabel_siswa.id_jenjang = '$id'";
        return $this->db->query($sql);
    }
    public function get_one_kelas($id_jenjang)
    {
        $sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				ORDER BY tabel_kelas.kelas ASC";
        return $this->db->query($sql);
    }
    public function get_one_tingkat($id_jenjang)
    {
        $sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				ORDER BY tabel_kelas.kelas ASC";
        return $this->db->query($sql);
    }
    public function get_penempatan($id_tahun, $angkatan, $id_kelas)
    {
        $sql = "SELECT * FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'
	        AND tabel_siswakelas.angkatan = '$angkatan'";
        return $this->db->query($sql);
    }
    public function get_siswa($id_tahun, $id_kelas)
    {
        $sql = "SELECT tabel_siswakelas.*,tabel_siswa.*,tabel_jenjang.*,tabel_kelas.*,tabel_tahunajaran.*,tabel_guru.nama FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas
				ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'";
        return $this->db->query($sql);
    }
    public function get_siswa_by_filter($id_tahun, $id_kelas, $id_jenjang)
    {
        $sql = "SELECT tabel_siswakelas.*,tabel_siswa.*,tabel_jenjang.*,tabel_kelas.*,tabel_tahunajaran.*,tabel_guru.nama FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas
				ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' AND tabel_kelas.id_jenjang = '$id_jenjang'";
        return $this->db->query($sql);
    }
    public function get_lulus($angkatan)
    {
        $sql = "SELECT * FROM tabel_siswa LEFT JOIN tabel_kelulusan
        ON (tabel_siswa.id_siswa = tabel_kelulusan.id_siswa) LEFT JOIN tabel_tahunajaran
        ON (tabel_tahunajaran.id_ta = tabel_kelulusan.id_ta) LEFT JOIN tabel_kelas
        ON (tabel_kelas.id_kelas = tabel_kelulusan.id_kelas)
       	WHERE tabel_kelulusan.angkatan='$angkatan'";
        return $this->db->query($sql);
    }
    public function get_all_kelas()
    {
        $sql = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.id_siswa,tabel_siswa.nisn,
						tabel_siswa.nama_lengkap,tabel_jenjang.id_jenjang,tabel_jenjang.jenjang,tabel_kelas.id_kelas,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran
					FROM tabel_siswakelas RIGHT JOIN tabel_siswa
		      ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas
					ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
	        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        ORDER BY tabel_siswa.id_siswa ASC";
        return $this->db->query($sql);
    }
    public function get_pendataan($id_tahun, $id_kelas)
    {
        $sql = "SELECT * FROM tabel_siswakelas INNER JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'";
        return $this->db->query($sql);
    }
    public function cek_siswa($id)
    {
        $sql = "SELECT * FROM tabel_siswakelas
	        WHERE tabel_siswakelas.id_siswa = '$id'";
        return $this->db->query($sql);
    }
    public function tambah($data)
    {
        $query = $this->db->insert('tabel_siswakelas', $data);
    }
    public function edit($id, $data)
    {
        $this->db->where('id_siswa', $id);
        $this->db->update('tabel_siswakelas', $data);
    }
    public function hapus($id)
    {
        $this->db->where('id_penempatan', $id);
        $this->db->delete('tabel_siswakelas');
    }
}
