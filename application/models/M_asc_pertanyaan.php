<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_pertanyaan extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_pertanyaan';
    }

    public function get($where = null, $single = false)
    {
        $this->db->distinct();
        $this->db->order_by("no_urut", "RANDOM");
        $this->db->limit(50);
        return parent::get($where, $single);
    }

    public function generate_no_urut($ujian)
    {
        $query = $this->db->from($this->table)
            ->select("COALESCE(MAX(no_urut),0) as no")
            ->where("id_ujian", $ujian["id_ujian"])
            ->get();
        if ($query == false)
            return 1;

        return $query->row_array()["no"] + 1;
    }

    protected function process($param = null, $use_param_only = false)
    {
        $data = $param;
        if ($use_param_only == false) {
            $data['no_urut'] = $this->input->post('no_urut');
            $data['pertanyaan'] = $this->input->post('pertanyaan');
            $data['jawaban'] = $this->input->post('jawaban');
            if ($param != null) {
                $data = array_merge($data, $param);
            }
        }
        return $data;
    }

    protected function init_rule()
    {
        $this->form_validation->set_rules('pertanyaan', 'Pertanyaan', 'required');
    }
}
