<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_jadwal_ujian_reserve extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'm_asc_jadwal_ujian_reserve';
    }
}