<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_penjadwalan extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_daf_jadwal INNER JOIN tabel_daf_jenis
				ON tabel_daf_jadwal.id_daf_jenis = tabel_daf_jenis.id_daf_jenis
				GROUP BY tabel_daf_jadwal.id_daf_jadwal asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_daf_jadwal
				WHERE id_daf_jadwal = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_daf_jadwal',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_daf_jadwal', $id);
		$this->db->update('tabel_daf_jadwal', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_daf_jadwal', $id);
		$this->db->delete('tabel_daf_jadwal'); 
	}
}