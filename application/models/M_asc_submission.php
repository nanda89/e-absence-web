<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_submission extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_submission';
    }

    public function publish($jadwal_ujian)
    {
        $where = array(
            "id_jadwal_ujian" => $jadwal_ujian["id_jadwal_ujian"],
            "doc_status" => $this->config->item("doc_status_join")
        );
        //$this->db->or_where('doc_status', $this->config->item("doc_status_draft"));
        $query = $this->db->select("id_peserta_ujian")
                            ->from("asc_peserta_ujian")
                            ->where($where)
                            ->get();
        if ( $query->num_rows() > 0 )
        {
            $this->set_error("Ada peserta ujian yang belum mengumpulkan");
            return false;
        }
        if ( $jadwal_ujian['tipe'] == $this->config->item("ujian_tipe")['p'] )
        {
            $query = $this->db->select("s.id_peserta_ujian, p.pertanyaan, p.jawaban as jawaban_siswa, s.jawaban, 
ROUND ( SUM(CASE WHEN p.jawaban=s.jawaban THEN 1 ELSE 0 END)/u.jumlah_pertanyaan*100 ) as nilai")
                    ->from("asc_submission s")
                    ->join("asc_pertanyaan p"," p.id_pertanyaan=s.id_pertanyaan", "inner")
                    ->join("asc_ujian u","u.id_ujian=p.id_ujian","inner")
                    ->join("asc_peserta_ujian ps","ps.id_peserta_ujian=s.id_peserta_ujian", "inner")
                    ->where("p.id_ujian", $jadwal_ujian['id_ujian'])
                    ->where("ps.id_jadwal_ujian", $jadwal_ujian['id_jadwal_ujian'])
                    ->group_by("id_peserta_ujian")
                    ->get();
            $list_peserta = $query->result_array();
            foreach ($list_peserta as $peserta)
            {
                $param = array(
                    "nilai" => $peserta["nilai"],
                    "doc_status" => $this->config->item("doc_status_publish")
                );
                $where = array(
                    "id_peserta_ujian" => $peserta["id_peserta_ujian"]
                );
                $this->db->where($where);
                $this->db->update("asc_peserta_ujian", $param);
            }
        }
        else
        {
            $where = array(
                "p.id_jadwal_ujian" => $jadwal_ujian['id_jadwal_ujian'],
                "s.doc_status" => $this->config->item("doc_status_draft")
            );
            $query = $this->db->select("s.id_submission, s.doc_status")
                    ->from("asc_submission s")
                    ->join("asc_peserta_ujian p","p.id_peserta_ujian=s.id_peserta_ujian", "inner")
                    ->where($where)
                    ->get();
            if ( $query->num_rows() > 0 )
            {
                $this->set_error("Terdapat jawaban yang belum dikoreksi.");
                return false;
            }
            $where = array(
                "p.id_jadwal_ujian" => $jadwal_ujian['id_jadwal_ujian'],
                "s.doc_status" =>  $this->config->item("doc_status_koreksi")
            );
            $query = $this->db->select("p.id_peserta_ujian, sum(s.nilai) as nilai")
                     ->from("asc_submission s")
                     ->join("asc_peserta_ujian p", "p.id_peserta_ujian=s.id_peserta_ujian", "inner")
                     ->where($where)
                     ->group_by("s.id_peserta_ujian")
                     ->get();
            $list_peserta = $query->result_array();
            foreach ($list_peserta as $peserta)
            {
                $param = array(
                    "nilai" => $peserta["nilai"],
                    "doc_status" => $this->config->item("doc_status_publish")
                );
                $where = array(
                    "id_peserta_ujian" => $peserta["id_peserta_ujian"]
                );
                $this->db->where($where);
                $this->db->update("asc_peserta_ujian", $param);
            }
        }
        $param = array(
                'doc_status' => $this->config->item("doc_status_done")
            );
        $where = array(
            'id_jadwal_ujian' => $jadwal_ujian['id_jadwal_ujian']
        );
        $this->db->where($where);
        $this->db->update("asc_jadwal_ujian", $param);
        return true;
    }

    public function list_nilai($where, $key = null)
    {
        if ( $where != null )
        {
            $this->db->where($where);
        }
        $this->db->where('p.doc_status', $this->config->item("doc_status_publish"));
        if ( $key != null )
        {
            $this->db->like("u.judul", $key, "both");
            $this->db->or_like("u.deskripsi", $key, "both");
        }
        $this->db->select("p.id_peserta_ujian, j.id_jadwal_ujian, u.tipe, u.kode, m.mapel, g.nama as guru, u.judul, u.deskripsi, p.nilai as total, j.waktu_mulai");
        $this->db->from("asc_peserta_ujian p");
        $this->db->join("asc_jadwal_ujian j","j.id_jadwal_ujian=p.id_jadwal_ujian","inner");
        $this->db->join("asc_ujian u","u.id_ujian=j.id_ujian","inner");
        $this->db->join("tabel_mapel m","m.id_mapel=u.id_mapel","inner");
        $this->db->join("tabel_guru g","g.id_guru=u.id_guru","inner");
        $query = $this->db->get();
        return $query->result_array();

    }

    public function list_by($where = null , $key = null)
    {
        if ( $where != null )
        {
            $this->db->where($where);
        }
        $query = $this->db->select("s.id_submission, s.id_submission, p.id_pertanyaan, u.tipe, s.jawaban, s.gambar, s.suara, s.koreksi, s.nilai,
p.pertanyaan, p.gambar as pertanyaan_gambar, p.suara as pertanyaan_suara, p.jawaban as jawaban_guru, p.no_urut")
                            ->from("asc_submission s")
                            ->join("asc_pertanyaan p", "p.id_pertanyaan=s.id_pertanyaan", "inner")
                            ->join("asc_ujian u", "u.id_ujian=p.id_ujian", "inner")
                            ->order_by("p.no_urut")
                            ->get();
        return $query->result_array();
    }
}