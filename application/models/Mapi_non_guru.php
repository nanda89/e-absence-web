<?php

	class Mapi_non_guru extends CI_Model
	{
		function get($id = NULL)
		{
			$this->db->select('*');
			$this->db->from('tabel_non_guru');
			if($id != NULL) $this->db->where('id_non_guru',$id);
			$query = $this->db->get();

			return ($id == NULL) ? $query->result_array() : $query->row_array();
		}

		function save($data = NULL,$id = NULL,$gambar = NULL)
		{
			if($id == NULL){
				$this->db->insert('tabel_non_guru',$data);
			}else{
				$this->db->where('id_non_guru',$id);
				$this->db->update('tabel_non_guru',$data);
			}
			if ($gambar != null) {
				$nip = $data['nip'];
					$decoded=base64_decode($gambar);
					do {
						$avatar = '';
						$keys = array_merge(range(0, 9), range('a', 'z'));

						for ($i = 0; $i < 10; $i++) {
							$avatar .= $keys[array_rand($keys)];
						}
						$avatar .= '.png';
					} while (file_exists('assets/panel/images/'.$avatar));
					file_put_contents('assets/panel/images/'.$avatar,$decoded);
					$this->db->where('nip', $nip);
					$this->db->update('tabel_non_guru', array('gambar' => $avatar));
				
			}
		}
	}