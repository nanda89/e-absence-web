<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kalender extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_kalender
				ORDER BY tanggal asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_kalender
				WHERE id_kalender = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_kalender',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_kalender', $id);
		$this->db->update('tabel_kalender', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_kalender', $id);
		$this->db->delete('tabel_kalender'); 
	}
}