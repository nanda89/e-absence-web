<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_konfigurasi extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_daf_jenis INNER JOIN tabel_jenjang
				ON tabel_daf_jenis.id_jenjang = tabel_jenjang.id_jenjang
				ORDER BY nama_proses asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_daf_jenis
				WHERE id_daf_jenis = '$id'";
		return $this->db->query($sql);
	}	
	public function get_one_jadwal($id)
	{
		$sql = "SELECT * FROM tabel_daf_jenis INNER JOIN tabel_daf_jadwal 
        	ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis)
			WHERE tabel_daf_jenis.id_daf_jenis = '$id'";
		return $this->db->query($sql);
	}
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_daf_jenis',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_daf_jenis', $id);
		$this->db->update('tabel_daf_jenis', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_daf_jenis', $id);
		$this->db->delete('tabel_daf_jenis'); 
	}
}