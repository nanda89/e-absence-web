<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_artikel extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_artikel LEFT JOIN tabel_kategori
				ON tabel_artikel.id_kategori = tabel_kategori.id_kategori
				ORDER BY id_artikel asc";
		return $this->db->query($sql);
	}
	public function get_pop()
	{
		$sql = "SELECT * FROM tabel_artikel LEFT JOIN tabel_kategori
				ON tabel_artikel.id_kategori = tabel_kategori.id_kategori
				WHERE tabel_artikel.status = 'publish'
				ORDER BY dibaca desc LIMIT 7";
		return $this->db->query($sql);
	}
	public function get_all_page ($perpage, $uri)
	{
		//$sql_query=$this->db->get('tabel_artikel',$perpage, $uri);	

		$this->db->select('*')
					->from('tabel_artikel')
					->join('tabel_kategori','tabel_artikel.id_kategori=tabel_kategori.id_kategori')					
					->where('tabel_kategori.kategori','Beranda')
					->limit($perpage,$uri);
		return $this->db->get();

		//return $sql_query;
	}
	public function get_all_kategori()
	{
		$sql = "SELECT * FROM tabel_kategori				
				ORDER BY kategori asc";
		return $this->db->query($sql);
	}
	public function get_edit($id)
	{
		$sql = "SELECT * FROM tabel_artikel LEFT JOIN tabel_kategori
				ON tabel_artikel.id_kategori = tabel_kategori.id_kategori
				WHERE id_artikel = '$id'";
		return $this->db->query($sql);
	}
	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_artikel LEFT JOIN tabel_kategori
				ON tabel_artikel.id_kategori = tabel_kategori.id_kategori
				WHERE permalink = '$id'";
		return $this->db->query($sql);
	}	
	public function get_one_komen($id)
	{
		$sql = "SELECT * FROM tabel_artikel INNER JOIN tabel_komentar
				ON tabel_artikel.id_artikel = tabel_komentar.id_artikel
				WHERE tabel_artikel.status = 'publish'
				AND tabel_artikel.permalink = '$id'";
		return $this->db->query($sql);
	}
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_artikel',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update('tabel_artikel', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_artikel', $id);
		$this->db->delete('tabel_artikel'); 
	}
}