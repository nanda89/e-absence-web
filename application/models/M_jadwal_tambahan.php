<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_jadwal_tambahan extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_kelas_regular($id_jenjang)
	{
		$sql = "SELECT * FROM tabel_kelas
		WHERE id_jenjang = '$id_jenjang'";
		return $this->db->query($sql);
	}

	public function get_kelas_tambahan($id_jenjang)
	{
		$sql = "SELECT DISTINCT 
					tabel_jadwal_tambahan.id_jadwal_tambahan AS id_kelas,
					tabel_jadwal_tambahan.nama_kelas AS nama_kelas
				FROM tabel_kelas_tambahan
		INNER JOIN tabel_jadwal_tambahan 
			ON tabel_jadwal_tambahan.id_jadwal_tambahan = tabel_kelas_tambahan.id_jadwal_tambahan
		WHERE tabel_kelas_tambahan.id_jadwal_tambahan IN 
			(SELECT id_jadwal_tambahan 
				FROM tabel_jadwal_tambahan a WHERE a.id_jenjang = '$id_jenjang')";
		return $this->db->query($sql);
	}

	public function get_siswa_reg_by_kelas($id_kelas)
	{
		$sql = "SELECT * FROM tabel_siswakelas 
			INNER JOIN tabel_siswa ON tabel_siswa.id_siswa = tabel_siswakelas.id_siswa
			INNER JOIN tabel_kelas ON tabel_kelas.id_kelas = tabel_siswakelas.id_kelas
			WHERE tabel_siswakelas.id_kelas = '$id_kelas' 
			AND tabel_siswakelas.id_ta = 
				(SELECT id_ta FROM tabel_tahunajaran ORDER BY id_ta DESC LIMIT 1)";

		return $this->db->query($sql);
	}

	public function get_all_student_by_kls_tambahan($id_jadwal)
	{
		$sql = "SELECT * FROM tabel_kelas_tambahan 
		INNER JOIN tabel_siswa ON tabel_siswa.id_siswa = tabel_kelas_tambahan.id_siswa
		INNER JOIN tabel_siswakelas ON tabel_siswakelas.id_siswa = tabel_kelas_tambahan.id_siswa
		INNER JOIN tabel_kelas ON tabel_kelas.id_kelas = tabel_siswakelas.id_kelas
		WHERE id_jadwal_tambahan = '$id_jadwal'";

		return $this->db->query($sql);
	}

	public function get_siswa_tmbhn_by_kelas($id_jadwal_tambahan)
	{
		$sql = "SELECT * FROM tabel_kelas_tambahan 
		INNER JOIN tabel_siswa ON tabel_siswa.id_siswa = tabel_kelas_tambahan.id_siswa
		INNER JOIN tabel_siswakelas ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
		INNER JOIN tabel_kelas ON tabel_kelas.id_kelas = tabel_siswakelas.id_kelas
		WHERE id_jadwal_tambahan = '$id_jadwal_tambahan'";

		return $this->db->query($sql);
	}

	public function get_one_kelas($id_jenjang)
	{
		$sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang' 
				ORDER BY tabel_kelas.kelas ASC";
		return $this->db->query($sql);
	}

	public function get_jadwal_by_id($jadwal_tambahan)
	{
		$sql = "SELECT * FROM tabel_jadwal_tambahan 
			WHERE id_jadwal_tambahan = '$jadwal_tambahan'";
		return $this->db->query($sql);
	}

	public function get_kelas_tambahan_by_id($jadwal_tambahan)
	{
		$sql = "SELECT * FROM tabel_jadwal_tambahan 
			INNER JOIN tabel_tahunajaran ON tabel_tahunajaran.id_ta =tabel_jadwal_tambahan.id_ta
			INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal_tambahan.id_jenjang)
	    	INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal_tambahan.id_guru)
    		INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal_tambahan.id_mapel)
			WHERE id_jadwal_tambahan = '$jadwal_tambahan'";
		return $this->db->query($sql);
	}

	public function get_jadwal($id_tahun, $id_jenjang, $nama_kelas, $id_guru, $id_mapel, $id_pembimbing1)
	{
		$kondisi = "";
		$pertama = true;
		if ($id_tahun != "" && $id_tahun != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.id_ta = '$id_tahun'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.id_ta = '$id_tahun'";
			}
		}
		if ($id_jenjang != "" && $id_jenjang != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.id_jenjang = '$id_jenjang'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.id_jenjang = '$id_jenjang'";
			}
		}
		if ($nama_kelas != "" && $nama_kelas != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.nama_kelas = '$nama_kelas'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.nama_kelas = '$nama_kelas'";
			}
		}
		if ($id_guru != "" && $id_guru != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.id_guru = '$id_guru'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.id_guru = '$id_guru'";
			}
		}

		if ($id_mapel != "" && $id_mapel != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.id_mapel = '$id_mapel'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.id_mapel = '$id_mapel'";
			}
		}

		if ($id_pembimbing1 != "" && $id_pembimbing1 != null) {
			if ($pertama) {
				$kondisi .= " where tabel_jadwal_tambahan.id_pembimbing_1 = '$id_pembimbing1'";
				$pertama = false;
			} else {
				$kondisi .= " and tabel_jadwal_tambahan.id_pembimbing_1 = '$id_pembimbing1'";
			}
		}

		$sql = "SELECT * FROM tabel_jadwal_tambahan
	    	INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal_tambahan.id_jenjang)
	    	INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal_tambahan.id_guru)
    		INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal_tambahan.id_mapel)";
		$sql .= $kondisi;
		$sql .= " ORDER BY tabel_jadwal_tambahan.hari,tabel_jadwal_tambahan.jam_awal ASC";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_mapel
				WHERE id_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function get_one_kat($id)
	{
		$sql = "SELECT * FROM tabel_kat_mapel
				WHERE id_kat_mapel = '$id'";
		return $this->db->query($sql);
	}

	public function tambah($data)
	{
		$query = $this->db->insert('tabel_jadwal_tambahan', $data);
	}

	public function tambah_siswa($data)
	{
		$query = $this->db->insert('tabel_kelas_tambahan', $data);
	}

	public function tambah_kat($data)
	{
		$query = $this->db->insert('tabel_kat_mapel', $data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_mapel', $id);
		$this->db->update('tabel_mapel', $data);
	}

	function edit_kat($id, $data)
	{
		$this->db->where('id_kat_mapel', $id);
		$this->db->update('tabel_kat_mapel', $data);
	}

	function hapus($id)
	{
		$this->db->where('id_mapel', $id);
		$this->db->delete('tabel_mapel');
	}
	function hapus_all()
	{
		$sql = "TRUNCATE TABLE tabel_jadwal";
		return $this->db->query($sql);
	}
	function hapus_jadwal($id)
	{
		$this->db->where('id_jadwal_tambahan', $id);
		$this->db->delete('tabel_kelas_tambahan');

		$this->db->where('id_jadwal_tambahan', $id);
		$this->db->delete('tabel_jadwal_tambahan');
	}

	function hapus_siswa($id, $id_siswa)
	{
		$this->db->where('id_jadwal_tambahan', $id);
		$this->db->where('id_siswa', $id_siswa);
		$this->db->delete('tabel_kelas_tambahan');
	}
}
