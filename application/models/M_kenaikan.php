<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kenaikan extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all($id)
	{
		$sql = "SELECT * FROM tabel_siswa LEFT JOIN tabel_jenjang ON
			tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang	        
	        WHERE tabel_siswa.id_jenjang = '$id'";
		return $this->db->query($sql);
	}	
	public function get_one_kelas($id_jenjang,$tingkat)
	{
		$sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang' AND tabel_kelas.tingkat = '$tingkat'
				ORDER BY tabel_kelas.kelas ASC";
		return $this->db->query($sql);
	}
	public function get_one_tingkat($id_jenjang)
	{
		$sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				ORDER BY tabel_kelas.kelas ASC";
		return $this->db->query($sql);
	}
	public function get_penempatan($id_tahun,$angkatan,$id_kelas)
	{
		$sql = "SELECT * FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang 
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'
	        AND tabel_siswakelas.angkatan = '$angkatan' AND tabel_siswakelas.status = 'aktif'";
		return $this->db->query($sql);
	}	
	public function get_siswa($id_tahun,$id_kelas)
	{
		$sql = "SELECT * FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang 
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'";
		return $this->db->query($sql);
	}
	public function get_all_kelas()
	{
		$sql = "SELECT * FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang 
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        ORDER BY tabel_siswa.nama_lengkap ASC";
		return $this->db->query($sql);
	}
	public function get_pendataan($id_tahun,$id_kelas)
	{
		$sql = "SELECT * FROM tabel_siswakelas RIGHT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang 
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun'";
		return $this->db->query($sql);
	}	
	public function cek_siswa($id)
	{
		$sql = "SELECT * FROM tabel_siswakelas 
	        WHERE tabel_siswakelas.id_siswa = '$id' AND tabel_siswakelas.status = 'aktif'";
		return $this->db->query($sql);
	}
	public function cek_lulus($id)
	{
		$sql = "SELECT * FROM tabel_kelulusan 
	        WHERE tabel_kelulusan.id_siswa = '$id'";
		return $this->db->query($sql);
	}
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_siswakelas',$data);
	}
	public function tambah_lulus($data)
	{
		$query=$this->db->insert('tabel_kelulusan',$data);
	}
	public function edit($id, $data)
	{
		$this->db->where('id_penempatan', $id);
		$this->db->update('tabel_siswakelas', $data); 
	}
	public function edit_lulus($id, $data)
	{
		$this->db->where('id_siswa', $id);
		$this->db->update('tabel_kelulusan', $data); 
	}
	public function hapus($id)
	{
		$this->db->where('id_penempatan', $id);
		$this->db->delete('tabel_siswakelas'); 
	}
	public function hapus_penempatan($id) {
		$this->db->where('id_siswa', $id);
		$this->db->delete('tabel_siswakelas');
	}
}