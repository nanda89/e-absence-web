<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_tahunajaran extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $sql = "SELECT * FROM tabel_tahunajaran
				ORDER BY tahun_ajaran asc";
        return $this->db->query($sql);
    }

    public function get_all_by_status($status)
    {
        $sql = "SELECT * FROM tabel_tahunajaran WHERE status = '$status'
				ORDER BY tahun_ajaran asc";
        return $this->db->query($sql);
    }

    public function get_last()
    {
        $sql = "SELECT * FROM tabel_tahunajaran
				ORDER BY tahun_ajaran desc limit 1";
        return $this->db->query($sql);
    }

    public function get_one($id)
    {
        $sql = "SELECT * FROM tabel_tahunajaran
				WHERE id_ta = '$id'";
        return $this->db->query($sql);
    }

    public function tambah($data)
    {
        $query = $this->db->insert('tabel_tahunajaran', $data);
    }

    public function edit($id, $data)
    {
        $this->db->where('id_ta', $id);
        $this->db->update('tabel_tahunajaran', $data);
    }

    public function hapus($id)
    {
        $this->db->where('id_ta', $id);
        $this->db->delete('tabel_tahunajaran');
    }
}
