<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_non_guru extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_non_guru
				ORDER BY tabel_non_guru.id_non_guru asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_non_guru
				WHERE id_non_guru = '$id'";
		return $this->db->query($sql);
	}	
	public function get_one_non_guru($id)
	{
		$sql = "SELECT * FROM tabel_non_guru INNER JOIN tabel_jenjang
				ON tabel_non_guru.id_jenjang=tabel_jenjang.id_jenjang				
				WHERE tabel_non_guru.id_jenjang = '$id'";
		return $this->db->query($sql);
	}	
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_non_guru',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_non_guru', $id);
		$this->db->update('tabel_non_guru', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_non_guru', $id);
		$this->db->delete('tabel_non_guru'); 
	}
	function remove_checked() 
    {
	 	$delete = $this->input->post('item');
 		for ($i=0; $i < count($delete) ; $i++) 
 		{ 
 			$this->db->where('id_non_guru', $delete[$i]);
 			$this->db->delete('tabel_non_guru');
 		}
	}
	 
}