<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_form_bayar extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$sql = "SELECT * FROM tabel_biaya INNER JOIN tabel_jenjang
				ON tabel_biaya.id_jenjang = tabel_jenjang.id_jenjang
				ORDER BY biaya asc";
		return $this->db->query($sql);
	}

	public function get_one($id)
	{
		$sql = "SELECT * FROM tabel_biaya
				WHERE id_biaya = '$id'";
		return $this->db->query($sql);
	}	
	public function get_riwayat($id)
	{
		$sql = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya
				ON tabel_bayar.id_biaya = tabel_biaya.id_biaya
				INNER JOIN tabel_siswakelas
				ON tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan
				INNER JOIN tabel_siswa
				ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
				WHERE tabel_bayar.id_penempatan = '$id'
				ORDER BY tabel_bayar.tgl_bayar ASC";
		return $this->db->query($sql);
	}	
	public function get_cetak($id_siswa,$tanggal)
	{
		$sql = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya ON tabel_bayar.id_biaya = tabel_biaya.id_biaya
				INNER JOIN tabel_siswakelas ON tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan
				INNER JOIN tabel_siswa ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
				INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
				INNER JOIN tabel_jenjang ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang
				INNER JOIN tabel_tahunajaran ON tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta
				WHERE tabel_siswakelas.id_siswa = '$id_siswa' AND tabel_siswakelas.status = 'aktif'
				AND tabel_bayar.tgl_bayar = '$tanggal'
				ORDER BY tabel_bayar.tgl_bayar ASC";
		return $this->db->query($sql);
	}	
	public function get_riwayat_one($id,$biaya)
	{
		$sql = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya
				ON tabel_bayar.id_biaya = tabel_biaya.id_biaya
				INNER JOIN tabel_siswakelas
				ON tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan
				WHERE tabel_bayar.id_penempatan = '$id' AND tabel_bayar.id_biaya = '$biaya'";
		return $this->db->query($sql);
	}
	public function get_riwayat_bln($id,$bln,$biaya,$thn)
	{
		$sql = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya
				ON tabel_bayar.id_biaya = tabel_biaya.id_biaya
				INNER JOIN tabel_siswakelas
				ON tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan
				WHERE tabel_bayar.id_penempatan = '$id' AND tabel_bayar.id_biaya = '$biaya' 
				AND LEFT(tabel_bayar.tgl_bayar,2) = '$bln'
				AND RIGHT(tabel_bayar.tgl_bayar,4) = '$thn'";
		return $this->db->query($sql);
	}
	public function get_riwayat_sms($id,$biaya,$tahun)
	{
		$sql = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya
				ON tabel_bayar.id_biaya = tabel_biaya.id_biaya
				INNER JOIN tabel_siswakelas
				ON tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan
				WHERE tabel_bayar.id_penempatan = '$id' AND tabel_bayar.id_biaya = '$biaya'
				AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'";
		return $this->db->query($sql);
	}

	public function get_tunggakan($id_jenjang,$id_tahun)
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran				
				 FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)					
			    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun'
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}	
	public function get_laporan($id_jenjang,$id_tahun,$bulan,$tahun)
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_jenjang.jenjang,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran,	
				tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
				LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)					
			    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun' AND tabel_siswakelas.status='aktif'
			    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}
	public function get_laporan1($bulan,$tahun)
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_kelas.tingkat,
				tabel_jenjang.jenjang,
				tabel_tahunajaran.tahun_ajaran,	
				tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
				LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
			    WHERE tabel_siswakelas.status='aktif'
			    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}
	public function get_laporan2($tahun)
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_kelas.tingkat,
				tabel_jenjang.jenjang,
				tabel_tahunajaran.tahun_ajaran,	
				tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
				LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
			    WHERE tabel_siswakelas.status='aktif'
			    AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}
	public function get_laporan3($id_jenjang,$bulan,$tahun)
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_kelas.tingkat,
				tabel_jenjang.jenjang,
				tabel_tahunajaran.tahun_ajaran,	
				tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)			
				LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_siswakelas.id_jenjang)		
			    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_kelas.status='aktif'
			    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}	
	public function get_tunggakan_all()
	{
		$sql = "SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_bayar.id_bayar,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran,
				tabel_biaya.biaya,
				tabel_biaya.nominal,
				tabel_bayar.tgl_bayar				
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)								    
				RIGHT JOIN tabel_bayar ON (tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_biaya.id_biaya = tabel_bayar.id_biaya)
			    ORDER BY tabel_siswa.id_siswa ASC";
		return $this->db->query($sql);
	}
	
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_bayar',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_biaya', $id);
		$this->db->update('tabel_biaya', $data); 
	}
	function hapus_riwayat($id)
	{
		$sql = "DELETE FROM tabel_bayar
				WHERE id_bayar = '$id'";
		return $this->db->query($sql);
	}
	function hapus($id)
	{
		$this->db->where('id_biaya', $id);
		$this->db->delete('tabel_biaya'); 
	}
	function hapus_bayar($id)
	{
		$this->db->where('id_bayar', $id);
		$this->db->delete('tabel_bayar'); 
	}
}