<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_jenjang extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $sql = "SELECT * FROM tabel_jenjang
				ORDER BY id_jenjang asc";
        return $this->db->query($sql);
    }

    public function get_all_kelas()
    {
        $sql = "SELECT * FROM tabel_kelas
				LEFT JOIN tabel_guru ON (tabel_kelas.id_guru = tabel_guru.id_guru)
				INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)
                INNER JOIN tabel_tahunajaran ON tabel_tahunajaran.id_ta = tabel_kelas.id_ta
				ORDER BY tabel_jenjang.jenjang,tabel_kelas.id_ta asc";
        return $this->db->query($sql);
    }

    public function get_all_kelas_by_last_ta()
    {
        $sql = "SELECT * FROM tabel_kelas
				LEFT JOIN tabel_guru ON (tabel_kelas.id_guru = tabel_guru.id_guru)
				INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)
				WHERE tabel_kelas.id_ta in(select MAX(id_ta) from tabel_tahunajaran)";
        return $this->db->query($sql);
    }

    public function get_one($id)
    {
        $sql = "SELECT * FROM tabel_jenjang
				WHERE id_jenjang = '$id'";
        return $this->db->query($sql);
    }
    public function get_one_jenjang($id)
    {
        $sql = "SELECT DISTINCT(tabel_kelas.kelas),tabel_jenjang.* FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id'";
        return $this->db->query($sql);
    }
    public function get_kelas($id)
    {
        $sql = "SELECT tabel_kelas.*,tabel_jenjang.* FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id' order by tabel_kelas.kelas";
        return $this->db->query($sql);
    }

    public function getKelasByJenjangAndTa($idJenjang, $idTA)
    {
        $sql = "SELECT tabel_kelas.*,tabel_jenjang.* FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$idJenjang' and id_ta ='$idTA' order by tabel_kelas.kelas";
        return $this->db->query($sql);
    }

    public function get_one_kelas($id)
    {
        $sql = "SELECT * FROM tabel_kelas
				LEFT JOIN tabel_guru ON (tabel_kelas.id_guru = tabel_guru.id_guru)
				INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)
				WHERE tabel_kelas.id_kelas = '$id'";
        return $this->db->query($sql);
    }

    public function get_kelas_by_id($id)
    {
        $sql = "SELECT * FROM tabel_kelas WHERE id_kelas = '$id'";
        return $this->db->query($sql);
    }

    public function tambah($data)
    {
        $query = $this->db->insert('tabel_jenjang', $data);
    }

    public function tambah_kelas($data)
    {
        $query = $this->db->insert('tabel_kelas', $data);
    }

    public function edit($id, $data)
    {
        $this->db->where('id_jenjang', $id);
        $this->db->update('tabel_jenjang', $data);
    }

    public function edit_kelas($id, $data)
    {
        $this->db->where('id_kelas', $id);
        $this->db->update('tabel_kelas', $data);
    }

    public function hapus($id)
    {
        $this->db->where('id_jenjang', $id);
        $this->db->delete('tabel_jenjang');
    }
    public function hapus_kelas($id)
    {
        $this->db->where('id_kelas', $id);
        $this->db->delete('tabel_kelas');
    }
}
