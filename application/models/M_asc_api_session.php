<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_api_session extends Asc_base_model {

    

    public function __construct()
    {
        parent::__construct();
        $this->table = "asc_api_session";
    }

    function login($user)
    {
        $user = (array)$user;
        // hapus semua session
        $this->db->where("id_user", $user['id_user']);
        $this->db->delete($this->table);

        // detail user
        $detail = $this->user_detail($user);
        if ( !$detail )
            return null;

        // buat session baru
        $token = $this->generate_token($user['password']);
        $data = array(
            "level" => $user["level"],
            "token" => $token
        );
        if ($user['level'] == 'guru')
            $data["id_user"] = $detail['id_guru'];
        else if ($user['level'] == 'siswa')
            $data["id_user"] = $detail['id_siswa'];
        else
            $data["id_user"] = $user['id_user'];
        $this->db->insert($this->table, $data);

        $user['token'] = $token;
        $data = array_merge($user, $detail);
        if ($user['level'] == 'siswa')
        {
            $this->db->select("k.kelas");
            $this->db->from("tabel_siswakelas sk");
            $this->db->join("tabel_kelas k", "k.id_kelas=sk.id_kelas");
            $this->db->where("sk.id_siswa", $data['id_siswa']);
            $result = $this->db->get();
            $data['kelas'] = $result->row()->kelas;
            unset($data['nama']);
        }
        elseif ($user['level'] == 'guru')
        {
            $this->db->select("m.mapel");
            $this->db->from("tabel_pengampu p");
            $this->db->join("tabel_mapel m", "m.id_mapel=p.id_mapel");
            $this->db->where("p.nik", $data['nik']);
            $result = $this->db->get();
            $data['mapel'] = $result->row()['mapel'];
        }
        unset($data['password']);
        unset($data['username']);
        unset($data['id_cabang']);
        return $data;
    }

    private function user_detail($user)
    {
        $tbl = 'tabel_guru';
        $col = 'nik';
        if ( $user['level'] == 'siswa' )
        {
            $tbl = 'tabel_siswa';
            $col = 'nisn';
        }
        $q = $this->db->from($tbl)
                ->where($col, $user['username'])
                ->get();
        if ( $q->num_rows() > 0 )
        {
            return (array) $q->row();
        }
        return false;
    }

    private function generate_token($password)
    {
        return $this->uuid->v5($password);
    }
}
