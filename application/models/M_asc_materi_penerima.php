<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_materi_penerima extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_materi_penerima';
    }

    public function list_by_siswa($id_siswa, $where=null, $search = null)
    {
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( $search != null )
        {
            foreach ($search as $column => $value) {
                $this->db->like($column, $value, 'both');
            }
        }
        $query = $this->db->select("m.id_materi, i.file, p.mapel, i.judul, i.materi, i.gambar, i.suara, g.nama as guru, i.created")
                    ->from($this->table . " m")
                    ->join("tabel_siswakelas s","s.id_kelas=m.id_kelas", "left")
                    ->join("asc_materi i","i.id_materi=m.id_materi", "inner")
                    ->join("tabel_mapel p","p.id_mapel=i.id_mapel", "inner")
                    ->join("tabel_guru g", " g.id_guru=i.id_guru", "inner")
                    ->where("s.id_siswa", $id_siswa)
                    ->get();
        return $query->result_array();
    }

    public function list_kelas($where)
    {
        $query = $this->db->from($this->table . ' p')
                        ->select("p.id_materi_penerima, k.id_kelas, k.kelas")
                        ->join("tabel_kelas k","k.id_kelas=p.id_kelas","inner")
                        ->where($where)
                        ->group_by("p.id_kelas")
                        ->get();
        return $query->result_array();
    }

    protected function init_rule()
    {
        $this->form_validation->set_rules('penerima', 'Penerima', 'required');
    }

    protected function process($param = null, $use_param_only = false)
    {
        $data = $param;
        if ( !$use_param_only )
        {
            $penerima = $this->input->post('penerima');
            $data['id_materi'] = $this->input->post('id_materi');
            $data['type'] = $this->input->post('type');
            if ( $data['type'] == 'kelas' )
            {
                $data['id_kelas'] = $penerima;
            }
            else
            {
                $data['id_siswa'] = $penerima;
            }
            if ( $param != null )
            {
                $data = array_merge($data, $param);
            }
        }
        $where = array(
            "id_materi" => $data["id_materi"],
        );
        if ( $data['type'] == 'kelas' )
        {
            $where['id_kelas'] = $data['id_kelas'];
        }
        else
        {
            $where['id_siswa'] = $data['id_siswa'];
        }
        $penerima = $this->get($where, true);
        if ( $penerima != null )
        {
            $this->set_error("Penerima sudah terdaftar.");
            return null;
        }

        return $data;
    }
}
