<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_asc_install extends CI_Model {
    private $dir = FCPATH."data/sql/";

	public function __construct() {		
		parent::__construct();		
	}

    public function getAll() {
        return $this->getData();
    }

    private function getData($where = null) {
        $obj = json_decode(file_get_contents($this->dir."query.json"), true);
        if (count($obj) > 0) {
            $var = array();
            foreach($obj as $i) {
                if ($where != null) {
                    foreach($where as $key) {
                        if ($key['status'] == $i['status'])
                            array_push($var, $i);
                    }
                } else
                    array_push($var, $i);
            }
            return $var;
        }
        return false;
    }

    public function updateData($data) {
        $obj = $this->asci->getNotInstalled();
        $x=1; $status = false;
        foreach($obj as $i) {
            if ($i['version'] == $data['version'] && $i['batch'] == $data['batch'] && $i['sql'] == $data['sql']) {
                $array_query = $this->getQuery($i['sql'], $i['type']);
                foreach($array_query as $query) {
                    try {
                        if (!empty(trim($query))) {
                            if ($i["type"] == "trigger") {
                                $this->db->query(strpos($query, "DROP") ? $query : $query."END"); $x++; $status=true;
                            } else {
                                $this->db->query($query); $x++; $status=true;
                            }
                        }
                    } catch (Exception $e) { $status = false; break; }
                }
            }
        }
        $this->updateJson($data);
        $msg = $status ? "{$x} query executed." : "Error";
        $var = array(
            "message" => $msg,
            "status" => $status
        );
        return $var;
    }

    private function updateJson($data) {
        $obj = $this->getAll();
        $var = array();
        foreach($obj as $i) {
            if ($i['version'] == $data['version'] && $i['batch'] == $data['batch'] && $i['sql'] == $data['sql']) { $i['status'] = true; }
            array_push($var, $i);
        }
        $f = fopen($this->dir."query.json", "w+");
        fwrite($f, json_encode($var, JSON_PRETTY_PRINT));
        return true;
    }

    private function getQuery($sql, $type) {
        $isi_file = file_get_contents($this->getSqlPath($sql));
        $string_query = rtrim( $isi_file, '\n;' );
        $array_query = $type == "trigger" ? explode('END;', $string_query) : explode(';', $string_query);
        return $array_query;
    }

    private function getSqlPath($sql) { return $this->dir.$sql; }

    public function getNotInstalled(){
        return $this->getData(array(
            "installed" => false
        ));
    }
}
