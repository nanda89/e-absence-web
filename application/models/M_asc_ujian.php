<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asc_ujian extends Asc_base_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'asc_ujian';
    }

    public function publish($ujian)
    {
        $where = array(
            'id_ujian' => $ujian['id_ujian'],
            'id_guru' => $ujian['id_guru']
        );
        $query = $this->db->from('asc_pertanyaan')
                            ->where($where)
                            ->get();
        $jumlah_pertanyaan = $query->num_rows();
        if ( $jumlah_pertanyaan > 0 )
        {
            $ujian['doc_status'] = $this->config->item('doc_status_publish');
            $ujian['kode'] = $this->generate_kode($ujian["id_guru"], $ujian["id_mapel"], $ujian["tipe"]);
            $ujian['jumlah_pertanyaan'] = $jumlah_pertanyaan;
            unset($ujian['id_ujian']);
            unset($ujian['id_guru']);
            if ( $this->save($ujian, $where) )
            {
                return true;
            }
        }
        $this->set_error("Ujian tidak memiliki pertanyaan");
        return false;
    }

    protected function process($param = null, $use_param_only = false)
    {
        $data = $param;
        if ( $use_param_only == false )
        {
            $data['judul'] = $this->input->post('judul');
            $data['deskripsi'] = $this->input->post('deskripsi');
            $data['id_mapel'] = $this->input->post('mapel');
            $data['doc_status'] = $this->input->post('status');
            $data['tipe'] = $this->input->post('tipe');
            $data['jumlah_pilihan'] = $this->input->post('jumlah_pilihan');
            if ( $param != null )
            {
                $data = array_merge($data, $param);
            }
        }
        return $data;
    }

    public function list_by($id_guru, $where = null, $key = null)
    {
        $this->db->select("u.id_ujian, u.kode, m.id_mapel, m.mapel, u.tipe, u.judul, u.deskripsi, u.jumlah_pertanyaan, u.doc_status, u.jumlah_pilihan");
        if ( $where != null )
        {
            $this->db->where($where);
        }
        $this->db->where("id_guru", $id_guru);
        if ( $key != null )
        {
            $this->db->like("u.judul", $key, "both");
            $this->db->or_like("u.deskripsi", $key, "both");
        }
        $q = $this->db->from($this->table . " u")
                        ->join("tabel_mapel m", "m.id_mapel=u.id_mapel", "inner")
                        ->get();
        return $q->result_array();
    }

    protected function init_rule()
    {
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
        $this->form_validation->set_rules('mapel', 'Mata Pelajaran', 'required');
        $this->form_validation->set_rules('tipe', 'Tipe ujian', 'required');
        //$this->form_validation->set_rules('jumlah_pilihan', 'Jumlah Pilihan', 'required');
    }

    public function generate_kode($id_guru, $id_mapel, $tipe)
    {
        $q = $this->db->select("(max(right(kode,4)) +1) as kode")
                        ->from($this->table)
                        ->where("id_guru", $id_guru)
                        ->where("id_mapel", $id_mapel)
                        ->get();
        return "U" . substr($tipe,0,1) . $id_guru. $id_mapel . $q->row_array()["kode"];
    }
}