<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_absen extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all_wali_kelas($nik,$tgl)
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_guru.nama,tabel_siswakelas.id_kelas
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)
	        WHERE tabel_guru.nik = '$nik'
	        ORDER BY tabel_siswa.nama_lengkap ASC";
		return $this->db->query($sql);
	}

	public function get_all_absen_kelas($kelas)
	{
		$sql = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_guru.nama,tabel_siswakelas.id_kelas
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)	        
	        WHERE tabel_kelas.id_kelas = '$kelas'
	        ORDER BY tabel_siswa.nisn ASC";
		return $this->db->query($sql);
	}

	public function get_all_kelas($nik)
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
	        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
	        WHERE tabel_guru.nik = '$nik'
	        ORDER BY tabel_absen.id_absen DESC";
		return $this->db->query($sql);
	}

	public function get_all_mapel($nik,$hari)
	{
		$sql = "SELECT DISTINCT(tabel_kelas.id_kelas),tabel_jenjang.jenjang,tabel_kelas.kelas, tabel_mapel.mapel, 
				tabel_jadwal.jam_awal, tabel_jadwal.jam_akhir, tabel_jadwal.status,tabel_jadwal.id_jadwal
				FROM tabel_jadwal INNER JOIN tabel_jenjang
				ON tabel_jadwal.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_kelas
				ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas INNER JOIN tabel_guru
				ON tabel_jadwal.id_guru = tabel_guru.id_guru INNER JOIN tabel_mapel
				ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
				WHERE tabel_guru.nik = '$nik' AND SUBSTR(tabel_jadwal.hari,3) = '$hari'
				ORDER BY tabel_kelas.kelas,tabel_jadwal.jam_awal ASC";
		return $this->db->query($sql);
	}

	public function get_jam_mapel($nik,$hari,$jam)
	{
		$sql = "SELECT DISTINCT(tabel_kelas.id_kelas),tabel_jenjang.jenjang,tabel_kelas.kelas, tabel_mapel.mapel, 
				tabel_jadwal.jam_awal, tabel_jadwal.jam_akhir, tabel_jadwal.status,tabel_jadwal.id_jadwal
				FROM tabel_jadwal INNER JOIN tabel_jenjang
				ON tabel_jadwal.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_kelas
				ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas INNER JOIN tabel_guru
				ON tabel_jadwal.id_guru = tabel_guru.id_guru INNER JOIN tabel_mapel
				ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
				WHERE tabel_guru.nik = '$nik' AND SUBSTR(tabel_jadwal.hari,3) = '$hari' AND '$jam' BETWEEN tabel_jadwal.jam_awal AND tabel_jadwal.jam_akhir
				ORDER BY tabel_kelas.kelas,tabel_jadwal.jam_awal ASC";
		return $this->db->query($sql);
	}

	public function get_all_kelas_siswa($nik)
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
	        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
	        WHERE tabel_siswa.nisn = '$nik'
	        ORDER BY tabel_absen.id_absen DESC";
		return $this->db->query($sql);
	}

	public function get_all()
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
	        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)	               
	        ORDER BY tabel_absen.id_absen DESC";
		return $this->db->query($sql);
	}
	public function get_all_absen_mapel()
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_siswa.nisn,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen_mapel.tgl,tabel_absen_mapel.absen,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
					ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
	        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen_mapel
	        ON (tabel_siswakelas.id_penempatan = tabel_absen_mapel.id_penempatan)	               
	        ORDER BY tabel_absen_mapel.id_absen_mapel DESC";
		return $this->db->query($sql);
	}	
	public function get_laporan($id_kelas,$id_tahun,$tanggal)
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
	        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' AND tabel_absen.tgl = '$tanggal'
	        ORDER BY tabel_absen.id_absen DESC";
		return $this->db->query($sql);
	}
	public function get_laporan1($id_kelas,$id_tahun,$bulan,$tahun)
	{
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
	        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
	        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
	        WHERE tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' 
	        AND RIGHT(tabel_absen.tgl,4) = '$tahun' AND MID(tabel_absen.tgl,4,2) = '$bulan'
	        ORDER BY tabel_absen.id_absen DESC";
		return $this->db->query($sql);
	}
	public function tambah($data)
	{
		$query=$this->db->insert('tabel_absen',$data);
	}

	function edit($id, $data)
	{
		$this->db->where('id_absen', $id);
		$this->db->update('tabel_absen', $data); 
	}
	
	function hapus($id)
	{
		$this->db->where('id_absen', $id);
		$this->db->delete('tabel_absen'); 
	}
}