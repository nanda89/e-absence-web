<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Panel extends CI_Controller
{
    private $conf = null;

    public function __construct()
    {
        parent::__construct();
        //===== Load Database =====

        if ($this->conf == null) {
            require_once __DIR__ . '/../config/database.php';
            $this->conf = $db['default'];
        }
        $link = mysqli_connect($this->conf['hostname'], $this->conf['username'], $this->conf['password'], $this->conf['database']);
        if (mysqli_connect_errno()) {} else {
            $this->load->database();
        }
        $this->load->model('m_admin');
        $this->load->model('m_pesan');

        $this->load->library('upload');
        $this->load->helper('url');
    }

    protected function template($page, $data)
    {
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        } else {
            $this->load->view('t_panel/header', $data);
            $this->load->view("t_panel/aside");
            $this->load->view("panel/$page");
            $this->load->view('t_panel/footer');
        }
    }

    public function index()
    {
        $link = mysqli_connect($this->conf['hostname'], $this->conf['username'], $this->conf['password'], $this->conf['database']);
        if (mysqli_connect_errno()) {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "setting'>";
        } else {
            if ($this->m_admin->get_code() == 1) {
                $page = "login";
                $data['judul'] = "Dashboard";
                $data['isi'] = "dashboard";
                $this->load->view("panel/$page");
            } else {
                $r = md5("getcode");
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "getcode?" . $r . "'>";
            }
        }
    }

    public function auth()
    {
        $page = "login";
        $data['judul'] = "Dashboard";
        $this->load->view("panel/$page");
    }

    public function home()
    {
        /* update asc_install */
        $this->load->model('M_asc_install', "asci");
        $q = $this->asci->getNotInstalled();
        if (!$q) {
            $page = "index";
            $data['title'] = "Dashboard";
            $data['isi'] = "dashboard";
            $data['judul1'] = "Dashboard";
            $data['judul2'] = "Control Panel";
            $this->template($page, $data);
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "elearning/asc_install'>";
        }

    }

    public function getcode()
    {
        $page = "login";
        $data['judul'] = "Dashboard";
        $data['isi'] = "dashboard";
        $this->load->view("panel/$page");
    }

    public function profil()
    {
        $page = "profil";
        $data['title'] = "Profil";
        $data['judul1'] = "Profil";
        $data['isi'] = "profil";
        $data['judul2'] = "";
        $this->template($page, $data);
    }

    public function set_profil()
    {
        $id = $this->session->userdata('id_user');

        $config['upload_path'] = './assets/panel/icon/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1024';

        $this->upload->initialize($config);
        if ($this->upload->do_upload('avatar')) {
            $data['avatar'] = $this->upload->file_name;

            $one = $this->m_admin->get_one($id);
            $row = $one->row();
            unlink("assets/panel/icon/" . $row->avatar); //Hapus Gambar
        }

        $data['username'] = $this->input->post('username');
        $data['nama'] = $this->input->post('nama');
        $password = $this->input->post('password');
        if ($password != '') {
            $data['password'] = md5($password);
        }
        $this->m_admin->edit($id, $data);

        $m = $this->m_admin->get_one($id);
        $row = $m->row();
        $ses_loginadmin = array('username' => $row->username,
            'nama' => $row->nama,
            'level' => $row->level,
            'id_cabang' => $row->id_cabang,
            'id_user' => $row->id_user);
        $this->session->set_userdata($ses_loginadmin);

        $_SESSION['pesan'] = "Profil berhasil diubah!";
        $_SESSION['tipe'] = "info";
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel/profil'>";
    }

    public function umum()
    {
        $page = "umum";
        $data['title'] = "Pengaturan Umum";
        $data['judul1'] = "Pengaturan Umum";
        $data['isi'] = "umum";
        $data['judul2'] = "";
        $data['setting'] = $this->m_admin->get_setting();
        $this->template($page, $data);
    }

    public function set_setting()
    {
        $id = 1;

        $config['upload_path'] = './assets/panel/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|ico';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';

        $this->upload->initialize($config);
        if ($this->upload->do_upload('gambar')) {
            $data['logo_gambar'] = $this->upload->file_name;

            $one = $this->m_admin->get_setting();
            $row = $one->row();
        }
        $this->upload->initialize($config);
        if ($this->upload->do_upload('gambar1')) {
            $data['logo_favicon'] = $this->upload->file_name;

            $one = $this->m_admin->get_setting();
            $row = $one->row();
        }
        $this->upload->initialize($config);
        if ($this->upload->do_upload('logo_sekolah')) {
            $data['logo_sekolah'] = $this->upload->file_name;

            $one = $this->m_admin->get_setting();
            $row = $one->row();
        }
        $this->upload->initialize($config);
        if ($this->upload->do_upload('kop_surat')) {
            $data['kop_surat'] = $this->upload->file_name;

            $one = $this->m_admin->get_setting();
            $row = $one->row();
        }
        $this->upload->initialize($config);
        if ($this->upload->do_upload('ttd')) {
            $data['ttd'] = $this->upload->file_name;

            $one = $this->m_admin->get_setting();
            $row = $one->row();
        }

        $data['logo_mini'] = $this->input->post('logo_mini');
        $data['logo_besar'] = $this->input->post('logo_besar');
        $data['akun_gmail'] = $this->input->post('akun_gmail');
        $data['alamat'] = $this->input->post('alamat');
        $data['website'] = $this->input->post('website');
        $data['pimpinan'] = $this->input->post('pimpinan');
        $data['nik'] = $this->input->post('nik');
        $data['kartu_gsm'] = $this->input->post('kartu_gsm');
        $data['cek_pulsa'] = $this->input->post('cek_pulsa');
        $data['metode'] = $this->input->post('metode');
        $data['kode_aktivasi'] = $this->input->post('kode_aktivasi');
        $data['link_tujuan'] = $this->input->post('link_tujuan');
        $data['telat'] = $this->input->post('telat');
        $data['toleransi_jam_akhir'] = $this->input->post('toleransi_jam_akhir');
        $data['lat'] = $this->input->post('lat');
        $data['lng'] = $this->input->post('lng');
        $data['radius'] = $this->input->post('radius');
        // $data['jam_masuk'] = $this->input->post('jam_masuk');
        $data['jam_pulang'] = $this->input->post('jam_pulang');
        $data['status_absen_pulang'] = $this->input->post('status_absen_pulang');
        $data['kota'] = $this->input->post('kota');
        $data['kapasitas_kelas'] = $this->input->post('kapasitas');

        $this->m_admin->edit_setting($id, $data);

        $_SESSION['pesan'] = "Pengaturan berhasil diubah!";
        $_SESSION['tipe'] = "info";
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel/umum'>";
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $rs_login = $this->m_admin->login($username, $password);
        if ($rs_login->num_rows() == 1) {

            $row = $rs_login->row();
            $ses_loginadmin = array('username' => $row->username,
                'nama' => $row->nama,
                'level' => $row->level,
                'id_user' => $row->id_user);
            $this->session->set_userdata($ses_loginadmin);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel/home'>";
        } else {
            $_SESSION['pesan'] = "Login Gagal!";
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }
    }

    public function logout()
    {
        session_destroy();
        session_unset();
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
    }
}
