<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender_pendidikan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');	
		$this->load->model('m_jenjang');	
		$this->load->model('m_kalender');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer',$data);
	}

	public function index()
	{		
		$page 				= ( !empty($_GET['print']) ) ? "kalender/kalender_pendidikan_print" : "kalender/kalender_pendidikan";
		$data 				= [];
		$data['isi'] 		= "kalender_pendidikan";		
		$data['title'] 		= "Kalender Pendidikan";
		$data['judul1']		= "Kalender Pendidikan";
		$data['judul2']		= "";					
		$data['set'] 		= "view";
		$data['agenda'] 	= $this->get_list_agenda();
		$data['scripts'] 	= '$.WebAbsence.modul.kalender.cetak();';
		$data['selected_ta']= ( isset( $_GET['ta'] ) && !empty( $_GET['ta'] ) ) ? $_GET['ta'] : date('Y') . '-' . (intval(date('Y'))+1);
		$tgl 				= date('d-m-Y');
		$data['color_tb_border'] = '#777';
		$data['color_bg_head'] = '#47F269';
		$data['color_footer_semester'] = '#1B76FE';
		$data['color_bg_foot'] = '#97FE65';
		$stylesheets = '';
		$stylesheets .= '
		.kp-table, .kp-table tbody tr td {
			border: 1px solid '.$data['color_tb_border'].';
		  }
		';
		$stylesheets .= '
		.kp-table thead tr th {
			background-color: '.$data['color_bg_head'].';
			border: 1px solid '.$data['color_tb_border'].';
		  }
		';
		$stylesheets .= '
		.kp-table tr th, .kp-table tr td {
			vertical-align: middle !important;
		  }
		';
		$data['stylesheets']= $stylesheets;
		if( isset( $_GET['print'] ) ) {
			$this->load->view('panel/'.$page,$data);
		} else {
			$this->template($page, $data);
		}
	}

	protected function get_list_agenda($smt=1) 
	{
		// if ( $tgl !== false ) {
		// 	$agenda = 
		// } else {
		// 	$agenda	= $this->m_kalender->get_all()->row();	
		// }
		$y = date('Y');
		$start = ( $smt == 2 ) ? $y . '-06-01' : $y . '-01-01';
		$end = ( $smt == 2 ) ? $y . '-12-30' : $y . '-06-30';
		$sql = "SELECT * FROM tabel_kalender WHERE tanggal BETWEEN $start AND $end";

		return $this->db->query($sql)->row();
	}

	/**
	 * Tambahan kalender pendidikan
	 */
	protected function get_kalender_pendidikan($args=array()) {

		if ( !isset($args['tahun_ta']) || empty( $args['tahun_ta'] ) ) {
			$args['tahun_ta'] = ( intval(date('Y'))-1 );
		}
		$tahun_ta = substr($args['tahun_ta'], 0,4);
		$query_by = ( isset($args['query_by']) ) ? $args['query_by'] : 'ta'; // ta, range


		$tahun_ta_q = $tahun_ta-1 . '-' . $tahun_ta;
		$ta_query = $this->db->query("SELECT * FROM tabel_tahunajaran WHERE tahun_ajaran ='$tahun_ta_q'"); // format: 2017-2018
		$ta = $ta_query->row();
		$tahun = explode('-', $tahun_ta_q);
		$tahun = $tahun[1];
		$result = array();
		$nama_bulan = $this->get_nama_bulan();


		// get agenda
		$agenda = array();
		$between_start = isset( $args['start_date'] ) ? $args['start_date'] : "$tahun/01/01";
		$between_end = isset( $args['end_date'] ) ? $args['end_date'] : "$tahun/12/31";
		$agenda_q = $this->db->query("SELECT * FROM tabel_kalender WHERE tanggal BETWEEN '$between_start' AND '$between_end'");
		$list_agenda = $agenda_q->result();
		if ( count( $list_agenda ) > 0 ) {
			foreach ($list_agenda as $row) {
				$agenda[$row->tanggal] = array(
					'agenda'	=> $row->agenda,
					'ket'		=> $row->keterangan,
					'libur'		=> $row->libur
				);
			}
		}

		// return $agenda;

		$date = new \DateTime($args['start_date']);
		$end = new \DateTime($args['end_date']);
		$interval = $date->diff($end);
		$count = intval( $interval->format('%R%a') );
		$result = [];
		$tmp_result = [];
		if ( $count > 0 ) {
			for ($i=0; $i <= $count; $i++) { 

				$bulan_key = $date->format('n');
				if ( !isset( $result[$bulan_key] ) ) {
					$result[$bulan_key] = array();
				}

				$tmp_result[ $date->format('Y-m-d') ] = array(
					'agenda'	=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? $agenda[$date->format('Y-m-d')]['agenda'] : '',
					'ket'		=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? $agenda[$date->format('Y-m-d')]['ket'] : '',
					'libur'		=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? (bool)$agenda[$date->format('Y-m-d')]['libur'] : false
				);

                if ( $date->format('N') == 7 ) {
                  $tmp_result[ $date->format('Y-m-d') ]['libur'] = true;
                }

				$result[$bulan_key][] = array(
					'tanggal'	=> $date->format('Y-m-d'),
					'agenda'	=> $tmp_result
				);



				$date->add( new \DateInterval('P1D') );

				
			}
		}

		return $result;

		// begin result kp
		if ( $query_by == 'range' ) {

			$date = new \DateTime($args['start_date']);
			$end = new \DateTime($args['end_date']);
			$interval = $date->diff($end);
			$result = [];
			if ( $interval > 0 ) {
				$count = intval( $interval->format('%R%a') );
				for ($i=0; $i <= $count; $i++) { 

					$result[ $date->format('Y-m-d') ] = array(
						'agenda'	=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? $agenda[$date->format('Y-m-d')]['agenda'] : '',
						'ket'		=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? $agenda[$date->format('Y-m-d')]['ket'] : '',
						'libur'		=> ( isset( $agenda[$date->format('Y-m-d')] ) ) ? (bool)$agenda[$date->format('Y-m-d')]['libur'] : false
					);

	                if ( $date->format('N') == 7 ) {
	                  $result[ $date->format('Y-m-d') ]['libur'] = true;
	                }

					$date->add( new \DateInterval('P1D') );

					
				}
			}
			

		} else {

			for ($i=1; $i <= 12; $i++) { 
				$result[$i] = array(
					'tahun'	=> ( $i < 7 ) ? $tahun : $tahun-1,
					'bulan'	=> $nama_bulan[$i],
					'agenda'=> [],
					'hk'	=> 0,
					'hl'	=> 0,
					'he'	=> 0,
				);
	            $dt_param = $tahun . '-' . mb_strimwidth($i, 0, 2, '0') . '-01';
	            $m = new \DateTime($dt_param);
	            $bulan_key = strtolower($m->format('M'));
	            // $result[$i]['hk'] = 0;
	            // $result[$i]['hl'] = 0;
	            // $result[$i]['he'] = 0;
	            $jml_hari = cal_days_in_month(CAL_GREGORIAN, $i, $tahun);
				for ($j=1; $j <= $jml_hari; $j++) { 
	                $libur = false;
	                $date = new \DateTime( $result[$i]['tahun'] . '-' . $m->format('m') . '-' . mb_strimwidth($j, 0, 2, '0') );
	                $result[$i]['agenda'][$j] = '';
	                if ( $date->format('N') == 7 ) {
	                  $result[$i]['agenda'][$j] = 'M';
	                  $libur = true;
	                }
	                if ( isset( $agenda[ $date->format('Y-m-d') ]['agenda'] ) ) {
	                	$result[$i]['agenda'][$j] = $agenda[ $date->format('Y-m-d') ]['agenda'];
	                }

	                if ( $libur ) {
	                  $result[$i]['hl']++;
	                } else {
	                  $result[$i]['he']++;
	                }
					
				}
			}

		}

		return $result;
	}

	public function get_nama_bulan() {
		return array(
			1	=> 'Januari',
			2	=> 'Februari',
			3	=> 'Maret',
			4	=> 'April',
			5	=> 'Mei',
			6	=> 'Juni',
			7	=> 'Juli',
			8	=> 'Agustus',
			9	=> 'September',
			10	=> 'Oktober',
			11	=> 'November',
			12	=> 'Desember',
		);
	}
}
?>