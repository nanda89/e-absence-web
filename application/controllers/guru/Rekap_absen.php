<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_absen extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/guru/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "rekap_absen";
		$data['isi']    = "rekap_absen";		
		$data['title']	= "Rekap Absensi Kelas";			
		$data['judul1']	= "Rekap Absensi Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$tgl 					= date('d-m-Y');
		$name = $this->session->userdata('username');		
		$data['dt_wali_kelas'] = $this->m_absen->get_all_kelas($name);					
		$this->template($page, $data);	
	}
}
?>