<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_siswa');
        $this->load->model('m_jenjang');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_penempatan');
        $this->load->model('m_siswakelas');
        $this->load->model('m_setting');
        $this->load->model('m_pesan');
        $this->load->model('m_semua');
        //===== Load Library =====
        $this->load->library('upload');
        $this->load->library('csvimport');
    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/guru/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "siswa";
        $data['isi'] = "siswa";
        $data['title'] = "Daftar Siswa";
        $data['judul1'] = "Daftar Siswa";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $this->template($page, $data);
    }
    public function add()
    {
        $page = "siswa";
        $data['title'] = "Siswa";
        $data['judul1'] = "Tambah Data Siswa";
        $data['judul2'] = "";
        $data['isi'] = "siswa";
        $data['set'] = "insert";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $this->template($page, $data);
    }
    public function save()
    {
        if ($this->input->post('save') == 'save') {
            //setting konfigurasi upload gambar
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('gambar')) {
                $gambar = "";
            } else {
                $gambar = $this->upload->file_name;
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['id_siswa'] = $this->input->post('id_siswa');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nisn'] = $this->input->post('nisn');
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $this->input->post('alamat');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['asal_sekolah'] = $this->input->post('asal_sekolah');
            $data['ket_asal'] = $this->input->post('ket_asal');
            $data['nama_ayah'] = $this->input->post('nama_ayah');
            $data['nama_ibu'] = $this->input->post('nama_ibu');
            $data['tgl_daftar'] = $this->input->post('tgl_daftar');
            $data['gambar'] = $gambar;

            $da['id_siswa'] = $this->input->post('id_siswa');
            $da['id_ta'] = $this->input->post('id_ta');
            $da['id_kelas'] = $this->input->post('id_kelas');

            $setting = $this->m_setting->get_one();
            $setting = $setting->row();
            $kapasitas = $setting->kapasitas_kelas;
            $totalSiswa = $this->m_siswakelas->countKelas($da['id_kelas']);

            if (intval($totalSiswa) > intval($kapasitas)) {

                $_SESSION['pesan'] = "Error : data gagal disimpan : <br/> melebihi kapasitas kelas";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "guru/siswa'>";

            } else {
                $this->m_siswa->tambah($data);
                $this->m_penempatan->tambah($da);

                $_SESSION['pesan'] = "Berhasil tersimpan!";
                $_SESSION['tipe'] = "info";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "guru/siswa'>";

            }

        }
    }
    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');

        if ($set == 'detail') {
            $page = "detail_siswa";
            $data['one_siswa'] = $this->m_siswa->get_one($id);

            $data['title'] = "Siswa";
            $data['judul1'] = "Detail Data Siswa";
            $data['isi'] = "siswa";
            $data['judul2'] = "";
            $data['set'] = "detail";
            $this->template($page, $data);
        } else if ($set == 'ubah') {
            $page = "edit_siswa";
            $data['one_siswa'] = $this->m_siswa->get_one($id);
            $data['dt_jenjang'] = $this->m_jenjang->get_all();
            $data['dt_ta'] = $this->m_tahunajaran->get_all();
            $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
            $data['title'] = "Siswa";
            $data['judul1'] = "Edit Data Siswa";
            $data['isi'] = "siswa";
            $data['judul2'] = "";
            $data['set'] = "edit";
            $this->template($page, $data);
        } elseif ($set == 'edit') {
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $data['gambar'] = $this->upload->file_name;

                $one = $this->m_siswa->get_one($id);
                $row = $one->row();
                if ($row->gambar != '') {
                    unlink("assets/panel/images/" . $row->gambar); //Hapus Gambar
                }
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['id_siswa'] = $this->input->post('id_siswa');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nisn'] = $this->input->post('nisn');
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $this->input->post('alamat');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['asal_sekolah'] = $this->input->post('asal_sekolah');
            $data['ket_asal'] = $this->input->post('ket_asal');
            $data['nama_ayah'] = $this->input->post('nama_ayah');
            $data['nama_ibu'] = $this->input->post('nama_ibu');
            $data['tgl_daftar'] = $this->input->post('tgl_daftar');
            $this->m_siswa->edit($id, $data);

            $da['id_siswa'] = $this->input->post('id_siswa');
            $da['id_ta'] = $this->input->post('id_ta');
            $da['id_kelas'] = $this->input->post('id_kelas');

            // cek kapasitas kelas tujuan,jika melebihi maka ditolak
            $setting = $this->m_setting->get_one();
            $setting = $setting->row();
            $kapasitas = $setting->kapasitas_kelas;
            $totalSiswa = $this->m_siswakelas->countKelas($da['id_kelas']);

            // var_dump("totalSiswa : " . $totalSiswa . " dari kelas : " . $da['id_kelas'] . " sedangkan kapasitas " . $kapasitas);
            $cek = $this->db->query("SELECT * FROM tabel_siswakelas WHERE id_siswa = '$id'")->num_rows();
            if ($cek == 0) {
                if (intval($totalSiswa) > intval($kapasitas)) {

                    $_SESSION['pesan'] = "Error : proses edit kelas gagal : <br/> kelas tujuan melebihi kapasitas";
                    $_SESSION['tipe'] = "danger";

                } else {
                    $_SESSION['pesan'] = "Success : Data berhasil ditambahkan : <br/> data siswa berhasil ditambahkan";
                    $_SESSION['tipe'] = "info";

                    $this->m_penempatan->tambah($da);
                }

            } else {
                if (intval($totalSiswa) > intval($kapasitas)) {

                    $_SESSION['pesan'] = "Error : proses edit kelas gagal : <br/> kelas tujuan melebihi kapasitas";
                    $_SESSION['tipe'] = "danger";

                } else {
                    $this->m_penempatan->edit($id, $da);
                    $_SESSION['pesan'] = "Success : Data berhasil diubah : <br/> data siswa berhasil diubah";
                    $_SESSION['tipe'] = "info";

                }

            }
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "guru/siswa'>";

        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "guru/siswa'>";
        }

    }
}
