<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_siswa extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/guru/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "rekap_siswa";
		$data['isi']    = "rekap_siswa";		
		$data['title']	= "Rekap Absensi Siswa";			
		$data['judul1']	= "Rekap Absensi Siswa";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$tgl 					= date('d-m-Y');
		$name = $this->session->userdata('username');		
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_siswa.nisn,tabel_jenjang.jenjang,
			tabel_kelas.kelas,
			tabel_tahunajaran.tahun_ajaran,tabel_absen_mapel.tgl,tabel_absen_mapel.absen,tabel_guru.nama,tabel_mapel.mapel
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
				ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen_mapel
        ON (tabel_siswakelas.id_penempatan = tabel_absen_mapel.id_penempatan) INNER JOIN tabel_jadwal
        ON (tabel_guru.id_guru = tabel_jadwal.id_guru) INNER JOIN tabel_mapel
        ON (tabel_jadwal.id_mapel = tabel_mapel.id_mapel)	                       				
				ORDER BY tabel_absen_mapel.id_absen_mapel DESC";
		$data['dt_wali_kelas'] 	= $this->db->query($sql);					
		$this->template($page, $data);	
	}
}
?>