<?php
function hari(){
  $hari=$day=gmdate("l", time()+60*60*7);		
  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }      
  $hariLengkap="$hari";
  return $hariLengkap;
}

defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_siswa extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/guru/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "absen_siswa_awal";
		$data['isi']    = "absen_siswa";		
		$data['title']	= "Absensi Siswa";			
		$data['judul1']	= "Absensi Siswa";			
		$data['judul2']	= "";					
		$data['set']		= "view";
		$tgl 			= gmdate("d-m-Y", time()+60*60*7);
    $jam  		= gmdate("H:i", time()+60*60*7);
		$hari 			= hari();		
		$name = $this->session->userdata('username');		
		$data['dt_wali_kelas'] = $this->m_absen->get_all_wali_kelas($name,$tgl);					
		//$data['dt_mapel_kelas'] = $this->m_absen->get_all_mapel($name,$hari);
		$data['dt_mapel_kelas'] = $this->m_absen->get_jam_mapel($name,$hari,$jam);
		$data['hari'] = $hari;					
		$this->template($page, $data);	
	}
	public function kelas()
	{		
		$page			= "absen_siswa";
		$data['isi']    = "absen_siswa";		
		$data['title']	= "Absensi Siswa";			
		$data['judul1']	= "Absensi Siswa";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$tgl 			= gmdate("d-m-Y", time()+60*60*7);
		$hari 			= hari();		
		$name = $this->session->userdata('username');		
		$kelas = $this->uri->segment(4);		
		$data['dt_absen_kelas'] = $this->m_absen->get_all_absen_kelas($kelas);					
		$data['dt_mapel_kelas'] = $this->m_absen->get_all_mapel($name,$hari);
		$data['hari'] = $hari;					
		$this->template($page, $data);	
	}
	public function ket(){
		$v						= $this->input->get("v");
		$g						= $this->input->get("g");
		$tgl  					= gmdate("d-m-Y", time()+60*60*7);		
		$id_penempatan			= $this->input->get("id");
		$data['id_penempatan']	= $this->input->get("id");
		$data['tgl'] 			= gmdate("d-m-Y", time()+60*60*7);		
		if($g=="masuk"){
			$data['absen_masuk']	= $this->input->get("v");
			$data['jam_masuk'] 		= gmdate("H:i", time()+60*60*7);
			$cek = "SELECT COUNT(id_absen) as jum from tabel_absen where id_penempatan = '$id_penempatan' and tgl = '$tgl'";
			$r = $this->db->query($cek);
			$w = $r->row();
			if($w->jum==0){
				$this->m_absen->tambah($data);
			}else{
				$isi = $this->db->query("SELECT * from tabel_absen where id_penempatan = '$id_penempatan' and tgl = '$tgl'")->row();
				if($isi->absen_masuk==""){
					$id = $isi->id_absen;
					$this->m_absen->edit($id,$data);	
				}else{
					$id = $isi->id_absen;
					$this->m_absen->edit($id,$data);
				}
			}								
		}elseif($g=="pulang"){
			$data['absen_pulang']	= $this->input->get("v");
			$data['jam_pulang'] 	= gmdate("H:i", time()+60*60*7);
			$cek = "SELECT COUNT(id_absen) as jum from tabel_absen where id_penempatan = '$id_penempatan' and tgl = '$tgl'";
			$r = $this->db->query($cek);
			$w = $r->row();
			if($w->jum==0){
				$this->m_absen->tambah($data);
			}else{
				$isi = $this->db->query("SELECT * from tabel_absen where id_penempatan = '$id_penempatan' and tgl = '$tgl'")->row();
				if($isi->absen_pulang==""){
					$id = $isi->id_absen;
					$this->m_absen->edit($id,$data);	
				}else{
					$id = $isi->id_absen;
					$this->m_absen->edit($id,$data);
				}
			}
		}
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";		
	}
	public function validasi_masuk(){
		$tgl2 = gmdate("d-m-Y", time()+60*60*7);		
		$id_kelas = $_GET['id'];
		$cek = $this->db->query("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas,
                    tabel_siswa.email,tabel_siswakelas.id_penempatan FROM tabel_siswakelas INNER JOIN tabel_siswa 
                    ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
                    WHERE tabel_siswakelas.id_kelas='$id_kelas' AND tabel_siswakelas.id_penempatan IN (SELECT id_penempatan FROM tabel_absen
                    WHERE tabel_absen.tgl='$tgl2')");
		$c=$cek->num_rows();
	    if($c>0){
		    foreach ($cek->result() as $periksa) {
		    	$id_s = $periksa->id_penempatan;		    	
		    	$this->db->query("UPDATE tabel_absen SET sms_masuk = '-',valid_masuk = 'valid' WHERE 
		    		id_penempatan='$id_s' AND tgl='$tgl2'");
		  	}
		}
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";		
	}
	public function validasi_pulang(){
		$tgl2 = gmdate("d-m-Y", time()+60*60*7);		
		$id_kelas = $_GET['id'];
		$cek = $this->db->query("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas,
                    tabel_siswa.email,tabel_siswakelas.id_penempatan FROM tabel_siswakelas INNER JOIN tabel_siswa 
                    ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
                    WHERE tabel_siswakelas.id_kelas='$id_kelas' AND tabel_siswakelas.id_penempatan IN (SELECT id_penempatan FROM tabel_absen
                    WHERE tabel_absen.tgl='$tgl2')");
		$c=$cek->num_rows();
	    if($c>0){
		    foreach ($cek->result() as $periksa) {
		    	$id_s = $periksa->id_penempatan;		    	
		    	$this->db->query("UPDATE tabel_absen SET sms_pulang = '-',valid_pulang = 'valid' WHERE id_penempatan='$id_s' AND tgl='$tgl2'");
		  	}
		}
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";		
	}
	public function notif_masuk(){
		$tgl2 = gmdate("d-m-Y", time()+60*60*7);		
		$id_kelas = $_GET['id'];
		$cek = $this->db->query("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas,
                    tabel_siswa.email FROM tabel_siswakelas INNER JOIN tabel_siswa 
                    ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
                    WHERE tabel_siswakelas.status='aktif' AND tabel_siswakelas.id_kelas='$id_kelas' AND tabel_siswakelas.id_penempatan NOT IN (SELECT id_penempatan FROM tabel_absen
                    WHERE tabel_absen.tgl='$tgl2' AND tabel_absen.absen_masuk<>'')");
		$c=$cek->num_rows();
	    if($c>0){
		    foreach ($cek->result() as $periksa) {
		      $nama = $periksa->nama_lengkap;
		      $kelas = $periksa->kelas;
		      $email = $periksa->email;

				$s = "SELECT * FROM tabel_setting WHERE id_setting=1";
				$ys = $this->db->query($s);
				$ce = $ys->row();
				require_once(APPPATH.'libraries/PHPMailerAutoload.php');
							
				$pesan	= $ce->isi_pesan_absen;
				$akun	= $ce->akun_gmail;
				$akun_webmail	= $ce->akun_webmail;					
				$pass_webmail	= $ce->pass_webmail;
				$host			= $ce->host_webmail;			
					
			 	$mail = new PHPMailer;
			
				 $mail->isSMTP();
				 $mail->Host = 'mail.'.$host.''; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
				 $mail->SMTPAuth = true;
				 $mail->Username = $akun_webmail; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
				 $mail->Password = $pass_webmail; //masukan kata sandi
				 $mail->Port = 587; //port tidak usah di ubah, biarkan 587
				
				 $mail->setFrom($akun, 'Pengingat'); //email pengirim
				 $mail->addAddress($email, 'penerima'); //email penerima
				 $mail->addReplyTo($email, 'pengingat');
				 $mail->isHTML(true);
				
			
					
				        ///atur pesan email disini
				 $mail->Subject = 'Reply';
				 $isiemail = $pesan;
				 $isiemail .= "Nama : ".$nama;
				 $isiemail .= "<br>Kelas : ".$kelas;
				 $mail->Body    = $isiemail;
				 $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				
				 if(!$mail->send()) {
				 	?>
						<script type="text/javascript">
							alert("Notifikasi telah terkirim ke email wali siswa");			
						</script>
					<?php
				 	echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";
				 } else {
				 	?>
						<script type="text/javascript">
							alert("Ada kesalahan dengan pengiriman notifikasi");			
						</script>
					<?php
				  	echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";
				 }
				 //echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";

			}
		}
	}
	public function notif_pulang(){
		$tgl2 = date('d-m-Y');
		$id_kelas = $_GET['id'];
		$cek = $this->db->query("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas,
                    tabel_siswa.email FROM tabel_siswakelas INNER JOIN tabel_siswa 
                    ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
                    WHERE tabel_siswakelas.status='aktif' AND tabel_siswakelas.id_kelas='$id_kelas' AND tabel_siswakelas.id_penempatan NOT IN (SELECT id_penempatan FROM tabel_absen
                    WHERE tabel_absen.tgl='$tgl2' AND tabel_absen.absen_pulang<>'')");
		$c=$cek->num_rows();
	    if($c>0){
		    foreach ($cek->result() as $periksa) {
		      $nama = $periksa->nama_lengkap;
		      $kelas = $periksa->kelas;
		      $email = $periksa->email;

				$s = "SELECT * FROM tabel_setting WHERE id_setting=1";
				$ys = $this->db->query($s);
				$ce = $ys->row();
				require_once(APPPATH.'libraries/PHPMailerAutoload.php');
							
				$pesan	= $ce->isi_pesan_absen;
				$akun	= $ce->akun_gmail;
				$akun_webmail	= $ce->akun_webmail;					
				$pass_webmail	= $ce->pass_webmail;
				$host			= $ce->host_webmail;			
					
			 	 $mail = new PHPMailer;
			
				 $mail->isSMTP();
				 $mail->Host = 'mail.'.$host.''; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
				 $mail->SMTPAuth = true;
				 $mail->Username = $akun_webmail; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
				 $mail->Password = $pass_webmail; //masukan kata sandi
				 $mail->Port = 587; //port tidak usah di ubah, biarkan 587
				
				 $mail->setFrom($akun, 'Pengingat'); //email pengirim
				 $mail->addAddress($email, 'penerima'); //email penerima
				 $mail->addReplyTo($email, 'pengingat');
				 $mail->isHTML(true);
				
			
					
				        ///atur pesan email disini
				 $mail->Subject = 'Reply';
				 $mail->Body    = $pesan;
				 $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				
				 if(!$mail->send()) {
				 	?>
						<script type="text/javascript">
							alert("Notifikasi telah terkirim ke email wali siswa");			
						</script>
					<?php
				 	echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";
				 } else {
				 	?>
						<script type="text/javascript">
							alert("Ada kesalahan dengan pengiriman notifikasi");			
						</script>
					<?php
				  	echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";
				 }
				 //echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";

			}
		}
	}
	public function get_kelas(){
		$id_jenjang	= $this->input->post('id_jenjang');
		$tingkat	= $this->input->post('tingkat');
		$dt_kelas  	= $this->m_penempatan->get_one_kelas($id_jenjang,$tingkat);
		$data .= "<option value=''>Pilih Kelas</option>";
		foreach ($dt_kelas->result() as $row) {
			$data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
		}
		echo $data;
	}
	public function get_tingkat(){
		$id_jenjang	= $this->input->post('id_jenjang');	
		$dt_kelas	= $this->m_jenjang->get_one_jenjang($id_jenjang);								
		$data .= "<option value=''>Pilih Tingkat</option>";
		foreach ($dt_kelas->result() as $row) {
			$data .= "<option>$row->tingkat</option>\n";
		}
		echo $data;
	}	
	public function t_pendataan(){
		$id_tahun = $this->input->post('id_tahun');		
		$id_kelas = $this->input->post('id_kelas');
		$data['dt_penempatan'] = $this->m_penempatan->get_pendataan($id_tahun,$id_kelas);
		$this->load->view('panel/t_pendataan',$data);
	}
	public function save()
	{
		$data['id_ta']		= $this->input->post('id_tahun');						
		$data['angkatan']	= $this->input->post('angkatan');						
		$data['id_kelas']	= $this->input->post('id_kelas');						
		$data['id_siswa']	= $this->input->post('id_siswa');								
		$detail['status']	= 'aktif';
		$id_siswa			= $this->input->post('id_siswa');								
		$cek = $this->m_penempatan->cek_siswa($id_siswa);
		if($cek->num_rows()>0){
			echo "nothing";
		}else{
			$this->m_penempatan->tambah($data);
			$this->m_siswa->edit($id_siswa,$detail);
			echo "nihil";
		}					
	}
	public function hapus(){
		$id 		= $this->input->post('id_absen');		
		$this->m_absen->hapus($id);	
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."guru/absen'>";				
	}

}
