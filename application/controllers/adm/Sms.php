<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('file');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_semua');		
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('upload');

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "sms";		
		$data['title']	= "Pengaturan SMS Gateway";			
		$data['isi']    = "sms";
		$data['judul1']	= "Pengaturan SMS Gateway";			
		$data['judul2']	= "";			
		$data['set']	= "view";
		$data['dt_admin']	= $this->m_admin->get_all();											
		$this->template($page, $data);	
	}

	public function cek(){
		$cek		= $this->input->post('cek');				
		$command 	= "cek.bat";
		$filepath 	= "http://localhost/web_absence/gammu/isi.txt";
		//$w = system($command);
		//echo $w;
		echo "<script language='text/javascript'>
	        document.location = '" . $filepath . "';
	    </script>";
	}

}
