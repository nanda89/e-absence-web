<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_tambahan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_jadwal');
        $this->load->model('m_guru');
        $this->load->model('m_jenjang');
        $this->load->model('m_mapel');
        $this->load->model('m_pesan');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_jadwal_tambahan');
        //===== Load Library =====

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "jadwal_tambahan";
        $data['isi'] = "jadwal_tambahan";
        $data['title'] = "SIAKAD | Jadwal Pelajaran Tambahan";
        $data['judul1'] = "Jadwal Pelajaran Tambahan";
        $data['dt_jadwal'] = $this->m_jadwal->get_all();
        $data['dt_tahun'] = $this->m_tahunajaran->get_all_by_status('Aktif');
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $data['judul2'] = "";
        // $data['judul1']        = "Jadwal Pelajaran";
        $data['set'] = "view";
        $this->template($page, $data);
    }

    public function add()
    {
        $page = "jadwal_tambahan";
        $data['title'] = "SIAKAD | Jadwal Pelajaran";
        $data['isi'] = "jadwal_tambahan";
        $data['judul1'] = "Jadwal Pelajaran Tambahan";
        $data['judul2'] = "";
        $data['judul1'] = "Jadwal Pelajaran Tambahan";
        $data['set'] = "insert";
        $data['dt_tahun'] = $this->m_tahunajaran->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $this->template($page, $data);
    }

    public function add_siswa()
    {
        $id_jadwal = $data['id_jadwal_tambahan'] = $this->input->post('id_jadwal');
        $id_siswa = explode(",", $this->input->post('id_siswa'));

        foreach ($id_siswa as $id_siswa) {
            $data['id_siswa'] = $id_siswa;
            $cek = $this->db->query("SELECT * FROM tabel_kelas_tambahan
				WHERE id_jadwal_tambahan = '$id_jadwal' AND id_siswa = '$id_siswa'");

            if ($cek->num_rows() > 0) {
                continue;
            }

            $this->m_jadwal_tambahan->tambah_siswa($data);
        }

        echo "nihil";
    }

    public function show_siswa()
    {
        $jadwal_tambahan_id = $this->input->post('id_jadwal');
        $students = $this->m_jadwal_tambahan->get_all_student_by_kls_tambahan($jadwal_tambahan_id);
        $data['dt_siswa'] = $students;

        $jdwl_tambahan = $this->m_jadwal_tambahan->get_kelas_tambahan_by_id($jadwal_tambahan_id);
        $jdwl_tmbh = "";
        if ($jdwl_tambahan->num_rows() > 0) {
            foreach ($jdwl_tambahan->result() as $row) {
                $jdwl_tmbh = $row;
            }
        }

        $data['dt_jdwl_tmbh'] = $jdwl_tmbh;
        $this->load->view('panel/t_daftar_siswa_kls_tambahan', $data);
    }

    public function kelas_tambahan()
    {
        $page = "kelas_tambahan";
        $data['isi'] = "kelas_tambahan";
        $data['title'] = "SIAKAD | Kelas Tambahan";
        $data['judul1'] = "Kelas Tambahan";

        $jadwal_tambahan_id = $this->input->get('jdwl_tambhn');
        $jdwl_tambahan = $this->m_jadwal_tambahan->get_jadwal_by_id($jadwal_tambahan_id);
        $jdwl_tmbh = "";
        if ($jdwl_tambahan->num_rows() > 0) {
            foreach ($jdwl_tambahan->result() as $row) {
                $jdwl_tmbh = $row;
            }
        }

        $data['dt_kelas_reg'] = $this->m_jadwal_tambahan->get_kelas_regular($jdwl_tmbh->id_jenjang);
        $data['dt_kelas_tambahan'] = $this->m_jadwal_tambahan->get_kelas_tambahan($jdwl_tmbh->id_jenjang);
        $data['dt_kls_tmbh'] = $jdwl_tmbh;
        $data['judul2'] = "";
        $data['set'] = "view";
        $this->template($page, $data);
    }

    public function t_jadwal()
    {
        $id_tahun = $this->input->post('id_tahun');
        $id_jenjang = $this->input->post('id_jenjang');
        $nama_kelas = $this->input->post('nama_kelas');
        $id_guru = $this->input->post('id_guru');
        $id_mapel = $this->input->post('id_mapel');
        $id_pembimbing1 = $this->input->post('id_pembimbing1');

        $data['dt_jadwal'] = $this->m_jadwal_tambahan
            ->get_jadwal($id_tahun, $id_jenjang, $nama_kelas, $id_guru, $id_mapel, $id_pembimbing1);
        $this->load->view('panel/t_jadwal_tambahan', $data);
    }

    public function t_siswa()
    {
        $id_kelas_reg = $this->input->post('id_kelas_reg');
        $id_kelas_tmbhn = $this->input->post('id_kelas_tmbhn');

        $data['dt_siswa'] = "";

        if ($id_kelas_reg != null || $id_kelas_reg != "") {
            $data['dt_siswa'] = $this->m_jadwal_tambahan->get_siswa_reg_by_kelas($id_kelas_reg);
        } else if ($id_kelas_tmbhn != null || $id_kelas_tmbhn != "") {
            $data['dt_siswa'] = $this->m_jadwal_tambahan->get_siswa_tmbhn_by_kelas($id_kelas_tmbhn);
        }

        $this->load->view('panel/t_daftar_siswa', $data);
    }

    public function export_now()
    {
        $page = "jadwal_tambahan";
        $data['title'] = "Jadwal Pelajaran Tambahan";
        $data['isi'] = "jadwal_tambahan";
        $data['judul1'] = "Jadwal Pelajaran Tambahan";
        $data['judul2'] = "";

        $id_tahun = $this->input->get('id_tahun');
        $id_jenjang = $this->input->get('id_jenjang');
        $nama_kelas = $this->input->get('nama_kelas');
        $id_guru = $this->input->get('id_guru');
        $id_mapel = $this->input->get('id_mapel');
        $id_pembimbing1 = $this->input->get('id_pembimbing1');

        $a = $this->input->get("a");
        $data['dt_jadwal'] = $this->m_jadwal_tambahan->get_jadwal($id_tahun, $id_jenjang, $nama_kelas, $id_guru, $id_mapel, $id_pembimbing1);
        $a = $this->input->get('a');
        if ($a == "1") {
            $this->load->view('panel/excel_jadwal_tambahan', $data);
        } elseif ($a == "2") {
            $this->load->view('panel/pdf_jadwal_tambahan', $data);
        }
    }

    public function delete_jadwal()
    {
        $id = $this->input->post('id_jadwal');
        $this->m_jadwal_tambahan->hapus_jadwal($id);
        echo "nihil";
    }

    public function delete_siswa()
    {
        $id = $this->input->post('id_jadwal');
        $id_siswa = $this->input->post('id_siswa');
        $this->m_jadwal_tambahan->hapus_siswa($id, $id_siswa);
        echo "nihil";
    }

    public function reset()
    {
        $this->m_jadwal->hapus_all();
        ?>
		<script type="text/javascript">
			alert("Semua data dihapus");
		</script>
<?php
echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jadwal_tambahan'>";
    }

    public function save()
    {
        $id_ta = $data['id_ta'] = $this->input->post('id_tahun');
        $id_jenjang = $data['id_jenjang'] = $this->input->post('id_jenjang');
        $nama_kelas = $data['nama_kelas'] = $this->input->post('nama_kelas');
        $id_mapel = $data['id_mapel'] = $this->input->post('id_mapel');
        $id_guru = $data['id_guru'] = $this->input->post('id_guru');
        $id_pendamping1 = $data['id_pembimbing_1'] = $this->input->post('id_pendamping1');
        $id_pendamping2 = $data['id_pembimbing_2'] = $this->input->post('id_pendamping2');
        $id_pendamping3 = $data['id_pembimbing_3'] = $this->input->post('id_pendamping3');
        $tgl_mulai = $data['start'] = $this->input->post('tgl_mulai');
        $tgl_akhir = $data['end'] = $this->input->post('tgl_akhir');
        $hari = $data['hari'] = $this->input->post('hari');
        $jam_awal = $data['jam_awal'] = $this->input->post('jam_awal');
        $jam_akhir = $data['jam_akhir'] = $this->input->post('jam_akhir');
        $pekan = $data['minggu_ke'] = $this->input->post('pekan');
        $status = $data['keterangan'] = $this->input->post('isi');

        $isValid_jam_awal = $this->validate_time($jam_awal);
        $isValid_jam_akhir = $this->validate_time($jam_akhir);

        if (!$isValid_jam_awal || !$isValid_jam_akhir) {
            echo "input jam tidak sesuai";

        } else {

        }
        //cek1: jika kelas sdh pernah dibuat
        $cek = $this->db->query("SELECT * FROM tabel_jadwal_tambahan
				WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang'
					AND nama_kelas = '$nama_kelas' AND id_mapel = '$id_mapel'
					AND id_guru = '$id_guru' AND id_pembimbing_1 = '$id_pendamping1'
					AND hari = '$hari' AND start = '$tgl_mulai' AND end ='$tgl_akhir'
					AND minggu_ke = '$pekan' AND jam_awal = '$jam_awal'
					AND jam_akhir = '$jam_akhir'");

        if ($cek->num_rows() > 0) {
            echo "Data yang anda masukkan sudah ada!";
        } else {
            $cek2 = $this->db->query("SELECT * FROM tabel_jadwal_tambahan
					WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND
					nama_kelas = '$nama_kelas' AND hari = '$hari' AND
					jam_awal = '$jam_awal' AND jam_akhir = '$jam_akhir' AND
					start = '$tgl_mulai' AND end = '$tgl_akhir' AND hari = '$hari' AND
					minggu_ke = '$pekan'");

            if ($cek2->num_rows() > 0) {
                $guru = $this->db->query("SELECT nama FROM tabel_guru WHERE id_guru=" . $cek2->row()->id_guru);
                echo "Sudah terisi oleh " . $guru->row()->nama;
            } else {
                // sekarang jadwal bisa diisi, cek dulu gurunya ada jadwal mengajar tidak di kelas lain pada jam yg sama
                $cek3 = $this->db->query("SELECT * FROM tabel_jadwal
					WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND
					id_guru = '$id_guru' AND hari LIKE '%$hari' AND jam_awal = '$jam_awal' AND
					jam_akhir = '$jam_akhir'");

                $cek4 = $this->db->query("SELECT * FROM tabel_jadwal_tambahan
					WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND
					id_guru = '$id_guru' AND hari = '$hari' AND jam_awal = '$jam_awal' AND
					jam_akhir = '$jam_akhir' AND minggu_ke = $pekan");

                if ($cek3->num_rows() > 0 || $cek4->num_rows() > 0) {
                    echo "Tidak diperkenankan mengajar 2 kelas dalam waktu yang bersamaan.";
                } else {

                    //cek nama kelas tidak boleh sama dengan id jenjang dan tahun ajaran yang sama
                    $cek5 = $this->db->query("SELECT * FROM tabel_jadwal_tambahan
					WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND
					nama_kelas = '$nama_kelas'");
                    if ($cek5->num_rows() > 0) {
                        echo "Nama kelas sudah ada sebelumnya,harap ubah nama kelas!";
                    } else {
                        $this->m_jadwal_tambahan->tambah($data);
                        echo "nihil";
                    }

                }
            }
        }

        // cek 2: cek jika dikelas x pada jam yang sama (walaupun beda mapel tetap jam harus beda dong)

    }

    public function validate_time($str)
    {

        $expl = explode(':', $str);
        $hh = $expl[0];
        $mm = $expl[1];

        if (!is_numeric($hh) || !is_numeric($mm)) {
            return false;
        } else if ((int) $hh > 24 || (int) $mm > 59) {
            return false;
        } else if (mktime((int) $hh, (int) $mm) === false) {
            return false;
        }

        return true;
    }
    public function get_tingkat()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_kelas = $this->m_jenjang->get_one_jenjang($id_jenjang);
        $data = "<option value=''>Pilih Tingkat</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option>$row->tingkat</option>\n";
        }
        echo $data;
    }
    public function get_kelas()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $level = $this->session->userdata('level');
        $id_cabang = $this->session->userdata('id_cabang');
        if ($level == 'operator cabang') {
            $dt_kelas = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang' AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
        } else {
            $dt_kelas = $this->m_jadwal->get_one_kelas($id_jenjang);
        }
        $data = "<option value=''>Pilih Kelas</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }
    public function get_guru()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_guru = $this->m_guru->get_one_guru($id_jenjang);

        $data = "";
        foreach ($dt_guru->result() as $row) {
            $data .= "<option value='$row->id_guru'>$row->nama ($row->alias)</option>\n";
        }
        echo $data;
    }
    public function get_mapel()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_mapel = $this->m_mapel->get_one_jen_tambah($id_jenjang);
        $data = "<option value=''>Pilih Mapel Tambahan</option>";
        foreach ($dt_mapel->result() as $row) {
            $data .= "<option value='$row->id_mapel'>$row->mapel</option>\n";
        }
        echo $data;
    }
}
