<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_konfigurasi');		
		$this->load->model('m_pesan');		
		$this->load->model('m_jenjang');		
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "konfigurasi";
		$data['isi']    = "konfigurasi";		
		$data['title']	= "SIAKAD | Konfigurasi Pendaftaran Siswa Baru";			
		$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";			
		$data['judul2']	= "";			
		$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";
		$data['set']	= "view";
		$data['dt_ta']	= $this->m_konfigurasi->get_all();					
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "konfigurasi";
		$data['isi']    = "konfigurasi";				
		$data['title']	= "SIAKAD | Konfigurasi Pendaftaran Siswa Baru";			
		$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";			
		$data['judul2']	= "";			
		$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";
		$data['set']	= "insert";			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['id_jenjang']	= $this->input->post('id_jenjang');						
			$data['nama_proses']	= $this->input->post('nama_proses');						
			$data['kode_awal']	= $this->input->post('kode_awal');						
			$data['ket']	= $this->input->post('ket');						
			$this->m_konfigurasi->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/konfigurasi'>";
		}			
	}
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "konfigurasi";	
			$data['isi']    = "konfigurasi";					
			$data['one_post']= $this->m_konfigurasi->get_one($id);
			$data['dt_jenjang']	= $this->m_jenjang->get_all();			
			$data['title']	= "SIAKAD | Konfigurasi Pendaftaran Siswa Baru";			
			$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";			
			$data['judul2']	= "";			
			$data['judul1']	= "Konfigurasi Pendaftaran Siswa Baru";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		elseif ($set == 'edit' )
		{
			$data['id_jenjang']	= $this->input->post('id_jenjang');						
			$data['nama_proses']	= $this->input->post('nama_proses');						
			$data['kode_awal']	= $this->input->post('kode_awal');						
			$data['ket']	= $this->input->post('ket');
			$this->m_konfigurasi->edit($id, $data);
			?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");			
					</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/konfigurasi'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_konfigurasi->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/konfigurasi'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/konfigurasi'>";
		}
	}	

}
