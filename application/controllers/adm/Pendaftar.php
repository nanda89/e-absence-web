<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftar extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_siswa');				
		$this->load->model('m_pesan');				
		$this->load->model('m_jenjang');		
		$this->load->model('m_penjadwalan');
		$this->load->model('m_konfigurasi');
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "calon_siswa";
		$data['isi']    = "calon_siswa";		
		$data['title']	= "SIAKAD | Data Pendaftaran";			
		$data['judul1']	= "Data Pendaftaran";			
		$data['judul2']	= "";			
		$data['judul1']	= "Data Pendaftaran";
		$data['set']	= "view";
		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$data['dt_ta']= $this->db->query("SELECT * FROM tabel_daf_jenis INNER JOIN tabel_daf_jadwal
        		ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis) INNER JOIN tabel_calon_siswa 
		        ON (tabel_daf_jadwal.id_daf_jadwal = tabel_calon_siswa.id_daf_jadwal) LEFT JOIN tabel_cabang
		        ON (tabel_calon_siswa.id_cabang = tabel_cabang.id_cabang)
		        WHERE tabel_calon_siswa.status <> 'aktif' AND tabel_calon_siswa.id_cabang = '$id_cabang'
				ORDER BY id_daftar asc");			
		}else{
			$data['dt_ta']	= $this->m_siswa->get_all_calon();					
		}
		$data['dt_jenjang']= $this->m_jenjang->get_all();			
		$data['dt_ja']	= $this->m_penjadwalan->get_all();					
		$data['dt_konfigurasi']	= $this->m_konfigurasi->get_all();							
		$this->template($page, $data);	
	}

	public function get_jadwal(){
		$id_daf_jenis	= $this->input->post('id_daf_jenis');
		$dt_daf  	= $this->m_konfigurasi->get_one_jadwal($id_daf_jenis);
		$data .= "<option value=''>Pilih Gelombang Pendaftaran</option>";
		foreach ($dt_daf->result() as $row) {
			$data .= "<option value='$row->id_daf_jadwal'>$row->kelompok</option>\n";
		}
		echo $data;
	}	

	public function filter(){
		$data['judul1']	= "Data Pendaftaran";			
		$data['judul2']	= "";			
		$data['judul1']	= "Data Pendaftaran";
		$data['title']	= "SIAKAD | Data Pendaftaran";			
		$id_daf_jenis	= $this->input->get('id_daf_jenis');
		$tahun			= $this->input->get('tahun');
		$id_daf_jadwal	= $this->input->get('id_daf_jadwal');
		$data['isi']    = "calon_siswa";	
		if($id_daf_jadwal<>""){	
			$data['dt_ta']	= $this->m_siswa->get_calon($id_daf_jenis,$tahun,$id_daf_jadwal);
		}else{
			$data['dt_ta']	= $this->m_siswa->get_calon_b($id_daf_jenis,$tahun);
		}

		$page			= "t_pendaftar";
		$this->template($page, $data);	
	}	

	public function lulus(){
		$id				= $this->input->get('v');								
		$detail['status']	= "lulus";		
		$this->m_siswa->edit_calon($id, $detail);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pendaftar'>";
	}
	public function t_lulus(){
		$id				= $this->input->get('v');								
		$detail['status']	= "tidak lulus";		
		$this->m_siswa->edit_calon($id, $detail);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pendaftar'>";
	}
	public function lulus1(){
		$j				= $this->input->get('a');								
		$t				= $this->input->get('b');								
		$w				= $this->input->get('c');								
		$id				= $this->input->get('d');								
		$detail['status']	= "lulus";		
		$this->m_siswa->edit_calon($id, $detail);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pendaftar/filter?id_daf_jenis=$j&tahun=$t&id_daf_jadwal=$w'>";
	}
	public function t_lulus1(){
		$j				= $this->input->get('a');								
		$t				= $this->input->get('b');								
		$w				= $this->input->get('c');								
		$id				= $this->input->get('d');								
		$detail['status']	= "tidak lulus";		
		$this->m_siswa->edit_calon($id, $detail);
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pendaftar/filter?id_daf_jenis=$j&tahun=$t&id_daf_jadwal=$w'>";
	}
		
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'detail')
		{
			$page					= "detail_calon_siswa";		
			$data['one_siswa']		= $this->m_siswa->cek_lulus($id);
			$data['one_detail']		= $this->m_siswa->cek_lulus_detail($id);
			$data['title']			= "SIAKAD | Detail Pendaftaran";			
			$data['judul1']			= "Detail Data Calon Siswa";	
			$data['isi']   			= "calon_siswa";					
			$data['judul2']			= "";						
			$data['set']			= "detail";	
			$this->template($page, $data);	
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_siswa->hapus_calon($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pendaftar'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/penjadwalan'>";
		}
	}	

}
