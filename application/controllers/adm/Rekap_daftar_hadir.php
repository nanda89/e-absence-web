<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_daftar_hadir extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		# SESSION
		$name = $this->session->userdata('nama');
		if ($name == "") echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";

		date_default_timezone_set("Asia/Jakarta");
		$this->load->database();

		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');
		$this->load->model('m_jenjang');
		$this->load->model('m_mapel');
		$this->load->model('m_jadwal');
		$this->load->model('m_guru');
		$this->load->model('m_tahunajaran');
		$this->load->model('m_siswakelas');

		$this->load->library('upload');
		$this->load->helper('url');
	}

	protected function template($page, $data)
	{
		$this->load->view('t_panel/header', $data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");
		$this->load->view('t_panel/footer');
	}

	public function index()
	{
		$page = "rekap_daftar_hadir";
		$tgl = date('d-m-Y');
		$name = $this->session->userdata('username');

		$data['isi'] = "rekap_daftar_hadir";
		$data['title'] = "Rekap Daftar Hadir Siswa";
		$data['judul1']	= "Rekap Daftar Hadir Siswa";
		$data['judul2']	= "";
		$data['set'] = "view";
		$data['dt_wali_kelas'] = $this->m_siswakelas->dt_wali_kelas();
		$data['dt_jenjang'] = $this->m_jenjang->get_all();
		$data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
		$data['dt_ta'] = $this->m_tahunajaran->get_all();

		$this->template($page, $data);
	}

	public function export_now()
	{
		$jenis = $this->input->get('jenis');

		$data['isi'] = "rekap_daftar_hadir";
		$data['title'] = "Rekap Daftar Hadir Siswa";
		$data['judul1']	= "Rekap Daftar Hadir Siswa";
		$data['judul2']	= "";

		if ($jenis == 'reguler') {

			$id_kelas = $data['id_kelas'] = $this->input->get('id_kelas');
			$id_jenjang = $data['id_jenjang'] = $this->input->get('id_jenjang');
			$id_mapel = $data['id_mapel'] = $this->input->get('id_mapel');
			$id_ta = $data['id_ta'] = $this->input->get('id_ta');
			$id_guru = $data['id_guru'] = $this->input->get('id_guru');
			$tgl_awal = $data['tgl_awal'] = $this->input->get('tgl_awal');
			$tgl_akhir = $data['tgl_akhir'] = $this->input->get('tgl_akhir');

			$data['ta'] = $this->input->get('ta');
			$data['guru'] = $this->input->get('guru');
			$data['mapel'] = $this->input->get('mapel');
			$data['kelas'] = $this->input->get('kelas');

			$url = base_url('assets/file/daftar_hadir.php') . "?id_kelas=" . $id_kelas . "&id_mapel=" . $id_mapel .
				"&id_jenjang=" . $id_jenjang . "&dari=" . $tgl_awal .
				"&ke=" . $tgl_akhir . "&guru=" . $id_guru . "&id_ta=" . $id_ta . "&jenis=Reguler";
		} else {
			$id_jenjang = $data['id_jenjang'] = $this->input->get('id_jenjang');
			$id_mapel = $data['id_mapel'] = $this->input->get('id_mapel');
			$id_ta = $data['id_ta'] = $this->input->get('id_ta');
			$id_guru = $data['id_guru'] = $this->input->get('id_guru');
			$tgl_awal = $data['tgl_awal'] = $this->input->get('tgl_awal');
			$tgl_akhir = $data['tgl_akhir'] = $this->input->get('tgl_akhir');

			$data['ta'] = $this->input->get('ta');
			$data['guru'] = $this->input->get('guru');
			$data['mapel'] = $this->input->get('mapel');
			$data['kelas'] = "Kelas Tambahan";

			$url = base_url('assets/file/daftar_hadir.php') . "?id_mapel=" . $id_mapel .
				"&id_jenjang=" . $id_jenjang . "&dari=" . $tgl_awal .
				"&ke=" . $tgl_akhir . "&guru=" . $id_guru . "&id_ta=" . $id_ta . "&jenis=Tambahan";
		}

		$json = json_decode(file_get_contents($url));

		$tabel1 = "";
		if (count($json) > 0) {
			$tabel1 .= '<table id="table" border="1" class="table table-bordered table-hovered">' .
				'<thead><tr><th width="1%">No</th><th width="3%">STB</th>' .
				'<th width="15%">Nama Siswa</th><th width="1%">JK</th>';

			foreach ($json->tanggal as $tgl) {
				$tabel1 .= '<th width="1%">' . $tgl->tgl . '</th>';
			}
			$tabel1 .= '<th width="1%">Hadir</th><th width="2%">Tdk. Hdr</th>';
			$tabel1 .= '</tr></thead><tbody>';

			foreach ($json->siswa as $sis) {
				$tabel1 .= '<tr><td>' . $sis->no . '</td><td style="text-align:center;">' . $sis->stb . '</td><td>' .
					$sis->nama . '</td><td style="text-align:center;">' . $sis->sex . '</td>';

				foreach ($sis->absen as $abs) {
					$tabel1 .= '<td style="text-align:center;">' . $abs . '</td>';
				}
				$tabel1 .= '<td style="text-align:center;">' . $sis->hadir . '</td><td style="text-align:center;">' . $sis->alpha . '</td>';
				$tabel1 .= '</tr>';
			}

			$tabel1 .= '</tbody></table>';
		}
		
		$tabel2 = '<br><table border="0" class="table table-bordered table-hovered"> <tr><td style="width: 15%;"><b>Total Siswa<b></td>' .
			'<td style="width: 1%;">:</td><td style="width: 20%;">' .
			$json->total_siswa . '</td><td style="width: 15%;"><b>Total Izin<b></td><td style="width: 1%;">:</td><td style="width: 20%;">'.$json->total_izin . '</td></tr>' .
			'<tr><td style="width: 15%;"><b>Total Tatap Muka<b></td>' .
			'<td style="width: 1%;">:</td><td style="width: 20%;">' .
			$json->total_tatap_muka . '</td><td style="width: 15%;"><b>Total Alpha<b></td><td style="width: 1%;">:</td><td style="width: 20%;">'.$json->total_alpha . '</td></tr>' .
			'<tr><td style="width: 15%"><b>Total Hadir<b></td>' .
			'<td style="width: 1%;">:</td><td style="width: 20%;">' .
			$json->total_hadir . '</td></td><td style="width: 15%;"><b>Total Nihil<b></td><td style="width: 1%;">:</td><td style="width: 20%;">'.$json->total_nihil . '</td></tr>' .
			'<tr><td style="width: 15%"><b>Total Sakit<b></td>' .
			'<td style="width: 1%;">:</td><td style="width: 20%;">' .
			$json->total_sakit . '</td></tr>' .
			'</table>';


		$table3 = "";

		if (count($json->attachment) > 0) {
			$table3 .= '<br><table id="table-attach" border="1" class="table table-bordered table-hovered">' .
				'<thead><tr><th width="5%;">No Urut</th><th width="15%;">Tanggal Tatap Muka</th>' .
				'<th width="45%;">Standar Kompetensi/Kompetensi Dasar/Indikator</th><th width="40%;">Keterangan</th></tr></thead><tbody>';

			foreach ($json->attachment as $att) {
				$table3 .= '<tr><td>' . $att->no . '</td><td>' . $att->tgl_tatap_muka .
					'</td><td>' . $att->kompetensi . '</td>' .
					'<td>' . $att->keterangan . '</td></tr>';
			}

			$table3 .= '</tbody></table>';
		}

		$data['tabel_1'] = $tabel1;
		$data['tabel_2'] = $tabel2;
		$data['tabel_3'] = $table3;


		$a	= $this->input->get('a');
		if ($a == "1") {
			$this->load->view('panel/excel_rekap_daftar_hadir', $data);
		} elseif ($a == "2") {
			$this->load->view('panel/pdf_rekap_daftar_hadir', $data);
		}
	}

	public function get_mapel()
	{
		$jenjang = $this->input->post('jenjang');
		$dt_mapel = $this->m_mapel->get_one_jen($jenjang);
		$mapel = "<option value=''>Pilih Mapel</option>";
		foreach ($dt_mapel->result() as $row) {
			$mapel .= "<option value='$row->id_mapel'>$row->mapel</option>\n";
		}

		echo $mapel;
	}

	public function get_kelas_reg_by_guru()
	{
		$id_guru = $this->input->post('id_guru');
		$dt_kelas = $this->m_guru->get_kelas_by_guru($id_guru);
		$kelas = "<option value=''>Pilih Kelas</option>";
		foreach ($dt_kelas->result() as $row) {
			$kelas .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
		}
		echo $kelas;
	}
}
