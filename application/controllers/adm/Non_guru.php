<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Non_guru extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_non_guru');
        $this->load->model('m_jenjang');
        $this->load->model('m_pesan');
        $this->load->model('m_mapel');
        $this->load->model('m_semua');
        //===== Load Library =====
        $this->load->library('upload');

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "non_guru";
        $data['isi'] = "non guru";
        $data['title'] = "Non guru";
        $data['judul1'] = "Non guru";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_non_guru'] = $this->m_non_guru->get_all();
        $this->template($page, $data);
    }

    public function add()
    {
        $page = "non_guru";
        $data['isi'] = "non guru";
        $data['title'] = "Non guru";
        $data['judul1'] = "Tambah Data Non Guru";
        $data['judul2'] = "";
        $data['set'] = "insert";
        $this->template($page, $data);
    }
    public function ajax_bulk_delete()
    {
        $tabel = "tabel_non_guru";
        $pk = "id_non_guru";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        // echo json_encode(array("status" => true));
        $_SESSION['pesan'] = "Berhasil dihapus!";
        $_SESSION['tipe'] = "danger";
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";

    }
    public function delete_multiple()
    {
        $this->m_non_guru->remove_checked();
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
    }

    public function save()
    {
        if ($this->input->post('save') == 'save') {
            //setting konfigurasi upload gambar
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('gambar')) {
                $gambar = "";
            } else {
                $gambar = $this->upload->file_name;
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['nip'] = $this->input->post('nip');
            $data['nama'] = $this->input->post('nama');
            $data['no_ktp'] = $this->input->post('no_ktp');
            $data['alias'] = $this->input->post('alias');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['ijazah'] = $this->input->post('ijazah');
            $data['pend_terakhir'] = $this->input->post('pend_terakhir');
            $data['alamat'] = $this->input->post('alamat');
            $data['unit_kerja'] = $this->input->post('unit_kerja');
            $data['tmt'] = $this->input->post('tmt');
            $data['jabatan'] = $this->input->post('jabatan');
            $data['gambar'] = $gambar;

            $user['nama'] = $this->input->post('alias');
            $user['username'] = $this->input->post('nip');
            $user['password'] = md5($this->input->post('password'));
            $user['level'] = "non guru";
            $user['avatar'] = $gambar;
            $user['tgl_daftar'] = $this->input->post('tmt');

            $this->db->trans_begin();
            $this->m_non_guru->tambah($data);
            if ($this->input->post('login_access') == "1") {
                $this->m_admin->tambah($user);
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $_SESSION['pesan'] = "Gagal tersimpan!";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";

            } else {
                $this->db->trans_commit();

                $_SESSION['pesan'] = "Berhasil tersimpan!";
                $_SESSION['tipe'] = "info";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";

            }
        }
    }

    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'ubah') {
            $page = "non_guru";
            $data['one_post'] = $this->m_non_guru->get_one($id);
            $data['title'] = "Non guru";
            $data['judul1'] = "Edit Data Non Guru";
            $data['isi'] = "non guru";
            $data['judul2'] = "";
            $data['set'] = "edit";
            $this->template($page, $data);
        }
        //DETAIL DATA
        elseif ($set == 'detail') {
            $page = "non_guru";
            $data['one_post'] = $this->m_non_guru->get_one($id);
            $data['title'] = "Non guru";
            $data['judul1'] = "Detail Data Non Guru";
            $data['isi'] = "non guru";
            $data['judul2'] = "";
            $data['set'] = "detail";
            $this->template($page, $data);
        }
        //EDIT DATA KEGIATAN
        elseif ($set == 'edit') {
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $data['gambar'] = $this->upload->file_name;

                $one = $this->m_non_guru->get_one($id);
                $row = $one->row();
                unlink("assets/panel/images/" . $row->gambar); //Hapus Gambar
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['nip'] = $this->input->post('nip');
            $data['nama'] = $this->input->post('nama');
            $data['no_ktp'] = $this->input->post('no_ktp');
            $data['alias'] = $this->input->post('alias');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['ijazah'] = $this->input->post('ijazah');
            $data['pend_terakhir'] = $this->input->post('pend_terakhir');
            $data['alamat'] = $this->input->post('alamat');
            $data['unit_kerja'] = $this->input->post('unit_kerja');
            $data['tmt'] = $this->input->post('tmt');
            $data['jabatan'] = $this->input->post('jabatan');
            $this->m_non_guru->edit($id, $data);
            $_SESSION['pesan'] = "Berhasil tersimpan!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_non_guru->hapus($id);
            $_SESSION['pesan'] = "Berhasil dihapus!";
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
        } elseif ($set == 'set_akun') {
            $data['username'] = $this->input->post('nip');
            $data['password'] = md5("sari123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "non guru";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "guru-lk.png";
            } else {
                $data['avatar'] = "guru-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->tambah($data);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
        } elseif ($set == 'set_default') {
            $data['username'] = $this->input->post('nip');
            $username = $this->input->post('nip');
            $data['password'] = md5("sari123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "non guru";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "guru-lk.png";
            } else {
                $data['avatar'] = "guru-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->edit_akun($username, $data);
            $_SESSION['pesan'] = "Berhasil set default!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/non_guru'>";
        }
    }

}
