<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_siswa');
		$this->load->model('m_jenjang');
		$this->load->model('m_pesan');		
		$this->load->model('m_kenaikan');		
		$this->load->model('m_tahunajaran');		
		$this->load->model('m_penempatan');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "pilih_lulus";
		$data['isi']    = "kelulusan";		
		$data['title']	= "SIAKAD | Data Kelulusan";			
		$data['judul1']	= "Data Kelulusan";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$data['dt_jenjang'] = $this->m_jenjang->get_all();					
		$data['dt_ta'] 		= $this->m_tahunajaran->get_all();					
		$this->template($page, $data);	
	}
	public function set_kelas()
	{		
		$page			= "kelulusan";
		$data['isi']    = "kelulusan";		
		$data['title']	= "SIAKAD | Data Kelulusan";			
		$data['judul1']	= "Data Kelulusan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$id				= $this->input->get('id_jenjang');
		$id_ta			= $this->input->get('id_ta');
		$id_kelas		= $this->input->get('id_kelas');		
		$id_jenjang		= $this->input->get('id_jenjang');		
		$data['dt_j']		= $id;					
		$data['dt_ke']		= $this->m_jenjang->get_one($id);					
		$data['dt_pen']		= $this->m_kenaikan->get_all($id);							
		$data['dt_ta'] 		= $this->m_tahunajaran->get_all();					
		$data['dt_kelas']	= $this->m_jenjang->get_one_jenjang($id);					
		$data['dt_siswa']	= $this->m_penempatan->get_siswa($id_ta,$id_kelas);					
		$this->template($page, $data);	
	}	
	public function get_kelas(){
		$id_jenjang	= $this->input->post('id_jenjang');
		$tingkat	= $this->input->post('tingkat');
		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$dt_kelas  	= $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
		}else{
			$dt_kelas  	= $this->m_penempatan->get_one_kelas($id_jenjang,$tingkat);	
		}
		$data .= "<option value=''>Pilih Kelas</option>";
		foreach ($dt_kelas->result() as $row) {
			$data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
		}
		echo $data;
	}	
	public function t_kelulusan(){		
		$angkatan = $this->input->post('angkatan');
		$data['dt_kelulusan'] = $this->m_penempatan->get_lulus($angkatan);
		$this->load->view('panel/t_kelulusan',$data);
	}
	public function t_siswakelas(){
		$id_tahun = $this->input->post('id_tahun');		
		$id_kelas = $this->input->post('id_kelas');
		$data['dt_kenaikan'] = $this->m_kenaikan->get_siswa($id_tahun,$id_kelas);
		$this->load->view('panel/t_siswalulus',$data);
	}
	public function save()
	{
		$data['id_ta']		= $this->input->post('id_tahun');						
		$data['id_kelas']	= $this->input->post('id_kelas');						
		$data['id_siswa']	= $this->input->post('id_siswa');								
		$detail['status']	= 'aktif';
		$id_siswa			= $this->input->post('id_siswa');								
		$cek = $this->m_kenaikan->cek_siswa($id_siswa);
		if($cek->num_rows()>0){
			echo "nothing";
		}else{
			$this->m_kenaikan->tambah($data);
			$this->m_siswa->edit($id_siswa,$detail);
			echo "nihil";
		}					
	}
	public function luluskan()
	{
		$id_penempatan		= $this->input->post('id_penempatan');						
		$data['id_ta']		= $this->input->post('id_ta');							
		$data['id_kelas']	= $this->input->post('id_kelas');										
		$data['id_siswa']	= $this->input->post('id_siswa');										
		$data['tgl_lulus']	= $this->input->post('tanggal');										

		$data2['status']	= 'non-aktif';
		$detail['status']	= 'non-aktif';

		$id_siswa			= $this->input->post('id_siswa');								
		$cek = $this->m_kenaikan->cek_lulus($id_siswa);
		if($cek->num_rows()>0){
			$this->m_kenaikan->edit_lulus($id_siswa,$data);
			echo "nihil";
		}else{
			$this->m_kenaikan->hapus_penempatan($id_siswa);
			$siswa = $this->m_siswa->get_one($id_siswa);
			$siswa = $siswa->row();
			$this->m_siswa->hapus($id_siswa);
			$haha = array(
				'id_siswalulus'=>$siswa->id_siswa,
				'id_jenjang' => $siswa->id_jenjang,
				'nama_lengkap' => $siswa->nama_lengkap,
				'nisn' => $siswa->nisn,
				'jenis_kelamin' => $siswa->jenis_kelamin,
				'tempat_lahir' => $siswa->tempat_lahir,
				'tgl_lahir' => $siswa->tgl_lahir,
				'agama' => $siswa->agama,
				'alamat' => $siswa->alamat,
				'no_hp' => $siswa->no_hp,
				'email' => $siswa->email,
				'asal_sekolah' => $siswa->asal_sekolah,
				'ket_asal' => $siswa->ket_asal,
				'nama_ayah' => $siswa->nama_ayah,
				'nama_ibu' => $siswa->nama_ibu,
				'gambar' => $siswa->gambar,
				'tgl_daftar' => $siswa->tgl_daftar,
				'no_induk_kelas' => $siswa->no_induk_kelas,
				'telp_ayah' => $siswa->telp_ayah,
				'telp_ibu' => $siswa->telp_ibu
			);
			$this->db->insert('tabel_siswalulus',$haha);
			$this->m_kenaikan->tambah_lulus($data);			
			echo "nihil";
		}					
	}
	public function turunkan()
	{
		$id_penempatan		= $this->input->post('id_penempatan');						
		$data['id_ta']		= $this->input->post('id_tahun');							
		$data['id_kelas']	= $this->input->post('id_kelas');										
		$data['id_siswa']	= $this->input->post('id_siswa');										
		$data['status']		= 'aktif';
		$detail['status']	= 'non-aktif';
		$id_siswa			= $this->input->post('id_siswa');								

		
		$this->m_kenaikan->edit($id_penempatan,$data);		
		echo "nihil";		
	}
	public function delete_kenaikan(){
		$id 		= $this->input->post('id_kenaikan');		
		$this->m_kenaikan->hapus($id);			
		echo "nihil";
	}

}
