<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_absen extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_tahunajaran');
		$this->load->model('m_pesan');		
		$this->load->model('m_jenjang');
		$this->load->model('m_semua');		

		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "cek_absen";		
		$data['title']	= "Cek Absen Kelas";			
		$data['isi']    = "cek_absen";
		$data['judul1']	= "Cek Absen Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "tahun_ajaran";		
		$data['title']	= "Tahun Ajaran";			
		$data['judul1']	= "Tahun Ajaran";		
		$data['isi']    = "cek_absen";	
		$data['judul2']	= "";			
		$data['judul1']	= "Tahun Ajaran";
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['tahun_ajaran']	= $this->input->post('tahun_ajaran');			
			$data['tgl_awal']			= $this->input->post('tgl_awal');			
			$data['tgl_akhir']		= $this->input->post('tgl_akhir');						
			$this->m_tahunajaran->tambah($data);
			
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tahunajaran'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "tahun_ajaran";		
			$data['one_post']= $this->m_tahunajaran->get_one($id);
			$data['title']	= "Tahun Ajaran";			
			$data['judul1']	= "Tahun Ajaran";			
			$data['isi']    = "cek_absen";
			$data['judul2']	= "";			
			$data['judul1']	= "Tahun Ajaran";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$data['tahun_ajaran']	= $this->input->post('tahun_ajaran');			
			$data['tgl_awal']			= $this->input->post('tgl_awal');			
			$data['tgl_akhir']		= $this->input->post('tgl_akhir');						
			$this->m_tahunajaran->edit($id, $data);
			
			$_SESSION['pesan'] 	= "Berhasil diubah!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tahunajaran'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_tahunajaran->hapus($id);			
			$_SESSION['pesan'] 	= "Berhasil dihapus!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tahunajaran'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tahunajaran'>";
		}
	}

}
