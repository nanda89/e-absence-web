<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_bayar extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_form_bayar');
		$this->load->model('m_jenjang');
		$this->load->model('m_biaya');
		$this->load->model('m_siswa');
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('cfpdf');

	}


	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "form_bayar";		
		$data['title']	= "SIAKAD | Form Pembayaran";			
		$data['isi']    = "form_bayar";
		$data['judul1']	= "Form Pembayaran";			
		$data['judul2']	= "";			
		$data['judul1']	= "Form Pembayaran";
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();			
		$this->template($page, $data);	
	}

	public function cari_nisn(){
		$nisn	= $this->input->post('nisn');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$nisn_re = $this->db->query("SELECT tabel_siswa.nama_lengkap,tabel_siswa.id_jenjang as kode, tabel_siswakelas.id_penempatan,
					tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran FROM tabel_siswa INNER JOIN tabel_jenjang
					ON tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang
					INNER JOIN tabel_siswakelas ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
					INNER JOIN tabel_tahunajaran ON tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta
					INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
					WHERE tabel_siswa.id_siswa = '$nisn' AND tabel_siswakelas.status = 'aktif'
					AND tabel_siswa.id_cabang = '$id_cabang'");		
		}else{
			$nisn_re = $this->m_siswa->get_nisn($nisn);							
		}
		
		$row 	= $nisn_re->row();	
		if($nisn_re->num_rows()>0){
			echo "nihil"."|".$row->nama_lengkap."|".$row->jenjang."|".$row->kode."|".$row->kelas."|".$row->tahun_ajaran."|".$row->id_penempatan;
		}else{
			echo "Maaf, data tidak ditemukan";
		}										
	}
	public function cetak(){
		$tgl2 = date('d F Y');
		$id_siswa	= $this->input->get('id');
		$tanggal	= $this->input->get('tanggal');		
		$dt_cetak = $this->m_form_bayar->get_cetak($id_siswa,$tanggal);
		$d = $dt_cetak->row();						
		if($dt_cetak->num_rows()>0)	{


			$pdf = new FPDF('p','mm','A4');
	        $pdf->AddPage();
	       // head
	       $pdf->SetFont('TIMES','',12);
	       $pdf->Cell(190, 5, 'SARIPUTRA JAMBI', 0, 1, 'C');
	       $pdf->SetFont('TIMES','',10);
	       $pdf->Cell(190, 5, 'JL. Pangeran Diponegoro no. 55 Kel. Sulanjana Kota Jambi', 0, 1, 'C');
	       $pdf->Cell(190, 5, 'Telp.+62-0741-23684', 0, 1, 'C');
	       $pdf->Cell(190, 5, 'E-mail : Service@sariputra-jambi.com', 0, 1, 'C');
	       $pdf->Line(11, 31, 200, 31);
	       
	       $pdf->Image(base_url().'/assets/web/images/ico/logo.png', 10, 10, 20);
	       
	       $pdf->SetFont('TIMES','B',12);
	       $pdf->Cell(1,2,'',0,1);
	       $pdf->Cell(190, 5, 'BUKTI PEMBAYARAN', 0, 1, 'C');
	       $pdf->Cell(2, 2,'',0,1);
	       $pdf->SetFont('TIMES','B',9);
	       // buat tabel disini
	       $pdf->SetFont('TIMES','B',9);
	       
	       $pdf->Cell(30,5,'KELAS',0,0);
	       $pdf->Cell(20,5,' : '.$d->kelas,0,1);
	       $pdf->Cell(30,5,'JENJANG',0,0);
	       $pdf->Cell(20,5,' : '.  strtoupper($d->jenjang),0,1);
	       $pdf->Cell(30,5,'TAHUN AJARAN',0,0);
	       $pdf->Cell(20,5,' : '.  strtoupper($d->tahun_ajaran),0,1); 
	       $pdf->Cell(30,5,'NO.INDUK',0,0);
	       $pdf->Cell(20,5,' : '.$d->id_siswa,0,1);  
	       $pdf->Cell(30,5,'NAMA SISWA',0,0);
	       $pdf->Cell(20,5,' : '.$d->nama_lengkap,0,1);       
	       
	       // kasi jarak
	       $pdf->Cell(3,2,'',0,1);
	       
	       $pdf->Cell(7, 5, 'No', 1, 0);
	       $pdf->Cell(50, 5, 'Jenis Pembayaran', 1, 0);
	       $pdf->Cell(30, 5, 'Nominal', 1, 0);
	       $pdf->Cell(35, 5, 'Tgl.Bayar', 1, 0);
	       $pdf->Cell(45, 5, 'Keterangan', 1, 1);       
	   
	       $pdf->SetFont('times','',9);
	       $i=1;
	       $jum = 0;
	       foreach ($dt_cetak->result() as $r)
	       {
	       		$tgl = date("d F Y", strtotime($r->tgl_bayar));
	       		$a = $r->nominal;
	       		$uang = number_format($a, 0, ',', '.');

	            $pdf->Cell(7, 5, $i, 1, 0);
	            $pdf->Cell(50, 5, $r->biaya, 1, 0);
	            $pdf->Cell(30, 5, "Rp. ".$uang, 1, 0);
	            $pdf->Cell(35, 5, $tgl, 1, 0);    
	            $pdf->Cell(45, 5, $r->ket, 1, 1);   
	            $i++; 
	            $jum = $jum + $a;
	            $j = number_format($jum, 0, ',', '.');
	       }

	       $pdf->Cell(57, 5, 'Total', 1, 0,'R');
	       $pdf->Cell(30, 5, "Rp. ".$j, 1, 0);
	       $pdf->Cell(35, 5, "", 1, 0);    
	       $pdf->Cell(45, 5, "", 1, 1); 
	       
	       
	       // tanda tangan
	       
	       $pdf->Cell(10, 5, '', 0, 1);
	       $pdf->Cell(10, 15, '', 0, 0);
	       $pdf->Cell(10, 5, 'Jambi,'.$tgl2, 0, 1);
	       $pdf->Cell(10, 5, '', 0, 0);
	       $pdf->Cell(10, 5, 'Bagian Akademik,', 0, 1);
	       $pdf->Cell(10, 10, '', 0, 0);
	       $pdf->Cell(10, 10, '', 0, 1);
	       $pdf->Cell(10, 5, '', 0, 0);
	       $pdf->Cell(10, 5, '_________________________', 0, 0);
	       
	       $pdf->Output(); 
	    }else{
	    	?>
				<script type="text/javascript">
					alert("Data tidak ditemukan");	
					history.go(-1);		
				</script>
			<?php			
	    }
	}
	public function t_riwayat(){
		$id = $this->input->post('id');
		$data['dt_riwayat'] = $this->m_form_bayar->get_riwayat($id);
		$this->load->view('panel/t_riwayat',$data);
	}
	public function cek_bi(){
		$id_biaya	= $this->input->post('id_biaya');		
		$biaya_re 	= $this->m_biaya->get_one($id_biaya);						
		$row 		= $biaya_re->row();									
		echo $row->nominal;
	}
	public function cari_biaya(){
		$jenjang		= $this->input->post('id_jenjang');		
		$jenjang_re 	= $this->m_biaya->get_biaya($jenjang);								
		$data .= "<option value=''>Pilih Jenis Pembayaran</option>";
		foreach ($jenjang_re->result() as $row) {
			$data .= "<option value='$row->id_biaya'>$row->biaya</option>\n";
		}						
		echo $data;
	}
	public function add()
	{		
		$page			= "form_bayar";		
		$data['title']	= "SIAKAD | Form Pembayaran";			
		$data['judul1']	= "Form Pembayaran";		
		$data['isi']    = "form_bayar";	
		$data['judul2']	= "";			
		$data['judul1']	= "Form Pembayaran";
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{	
		$id_biaya 			= $this->input->post('id_biaya');			
		$tgl_bayar			= $this->input->post('tanggal');						
		$id_penempatan		= $this->input->post('id_penempatan');			
		$data['id_biaya'] 	= $this->input->post('id_biaya');			
		$data['id_penempatan']	= $this->input->post('id_penempatan');			
		$data['tgl_bayar']	= $this->input->post('tanggal');						
		$data['ket']		= $this->input->post('uraian');						

		$tahun = date("Y", strtotime($tgl_bayar));
		$bulan = date("m", strtotime($tgl_bayar));
		$cek = $this->m_biaya->get_one($id_biaya)->row();
		$frekuensi = $cek->frekuensi;

		if($frekuensi=='Perbulan'){
			$cari 	= $this->m_form_bayar->get_riwayat_bln($id_penempatan,$bulan,$id_biaya,$tahun);
			$ambil 	= $cari->row();
			if($cari->num_rows()==0){
				$this->m_form_bayar->tambah($data);
				echo "nihil";	
			}else{
				echo "Anda telah membayar ".$ambil->biaya." untuk bulan ".$bulan;	
			}
		}elseif($frekuensi=='Persemester'){
			$cari 	= $this->m_form_bayar->get_riwayat_sms($id_penempatan,$id_biaya,$tahun);
			$ambil 	= $cari->row();
			if($cari->num_rows()<2){
				$this->m_form_bayar->tambah($data);
				echo "nihil";	
			}else{
				echo "Anda telah membayar ".$ambil->biaya." untuk tahun ".$tahun;	
			}
		}elseif($frekuensi=='Pertahun'){
			$cari 	= $this->m_form_bayar->get_riwayat_sms($id_penempatan,$id_biaya,$tahun);
			$ambil 	= $cari->row();
			if($cari->num_rows()==0){
				$this->m_form_bayar->tambah($data);
				echo "nihil";	
			}else{
				echo "Anda telah membayar ".$ambil->biaya." untuk tahun ".$tahun;	
			}
		}elseif($frekuensi=='Satu Kali'){
			$cari 	= $this->m_form_bayar->get_riwayat_one($id_penempatan,$id_biaya);
			$ambil 	= $cari->row();
			if($cari->num_rows()==0){
				$this->m_form_bayar->tambah($data);
				echo "nihil";	
			}else{
				echo "Anda telah membayar ".$ambil->biaya;	
			}
		}

		
	}
	
	public function delete_riwayat(){
		$id_bayar 		= $this->input->post('id_bayar');		
		$this->m_form_bayar->hapus_riwayat($id_bayar);			
		echo "nihil";
	}

}
