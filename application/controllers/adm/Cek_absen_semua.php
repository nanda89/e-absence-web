<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_absen_semua extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_tahunajaran');
		$this->load->model('m_pesan');		
		$this->load->model('m_jenjang');
		$this->load->model('m_semua');		

		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "cek_absen_semua";		
		$data['title']	= "Cek Absen Semua";			
		$data['isi']    = "cek_absen_semua";
		$data['judul1']	= "Cek Absen Semua";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$this->template($page, $data);	
	}
	public function desktop()
	{		
		$page						= "cek_absen_semua_desktop";		
		$data['title']	= "Cek Absen Semua";			
		$data['isi']    = "cek_absen_semua";
		$data['judul1']	= "Cek Absen Semua";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$this->load->view('t_panel/header2',$data);		
		$this->load->view("panel/".$page);	
		$this->load->view('t_panel/footer2');				
	}

	
}
