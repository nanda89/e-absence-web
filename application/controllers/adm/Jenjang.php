<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenjang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_jenjang');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_pesan');
        $this->load->model('m_guru');
        $this->load->model('m_siswakelas');
        $this->load->model('m_kelas');

        //===== Load Library =====

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "jenjang";
        $data['isi'] = "kelas";
        $data['title'] = "Jenjang & Kelas";
        $data['judul1'] = "Jenjang & Kelas";
        $data['judul2'] = "";
        $data['judul1'] = "Jenjang & Kelas";
        $data['set'] = "view";
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_tahun_ajaran'] = $this->m_tahunajaran->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_jen'] = $this->m_jenjang->get_all();
        $data['dt_g'] = $this->m_guru->get_all();
        $this->template($page, $data);
    }

    public function add()
    {
        $page = "tahun_ajaran";
        $data['title'] = "Tahun Ajaran";
        $data['judul1'] = "Tahun Ajaran";
        $data['judul2'] = "";
        $data['judul1'] = "Tahun Ajaran";
        $data['set'] = "insert";
        $this->template($page, $data);
    }

    public function kirim_json()
    {
        //load the Curl library

        $url = 'http://localhost/web_absence_server/assets/json_file/tes.php/';
        $ch = curl_init($url);
        $sql = $this->m_jenjang->get_all();
        foreach ($sql->result() as $k) {
            $jenjang = $k->jenjang;
            $jam_pulang = $k->jam_pulang;
            $jam_masuk = $k->jam_masuk;
            $myArray[] = array('isi' => array("jenjang" => $jenjang, "jam_pulang" => $jam_pulang, "jam_masuk" => $jam_masuk));
        }
        $payload = json_encode(array("customer" => $myArray));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        # Print response.
        echo "<pre>$result</pre>";

    }

    public function save()
    {
        if ($this->input->post('save') == 'save') {
            $data['jenjang'] = $this->input->post('jenjang');
            $data['jam_masuk'] = $this->input->post('jam_masuk');
            $data['jam_pulang'] = $this->input->post('jam_pulang');
            $this->m_jenjang->tambah($data);
            $_SESSION['pesan2'] = "Berhasil tersimpan!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";

        }
    }

    public function save_kelas()
    {
        if ($this->input->post('save') == 'save') {
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['id_ta'] = $this->input->post('id_ta');
            $data['id_guru'] = $this->input->post('id_guru');
            $data['kelas'] = $this->input->post('kelas');
            $this->m_jenjang->tambah_kelas($data);
            $_SESSION['pesan2'] = "Berhasil tersimpan!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        }
    }

    public function duplicate_kelas()
    {

        $idJenjangSumber = $this->input->post('id_jenjang_sumber');
        $idTASumber = $this->input->post('id_ta_sumber');
        $idJenangTujuan = $this->input->post('id_jenjang_tujuan');
        $idTATujuan = $this->input->post('id_ta_tujuan');
        try {
            if ($idJenjangSumber == $idJenangTujuan && $idTASumber == $idTATujuan) {
                throw new Exception("Input data sumber dan tujuan tidak boleh sama");
            }
            $countKelasByTAAndJenjang = $this->m_siswakelas->countKelasByTAAndJenjang($idTATujuan, $idJenangTujuan);
            if ($countKelasByTAAndJenjang > 0) {
                throw new Exception("Gagal dplikasi data, Data sudah ada sebelumnya");
            }
            $countDataSumber = $this->m_siswakelas->countKelasByTAAndJenjang($idTASumber, $idJenjangSumber);
            if ($countDataSumber == 0) {
                throw new Exception("Gagal dplikasi data, Data sumber tidak ditemukan");
            }

            $query = $this->m_kelas->getKelasByJenjangAndTa($idJenjangSumber, $idTASumber);

            $this->db->trans_begin();

            foreach ($query->result() as $kelas) {
                $data['id_jenjang'] = $idJenangTujuan;
                $data['id_ta'] = $idTATujuan;
                $data['id_guru'] = $kelas->id_guru;
                $data['kelas'] = $kelas->kelas;
                $this->m_jenjang->tambah_kelas($data);
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                throw new Exception("Internal server error");
            } else {
                $this->db->trans_commit();
            }

            $_SESSION['pesan2'] = "Data Berhasil ditambahkan!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";

        } catch (Exception $e) {
            // var_dump();
            $pesan = $e->getMessage();
            $_SESSION['pesan2'] = "Data gagal ditambahkan ($pesan)";
            $_SESSION['tipe2'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";

        }

    }

    public function ajax_bulk_delete()
    {
        $tabel = "tabel_kelas";
        $pk = "id_kelas";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        // echo json_encode(array("status" => TRUE));
        $_SESSION['pesan'] = "Berhasil dihapus!";
        $_SESSION['tipe'] = "info";
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";

    }
    public function cari()
    {
        $id = $this->input->post('id');
        $jenjang = $this->m_jenjang->get_one($id);
        $row = $jenjang->row();
        if ($jenjang->num_rows() > 0) {
            echo $row->id_jenjang . "|" . $row->jenjang;
        }
    }

    public function cari_kelas()
    {
        $id = $this->input->post('id');
        $kelas = $this->m_jenjang->get_one_kelas($id);
        $row = $kelas->row();
        if ($kelas->num_rows() > 0) {
            echo $row->id_kelas . "|" . $row->kelas . "|" . $row->jenjang . "|" . $row->nama . "|" . $row->id_jenjang . "|" . $row->id_guru;
        }
    }

    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'edit') {
            $data['jenjang'] = $this->input->post('jenjang');
            $data['jam_masuk'] = $this->input->post('jam_masuk');
            $data['jam_pulang'] = $this->input->post('jam_pulang');
            $this->m_jenjang->edit($id, $data);
            $_SESSION['pesan'] = "Berhasil tersimpan!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_jenjang->hapus($id);
            $_SESSION['pesan'] = "Berhasil dihapus!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        }
    }

    public function process_kelas()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'edit') {
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $id = $this->input->post('id_kelas');
            $data['id_guru'] = $this->input->post('id_guru');
            $data['kelas'] = $this->input->post('kelas');
            $this->m_jenjang->edit_kelas($id, $data);
            $_SESSION['pesan2'] = "Berhasil tersimpan!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_jenjang->hapus_kelas($id);
            $_SESSION['pesan2'] = "Berhasil dihapus!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jenjang'>";
        }
    }

}
