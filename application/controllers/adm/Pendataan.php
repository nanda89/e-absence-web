<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendataan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_siswa');
        $this->load->model('m_jenjang');
        $this->load->model('m_pesan');
        $this->load->model('m_penempatan');
        $this->load->model('m_tahunajaran');
        //===== Load Library =====
        $this->load->library('upload');
        $this->load->library('cfpdf');
        $this->load->library('excel');
        $this->load->library('csvimport');

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "pendataan";
        $data['isi'] = "pendataan";
        $data['title'] = "Data Siswa Aktif";
        $data['judul1'] = "Data Siswa Aktif";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();

        // $data['dt_penempatan'] = $this->m_penempatan->get_all_kelas();

        $this->template($page, $data);
    }
    public function excel_siswa_filter()
    {

        $id_ta = $this->input->get('id_ta');
        $id_kelas = $this->input->get('id_kelas');
        $dt_penempatan = $this->m_siswa->get_all();
        $data['dt_penempatan'] = $this->m_penempatan->get_siswa($id_ta, $id_kelas);
        $this->load->view('panel/excel_siswa_filter', $data);
    }
    public function ajax_bulk_delete()
    {
        $tabel = "tabel_siswakelas";
        $pk = "id_penempatan";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        echo json_encode(array("status" => true));
    }
    public function cetak_pdf()
    {
        $tgl = date('d-M-Y');
        $id_ta = $this->input->get('id_ta');
        $id_kelas = $this->input->get('id_kelas');
        $dt_penempatan = $this->m_penempatan->get_siswa($id_ta, $id_kelas);
        $d = $dt_penempatan->row();

        $pdf = new FPDF('p', 'mm', 'A4');
        $pdf->AddPage();
        // head
        $pdf->SetFont('TIMES', '', 12);
        $pdf->Cell(190, 5, 'SARIPUTRA JAMBI', 0, 1, 'C');
        $pdf->SetFont('TIMES', '', 10);
        $pdf->Cell(190, 5, 'JL. Pangeran Diponegoro no. 55 Kel. Sulanjana Kota Jambi', 0, 1, 'C');
        $pdf->Cell(190, 5, 'Telp.+62-0741-23684', 0, 1, 'C');
        $pdf->Cell(190, 5, 'E-mail : Service@sariputra-jambi.com', 0, 1, 'C');
        $pdf->Line(11, 31, 200, 31);

        $pdf->Image(base_url() . '/assets/web/images/ico/logo.png', 10, 10, 20);

        $pdf->SetFont('TIMES', 'B', 12);
        $pdf->Cell(1, 2, '', 0, 1);
        $pdf->Cell(190, 5, 'LAPORAN DATA SISWA', 0, 1, 'C');
        $pdf->Cell(2, 2, '', 0, 1);
        $pdf->SetFont('TIMES', 'B', 9);
        // buat tabel disini
        $pdf->SetFont('TIMES', 'B', 9);

        $pdf->Cell(30, 5, 'KELAS', 0, 0);
        $pdf->Cell(20, 5, ' : ' . $d->kelas, 0, 1);
        $pdf->Cell(30, 5, 'JENJANG', 0, 0);
        $pdf->Cell(20, 5, ' : ' . strtoupper($d->jenjang), 0, 1);
        $pdf->Cell(30, 5, 'TAHUN AJARAN', 0, 0);
        $pdf->Cell(20, 5, ' : ' . strtoupper($d->tahun_ajaran), 0, 1);
        $pdf->Cell(30, 5, 'WALI KELAS', 0, 0);
        $pdf->Cell(20, 5, ' : ' . strtoupper($d->nama), 0, 1);

        // kasi jarak
        $pdf->Cell(3, 2, '', 0, 1);

        $pdf->Cell(7, 5, 'No', 1, 0);
        $pdf->Cell(30, 5, 'No.Induk', 1, 0);
        $pdf->Cell(30, 5, 'NISN', 1, 0);
        $pdf->Cell(65, 5, 'Nama Lengkap', 1, 0);
        $pdf->Cell(55, 5, 'Tempat,Tgl.Lahir', 1, 1);

        $pdf->SetFont('times', '', 9);
        $i = 1;
        foreach ($dt_penempatan->result() as $r) {
            $pdf->Cell(7, 5, $i, 1, 0);
            $pdf->Cell(30, 5, strtoupper($r->id_siswa), 1, 0);
            $pdf->Cell(30, 5, strtoupper($r->nisn), 1, 0);
            $pdf->Cell(65, 5, $r->nama_lengkap, 1, 0);
            $pdf->Cell(55, 5, $r->tempat_lahir . "," . $r->tgl_lahir, 1, 1);
            $i++;
        }

        // tanda tangan
        /*
        $pdf->Cell(95, 5, '', 0, 1);
        $pdf->Cell(95, 15, '', 0, 0);
        $pdf->Cell(25, 5, 'Jambi,'.$tgl, 0, 1);
        $pdf->Cell(95, 5, '', 0, 0);
        $pdf->Cell(25, 5, 'Kepala Sekolah,', 0, 1);
        $pdf->Cell(95, 10, '', 0, 0);
        $pdf->Cell(25, 10, '', 0, 1);
        $pdf->Cell(95, 5, '', 0, 0);
        $pdf->Cell(25, 5, 'Dendin Supriadi,S.Pd.M.T,', 0, 0);
         */
        $pdf->Output();
    }

    public function search_by_filter()
    {
        $id_ta = $_POST['id_ta'];
        $id_kelas = $_POST['id_kelas'];
        $id_jenjang = $_POST['id_jenjang'];

        $response = $this->m_penempatan->get_siswa_by_filter($id_ta, $id_kelas, $id_jenjang)->result_array();

        echo json_encode($response);
    }

    public function filter()
    {
        $data['judul1'] = "Data Siswa Aktif";
        $data['judul2'] = "";
        $data['title'] = "Data Siswa Aktif";
        $id_ta = $this->input->get('id_tahun');
        $id_kelas = $this->input->get('id_kelas');
        $data['isi'] = "pendataan";
        $data['set'] = "view";
        $data['dt_penempatan'] = $this->m_penempatan->get_siswa($id_ta, $id_kelas);
        $page = "t_pendataan";
        $this->template($page, $data);
    }
    public function set_jenjang()
    {
        $id = $this->input->get('id_jenjang');
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/penempatan/set/$id'>";
    }
    public function get_kelas()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_kelas = $this->m_penempatan->get_one_kelas($id_jenjang);

        $data = "<option value=''>Pilih Kelas</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }
    public function t_pendataan()
    {
        $id_tahun = $this->input->post('id_tahun');
        $id_kelas = $this->input->post('id_kelas');
        $data['dt_penempatan'] = $this->m_penempatan->get_pendataan($id_tahun, $id_kelas);
        $this->load->view('panel/t_pendataan', $data);
    }
    public function save()
    {
        $data['id_ta'] = $this->input->post('id_tahun');
        $data['angkatan'] = $this->input->post('angkatan');
        $data['id_kelas'] = $this->input->post('id_kelas');
        $data['id_siswa'] = $this->input->post('id_siswa');
        $detail['status'] = 'aktif';
        $id_siswa = $this->input->post('id_siswa');
        $cek = $this->m_penempatan->cek_siswa($id_siswa);
        if ($cek->num_rows() > 0) {
            echo "nothing";
        } else {
            $this->m_penempatan->tambah($data);
            $this->m_siswa->edit($id_siswa, $detail);
            echo "nihil";
        }
    }
    public function delete_penempatan()
    {
        $id = $this->input->post('id_penempatan');
        $this->m_penempatan->hapus($id);
        echo "nihil";
    }
    public function export_all()
    {

        $data['dt_penempatan'] = $this->m_penempatan->get_all_kelas();
        $this->load->view('panel/excel_siswa_all', $data);
    }
    public function import_all()
    {
        $page = "pendataan";
        $data['isi'] = "pendataan";
        $data['title'] = "Data Siswa Aktif";
        $data['judul1'] = "Data Siswa Aktif";
        $data['judul2'] = "";
        $data['set'] = "import";
        $this->template($page, $data);
    }
    public function importcsv()
    {
        $page = "pendataan";
        $data['isi'] = "pendataan";
        $data['title'] = "Data Siswa Aktif";
        $data['judul1'] = "Data Siswa Aktif";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['error'] = ''; //initialize image upload error array to empty

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        //$this->load->library('upload', $config);
        $this->upload->initialize($config);

        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $_SESSION['pesan'] = $this->upload->display_errors();
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/pendataan'>";
        } else {
            $file_data = $this->upload->data();
            $file_path = './uploads/' . $file_data['file_name'];

            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $ro) {
                    $id_siswa = $ro['id_siswa'];
                    $insert_da = array(
                        'id_ta' => $ro['id_ta'],
                        'id_kelas' => $ro['id_kelas'],
                    );
                    $this->m_siswa->edit_csv_p($id_siswa, $insert_da);
                }
                $_SESSION['pesan'] = "Data berhasil diimport";
                $_SESSION['tipe'] = "success";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/pendataan'>";
            } else {
                $_SESSION['pesan'] = "Error Occured";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/pendataan'>";
            }

        }
    }

    public function import_filter()
    {
        $page = "t_pendataan";
        $data['isi'] = "pendataan";
        $data['title'] = "Data Siswa Aktif";
        $data['judul1'] = "Data Siswa Aktif";
        $data['judul2'] = "";
        $data['set'] = "import";
        $this->template($page, $data);
    }
}
