l<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Edit_penjadwalan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_konfigurasi');
        $this->load->model('m_penjadwalan');
        $this->load->model('m_pesan');
        $this->load->model('m_jenjang');
        //===== Load Library =====

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'ubah') {
            $page = "penjadwalan";
            $data['one_post'] = $this->m_penjadwalan->get_one($id);
            $data['dt_konfigurasi'] = $this->m_konfigurasi->get_all();
            $data['isi'] = "penjadwalan";
            $data['title'] = "SIAKAD | Set Jadwal Pendaftaran";
            $data['judul1'] = "Set Jadwal Pendaftaran";
            $data['judul2'] = "";
            $data['judul1'] = "Set Jadwal Pendaftaran";
            $data['set'] = "edit";
            $this->template($page, $data);
        } elseif ($set == 'edit') {

            ?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");
					</script>
			<?php
echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/penjadwalan'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_penjadwalan->hapus($id);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/penjadwalan'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/penjadwalan'>";
        }
    }

}
