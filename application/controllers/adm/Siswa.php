<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_siswa');
        $this->load->model('m_siswakelas');
        $this->load->model('m_jenjang');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_penempatan');
        $this->load->model('m_pesan');
        $this->load->model('m_semua');
        $this->load->model('m_setting');

        //===== Load Library =====
        $this->load->library('upload');
        $this->load->library('csvimport');
    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "siswa";
        $data['isi'] = "siswa";
        $data['title'] = "Siswa";
        $data['judul1'] = "Siswa";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        // $this->load->view('siswa');
        $this->template($page, $data);
    }

    public function import()
    {
        $page = "siswa";
        $data['judul1'] = "Siswa";
        $data['judul2'] = "";
        $data['isi'] = "siswa";
        $data['title'] = "Siswa";
        $data['set'] = "import";
        $this->template($page, $data);
    }
    public function ajax_bulk_delete()
    {
        $tabel = "tabel_siswa";
        $pk = "id_siswa";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        echo json_encode(array("status" => true));
    }
    public function importcsv()
    {
        $page = "siswa";
        $tabel = "tabel_siswa";
        $data['isi'] = "siswa";
        $data['title'] = "Siswa";
        $data['set'] = "view";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['error'] = ''; //initialize image upload error array to empty

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        //$this->load->library('upload', $config);
        $this->upload->initialize($config);

        $errorData = array();

        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $_SESSION['pesan'] = $this->upload->display_errors();
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
        } else {
            $file_data = $this->upload->data();
            $file_path = './uploads/' . $file_data['file_name'];

            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);

                $listIdKelas = array();
                $errorBaris = 2;
                //cek kolom tidak boleh kosong : id_siswa,id_jenjang,id_ta,id_kelas,nisn
                foreach ($csv_array as $cek) {
                    // $idSiswa     = $cek['id_siswa'];

                    $idJenjang = $cek['id_jenjang'];
                    $idTA = $cek['id_ta'];
                    $idKelas = $cek['id_kelas'];
                    $nisn = $cek['nisn'];

                    if ($idJenjang == '') {
                        $errorData[] = array(
                            'pesan' => 'pada baris ' . $errorBaris . ' kolom id_jenjang tidak boleh kosong',
                        );
                    } else {
                        $cekJenjang = $this->m_jenjang->get_one($idJenjang);
                        if ($cekJenjang->num_rows() == 0) {
                            $errorData[] = array(
                                'pesan' => 'pada baris ' . $errorBaris . ' pastikan id_jenjang sesuai',
                            );
                        }
                    }
                    if ($idTA == '') {
                        $errorData[] = array(
                            'pesan' => 'pada baris ' . $errorBaris . ' kolom id_ta tidak boleh kosong',
                        );
                    } else {
                        $cekTA = $this->m_tahunajaran->get_one($idTA);
                        if ($cekTA->num_rows() == 0) {
                            $errorData[] = array(
                                'pesan' => 'pada baris ' . $errorBaris . ' pastikan id_ta sesuai',
                            );
                        }
                    }
                    if ($idKelas == '') {
                        $errorData[] = array(
                            'pesan' => 'pada baris ' . $errorBaris . ' kolom id_kelas tidak boleh kosong',
                        );
                    } else {
                        $cekKelas = $this->m_jenjang->get_kelas_by_id($idKelas);
                        if ($cekKelas->num_rows() == 0) {
                            $errorData[] = array(
                                'pesan' => 'pada baris ' . $errorBaris . ' pastikan id_kelas sesuai',
                            );
                        } else {
                            //insert id kelas into array and make sure not duplicate the id kelas
                            $idKelas = $cekKelas->row();
                            if (!in_array($idKelas->id_kelas, $listIdKelas)) {
                                array_push($listIdKelas, $idKelas->id_kelas);
                            }

                        }
                    }
                    if ($nisn == '') {
                        $errorData[] = array(
                            'pesan' => 'pada baris ' . $errorBaris . ' kolom nisn tidak boleh kosong',
                        );
                    }

                    $errorBaris = $errorBaris + 1;
                }

                if (count($errorData) > 0) {
                    $pesan = '';
                    foreach ($errorData as $err) {
                        $pesan .= $err['pesan'] . "<br/>";
                    }
                    $_SESSION['pesan'] = "Error data gagal disimpan, harap periksa kembali data berikut : <br/>" . $pesan;
                    $_SESSION['tipe'] = "danger";
                    echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

                } else {
                    foreach ($csv_array as $row) {
                        $no_hp = $row['no_hp'];
                        $p = substr($no_hp, 0, 2);
                        $p2 = substr($no_hp, 0, 1);
                        if ($p == '08') {
                            $hasil = substr_replace($no_hp, "+628", 0, 2);
                        } else {
                            $hasil = $no_hp;
                        }

                        if ($p2 == '8') {
                            $hasil = substr_replace($no_hp, "+628", 0, 1);
                        } elseif ($p2 == '6') {
                            $hasil = substr_replace($no_hp, "+6", 0, 1);
                        }

                        $curDate = date('d-m-Y');
                        $tglDaftar = $row['tgl_daftar'] == '' ? $curDate : $row['tgl_daftar'];

                        $jenisKelamin = $row['jenis_kelamin'] != '' ? $row['jenis_kelamin'] : 'laki-laki';
                        $sex = strtolower($jenisKelamin);
                        if (strpos($sex, 'laki') !== false || strpos($sex, 'pria') !== false) {
                            $sex = 'laki-laki';
                        } else {
                            $sex = 'perempuan';
                        }

                        $id = $row['id_siswa'];
                        $nisn = $row['nisn'];

                        $insert_data = array(
                            // 'id_siswa'=>$id,
                            'id_jenjang' => $row['id_jenjang'],
                            'nama_lengkap' => strtoupper($row['nama_lengkap']),
                            'nisn' => $nisn,
                            'jenis_kelamin' => $sex,
                            'tempat_lahir' => $row['tempat_lahir'],
                            'tgl_lahir' => $row['tgl_lahir'],
                            'agama' => $row['agama'],
                            'alamat' => $row['alamat'],
                            'no_hp' => $hasil,
                            'email' => $row['email'],
                            'asal_sekolah' => $row['asal_sekolah'],
                            'ket_asal' => $row['ket_asal'],
                            'nama_ayah' => $row['nama_ayah'],
                            'nama_ibu' => $row['nama_ibu'],
                            'tgl_daftar' => $tglDaftar,
                        );

                        //cek id_siswa atau nisn apakah sudah ada sebelumnya
                        $cekDataSiswa = $this->m_siswa->cek_by_nisn($nisn);

                        if ($cekDataSiswa->num_rows() == 0) {
                            $response_id_siswa = $this->m_siswa->insert_csv_by_return($insert_data);

                            $insert_da = array(
                                'id_siswa' => $response_id_siswa,
                                'id_ta' => $row['id_ta'],
                                'id_kelas' => $row['id_kelas'],
                            );
                            $this->m_siswa->insert_csv_p($insert_da);
                        } else {
                            $id_siswa = $cekDataSiswa->row();
                            $this->m_siswa->edit_by_id_or_nisn($id_siswa->id_siswa, $nisn, $insert_data);
                        }

                    }

                    $setting = $this->m_setting->get_one();
                    $setting = $setting->row();
                    $kapasitas = $setting->kapasitas_kelas;

                    foreach ($listIdKelas as $idkel) {
                        //check total siswa pada setiap kelas,jika melebihi
                        //kapasitas maka hapus siswa yang terakhir dan informasikan

                        $totalSiswa = $this->m_siswakelas->countKelas($idkel);

                        if (intval($totalSiswa) > intval($kapasitas)) {

                            $dataSiswaKelas = $this->m_siswakelas->getDataByIdKelas($idkel);
                            $no = 0;
                            foreach ($dataSiswaKelas->result_array() as $dtSisKel) {
                                $no++;
                                if (intval($no) <= intval($kapasitas)) {
                                    continue;
                                }
                                //hapus pada tabel siswakelas
                                $this->m_siswakelas->hapus_siswa($dtSisKel['id_siswa']);

                                $siswa = $this->m_siswa->get_one($dtSisKel['id_siswa']);
                                $siswa = $siswa->row();

                                $filter = array(
                                    'id_kelas' => $idkel,
                                );
                                $dataKelas = $this->m_semua->kondisi('tabel_kelas', $filter);
                                $dtKelas = $dataKelas->row();
                                $errorData[] = array(
                                    'pesan' => 'siswa dengan nama ' . $siswa->nama_lengkap . ' dihapus dari kelas ' . $dtKelas->kelas,
                                );

                            }

                            if (count($errorData) > 0) {
                                // var_dump("111111");
                                $pesan = '';
                                foreach ($errorData as $err) {
                                    $pesan .= $err['pesan'] . "<br/>";
                                }
                                $_SESSION['pesan'] = "Error melebihi kapasitas kelas : <br/>" . $pesan;
                                $_SESSION['tipe'] = "danger";
                                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

                            } else {
                                // var_dump("222222");
                                $_SESSION['pesan'] = "Data berhasil diimport 2";
                                $_SESSION['tipe'] = "success";
                                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

                            }

                        } else {
                            if (count($errorData) > 0) {
                                // var_dump("111111");
                                $pesan = '';
                                foreach ($errorData as $err) {
                                    $pesan .= $err['pesan'] . "<br/>";
                                }
                                $_SESSION['pesan'] = "Error melebihi kapasitas kelas : <br/>" . $pesan;
                                $_SESSION['tipe'] = "danger";
                                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

                            } else {
                                // var_dump("222222");
                                $_SESSION['pesan'] = "Data berhasil diimport 1";
                                $_SESSION['tipe'] = "success";
                                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

                            }

                        }

                    }

                }
            } else {
                $_SESSION['pesan'] = "Error Occured";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
            }

        }
    }

    public function download()
    {
        $this->load->view('panel/template');
    }
    public function get_kelas()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $id_ta = $this->input->post('id_ta');

        // var_dump("id_jenjang : " . $id_jenjang . " ,id_kelas : " . $id_ta);

        $dt_kelas = $this->m_jenjang->getKelasByJenjangAndTa($id_jenjang, $id_ta);
        $data = "<option value=''>Pilih Kelas</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }

    public function get_kelas_edit()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_kelas = $this->m_jenjang->get_kelas($id_jenjang);
        $data = "<option value=''>Pilih</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }
    public function add()
    {
        $page = "siswa";
        $data['title'] = "Siswa";
        $data['judul1'] = "Tambah Data Siswa";
        $data['judul2'] = "";
        $data['isi'] = "siswa";
        $data['set'] = "insert";
        $data['dt_siswa'] = $this->m_siswa->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $this->template($page, $data);
    }

    public function filter()
    {
        $data['judul2'] = "";
        $data['judul1'] = "Tambah Data Siswa";
        $data['title'] = "Siswa";
        $id_jenjang = $this->input->get('id_jenjang');
        $tahun = $this->input->get('tahun');
        $data['isi'] = "siswa";
        if ($tahun != "") {
            $data['dt_ta'] = $this->m_siswa->get_siswa($id_jenjang, $tahun);
        } else {
            $data['dt_ta'] = $this->m_siswa->get_siswa_b($id_jenjang);
        }

        $page = "t_siswa";
        $this->template($page, $data);
    }

    public function cari_id()
    {
        $jenjang = $this->input->post('jenjang');
        $th = date("y");
        $id_siswa = $this->m_siswa->get_end($jenjang);
        if ($id_siswa->num_rows() > 0) {
            $row = $id_siswa->row();
            $id = substr($row->id_siswa, 4, 4) + 1;
            if ($id < 10) {
                $kode = $th . $jenjang . '000' . $id;
            } elseif ($id > 9 && $id <= 99) {
                $kode = $th . $jenjang . '00' . $id;
            } elseif ($id > 99 && $id <= 999) {
                $kode = $th . $jenjang . '0' . $id;
            } elseif ($id > 999) {
                $kode = $th . $jenjang . $id;
            }
        } else {
            $kode = $th . $jenjang . '0001';
        }
        echo $kode;
    }

    public function save()
    {
        if ($this->input->post('save') == 'save') {
            //setting konfigurasi upload gambar
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('gambar')) {
                $gambar = "";
            } else {
                $gambar = $this->upload->file_name;
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['id_siswa'] = $this->input->post('id_siswa');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nisn'] = $this->input->post('nisn');
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $this->input->post('alamat');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['asal_sekolah'] = $this->input->post('asal_sekolah');
            $data['ket_asal'] = $this->input->post('ket_asal');
            $data['nama_ayah'] = $this->input->post('nama_ayah');
            $data['nama_ibu'] = $this->input->post('nama_ibu');
            $data['tgl_daftar'] = $this->input->post('tgl_daftar');
            $data['gambar'] = $gambar;

            $da['id_siswa'] = $this->input->post('id_siswa');
            $da['id_ta'] = $this->input->post('id_ta');
            $da['id_kelas'] = $this->input->post('id_kelas');

            $setting = $this->m_setting->get_one();
            $setting = $setting->row();
            $kapasitas = $setting->kapasitas_kelas;
            $totalSiswa = $this->m_siswakelas->countKelas($da['id_kelas']);

            if (intval($totalSiswa) > intval($kapasitas)) {

                $_SESSION['pesan'] = "Error : data gagal disimpan : <br/> melebihi kapasitas kelas";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

            } else {
                $this->m_siswa->tambah($data);
                $this->m_penempatan->tambah($da);

                $_SESSION['pesan'] = "Berhasil tersimpan!";
                $_SESSION['tipe'] = "info";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

            }

        }
    }

    public function process()
    {

        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'ubah') {
            $page = "edit_siswa";
            $data['one_siswa'] = $this->m_siswa->get_one($id);
            $data['dt_jenjang'] = $this->m_jenjang->get_all();
            $data['dt_ta'] = $this->m_tahunajaran->get_all();
            $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
            $data['title'] = "Siswa";
            $data['judul1'] = "Edit Data Siswa";
            $data['isi'] = "siswa";
            $data['judul2'] = "";
            $data['set'] = "edit";
            $this->template($page, $data);
        }
        //DETAIL DATA
        elseif ($set == 'detail') {
            $page = "detail_siswa";
            $data['one_siswa'] = $this->m_siswa->get_one($id);

            $data['title'] = "Siswa";
            $data['judul1'] = "Detail Data Siswa";
            $data['isi'] = "siswa";
            $data['judul2'] = "";
            $data['set'] = "detail";
            $this->template($page, $data);
        }
        //EDIT DATA KEGIATAN
        elseif ($set == 'edit') {
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $data['gambar'] = $this->upload->file_name;

                $one = $this->m_siswa->get_one($id);
                $row = $one->row();
                if ($row->gambar != '') {
                    unlink("assets/panel/images/" . $row->gambar); //Hapus Gambar
                }
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['id_siswa'] = $this->input->post('id_siswa');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nisn'] = $this->input->post('nisn');
            $data['nama_lengkap'] = $this->input->post('nama_lengkap');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['alamat'] = $this->input->post('alamat');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['asal_sekolah'] = $this->input->post('asal_sekolah');
            $data['ket_asal'] = $this->input->post('ket_asal');
            $data['nama_ayah'] = $this->input->post('nama_ayah');
            $data['nama_ibu'] = $this->input->post('nama_ibu');
            $data['tgl_daftar'] = $this->input->post('tgl_daftar');
            $this->m_siswa->edit($id, $data);

            $da['id_siswa'] = $this->input->post('id_siswa');
            $da['id_ta'] = $this->input->post('id_ta');
            $da['id_kelas'] = $this->input->post('id_kelas');

            // cek kapasitas kelas tujuan,jika melebihi maka ditolak
            $setting = $this->m_setting->get_one();
            $setting = $setting->row();
            $kapasitas = $setting->kapasitas_kelas;
            $totalSiswa = $this->m_siswakelas->countKelas($da['id_kelas']);

            // var_dump("totalSiswa : " . $totalSiswa . " dari kelas : " . $da['id_kelas'] . " sedangkan kapasitas " . $kapasitas);
            $cek = $this->db->query("SELECT * FROM tabel_siswakelas WHERE id_siswa = '$id'")->num_rows();
            if ($cek == 0) {
                if (intval($totalSiswa) > intval($kapasitas)) {

                    $_SESSION['pesan'] = "Error : proses edit kelas gagal : <br/> kelas tujuan melebihi kapasitas";
                    $_SESSION['tipe'] = "danger";

                } else {
                    $_SESSION['pesan'] = "Success : Data berhasil ditambahkan : <br/> data siswa berhasil ditambahkan";
                    $_SESSION['tipe'] = "info";

                    $this->m_penempatan->tambah($da);
                }

            } else {
                if (intval($totalSiswa) > intval($kapasitas)) {

                    $_SESSION['pesan'] = "Error : proses edit kelas gagal : <br/> kelas tujuan melebihi kapasitas";
                    $_SESSION['tipe'] = "danger";

                } else {
                    $this->m_penempatan->edit($id, $da);
                    $_SESSION['pesan'] = "Success : Data berhasil diubah : <br/> data siswa berhasil diubah";
                    $_SESSION['tipe'] = "info";

                }

            }
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";

        } elseif ($set == 'set_akun') {
            $data['username'] = $this->input->post('id');
            $data['password'] = md5("putra123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "siswa";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "siswa-pr2.png";
            } else {
                $data['avatar'] = "siswa-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->tambah($data);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
        } elseif ($set == 'set_default') {
            $username = $this->input->post('id');
            $data['username'] = $this->input->post('id');
            $data['password'] = md5("putra123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "siswa";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "siswa-pr2.png";
            } else {
                $data['avatar'] = "siswa-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->edit_akun($username, $data);
            ?>
			<script>alert("Berhasil set default");</script>
			<?php
echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_siswa->hapus($id);
            $this->m_siswakelas->hapus_siswa($id);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/siswa'>";
        }
    }

}
