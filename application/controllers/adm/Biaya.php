<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biaya extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_biaya');
		$this->load->model('m_jenjang');
		$this->load->model('m_pesan');		
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "biaya";		
		$data['title']	= "SIAKAD | Setup Biaya";			
		$data['isi']    = "biaya";
		$data['judul1']	= "Setup Biaya";			
		$data['judul2']	= "";			
		$data['judul1']	= "Setup Biaya";
		$data['set']	= "view";
		$data['dt_biaya']	= $this->m_biaya->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "biaya";		
		$data['title']	= "SIAKAD | Setup Biaya";			
		$data['judul1']	= "Setup Biaya";		
		$data['isi']    = "biaya";	
		$data['judul2']	= "";			
		$data['judul1']	= "Setup Biaya";
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['biaya']			= $this->input->post('biaya');			
			$data['id_jenjang']		= $this->input->post('id_jenjang');			
			$data['nominal']		= $this->input->post('nominal');						
			$data['uraian']			= $this->input->post('uraian');						
			$data['frekuensi']		= $this->input->post('frekuensi');						
			$this->m_biaya->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/biaya'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "biaya";		
			$data['one_post']= $this->m_biaya->get_one($id);
			$data['dt_jenjang']	= $this->m_jenjang->get_all();			
			$data['title']	= "SIAKAD | Setup Biaya";			
			$data['judul1']	= "Setup Biaya";			
			$data['isi']    = "biaya";
			$data['judul2']	= "";			
			$data['judul1']	= "Setup Biaya";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$data['biaya']			= $this->input->post('biaya');			
			$data['id_jenjang']		= $this->input->post('id_jenjang');			
			$data['nominal']		= $this->input->post('nominal');						
			$data['uraian']			= $this->input->post('uraian');						
			$data['frekuensi']		= $this->input->post('frekuensi');						
			$this->m_biaya->edit($id, $data);
			?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");			
					</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/biaya'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_biaya->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/biaya'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/biaya'>";
		}
	}

}
