<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		//$name = $this->session->userdata('nama');		
		if (!$this->session->userdata('nama'))
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_pesan');		
		$this->load->model('m_artikel');		
		$this->load->model('m_kategori');		
		$this->load->model('m_semua');		
		//===== Load Library =====
		$this->load->library('upload');

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "artikel";
		$data['isi']    = "artikel";		
		$data['title']	= "SIAKAD | Artikel";			
		$data['judul1']	= "Artikel";			
		$data['judul2']	= "";			
		$data['judul1']	= "Artikel";
		$data['set']	= "view";
		$data['dt_ka']	= $this->m_artikel->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "artikel";
		$data['isi']    = "artikel";				
		$data['title']	= "SIAKAD | Artikel";			
		$data['judul1']	= "Artikel";			
		$data['judul2']	= "";			
		$data['judul1']	= "Artikel";
		$data['set']	= "insert";	
		$data['dt_kategori'] = $this->m_artikel->get_all_kategori();
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			//setting konfigurasi upload gambar
			$config['upload_path'] 		= './assets/panel/images/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['max_size']			= '2000';
			$config['max_width']  		= '2000';
			$config['max_height']  		= '1024';
					
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('gambar')){
				$gambar="";
			}else{
				$gambar=$this->upload->file_name;
			} 


			$data['id_kategori']	= $this->input->post('id_kategori');			
			$data['judul']			= $this->input->post('judul');			
			$data['penulis']		= $this->input->post('penulis');			
			$data['isi']			= $this->input->post('isi');						
			$data['waktu']			= date('d-M-Y') ." at ". date('H:i:s');
			$data['gambar'] 		= $gambar;
			$data['dibaca'] 		= "0";
			$data['status']			= $this->input->post('status');
			$data['permalink'] 		= url_title($data['judul']);			
			$this->m_artikel->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "artikel";		
			$data['one_post']= $this->m_artikel->get_edit($id);
			$data['title']	= "SIAKAD | Artikel";			
			$data['judul1']	= "Artikel";	
			$data['isi']    = "artikel";				
			$data['judul2']	= "";			
			$data['judul1']	= "Artikel";
			$data['dt_kategori'] = $this->m_artikel->get_all_kategori();
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$config['upload_path'] 		= './assets/panel/images/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['max_size']			= '2000';
			$config['max_width']  		= '2000';
			$config['max_height']  		= '1024';
					
			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$data['gambar']=$this->upload->file_name;
				
				$one_artikel = $this->m_artikel->get_one($id);
				$row = $one_artikel->row();
				unlink("assets/panel/images/".$row->gambar); //Hapus Gambar
			}
			
			$data['id_kategori']	= $this->input->post('id_kategori');			
			$data['judul']			= $this->input->post('judul');			
			$data['penulis']		= $this->input->post('penulis');			
			$data['isi']			= $this->input->post('isi');						
			$data['status']			= $this->input->post('status');
			$data['permalink'] 		= url_title($data['judul']);		
			$this->m_artikel->edit($id, $data);
			?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");			
					</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_artikel->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}
		elseif ( $this->input->post('s_process') == 'publish' )
		{
			$data['status']	= "draft";			
			$this->m_artikel->edit($id, $data);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}
		elseif ( $this->input->post('s_process') == 'draft' )
		{
			$data['status']	= "publish";			
			$this->m_artikel->edit($id, $data);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/artikel'>";
		}
	}

}
