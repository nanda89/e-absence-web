<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absen_siswa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		# SESSION
		$name = $this->session->userdata('nama');
		if ($name == "") echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";

		date_default_timezone_set("Asia/Jakarta");
		$this->load->database();

		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');
		$this->load->model('m_jenjang');
		$this->load->model('m_mapel');
		$this->load->model('m_absen_server');
		$this->load->model('m_siswakelas');
		$this->load->model('m_jadwal');

		$this->load->library('upload');
		$this->load->helper('url');
	}

	protected function template($page, $data)
	{
		$this->load->view('t_panel/header', $data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");
		$this->load->view('t_panel/footer');
	}

	public function index()
	{
		$page = "absen_siswa";
		$tgl = date('d-m-Y');
		$name = $this->session->userdata('username');

		$data['isi'] = "absen_siswa";
		$data['title'] = "Absensi Siswa Harian";
		$data['judul1']	= "Absensi Siswa Harian";
		$data['judul2']	= "";
		$data['set'] = "view";
		$data['dt_wali_kelas'] = $this->m_siswakelas->dt_wali_kelas();
		$data['dt_jenjang'] = $this->m_jenjang->get_all();
		$data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
		$this->template($page, $data);
	}

	public function ajax_list()
	{
		$list = $this->m_absen_server->get_datatables();
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $customers) {
			$no++;
			$row = array();
			$row[] = "<input type='checkbox' class='data-check' value='$customers->id_absen_mapel'>";
			$row[] = $no;
			$row[] = $customers->nisn;
			$row[] = $customers->id_siswa;
			$row[] = $customers->nama_lengkap;
			$row[] = $customers->mapel;
			$row[] = $customers->kelas;
			$row[] = $customers->tgl;
			$row[] = $customers->absen;

			$data[] = $row;
		}

		echo json_encode(array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_absen_server->count_all(),
			"recordsFiltered" => $this->m_absen_server->count_filtered(),
			"data" => $data,
		));
	}

	public function ajax_bulk_delete()
	{
		$tabel = "tabel_absen_mapel";
		$pk = "id_absen_mapel";
		$list_id = $this->input->post('id');
		foreach ($list_id as $id) {
			$this->m_admin->delete($tabel, $pk, $id);
		}
		echo json_encode(array("status" => TRUE));
	}

	public function export_all()
	{
		$data['dt_wali_kelas'] = $this->m_siswakelas->dt_wali_kelas();
		$this->load->view('panel/excel_absen_export_all', $data);
	}

	public function filter()
	{
		$page = "absen_siswa";
		$data['isi'] = "absen_siswa";
		$data['title'] = "Absensi Siswa Harian";
		$data['judul1']	= "Absensi Siswa Harian";
		$data['judul2']	= "";
		$data['set'] = "filter";

		$id_jenjang = $this->input->get('id_jenjang');
		$id_kelas = $this->input->get('id_kelas');
		$tgl_awal = $this->input->get('tgl_awal');
		$tgl_akhir = $this->input->get('tgl_akhir');
		$status = $this->input->get('status');
		$id_mapel = $this->input->get('id_mapel');

		$data['url_json'] = base_url() . 'adm/absen_siswa/filter_json?id_jenjang=' . $id_jenjang . '&id_kelas=' . $id_kelas . '&id_mapel=' . $id_mapel . '&status=' . $status . '&tgl_awal=' . $tgl_awal . '&tgl_akhir=' . $tgl_akhir;
		//echo $data['url_json'];
		$this->template($page, $data);
	}

	public function filter_json()
	{
		
		$numbcol = $this->input->get('iSortCol_0');

		$echo = $this->input->get('sEcho');
		$start = $this->input->get('iDisplayStart');
		$length = $this->input->get('iDisplayLength');
		$search = $this->input->get('sSearch');
		$sorting = $this->input->get('sSortDir_0');
		$colsorting = $this->input->get('mDataProp_' . $numbcol);

		$id_jenjang = $this->input->get('id_jenjang');
		$id_kelas = $this->input->get('id_kelas');
		$tgl_awal = $this->input->get('tgl_awal');
		$tgl_akhir = $this->input->get('tgl_akhir');
		$status = $this->input->get('status');
		$id_mapel = $this->input->get('id_mapel');

		if ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal == '' and $tgl_akhir == '') {
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal <> '' and $tgl_akhir == '') {
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal <> '' and $tgl_akhir <> '') {
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal == '' and $tgl_akhir == '') {
			if ($status == 'semua') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal <> '' and $tgl_akhir == '') {
			if ($status == 'semua') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal <> '' and $tgl_akhir <> '') {
			if ($status == 'semua') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
		}

		if ($tgl_awal == '') {
			$tgl_awal = date('d-m-Y');
		}
		if ($tgl_akhir == '') {
			$tgl_akhir = $tgl_awal;
		}

		$res = $this->m_siswakelas->json_filter($start, $length, $search, false, $sorting, $colsorting, $tgl_awal, $tgl_akhir, $status, $kondisi);
		$count = $this->m_siswakelas->json_filter($start, $length, $search, true, $sorting, $colsorting, $tgl_awal, $tgl_akhir, $status, $kondisi);

		//print_r($res);
		// die();

		$data = array("sEcho" => $echo, "iTotalRecords" => $count, "iTotalDisplayRecords" => $count, "aaData" => $res);
		echo json_encode($data);
	}

	public function edit_absensi()
	{
		$id_absen_mapel = $this->input->post('id_absen_mapel');
		$absen = $this->input->post('absen');
		$id_penempatan = $this->input->post('id_penempatan');
		$id_jadwal = $this->input->post('id_jadwal');

		if ($id_absen_mapel != "undefined") {
			$this->db->set('absen', $absen);
			$this->db->where('id_absen_mapel', $id_absen_mapel);
			if ($this->db->update('tabel_absen_mapel')) {
				echo "Data Berhasil Disimpan";
			} else {
				echo "Data Gagal Disimpan";
			};
		} else if ($id_penempatan != "undefined") {
			$tgl = $this->input->post('tgl');
			$status = $this->m_jadwal->get_jadwal_by_id($id_jadwal);

			foreach ($status->result() as $row) {
				$sta = $row->status;
			}

			$data = array(
				'id_penempatan' => $id_penempatan,
				'id_jadwal' => $id_jadwal,
				'tgl' => $tgl,
				'jam' => "",
				'absen' => $absen,
				'valid' => "valid",
				'rekap' => "-",
				'status' => $sta,
				'keterangan' => "Manual",
			);

			if ($this->db->insert('tabel_absen_mapel', $data)) {
				echo "Data Berhasil Disimpan";
			} else {
				echo "Data Gagal Disimpan";
			};
		}
	}

	public function export_filter()
	{
		$id_kelas = $this->input->get('id_kelas');
		$tgl_awal = $this->input->get('tgl_awal');
		$tgl_akhir = $this->input->get('tgl_akhir');
		$status = $this->input->get('status');
		$id_mapel = $this->input->get('id_mapel');
		$a = $this->input->get('a');

		if ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal == '' and $tgl_akhir == '') {
			$tipe = "harian";
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal <> '' and $tgl_akhir == '') {
			$tipe = "harian";
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel == '' and $tgl_awal <> '' and $tgl_akhir <> '') {
			$tipe = "harian";

			// $end = date('Y-m-d', strtotime($tgl_akhir . ' +1 day'));

			// $period = new DatePeriod(
			//      new DateTime($tgl_awal),
			//      new DateInterval('P1D'),
			//      new DateTime($end)
			// );

			// $rekap = array();

			// foreach ($period as $key => $value) {
			// $tgl_nya = $value->format('Y-m-d');

			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}
			// }
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal == '' and $tgl_akhir == '') {
			$tipe = "harian";
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal <> '' and $tgl_akhir == '') {
			$tipe = "harian";
			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";;
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
		} elseif ($id_kelas <> '' and $status <> '' and $id_mapel <> '' and $tgl_awal <> '' and $tgl_akhir <> '') {
			$tipe = "harian";

			// $end = date('Y-m-d', strtotime($tgl_akhir . ' +1 day'));

			// $period = new DatePeriod(
			//      new DateTime($tgl_awal),
			//      new DateInterval('P1D'),
			//      new DateTime($end)
			// );

			// $rekap = array();

			// foreach ($period as $key => $value) {
			// $tgl_nya = $value->format('Y-m-d');

			if ($status == 'semua') {
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";;
			} elseif ($status == 'tidak_hadir') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			} elseif ($status == 'izin' or $status == 'alpha' or $status == 'sakit') {
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'  AND tabel_jadwal.id_mapel = '$id_mapel'";
			}
			// }
		}

		if ($tgl_awal == "") $tgl_awal = date('d-m-Y');
		if ($tgl_akhir == "") $tgl_akhir = $tgl_awal;

		if ($tipe == "harian") {
			$sql = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_siswa.nisn,
					tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_siswa.jenis_kelamin,tabel_kelas.id_kelas,tabel_siswa.agama,
					tabel_tahunajaran.tahun_ajaran,tabel_jadwal.hari 
					FROM tabel_siswakelas LEFT JOIN tabel_siswa
		        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
						ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
		        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
		        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
		        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_jadwal
		        ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)	                       
						" . $kondisi . "";
			$sql .= " order by tabel_siswa.nama_lengkap asc";
		}


		$data['tgl_awal'] 	= $tgl_awal;
		$data['tgl_akhir'] 	= $tgl_akhir;
		if ($tipe == "harian" and $a == '1') {
			$data['dt_wali_kelas'] 	= $this->db->query($sql);
			$this->load->view('panel/pdf_absen_siswa_all', $data); // SINGLE PREVIEW WEB			
		} elseif ($tipe == "rekap" and $a == '1') {
			$data['tipe'] = 1;
			$data['res'] = $rekap;
			$this->load->view('panel/pdf_absen_siswa_all2', $data); // ALL PREVIEW WEB
		} elseif ($tipe == "harian" and $a == '2') {
			$data['dt_wali_kelas'] 	= $this->db->query($sql);
			$this->load->view('panel/excel_absen_siswa_all', $data); // SINGLE EXCELL
		} elseif ($tipe == "rekap" and $a == '2') {
			$data['tipe'] = 2;
			$data['res'] = $rekap;
			$this->load->view('panel/excel_absen_siswa_rekap', $data); // ALL EXCELL
		}
	}

	public function get_mapel()
	{
		$jenjang = $this->input->post('jenjang');
		$dt_mapel = $this->m_mapel->get_one_jen($jenjang);
		$data = "<option value=''>Pilih Mapel</option>";
		foreach ($dt_mapel->result() as $row) {
			$data .= "<option value='$row->id_mapel'>$row->mapel</option>\n";
		}
		echo $data;
	}
}
