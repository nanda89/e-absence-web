<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tunggakan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_form_bayar');
		$this->load->model('m_jenjang');
		$this->load->model('m_biaya');
		$this->load->model('m_siswa');
		$this->load->model('m_pesan');
		$this->load->model('m_tahunajaran');		
		//===== Load Library =====
		$this->load->library('email');

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "tunggakan";		
		$data['title']	= "SIAKAD | Cek Tunggakan";			
		$data['isi']    = "tunggakan";
		$data['judul1']	= "Cek Tunggakan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_bayar.id_bayar,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran,
				tabel_biaya.biaya,
				tabel_biaya.nominal,
				tabel_bayar.tgl_bayar				
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)								    
				RIGHT JOIN tabel_bayar ON (tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_biaya.id_biaya = tabel_bayar.id_biaya)
				WHERE tabel_siswa.id_cabang = '$id_cabang'
			    ORDER BY tabel_siswa.id_siswa ASC");		
		}else{
			$data['dt_tu'] = $this->m_form_bayar->get_tunggakan_all();	
		}						
		
		$this->template($page, $data);	
	}

	public function t_tunggakan(){
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran				
				 FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)					
			    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun'
			    AND tabel_siswa.id_cabang = '$id_cabang'
			    ORDER BY tabel_siswa.id_siswa ASC");
		}else{
			$data['dt_tu'] = $this->m_form_bayar->get_tunggakan($id_jenjang,$id_tahun);	
		}
		
		$page			= "t_tunggakan";		
		$data['title']	= "SIAKAD | Cek Tunggakan";			
		$data['isi']    = "tunggakan";
		$data['judul1']	= "Cek Tunggakan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();
		$this->template($page, $data);	
	}
	
	public function delete_riwayat(){
		$id_bayar 		= $this->input->post('id_bayar');		
		$this->m_form_bayar->hapus_riwayat($id_bayar);			
		echo "nihil";
	}

	public function mailku() {
		$bln2	= date('m');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$sql = "SELECT DISTINCT(tabel_siswakelas.id_siswa),tabel_siswa.nama_lengkap,tabel_siswa.email
			FROM tabel_siswakelas INNER JOIN tabel_siswa ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa
			WHERE tabel_siswakelas.status='aktif' AND tabel_siswa.id_cabang = '$id_cabang' 
			AND tabel_siswakelas.id_penempatan NOT IN (SELECT tabel_bayar.id_penempatan 
				FROM tabel_bayar WHERE LEFT(tabel_bayar.tgl_bayar,2) = '$bln2')";
		}else{
			$sql = "SELECT DISTINCT(tabel_siswakelas.id_siswa),tabel_siswa.nama_lengkap,tabel_siswa.email
			FROM tabel_siswakelas INNER JOIN tabel_siswa ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa
			WHERE tabel_siswakelas.status='aktif' AND tabel_siswakelas.id_penempatan NOT IN (SELECT tabel_bayar.id_penempatan 
				FROM tabel_bayar WHERE LEFT(tabel_bayar.tgl_bayar,2) = '$bln2')";	
		}
		
		$y = $this->db->query($sql);
		$cek = $y->num_rows();
		if($cek>0){
			foreach ($y->result() as $ambil) {
				$s = "SELECT isi_pesan,akun_gmail FROM tabel_setting WHERE id_setting=1";
				$ys = $this->db->query($s);
				$ce = $ys->row();
				require_once(APPPATH.'libraries/PHPMailerAutoload.php');
				$nama	= $ambil->nama_lengkap;			
				$em		= $ambil->email;
				$bln	= date('F');			
				$pesan	= $ce->isi_pesan;
				$akun	= $ce->akun_gmail;
				$akun_webmail	= $ce->akun_webmail;					
				$pass_webmail	= $ce->pass_webmail;
				$host			= $ce->host_webmail;			
					
			 	$mail = new PHPMailer;
			
				 $mail->isSMTP();
				 $mail->Host = 'mail.'.$host.''; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
				 $mail->SMTPAuth = true;
				 $mail->Username = $akun_webmail; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
				 $mail->Password = $pass_webmail; //masukan kata sandi
				 $mail->Port = 587; //port tidak usah di ubah, biarkan 587
				
				 $mail->setFrom($akun, 'Pengingat'); //email pengirim
				 $mail->addAddress($em, 'penerima'); //email penerima
				 $mail->addReplyTo($em, 'pengingat');
				 $mail->isHTML(true);
				
			
					
				        ///atur pesan email disini
				 $mail->Subject = 'Reply';
				 $isiemail = $pesan;
				 $isiemail .= "<b>Nama :".$nama."</b><br>";
				 $isiemail .= "<b>Bulan :".$bln."</b>";
				 $mail->Body    = $isiemail;
				 //$mail->Body    = 
				 $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				
				 if(!$mail->send()) {
				 	echo "<script type='text/javascript'>
							alert('Gagal dikirm');			
						</script>";
				 	echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tunggakan'>";
				 } else {
				  echo "<script type='text/javascript'>
							alert('Pesan berhasil dikirm');			
						</script>";
				  echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tunggakan'>";
				 }

			}
		}
	 	
	}

}
