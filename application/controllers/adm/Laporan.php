<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_form_bayar');
		$this->load->model('m_jenjang');
		$this->load->model('m_biaya');
		$this->load->model('m_siswa');
		$this->load->model('m_pesan');
		$this->load->model('m_tahunajaran');		
		//===== Load Library =====
		$this->load->library('email');
		$this->load->library('cfpdf');	

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function keuangan()
	{		
		$page			= "lap_keuangan";		
		$data['title']	= "SIAKAD | Laporan Keuangan";			
		$data['isi']    = "lap_keuangan";
		$data['judul1']	= "Laporan Keuangan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$data['dt_tu'] = $this->db->query("SELECT tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
				tabel_siswa.nama_lengkap,
				tabel_siswa.id_jenjang,
				tabel_kelas.kelas,
				tabel_bayar.id_bayar,
				tabel_kelas.tingkat,
				tabel_tahunajaran.tahun_ajaran,
				tabel_biaya.biaya,
				tabel_biaya.nominal,
				tabel_bayar.tgl_bayar				
				FROM tabel_tahunajaran 
				INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
				LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
				LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)								    
				RIGHT JOIN tabel_bayar ON (tabel_bayar.id_penempatan = tabel_siswakelas.id_penempatan)
				INNER JOIN tabel_biaya ON (tabel_biaya.id_biaya = tabel_bayar.id_biaya)
				WHERE tabel_siswa.id_cabang = '$id_cabang'
			    ORDER BY tabel_siswa.id_siswa ASC");
		}else{
			$data['dt_tu'] = $this->m_form_bayar->get_tunggakan_all();	
		}								
		
		$this->template($page, $data);	
	}
	public function hapus_bayar()
	{		
		$id = $this->input->post('id');
		$this->m_form_bayar->hapus_bayar($id);
		$page			= "lap_keuangan";		
		$data['title']	= "SIAKAD | Laporan Keuangan";			
		$data['isi']    = "lap_keuangan";
		$data['judul1']	= "Laporan Keuangan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();								
		$data['dt_tu'] = $this->m_form_bayar->get_tunggakan_all();
		$this->template($page, $data);	
	}
	public function t_keuangan(){
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_jenjang.jenjang,
								tabel_kelas.tingkat,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)					
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun' AND tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)			
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_siswakelas.id_jenjang)		
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_kelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan($id_jenjang,$id_tahun,$bulan,$tahun);
			}elseif($bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan1($bulan,$tahun);
			}elseif($tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan2($tahun);
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan3($id_jenjang,$bulan,$tahun);
			}
		}

		$page			= "t_keuangan";		
		$data['title']	= "SIAKAD | Laporan Keuangan";			
		$data['isi']    = "lap_keuangan";
		$data['judul1']	= "Laporan Keuangan";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_form_bayar']	= $this->m_form_bayar->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();			
		$data['dt_biaya']	= $this->m_biaya->get_all();
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();
		$this->template($page, $data);	
	}
	public function keuangan_excel(){
		
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_jenjang.jenjang,
								tabel_kelas.tingkat,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)					
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun' AND tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)			
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_siswakelas.id_jenjang)		
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_kelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan($id_jenjang,$id_tahun,$bulan,$tahun);
			}elseif($bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan1($bulan,$tahun);
			}elseif($tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan2($tahun);
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$data['dt_tu'] = $this->m_form_bayar->get_laporan3($id_jenjang,$bulan,$tahun);
			}
		}

		$this->load->view('panel/excel_keuangan',$data);
	}
	public function keuangan_pdf(){
		$tgl2 = date('d M Y');
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_jenjang.jenjang,
								tabel_kelas.tingkat,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)					
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_siswakelas.id_ta='$id_tahun' AND tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($tahun <> ""){
				$dt_tu = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)		
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang)			
							    WHERE tabel_siswakelas.status='aktif'
							    AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->db->query("SELECT 	tabel_siswa.id_siswa, tabel_siswakelas.id_penempatan,
								tabel_siswa.nama_lengkap,
								tabel_siswa.id_jenjang,
								tabel_kelas.kelas,
								tabel_kelas.tingkat,
								tabel_jenjang.jenjang,
								tabel_tahunajaran.tahun_ajaran,	
								tabel_bayar.tgl_bayar,tabel_bayar.ket,tabel_biaya.biaya,tabel_biaya.nominal			
								FROM tabel_tahunajaran 
								INNER JOIN tabel_siswakelas ON (tabel_tahunajaran.id_ta = tabel_siswakelas.id_ta)
								INNER JOIN tabel_bayar ON (tabel_siswakelas.id_penempatan = tabel_bayar.id_penempatan)
								INNER JOIN tabel_biaya ON (tabel_bayar.id_biaya = tabel_biaya.id_biaya)
								LEFT JOIN tabel_kelas ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas)
								LEFT JOIN tabel_siswa ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)			
								LEFT JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_siswakelas.id_jenjang)		
							    WHERE tabel_siswa.id_jenjang = '$id_jenjang' AND tabel_kelas.status='aktif'
							    AND LEFT(tabel_bayar.tgl_bayar,2) = '$bulan' AND RIGHT(tabel_bayar.tgl_bayar,4) = '$tahun'
							    AND tabel_siswa.id_cabang = '$id_cabang'
							    ORDER BY tabel_siswa.id_siswa ASC");
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->m_form_bayar->get_laporan($id_jenjang,$id_tahun,$bulan,$tahun);
			}elseif($bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->m_form_bayar->get_laporan1($bulan,$tahun);
			}elseif($tahun <> ""){
				$dt_tu = $this->m_form_bayar->get_laporan2($tahun);
			}elseif($id_jenjang <> "" AND $bulan <> "" AND $tahun <> ""){
				$dt_tu = $this->m_form_bayar->get_laporan3($id_jenjang,$bulan,$tahun);
			}
		}
			 
		$d = $dt_tu->row();	

		$pdf = new FPDF('l','mm','A4');
        $pdf->AddPage();
       // head
       $pdf->SetFont('TIMES','',12);
       $pdf->Cell(290, 5, 'SARIPUTRA JAMBI', 0, 1, 'C');
       $pdf->SetFont('TIMES','',10);
       $pdf->Cell(290, 5, 'JL. Pangeran Diponegoro no. 55 Kel. Sulanjana Kota Jambi', 0, 1, 'C');
       $pdf->Cell(290, 5, 'Telp.+62-0741-23684', 0, 1, 'C');
       $pdf->Cell(290, 5, 'E-mail : Service@sariputra-jambi.com', 0, 1, 'C');
       $pdf->Line(11, 31, 280, 31);
       
       $pdf->Image(base_url().'/assets/web/images/ico/logo.png', 10, 10, 20);
       
       $pdf->SetFont('TIMES','B',12);
       $pdf->Cell(1,2,'',0,1);
       $pdf->Cell(290, 5, 'LAPORAN KEUANGAN', 0, 1, 'C');
       $pdf->Cell(2, 2,'',0,1);
       $pdf->SetFont('TIMES','B',9);
       // buat tabel disini
       $pdf->SetFont('TIMES','B',9);
       
       // kasi jarak
       $pdf->Cell(3,2,'',0,1);
       
       $pdf->Cell(7, 5, 'No', 1, 0);
       $pdf->Cell(20, 5, 'No.Induk', 1, 0);
       $pdf->Cell(60, 5, 'Nama Lengkap', 1, 0);
       $pdf->Cell(25, 5, 'Jenjang', 1, 0);
       $pdf->Cell(25, 5, 'Kelas', 1, 0);
       $pdf->Cell(75, 5, 'Biaya', 1, 0);
       $pdf->Cell(25, 5, 'Nominal', 1, 0);
       $pdf->Cell(25, 5, 'Tgl.Bayar', 1, 1);             
   
       $pdf->SetFont('times','',9);
       $i=1;
       $jum = 0;
       foreach ($dt_tu->result() as $r)
       {

            $pdf->Cell(7, 5, $i, 1, 0);
            $pdf->Cell(20, 5, strtoupper($r->id_siswa), 1, 0);
            $pdf->Cell(60, 5, $r->nama_lengkap, 1, 0);
            $pdf->Cell(25, 5, $r->jenjang, 1, 0);    
            $pdf->Cell(25, 5, $r->kelas, 1, 0);
            $pdf->Cell(75, 5, $r->biaya, 1, 0); 
            $pdf->Cell(25, 5, $r->nominal, 1, 0); 
            $pdf->Cell(25, 5, $r->tgl_bayar, 1, 1); 
            $i++; 
            $jum = $jum + ($r->nominal);
	        $j = number_format($jum, 0, ',', '.');
       }
       
       
       $pdf->Cell(212, 5, 'Total', 1, 0,'R');
       $pdf->Cell(50, 5, "Rp. ".$j, 1, 2);                  
       
       // tanda tangan
       
       $pdf->Cell(10, 5, '', 0, 1);
       $pdf->Cell(10, 15, '', 0, 0);
       $pdf->Cell(10, 5, 'Jambi,'.$tgl2, 0, 1);
       $pdf->Cell(10, 5, '', 0, 0);
       $pdf->Cell(10, 5, 'Bagian Akademik,', 0, 1);
       $pdf->Cell(10, 10, '', 0, 0);
       $pdf->Cell(10, 10, '', 0, 1);
       $pdf->Cell(10, 5, '', 0, 0);
       $pdf->Cell(10, 5, '_________________________', 0, 0);
       
       $pdf->Output(); 
	}
	public function absensi()
	{		
		$page			= "lap_absensi";		
		$data['title']	= "SIAKAD | Laporan Absensi";			
		$data['isi']    = "lap_absensi";
		$data['judul1']	= "Laporan Absensi";			
		$data['judul2']	= "";					
		$data['set']	= "view";
					
		$data['dt_tahun']	= $this->m_tahunajaran->get_all();			
		$data['dt_jenjang']	= $this->m_jenjang->get_all();												
		$data['dt_kelas']	= $this->m_jenjang->get_all_kelas();

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$data['dt_wali_kelas'] = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
											tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
											FROM tabel_siswakelas LEFT JOIN tabel_siswa
									        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
								        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
									        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
									        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
									        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
									        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
									        WHERE tabel_siswakelas.status = 'aktif'	AND tabel_siswa.id_cabang = '$id_cabang'        
									        ORDER BY tabel_absen.id_absen DESC");	
		}else{
			$data['dt_wali_kelas'] = $this->m_absen->get_all();		
		}									
		
		$this->template($page, $data);	
	}
	public function t_absen(){
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$id_kelas = $this->input->get('id_kelas');
		$tanggal = $this->input->get('tanggal');
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$data['dt_wali_kelas'] = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' AND tabel_absen.tgl = '$tanggal'
						        ORDER BY tabel_absen.id_absen DESC");
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$data['dt_wali_kelas'] = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' 
						        AND RIGHT(tabel_absen.tgl,4) = '$tahun' AND MID(tabel_absen.tgl,4,2) = '$bulan'
						        ORDER BY tabel_absen.id_absen DESC");
			}else{
				$data['dt_wali_kelas'] = "nihil";
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$data['dt_wali_kelas'] = $this->m_absen->get_laporan($id_kelas,$id_tahun,$tanggal);
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$data['dt_wali_kelas'] = $this->m_absen->get_laporan1($id_kelas,$id_tahun,$bulan,$tahun);
			}else{
				$data['dt_wali_kelas'] = "nihil";
			}
		}
		

		$page			= "t_absen";		
		$data['title']	= "SIAKAD | Laporan Absensi";			
		$data['isi']    = "lap_absensi";
		$data['judul1']	= "Laporan Absensi";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$this->template($page, $data);	
	}
	public function absensi_excel(){
		
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$id_kelas = $this->input->get('id_kelas');
		$tanggal = $this->input->get('tanggal');
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$data['dt_wali_kelas'] = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' AND tabel_absen.tgl = '$tanggal'
						        ORDER BY tabel_absen.id_absen DESC");
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$data['dt_wali_kelas'] = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' 
						        AND RIGHT(tabel_absen.tgl,4) = '$tahun' AND MID(tabel_absen.tgl,4,2) = '$bulan'
						        ORDER BY tabel_absen.id_absen DESC");
			}else{
				$data['dt_wali_kelas'] = "nihil";
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$data['dt_wali_kelas'] = $this->m_absen->get_laporan($id_kelas,$id_tahun,$tanggal);
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$data['dt_wali_kelas'] = $this->m_absen->get_laporan1($id_kelas,$id_tahun,$bulan,$tahun);
			}else{
				$data['dt_wali_kelas'] = "nihil";
			}
		}

		$this->load->view('panel/excel_absen',$data);
	}
	public function absensi_pdf(){
		$tgl2 = date('d M Y');
		
		$id_jenjang = $this->input->get('id_jenjang');		
		$id_tahun = $this->input->get('id_tahun');
		$id_kelas = $this->input->get('id_kelas');
		$tanggal = $this->input->get('tanggal');
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$dt_wali_kelas = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' AND tabel_absen.tgl = '$tanggal'
						        ORDER BY tabel_absen.id_absen DESC");
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$dt_wali_kelas = $this->db->query("SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,
								tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
								FROM tabel_siswakelas LEFT JOIN tabel_siswa
						        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
					        	ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
						        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
						        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
						        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
						        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)
						        WHERE tabel_siswakelas.status = 'aktif' AND tabel_siswa.id_cabang='$id_cabang'
						        AND tabel_siswakelas.id_kelas = '$id_kelas' AND tabel_siswakelas.id_ta = '$id_tahun' 
						        AND RIGHT(tabel_absen.tgl,4) = '$tahun' AND MID(tabel_absen.tgl,4,2) = '$bulan'
						        ORDER BY tabel_absen.id_absen DESC");
			}else{
				$dt_wali_kelas = "nihil";
			}
		}else{
			if($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $tanggal <> ""){
				$dt_wali_kelas = $this->m_absen->get_laporan($id_kelas,$id_tahun,$tanggal);
			}elseif($id_jenjang <> "" AND $id_tahun <> "" AND $id_kelas <> "" AND $bulan <> ""){
				$dt_wali_kelas = $this->m_absen->get_laporan1($id_kelas,$id_tahun,$bulan,$tahun);
			}else{
				$dt_wali_kelas = "nihil";
			}
		}
		 

		$d = $dt_wali_kelas->row();	

		$pdf = new FPDF('p','mm','A4');
        $pdf->AddPage();
       // head
       $pdf->SetFont('TIMES','',12);
       $pdf->Cell(180, 5, 'SARIPUTRA JAMBI', 0, 1, 'C');
       $pdf->SetFont('TIMES','',10);
       $pdf->Cell(180, 5, 'JL. Pangeran Diponegoro no. 55 Kel. Sulanjana Kota Jambi', 0, 1, 'C');
       $pdf->Cell(180, 5, 'Telp.+62-0741-23684', 0, 1, 'C');
       $pdf->Cell(180, 5, 'E-mail : Service@sariputra-jambi.com', 0, 1, 'C');
       $pdf->Line(11, 31, 190, 31);
       
       $pdf->Image(base_url().'/assets/web/images/ico/logo.png', 10, 10, 20);
       
       $pdf->SetFont('TIMES','B',12);
       $pdf->Cell(1,2,'',0,1);
       $pdf->Cell(180, 5, 'LAPORAN ABSENSI', 0, 1, 'C');
       $pdf->Cell(2, 2,'',0,1);
       $pdf->SetFont('TIMES','B',9);
       // buat tabel disini
       $pdf->SetFont('TIMES','B',9);
       
       // kasi jarak
       $pdf->Cell(3,2,'',0,1);
       
       $pdf->Cell(7, 5, 'No', 1, 0);
       $pdf->Cell(20, 5, 'No.Induk', 1, 0);
       $pdf->Cell(40, 5, 'Nama Lengkap', 1, 0);
       $pdf->Cell(20, 5, 'Jenjang', 1, 0);
       $pdf->Cell(10, 5, 'Kelas', 1, 0);
       $pdf->Cell(25, 5, 'Tanggal', 1, 0);
       $pdf->Cell(30, 5, 'Jam Absen Masuk', 1, 0);
       $pdf->Cell(30, 5, 'Jam Absen Pulang', 1, 1);             
   
       $pdf->SetFont('times','',9);
       $i=1;
       foreach ($dt_wali_kelas->result() as $r)
       {
       		  $tanggal = date("d F Y", strtotime($r->tgl));
	          $tgl = date('d-m-Y');
	          $s = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
	                WHERE tabel_siswakelas.id_penempatan='$r->id_penempatan' and tabel_absen.tgl = '$r->tgl'";
	          $d = $this->db->query($s);
	          $ambil = $d->row();
	          if($d->num_rows()>0){
	            if($ambil->absen_masuk==''){
	              $absen_masuk = "-";
	              $jam_masuk = '';  
	            }else{
	              $absen_masuk = $ambil->absen_masuk; 
	              $jam_masuk = $ambil->jam_masuk." | ";  
	            }

	            if($ambil->absen_pulang==''){
	              $absen_pulang = "-";  
	              $jam_pulang = '';
	            }else{
	              $absen_pulang = $ambil->absen_pulang; 
	              $jam_pulang = $ambil->jam_pulang." | "; 
	            }
	          }else{
	            $absen_pulang = "-";
	            $absen_masuk = "-";
	            $jam_masuk = '';
	            $jam_pulang = '';
	          }
            $pdf->Cell(7, 5, $i, 1, 0);
            $pdf->Cell(20, 5, strtoupper($r->id_siswa), 1, 0);
            $pdf->Cell(40, 5, $r->nama_lengkap, 1, 0);
            $pdf->Cell(20, 5, $r->jenjang, 1, 0);    
            $pdf->Cell(10, 5, $r->kelas, 1, 0);
            $pdf->Cell(25, 5, $tanggal, 1, 0); 
            $pdf->Cell(30, 5, $jam_masuk, 1, 0); 
            $pdf->Cell(30, 5, $jam_pulang, 1, 1); 
            $i++;             
       }
       
      
       // tanda tangan
       
       $pdf->Cell(10, 5, '', 0, 1);
       $pdf->Cell(10, 15, '', 0, 0);
       $pdf->Cell(10, 5, 'Jambi,'.$tgl2, 0, 1);
       $pdf->Cell(10, 5, '', 0, 0);
       $pdf->Cell(10, 5, 'Wali Kelas,', 0, 1);
       $pdf->Cell(10, 10, '', 0, 0);
       $pdf->Cell(10, 10, '', 0, 1);
       $pdf->Cell(10, 5, '', 0, 0);
       $pdf->Cell(10, 5, '_________________________', 0, 0);
       
       $pdf->Output(); 
	}
}