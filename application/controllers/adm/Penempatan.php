<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penempatan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_siswa');
		$this->load->model('m_jenjang');
		$this->load->model('m_pesan');		
		$this->load->model('m_penempatan');		
		$this->load->model('m_tahunajaran');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "pilih";
		$data['isi']    = "penempatan";		
		$data['title']	= "SIAKAD | Penempatan Kelas";			
		$data['judul1']	= "Penempatan Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "view";		
		$data['dt_jenjang'] = $this->m_jenjang->get_all();					
		$this->template($page, $data);	
	}
	public function set($id)
	{		
		$page			= "penempatan";
		$data['isi']    = "penempatan";		
		$data['title']	= "SIAKAD | Penempatan Kelas";			
		$data['judul1']	= "Penempatan Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_j']		= $id;					
		$data['dt_ke']		= $this->m_jenjang->get_one($id);		
		$data['dt_pen']		= $this->m_penempatan->get_all($id);								
		$data['dt_kelas']	= $this->m_jenjang->get_one_jenjang($id);													
		$data['dt_ta'] 		= $this->m_tahunajaran->get_all();					
		
		$this->template($page, $data);	
	}
	public function set_jenjang(){
		$id	= $this->input->get('id_jenjang');										
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/penempatan/set/$id'>";
	}	
	public function get_kelas(){
		$id_jenjang	= $this->input->post('id_jenjang');
		$tingkat	= $this->input->post('tingkat');

		$level = $this->session->userdata('level');
		$id_cabang = $this->session->userdata('id_cabang');
		if ($level=='operator cabang') {
			$dt_kelas  	= $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang 
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
		}else{
			$dt_kelas  	= $this->m_penempatan->get_one_kelas($id_jenjang,$tingkat);	
		}
		
		$data .= "<option value=''>Pilih Kelas</option>";
		foreach ($dt_kelas->result() as $row) {
			$data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
		}
		echo $data;
	}	
	public function t_penempatan(){
		$id_tahun = $this->input->post('id_tahun');
		$angkatan = $this->input->post('angkatan');
		$id_kelas = $this->input->post('id_kelas');
		$data['dt_penempatan'] = $this->m_penempatan->get_penempatan($id_tahun,$angkatan,$id_kelas);
		$this->load->view('panel/t_penempatan',$data);
	}
	public function save()
	{
		$data['id_ta']		= $this->input->post('id_tahun');						
		$data['angkatan']	= $this->input->post('angkatan');						
		$data['id_kelas']	= $this->input->post('id_kelas');						
		$data['id_siswa']	= $this->input->post('id_siswa');								
		$data['status']		= 'aktif';								
		$detail['status']	= 'aktif';
		$id_siswa			= $this->input->post('id_siswa');								
		$cek = $this->m_penempatan->cek_siswa($id_siswa);
		if($cek->num_rows()>0){
			echo "nothing";
		}else{
			$this->m_penempatan->tambah($data);
			$this->m_siswa->edit($id_siswa,$detail);
			echo "nihil";
		}					
	}
	public function delete_penempatan(){
		$id 				= $this->input->post('id_penempatan');		
		$id_siswa			= $this->input->post('id_siswa');								
		$this->m_penempatan->hapus($id);			
		$detail['status']	= '';				
		$this->m_siswa->edit($id_siswa,$detail);		
		echo "nihil";
	}

}
