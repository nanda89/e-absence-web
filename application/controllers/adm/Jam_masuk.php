<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jam_masuk extends CI_Controller {
	public function __construct()
	{				
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_guru');
		$this->load->model('m_jenjang');
		$this->load->model('m_pesan');
		$this->load->model('m_mapel');
		$this->load->model('m_semua');		
		$this->load->model('m_jam_masuk');
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "jam_masuk";	
		$data['isi']    = "jam_masuk";	
		$data['title']	= "Jam Masuk";			
		$data['judul1']	= "Jam Masuk";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_jam_masuk']= $this->m_jam_masuk->get_all();						
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "jam_masuk";
		$data['isi']    = "jam_masuk";			
		$data['title']	= "Jam Masuk";			
		$data['judul1']	= "Tambah Data Jam Masuk";			
		$data['judul2']	= "";					
		$data['set']	= "insert";				
		$this->template($page, $data);	
	}
	public function ajax_bulk_delete()
	{
		$tabel			= "tabel_jam_masuk";
		$pk 				= "id_jam_masuk";
		$list_id 		= $this->input->post('id');
		foreach ($list_id as $id) {
			$this->m_jam_masuk->hapus($id);
		}
		echo json_encode(array("status" => TRUE));
	}
	function delete_multiple(){
		$this->m_jam_masuk->remove_checked();
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/jam_masuk'>";
	}	
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			//setting konfigurasi upload gambar
			$config['upload_path'] 		= './assets/panel/images/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['max_size']			= '0';
			$config['max_width']  		= '0';
			$config['max_height']  		= '0';
					
			$this->upload->initialize($config);

			$data['hari']			= $this->input->post('hari');	
			$data['jam'] 			= $this->input->post('jam');	

			$this->m_jam_masuk->tambah($data);
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/jam_masuk'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "jam_masuk";		
			$data['one_post']= $this->m_jam_masuk->get_one($id);
			$data['title']	= "Jam Masuk";			
			$data['judul1']	= "Edit Data Jam Masuk";			
			$data['isi']    = "jam_masuk";				
			$data['judul2']	= "";						
			$data['set']	= "edit";				
			$this->template($page, $data);	
		}
		//DETAIL DATA
		elseif ($set == 'detail')
		{
			$page			= "jam_masuk";		
			$data['one_post']= $this->m_jam_masuk->get_one($id);
			$data['title']	= "Jam Masuk";			
			$data['judul1']	= "Detail Data Jam Masuk";	
			$data['isi']    = "jam_masuk";			
			$data['judul2']	= "";						
			$data['set']	= "detail";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$config['upload_path'] 		= './assets/panel/images/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['max_size']			= '0';
			$config['max_width']  		= '0';
			$config['max_height']  		= '0';
					
			$data['hari']			= $this->input->post('hari');			
			$data['jam']		= $this->input->post('jam');					
			$this->m_jam_masuk->edit($id, $data);
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/jam_masuk'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_jam_masuk->hapus($id);		
			$_SESSION['pesan'] 	= "Berhasil dihapus!";
			$_SESSION['tipe'] 	= "danger";	
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/jam_masuk'>";
		}
		
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/jam_masuk'>";
		}
	}

}
