<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_jadwal');
        $this->load->model('m_guru');
        $this->load->model('m_jenjang');
        $this->load->model('m_mapel');
        $this->load->model('m_pesan');
        $this->load->model('m_tahunajaran');
        //===== Load Library =====

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "jadwal";
        $data['isi'] = "jadwal";
        $data['title'] = "SIAKAD | Jadwal Pelajaran";
        $data['judul1'] = "Jadwal Pelajaran";
        $data['dt_jadwal'] = $this->m_jadwal->get_all();
        $data['dt_tahun'] = $this->m_tahunajaran->get_all_by_status('Aktif');
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $data['judul2'] = "";
        $data['judul1'] = "Jadwal Pelajaran";
        $data['set'] = "view";
        $this->template($page, $data);
    }

    public function add()
    {
        $page = "jadwal";
        $data['title'] = "SIAKAD | Jadwal Pelajaran";
        $data['isi'] = "jadwal";
        $data['judul1'] = "Jadwal Pelajaran";
        $data['judul2'] = "";
        $data['judul1'] = "Jadwal Pelajaran";
        $data['set'] = "insert";
        $data['dt_tahun'] = $this->m_tahunajaran->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $data['dt_kelas'] = $this->m_jenjang->get_all_kelas();
        $this->template($page, $data);
    }

    public function t_jadwal()
    {
        $id_tahun = $this->input->post('id_tahun');
        $id_jenjang = $this->input->post('id_jenjang');
        $id_kelas = $this->input->post('id_kelas');
        $id_guru = $this->input->post('id_guru');
        $data['dt_jadwal'] = $this->m_jadwal->get_jadwal($id_tahun, $id_jenjang, $id_kelas, $id_guru);
        $this->load->view('panel/t_jadwal', $data);
    }

    public function export_now()
    {
        $page = "jadwal";
        $data['title'] = "Jadwal";
        $data['isi'] = "jadwal";
        $data['judul1'] = "Jadwal";
        $data['judul2'] = "";
        $id_tahun = $this->input->get('id_tahun');
        if ($id_tahun != '') {
            $data['tahun'] = $this->db->query("SELECT tahun_ajaran FROM tabel_tahunajaran where id_ta='$id_tahun'")->row()->tahun_ajaran;
        } else {
            $data['tahun'] = '';
        }

        $id_jenjang = $this->input->get('id_jenjang');
        if ($id_jenjang != '') {
            $data['jenjang'] = $this->db->query("SELECT jenjang FROM tabel_jenjang where id_jenjang='$id_jenjang'")->row()->jenjang;
        } else {
            $data['jenjang'] = '';
        }

        $id_kelas = $this->input->get('id_kelas');
        if ($id_kelas != '') {
            $data['kelas'] = $this->db->query("SELECT kelas FROM tabel_kelas where id_kelas='$id_kelas'")->row()->kelas;
        } else {
            $data['kelas'] = '';
        }

        $id_guru = $this->input->get('id_guru');
        if ($id_guru != '') {
            $data['guru'] = $this->db->query("SELECT nama FROM tabel_guru where id_guru='$id_guru'")->row()->nama;
        } else {
            $data['guru'] = '';
        }

        $a = $this->input->get("a");
        $data['dt_jadwal'] = $this->m_jadwal->get_jadwal($id_tahun, $id_jenjang, $id_kelas, $id_guru);
        $a = $this->input->get('a');
        if ($a == "1") {
            $this->load->view('panel/excel_jadwal', $data);
        } elseif ($a == "2") {
            $this->load->view('panel/pdf_jadwal', $data);
        }
    }

    public function delete_jadwal()
    {
        $id = $this->input->post('id_jadwal');
        $this->m_jadwal->hapus_jadwal($id);
        echo "nihil";
    }
    public function reset()
    {
        $this->m_jadwal->hapus_all();
        ?>
			<script type="text/javascript">
				alert("Semua data dihapus");
			</script>
		<?php
echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/jadwal'>";
    }

    public function save()
    {
        $id_ta = $data['id_ta'] = $this->input->post('id_tahun');
        $id_jenjang = $data['id_jenjang'] = $this->input->post('id_jenjang');
        $id_kelas = $data['id_kelas'] = $this->input->post('id_kelas');
        $id_mapel = $data['id_mapel'] = $this->input->post('id_mapel');
        $id_guru = $data['id_guru'] = $this->input->post('id_guru');
        $hari = $data['hari'] = $this->input->post('hari');
        $jam_awal = $data['jam_awal'] = $this->input->post('jam_awal');
        $jam_akhir = $data['jam_akhir'] = $this->input->post('jam_akhir');
        $status = $data['status'] = $this->input->post('isi');

        $isValid_jam_awal = $this->validate_time($jam_awal);
        $isValid_jam_akhir = $this->validate_time($jam_akhir);

        if (!$isValid_jam_awal || !$isValid_jam_akhir) {
            echo "input jam tidak sesuai";
        } else {
            $cek = $this->db->query("SELECT * FROM tabel_jadwal WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND id_kelas = '$id_kelas' AND id_mapel = '$id_mapel'
                    AND id_guru = '$id_guru' AND hari = '$hari' AND jam_awal = '$jam_awal' AND jam_akhir = '$jam_akhir' AND status = '$status'");
            if ($cek->num_rows() > 0) {
                echo "Data yang anda masukkan sudah ada!";
            } else {

                // cek 2: cek jika dikelas x pada jam yang sama (walaupun beda mapel tetap jam harus beda dong)
                $cek2 = $this->db->query("SELECT * FROM tabel_jadwal WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND id_kelas = '$id_kelas' AND hari = '$hari' AND jam_awal = '$jam_awal' AND jam_akhir = '$jam_akhir' AND status = '$status'");

                if ($cek2->num_rows() > 0) {
                    $guru = $this->db->query("SELECT nama FROM tabel_guru WHERE id_guru=" . $cek2->row()->id_guru);
                    echo "Sudah terisi oleh " . $guru->row()->nama;
                } else {

                    // sekarang jadwal bisa diisi, cek dulu gurunya ada jadwal mengajar tidak di kelas lain pada jam yg sama
                    $cek3 = $this->db->query("SELECT * FROM tabel_jadwal WHERE id_ta = '$id_ta' AND id_jenjang = '$id_jenjang' AND id_guru = '$id_guru' AND hari = '$hari' AND jam_awal = '$jam_awal' AND jam_akhir = '$jam_akhir' AND status = '$status'");

                    if ($cek3->num_rows() > 0) {
                        echo "Tidak diperkenankan mengajar 2 kelas dalam waktu yang bersamaan.";
                    } else {

                        $this->m_jadwal->tambah($data);
                        echo "nihil";

                    }
                }
            }

        }

    }
    public function validate_time($str)
    {

        $expl = explode(':', $str);
        $hh = $expl[0];
        $mm = $expl[1];

        if (!is_numeric($hh) || !is_numeric($mm)) {
            return false;
        } else if ((int) $hh > 24 || (int) $mm > 59) {
            return false;
        } else if (mktime((int) $hh, (int) $mm) === false) {
            return false;
        }

        return true;
    }

    public function get_tingkat()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_kelas = $this->m_jenjang->get_one_jenjang($id_jenjang);
        $data = "<option value=''>Pilih Tingkat</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option>$row->tingkat</option>\n";
        }
        echo $data;
    }
    public function get_kelas()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $level = $this->session->userdata('level');
        $id_cabang = $this->session->userdata('id_cabang');
        if ($level == 'operator cabang') {
            $dt_kelas = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang' AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
        } else {
            $dt_kelas = $this->m_jadwal->get_one_kelas($id_jenjang);
        }
        $data = "<option value=''>Pilih Kelas</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }
    public function get_guru()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $id_mapel = $this->input->post('id_mapel');

        $dt_guru = $this->m_guru->get_guru_by_jenjang_mapel($id_jenjang, $id_mapel);
        $data = "<option value=''>Pilih Guru</option>";
        foreach ($dt_guru->result() as $row) {
            $data .= "<option value='$row->id_guru'>$row->nama ($row->alias)</option>\n";
        }
        echo $data;
    }
    public function get_mapel()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $dt_mapel = $this->m_mapel->get_one_jen($id_jenjang);
        $data = "<option value=''>Pilih Mapel</option>";
        foreach ($dt_mapel->result() as $row) {
            $data .= "<option value='$row->id_mapel'>$row->mapel</option>\n";
        }
        echo $data;
    }

}
