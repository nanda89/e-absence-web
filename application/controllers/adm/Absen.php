<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_absen');
		$this->load->model('m_pesan');	
		$this->load->model('m_jenjang');		
		//===== Load Library =====
		$this->load->library('upload');		

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page						= "absen";
		$data['isi']    = "absen";		
		$data['title']	= "Rekap Absensi Kelas";			
		$data['judul1']	= "Rekap Absensi Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$tgl 			= date('d-m-Y');
		$name = $this->session->userdata('username');
		//$data['dt_wali_kelas'] = $this->m_absen->get_all();		
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
					ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
	        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)              
	        ORDER BY tabel_siswakelas.id_kelas ";		
		$data['dt_wali_kelas'] 	= $this->db->query($sql);
		$data['dt_jenjang'] = $this->m_jenjang->get_all();							
		$data['dt_kelas']	= $this->m_jenjang->get_all_kelas();
		
		$this->template($page, $data);	
	}
	public function ajax_bulk_delete()
	{
		$tabel			= "tabel_absen";
		$pk 				= "id_absen";
		$list_id 		= $this->input->post('id');
		foreach ($list_id as $id) {
			$this->m_admin->delete($tabel,$pk,$id);
		}
		echo json_encode(array("status" => TRUE));
	}
	public function export_all()
	{						
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
	        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
					ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
	        ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
	        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
	        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) ORDER BY tabel_siswakelas.id_kelas " ;	
		$data['dt_wali_kelas'] 	= $this->db->query($sql);
		$this->load->view('panel/excel_absen_all',$data);
	}
	public function filter()
	{		
		$page						= "absen";
		$data['isi']    = "absen";		
		$data['title']	= "Rekap Absensi Kelas";			
		$data['judul1']	= "Rekap Absensi Kelas";			
		$data['judul2']	= "";					
		$data['set']	= "filter";		
		$id_kelas 		= $this->input->get('id_kelas');
		$tgl_awal 		= $this->input->get('tgl_awal');
		$data['tgl_awal'] = $tgl_awal;
		$tgl_akhir 		= $this->input->get('tgl_akhir');
		$data['tgl_akhir'] = $tgl_akhir;
		$status 			= $this->input->get('status');
		$data['status'] = $status;
		if($id_kelas<>'' AND $status <> '' AND $tgl_awal == '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir <> ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='hadir'){
				$kondisi = "WHERE  AND tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE  tabel_kelas.id_kelas='$id_kelas' ";
			}			
		}
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas, tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
			ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
			ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
      ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
			ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
			ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta)                
			".$kondisi. "ORDER BY tabel_siswakelas.id_kelas";
		$data['dt_wali_kelas'] = $this->db->query($sql);									
		$this->template($page, $data);	
	}
	public function export_filter()
	{						
		$id_kelas 		= $this->input->get('id_kelas');
		$tgl_awal 		= $this->input->get('tgl_awal');
		$tgl_akhir 		= $this->input->get('tgl_akhir');
		$status 			= $this->input->get('status');
		if($id_kelas<>'' AND $status <> '' AND $tgl_awal == '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir <> ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}			
		}
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas, tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
			ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
			ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
      ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
			ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
			ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
			ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)	               
			".$kondisi."
			ORDER BY tabel_absen.id_absen DESC";
		$data['dt_wali_kelas'] = $this->db->query($sql);		
		$this->load->view('panel/excel_absen_all',$data);
	}
	public function sms()
	{		
		$id 			= $this->input->get('id');
		$r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();
		$page			= "sms_absen";
		$data['isi']    = "absen";		
		$data['title']	= "Set Notifikasi";			
		$data['judul1']	= "Set Notifikasi ".$r->jenis;			
		$data['judul2']	= "";					
		$data['set']	= "set_sms";		
		$id_kelas 		= $this->input->get('id_kelas');
		$tgl_awal 		= $this->input->get('tgl_awal');
		$tgl_akhir 		= $this->input->get('tgl_akhir');
		$status 		= $this->input->get('status');
		if($id_kelas<>'' AND $status <> '' AND $tgl_awal == '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir <> ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}			
		}
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp,tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
			tabel_absen.jam_masuk,tabel_absen.jam_pulang
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
			ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
			ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
      ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
			ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
			ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
			ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)	               
			".$kondisi."
			ORDER BY tabel_absen.id_absen DESC";
		$data['dt_wali_kelas'] = $this->db->query($sql);									
		$this->template($page, $data);	
	}
	public function export_kirim()
	{						
		$id 			= $this->input->get('id');
		$r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();		
		$data['judul']	= $r->jenis;							
		$id_kelas 		= $this->input->get('id_kelas');
		$tgl_awal 		= $this->input->get('tgl_awal');
		$tgl_akhir 		= $this->input->get('tgl_akhir');
		$status 		= $this->input->get('status');
		if($id_kelas<>'' AND $status <> '' AND $tgl_awal == '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir <> ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}			
		}
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp, tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
			tabel_absen.jam_masuk,tabel_absen.jam_pulang
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
			ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
			ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
      ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
			ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
			ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
			ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)	               
			".$kondisi."
			ORDER BY tabel_absen.id_absen DESC";
		$data['dt_wali_kelas'] = $this->db->query($sql);		
		$this->load->view('panel/excel_sms',$data);
	}
	public function sms_kirim()
	{						
		$id 			= $this->input->get('id');
		$r 				= $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();		

		$data['judul']	= $r->jenis;							
		$id_kelas 		= $this->input->get('id_kelas');
		$tgl_awal 		= $this->input->get('tgl_awal');
		$tgl_akhir 		= $this->input->get('tgl_akhir');
		$status 			= $this->input->get('status');
		if($id_kelas<>'' AND $status <> '' AND $tgl_awal == '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir == ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl='$tgl_awal'";
			}			
		}elseif($id_kelas<>'' AND $status <> '' AND $tgl_awal <> '' AND $tgl_akhir <> ''){
			if($status=='semua'){
				$kondisi = "WHERE tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk='hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='tidak_hadir'){
				$kondisi = "WHERE tabel_absen.absen_masuk<>'hadir' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}elseif($status=='izin' or $status=='alpha' or $status=='sakit'){
				$kondisi = "WHERE tabel_absen.absen_masuk='$status' AND tabel_kelas.id_kelas='$id_kelas' AND tabel_absen.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'";
			}			
		}
		$sql = "SELECT tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
			tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp, tabel_siswa.nisn,
			tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
			tabel_absen.jam_masuk,tabel_absen.jam_pulang
			FROM tabel_siswakelas LEFT JOIN tabel_siswa
			ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_kelas        
			ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_jenjang
      ON (tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang) INNER JOIN tabel_guru      	
			ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
			ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
			ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)	               
			".$kondisi."
			ORDER BY tabel_absen.id_absen DESC";

		$dt_wali_kelas = $this->db->query($sql);
		foreach($dt_wali_kelas->result() as $row) {                           
			$r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();              
			if($id == 2){
	      if($row->absen_pulang == 'hadir'){
	        $find     = array("nama_i","nis_i","tgl_i","jam_i");
	        $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_pulang);
	        $isi      = str_replace($find, $replace, $r->isi);
	      }else{
	        $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
	        $find     = array("nama_i","nis_i","tgl_i","absen_i");
	        $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->absen_masuk);
	        $isi      = str_replace($find, $replace, $as->isi);
	      }                  
	    }elseif($id == 1){
	      if($row->absen_pulang == 'hadir'){
	        $find     = array("nama_i","nis_i","tgl_i","jam_i");
	        $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_masuk);
	        $isi      = str_replace($find, $replace, $r->isi);
	      }else{
	        $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
	        $find     = array("nama_i","nis_i","tgl_i","absen_i");
	        $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->absen_masuk);
	        $isi      = str_replace($find, $replace, $as->isi);
	      }
	    }elseif($id == 3){
			  $hadir    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
						  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
						  AND absen_masuk = 'hadir' AND valid_masuk = 'valid'")->row();
			  $alpa     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
						  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
						  AND absen_masuk = 'alpha' AND valid_masuk = 'valid'")->row();
			  $izin     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
						  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
						  AND absen_masuk = 'izin' AND valid_masuk = 'valid'")->row();
			  $sakit    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
						  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
						  AND absen_masuk = 'sakit' AND valid_masuk = 'valid'")->row();
			  $find     = array("nama_i","nis_i","tgl1_i","tgl2_i","hadir_i","alpa_i","izin_i","sakit_i");
			  $replace  = array($row->nama_lengkap,$row->id_siswa,$tgl_awal,$tgl_akhir,$hadir->jum,$alpa->jum,$izin->jum,$sakit->jum);
			  $isi      = str_replace($find, $replace, $r->isi);
			}	

			$waktu = gmdate("Y-m-d H:i:s", time()+60*60*7);
			$date=date_create($waktu);
			$date = strtotime($waktu."+ 5 minutes");
			$h = date('Y-m-d H:i:s',$date);


			$myArray = array();
			$d	= $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();			        
			$tujuan = $d->link_tujuan;				

			if($d->metode == 'Sewa Server'){
				$a = $d->kode_aktivasi;
				$b = $h;
				$c = $isi;
				$d = $row->no_hp;
				$e = $r->jenis;
				$f = base_url()."adm/absen";			    									
				$myArray[] 	= array("kode" => $a, "waktu" => $b,"isi" => $c,"no_hp" => $d,"jenis" => $e,"link_lagi" => $f);							

				$url = $tujuan.'assets/json_file/simpan.php/';
				$ch = curl_init($url);
					
				$payload = json_encode(array("rekap"=>$myArray));
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
				//curl_setopt( $ch, CURLOPT_HEADER, true);
				curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
				# Return response instead of printing.
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				# Send request.
				$result = curl_exec($ch);
				curl_close($ch);
				# Print response.
				echo "<pre>$result</pre>";

				$_SESSION['tipe'] 	= "info";

			}elseif($d->metode == 'Kirim Sendiri'){
				$dt['DestinationNumber'] 	= $row->no_hp;
				$dt['SendingDateTime'] 		= $h;
				$dt['TextDecoded'] 				= $isi;
				$this->db->insert("outbox",$dt);
				$_SESSION['pesan'] 	= "Pesan berhasil tersimpan di outbox, akan terkirim dalam jangka waktu 10 menit kemudian";
				$_SESSION['tipe'] 	= "info";
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/absen'>";
			}
		}	    	    			

	   
	}
}
?>