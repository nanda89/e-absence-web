<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sinkron_absen_guru_rekap extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        # CORE
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        $this->load->database();
        $this->load->helper('url');

        $this->load->model('m_admin');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_pesan');
        $this->load->model('m_jenjang');
        $this->load->model('m_semua');
        $this->load->model('m_guru');
        $this->load->model('m_siswakelas');
        $this->load->model('m_absen_mapel');
        $this->load->model('m_jadwal');

    }

    public function cek_hari($tgl)
    {
        $hari = $day = $tgl;
        switch ($hari) {
            case "Sunday":$hari = "Minggu";
                break;
            case "Monday":$hari = "Senin";
                break;
            case "Tuesday":$hari = "Selasa";
                break;
            case "Wednesday":$hari = "Rabu";
                break;
            case "Thursday":$hari = "Kamis";
                break;
            case "Friday":$hari = "Jumat";
                break;
            case "Saturday":$hari = "Sabtu";
                break;
        }
        return $hari;
    }

    public function hari()
    {
        $hari = $day = gmdate("l", time() + 60 * 60 * 7);
        switch ($hari) {
            case "Sunday":$hari = "Minggu";
                break;
            case "Monday":$hari = "Senin";
                break;
            case "Tuesday":$hari = "Selasa";
                break;
            case "Wednesday":$hari = "Rabu";
                break;
            case "Thursday":$hari = "Kamis";
                break;
            case "Friday":$hari = "Jumat";
                break;
            case "Saturday":$hari = "Sabtu";
                break;
        }
        return $hari;
    }

    public function hari_apa($s)
    {
        if ($s == "0 Minggu") {
            return "Sunday";
        } else if ($s == "1 Senin") {
            return "Monday";
        } else if ($s == "2 Selasa") {
            return "Tuesday";
        } else if ($s == "3 Rabu") {
            return "Wednesday";
        } else if ($s == "4 Kamis") {
            return "Thursday";
        } else if ($s == "5 Jumat") {
            return "Friday";
        } else if ($s == "6 Sabtu") {
            return "Saturday";
        }
    }

    public function datediff($tgl1, $tgl2)
    {
        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        return array("years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff));
    }

    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer', $data);
    }

    public function index()
    {
        $page = "sinkron_absen_guru_rekap";
        $data['title'] = "Sinkron Mengajar Guru";
        $data['isi'] = "sinkron_absen_guru_rekap";
        $data['judul1'] = "Sinkron Mengajar Guru";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['guru'] = $this->m_guru->get_all();
        $data['jenjang'] = $this->m_jenjang->get_all();
        $data['tahunajaran'] = $this->m_tahunajaran->get_all();
        $this->template($page, $data);
    }

    public function filter()
    {
        $page = "sinkron_absen_guru_rekap";
        $data['title'] = "Sinkron Mengajar Guru";
        $data['isi'] = "sinkron_absen_guru_rekap";
        $data['judul1'] = "Sinkron Mengajar Guru";
        $data['judul2'] = "";
        $data['set'] = "filter";
        $data['tgl'] = $this->input->post('tgl');
        $data['tgl_akhir'] = $this->input->post('tgl2');
        $data['scripts'] = '$.WebAbsence.modul.sinkron.mengajar()';
        $data['jenis'] = $this->input->post('jenis');
        $data['id_guru'] = $this->input->post('guru');
        if ($data['id_guru'] != '') {
            $data['nama_guru'] = $this->m_guru->get_one($data['id_guru'])->row()->nama;
        } else {
            $data['nama_guru'] = '';
        }

        $data['id_jenjang'] = $this->input->post('jenjang');
        if ($data['id_jenjang'] != '') {
            $data['jenjang'] = $this->m_jenjang->get_one($data['id_jenjang'])->row()->jenjang;
        } else {
            $data['jenjang'] = '';
        }

        $data['id_ta'] = $this->input->post('tahunajaran');
        if ($data['id_ta'] != '') {
            $data['tahunajaran'] = $this->m_tahunajaran->get_one($data['id_ta'])->row()->tahun_ajaran;
        } else {
            $data['tahunajaran'] = '';
        }

        $this->template($page, $data);
    }

    public function export_filter()
    {
        $page = "sinkron_absen_guru_rekap";
        $data['title'] = "Rekap Validasi Absen Guru";
        $data['isi'] = "sinkron_absen_guru_rekap";
        $data['judul1'] = "Rekap Validasi Absen Guru";
        $data['judul2'] = "";
        $data['set'] = "filter";
        $data['tgl'] = $this->input->get('tgl');
        $data['tgl_akhir'] = $this->input->get('tgl_akhir');
        $data['jenis'] = $this->input->get('jenis');
        $data['id_guru'] = $this->input->get('id_guru');
        if ($data['id_guru'] != '') {
            $data['nama_guru'] = $this->m_guru->get_one($data['id_guru'])->row()->nama;
        } else {
            $data['nama_guru'] = '';
        }

        $data['id_jenjang'] = $this->input->get('id_jenjang');
        if ($data['id_jenjang'] != '') {
            $data['jenjang'] = $this->m_jenjang->get_one($data['id_jenjang'])->row()->jenjang;
        } else {
            $data['jenjang'] = '';
        }

        $data['id_ta'] = $this->input->get('id_ta');
        if ($data['id_ta'] != '') {
            $data['tahunajaran'] = $this->m_tahunajaran->get_one($data['id_ta'])->row()->tahun_ajaran;
        } else {
            $data['tahunajaran'] = '';
        }

        $a = $this->input->get('a');
        if ($a == "1") {
            $this->load->view('panel/excel_absen_guru_rekap', $data);
        } elseif ($a == "2") {
            $this->load->view('panel/pdf_absen_guru_rekap', $data);
        }
    }

    public function export_now()
    {
        $page = "sinkron_absen_guru_rekap";
        $data['title'] = "Rekap Validasi Absen Guru";
        $data['isi'] = "sinkron_absen_guru_rekap";
        $data['judul1'] = "Rekap Validasi Absen Guru";
        $data['judul2'] = "";
        $data['set'] = "filter";
        $data['tgl'] = gmdate("d-m-Y", time() + 60 * 60 * 7);
        $data['tgl_akhir'] = gmdate("d-m-Y", time() + 60 * 60 * 7);
        $data['jenis'] = "Semua";
        $data['id_guru'] = '';
        $data['nama_guru'] = '';
        $a = $this->input->get('a');
        if ($a == "1") {
            $this->load->view('panel/excel_absen_guru_rekap', $data);
        } elseif ($a == "2") {
            $this->load->view('panel/pdf_absen_guru_rekap', $data);
        }
    }

    public function absen_manual_mengajar()
    {
        /*
        You will get 'pk', 'name' and 'value' in $_POST array.
         */
        $id_jadwal = $this->input->post('pk');
        $tgl = $this->input->post('tgl');
        $name = $this->input->post('name');
        $absen = $this->input->post('value');
        $valid = 'valid';
        $keterangan = strtolower(str_replace(' ', '_', $absen));
        /*
        Check submitted value
         */
        if (!empty($absen)) {

            $cek = $this->db->query("SELECT * FROM tabel_absen_mapel WHERE tgl='$tgl' AND id_jadwal='$id_jadwal' AND valid='$valid'");

            if ($cek->num_rows() > 0) {
                $this->db->query("UPDATE tabel_absen_mapel SET absen='$absen', keterangan='$keterangan' WHERE tgl='$tgl' AND id_jadwal='$id_jadwal' AND valid='$valid'");
            } else {
                $data = array(
                    'id_jadwal' => $id_jadwal,
                    'tgl' => $tgl,
                    'absen' => $absen,
                    'valid' => $valid,
                    'keterangan' => $keterangan,
                );
                $this->db->insert('tabel_absen_mapel', $data);
            }

            /*
            If value is correct you process it (for example, save to db).
            In case of success your script should not return anything, standard HTTP response '200 OK' is enough.

            for example:
            $result = mysql_query('update users set '.mysql_escape_string($name).'="'.mysql_escape_string($value).'" where user_id = "'.mysql_escape_string($pk).'"');
             */

            //here, for debug reason we just return dump of $_POST, you will see result in browser console
            print_r($cek->num_rows());
        } else {
            /*
            In case of incorrect value or error you should return HTTP status != 200.
            Response body will be shown as error message in editable form.
             */
            header('HTTP/1.0 400 Bad Request', true, 400);
            echo "This field is required!";
        }
    }

    public function sinkronisasi()
    {
        $re = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
        $tgl = explode('-', $this->input->get('tanggal'));
        //$tgl    = "2019-01-01";
        $tgl_akhir = explode('-', $this->input->get('tgl_akhir'));
        $jenis = $this->input->get('jenis');
        $id_guru = $this->input->get('id_guru');
        $tgl2 = $tgl;

        if ($id_guru != '') {
            $nama_guru = $this->m_guru->get_one($id_guru)->row()->nama;
        } else {
            $nama_guru = '';
        }

        $id_jenjang = $this->input->get('id_jenjang');
        if ($id_jenjang != '') {
            $jenjang = $this->m_jenjang->get_one($id_jenjang)->row()->jenjang;
        } else {
            $jenjang = '';
        }

        $id_ta = $this->input->get('id_ta');
        if ($id_ta != '') {
            $tahunajaran = $this->m_tahunajaran->get_one($id_ta)->row()->tahun_ajaran;
        } else {
            $tahunajaran = '';
        }

        $tgl2 = $tgl;

        $args = array(
            'jenis' => $jenis,
            'id_guru' => $id_guru,
            'tgl' => $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0],
            'tgl_akhir' => $tgl_akhir[2] . '-' . $tgl_akhir[1] . '-' . $tgl_akhir[0],
        );
        $absensi = get_all_harian_absen_mengajar_guru($args);
        $data_absen = array();
        foreach ($absensi as $absen) {
            $data_absen[] = (array) $absen;
        }

        ini_set('max_execution_time', 300);
        $settings = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
        $jenjang = $this->db->query("SELECT * FROM tabel_jenjang ORDER BY id_jenjang LIMIT 0,1")->row();
        $kode_aktivasi = $settings->kode_aktivasi;
        $nama_sekolah = $settings->logo_besar;
        $link_sinkron = $settings->link_sinkron;
        $kota = $settings->kota;
        $pimpinan = $settings->pimpinan;
        $jenjang = $jenjang->jenjang;

        $array = array(
            'data_absen' => $data_absen,
            'jenjang' => $jenjang,
            'pimpinan' => $pimpinan,
            'kota' => $kota,
            'kode_aktivasi' => $kode_aktivasi,
            'nama_sekolah' => $nama_sekolah,
        );
        $payload = json_encode($array, JSON_PRETTY_PRINT);

        //echo $payload;
        $json_cek = file_get_contents($link_sinkron . "sync/cek_sync/" . $kode_aktivasi);
        $obj = json_decode($json_cek, true);

        //echo $obj;
        if ($obj == 1) {
            $url_post = $link_sinkron . "sync/sync_rekap_absen_guru/do_sync";
            $ch = curl_init($url_post);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            ));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            $_SESSION['pesan'] = "Data berhasil disinkronisasi dengan web monitoring ({elapsed_time} seconds)";
            $_SESSION['tipe'] = "info";
            //echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cek_absen_guru_rekap'>";
            echo "ok";

        } else {
            $_SESSION['pesan'] = "Sekolah anda belum terdaftar di database web dinas";
            $_SESSION['tipe'] = "danger";
            //echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cek_absen_guru_rekap'>";
            echo "ok";
        }
    }

    public function sinkronisasi2()
    {
        ini_set('max_input_vars', 10000);
        $no = 1;
        $jum = $_POST['jumx'];

        for ($i = 1; $i <= $jum - 1; $i++) {
            $re = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();

            $json = file_get_contents($re->link_tujuan . "assets/file/link.php", true);
            //$json = file_get_contents("http://localhost/web_absence_server/assets/file/link.php",true);

            $jenjang = $this->db->query("SELECT * FROM tabel_jenjang ORDER BY id_jenjang LIMIT 0,1")->row();

            $a = $_POST["id_guru_" . $i];
            $b = $_POST["namax_" . $i];
            $c = $_POST["kelasx_" . $i];
            $d = date("Y-m-d", strtotime($_POST["tglx_" . $i]));
            $e = $_POST["jam_absenx_" . $i];
            $f = $_POST["valx_" . $i];
            $g = $_POST["ketx_" . $i];
            $h = $_POST["mapelx_" . $i];
            $jum = $_POST["jumx"];

            $data_absen[] = array(
                "id" => $a,
                "nama" => $b,
                "mengajar" => $c,
                "tgl" => $d,
                "jam_absen" => $e,
                "val" => $f,
                "ket" => $g,
                "mapel" => $h,
            );

            //$result = file_get_contents('http://localhost/web_dashboard2/assets/json/simpan_mengajar.php?', false, $context);
        }
        $settings = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
        $jenjang = $this->db->query("SELECT * FROM tabel_jenjang ORDER BY id_jenjang LIMIT 0,1")->row();
        $kode_aktivasi = $settings->kode_aktivasi;
        $nama_sekolah = $settings->logo_besar;
        $link_sinkron = $settings->link_sinkron;
        $kota = $settings->kota;
        $pimpinan = $settings->pimpinan;
        $jenjang = $jenjang->jenjang;

        $payload = json_encode($array = array('data_absen' => $data_absen, 'jenjang' => $jenjang, 'pimpinan' => $pimpinan, 'kota' => $kota, 'kode_aktivasi' => $kode_aktivasi, 'nama_sekolah' => $nama_sekolah), JSON_PRETTY_PRINT);
        //echo $payload;
        //$url_post = $link_sinkron."sync/sync_rekap_absen_guru/do_sync";
        $json_cek = file_get_contents($link_sinkron . "sync/cek_sync/" . $kode_aktivasi);
        $obj = json_decode($json_cek, true);
        //echo $obj;
        if ($obj == 1) {
            $url_post = $link_sinkron . "sync/sync_rekap_absen_guru/do_sync";
            $ch = curl_init($url_post);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            ));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            $_SESSION['pesan'] = "Data berhasil disinkronisasi dengan web monitoring ({elapsed_time} seconds)";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/cek_absen_guru_rekap'>";
        } else {
            $_SESSION['pesan'] = "Sekolah ini tidak terdaftar di web monitoring";
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/cek_absen_guru_rekap'>";
        }
    }

    public function get_absensi_mapel_by_id_kelas()
    {
        $id_kelas = $_POST['id_kelas'];
        $id_jadwal = $_POST['id_jadwal'];
        $tgl = $_POST['tgl'];

        $response = $this->m_siswakelas->get_absensi_mapel_by_id_kelas($id_kelas, $id_jadwal, $tgl);

        echo json_encode($response);
    }

    public function change_absence_all($dtlength)
    {
        $query = '';
        for ($i = 0; $i < $dtlength; $i++) {
            $id_penempatan = $_POST['id_penempatan'][$i];
            $id_jadwal = $_POST['id_jadwal'][$i];
            $jam_absen = $_POST['jam_absen'][$i];
            $tgl = $_POST['tgl'][$i];
            $absen = $_POST['absen'][$i];

            $jadwal = $this->m_jadwal->get_jadwal_by_id($id_jadwal)->row();

            $row = $this->m_absen_mapel->get_by_penempatan_jadwal_tgl($id_penempatan, $id_jadwal, $tgl);
            $data = array(
                'id_penempatan' => $id_penempatan,
                'id_jadwal' => $id_jadwal,
                'tgl' => $tgl,
                'jam' => $absen === 'sakit' ? '' : $jam_absen,
                'absen' => $absen,
                'valid' => 'valid',
                'status' => $jadwal->status === '' ? $jadwal->status : '',
                'keterangan' => $absen,
            );
            if ($row != null) {
                $query = $this->m_absen_mapel->update($row['id_absen_mapel'], $data);
                flush();
            } else {
                $query = $this->m_absen_mapel->add($data);
            }

        }

        redirect('adm/sinkron_absen_guru_rekap');
    }

    public function change_absence()
    {
        $id_penempatan = $_POST['id_penempatan'];
        $id_jadwal = $_POST['id_jadwal'];
        $jam_absen = $_POST['jam_absen'];
        $tgl = $_POST['tgl'];
        $absen = $_POST['absen'];

        $jadwal = $this->m_jadwal->get_jadwal_by_id($id_jadwal)->row();

        $row = $this->m_absen_mapel->get_by_penempatan_jadwal_tgl($id_penempatan, $id_jadwal, $tgl);
        $data = array(
            'id_penempatan' => $id_penempatan,
            'id_jadwal' => $id_jadwal,
            'tgl' => $tgl,
            'jam' => $absen === 'sakit' ? '' : $jam_absen,
            'absen' => $absen,
            'valid' => 'valid',
            'status' => $jadwal->status === '' ? $jadwal->status : '',
            'keterangan' => $absen,
        );
        if ($row != null) {
            $query = $this->m_absen_mapel->update($row['id_absen_mapel'], $data);
        } else {
            $query = $this->m_absen_mapel->add($data);
        }

        if ($query) {
            http_response_code(200);
        }
    }
}
