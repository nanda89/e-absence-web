<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_guru');
        $this->load->model('m_jenjang');
        $this->load->model('m_pesan');
        $this->load->model('m_mapel');
        $this->load->model('m_semua');
        //===== Load Library =====
        $this->load->library('upload');

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "guru";
        $data['isi'] = "guru";
        $data['title'] = "Guru";
        $data['judul1'] = "Guru";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_guru'] = $this->m_guru->get_all();
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $this->template($page, $data);
    }

    public function add()
    {
        $page = "guru";
        $data['isi'] = "guru";
        $data['title'] = "Guru";
        $data['judul1'] = "Tambah Data Guru";
        $data['judul2'] = "";
        $data['set'] = "insert";
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $this->template($page, $data);
    }
    public function ajax_bulk_delete()
    {
        $tabel = "tabel_guru";
        $pk = "id_guru";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        echo json_encode(array("status" => true));
    }
    public function delete_multiple()
    {
        $this->m_guru->remove_checked();
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
    }
    public function cari_mapel()
    {
        $id = $this->input->post('id_mapel');
        $dt_mapel = $this->m_mapel->get_one($id)->row();
        echo $dt_mapel->id_mapel . "|" . $dt_mapel->mapel;
    }
    public function t_mapel()
    {
        $id = $this->input->post('nik');
        $dq = "SELECT * FROM tabel_pengampu INNER JOIN tabel_mapel ON tabel_pengampu.id_mapel=tabel_mapel.id_mapel
		INNER JOIN tabel_kat_mapel ON tabel_mapel.id_kat_mapel = tabel_kat_mapel.id_kat_mapel
		WHERE tabel_pengampu.nik = '$id'";
        $data['dt_mapel'] = $this->db->query($dq);
        $this->load->view('panel/t_mapel', $data);
    }

    public function delete_mapel()
    {
        $id = $this->input->post('id_ampu');
        $da = "DELETE FROM tabel_pengampu WHERE id_pengampu = '$id'";
        $this->db->query($da);
        echo "nihil";
    }
    public function save_mapel()
    {
        $nik = $this->input->post('nik');
        $id_mapel = $this->input->post('id_mapel');
        $c = $this->db->query("SELECT * FROM tabel_pengampu WHERE nik ='$nik' AND id_mapel = '$id_mapel'");
        if ($c->num_rows() == 0) {
            $data['nik'] = $this->input->post('nik');
            $data['id_mapel'] = $this->input->post('id_mapel');
            $this->db->insert('tabel_pengampu', $data);
            echo "nihil";
        } else {
            echo "nothing";
        }
    }
    public function save()
    {
        if ($this->input->post('save') == 'save') {
            //setting konfigurasi upload gambar
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('gambar')) {
                $gambar = "";
            } else {
                $gambar = $this->upload->file_name;
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['nik'] = $this->input->post('nik');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nama'] = $this->input->post('nama');
            $data['no_ktp'] = $this->input->post('no_ktp');
            $data['alias'] = $this->input->post('alias');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['ijazah'] = $this->input->post('ijazah');
            $data['pend_terakhir'] = $this->input->post('pend_terakhir');
            $data['alamat'] = $this->input->post('alamat');
            $data['unit_kerja'] = $this->input->post('unit_kerja');
            $data['tmt'] = $this->input->post('tmt');
            $data['status_guru_bk'] = $this->input->post('status_guru_bk');
            $data['status_kepsek'] = $this->input->post('status_kepsek');
            $data['gambar'] = $gambar;

            $user['nama'] = $this->input->post('alias');
            $user['username'] = $this->input->post('nik');
            $user['password'] = md5($this->input->post('password'));
            $user['level'] = "guru";
            $user['avatar'] = $gambar;
            $user['tgl_daftar'] = $this->input->post('tmt');

            $this->db->trans_begin();

            $this->m_guru->tambah($data);

            if ($this->input->post('login_access') == "1") {
                $this->m_admin->tambah($user);
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $_SESSION['pesan'] = "Gagal tersimpan!";
                $_SESSION['tipe'] = "danger";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";

            } else {
                $this->db->trans_commit();

                $_SESSION['pesan'] = "Berhasil tersimpan!";
                $_SESSION['tipe'] = "info";
                echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";

            }

        }
    }

    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'ubah') {
            $page = "guru";
            $data['one_post'] = $this->m_guru->get_one($id);
            $data['title'] = "Guru";
            $data['judul1'] = "Edit Data Guru";
            $data['isi'] = "guru";
            $data['judul2'] = "";
            $data['set'] = "edit";
            $data['dt_jenjang'] = $this->m_jenjang->get_all();
            $data['dt_mapel'] = $this->m_mapel->get_all();
            $this->template($page, $data);
        }
        //DETAIL DATA
        elseif ($set == 'detail') {
            $page = "guru";
            $data['one_post'] = $this->m_guru->get_one($id);
            $data['title'] = "Guru";
            $data['judul1'] = "Detail Data Guru";
            $data['isi'] = "guru";
            $data['judul2'] = "";
            $data['set'] = "detail";
            $data['dt_mapel'] = $this->m_mapel->get_all();
            $this->template($page, $data);
        }
        //EDIT DATA KEGIATAN
        elseif ($set == 'edit') {
            $config['upload_path'] = './assets/panel/images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $data['gambar'] = $this->upload->file_name;

                $one = $this->m_guru->get_one($id);
                $row = $one->row();
                unlink("assets/panel/images/" . $row->gambar); //Hapus Gambar
            }

            $no_hp = $this->input->post('no_hp');
            $p = substr($no_hp, 0, 2);
            $p2 = substr($no_hp, 0, 1);
            if ($p == '08') {
                $hasil = substr_replace($no_hp, "+628", 0, 2);
            } else {
                $hasil = $no_hp;
            }

            if ($p2 == '8') {
                $hasil = substr_replace($no_hp, "+628", 0, 1);
            } elseif ($p2 == '6') {
                $hasil = substr_replace($no_hp, "+6", 0, 1);
            }

            $data['nik'] = $this->input->post('nik');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['nama'] = $this->input->post('nama');
            $data['no_ktp'] = $this->input->post('no_ktp');
            $data['alias'] = $this->input->post('alias');
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['tempat_lahir'] = $this->input->post('tempat_lahir');
            $data['tgl_lahir'] = $this->input->post('tgl_lahir');
            $data['agama'] = $this->input->post('agama');
            $data['no_hp'] = $hasil;
            $data['email'] = $this->input->post('email');
            $data['ijazah'] = $this->input->post('ijazah');
            $data['pend_terakhir'] = $this->input->post('pend_terakhir');
            $data['alamat'] = $this->input->post('alamat');
            $data['unit_kerja'] = $this->input->post('unit_kerja');
            $data['tmt'] = $this->input->post('tmt');
            $data['status_guru_bk'] = $this->input->post('status_guru_bk');
            $data['status_kepsek'] = $this->input->post('status_kepsek');
            $this->m_guru->edit($id, $data);
            $_SESSION['pesan'] = "Berhasil tersimpan!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_guru->hapus($id);
            $_SESSION['pesan'] = "Berhasil dihapus!";
            $_SESSION['tipe'] = "danger";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
        } elseif ($set == 'set_akun') {
            $data['username'] = $this->input->post('nik');
            $data['password'] = md5("sari123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "guru";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "guru-lk.png";
            } else {
                $data['avatar'] = "guru-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->tambah($data);
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
        } elseif ($set == 'set_default') {
            $data['username'] = $this->input->post('nik');
            $username = $this->input->post('nik');
            $data['password'] = md5("sari123");
            $data['nama'] = $this->input->post('nama');
            $data['level'] = "guru";
            $jk = $this->input->post('jk');
            if ($jk == "laki-laki") {
                $data['avatar'] = "guru-lk.png";
            } else {
                $data['avatar'] = "guru-pr1.png";
            }
            $data['tgl_daftar'] = date('Y-M-d');

            $this->m_admin->edit_akun($username, $data);
            $_SESSION['pesan'] = "Berhasil set default!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/guru'>";
        }
    }

}
