<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_mapel');
        $this->load->model('m_pesan');
        $this->load->model('m_semua');
        //===== Load Library =====

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "mapel";
        $data['isi'] = "mapel";
        $data['title'] = "Mata Pelajaran";
        $data['judul1'] = "Mata Pelajaran";
        $data['judul2'] = "";
        $data['judul1'] = "Mata Pelajaran";
        $data['set'] = "view";
        $data['dt_mapel'] = $this->m_mapel->get_all();
        $data['dt_kat_mapel'] = $this->m_mapel->get_all_kat();
        $data['dt_kat'] = $this->m_mapel->get_all_kat();
        $data['dt_jenjang'] = $this->m_semua->getAll("tabel_jenjang");
        $data['dt_jenjang2'] = $this->m_semua->getAll("tabel_jenjang");

        $this->template($page, $data);
    }

    public function add()
    {
        $page = "mapel";
        $data['title'] = "Mata Pelajaran";
        $data['judul1'] = "Mata Pelajaran";
        $data['isi'] = "mapel";
        $data['judul2'] = "";
        $data['judul1'] = "Mata Pelajaran";
        $data['set'] = "insert";
        $this->template($page, $data);
    }
    public function ajax_bulk_delete()
    {
        $tabel = "tabel_mapel";
        $pk = "id_mapel";
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->m_admin->delete($tabel, $pk, $id);
        }
        // echo json_encode(array("status" => true));
        $_SESSION['pesan2'] = "Berhasil dihapus!";
        $_SESSION['tipe2'] = "info";
        echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";

    }

    public function save()
    {
        if ($this->input->post('save') == 'save') {
            $data['kategori_mapel'] = $this->input->post('kategori_mapel');
            $this->m_mapel->tambah_kat($data);
            $_SESSION['pesan'] = "Berhasil tersimpan!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
    }

    public function save_mapel()
    {
        if ($this->input->post('save') == 'save') {
            $data['id_kat_mapel'] = $this->input->post('id_kat_mapel');
            $data['mapel'] = $this->input->post('mapel');
            $data['kkm'] = $this->input->post('kkm');
            $data['id_jenjang'] = $this->input->post('id_jenjang');

            $katMapel = $this->m_mapel->get_one_kat($data['id_kat_mapel']);
            $dt = $katMapel->row();
            $jenis = $dt->kategori_mapel;
            $data['jenis'] = $jenis;

            $this->m_mapel->tambah($data);
            $_SESSION['pesan2'] = "Berhasil tersimpan!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
    }

    public function cari()
    {
        $id = $this->input->post('id');
        $kategori_mapel = $this->m_mapel->get_one_kat($id);
        $row = $kategori_mapel->row();
        if ($kategori_mapel->num_rows() > 0) {
            echo $row->id_kat_mapel . "|" . $row->kategori_mapel;
        }
    }

    public function cari_mapel()
    {
        $id = $this->input->post('id');
        $mapel = $this->m_mapel->get_one($id);
        $row = $mapel->row();
        if ($mapel->num_rows() > 0) {
            echo $row->id_mapel . "|" . $row->mapel . "|" . $row->id_kat_mapel . "|" . $row->kkm . "|" . $row->id_jenjang;
        }
    }

    public function process()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'edit') {
            $data['kategori_mapel'] = $this->input->post('kategori_mapel');
            $this->m_mapel->edit_kat($id, $data);
            $_SESSION['pesan'] = "Berhasil tersimpan!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_mapel->hapus_kat($id);
            $_SESSION['pesan'] = "Berhasil dihapus!";
            $_SESSION['tipe'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
    }

    public function process_mapel()
    {
        $id = $this->input->post('id');
        $set = $this->input->post('s_process');
        //FORM EDIT KEGIATAN
        if ($set == 'edit') {
            $data['id_kat_mapel'] = $this->input->post('id_kat_mapel');
            $id = $this->input->post('id_mapel');
            $data['kkm'] = $this->input->post('kkm');
            $data['mapel'] = $this->input->post('mapel');
            $data['id_jenjang'] = $this->input->post('id_jenjang');
            $data['jenis'] = $this->input->post('id_jenis_mapel');
            $this->m_mapel->edit($id, $data);
            $_SESSION['pesan2'] = "Berhasil tersimpan!";
            $_SESSION['tipe2'] = "info";

            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
        //HAPUS DATA KEGIATAN
        elseif ($set == 'hapus') {
            $this->m_mapel->hapus($id);
            $_SESSION['pesan2'] = "Berhasil dihapus!";
            $_SESSION['tipe2'] = "info";
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        } else {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "adm/mapel'>";
        }
    }

}
