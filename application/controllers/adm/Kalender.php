<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender extends CI_Controller {
  
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		//$name = $this->session->userdata('nama');		
		if (!$this->session->userdata('nama'))
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_kalender');
		$this->load->model('m_pesan');		
		//===== Load Library =====
	}
  
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer',$data);
	}

	public function index()
	{		
		$page			= "kalender";
		$data['isi']    = "kalender";		
		$data['title']	= "SIAKAD | Kalender";			
		$data['judul1']	= "Kalender";			
		$data['judul2']	= "";			
		$data['judul1']	= "Kalender";
		$data['set']	= "view";
		$data['dt_ka']	= $this->m_kalender->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			        = "kalender";
		$data['isi']      = "kalender";				
		$data['title']	  = "SIAKAD | Kalender";			
		$data['judul1']	  = "Kalender";			
		$data['judul2']	  = "";			
		$data['judul1']	  = "Kalender";
		$data['set']	    = "insert";	
    $data['scripts']  = '$.WebAbsence.modul.kalender.agenda.form_init();';
		$this->template($page, $data);	
	}
		
	public function ajax_bulk_delete()
	{
		$tabel			= "tabel_kalender";
		$pk 				= "id_kalender";
		$list_id 		= $this->input->post('id');
		foreach ($list_id as $id) {
			$this->m_admin->delete($tabel,$pk,$id);
		}
		echo json_encode(array("status" => TRUE));
	}
  
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			// $tanggal = explode('-', $this->input->post('tanggal'));
			// $data['tanggal']	= $tanggal[2] . '-' . $tanggal[1] . '-' . $tanggal[0];
      
      $agenda = $this->input->post('agenda');
      $ket    = $this->input->post('keterangan');
      $warna  = $this->input->post('warna');
      $libur  = ( !is_null($this->input->post('libur')) ) ? 1 : 0;      
      
      $rangedate = explode( ' - ', $this->input->post('tanggal') );
      $start = explode('/', $rangedate[0]);
      $end = explode('/', $rangedate[1]);
      $date_param = $start[2] . '-' . $start[0] . '-' . $start[1];
      $date_param_2 = $end[2] . '-' . $end[0] . '-' . $end[1];
      $date = new \DateTime( $date_param );
      $date2 = new \DateTime( $date_param_2 );
      $interval = $date->diff($date2);
      $count = intval( $interval->format('%R%a') );
      
      if( $count > 0 ) {
        for( $i=0; $i <= $count; $i++ ) {
          
          $data = array();
          $data['tanggal']    = $date->format('d-m-Y');
          $data['agenda']			= $agenda;	
          $data['keterangan']	= $ket;
          $data['warna']		  = $warna;
          $data['libur']		  = $libur;
          $this->m_kalender->tambah($data);
          
          $date->add( new \DateInterval('P1D') );
          
        }
      } else {
        
          $data = array();
          $data['tanggal']    = $date->format('d-m-Y');
          $data['agenda']			= $agenda;	
          $data['keterangan']	= $ket;
          $data['warna']		  = $warna;
          $data['libur']		  = $libur;
          $this->m_kalender->tambah($data);
        
      }
      

			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kalender'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "kalender";		
			$data['one_post']= $this->m_kalender->get_one($id);
			$data['title']	= "SIAKAD | Kalender";			
			$data['judul1']	= "Kalender";	
			$data['isi']    = "kalender";				
			$data['judul2']	= "";			
			$data['judul1']	= "Kalender";
			$data['set']	= "edit";	
      $data['scripts'] = '$.WebAbsence.modul.kalender.agenda.form_init();';
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$tanggal = explode('-', $this->input->post('tanggal'));
			$data['tanggal']	= $this->input->post('tanggal'); //$tanggal[2] . '-' . $tanggal[1] . '-' . $tanggal[0];			
			$data['agenda']			= $this->input->post('agenda');			
			$data['keterangan']		= $this->input->post('keterangan');
      $data['warna']		= $this->input->post('warna');
			$data['libur']		= ( !is_null($this->input->post('libur')) ) ? 1 : 0;	
			$this->m_kalender->edit($id, $data);
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kalender'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_kalender->hapus($id);			
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kalender'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kalender'>";
		}
	}

}
