<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Sinkron_kehadiran_non_guru extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			# CORE
			$name = $this->session->userdata('nama');
			if ($name == "") echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";


			$this->load->database();
			$this->load->helper('url');

			$this->load->model('m_admin');
			$this->load->model('m_tahunajaran');
			$this->load->model('m_pesan');
			$this->load->model('m_jenjang');
			$this->load->model('m_semua');
			$this->load->model('m_guru');
		}

		protected function template($page, $data)
		{
			$this->load->view('t_panel/header',$data);
			$this->load->view("t_panel/aside");
			$this->load->view("panel/$page");
			$this->load->view('t_panel/footer',$data);
		}

		public function cek_hari($tgl){  
			$hari=$day= $tgl;
			switch($hari){
				case"Sunday":$hari="Minggu"; break;
				case"Monday":$hari="Senin"; break;
				case"Tuesday":$hari="Selasa"; break;
				case"Wednesday":$hari="Rabu"; break;
				case"Thursday":$hari="Kamis"; break;
				case"Friday":$hari="Jumat"; break;
				case"Saturday":$hari="Sabtu"; break;
			}  
			return $hari;
		}

		public function hari(){  
			$hari=$day= gmdate("l", time()+60*60*7);
			switch($hari){
				case"Sunday":$hari="Minggu"; break;
				case"Monday":$hari="Senin"; break;
				case"Tuesday":$hari="Selasa"; break;
				case"Wednesday":$hari="Rabu"; break;
				case"Thursday":$hari="Kamis"; break;
				case"Friday":$hari="Jumat"; break;
				case"Saturday":$hari="Sabtu"; break;
			}  
			return $hari;
		}

		public function datediff($tgl1, $tgl2){
			$tgl1 = strtotime($tgl1);
			$tgl2 = strtotime($tgl2);
			$diff_secs = abs($tgl1 - $tgl2);
			$base_year = min(date("Y", $tgl1), date("Y", $tgl2));
			$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
			return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
		}

		public function index()
		{		
			$page			      = "sinkron_kehadiran_non_guru";		
			$data['title']	= "Sinkron Kehadiran Non-Guru";			
			$data['isi']    = "sinkron_kehadiran_non_guru";
			$data['judul1']	= "Sinkron Kehadiran Non-Guru";			
			$data['judul2']	= "";					
			$data['set']	  = "view";		
			$this->template($page, $data);	
		}

		public function filter()
		{		
			$page			= "sinkron_kehadiran_non_guru";		
			$data['title']	= "Sinkron Kehadiran Non-Guru";			
			$data['isi']    = "sinkron_kehadiran_non_guru";
			$data['judul1']	= "Sinkron Kehadiran Non-Guru";			
			$data['judul2']	= "";
      $data['scripts'] = "$.WebAbsence.modul.sinkron.kehadiran_non_guru();";
			$data['set']	= "filter";
			$data['tgl']	= $this->input->post('tgl');
			$data['tgl_akhir']	= $this->input->post('tgl2');
			$data['jenis']	= $this->input->post('jenis');
			$this->template($page, $data);
		}
    
    public function absen_manual() 
    {
      /*
      You will get 'pk', 'name' and 'value' in $_POST array.
      */
      $id_guru = $this->input->post('pk');
      $tgl = $this->input->post('tgl');
      $name = $this->input->post('name');
      $value = $this->input->post('value');
      /*
       Check submitted value
      */
      if(!empty($value)) {
        
          $data = array();
          $data['keterangan'] = $value;
        
          $cek = $this->db->query("SELECT id_absen_non_guru FROM tabel_absen_non_guru WHERE id_non_guru='$id_guru' AND tgl='$tgl'");
          
          if( $cek->num_rows() > 0 ) {
            $this->db->query("UPDATE tabel_absen_non_guru SET keterangan='$value' WHERE id_non_guru='$id_guru' AND tgl='$tgl'"); 
          } else {
            $data['id_non_guru'] = $id_guru;
            $data['tgl'] = $tgl;
            $data['keterangan'] = $value;
            $this->db->insert('tabel_absen_non_guru',$data);
          }
        
          /*
            If value is correct you process it (for example, save to db).
            In case of success your script should not return anything, standard HTTP response '200 OK' is enough.

            for example:
            $result = mysql_query('update users set '.mysql_escape_string($name).'="'.mysql_escape_string($value).'" where user_id = "'.mysql_escape_string($pk).'"');
          */

          //here, for debug reason we just return dump of $_POST, you will see result in browser console
          print_r($cek->num_rows());
      } else {
          /* 
          In case of incorrect value or error you should return HTTP status != 200. 
          Response body will be shown as error message in editable form.
          */
          header('HTTP/1.0 400 Bad Request', true, 400);
          echo "This field is required!";
      }
    }

		public function export_filter()
		{		
			$page			= "sinkron_kehadiran_non_guru";		
			$data['title']	= "Sinkron Kehadiran Non-Guru";			
			$data['isi']    = "sinkron_kehadiran_non_guru";
			$data['judul1']	= "Sinkron Kehadiran Non-Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		
			$data['tgl']	= $this->input->get('tgl');
			$data['tgl_akhir']	= $this->input->get('tgl_akhir');
			$data['jenis']	= $this->input->get('jenis');
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_kehadiran_non_guru',$data);
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_kehadiran_non_guru',$data);				
			}		
		}

		public function export_now()
		{		
			$page			= "sinkron_kehadiran_non_guru";		
			$data['title']	= "Sinkron Kehadiran Non-Guru";			
			$data['isi']    = "sinkron_kehadiran_non_guru";
			$data['judul1']	= "Sinkron Kehadiran Non-Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		        
			$data['tgl']	= gmdate("d-m-Y", time()+60*60*7);
			$data['tgl_akhir']	= gmdate("d-m-Y", time()+60*60*7);
			$data['jenis']	= "Semua";
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_kehadiran_non_guru',$data);				
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_kehadiran_non_guru',$data);				
			}		
		}
    
		public function sinkronisasi()
		{
      $tgl_param = explode('-', $this->input->get('tgl'));
      $tgl_akhir_param = explode('-', $this->input->get('tgl_akhir'));
      $args = array(
        'tgl'       => $tgl_param[2] . '-' . $tgl_param[1] . '-' . $tgl_param[0],
        'tgl_akhir' => $tgl_akhir_param[2] . '-' . $tgl_akhir_param[1] . '-' . $tgl_akhir_param[0]
      );
      $absensi = get_all_harian_absen_kehadiran_non_guru($args);
      $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
			foreach ($absensi as $absen) {

        $a = $absen->id_non_guru;
        $b = $absen->nama;
        $c = date("Y-m-d", strtotime($absen->tgl));    
        $d = "non-guru";    
        $e = $absen->jam_masuk_kerja;							
        $f = $absen->jam_pulang_kerja;
        $g = $absen->validasi_jam_masuk;
        $h = $absen->validasi_jam_pulang;
        $i = $absen->keterangan_jam_masuk;
        $j = $absen->keterangan_jam_pulang;
        $k = $re->kode_aktivasi;
        $kunci = md5($a."-".$k."-".$c);


        $data_absen[] = array(
          'id'            => $a,
          'nama'          => $b,
          "waktu"         =>$c,
          "jenis"         =>$d,
          "status"        =>"non-guru",
          "jam_masuk"     =>$e,
          "jam_pulang"    =>$f,
          "val_masuk"     =>$g,
          "val_pulang"    =>$h,
          "ket_masuk"     =>$i,
          "ket_pulang"    =>$j,
          "kode_aktivasi" =>$k,
          "kunci"         =>$kunci
        );
			}
      
			ini_set('max_execution_time', 300);
			$settings       = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
			$jenjang        = $this->db->query("SELECT * FROM tabel_jenjang ORDER BY id_jenjang LIMIT 0,1")->row();
			$kode_aktivasi  = $settings->kode_aktivasi;
			$nama_sekolah   = $settings->logo_besar;
			$link_sinkron   = $settings->link_sinkron;
			$kota           = $settings->kota;
			$pimpinan       = $settings->pimpinan;
			$jenjang        = $jenjang->jenjang;

      $array = array(
        'data_absen'    => $data_absen, 
        'jenjang'       => $jenjang,
        'pimpinan'      => $pimpinan, 
        'kota'          => $kota,
        'kode_aktivasi' => $kode_aktivasi, 
        'nama_sekolah'  => $nama_sekolah
      );
			$payload  = json_encode( $array, JSON_PRETTY_PRINT );
			//echo $payload;
			//$url_post = $link_sinkron."sync/sync_rekap_absen_guru/do_sync";
			$json_cek = file_get_contents($link_sinkron."sync/cek_sync/".$kode_aktivasi);                
			$obj   = json_decode($json_cek,true);                                                                      
			//echo $obj;
			if($obj == 1){  
				$url_post = $link_sinkron."sync/sync_rekap_kehadiran_non_guru/do_sync";
				$ch = curl_init($url_post);
			  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			  curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
			  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json'
				));
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			  $result = curl_exec($ch);
			  curl_close($ch);
				$_SESSION['pesan'] 	= "Data berhasil disinkronisasi dengan web monitoring ({elapsed_time} seconds)";
				$_SESSION['tipe'] 	= "info";
				//echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cek_absen_non_guru'>";					
				echo "ok";
			}else{
				$_SESSION['pesan'] 	= "Sekolah anda belum terdaftar di database web dinas";
				$_SESSION['tipe'] 	= "danger";
				//echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cek_absen_non_guru'>";					
				echo "ok";
			}


		}

		public function sync($param1='',$param2='')
		{
			function cek_hari($tgl){
			    $hari=$day= $tgl;
			    switch($hari){
			        case"Sunday":$hari="Minggu"; break;
			        case"Monday":$hari="Senin"; break;
			        case"Tuesday":$hari="Selasa"; break;
			        case"Wednesday":$hari="Rabu"; break;
			        case"Thursday":$hari="Kamis"; break;
			        case"Friday":$hari="Jumat"; break;
			        case"Saturday":$hari="Sabtu"; break;
			    }
			    return $hari;
			}

			function hari(){
			    $hari=$day= gmdate("l", time()+60*60*7);
			    switch($hari){
			        case"Sunday":$hari="Minggu"; break;
			        case"Monday":$hari="Senin"; break;
			        case"Tuesday":$hari="Selasa"; break;
			        case"Wednesday":$hari="Rabu"; break;
			        case"Thursday":$hari="Kamis"; break;
			        case"Friday":$hari="Jumat"; break;
			        case"Saturday":$hari="Sabtu"; break;
			    }
			    return $hari;
			}

			function hari_apa($s) {
			    if ($s == "0 Minggu") {
			        return "Sunday";
			    }
			    else if ($s == "1 Senin") {
			        return "Monday";
			    }
			    else if ($s == "2 Selasa") {
			        return "Tuesday";
			    }
			    else if ($s == "3 Rabu") {
			        return "Wednesday";
			    }
			    else if ($s == "4 Kamis") {
			        return "Thursday";
			    }
			    else if ($s == "5 Jumat") {
			        return "Friday";
			    }
			    else if ($s == "6 Sabtu") {
			        return "Saturday";
			    }
			}

			function datediff($tgl1, $tgl2){
					$tgl1 = strtotime($tgl1);
					$tgl2 = strtotime($tgl2);
					$diff_secs = abs($tgl1 - $tgl2);
					$base_year = min(date("Y", $tgl1), date("Y", $tgl2));
					$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
					return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
			}

			if ($param1=='filter') {
				$tgl = $this->input->get('tgl');
				$tgl_akhir = $this->input->get('tgl2');
				$jenis = $this->input->get('jenis');
				$id_guru = $this->input->get('guru');
				$id_jenjang = $this->input->get('jenjang');
				$id_ta = $this->input->get('tahunajaran');
				if($jenis == "Semua"){
						$sql = "SELECT * FROM tabel_kelas INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas) INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru) INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) ";
						if ($id_guru != '') $sql .= " where tabel_jadwal.id_guru = '$id_guru'";
						$sql .= " ORDER BY hari,jam_awal ASC";
						$t = $this->db->query($sql);

						$begin = new DateTime($tgl);
						$end = new DateTime($tgl_akhir);
						$end->modify('+1 day');

						$interval = DateInterval::createFromDateString('1 day');
						$period = new DatePeriod($begin, $interval, $end);

						foreach ($period as $dt) {
								$tgltgl = $dt->format("d-m-Y");
								$harihari = $dt->format("l");

								foreach($t->result() as $row) {
										$jam_absen  = "$row->jam_awal - $row->jam_akhir";
										$hari_ = hari_apa($row->hari);
										if ($harihari != $hari_) continue;
										$cek = $this->db->query("SELECT * FROM tabel_absen_mapel WHERE tgl = '$tgltgl' AND id_jadwal = '$row->id_jadwal' AND valid = 'valid'");

										if($cek->num_rows() > 0){
												$isi = $cek->row();
												$awal     = date("H:i", strtotime($row->jam_awal));
												$deadline = date("H:i", strtotime($isi->jam));
												$a        = datediff($awal,$deadline);
												$dif      = $a['minutes_total'];

												$keterangan = $isi->keterangan;

												$re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
												$telat = $re->telat;
												$val = "present $isi->jam";
												if ($keterangan == "") {
														if ($row->status == "pulang") $ket = "Absen Sesuai";
														else {
																if ($dif > $telat) {
																		$dif = $dif - $telat;
																		$ket = "terlambat $dif menit";
																}
																else $ket = "Absen Sesuai";
														}
												}
												else {
														$ket = $keterangan;
												}
										}else{
												$val = "nihil";
												$dif = "";
												$ket = "";
										}
										$data_absen[] = array(
											'id' => $row->id_guru,
											'nama' => $row->nama,
											'mengajar' => $row->jenjang." ".$row->kelas,
											'mapel' => $row->mapel,
											'jam_absen' => $jam_absen,
											'val' => $val,
											'ket' => $ket,
											'tgl' => $tgltgl
										);

						}


				}
					}elseif($jenis=="Terlambat"){
					$begin = new DateTime($tgl);
					$end = new DateTime($tgl_akhir);
					$end->modify('+1 day');

					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($begin, $interval, $end);

					foreach ($period as $dt) {
							$tgltgl = $dt->format("d-m-Y");
							$t = $this->db->query("SELECT DISTINCT(tabel_absen_mapel.id_jadwal) FROM tabel_jadwal INNER JOIN tabel_absen_mapel ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal
											WHERE SUBSTR(tabel_jadwal.hari,3) = '$hari' AND tabel_absen_mapel.tgl = '$tgltgl'
											ORDER BY hari,jam_awal ASC");
							foreach($t->result() as $row) {


							$se = $this->db->query("SELECT * FROM tabel_jadwal INNER JOIN tabel_absen_mapel ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal
											WHERE tabel_jadwal.id_jadwal = '$row->id_jadwal'")->row();

							$jam_absen  = "$se->jam_awal - $se->jam_akhir";

							$awal     = date("H:i", strtotime($se->jam_awal));
							$deadline = date("H:i", strtotime($se->jam));
							$a        = datediff($awal,$deadline);
							$dif      = $a['minutes_total'];

							$re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
							$telat = $re->telat;
							if($telat < $dif && $se->keterangan != "offline"){
							//if($se->num_rows() == 0){

									$t = $this->db->query("SELECT * FROM tabel_jadwal
									INNER JOIN tabel_kelas ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas
									INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
									INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
									INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
									WHERE tabel_jadwal.id_jadwal = '$row->id_jadwal'")->row();

									$val = "present $se->jam";
									$ket = "terlambat $dif menit";

									if($t != NULL){
										$data_absen[] = array(
											'id' => $row->id_guru,
											'nama' => $row->nama,
											'mengajar' => $row->jenjang." ".$row->kelas,
											'mapel' => $row->mapel,
											'jam_absen' => $jam_absen,
											'val' => $val,
											'ket' => $ket,
											'tgl' => $tgltgl
										);
									}
							}
							}
					}
					}else{
					$begin = new DateTime($tgl);
					$end = new DateTime($tgl_akhir);
					$end->modify('+1 day');

					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($begin, $interval, $end);

					foreach ($period as $dt) {
							$tgltgl = $dt->format("d-m-Y");
							$t = $this->db->query("SELECT * FROM tabel_kelas
											INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)
											INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
											INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
											INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
											WHERE SUBSTR(tabel_jadwal.hari,3) = '$hari' ORDER BY hari,jam_awal ASC");
							foreach($t->result() as $row) {
							$jam_absen  = "$row->jam_awal - $row->jam_akhir";
							$cek = $this->db->query("SELECT * FROM tabel_absen_mapel WHERE tgl BETWEEN '$tgl2' AND '$tgl_akhir' AND id_jadwal = '$row->id_jadwal' AND valid = 'valid'");

							if($cek->num_rows() == 0){
									$val = "nihil";
									$dif = "";
									$ket = "";
									$data_absen[] = array(
										'id' => $row->id_guru,
										'nama' => $row->nama,
										'mengajar' => $row->jenjang." ".$row->kelas,
										'mapel' => $row->mapel,
										'jam_absen' => $jam_absen,
										'val' => $val,
										'ket' => $ket,
										'tgl' => $tgltgl
									);
									}
							}
					}
					}
			}else {
				$tgl1  = gmdate("d-m-Y", time()+60*60*7);
				$hari  = hari();
				$tgl2 = gmdate("d-m-Y", time()+60*60*7);
				$t = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas) INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru) INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) ORDER BY hari,jam_awal ASC");
				foreach($t->result() as $row) {
						$jam_absen  = $row->jam_awal." - ".$row->jam_akhir;
						$cek = $this->db->query("SELECT * FROM tabel_absen_mapel WHERE tgl = '$tgl2' AND id_jadwal = '$row->id_jadwal' AND valid = 'valid'");
						if($cek->num_rows() > 0){
								$isi = $cek->row();
								$awal     = date("H:i", strtotime($row->jam_awal));
								$deadline = date("H:i", strtotime($isi->jam));
								$a        = datediff($awal,$deadline);
								$dif      = $a['minutes_total'];
								$keterangan = $isi->keterangan;
								$re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
								$telat = $re->telat;
								$val = "present $isi->jam";
								if ($keterangan == "") {
										if ($row->status == "pulang") $ket = "Absen Sesuai";
										else {
												if ($dif > $telat) {
														$dif = $dif - $telat;
														$ket = "terlambat $dif menit";
												}
												else $ket = "Absen Sesuai";
										}
								}
								else {
										$ket = $keterangan;
								}
						}else{
								$val = "nihil";
								$dif = "";
								$ket = "";
						}
								$data_absen[] = array(
									'id' => $row->id_guru,
									'nama' => $row->nama,
									'mengajar' => $row->jenjang." ".$row->kelas,
									'mapel' => $row->mapel,
									'jam_absen' => $jam_absen,
									'val' => $val,
									'ket' => $ket,
									'tgl' => $tgl1
								);
				}
			}
			ini_set('max_execution_time', 300);
			$settings = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
			$kode_aktivasi = $settings->kode_aktivasi;
			$nama_sekolah = $settings->logo_besar;
			$link_sinkron = $settings->link_sinkron;

			$payload  = json_encode($array = array('data_absen' => $data_absen, 'kode_aktivasi' => $kode_aktivasi, 'nama_sekolah'=> $nama_sekolah),JSON_PRETTY_PRINT);
			//echo $payload;
			//$url_post = $link_sinkron."sync/sync_rekap_absen_guru/do_sync";
			$url_post = $link_sinkron."sync/sync_rekap_absen_guru/do_sync";
			$ch = curl_init($url_post);
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		  curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
		  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json'
			));
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		  $result = curl_exec($ch);
		  curl_close($ch);
			 //return $result;
			$_SESSION['pesan'] 	= "Data berhasil disinkronisasi dengan web monitoring";
			$_SESSION['tipe'] 	= "info";
			// echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/sinkron_absen_guru_rekap'>";
			echo "ok";
		}

	}
