<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_kategori');
		$this->load->model('m_pesan');		
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "kategori";		
		$data['title']	= "SIAKAD | Kategori";			
		$data['isi']    = "kategori";
		$data['judul1']	= "Kategori";			
		$data['judul2']	= "";			
		$data['judul1']	= "Kategori";
		$data['set']	= "view";
		$data['dt_ta']	= $this->m_kategori->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "kategori";		
		$data['title']	= "SIAKAD | Kategori";			
		$data['judul1']	= "Kategori";		
		$data['isi']    = "kategori";	
		$data['judul2']	= "";			
		$data['judul1']	= "Kategori";
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['kategori']		= $this->input->post('kategori');						
			$data['link'] 		= url_title($data['kategori']);			
			$this->m_kategori->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kategori'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "kategori";		
			$data['one_post']= $this->m_kategori->get_one($id);
			$data['title']	= "SIAKAD | Kategori";			
			$data['judul1']	= "Kategori";			
			$data['isi']    = "kategori";
			$data['judul2']	= "";			
			$data['judul1']	= "Kategori";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$data['kategori']	= $this->input->post('kategori');						
			$data['link'] 		= url_title($data['kategori']);			
			$this->m_kategori->edit($id, $data);
			?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");			
					</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kategori'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_kategori->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kategori'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/kategori'>";
		}
	}

}
