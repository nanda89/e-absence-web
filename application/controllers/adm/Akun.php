<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_semua');		
		$this->load->model('m_pesan');		
		//===== Load Library =====
		$this->load->library('upload');

	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "akun";		
		$data['title']	= "Manajemen Akun";			
		$data['isi']    = "akun";
		$data['judul1']	= "Manajemen Akun";			
		$data['judul2']	= "";			
		$data['set']	= "view";
		$data['dt_admin']	= $this->m_admin->get_all();											
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "akun";		
		$data['title']	= "Manajemen Akun";			
		$data['judul1']	= "Manajemen Akun";		
		$data['isi']    = "akun";	
		$data['judul2']	= "";			
		$data['dt_cabang']	= $this->m_semua->getAll("tabel_cabang");					
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
	public function ajax_bulk_delete()
	{
		$tabel			= "tabel_user";
		$pk 				= "id_user";
		$list_id 		= $this->input->post('id');
		foreach ($list_id as $id) {
			$this->m_admin->delete($tabel,$pk,$id);
		}
		echo json_encode(array("status" => TRUE));
	}
	public function save()
	{
		$username = $this->input->post('username');
		$c = $this->m_admin->cek_user($username);
        if($c->num_rows()>0)
        {
			$_SESSION['pesan'] 	= "Username '$username' sudah digunakan, gunakan yang lain.";
			$_SESSION['tipe'] 	= "warning";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/akun/add'>";
			return;
        }
		$config['upload_path'] 		= './assets/panel/icon/';
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
		$config['max_size']			= '2000';
		$config['max_width']  		= '2000';
		$config['max_height']  		= '1024';
				
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('avatar')){
			$gambar="";
		}else{
			$gambar=$this->upload->file_name;
		} 


		$data['nama']			= $this->input->post('nama');			
		$data['username']		= $this->input->post('username');			
		$data['password']		= md5($this->input->post('password'));						
		$data['avatar'] 		= $gambar;			
		$data['tgl_daftar']		= date('Y-M-d');		
		$data['level']			= $this->input->post('level');					
		$this->m_admin->tambah($data);
		
		$_SESSION['pesan'] 	= "Berhasil tersimpan!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/akun'>";
		
	}
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "akun";		
			$data['one_post']= $this->m_semua->getByID("tabel_user","id_user",$id);
			$data['title']	= "Manajemen Akun";			
			$data['judul1']	= "Manajemen Akun";			
			$data['isi']    = "akun";
			$data['judul2']	= "";				
			$data['dt_cabang']	= $this->m_semua->getAll("tabel_cabang");							
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$config['upload_path'] 		= './assets/panel/icon/';
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
		$config['max_size']			= '2000';
		$config['max_width']  		= '2000';
		$config['max_height']  		= '1024';
				
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('avatar')){
			$gambar="";
		}else{
			$gambar=$this->upload->file_name;
		} 


		$data['nama']			= $this->input->post('nama');			
		$data['username']		= $this->input->post('username');
		$data['imei_1']			= $this->input->post('imei1');
		$data['imei_2']			= $this->input->post('imei2');			
		$password				= $this->input->post('password');
		if($password<>''){
			$data['password']		= md5($this->input->post('password'));						
		}								
		$data['avatar'] 		= $gambar;					
		$data['level']			= $this->input->post('level');			
			$this->m_admin->edit($id, $data);
			
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/akun'>";
		}
		elseif ($set == 'hapus' )
		{
			$this->m_admin->hapus($id);			
			$_SESSION['pesan'] 	= "Berhasil terhapus!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/akun'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/akun'>";
		}
	}

}
