<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Cek_absen_non_guru extends CI_Controller 
	{
		public function __construct()
		{		
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			# CORE
			$name = $this->session->userdata('nama');
			if ($name == "") echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";

			
			$this->load->database();
			$this->load->helper('url');
			
			$this->load->model('m_admin');
			$this->load->model('m_tahunajaran');
			$this->load->model('m_pesan');		
			$this->load->model('m_jenjang');
			$this->load->model('m_semua');
		}

		protected function template($page, $data)
		{
			$this->load->view('t_panel/header',$data);
			$this->load->view("t_panel/aside");
			$this->load->view("panel/$page");		
			$this->load->view('t_panel/footer');
		}

		public function index()
		{		
			$page			= "cek_absen_non_guru";		
			$data['title']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['isi']    = "cek_absen_non_guru";
			$data['judul1']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['judul2']	= "";					
			$data['set']	= "view";		
			$this->template($page, $data);	
		}

		public function filter()
		{		
			$page			= "cek_absen_non_guru";		
			$data['title']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['isi']    = "cek_absen_non_guru";
			$data['judul1']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		
			$data['tgl']	= $this->input->post('tgl');
			$data['tgl_akhir']	= $this->input->post('tgl2');
			$data['jenis']	= $this->input->post('jenis');
			$this->template($page, $data);
		}

		public function export_filter()
		{		
			$page			= "cek_absen_non_guru";		
			$data['title']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['isi']    = "cek_absen_non_guru";
			$data['judul1']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		
			$data['tgl']	= $this->input->get('tgl');
			$data['tgl_akhir']	= $this->input->get('tgl_akhir');
			$data['jenis']	= $this->input->get('jenis');
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_non_guru',$data);
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_non_guru',$data);				
			}		
		}

		public function export_now()
		{		
			$page			= "cek_absen_non_guru";		
			$data['title']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['isi']    = "cek_absen_non_guru";
			$data['judul1']	= "Lap.Validasi Kehadiran Non Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		        
			$data['tgl']	= gmdate("d-m-Y", time()+60*60*7);
			$data['tgl_akhir']	= gmdate("d-m-Y", time()+60*60*7);
			$data['jenis']	= "Semua";
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_non_guru',$data);				
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_non_guru',$data);				
			}		
		}	
	}