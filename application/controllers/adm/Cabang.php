<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_cabang');
		$this->load->model('m_pesan');		
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "cabang";		
		$data['title']	= "SIAKAD | Data Cabang";			
		$data['isi']    = "cabang";
		$data['judul1']	= "Data Cabang";			
		$data['judul2']	= "";			
		$data['judul1']	= "Data Cabang";
		$data['set']	= "view";
		$data['dt_ta']	= $this->m_cabang->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "cabang";		
		$data['title']	= "SIAKAD | Data Cabang";			
		$data['judul1']	= "Data Cabang";		
		$data['isi']    = "cabang";	
		$data['judul2']	= "";			
		$data['judul1']	= "Data Cabang";
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['nama_cabang']	= $this->input->post('nama_cabang');			
			$data['alamat']			= $this->input->post('alamat');			
			$data['pimpinan']		= $this->input->post('pimpinan');						
			$this->m_cabang->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cabang'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->post('id');
		$set	= $this->input->post('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'ubah')
		{
			$page			= "cabang";		
			$data['one_post']= $this->m_cabang->get_one($id);
			$data['title']	= "SIAKAD | Data Cabang";			
			$data['judul1']	= "Data Cabang";			
			$data['isi']    = "cabang";
			$data['judul2']	= "";			
			$data['judul1']	= "Data Cabang";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$data['nama_cabang']	= $this->input->post('nama_cabang');			
			$data['alamat']			= $this->input->post('alamat');			
			$data['pimpinan']		= $this->input->post('pimpinan');						
			$this->m_cabang->edit($id, $data);
			?>
					<script type="text/javascript">
						alert("Berhasil Tersimpan");			
					</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cabang'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_cabang->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cabang'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/cabang'>";
		}
	}

}
