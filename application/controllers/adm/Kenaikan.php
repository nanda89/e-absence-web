<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kenaikan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        //---- cek session -------//
        $name = $this->session->userdata('nama');
        if ($name == "") {
            echo "<meta http-equiv='refresh' content='0; url=" . base_url() . "panel'>";
        }

        //===== Load Database =====
        $this->load->database();
        $this->load->helper('url');
        //===== Load Model =====
        $this->load->model('m_admin');
        $this->load->model('m_siswa');
        $this->load->model('m_jenjang');
        $this->load->model('m_pesan');
        $this->load->model('m_kenaikan');
        $this->load->model('m_tahunajaran');
        $this->load->model('m_penempatan');
        $this->load->model('m_setting');
        $this->load->model('m_siswakelas');

        //===== Load Library =====
        $this->load->library('upload');

    }
    protected function template($page, $data)
    {
        $this->load->view('t_panel/header', $data);
        $this->load->view("t_panel/aside");
        $this->load->view("panel/$page");
        $this->load->view('t_panel/footer');
    }

    public function index()
    {
        $page = "pilih_kelas";
        $data['isi'] = "kenaikan";
        $data['title'] = "SIAKAD | Kenaikan Kelas";
        $data['judul1'] = "Kenaikan Kelas";
        $data['judul2'] = "";
        $data['set'] = "view";
        $data['dt_jenjang'] = $this->m_jenjang->get_all();
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $this->template($page, $data);
    }
    public function set_kelas()
    {
        $page = "kenaikan";
        $data['isi'] = "kenaikan";
        $data['title'] = "SIAKAD | Kenaikan Kelas";
        $data['judul1'] = "Kenaikan Kelas";
        $data['judul2'] = "";
        $data['set'] = "view";
        $id = $this->input->get('id_jenjang');
        $id_ta = $this->input->get('id_ta');
        $id_kelas = $this->input->get('id_kelas');
        $data['dt_j'] = $id;
        $data['dt_ke'] = $this->m_jenjang->get_one($id);
        $data['dt_pen'] = $this->m_kenaikan->get_all($id);
        $data['dt_ta'] = $this->m_tahunajaran->get_all();
        $level = $this->session->userdata('level');
        $id_cabang = $this->session->userdata('id_cabang');
        if ($level == 'operator cabang') {
            $data['dt_kelas'] = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id'
				AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
        } else {
            $data['dt_kelas'] = $this->m_penempatan->get_one_kelas($id);
        }
        $data['dt_siswa'] = $this->m_penempatan->get_siswa($id_ta, $id_kelas);
        $this->template($page, $data);
    }
    public function get_kelas()
    {
        $id_jenjang = $this->input->post('id_jenjang');
        $tingkat = $this->input->post('tingkat');
        $level = $this->session->userdata('level');
        $id_cabang = $this->session->userdata('id_cabang');
        if ($level == 'operator cabang') {
            $dt_kelas = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jenjang
				ON tabel_jenjang.id_jenjang = tabel_kelas.id_jenjang
				WHERE tabel_kelas.id_jenjang = '$id_jenjang'
				AND tabel_kelas.id_cabang = '$id_cabang'
				ORDER BY tabel_kelas.kelas ASC");
        } else {
            $dt_kelas = $this->m_penempatan->get_one_kelas($id_jenjang);
        }
        $data = "<option value=''>Pilih Kelas</option>";
        foreach ($dt_kelas->result() as $row) {
            $data .= "<option value='$row->id_kelas'>$row->kelas</option>\n";
        }
        echo $data;
    }
    public function t_kenaikan()
    {
        $id_tahun = $this->input->post('id_tahun');
        $id_kelas = $this->input->post('id_kelas');
        $data['dt_kenaikan'] = $this->m_kenaikan->get_siswa($id_tahun, $id_kelas);
        $this->load->view('panel/t_kenaikan', $data);
    }
    public function t_siswakelas()
    {
        $id_tahun = $this->input->post('id_tahun');
        $id_kelas = $this->input->post('id_kelas');
        $data['dt_kenaikan'] = $this->m_kenaikan->get_siswa($id_tahun, $id_kelas);
        $this->load->view('panel/t_siswakelas', $data);
    }
    public function save()
    {
        $data['id_ta'] = $this->input->post('id_tahun');
        $data['angkatan'] = $this->input->post('angkatan');
        $data['id_kelas'] = $this->input->post('id_kelas');
        $data['id_siswa'] = $this->input->post('id_siswa');
        $detail['status'] = 'aktif';
        $id_siswa = $this->input->post('id_siswa');
        $cek = $this->m_kenaikan->cek_siswa($id_siswa);
        if ($cek->num_rows() > 0) {
            echo "nothing";
        } else {
            $this->m_kenaikan->tambah($data);
            $this->m_siswa->edit($id_siswa, $detail);
            echo "nihil";
        }
    }
    public function naikkan()
    {
        $id_penempatan = $this->input->post('id_penempatan');
        $data['id_ta'] = $this->input->post('id_tahun');
        $data['id_kelas'] = $this->input->post('id_kelas');
        $data['id_siswa'] = $this->input->post('id_siswa');

        $id_siswa = $this->input->post('id_siswa');

        $setting = $this->m_setting->get_one();
        $setting = $setting->row();
        $kapasitas = $setting->kapasitas_kelas;
        $totalSiswa = $this->m_siswakelas->countKelas($data['id_kelas']);

        if (intval($totalSiswa) > intval($kapasitas)) {
            echo "Error : kapasitas kelas tujuan tidak mencukupi | nihil";

        } else {
            $this->m_kenaikan->edit($id_penempatan, $data);
            echo "nihil";

        }

    }
    public function turunkan()
    {
        $id_penempatan = $this->input->post('id_penempatan');
        $data['id_ta'] = $this->input->post('id_tahun');
        $data['id_kelas'] = $this->input->post('id_kelas');
        $data['id_siswa'] = $this->input->post('id_siswa');
        $id_siswa = $this->input->post('id_siswa');

        $this->m_kenaikan->edit($id_penempatan, $data);
        echo "nihil";
    }
    public function delete_kenaikan()
    {
        $id = $this->input->post('id_kenaikan');
        $this->m_kenaikan->hapus($id);
        echo "nihil";
    }

}
