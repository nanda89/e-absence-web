<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_tahunajaran');
		$this->load->model('m_pesan');		
		$this->load->model('m_semua');		
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function inbox()
	{		
		$page			= "pesan_masuk";		
		$data['title']	= "Pesan Masuk";			
		$data['isi']    = "pesan_masuk";
		$data['judul1']	= "Pesan Masuk";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_sms_masuk']	= $this->db->query("SELECT * FROM inbox ORDER BY ID desc");			
		$this->template($page, $data);	
	}

	public function delete_inbox()
	{		
		$id = $this->input->post('id');	
		$this->db->query("DELETE FROM inbox WHERE ID = '$id'");			
		$_SESSION['pesan'] 	= "Berhasil dihapus!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/inbox'>";
	}

	public function sent()
	{		
		$page			= "pesan_terkirim";		
		$data['title']	= "Pesan Terkirim";			
		$data['isi']    = "pesan_terkirim";
		$data['judul1']	= "Pesan Terkirim";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_sms_terkirim'] = $this->db->query("SELECT * FROM sentitems ORDER BY ID desc");			
		$this->template($page, $data);	
	}

	public function delete_sent()
	{		
		$id = $this->input->post('id');	
		$this->db->query("DELETE FROM sentitems WHERE ID = '$id'");			
		$_SESSION['pesan'] 	= "Berhasil dihapus!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/sent'>";
	}

	public function draft()
	{		
		$page			= "draft";		
		$data['title']	= "Draft";			
		$data['isi']    = "draft";
		$data['judul1']	= "Draft";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_draft'] = $this->db->query("SELECT * FROM outbox ORDER BY ID desc");			
		$this->template($page, $data);	
	}

	public function delete_draft()
	{		
		$id = $this->input->post('id');	
		$this->db->query("DELETE FROM outbox WHERE ID = '$id'");			
		$_SESSION['pesan'] 	= "Berhasil dihapus!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/draft'>";
	}

	public function auto()
	{		
		$page			= "sms_auto";		
		$data['title']	= "Isi Pesan Auto";			
		$data['isi']    = "sms_auto";
		$data['judul1']	= "Isi Pesan Auto";			
		$data['judul2']	= "";					
		$data['set']	= "view";
		$data['dt_auto'] = $this->db->query("SELECT * FROM sms_auto ORDER BY id_pesan asc");			
		$this->template($page, $data);	
	}
	public function autoadd()
	{		
		$page			= "sms_auto";		
		$data['title']	= "Isi Pesan Auto";			
		$data['isi']    = "sms_auto";
		$data['judul1']	= "Isi Pesan Auto";			
		$data['judul2']	= "";					
		$data['set']	= "insert";		
		$this->template($page, $data);	
	}
	public function autosave()
	{
		if($this->input->post('save') == 'save')
		{
			$data['jenis']	= $this->input->post('jenis');			
			$data['isi']	= $this->input->post('isi');						
			$this->m_admin->simpan('sms_auto',$data);
			
			$_SESSION['pesan'] 	= "Berhasil tersimpan!";
			$_SESSION['tipe'] 	= "info";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/auto'>";
		}			
	}
	public function delete_auto()
	{		
		$id = $this->input->get('id');	
		$this->db->query("DELETE FROM sms_auto WHERE id_pesan = '$id'");			
		$_SESSION['pesan'] 	= "Berhasil dihapus!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/auto'>";
	}
	public function autoedit()
	{		
		$page			= "sms_auto";		
		$id 			= $this->input->get('id');	
		$data['title']	= "Isi Pesan Auto";			
		$data['isi']    = "sms_auto";
		$data['judul1']	= "Isi Pesan Auto";			
		$data['judul2']	= "";					
		$data['set']	= "edit";
		$data['one'] 	= $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'");			
		$this->template($page, $data);	
	}
	public function autoupdate()
	{
		$id 			= $this->input->post('id');	
		$data['jenis']	= $this->input->post('jenis');			
		$data['isi']	= $this->input->post('isi');						
		$this->m_admin->ubah('sms_auto','id_pesan',$id,$data);
		
		$_SESSION['pesan'] 	= "Berhasil tersimpan!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/auto'>";			
	}
	public function schedule()
	{		
		$page			= "schedule";		
		$data['title']	= "Pengaturan Jadwal SMS";			
		$data['judul1']	= "Pengaturan Jadwal SMS";			
		$data['isi']	= "schedule";				
		$data['judul2']	= "";	
		$data['setting'] = $this->db->query("SELECT * FROM tabel_schedule");		
		$this->template($page, $data);	
	}
	public function update()
	{
		$id 	= $this->input->post('id_schedule');			
		
		$d['status']	= $this->input->post('status');
		$d['aturan']	= $this->input->post('aturan');
		$this->m_admin->ubah('tabel_schedule','id_schedule',$id,$d);
		
		$_SESSION['pesan'] 	= "Berhasil tersimpan!";
		$_SESSION['tipe'] 	= "info";
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/gateway/schedule'>";			
	}

}
