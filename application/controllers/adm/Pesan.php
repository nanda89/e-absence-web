<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {
	public function __construct()
	{		
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//---- cek session -------//		
		$name = $this->session->userdata('nama');
		if ($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}

		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
		$this->load->model('m_admin');
		$this->load->model('m_pesan');
		//===== Load Library =====


	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);
		$this->load->view("t_panel/aside");
		$this->load->view("panel/$page");		
		$this->load->view('t_panel/footer');
	}

	public function index()
	{		
		$page			= "pesan";		
		$data['title']	= "SIAKAD | Pesan";			
		$data['isi']    = "pesan";
		$data['judul1']	= "pesan";			
		$data['judul2']	= "";			
		$data['judul1']	= "Pesan";
		$data['set']	= "view";
		$data['dt_ta']	= $this->m_pesan->get_all();			
		$this->template($page, $data);	
	}

	public function add()
	{		
		$page			= "pesan";		
		$data['title']	= "SIAKAD | pesan";			
		$data['judul1']	= "pesan";		
		$data['isi']    = "pesan";	
		$data['judul2']	= "";			
		$data['judul1']	= "pesan";
		$data['set']	= "insert";			
		$this->template($page, $data);	
	}
		
	
	public function save()
	{
		if($this->input->post('save') == 'save')
		{
			$data['pesan']		= $this->input->post('pesan');						
			$data['link'] 		= url_title($data['pesan']);			
			$this->m_pesan->tambah($data);
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pesan'>";
		}			
	}
	
	public function process()
	{
		$id		= $this->input->get('id');
		$set	= $this->input->get('s_process');		
		//FORM EDIT KEGIATAN
		if ($set == 'lihat')
		{
			$page			= "pesan";		
			$data['one_post']= $this->m_pesan->get_one($id);
			$data['title']	= "SIAKAD | Pesan";			
			$data['judul1']	= "Pesan";			
			$data['isi']    = "pesan";
			$data['judul2']	= "";			
			$data['judul1']	= "Pesan";
			$data['set']	= "edit";	
			$this->template($page, $data);	
		}
		//EDIT DATA KEGIATAN
		elseif ($set == 'edit' )
		{
			$data['status']	= "read";			
			$this->m_pesan->edit($id, $data);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pesan'>";
		}
		//HAPUS DATA KEGIATAN
		elseif ($set == 'hapus' )
		{
			$this->m_pesan->hapus($id);			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pesan'>";
		}
		else
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/pesan'>";
		}
	}

}
