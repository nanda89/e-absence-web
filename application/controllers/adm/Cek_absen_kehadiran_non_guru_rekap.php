<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Cek_absen_kehadiran_non_guru_rekap extends CI_Controller 
	{
		public function __construct()
		{		
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			# CORE
			$name = $this->session->userdata('nama');
			if ($name == "") echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";

			
			$this->load->database();
			$this->load->helper('url');
			
			$this->load->model('m_admin');
			$this->load->model('m_tahunajaran');
			$this->load->model('m_pesan');		
			$this->load->model('m_jenjang');
			$this->load->model('m_semua');
			$this->load->model('m_guru');
		}

		protected function template($page, $data)
		{
			$this->load->view('t_panel/header',$data);
			$this->load->view("t_panel/aside");
			$this->load->view("panel/$page");		
			$this->load->view('t_panel/footer',$data);
		}

		public function index()
		{		
			$page			      = "cek_absen_kehadiran_non_guru_rekap";		
			$data['title']	= "Rekap Kehadiran Absen Non Guru";			
			$data['isi']    = "cek_absen_kehadiran_non_guru_rekap";
			$data['judul1']	= "Rekap Kehadiran Absen Non Guru";			
			$data['judul2']	= "";					
			$data['set']	  = "view";
			$data['guru']     = $this->m_guru->get_all();
			$data['jenjang']  = $this->m_jenjang->get_all();
			$data['tahunajaran'] = $this->m_tahunajaran->get_all();
			$this->template($page, $data);	
		}

		public function filter()
		{		
			$page			      = "cek_absen_kehadiran_non_guru_rekap";		
			$data['title']	= "Rekap Kehadiran Absen Non Guru";			
			$data['isi']    = "cek_absen_kehadiran_non_guru_rekap";
			$data['judul1']	= "Rekap Kehadiran Absen Non Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		
			$data['tgl']	= $this->input->post('tgl');
			$data['tgl_akhir']	= $this->input->post('tgl2');
			$data['jenis']	= $this->input->post('jenis');
			$data['id_guru'] = $this->input->post('guru');
			if ($data['id_guru'] != '') $data['nama_guru'] = $this->m_guru->get_one($data['id_guru'])->row()->nama;
			else $data['nama_guru'] = '';
			$data['id_jenjang'] = $this->input->post('jenjang');
			if ($data['id_jenjang'] != '') $data['jenjang'] = $this->m_jenjang->get_one($data['id_jenjang'])->row()->jenjang;
			else $data['jenjang'] = '';
			$data['id_ta'] = $this->input->post('tahunajaran');
			if ($data['id_ta'] != '') $data['tahunajaran'] = $this->m_tahunajaran->get_one($data['id_ta'])->row()->tahun_ajaran;
			else $data['tahunajaran'] = '';
			$this->template($page, $data);
		}

		public function export_filter()
		{		
			$page			      = "cek_absen_kehadiran_non_guru_rekap";		
			$data['title']	= "Rekap Validasi Absen Guru";			
			$data['isi']    = "cek_absen_kehadiran_non_guru_rekap";
			$data['judul1']	= "Rekap Validasi Absen Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		

			$data['tgl']	= $this->input->get('tgl');
			$data['tgl_akhir']	= $this->input->get('tgl_akhir');
			$data['jenis']	= $this->input->get('jenis');
			$data['id_guru'] = $this->input->get('id_guru');
			if ($data['id_guru'] != '') $data['nama_guru'] = $this->m_guru->get_one($data['id_guru'])->row()->nama;
			else $data['nama_guru'] = '';
			$data['id_jenjang'] = $this->input->get('id_jenjang');
			if ($data['id_jenjang'] != '') $data['jenjang'] = $this->m_jenjang->get_one($data['id_jenjang'])->row()->jenjang;
			else $data['jenjang'] = '';
			$data['id_ta'] = $this->input->get('id_ta');
			if ($data['id_ta'] != '') $data['tahunajaran'] = $this->m_tahunajaran->get_one($data['id_ta'])->row()->tahun_ajaran;
			else $data['tahunajaran'] = '';
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_kehadiran_non_guru_rekap',$data);
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_kehadiran_non_guru_rekap',$data);				
			}		
		}

		public function export_now()
		{		
			$page			= "cek_absen_guru_rekap";		
			$data['title']	= "Rekap Validasi Absen Guru";			
			$data['isi']    = "cek_absen_guru_rekap";
			$data['judul1']	= "Rekap Validasi Absen Guru";			
			$data['judul2']	= "";					
			$data['set']	= "filter";		        
			$data['tgl']	= gmdate("d-m-Y", time()+60*60*7);
			$data['tgl_akhir']	= gmdate("d-m-Y", time()+60*60*7);
			$data['jenis']	= "Semua";
			$data['id_guru'] = '';
			$data['nama_guru'] = '';
			$a	= $this->input->get('a');
			if($a == "1"){
				$this->load->view('panel/excel_absen_guru_rekap',$data);				
			}elseif($a == "2"){
				$this->load->view('panel/pdf_absen_guru_rekap',$data);				
			}		
		}	
	}