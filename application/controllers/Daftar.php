<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====	
		$this->load->model('m_artikel');
		$this->load->model('m_admin');
		$this->load->model('m_kategori');
		$this->load->model('m_siswa');
		$this->load->model('m_semua');
		//===== Load Library =====
		$this->load->library('upload');
	}
	protected function template($page, $data)
	{
		$this->load->view('t_web/header',$data);		
		$this->load->view("web/$page");		
		//$this->load->view("t_web/aside");
		$this->load->view('t_web/footer');
	}
	public function index()
	{		
		$id						= $this->input->post('id_jenjang');						
		$kode_awal				= $this->input->post('kode_awal');						
		$data['id_daf_jadwal']	= $this->input->post('id_daf_jadwal');
		$data['id_jenjang']		= $this->input->post('id_jenjang');
		$th 		= date("y");
		$id_c_siswa 	= $this->m_siswa->get_end_calon($id);						
		if($id_c_siswa->num_rows()>0){
			$row 	= $id_c_siswa->row();				
			$id2 	= substr($row->id_daftar,6,4)+1;	
			if($id2<10){
	            $kode = $kode_awal.$th.'000'.$id2;
	        }elseif($id2>9 && $id2<99){
	            $kode = $kode_awal.$th.'00'.$id2;
	        }elseif($id2>98 && $id2<999){
	            $kode = $kode_awal.$th.'0'.$id2;
	        }elseif($id2>998){
	            $kode =$kode_awal.$th.$id2;			
	        }
		}else{
			$kode = $kode_awal.$th.'0001';
		} 
		$data['kode']	= $kode;					
		$data['judul']	= "WEB SIAKAD | Pendaftaran";				
		$data['isi']	= "info_daftar";
		$data['dt_cabang']= $this->m_semua->getAll("tabel_cabang");						
		$page			= "daftar";				
		$this->template($page,$data);
	}	
	public function selesai($id_daftar)
	{				
		$data['id_daftar']		= $id_daftar;
		$data['judul']			= "WEB SIAKAD | Selesai";				
		$data['isi']			= "info_daftar";					
		$page					= "selesai";				
		$this->template($page,$data);
	}		
	public function save()
	{		
		if($this->input->post('save') == 'save')
		{
			//setting konfigurasi upload gambar
			$config['upload_path'] 		= './assets/panel/images/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['max_size']			= '2000';
			$config['max_width']  		= '2000';
			$config['max_height']  		= '1024';
					
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('gambar')){
				$gambar="";
			}else{
				$gambar=$this->upload->file_name;
			} 

			$id_daftar						= $this->input->post('id_daftar');			
			$data['id_daf_jadwal']			= $this->input->post('id_daf_jadwal');			
			$data['id_daftar']				= $this->input->post('id_daftar');			
			$data['id_cabang']				= $this->input->post('id_cabang');			
			$data['nisn']					= $this->input->post('nisn');			
			$data['nama_lengkap']			= $this->input->post('nama_lengkap');			
			$data['no_seri_ijazah']			= $this->input->post('no_seri_ijazah');			
			$data['no_seri_skhun']			= $this->input->post('no_seri_skhun');			
			$data['no_ujian_nasional']		= $this->input->post('no_ujian_nasional');			
			$data['nik']					= $this->input->post('nik');			
			$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
			$data['tempat_lahir']			= $this->input->post('tempat_lahir');
			$data['tgl_lahir']				= $this->input->post('tgl_lahir');
			$data['kebutuhan_khusus']		= $this->input->post('kebutuhan_khusus');
			$data['agama']					= $this->input->post('agama');
			$data['alat_transport']			= $this->input->post('alat_transport');
			$data['wn']						= $this->input->post('wn');
			$data['jenis_tinggal']			= $this->input->post('jenis_tinggal');
			$data['no_telp']				= $this->input->post('no_telp');
			$data['no_kps']					= $this->input->post('no_kps');
			$data['pip']					= $this->input->post('pip');			
			$data['bank']					= $this->input->post('bank');			
			$data['tgl_daftar']				= $this->input->post('tgl_daftar');			
			$data['id_jenjang']				= $this->input->post('id_jenjang');			
			$data['al_jalan']				= $this->input->post('al_jalan');
			$data['al_dusun']				= $this->input->post('al_dusun');
			$data['al_kel']					= $this->input->post('al_kel');
			$data['al_kec']					= $this->input->post('al_kec');
			$data['al_kab']					= $this->input->post('al_kab');
			$data['al_prov']				= $this->input->post('al_prov');
			$data['al_rtw']					= $this->input->post('al_rtw');
			$data['al_kodepos']				= $this->input->post('al_kodepos');
			$data['no_hp']					= $this->input->post('no_hp');
			$data['email']					= $this->input->post('email');
			$data['asal_sekolah']			= $this->input->post('asal_sekolah');
			$data['ket_asal']				= $this->input->post('ket_asal');
			$data['status']					= 'calon';
			$data['gambar']					= $gambar;

			$this->m_siswa->tambah_calon($data);

			$detail['id_daftar']				= $this->input->post('id_daftar');			
			$detail['ay_nama']					= $this->input->post('ay_nama');
			$detail['ay_tahun_lahir']			= $this->input->post('ay_tahun_lahir');
			$detail['ay_kebutuhan_khusus']		= $this->input->post('ay_kebutuhan_khusus');
			$detail['ay_kerja']					= $this->input->post('ay_kerja');
			$detail['ay_pendidikan']			= $this->input->post('ay_pendidikan');
			$detail['ay_penghasilan']			= $this->input->post('ay_penghasilan');
			$detail['ay_notelp']				= $this->input->post('ay_notelp');
			$detail['ay_email']					= $this->input->post('ay_email');
			$detail['ib_nama']					= $this->input->post('ib_nama');
			$detail['ib_tahun_lahir']			= $this->input->post('ib_tahun_lahir');
			$detail['ib_kebutuhan_khusus']		= $this->input->post('ib_kebutuhan_khusus');
			$detail['ib_kerja']					= $this->input->post('ib_kerja');
			$detail['ib_pendidikan']			= $this->input->post('ib_pendidikan');
			$detail['ib_penghasilan']			= $this->input->post('ib_penghasilan');
			$detail['ib_notelp']				= $this->input->post('ib_notelp');
			$detail['ib_email']					= $this->input->post('ib_email');
			$detail['wl_nama']					= $this->input->post('wl_nama');
			$detail['wl_tahun_lahir']			= $this->input->post('wl_tahun_lahir');
			$detail['wl_kebutuhan_khusus']		= $this->input->post('wl_kebutuhan_khusus');
			$detail['wl_kerja']					= $this->input->post('wl_kerja');
			$detail['wl_pendidikan']			= $this->input->post('wl_pendidikan');
			$detail['wl_penghasilan']			= $this->input->post('wl_penghasilan');
			$detail['wl_hubungan']				= $this->input->post('wl_hubungan');
			$detail['tinggi']					= $this->input->post('tinggi');
			$detail['berat']					= $this->input->post('berat');
			$detail['jarak']					= $this->input->post('jarak');
			$detail['waktu']					= $this->input->post('waktu');
			$detail['saudara_kandung']			= $this->input->post('saudara_kandung');

			$this->m_siswa->tambah_c_detail($detail);
			
			?>
				<script type="text/javascript">
					alert("Berhasil Tersimpan");			
				</script>
			<?php
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."daftar/selesai/$id_daftar'>";
		}
	}
}
