<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Getcode extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		//===== Load Database =====
        require_once(__DIR__.'/../config/database.php');
        $conf = $db['default'];
		$link=mysqli_connect($conf['hostname'],$conf['username'],$conf['password'],$conf['database']);
		if(mysqli_connect_errno())
		{}else{
			$this->load->database();
		}
		$this->load->model('m_admin');
		$this->load->model('m_pesan');		
			
		$this->load->library('upload');		
		$this->load->helper('url');
	}

	protected function template($page, $data)
	{
		$name = $this->session->userdata('nama');
			if($name == ""){
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}else{
			$this->load->view('t_panel/header',$data);
			$this->load->view("t_panel/aside");
			$this->load->view("panel/$page");		
			$this->load->view('t_panel/footer');
		}
	}
	
	public function submit()
	{
		$code 	= $this->input->post("code");
		$line2 	= $this->m_admin->cek_code($code);		   
    if($line2 == "1"){
    	echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel/home'>";					    	
    }else{
    	$_SESSION['pesan'] 	= "Installation Code is Wrong!";
			$_SESSION['tipe'] 	= "danger";
    	$r = md5("getcode");
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."getcode?".$r."'>";				
    }    	             
	}

	public function index()
	{		
		$data['judul']	= "Dashboard";				
		$page						= "getcode";	
		$data['isi']		= "dashboard";				
		$this->load->view("panel/$page");	
	}
	
}
