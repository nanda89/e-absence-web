<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====	
		$this->load->model('m_artikel');
		$this->load->model('m_admin');
		$this->load->model('m_kategori');
		$this->load->model('m_siswa');
		//===== Load Library =====
	}
	protected function template($page, $data)
	{
		$this->load->view('t_panel/header',$data);		
		$this->load->view("panel/$page");				
		$this->load->view('t_panel/footer');
	}
	public function index()
	{
		$data['judul']	= "WEB SIAKAD | Cek Kelulusan";				
		$data['isi']	= "kelulusan";				
		$data['dt_kategori']	=	$this->m_kategori->get_all(); 		
		$page			= "kelulusan";				
		$this->template($page,$data);
	}	

	public function cek()
	{		
		$id_daftar	= $this->input->post('id_daftar');								
		$ro = $this->m_siswa->cek_lulus($id_daftar);
		if($ro->num_rows()>0){
			$hasil = $ro->row();
			$c = $hasil->status;
			if($c=="lulus" or $c=="aktif"){
				$pesan = "Selamat atas kelulusan anda";
			}else{
				$pesan = "Maaf, anda belum beruntung";
			}
		}else{
			$pesan = "No Pendaftaran tersebut tidak cocok dengan database kami";
		}		
		echo $pesan;
	}
}
