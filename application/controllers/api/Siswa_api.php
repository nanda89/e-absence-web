<?php

	class Siswa_api extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			$this->load->database();
			$this->load->helper('api_helper');
			$this->load->model(array('Mapi_siswa'=>'siswa'));
		}

		public function profil()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_siswa = $this->input->post('id_siswa');
				$siswa = $this->siswa->get($id_siswa);

				if($siswa == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No siswa data found!';
					echo api_generate(FALSE, $msg);
				}else{
					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Data Found!';

					$siswa['url_photo'] = base_url().'assets/panel/images/'.$siswa['gambar'];
					$res = $siswa;
					echo api_generate(TRUE, $msg, $res);
				}
			}
		}

		public function save()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_siswa = $this->input->post('id_siswa');
				$siswa = $this->siswa->get($id_siswa);

				if($siswa == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No siswa data found!';
					echo api_generate(FALSE, $msg);
				}else{
					$nama_siswa = $this->input->post('nama_siswa');
					$nisn = $this->input->post('nisn');
					$induk_kelas = $this->input->post('induk_kelas');
					$nama_ayah = $this->input->post('nama_ayah');
					$nama_ibu = $this->input->post('nama_ibu');
					$telp_ayah = $this->input->post('telp_ayah');
					$telp_ibu = $this->input->post('telp_ibu');
					
					$alamat = $this->input->post('alamat');
					$tempat_lahir = $this->input->post('tempat_lahir');
					$tanggal_lahir = $this->input->post('tanggal_lahir');
					$no_hp = $this->input->post('no_hp');
					$gambar = $this->input->post('gambar');

					$this->siswa->save(array(
							'nama_lengkap'=>clean($nama_siswa),
							'nisn'=>clean($nisn),
							'no_induk_kelas'=>clean($induk_kelas),
							'nama_ayah'=>strtoupper(clean($nama_ayah)),
							'nama_ibu'=>strtoupper(clean($nama_ibu)),
							'telp_ayah'=>$telp_ayah,
							'telp_ibu'=>$telp_ibu,
							'alamat'=>clean($alamat),
							'tempat_lahir'=>clean($tempat_lahir),
							'tgl_lahir'=>$tanggal_lahir,
							'no_hp'=>$no_hp
						),$id_siswa,$gambar);

					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Saved!';
					echo api_generate(TRUE, $msg);
				}
			}
		}
	}