<?php

	class Guru_api extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			$this->load->database();
			$this->load->helper('api_helper');
			$this->load->model(array('Mapi_guru'=>'guru'));
		}

		public function profil()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_guru = $this->input->post('id_guru');
				$guru = $this->guru->get($id_guru);

				if($guru == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No guru data found!';
					echo api_generate(FALSE, $msg);
				}else{
					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Data Found!';
					$guru['url_photo'] = base_url().'assets/panel/images/'.$guru['gambar'];
					$res = $guru;
					echo api_generate(TRUE, $msg, $res);
				}
			}
		}

		public function save()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_guru = $this->input->post('id_guru');
				$guru = $this->guru->get($id_guru);

				if($guru == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No guru data found!';
					echo api_generate(FALSE, $msg);
				}else{
					$nik = $this->input->post('nik');
					$nama = $this->input->post('nama');
					$no_ktp = $this->input->post('no_ktp');
					$no_hp = $this->input->post('no_hp');
					$email = $this->input->post('email');
					$alamat = $this->input->post('alamat');
					$gambar = $this->input->post('gambar');

					$this->guru->save(array(
							'nik'=>clean($nik),
							'nama'=>clean($nama),
							'no_ktp'=>clean($no_ktp),
							'no_hp'=>$no_hp,
							'email'=>clean($email),
							'alamat'=>clean($alamat)
						),$id_guru,$gambar);

					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Saved!';
					echo api_generate(TRUE, $msg);
				}
			}
		}
	}