<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends Asc_api_controller {
    public function index_get(){
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        $res = array(
            "param_size" => count($queries),
            "data" => $queries
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($res, JSON_PRETTY_PRINT));

        ob_end_flush();
    }

    public function index_post(){
        ob_start();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $this->output->set_status_header(201);
        $this->output->set_content_type('application/json')
                     ->set_output(json_encode(json_decode($stream_clean, true), JSON_PRETTY_PRINT));
        ob_end_flush();
    }

    public function index_put(){
        ob_start();
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $this->output->set_status_header(201);
        $this->output->set_content_type('application/json')
                     ->set_output(json_encode(json_decode($stream_clean, true), JSON_PRETTY_PRINT));
        ob_end_flush();
    }

    public function index_delete(){
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        $res = array(
            "param_size" => count($queries),
            "data" => $queries
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($res, JSON_PRETTY_PRINT));

        ob_end_flush();
    }

    public function kirim_post(){
        ob_start();
        $data = array(
            "user" => $this->input->post("user"),
            "pass" => $this->input->post("pass"),
            "desc" => $this->input->post("desc"),
            "pic"  => $this->input->post("pic"),
            "ico"  => $this->input->post("ico")
        );
        $this->output->set_status_header(201);
        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($data, JSON_PRETTY_PRINT));
        ob_end_flush();
    }

    public function form_post(){
        ob_start();
        $pic = $this->upload('pic');
        $ico = $this->upload('ico');
        $data = array(
            "user" => $this->input->post("user"),
            "pass" => $this->input->post("pass"),
            "desc" => $this->input->post("desc"),
            "pic"  => $pic,
            "ico"  => $ico
        );
        $this->output->set_status_header(201);
        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($data, JSON_PRETTY_PRINT));
        ob_end_flush();
    }

    private function upload($id) {
        $name = "asclib_".$_FILES[$id]['name'];
        $config['file_name']            = $name;
        $config['upload_path']          = FCPATH.'assets/upload/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_ext_tolower']     = true;
        $config['remove_spaces']        = true;
        $config['detect_mime']          = true;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($id)) {
            return $name;
        }
        return null;
    }
}
