<?php

	class Non_guru_api extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			date_default_timezone_set("Asia/Jakarta");
			$this->load->database();
			$this->load->helper('api_helper');
			$this->load->model(array('Mapi_non_guru'=>'non_guru'));
		}

		public function profil()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_non_guru = $this->input->post('id_non_guru');
				$non_guru = $this->non_guru->get($id_non_guru);

				if($non_guru == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No non guru data found!';
					echo api_generate(FALSE, $msg);
				}else{
					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Data Found!';
					$non_guru['url_photo'] = base_url().'assets/panel/images/'.$non_guru['gambar'];
					$res = $non_guru;
					echo api_generate(TRUE, $msg, $res);
				}
			}
		}

		public function save()
		{
			if(!$this->input->post()){
				header('HTTP/1.1 406 Not Acceptable', true, 406);
				$msg = 'No data procced!';
				echo api_generate(FALSE, $msg);
			}else{
				$id_non_guru = $this->input->post('id_non_guru');
				$non_guru = $this->non_guru->get($id_non_guru);

				if($non_guru == NULL){
					header('HTTP/1.1 404 Not Found', true, 404);
					$msg = 'No non guru data found!';
					echo api_generate(FALSE, $msg);
				}else{
					$nip = $this->input->post('nip');
					$nama = $this->input->post('nama');
					$no_ktp = $this->input->post('no_ktp');
					$no_hp = $this->input->post('no_hp');
					$email = $this->input->post('email');
					$alamat = $this->input->post('alamat');
					$gambar = $this->input->post('gambar');
					$jabatan = $this->input->post('jabatan');

					$this->non_guru->save(array(
							'nip'=>clean($nip),
							'nama'=>clean($nama),
							'no_ktp'=>clean($no_ktp),
							'no_hp'=>$no_hp,
							'email'=>clean($email),
							'alamat'=>clean($alamat),
							'jabatan'=>clean($jabatan)
						),$id_non_guru,$gambar);

					header('HTTP/1.1 200 OK', true, 200);
					$msg = 'Saved!';
					echo api_generate(TRUE, $msg);
				}
			}
		}
	}