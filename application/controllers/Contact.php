<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====	
		$this->load->model('m_artikel');
		$this->load->model('m_admin');
		$this->load->model('m_kategori');
		//===== Load Library =====
	}
	protected function template($page, $data)
	{
		$this->load->view('t_web/header',$data);		
		$this->load->view("web/$page");		
		$this->load->view("t_web/aside");
		$this->load->view('t_web/footer');
	}
	public function index()
	{
		$data['judul']	= "WEB SIAKAD | Kontak";				
		$data['isi']	= "contact";				
		$data['dt_kategori']	=	$this->m_kategori->get_all(); 		
		$page			= "contact";				
		$this->template($page,$data);
	}	

	public function save()
	{		
		$data['nama']	= $this->input->post('nama');						
		$data['email']	= $this->input->post('email');						
		$data['asal']	= $this->input->post('asal');						
		$data['pesan']	= $this->input->post('pesan');						
		$pesan			= $this->input->post('pesan');						
		$nama			= $this->input->post('nama');						
		$email			= $this->input->post('email');						
		$data['waktu']			= date('d-M-Y') ." at ". date('H:i:s');
		$data['status'] = 'unread';
		$this->m_admin->tambah_pesan($data);

				$s = "SELECT * FROM tabel_setting WHERE id_setting=1";
				$ys = $this->db->query($s);
				$ce = $ys->row();
				require_once(APPPATH.'libraries/PHPMailerAutoload.php');
				
				$akun	= $ce->akun_gmail;	
				$akun_webmail	= $ce->akun_webmail;					
				$pass_webmail	= $ce->pass_webmail;
				$host			= $ce->host_webmail;	
					
			 	$mail = new PHPMailer;
			
				 $mail->isSMTP();
				 $mail->Host = 'mail.'.$host.''; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
				 $mail->SMTPAuth = true;
				 $mail->Username = $akun_webmail; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
				 $mail->Password = $pass_webmail; //masukan kata sandi
				 $mail->Port = 587; //port tidak usah di ubah, biarkan 587
				
				 $mail->setFrom($email, 'Pesan'); //email pengirim
				 $mail->addAddress($akun, 'penerima'); //email penerima
				 $mail->addReplyTo($akun, 'pengingat');
				 $mail->isHTML(true);
				
			
					
				        ///atur pesan email disini
				 $mail->Subject = 'Kontak';
				 $isiemail = $pesan;
				 $isiemail .= "<b>Nama :".$nama."</b><br>";
				 $isiemail .= "<b>Email :".$email."</b>";
				 $mail->Body    = $isiemail;
				 //$mail->Body    = 
				 $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				
				 if(!$mail->send()) {
				 	echo "<script type='text/javascript'>
							alert('Gagal dikirm');			
						</script>";
				 	echo "<meta http-equiv='refresh' content='0; url=".base_url()."contact'>";
				 } else {
				  echo "<script type='text/javascript'>
							alert('Pesan berhasil dikirm');			
						</script>";
				  	echo "<meta http-equiv='refresh' content='0; url=".base_url()."contact'>";
				 }			
	}
}
