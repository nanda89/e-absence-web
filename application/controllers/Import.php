<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====
				
		//===== Load Library =====
	}
	protected function template($page, $data)
	{
		//$this->load->view('t_setting/header',$data);		
		$this->load->view("setting/$page",$data);		
		//$this->load->view("t_setting/aside");
		//$this->load->view('t_setting/footer');
	}	
	public function index()
	{
		  $isi_file = file_get_contents('./assets/dbs_absence.sql');
		  $string_query = rtrim( $isi_file, '\n;' );
		  $array_query = explode(';', $string_query);
		  foreach($array_query as $query)
		  {
		    $this->db->query($query);
		  }
		  // elearning
		  $this->load->model("M_asc_install", "asci");
		  $v ='1';
		  $b = '2';
		  $f ='1-2-trigger.sql';
		  $var = array(
	            "version" => $v,
	            "batch" => $b,
	            "sql" => $f
          );
          $q = $this->asci->updateData($var);
		  $v ='1';
		  $b = '3';
		  $f ='1-3-trigger.sql';
		  $var = array(
	            "version" => $v,
	            "batch" => $b,
	            "sql" => $f
          );
          $q = $this->asci->updateData($var);
		  echo "nihil";
	}
}
