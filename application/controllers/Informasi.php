<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		$this->load->helper('url');
		//===== Load Model =====	
		$this->load->model('m_artikel');
		$this->load->model('m_admin');
		$this->load->model('m_kategori');
		//===== Load Library =====
	}
	protected function template($page, $data)
	{
		$this->load->view('t_web/header',$data);		
		$this->load->view("web/$page");		
		//$this->load->view("t_web/aside");
		$this->load->view('t_web/footer');
	}
	public function index()
	{
		$data['judul']	= "WEB SIAKAD | Info Pendaftaran";				
		$data['isi']	= "info_daftar";						
		$page			= "info_daftar";				
		$this->template($page,$data);
	}	

	public function save()
	{		
		$data['nama']	= $this->input->post('nama');						
		$data['email']	= $this->input->post('email');						
		$data['asal']	= $this->input->post('asal');						
		$data['pesan']	= $this->input->post('pesan');						
		$data['waktu']			= date('d-M-Y') ." at ". date('H:i:s');
		$data['status'] = 'unread';
		$this->m_admin->tambah_pesan($data);
		?>
			<script type="text/javascript">
				alert("Pesan telah terkirim");			
			</script>
		<?php
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."contact'>";	
	}
}
