<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		$this->load->database();
		//$this->load->helper('url');
		//===== Load Model =====
		$this->load->model("m_admin");
				
		//===== Load Library =====
		$this->load->library('upload');
		$this->load->dbutil();

	}
	protected function template($page, $data)
	{
		$name = $this->session->userdata('nama');
		if($name=="")
		{
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		}else{
			$this->load->view('t_panel/header',$data);
			$this->load->view("t_panel/aside");
			$this->load->view("panel/$page");		
			$this->load->view('t_panel/footer');
		}
	}
	public function index()
	{		
		$data['title']	= "Setting";			
		$data['isi']	= "backup";				
		$data['judul1']	= "Setting";			
		$data['judul2']	= "Backup & Restore";
		$page						= "backup";		
		$this->template($page,$data);
	}
	public function back()
	{		
 
    // nyiapin aturan untuk file backup
    $aturan = array(    
            'format'      => 'zip',            
            'filename'    => 'my_db_backup.sql'
          );


    $backup = $this->dbutil->backup($aturan);

    $nama_database = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
    $simpan = '/backup'.$nama_database;

    $this->load->helper('file');
    write_file($simpan, $backup);


    $this->load->helper('download');
    force_download($nama_database, $backup);
	}


	function restoredb(){
		$config['upload_path'] = './uploads/backup/'; // jangan lupa buat folder baru dengan nama folder temp sejajar dengan folder application dan system
    $config['allowed_types'] = '*';
    $file_name = $_FILES['db_file']['name'];
    $this->load->library('upload', $config);
    $fupload 	= $_FILES['db_file'];

		$nama 		= $_FILES['db_file']['name'];
		if(isset($fupload)){
			$lokasi_file 	= $fupload['tmp_name'];
			$direktori  	= "uploads/backup/".$nama;
			move_uploaded_file($lokasi_file,"$direktori");

      $path_file = "uploads/backup/".$nama;

      $ext = pathinfo($file_name, PATHINFO_EXTENSION);
      if ($ext == 'sql') {
          $isi_file = file_get_contents($path_file);
          $string_query = rtrim($isi_file, "\n;");
          $array_query = explode(";", $string_query);

          $db_debug = $this->db->db_debug; //save setting
          $this->db->db_debug = FALSE; //disable debugging for queries

          foreach ($array_query as $query) {
              $this->db->query($query);
          };
          $this->db->db_debug = $db_debug;
           
          $_SESSION['pesan'] 	= "Restore berhasil!";
					$_SESSION['tipe'] 	= "info";
      } else {
          $_SESSION['pesan'] 	= "Restore gagal!";
					$_SESSION['tipe'] 	= "danger";
      }
      @unlink($path_file);
    }    
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."backup'>";				
	}	
	
}
