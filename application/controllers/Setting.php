<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct()
	{		
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		//===== Load Database =====
		//$this->load->database();
		//$this->load->helper('url');
		//===== Load Model =====
				
		//===== Load Library =====
	}
	protected function template($page, $data)
	{
		//$this->load->view('t_setting/header',$data);		
		$this->load->view("setting/$page",$data);		
		//$this->load->view("t_setting/aside");
		//$this->load->view('t_setting/footer');
	}
	public function index()
	{		
		$data['judul']	= "Installation";				
		$data['isi']	= "setting";				
		$page			= "index";		
		$this->template($page,$data);
	}
	public function set_db()
	{					
		$server = $_POST['server'];
		$dbname = $_POST['dbname'];
		$user 	= $_POST['user'];
		$password = $_POST['password'];
		$conn = new mysqli($server, $user, $password);		
		$cek = $conn->query("CREATE DATABASE ".$dbname);		
		if($cek){
			echo "nihil";		
		}				
	}	
	public function import()
	{							
				
		  $isi_file = file_get_contents('./assets/dbs_absence.sql');
		  $string_query = rtrim( $isi_file, '\n;' );
		  $array_query = explode(';', $string_query);
		  foreach($array_query as $query)
		  {
		    $this->db->query($query);
		  }
		 
		 	unlink("c:/xampp/htdocs/web_absence/assets/panel/font-awesome/fonts/SEC.PDO");
			echo "nihil";
	}
}

