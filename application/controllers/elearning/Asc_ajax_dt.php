<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ajax_dt extends Asc_base_controller
{
    // DATATABLE
    public function grup_siswa()
    {
        $this->datatables->from("asc_grup_siswa g")
                        ->select("g.nama, k.kelas")
                        ->join('tabel_kelas k', 'k.id_kelas=g.id_kelas');
        echo $this->datatables->generate();
    }

    public function submission($id_jadwal_ujian)
    {
        $this->datatables->select("p.id_peserta_ujian, p.id_jadwal_ujian, s.nisn, s.nama_lengkap, p.waktu_join, p.waktu_submit,p.nilai, p.doc_status");
        $where = array(
            'p.id_jadwal_ujian' => $id_jadwal_ujian
        );
        $this->datatables->where($where);
        $this->datatables->from("asc_peserta_ujian p");
        $this->datatables->join("tabel_siswa s", "s.id_siswa=p.id_siswa");
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_submission/form/$1/$2','<span class="fa fa-edit"></span>',array('class'=>'btn btn-primary btn-flat', 'title'=>'Jawaban Siswa')),
                'id_jadwal_ujian, id_peserta_ujian');
        echo $this->datatables->generate();
    }

    public function jadwal_ujian()
    {
        $this->datatables->select("j.id_jadwal_ujian,
            m.mapel , u.judul, u.deskripsi, DATE_FORMAT(j.waktu_mulai,'%d %M %Y %H:%i:%s') AS waktu_mulai, DATE_FORMAT(j.waktu_selesai,'%d %M %Y %H:%i:%s') AS waktu_selesai,, j.doc_status")
                    ->from("asc_jadwal_ujian j")
                    ->join("asc_ujian u","u.id_ujian=j.id_ujian","inner")
                    ->join("tabel_mapel m","m.id_mapel=u.id_mapel","inner");
        $this->datatables->where('j.id_guru', $this->id_guru);
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_jadwal_ujian/form/$1','<span class="fa fa-edit"></span>',array('class'=>'btn btn-primary btn-flat', 'title'=>'Ubah'))
                . '&nbsp;' .
                anchor('elearning/asc_jadwal_ujian/delete/$1','<span class="fa fa-trash"></span>',array('class'=>'btn btn-danger btn-flat', 'title'=>'Hapus','onclick'=>"return confirm('Anda yakin menghapus data ini ?');"))
                . '&nbsp;' .
                anchor('elearning/asc_jadwal_ujian/publish/$1','<span class="fa fa-eye"></span>',array('class'=>'btn btn-info btn-flat', 'title'=>'Publish','onclick'=>"return confirm('Anda yakin mempublish jadwal ini ?');"))
                . '&nbsp;' .
                anchor('elearning/asc_submission/index/$1','<span class="fa fa-book"></span>',array('class'=>'btn bg-yellow btn-flat', 'title'=>'Submission')),
                'id_jadwal_ujian');
        echo $this->datatables->generate();
    }

    public function materi_penerima()
    {
        $id_materi = $this->input->get('id_materi');
        if ( empty($id_materi) )
        {
            $id_materi = 0;
        }
        $this->datatables->select("p.id_materi_penerima, p.`type`, COALESCE(s.nama_lengkap,k.kelas) as penerima")
                        ->from("asc_materi_penerima p")
                        ->join('tabel_siswa s', 's.id_siswa=p.id_siswa', 'left')
                        ->join('tabel_kelas k', 'k.id_kelas=p.id_kelas', 'left')
                        ->where('p.id_guru', $this->id_guru)
                        ->where('p.id_materi', $id_materi);
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_materi_penerima/delete?id_penerima=$1','<span class="fa fa-trash"></span>',array('class'=>'btn btn-danger btn-flat','onclick'=>"return confirm('Anda yakin menghapus data ini ?');")),
                'id_materi_penerima');
        echo $this->datatables->generate();
    }

    public function pertanyaan($id_ujian)
    {
        $this->datatables->from("asc_pertanyaan")
                        ->select("id_ujian, id_pertanyaan, no_urut, pertanyaan, jawaban")
                        ->where('id_ujian', $id_ujian);
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_pertanyaan/form/$1/$2','<span class="fa fa-edit"></span>',array('class'=>'btn btn-primary btn-flat'))
                . '&nbsp;' .
                anchor('elearning/asc_pertanyaan/delete/$1/$2','<span class="fa fa-trash"></span>',array('class'=>'btn btn-danger btn-flat','onclick'=>"return confirm('Anda yakin menghapus data ini ?');")),
                'id_ujian,id_pertanyaan');
        echo $this->datatables->generate();
    }

    public function ujian()
    {
        $this->datatables->from("asc_ujian m")
                        ->select("m.id_ujian, m.kode, k.mapel, m.judul, m.deskripsi, m.tipe,  m.jumlah_pertanyaan, m.doc_status")
                        ->where('m.id_guru', $this->id_guru)
                        ->join('tabel_mapel k', 'k.id_mapel=m.id_mapel');
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_ujian/form/$1','<span class="fa fa-edit"></span>',array('class'=>'btn btn-primary btn-flat', 'title'=>'Ubah'))
                . '&nbsp;' .
                anchor('elearning/asc_ujian/delete/$1','<span class="fa fa-trash"></span>',array('class'=>'btn btn-danger btn-flat', 'title'=>'Hapus','onclick'=>"return confirm('Anda yakin menghapus data ini ?');"))
                . '&nbsp;' .
                anchor('elearning/asc_ujian/publish/$1','<span class="fa fa-eye"></span>',array('class'=>'btn btn-info btn-flat', 'title'=>'Publish','onclick'=>"return confirm('Anda yakin mempublish data ini ?');"))
                . '&nbsp;' .
                anchor('elearning/asc_pertanyaan/index/$1','<span class="fa fa-question"></span>',array('class'=>'btn bg-purple btn-flat', 'title'=>'Pertanyaan'))
                ,
                'id_ujian');
        echo $this->datatables->generate();
    }

    public function materi()
    {
        $this->datatables->from("asc_materi m")
                        ->select("m.id_materi, k.mapel, m.judul, m.materi, m.doc_status")
                        ->where('m.id_guru', $this->id_guru)
                        ->join('tabel_mapel k', 'k.id_mapel=m.id_mapel');
        $this->datatables->add_column(
                'tindakan',
                anchor('elearning/asc_materi/form/$1','<span class="fa fa-edit"></span>',array('class'=>'btn btn-primary btn-flat'))
                . '&nbsp;' .
                anchor('elearning/asc_materi/delete?id_materi=$1','<span class="fa fa-trash"></span>',array('class'=>'btn btn-danger btn-flat','onclick'=>"return confirm('Anda yakin menghapus data ini ?');"))
                . '&nbsp;' .
                anchor('elearning/asc_materi_penerima/index?id_materi=$1','<span class="fa fa-eye"></span>',array('class'=>'btn btn-info btn-flat'))
                ,
                'id_materi');
        echo $this->datatables->generate();
    }

    // SELECT2
    public function ujian_select()
    {
        $like = array("judul", "kode");
        $where = array(
            'id_guru' => $this->id_guru,
            'doc_status' => $this->config->item('doc_status_publish')
        );
        $this->db->select( "CONCAT(judul,' (',kode,')') as text, id_ujian as id");
        $this->db->from("asc_ujian");
        $this->db->limit(20);
        $search = $this->input->get('search');
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( !empty($search) && $like != null)
        {
            $this->db->like('judul', $search, 'both');
        }
        $query = $this->db->get()->result_array();
        echo json_encode(array('items' => $query));
    }

    public function mapel_select()
    {
        $this->db->select("id_mapel as id, mapel as text")
                    ->from('tabel_mapel');
        $search = $this->input->get('search');
        if ( !empty($search) )
        {
            $this->db->like('mapel', $search, 'both');
        }
        $query = $this->db->get()->result_array();
		echo json_encode(array('items' => $query));
    }

    public function kelas_select()
    {
        return $this->generate_select2("tabel_kelas", array("id_kelas" => "id", "kelas" => "text"), array("kelas"));
    }

    public function siswa_select()
    {
        return $this->generate_select2("tabel_siswa", array("id_siswa" => "id", "nama_lengkap" => "text"), array("nama_lengkap", "nisn"));
    }

    private function generate_select2($table, $columns, $like = null, $where = null, $limit = 20)
    {
        foreach($columns as $key => $value)
        {
            $this->db->select( $key . " as " . $value);
        }
        $this->db->from($table);
        $this->db->limit($limit);
        $search = $this->input->get('search');
        if ( $where != null )
        {
            $this->db->where($where);
        }
        if ( !empty($search) && $like != null)
        {
            foreach($like as $value)
            {
                $this->db->or_like($value, $search, 'both');
            }            
        }
        $query = $this->db->get()->result_array();
		echo json_encode(array('items' => $query));
    }
}