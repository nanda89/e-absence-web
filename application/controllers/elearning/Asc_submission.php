<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_submission extends Asc_base_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_ujian", "m_ujian");
        $this->load->model("m_asc_jadwal_ujian", "m_jadwal");
        $this->load->model("m_asc_peserta_ujian", "m_peserta");
        $this->load->model("m_asc_pilihan", "m_pilihan");
        $this->load->model("m_asc_submission", "m_sub");
    }

    private function to_jadwal_ujian()
    {
        redirect(base_url() . "elearning/asc_jadwal_ujian/index");
    }

    private function check_jadwal($id_jadwal_ujian)
    {
        if ( $id_jadwal_ujian == null )
        {
            return null;
        }
        $where = array(
            't.id_jadwal_ujian' => $id_jadwal_ujian,
            't.id_guru' => $this->id_guru
        );
        $jadwal_ujian = $this->m_jadwal->single($where);
        if ( $jadwal_ujian == null || $jadwal_ujian['doc_status'] == $this->config->item("doc_status_draft") )
        {
            return null;
        }
        return $jadwal_ujian;
    }

    public function publish($id_jadwal_ujian=null)
    {
        $jadwal_ujian = $this->check_jadwal($id_jadwal_ujian);
        if ( $id_jadwal_ujian == null )
        {
            $this->to_jadwal_ujian();
            return;
        }
        if ( $this->m_sub->publish($jadwal_ujian) )
        {
            $this->set_alert(self::ALERT_SUCCESS_PUBLISH);
        }
        else
        {
            $this->set_alert_danger($this->m_sub->get_error());
        }
        $this->to_index('index/' . $id_jadwal_ujian);
    }

    public function index($id_jadwal_ujian=null)
    {
        $jadwal_ujian = $this->check_jadwal($id_jadwal_ujian);
        if ( $id_jadwal_ujian == null )
        {
            $this->to_jadwal_ujian();
            return;
        }
        $data = array();
        $data['jadwal_ujian'] = $jadwal_ujian;
        $data['id_jadwal_ujian'] = $id_jadwal_ujian;
        $this->set_title('Submission');
        $this->set_subtitle($jadwal_ujian['judul']);
        $this->template('elearning/asc_submission_list', $data , 'panel/elearning/asc_submission_js');
    }

    public function form($id_jadwal_ujian, $id_peserta)
    {
        $jadwal_ujian = $this->check_jadwal($id_jadwal_ujian);
        if ( $id_jadwal_ujian == null )
        {
            $this->to_jadwal_ujian();
            return;
        }
        $where = array(
            "p.id_peserta_ujian" => $id_peserta,
            "p.id_jadwal_ujian" => $id_jadwal_ujian
        );
        $peserta = $this->m_peserta->list_by($where, null);
        if ( count($peserta) <= 0 )
        {
            $this->to_jadwal_ujian();
            return;
        }
        $peserta = $peserta[0];
        $where = array(
            't.id_ujian' => $jadwal_ujian['id_ujian']
        );
        $ujian = $this->m_ujian->get($where, true);
        if ( $ujian == null )
        {
            $this->to_jadwal_ujian();
            return;
        }
        $judul_header = $peserta['nama_lengkap'];
        $data = array();
        $data['ujian'] = $ujian;
        $data['peserta'] = $peserta;
        $data['judul_header'] = $judul_header;
        $data['jadwal_ujian'] = $jadwal_ujian;
        $data['id_jadwal_ujian'] = $id_jadwal_ujian;
        $data['id_peserta_ujian'] = $id_peserta;
        /* GENERATE JAWABAN SERTA PERTANYAAN */
        $where = array(
            "s.id_peserta_ujian" => $peserta["id_peserta_ujian"]
        );
        $list_sub = $this->m_sub->list_by($where, null);
        if ( $this->input->post("save") != null )
        {
            $total = count($list_sub);
            for($i=0; $i<$total; $i++)
            {
                $id_submission = $this->input->post("id_submission" . $i);
                $nilai = $this->input->post("nilai" . $i);
                $koreksi = $this->input->post("koreksi" . $i);
                $param = array(
                    'nilai' => $nilai,
                    'koreksi' => $koreksi,
                    'doc_status' => $this->config->item('doc_status_koreksi')
                );
                $where = array(
                    'id_submission' => $id_submission
                );
                $this->m_sub->save($param, $where, true);
            }
            $param = array('doc_status' => $this->config->item('doc_status_koreksi')
            );
            $where = array(
                'id_peserta_ujian' => $peserta['id_peserta_ujian']
            );
            $this->m_peserta->save($param, $where, true);
            $this->set_alert(self::ALERT_SUCCESS_SAVE);
        }
        $jawaban = array();
        foreach ($list_sub as $sub)
        {
            $param = $sub;
            $param["pilihan"] = null;
            if ( $sub["tipe"] == $this->config->item("ujian_tipe")["p"] )
            {
                $where = array("id_pertanyaan" => $sub["id_pertanyaan"]);
                $param["pilihan"] = $this->m_pilihan->get($where);
            }
            $jawaban[] = $param;
        }
        $data['jawaban'] = $jawaban;
        $data['readonly'] = $peserta['doc_status'] == $this->config->item('doc_status_publish');
        $this->set_title('Submission');
        $this->set_subtitle($jadwal_ujian['judul']);
        $this->template('elearning/asc_submission_form', $data , 'panel/elearning/asc_submission_js');
    }
}