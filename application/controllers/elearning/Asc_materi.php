<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_materi extends Asc_base_controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_title('Materi');
    }

    public function index()
    {
        $data = array();
        $this->set_subtitle('List');
        $this->template('elearning/asc_materi_list', null , 'panel/elearning/asc_materi_js');
    }

    public function delete()
    {
        $id_materi = $this->input->get("id_materi");
        if ( empty($id_materi) )
        {
            $this->to_index();
            return;
        }

        $this->load->model('m_asc_materi', 'm_materi');
        $param = array(
            'id_materi' => $id_materi,
            'id_guru' => $this->id_guru,
        );
        $materi = $this->m_materi->get($param, true);
        if ( $materi == false )
            return $this->to_index();

        if ( $materi['doc_status'] == self::DOC_STATUS_DRAFT && $this->m_materi->delete($param) )
        {
            $this->set_alert(self::ALERT_SUCCESS_DELETE);
        }
        else
        {
            if ( $materi['doc_status'] == self::DOC_STATUS_PUBLISH )
            {
                $this->set_alert_ilegal_delete( $materi['doc_status'] );
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
        }
        $this->to_index();
    }

    public function form($id = null)
    {
        $this->load->model('m_asc_materi', 'm_materi');
        $id_materi = $id;
        if ( $id_materi == null )
        {
            $id_materi = $this->input->post("id_materi");
        }
        $judul_header = '';
        $subtitle = 'Form baru';
        $materi = null;
        if ( !empty($id_materi) )
        {
            $filter = array(
                'id_materi' => $id_materi,
                'id_guru' => $this->id_guru
            );
            $materi = $this->m_materi->get($filter, true);
            if ( $materi == false )
            {
                $this->to_index();
                return;
            }
            $subtitle = 'Form edit';
            $judul_header = $materi['judul'];
        }
        else
        {
            $subtitle = 'Form baru';
            $id_materi = -1;
        }
        $param = array('id_guru' => $this->id_guru);
        if ( $this->m_materi->validate() )
        {
            $is_allow_save = true;
            $file_name = $_FILES['gambar']['name'];
            if ( empty( $file_name ) )
            {
                if ( $materi != null )
                {
                    $param['gambar'] = $materi['gambar'];
                }
            }
            else
            {
                $removed_file = '';
                if ( $materi != null )
                {
                    $removed_file = $materi['gambar'];
                }
                $gambar = $this->upload_file('gambar', $file_name , self::UPLOAD_TYPE_IMAGE, $removed_file);
                if ( $gambar == false )
                {
                    $is_allow_save = false;
                    $this->set_alert_danger($this->upload->display_errors());
                    return;
                }
                $param['gambar'] = $gambar;
            }
            if ( $is_allow_save )
            {
                $where = null;
                if ( $id_materi > -1 )
                {
                    $where = array('id_materi' => $id_materi);
                }
                if ( $this->m_materi->save($param , $where) )
                {
                    $this->set_alert(self::ALERT_SUCCESS_SAVE);
                }
                else
                {
                    $this->set_alert_danger(self::ALERT_FAILED);
                }
                $this->to_index();
                return;
            }
        }
        $data = array();
        $data['materi'] = $materi;
        $data['id_materi'] = $id_materi;
        $data['judul_header'] = $judul_header;
        $this->set_subtitle( $subtitle ) ;
        $this->template('elearning/asc_materi_form', $data , 'panel/elearning/asc_materi_js');
    }
}