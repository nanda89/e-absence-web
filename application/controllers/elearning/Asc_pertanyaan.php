<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_pertanyaan extends Asc_base_controller {

	private $ujian;
	private $data = array();
	// uri : {base_url}/elearning/asc_pertanyaan/{method}/id_ujian/id_pertanyaan
	public function __construct()
	{
		parent::__construct();
		$id_ujian = $this->uri->segment(4);
		if ( empty($id_ujian) )
			$this->to_ujian();
		$this->load->model("m_asc_ujian","m_ujian");
		$param = array(
            'id_ujian' => $id_ujian,
            'id_guru' => $this->id_guru,
        );
		$this->ujian = $this->m_ujian->get($param, true);
		if ( $this->ujian == false )
			$this->to_ujian();

		$this->data['ujian'] = $this->ujian;
        $this->data['readonly'] = ($this->ujian['doc_status'] == $this->config->item('doc_status_publish'));
        $this->load->model("m_asc_pertanyaan","m_pertanyaan");
	}

	private function to_ujian()
	{
		redirect(base_url() . 'elearning/asc_ujian');
	}

    public function index($id_ujian)
    {
        $this->set_title('Pertanyaan');
        $this->set_subtitle($this->ujian['judul']);
        $this->template('elearning/asc_pertanyaan_list', $this->data , 'panel/elearning/asc_pertanyaan_js');
    }

    public function delete($id_ujian, $id_pertanyaan = null)
    {
        if ( $this->ujian['doc_status'] == $this->config->item("doc_status_publish") )
        {
            $this->set_alert_danger('Ujian sudah dipublish, pertanyaan tidak bisa dihapus');
            $this->to_index('index/' . $id_ujian);
            return;
        }
        $where = array(
            'id_guru' => $this->id_guru,
            'id_ujian' => $id_ujian,
            'id_pertanyaan' => $id_pertanyaan
        );
        $pertanyaan = $this->m_pertanyaan->get($where, true);
        if ( $pertanyaan != null && $this->m_pertanyaan->delete($where))
        {
            $this->set_alert(self::ALERT_SUCCESS_DELETE);
        }
        else
        {
            $this->set_alert_danger(self::ALERT_FAILED);
        }
        $this->to_index('index/' . $id_ujian);
    }

    public function form($id_ujian, $id_pertanyaan = null)
    {
    	$this->load->model("m_asc_pertanyaan", "m_pertanyaan");
    	$this->load->model("m_asc_pilihan", "m_pilihan");
		$this->data['pertanyaan'] = null;
        $judul = "Pertanyaan baru";
		$no_urut = $this->m_pertanyaan->generate_no_urut($this->ujian);
		$this->data['no_urut'] = $no_urut;
		$this->data['list_pilihan'] = null;
        $pertanyaan = null;
		if ( $id_pertanyaan != null )
		{
            $where = array(
                'id_ujian' => $this->ujian['id_ujian'],
                'id_guru' => $this->id_guru,
                'id_pertanyaan' => $id_pertanyaan
            );
            $pertanyaan = $this->m_pertanyaan->get($where, true);
            if ( $pertanyaan != null )
            {
                $this->data['pertanyaan'] = $pertanyaan;
                $judul = $pertanyaan['pertanyaan'];
                $no_urut = $pertanyaan['no_urut'];
                if ( $this->ujian['tipe'] == $this->config->item('ujian_tipe')['p'] )
                {
                    $param_pilihan = array('id_pertanyaan' => $id_pertanyaan);
                    $this->data['list_pilihan'] = $this->m_pilihan->get($param_pilihan);
                }
            }
		}
        if ( !$this->data['readonly'] && $this->m_pertanyaan->validate() )
        {
            $param = array();
            $param['id_ujian'] = $id_ujian;
            $param['id_pertanyaan'] = $id_pertanyaan;
            $param['id_guru'] = $this->id_guru;
            $is_allow_save = true;
            $file_name = $_FILES['gambar']['name'];
            if ( empty( $file_name ) )
            {
                if ( $pertanyaan != null )
                {
                    $param['gambar'] = $pertanyaan['gambar'];
                }
            }
            else
            {
                $removed_file = '';
                if ( $pertanyaan != null )
                {
                    $removed_file = $pertanyaan['gambar'];
                }
                $gambar = $this->upload_file('gambar', $file_name , self::UPLOAD_TYPE_IMAGE, $removed_file,$this->id_guru);
                if ( $gambar == false )
                {
                    $this->set_alert_danger($this->upload->display_errors());
                    $is_allow_save = false;
                }
                $param['gambar'] = $gambar;
            }
            if ( $is_allow_save )
            {
                $where = null;
                if ( $pertanyaan != null )
                {
                    $where = array('id_pertanyaan' => $id_pertanyaan);
                }
                if ( $this->m_pertanyaan->save($param , $where) )
                {
                    
                }
                else
                {
                    $this->set_alert_danger(self::ALERT_FAILED);
                }
                //$this->to_index('index/' .$id_ujian.'/'.$id_pertanyaan);
                //return;
            }
            // simpan pilihan
            if ( $pertanyaan == null )
            {
                $id_pertanyaan = $this->db->insert_id();
            }
            if ( $id_pertanyaan != null && $this->ujian['tipe'] == $this->config->item('ujian_tipe')['p'] )
            {
                for ($i=0; $i<$this->ujian['jumlah_pilihan']; $i++)
                {
                    $huruf = $this->input->post('huruf_' . $i);
                    $param =  array(
                        'id_pertanyaan' => $id_pertanyaan,
                        'text' => $this->input->post('pilihan_' . $i),
                        'huruf' => $huruf
                    );
                    $is_allow_save = true;
                    $where = array(
                        'id_pertanyaan' => $id_pertanyaan,
                        'huruf' => $huruf
                    );
                    $pilihan = $this->m_pilihan->get($where, true);
                    $file_name = $_FILES['gambar_' . $i]['name'];
                    if ( empty( $file_name ) )
                    {
                        if ( $pilihan != null )
                        {
                            $param['gambar'] = $pilihan['gambar'];
                        }
                    }
                    else
                    {
                        $removed_file = '';
                        if ( $pilihan != null )
                        {
                            $removed_file = $pilihan['gambar'];
                        }
                        $gambar = $this->upload_file('gambar_' . $i, $file_name , self::UPLOAD_TYPE_IMAGE, $removed_file,$this->id_guru);
                        if ( $gambar == false )
                        {
                            $is_allow_save = false;
                            $this->set_alert_danger('Pilhan ' . $huruf . ' : '. $this->upload->display_errors());
                            break;// hindari proses loop berikutnya
                        }
                        $param['gambar'] = $gambar;
                    }
                    if ( $pilihan == null )
                    {
                        $where = null;
                    }
                    if ( $is_allow_save )
                    {
                        $this->m_pilihan->save($param, $where, true);
                    }
                }
            }
            if ( $is_allow_save )
            {
                $this->set_alert(self::ALERT_SUCCESS_SAVE);
                $this->to_index('index/' .$id_ujian.'/'.$id_pertanyaan);
                return;
            }
        }
        $this->data['no_urut'] = $no_urut;
        $this->data['judul'] = $judul;
		$this->set_title('Pertanyaan ' );
        $this->set_subtitle($this->ujian['judul']);
        $this->template('elearning/asc_pertanyaan_form', $this->data , 'panel/elearning/asc_pertanyaan_js');
    }
}