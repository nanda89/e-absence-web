<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_install extends CI_Controller {
    private $page = "asc_install";

    function __construct() {
        parent::__construct();
		$this->load->database();
        $this->load->helper('directory');
        $this->load->model('m_admin');
        $this->load->model("M_asc_install", "asci");
    }

    function index() {
        $lvl = $this->session->userdata('level');
        if ($lvl != "administrator") {
            $this->gotoLogin();
        } else {
            $obj = $this->asci->getNotInstalled();
            if (!$obj) { $this->gotoHome(); }
            $this->template($this->page, $obj);
        }
    }

    function doUpdate(){
        $var = array(
            "version" => $this->input->post("v"),
            "batch" => $this->input->post("b"),
            "sql" => $this->input->post("f")
        );
        $q = $this->asci->updateData($var);
        if ($q['status']) $this->gotoHome();
    }

	private function template($page, $data) {
		$name = $this->session->userdata('nama');
		if ($name == "") {
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
		} else {
            $content = array(
                'judul1' => "Install DB",
                'judul2' => "",
                'isi' => "Install DB",
                'title' =>  "Install DB"
            );
			$this->load->view('t_panel/header',$content);
			$this->load->view("t_panel/aside");
			$page = $this->load->view("panel/elearning/$page", array('data' => $data), TRUE);
            $content['page'] = $page;
            $this->load->view("panel/elearning/asc_body", $content);
			$this->load->view('t_panel/footer');
		}
	}

    public function gotoHome() { echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel/home'>"; }

    private function gotoLogin() {
		session_unset();
        $_SESSION['pesan'] = "Database tidak update, mohon hubungi admin anda.";
        $_SESSION['tipe'] = "danger";
        echo "<meta http-equiv='refresh' content='0; url=".base_url()."panel'>";
    }
}
