<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ujian extends Asc_base_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_ujian", "m_ujian");
    }

    public function index()
    {
        $data = array();
        $this->set_title('Ujian');
        $this->set_subtitle('List');
        $this->template('elearning/asc_ujian_list', $data , 'panel/elearning/asc_ujian_js');
    }

    public function delete($id_ujian = null)
    {
        $this->load->model("m_asc_pertanyaan", "m_pertanyaan");
        $where = array(
                'id_ujian' => $id_ujian,
                'id_guru' => $this->id_guru
            );
        $ujian = $this->m_ujian->get($where, true);
        if ( $ujian != null )
        {
            if ( strtolower($ujian['doc_status']) == $this->config->item('doc_status_draft') )
            {
                if ( $this->m_ujian->delete($where) )
                {
                    $this->m_pertanyaan->delete($where);
                    $this->set_alert(self::ALERT_SUCCESS_DELETE);
                }
                else
                {
                    $this->set_alert_danger(self::ALERT_FAILED);
                }
            }
            elseif ( strtolower($ujian['doc_status']) == $this->config->item('doc_status_publish'))
            {
                $this->set_alert_ilegal_delete( $ujian['doc_status'] );
            }
        }
        $this->to_index();
    }
    public function publish($id_ujian = null)
    {
        $this->load->model("m_asc_pertanyaan", "m_pertanyaan");
        $where = array(
                'id_ujian' => $id_ujian,
                'id_guru' => $this->id_guru
            );
        $ujian = $this->m_ujian->get($where, true);
        if ( $ujian != null && strtolower($ujian['doc_status']) == $this->config->item('doc_status_draft'))
        {
            if ( $this->m_ujian->publish($ujian) )
            {
                $this->set_alert(self::ALERT_SUCCESS_PUBLISH);
            }
            else
            {
                $this->set_alert_danger($this->m_ujian->get_error());
            }
        }
        $this->to_index();
    }

    public function form($id_ujian = null, $tipe = 'e')
    {
        $data = array();
        $data['ujian_tipe'] = $this->config->item("ujian_tipe")[$tipe];
        $data['doc_status'] = $this->config->item("doc_status_draft");
        $ujian = null;
        $data['id_ujian'] = 0;
        $readonly = false;
        $judul_header = "Baru";
        $where = null;
        if ( $id_ujian == null )
        {
            $id_ujian = $this->input->post("id_ujian");
        }
        if ( $id_ujian != null && $id_ujian > 0 )
        {
            $where = array(
                'id_ujian' => $id_ujian
            );
            $ujian = $this->m_ujian->list_by($this->id_guru, $where);
            if ( $ujian != null )
            {
                $ujian = $ujian[0];
                $judul_header = $ujian["judul"];
                $data['ujian_tipe'] = $ujian["tipe"];
                $readonly = ( $ujian["doc_status"] == $this->config->item('doc_status_publish') );
                $data['doc_status'] = $ujian["doc_status"];
            }
        }
        if ( $this->m_ujian->validate() )
        {
            $where = null;
            if ( $ujian != null )
            {
                $where = array(
                    'id_ujian' => $id_ujian
                );
            }
            $param = array(
                'id_guru' => $this->id_guru
            );
            if ( $this->m_ujian->save($param , $where) )
            {
                $this->set_alert(self::ALERT_SUCCESS_SAVE);
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
            $this->to_index();
            return;
        }
        $data['ujian'] = $ujian;
        $data['readonly'] = $readonly;
        $data['id_ujian'] = $id_ujian;
        $data['judul_header'] = $judul_header;
        //$this->set_subtitle( $subtitle ) ;
        $this->set_title('Ujian');
        $this->set_subtitle($data['ujian_tipe']);
        $this->template('elearning/asc_ujian_form', $data , 'panel/elearning/asc_ujian_js');
    }
}