<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_materi_penerima extends Asc_base_controller {

    private $materi;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_asc_materi_penerima', 'm_penerima');
        $this->load->model('m_asc_materi', 'm_materi');
        $id_materi = $this->input->get("id_materi");
        if ( empty($id_materi) || intval($id_materi) <= 0 )
        {
            redirect ( base_url() . 'elearning/asc_materi/' );
            return;
        }
        $param = array(
            'id_materi' => $id_materi,
            'id_guru' => $this->id_guru,
        );
        $this->materi = $this->m_materi->get($param, true);
        if ( $this->materi == false )
        {
            redirect ( base_url() . 'elearning/asc_materi/' );
            return;
        }
        $this->set_title('Penerima Materi');
    }

    public function form()
    {
        $data = array();
        $data['type'] = "kelas"; // bug fix : hilangkan penerima siswa
        //$this->input->get('type');
        $data['id_materi'] = $this->materi['id_materi'];
        $data['judul_header'] = "Siswa";
        if ( $data['type'] != 'siswa' )
        {
            $data['judul_header'] = "Kelas";
        }
        if ( $this->m_penerima->validate() )
        {
            $param = array ( 'id_guru' => $this->id_guru );
            $penerima = $this->input->post('penerima');
            $where = array (
                'type' => $data['type'],
                'id_materi' => $this->materi['id_materi']
            );
            if ( $data['type'] == 'siswa')
            {
                $where['id_siswa'] = $penerima;
            }
            else
            {
                $where['id_kelas'] = $penerima;
            }
            // pastikan penerima belum pernah dimasukan pada materi
            $penerima = $this->m_penerima->get($where, true);
            if ( $penerima == false && $this->m_penerima->save($param , null) )
            {
                $this->set_alert(self::ALERT_SUCCESS_SAVE);
            }
            else
            {
                if ( $penerima != false )
                {
                    $this->set_alert_danger(self::ALERT_FAILED . ' Penerima sudah pernah dimasukan.');
                }
                else
                {
                    $this->set_alert_danger(self::ALERT_FAILED);
                }
            }
        }
        $this->set_subtitle($this->materi['judul']);
        $this->template('elearning/asc_materi_penerima_form', $data , 'panel/elearning/asc_materi_penerima_js');
    }

    public function index()
    {
        $data = array();
        $this->set_subtitle($this->materi['judul']);
        $this->template('elearning/asc_materi_penerima_list', $data , 'panel/elearning/asc_materi_penerima_js');
    }

    public function delete()
    {
        $id_penerima = $this->input->get("id_penerima");
        if ( empty($id_penerima) || intval($id_penerima) <= 0 )
        {
            redirect( base_url() );
            return;
        }
        $this->load->model('m_asc_materi_penerima', 'm_penerima');
        $param = array(
            'id_materi_penerima' => $id_penerima,
            'id_guru' => $this->id_guru,
        );
        $query_string = '';
        $penerima = $this->m_penerima->get($param, true);
        if ( $penerima != false )
        {
            if ( $this->m_penerima->delete($param) )
            {
                $this->set_alert(self::ALERT_SUCCESS_DELETE);
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
            $query_string = "index?id_materi=" . $penerima['id_materi'];
        }
        else
        {
            redirect ( base_url() . 'elearning/asc_materi/' );
        }
        $this->to_index($query_string);
    }
}