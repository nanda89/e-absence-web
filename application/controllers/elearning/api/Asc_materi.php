<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_materi extends Asc_api_controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('m_asc_materi', 'm_materi');
        $this->load->model('M_asc_api_session', 'api_session');
    }

    public function index_get(){
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        $id = false; $res = array();

        if (count($queries) == 1) {
            if (array_key_exists("guru", $queries)) {
                $id = $queries['guru'];
                $this->db->where(array("id_guru" => $id));
                $q = $this->db->get("asc_materi");
                if (count($q->result()) > 0)
                    $res = $q->result();
                $this->send_response($res, "ok", true);
            } else {
                $this->send_response(null, "parameter 'guru' required", false, 500);
            }
        } else if(count($queries) > 1) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        } else {
            $this->send_response(null, "parameter 'guru' required", false, 500);
        }

        ob_end_flush();
    }

    public function siswa_get(){
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        $id = false; $res = array();

        if (count($queries) == 1) {
            if (array_key_exists("id", $queries)) {
                $id = $queries['id'];
                $this->db->where(array("id_materi" => $id));
                $q = $this->db->get("asc_materi");
                if (count($q->result()) > 0)
                    $res = $q->result();
            } else {
                $this->send_response(null, "parameter 'id' required", false, 500);
            }
        } else if(count($queries) > 1) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        } else {
            $q = $this->db->get("asc_materi");
            if (count($q->result()) > 0)
                $res = $q->result();
        }

        $this->send_response($res, "ok", true);

        ob_end_flush();
    }

    public function materi_put() {
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        if (count($queries) > 0) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        } else {
    		$json = $this->security->xss_clean($this->input->raw_input_stream);
            $obj = json_decode($json, true);
            $res = $this->simpan($obj);
            $this->send_response($res, "ok", true, 201);
        }

        ob_end_flush();
    }

    public function index_post() {
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        if (count($queries) > 1 || count($queries) <= 0) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        } else {
            if (!array_key_exists("id", $queries)) {
                $this->send_response(null, "parameter 'id' required", false, 500);
            } else {
        		$json = $this->security->xss_clean($this->input->raw_input_stream);
                $obj = json_decode($json, true);
                $res = $this->simpan($obj, $queries['id']);
                $this->send_response($res, "ok", true, 200);
            }
        }

        ob_end_flush();
    }

    public function index_delete() {
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        if (count($queries) > 2 || count($queries) <= 0) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        }

        if (!array_key_exists('id', $queries) || !array_key_exists('token', $queries)) {
            $this->send_response(null, "parameter 'id' and 'guru' required", false, 500);
        }

        $id = $queries['id'];
        $token = $queries['token'];

        $user = $this->api_session->get(array("token" => $token), true);

        $res = $this->hapus($id, $user["id_user"]);
        if ($res['status'] == 'error') {
            $this->send_response($res['pesan'], $res['status'], false, 403);
        } else {
            $this->send_response($res['pesan'], $res['status'], true);
        }

        ob_end_flush();
    }

    public function param_get(){
        ob_start();

        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        if (count($queries) > 0) {
            $this->send_response(null, "illegal parameter, count: ".count($queries), false, 500);
        }

        $this->db->select("id_mapel as id, mapel as text")
                    ->from('tabel_mapel');
        $query = $this->db->get()->result_array();

        $res = array ("mapel" => $query, "status" => array (self::DOC_STATUS_DRAFT, self::DOC_STATUS_PUBLISH));
        $this->send_response($res, "ok", true);
        ob_end_flush();
    }

    private function hapus($id_materi, $id_guru) {
        $res = array();

        if (empty($id_materi)) {
            $res = array("status"=>"error","pesan"=>"id materi tidak boleh kosong");
            return $res;
        }

        $param = array (
            'id_materi' => $id_materi,
            'id_guru' => $id_guru,
        );

        $materi = $this->m_materi->get($param, true);
        if ($materi == false) {
            $res = array("status"=>"error","pesan"=>"id tidak ditemukan");
            return $res;
        }

        if ($materi['doc_status'] == self::DOC_STATUS_DRAFT && $this->m_materi->delete($param)) {
            $res = array("status"=>"success","pesan"=>"id=".$id_materi." telah dihapus");
        } else {
            if ($materi['doc_status'] == self::DOC_STATUS_PUBLISH) {
                $res = array("status"=>"error","pesan"=>"materi sudah yang sudah terbit tidak dapat dihapus");
            } else {
                $res = array("status"=>"error","pesan"=>self::ALERT_FAILED);
            }
        }
        return $res;
    }

    private function simpan($data, $id = null) {
        $res = array();

        $param = array(
            "id_mapel"   => $data['mapel'],
            "id_guru"    => $data['guru'],
            "judul"      => $data['judul'],
            "materi"     => $data['materi'],
            "doc_status" => $data['status']
        );

        if (array_key_exists('gambar', $data))
            $param["gambar"] = $data['gambar'];

        if (array_key_exists('suara', $data))
            $param["suara"] = $data['suara'];

        if (array_key_exists('file', $data))
            $param["file"] = $data['file'];

        if (array_key_exists('id_materi', $data))
            $id = $data['id_materi'];

        if ($id == null) { // do insert
            $this->db->insert("asc_materi", $param);

            $last_id = $this->db->insert_id();
            $this->db->where(array("id_materi" => $last_id));
            $q = $this->db->get("asc_materi");

            if (count($q->result()) > 0)
                $res = $q->result()[0];
        } else { // do update
            $where = array('id_materi' => $id);
            $this->db->where($where);
            $this->db->update("asc_materi", $param);

            $this->db->reset_query();

            $this->db->where(array("id_materi" => $id));
            $q = $this->db->get("asc_materi");

            if (count($q->result()) > 0)
                $res = $q->result()[0];
        }

        return $res;
    }

}
