<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_session extends Asc_api_controller {

    public function login_post()
    {
        $this->load->model('M_admin', 'm_admin');
        $this->load->model('M_asc_api_session', 'api_session');
        $username = $this->get_param('username');
        $password = $this->get_param('pass');
        $rs_login = $this->m_admin->login($username, $password);
        $data = null;
        $msg = "Akun tidak ditemukan.";
		if ($rs_login->num_rows() == 1)
		{
            $data = $this->api_session->login($rs_login->row());
            $msg = "ok";
        }
        else
        {
            $query = $this->db->from("tabel_siswa")
                                ->where("nisn", $username)
                                ->get();
            if ( $query->num_rows() > 0 )
            {
                $msg = "ok";
                $siswa = $query->row_array();
                $data['nama']           = $siswa['nama_lengkap'];
                $data['username']       = $username;
                $data['password']       = $password;
                $data['avatar']         = '';
                $data['tgl_daftar']     = date('Y-M-d');
                $data['level']          = 'siswa';
                $this->m_admin->tambah($data);
                $rs_login = $this->m_admin->login($username, $password);
                $data = $this->api_session->login($rs_login->row());
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }
    public function ping_post()
    {
        $server_key = $this->get_param('server_key');
        $data = null;
        $msg = "Invalid credential.";
        if ( $server_key == $this->config->item("server_key"))
        {
            $this->load->model('M_admin', 'm_admin');
            $data = $this->m_admin->get_setting()->row_array();
            $msg = "Ok";
        }
        $this->send_response($data, $msg, ($data != null));

    }
}
