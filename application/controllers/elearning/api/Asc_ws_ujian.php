<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asc_ws_ujian extends Asc_api_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_ujian", "m_ujian");
        $this->load->model("m_asc_pertanyaan", "m_pertanyaan");
        $this->load->model("m_asc_pilihan", "m_pilihan");
    }

    /*
    url : { HOST } / elearning / asc_ws_ujian / publish
    method : POST
    request :
    {
      "token" : "861ca219-6a35-523b-93dc-2703d9d226f4",
      "id_ujian" : "12"
    }
    response :
    {
        "server_time": "2018-06-14 06:40:16",
        "response_status": true,
        "message": "Ok",
        "data": {
            "id_ujian": "12",
            "id_guru": "118",
            "id_mapel": "12",
            "kode": "U118128111",
            "tipe": "",
            "kategori": null,
            "judul": "Heboh soal terlalu sulit 111",
            "deskripsi": "Hebooh deskripsi",
            "jumlah_pilihan": "0",
            "jumlah_pertanyaan": "2",
            "doc_status": "publish",
            "created": "2018-06-10 08:59:50",
            "updated": "2018-06-10 14:35:20"
        }
    }
    
    atau

    {
    "server_time": "2018-06-14 06:44:24",
    "response_status": false,
    "message": "Ujian tidak memiliki pertanyaan",
    "data": null
    }
    */

    public function publish_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $id_ujian = $this->get_param("id_ujian");
        $data = null;
        $where = array(
            "id_ujian" => $id_ujian,
            "id_guru" => $user["id_user"]
        );
        $ujian = $this->m_ujian->get($where, true);
        $msg = "Ujian tidak ditemukan";
        if ($ujian != null) {
            if ($ujian["doc_status"] == $this->config->item("doc_status_publish")) {
                $msg = "Ujian sudah pernah dipublish.";
            } elseif ($ujian["doc_status"] == $this->config->item("doc_status_draft")) {
                if ($this->m_ujian->publish($ujian)) {
                    $msg = "Ok";
                    $data = $this->m_ujian->get($where, true);
                } else {
                    $msg = $this->m_ujian->get_error();
                }
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function ujian_remove_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $id_ujian = $this->get_param("id_ujian");
        $where = array(
            "id_ujian" => $id_ujian,
            "id_guru" => $user["id_user"]
        );
        $ujian = $this->m_ujian->get($where, true);
        $msg = "Ujian tidak ditemukan";
        if ($ujian != null) {
            if ($ujian["doc_status"] == $this->config->item("doc_status_publish")) {
                $msg = "Ujian yang sudah dipublish tidak bisa dihapus.";
            } else if ($this->m_ujian->delete($where)) {
                unset($where["id_guru"]);
                $this->m_pertanyaan->delete($where);
                $msg = "Ok";
            }
        }
        $data = null;
        $this->send_response($data, $msg, ($data != null));
    }

    public function pertanyaan_post()
    {
        $user = $this->process_token();
        $id_ujian = $this->get_param("id_ujian");
        $pertanyaan = null;
        $where = array(
            "id_ujian" => $id_ujian
        );
        $ujian = $this->m_ujian->get($where, true);
        $msg = "Ujian tidak ditemukan";
        $data = null;
        if ($ujian != null) {
            if ($user["level"] == $this->config->item("user_level")["siswa"] && $ujian["doc_status"] != $this->config->item("doc_status_publish")) {
                $msg = "Ujian belum dipublish";
            } else {
                $pertanyaan_list = $this->m_pertanyaan->get($where);
                $list = array();
                $i = 1;
                foreach ($pertanyaan_list as $key => $pertanyaan) {
                    $where = array("id_pertanyaan" => $pertanyaan["id_pertanyaan"]);
                    $list[$key] = $pertanyaan;
                    if ($ujian["tipe"] == $this->config->item("ujian_tipe")["p"]) {
                        $list[$key]["pilihan"] = $this->m_pilihan->get($where);
                    } else {
                        $list[$key]["pilihan"] = null;
                    }

                    $i += 1;
                }
                $data = $list;
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function pertanyaan_remove_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $id_ujian = $this->get_param("id_ujian");
        $pertanyaan = $this->get_param("pertanyaan");
        $where = array(
            "id_ujian" => $id_ujian,
            "id_guru" => $user["id_user"]
        );
        $ujian = $this->m_ujian->get($where, true);
        $msg = "Ujian tidak ditemukan";
        $data = null;
        if ($pertanyaan != null && $ujian != null) {
            if ($ujian["doc_status"] == $this->config->item("doc_status_publish")) {
                $msg = "Ujian yang sudah dipublish tidak bisa dihapus.";
            } else {
                $data = array();
                foreach ($pertanyaan as $row) {
                    $where["id_pertanyaan"] = $row["id_pertanyaan"];
                    $is_deleted = $this->m_pertanyaan->delete($where);
                    $data[] = array(
                        "id_pertanyaan" => $row["id_pertanyaan"],
                        "success" => $is_deleted
                    );
                }
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function ujian_post()
    {
        $user = $this->process_token();
        $data = null;
        $msg = "";
        $mapel = $this->get_param("id_mapel");
        $key = $this->get_param("key");
        $tipe = $this->get_param("tipe");
        $doc_status = $this->get_param("doc_status");
        $where = null;
        if ($mapel != null || $tipe != null || $doc_status != null) {
            $where = array();
        }
        if ($mapel != null) {
            $where["id_mapel"] = $mapel;
        }
        if ($tipe != null) {
            $where["tipe"] = $tipe;
        }
        if ($doc_status != null) {
            $where["doc_status"] = $doc_status;
        }
        if ($user["level"] == $this->config->item("user_level")["guru"]) {
            $data = $this->m_ujian->list_by($user["id_user"], $where, $key);
        } elseif ($user["level"] == $this->config->item("user_level")["siswa"]) {
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function ujian_put()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $data = null;
        $msg = "";
        $where = null;
        // simpan data ujian
        $ujian = $this->get_param("ujian");
        $id_ujian = null;
        if ($ujian != null) {
            $param = $ujian;
            $param["id_guru"] = $user["id_user"];
            $data = null;
            $msg = "Terjadi kesalahan.";
            $kode = $ujian["kode"];
            $id_ujian = $ujian["id_ujian"];
            if (true) {
                if ($id_ujian != null) {
                    $where = array("id_ujian" => $id_ujian);
                }
                if ($id_ujian == null) {
                    $kode = "";
                    $this->m_ujian->generate_kode($param["id_guru"], $param["id_mapel"], $param["tipe"]);
                }
                $param["kode"] = $kode;
                if ($this->m_ujian->save($param, $where, true)) {
                    if ($id_ujian == null) {
                        $id_ujian = $this->db->insert_id();
                    }
                    $data["ujian"] = array("id_ujian" => $id_ujian, "kode" => $kode);
                    $msg = "Ujian : Ok";
                }
            } else {
                $msg = validation_errors();
            }
        }
        // simpan pertanyaan
        $pertanyaan = $this->get_param("pertanyaan");
        if ($pertanyaan != null) {
            $id_ujian_pertanyaan = $pertanyaan["id_ujian"];
            if ($id_ujian_pertanyaan == null && $id_ujian == null) {
                $msg = $msg . "id_ujian tidak ada";
            } elseif ($id_ujian_pertanyaan == null && $id_ujian != null) {
                $id_ujian_pertanyaan = $id_ujian;
            }

            if ($id_ujian_pertanyaan == null) {
                $msg = $msg . " Pertanyaan : id_ujian tidak ada";
            } else {
                $where = array(
                    "id_ujian"  => $id_ujian_pertanyaan,
                    "id_guru" => $user["id_user"]
                );
                $ujian = $this->m_ujian->get($where, true);
                if ($ujian == null) {
                    $msg = $msg . " Pertanyaan : Ujian tidak ditemukan";
                } else {
                    $list_pertanyaan = $pertanyaan["list"];
                    $respon_pertanyaan = array();
                    if ($list_pertanyaan != null) {
                        foreach ($list_pertanyaan as $row) {
                            $param = null;
                            $param = $row;
                            $param["id_ujian"] = $id_ujian_pertanyaan;
                            $param["id_guru"] = $user["id_user"];
                            $id_pertanyaan = array_key_exists("id_pertanyaan", $row) ? $row["id_pertanyaan"] : null;
                            $no_urut = array_key_exists("no_urut", $row) ? $row["no_urut"] : null;
                            $where = null;
                            if ($id_pertanyaan != null) {
                                $where = array(
                                    "id_guru" => $user["id_user"],
                                    "id_pertanyaan" => $id_pertanyaan,
                                    "id_ujian" => $id_ujian_pertanyaan
                                );
                            } else {
                                $no_urut = $this->m_pertanyaan->generate_no_urut($ujian);
                            }
                            $param["no_urut"] = $no_urut;
                            $msg_pertanyaan = "Terjadi kesalahan.";
                            $pilihan_list = array_key_exists("pilihan", $row) ? $row["pilihan"] : null;
                            unset($param["pilihan"]);
                            if ($this->m_pertanyaan->save($param, $where, true)) {
                                if ($id_pertanyaan == null) {
                                    $id_pertanyaan = $this->db->insert_id();
                                }
                                $msg_pertanyaan = "Ok";
                            } else {
                                if ($id_pertanyaan == null) {
                                    $id_pertanyaan = -1;
                                }
                            }
                            $pilihan_respon = null;
                            if ($ujian["tipe"] == $this->config->item("ujian_tipe")["p"] && $pilihan_list != null) {
                                $pilihan_respon = array();
                                foreach ($pilihan_list as $pilihan) {
                                    $param = null;
                                    $param = $pilihan;
                                    $param["id_pertanyaan"] = $id_pertanyaan;
                                    $where = null;
                                    $id_pilihan = $pilihan["id_pilihan"];
                                    if ($id_pilihan != null) {
                                        $where = array(
                                            "id_pertanyaan" => $id_pertanyaan,
                                            "id_pilihan" => $id_pilihan
                                        );
                                        unset($pilihan["id_pilihan"]);
                                    }
                                    $respon = array("pilihan" => $pilihan["huruf"]);
                                    if ($this->m_pilihan->save($param, $where, true)) {
                                        $respon["id_pilihan"] = ($id_pilihan == null ? $this->db->insert_id() : $id_pilihan);
                                    } else {
                                        $respon["id_pilihan"] = -1;
                                    }
                                }
                            }
                            $respon_pertanyaan[] = array(
                                "no_urut" => $no_urut,
                                "id_pertanyaan" =>  $id_pertanyaan,
                                "msg" => $msg_pertanyaan,
                                "pilihan" => $pilihan_respon
                            );
                        }
                        $data["pertanyaan"] = $respon_pertanyaan;
                        $msg = $msg . " Pertanyaan : Ok";
                    }
                }
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }
}
