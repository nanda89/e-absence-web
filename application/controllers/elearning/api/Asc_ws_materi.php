<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ws_materi extends Asc_api_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("m_asc_materi_penerima", "m_materi_penerima");
		$this->load->model("m_asc_materi", "m_materi");
	}

    public function materi_put()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $param = array(
            "id_guru" => $user["id_user"],
            "id_mapel" => $this->get_param("id_mapel"),
            "judul" => $this->get_param("judul"),
            "materi" => $this->get_param("materi"),
            "gambar" => $this->get_param("gambar"),
            "suara" => $this->get_param("suara"),
            "file" => $this->get_param("file"),
            "doc_status" => $this->get_param("doc_status"),
        );
        $where = null;
        $id_materi = $this->get_param("id_materi");
        if ( !empty($id_materi) || $id_materi != null )
        {
            $where = array("id_materi" => $id_materi );
        }
        $data = null;
        $msg = "Terjadi kesalahan.";
        if ( $this->m_materi->save($param, $where, true) )
        {
            $data = $this->db->insert_id();
            $msg = "Ok";
        }else
        {
            $msg = $this->m_materi->get_error();
        }
        $this->send_response($data, $msg, ($data != null) );
    }

    public function penerima_remove_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $where = array(
            "id_guru" => $user["id_user"],
            "id_materi" => $this->get_param("id_materi")
        );
        $materi = $this->m_materi->single($where);
        $data = null;
        $msg = "Materi tidak ditemukan.";
        if ( $materi != null )
        {
            $list_penerima = $this->get_param("penerima");
            if ( $list_penerima == null )
            {
                $msg = "Penerima belum dimasukan.";
            }
            else
            {
                $data = array();
                foreach ($list_penerima as $penerima)
                {
                    $where["id_materi_penerima"] = $penerima['id_materi_penerima'];
                    $saved = $this->m_materi_penerima->delete($where);
                    $res =  array(
                        'id_materi_penerima' => $penerima['id_materi_penerima'],
                        'success' => $saved
                    );
                    $data[] = $res;
                }
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, $data != null );
    }

    public function penerima_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $where = array(
            "id_guru" => $user["id_user"],
            "id_materi" => $this->get_param("id_materi")
        );
        $materi = $this->m_materi->single($where);
        $data = null;
        $msg = "Materi tidak ditemukan.";
        if ( $materi != null )
        {
            $where = array(
            "p.id_guru" => $user["id_user"],
            "p.id_materi" => $this->get_param("id_materi")
            );
            $data = $this->m_materi_penerima->list_kelas($where);
            $msg = "Ok";
        }
        $this->send_response($data, $msg, $data != null );
    }

    public function penerima_put()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $where = array(
            "id_guru" => $user["id_user"],
            "id_materi" => $this->get_param("id_materi")
        );
        $materi = $this->m_materi->single($where);
        $data = null;
        $msg = "Materi tidak ditemukan.";
        if ( $materi != null )
        {
            if ( $materi['doc_status'] != $this->config->item("doc_status_publish") )
            {
                $msg = "Materi belum dipublish.";
            }
            else
            {
                $kelas_list = $this->get_param("kelas");
                if ( $kelas_list == null )
                {
                    $msg = "Kelas belum dimasukan.";
                }
                else
                {
                    $data = array();
                    foreach ($kelas_list as $kelas)
                    {
                        $where["type"] = "kelas";
                        $where["id_kelas"] = $kelas['id_kelas'];
                        $saved = $this->m_materi_penerima->save($where, null, true);
                        $penerima =  array(
                            'kelas' => $kelas['id_kelas'],
                            'success' => $saved
                        );
                        $data[] = $penerima;
                    }
                    $msg = "Ok";
                }
            }
        }
        $this->send_response($data, $msg, $data != null );
    }

    public function remove_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $where = array(
            "id_guru" => $user["id_user"],
            "id_materi" => $this->get_param("id_materi")
        );
        $materi = $this->m_materi->single($where);
        $data = null;
        $msg = "Materi tidak ditemukan.";
        if ( $materi != null )
        {   
            if ( $materi['doc_status'] == $this->config->item("doc_status_publish"))
            {
                $msg = "Materi dengan status publish tidak dapat dihapus.";
            }
            else if ( $this->m_materi->delete($where) )
            {
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, $msg == "Ok" );
    }

    public function publish_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $where = array(
            "id_guru" => $user["id_user"],
            "id_materi" => $this->get_param("id_materi")
        );
        $materi = $this->m_materi->single($where);
        $data = null;
        $msg = "Materi tidak ditemukan.";
        if ( $materi != null )
        {
            $materi['doc_status'] = $this->config->item("doc_status_publish");
            unset($materi['id_materi']);
            if ( $this->m_materi->save($materi, $where, true) )
            {
                $msg = "Ok";
            }
            else
            {
                $msg = $this->m_materi->get_error();
            }
        }
        $this->send_response($data, $msg, ($msg == "Ok") );
    }

    public function materi_post()
    {
    	$user = $this->process_token();
    	$data = null;
        $where = null;
        $mapel = $this->get_param("id_mapel");
        $key = $this->get_param("key");
        if ( $mapel != null )
        {
            $where = array("id_mapel" => $mapel );
        }
        if ( $key != null )
        {
            $key = array( "judul" => $key, "materi" => $key);
        }
    	if ($user["level"] == $this->config->item("user_level")["siswa"])
    	{
    		// siswa
	        $data = $this->m_materi_penerima->list_by_siswa($user["id_user"], $where, $key);
    	}
    	elseif ( $user["level"] == $this->config->item("user_level")["guru"] )
    	{
            $where ["id_guru"] = $user["id_user"];
			$data = $this->m_materi->list_by($where);
    	}
        $msg = "Ok";
        $this->send_response($data, $msg, $data != null);
    }
}
