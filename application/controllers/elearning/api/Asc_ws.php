<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ws extends Asc_api_controller {

	/*
		Content-type : multipart/form-data
		request :
		token : login token
		total_file : banyak file yang akan diupload
		file_n : nama file yang akan diupload, n= 1,2,3,...
		file_n_removed : nama file yang akan dihapus (null jika tidak ada), n= 1,2,3,...

		response :
		data : array
		ori : nama file asli
		upload : nama file di server

		contoh : 
		{
		"server_time": "2018-06-02 09:39:38",
		"response_status": true,
		"message": "Ok",
		"data":[
				{
				"ori": "importtype.xlsx",
				"upload": "118-20180602093938_importtype.xlsx"
				},
				{
				"ori": "2.bmp",
				"upload": "118-20180602093938_2.bmp"
				}
			]
		}
	*/
	public function upload_post()
	{
		$user = $this->process_token(false);
		$total_file = $this->input->post("total_file");
		$respon = null;
		$data = array();
		if ( empty($total_file) || $total_file < 1)
		{
			$this->send_response(null, "Jumlah file tidak diketahui.", false);
		}
		$this->load->library('upload');
		for($i=1; $i<=$total_file; $i++)
		{
			$id = "file_" . $i;
			$gambar_res = array();
			$gambar_res["ori"] = $_FILES[$id]['name'];
			$file_name = $this->upload_file($user["id_user"], $id);
			if ( $file_name == null )
			{
				$file_name = $this->upload->display_errors();
			}
			$gambar_res["upload"] = $file_name;
			$removed_file = $this->input->post($id . "_removed");
            if ( !empty($removed_file))
            {
            	$removed_file = $this->config->item('upload_path') . $removed_file;
            	if ( file_exists($removed_file))
            	{
                	unlink($removed_file); //Hapus Gambar
            	}
            }
			$data[] = $gambar_res;
		}
		$this->send_response($data, "Ok", (sizeof($data)>=$total_file));
	}

    public function mapel_post()
    {
        $user = $this->process_token();
        $this->db->select("id_mapel, mapel");
        $this->db->from("tabel_mapel");
        $q = $this->db->get();
        $data = $q->result_array();
        $this->send_response($data, "Ok", $data != null );
    }

    public function kelas_post()
    {
        $user = $this->process_token();
        $this->db->select("id_kelas, kelas");
        $this->db->from("tabel_kelas");
        $q = $this->db->get();
        $data = $q->result_array();
        $this->send_response($data, "Ok", $data != null );
    }

	private function upload_file($id_user, $id)
	{
		$file_name = $_FILES[$id]['name'];
        $config['file_name']		= $id_user ."-" . date("Ymdhis") . "_" . $file_name;
        $config['upload_path'] 		= $this->config->item("upload_path");
        $config['file_ext_tolower']     = true;
        $config['remove_spaces']        = true;
        $config['detect_mime']          = true;

// https://stackoverflow.com/a/28021591
        $config['allowed_types'] 	= 'gif|jpg|png|mp3|audio|mpeg|wav|jpeg|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|cdr|psd';
		$this->upload->initialize($config);
		if( $this->upload->do_upload($id) )
        {
			$file_name = $this->upload->file_name;
            return $file_name;
		}
        return null;
	}
}