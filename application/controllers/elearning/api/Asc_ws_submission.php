<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ws_submission extends Asc_api_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_submission", "m_sub");
        $this->load->model("m_asc_jadwal_ujian", "m_jadwal");
        $this->load->model("m_asc_peserta_ujian", "m_peserta");
        $this->load->model("m_asc_pilihan", "m_pilihan");
    }

    public function nilai_peserta_post()
    {
        $user = $this->process_token($this->config->item("user_level")["siswa"]);
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $where = array(
            "p.id_siswa" => $user["id_user"]
        );
        $key = $this->get_param("key");
        $mapel = $this->get_param("id_mapel");
        if ( $mapel != null )
        {
            $where["u.id_mapel"] = $mapel;
        }
        $data = $this->m_sub->list_nilai($where, $key);
        $msg = "Ok";
        $this->send_response($data, $msg, ($data != null));
    }

    /*
    url : { BASE_URL } / elearning / api / asc_ws_submission/koreksi
    method : PUT
    request :
    {
      "token" : "861ca219-6a35-523b-93dc-2703d9d226f4",
      "id_peserta_ujian" : "5",
      "submission" : [
        {
          "id_submission" : "44",
          "nilai" : "32",
          "koreksi" : "belajar lagi ya"
        },
        {
          "id_submission" : "45",
          "nilai" : "52",
          "koreksi" : "pinter bgt"
        }  
      ]
    }
    response :
    {
        "server_time": "2018-06-26 12:18:07",
        "response_status": false,
        "message": "Ok",
        "data": null
    }
    */
    public function koreksi_put()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $data = null;
        $msg = "Peserta tidak ditemukan";
        $id_peserta_ujian = $this->get_param("id_peserta_ujian");
        $submission = $this->get_param("submission");
        $where = array("id_peserta_ujian" => $id_peserta_ujian);
        $peserta = $this->m_peserta->get($where, true);
        if ( $submission != null && $peserta != null )
        {
            foreach ($submission as $row)
            {
                $id_submission = $row["id_submission"];
                $nilai = $row["nilai"];
                $koreksi = $row["koreksi"];
                $param = array(
                    'nilai' => $nilai,
                    'koreksi' => $koreksi,
                    'doc_status' => $this->config->item('doc_status_koreksi')
                );
                $where = array(
                    'id_submission' => $id_submission
                );
                $this->m_sub->save($param, $where, true);
            }
            $param = array(
                'doc_status' => $this->config->item('doc_status_koreksi')
            );
            $where = array(
                'id_peserta_ujian' => $peserta['id_peserta_ujian']
            );
            $this->m_peserta->save($param, $where, true);
            $msg = "Ok";
        }
        $this->send_response($data, $msg, ($msg == "Ok"));
    }

    public function submit_peserta_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $id_jadwal = $this->get_param("id_jadwal_ujian");
        $where = array("id_jadwal_ujian" => $id_jadwal);
        $jadwal = $this->m_jadwal->get($where, true);
        if ( $jadwal != null )
        {
            $where = array(
                "p.id_jadwal_ujian" => $id_jadwal
            );
            $key = $this->get_param("key");
            $data = $this->m_peserta->list_by($where, $key);
            $msg = "Ok";
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function submit_post()
    {
        $user = $this->process_token();
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $id_jadwal = $this->get_param("id_jadwal_ujian");
        $where = array("id_jadwal_ujian" => $id_jadwal);
        $jadwal = $this->m_jadwal->get($where, true);
        if ( $jadwal != null )
        {
            $peserta = null;
            if( $user["level"] == $this->config->item("user_level")["guru"] )
            {
                $where = array(
                    "id_peserta_ujian" => $this->get_param("id_peserta_ujian")
                );
                $peserta = $this->m_peserta->get($where, true);
            }
            else
            {
                $where = array(
                    "id_jadwal_ujian" => $id_jadwal,
                    "id_siswa" => $user["id_user"]
                );
                $peserta = $this->m_peserta->get($where, true);
            }
            if ( $peserta == null )
            {
                $msg = "Siswa tidak termasuk peserta ujian.";
            }
            else
            {
                $where = array(
                    "s.id_peserta_ujian" => $peserta["id_peserta_ujian"]
                );
                if( $user["level"] == $this->config->item("user_level")["siswa"] )
                {
                    //$where["s.doc_status"] = $this->config->item("doc_status_publish");
                }
                $list_sub = $this->m_sub->list_by($where, null);
                $data = array();
                foreach ($list_sub as $sub)
                {
                    $param = $sub;
                    $param["pilihan"] = null;
                    if ( $sub["tipe"] == $this->config->item("ujian_tipe")["p"] )
                    {
                        $where = array("id_pertanyaan" => $sub["id_pertanyaan"]);
                        $param["pilihan"] = $this->m_pilihan->get($where);
                    }
                    $data[] = $param;
                }
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }
    /*
        URL : { BASE_URL } / elearning / api / asc_ws_submission / publish
        method : POST
        request :
        {
          "token" : "861ca219-6a35-523b-93dc-2703d9d226f4",
          "id_jadwal_ujian" : "5"
        }
        response :
        {
            "server_time": "2018-06-19 09:19:23",
            "response_status": false,
            "message": "Ok",
            "data": null
        }

    */
    public function publish_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $id_jadwal = $this->get_param("id_jadwal_ujian");
        $where = array(
            "t.id_jadwal_ujian" => $id_jadwal,
            "t.id_guru" => $user['id_user']
        );
        $jadwal = $this->m_jadwal->single($where);
        if ( $jadwal != null )
        {
            if ( $jadwal['doc_status'] != $this->config->item("doc_status_publish") )
            {
                $msg = "Submission tidak bisa diproses.";
            }
            elseif ( $this->m_sub->publish($jadwal) )
            {
                $msg = "Ok";
            }
            else
            {
                $msg = $this->m_sub->get_error();
            }
        }
        $this->send_response($data, $msg, ($msg == "Ok"));
    }
    /*
    [SISWA]
    url : {BASE_URL } / elearning / api / asc_ws_submission / join
    method : post
    request :
    {
      "token" : "0583ff95-6206-5df2-94a8-4c1764227814",
      "id_peserta_ujian" : "1"
    }
    response :
    {
        "server_time": "2018-06-13 05:31:57",
        "response_status": true,
        "message": "Ok",
        "data": {
            "id_peserta_ujian": "1",
            "waktu_join": "2018-06-13 05:31:57"
        }
    }
    */
    public function join_post()
    {
        $user = $this->process_token($this->config->item("user_level")["siswa"]);
        $data = null;
        $msg = "Peserta tidak ditemukan";
        $id_peserta_ujian = $this->get_param("id_peserta_ujian");
        $where = array(
            'id_peserta_ujian' => $id_peserta_ujian,
            'id_siswa' => $user['id_user']
        );
        $peserta = $this->m_peserta->get($where, true);
        if ( $peserta != null )
        {
            if ( $peserta['doc_status'] == $this->config->item('doc_status_draft') )
            {
                $date_join = date('Y-m-d H:i:s');
                $param = array(
                    'waktu_join' => $date_join,
                    'doc_status' => $this->config->item('doc_status_join')
                );
                $this->m_peserta->save($param, $where, true);
                $data = array(
                    'id_peserta_ujian' => $id_peserta_ujian,
                    'waktu_join' => $date_join
                );
                $msg= "Ok";
            }
            else
            {
                $msg = "Peserta sudah memulai ujian";
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }

    public function submit_put()
    {
        $user = $this->process_token();
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $id_jadwal = $this->get_param("id_jadwal_ujian");
        $where = array("id_jadwal_ujian" => $id_jadwal);
        $ujian = $this->m_jadwal->get($where, true);
        if ( $ujian != null )
        {
            if ( $user["level"] == $this->config->item("user_level")["siswa"])
            {
                $where = array(
                    "id_jadwal_ujian" => $id_jadwal,
                    "id_siswa" => $user["id_user"]
                );
                $peserta = $this->m_peserta->get($where, true);
                if ( $peserta != null && $peserta['doc_status'] == $this->config->item("doc_status_join") )
                {
                    $id_peserta = $peserta["id_peserta_ujian"];
                    $jawaban_list = $this->get_param("jawaban");
                    foreach ($jawaban_list as $jawaban)
                    {
                        $param = $jawaban;
                        $param["id_peserta_ujian"] = $id_peserta;
                        if ( $this->m_sub->save($param, null ,true) )
                        {
                            $data[] = array(
                                "id_pertanyaan" => $jawaban["id_pertanyaan"],
                                "id_submission" => $this->db->insert_id()
                            );
                        }
                    }
                    $date_submit = date('Y-m-d H:i:s');
                    $param = array(
                        "doc_status" => $this->config->item("doc_status_submit"),
                        'waktu_submit' => $date_submit
                    );
                    $where = array("id_peserta_ujian" => $id_peserta);
                    $this->m_peserta->save($param, $where, true);
                    $msg= "Ok";
                }
                elseif ( $peserta != null && $peserta['doc_status'] != $this->config->item("doc_status_draft") )
                {
                    $msg = "Peserta sudah pernah mengirim jawaban";
                }
                else
                {
                    $msg = "Peserta tidak ditemukan";
                }
            }
            else
            {
                $jawaban_list = $this->get_param("jawaban");
                foreach ($jawaban_list as $jawaban)
                {
                    $param = $jawaban;
                    $where = array("id_submission" => $param["id_submission"]);
                    unset($param["id_submission"]);
                    $param["doc_status"] = "done";
                    if ( $this->m_sub->save($param, $where ,true) )
                    {
                        $data[] = array(
                            "id_submission" => $where["id_submission"],
                            "doc_status" => "done"
                        );
                    }
                }
                $msg= "Ok";
            }
        }
        $this->send_response($data, $msg, ($data != null));
    }
}