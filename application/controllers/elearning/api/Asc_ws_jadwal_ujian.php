<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_ws_jadwal_ujian extends Asc_api_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_ujian", "m_ujian");
        $this->load->model("m_asc_jadwal_ujian", "m_jadwal");
        $this->load->model("m_asc_peserta_ujian", "m_peserta");
    }

    public function jadwal_ujian_remove_post()
    {
        $user = $this->process_token();
        if ( $user == null )
            return;

        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $id_jadwal_ujian = $this->get_param("id_jadwal_ujian");
        $where = array(
            "id_guru" => $user["id_user"],
            "id_jadwal_ujian" => $id_jadwal_ujian
        );
        $jadwal = $this->m_jadwal->get($where, true);
        if ( $jadwal != null )
        {
            if ( $this->m_jadwal->delete_all_ujian($where) )
            {
                $msg = "Ok";
            }
        }
        $this->send_response($data, $msg, ($msg == "Ok"));
    }

    public function jadwal_ujian_post()
    {
        $user = $this->process_token();
        $data = null;
        $msg = "";
        $mapel = $this->get_param("id_mapel");
        $doc_status = $this->get_param("doc_status");
        $key = $this->get_param("key");
        if ( $user["level"] == $this->config->item("user_level")["siswa"] )
        {
            $where = array("p.id_siswa" => $user["id_user"]);
            if($mapel != null )
            {
                $where["u.id_mapel"] = $mapel;
            }
            $data = $this->m_jadwal->list_by($where, $key);
        }
        elseif  ( $user["level"] == $this->config->item("user_level")["guru"] )
        {
            $where = array("u.id_guru" => $user["id_user"]);
            if($mapel != null )
            {
                $where["u.id_mapel"] = $mapel;
            }
            if ( $doc_status != null )
            {
                $where["j.doc_status<>"] = $doc_status;
            }
            $data = $this->m_jadwal->guru($where, $key);
        }
        $this->send_response($data, $msg, ($data != null ));
    }

    public function publish_post()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $id_jadwal_ujian = $this->get_param("id_jadwal_ujian");
        $data = null;
        $msg = "Jadwal tidak ditemukan";
        $where = array(
            "id_guru" => $user["id_user"],
            "id_jadwal_ujian" => $id_jadwal_ujian,
        );
        $jadwal_ujian = $this->m_jadwal->get($where, true);
        if ( $jadwal_ujian != null )
        {
            if ( $jadwal_ujian["doc_status"] == $this->config->item("doc_status_publish"))
            {
                $msg = "Jadwal sudah pernah dipublish.";
            }
            else
            {
                $param["doc_status"] = $this->config->item("doc_status_publish");
                if ( $this->m_jadwal->save($param, $where, true))
                {
                    $data = $this->m_peserta->generate_peserta($jadwal_ujian);
                    $msg = "Ok";
                }
                if ( $data == null )
                {
                    $msg = "Terjadi kesalahan. Jadwal gagal dipublish.";
                    $param["doc_status"] = $this->config->item("doc_status_draft");
                    $this->m_jadwal->save($param, $where, true);
                }
            }
        }
        $this->send_response($data, $msg, ($data != null ));
    }
    public function jadwal_ujian_put()
    {
        $user = $this->process_token($this->config->item("user_level")["guru"]);
        $data = null;
        $msg = "Ujian tidak ditemukan";
        $jadwal_ujian = $this->get_param("jadwal_ujian");
        $id_ujian = $jadwal_ujian["id_ujian"];
        $param = array(
            "id_ujian" => $id_ujian,
            "id_guru" => $user["id_user"]
        );
        $ujian = $this->m_ujian->get($param, true);
        if ( $ujian != null && $ujian["doc_status"] != $this->config->item("doc_status_publish") )
        {
            $msg = "Ujian belum dipublish.";
        }
        elseif ( $ujian != null && $ujian["doc_status"] == $this->config->item("doc_status_publish") )
        {
            $jadwal_ujian["id_guru"] = $user["id_user"];
            $jadwal_ujian["countdown"] = $this->config->item("ujian_countdown");
            $where = null;
            $id_jadwal_ujian = $jadwal_ujian["id_jadwal_ujian"];
            $jadwal = null;
            if ( $id_jadwal_ujian != null )
            {
                $jadwal = $this->m_jadwal->get(array("id_jadwal_ujian" => $id_jadwal_ujian), true);
            }else
            {
                unset($jadwal_ujian["id_jadwal_ujian"]);
            }
            if ( $id_jadwal_ujian != null && $jadwal == null )
            {
                $msg = "Jadwal ujian tidak ditemukan";
            }
            else
            {
                $where = null;
                if ( $id_jadwal_ujian != null )
                {
                    $where = array("id_jadwal_ujian" => $id_jadwal_ujian);
                }
                if ( $this->m_jadwal->save($jadwal_ujian, $where, true) )
                {
                    if ( $id_jadwal_ujian == null )
                    {
                        $id_jadwal_ujian = $this->db->insert_id();
                    }
                    $data = $id_jadwal_ujian;
                    $msg = "Jadwal Ok";
                }
                else
                {
                    $msg ="Terjadi kesalahan.";
                }
            }
        }
        $this->send_response($data, $msg, ($data != null ));
    }
}
