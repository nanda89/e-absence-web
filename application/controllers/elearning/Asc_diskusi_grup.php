<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_diskusi_grup extends Asc_base_controller {

    public function index()
    {
        $data = array();
        $this->set_title('Diskusi Grup');
        $this->set_subtitle('List');
        $this->template('elearning/asc_ujian');
    }
}