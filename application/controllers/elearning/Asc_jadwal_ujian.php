<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_jadwal_ujian extends Asc_base_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_asc_ujian", "m_ujian");
        $this->load->model("m_asc_jadwal_ujian", "m_jadwal");
        $this->load->model("m_asc_peserta_ujian", "m_peserta");
        $this->load->model("m_asc_submission", "m_sub");
    }

    public function publish($id_jadwal_ujian=null)
    {
        if ( empty($id_jadwal_ujian) || $id_jadwal_ujian == null )
        {
            $this->to_index();
            return;
        }
        $where = array(
            'id_jadwal_ujian' => $id_jadwal_ujian,
            'id_guru' => $this->id_guru
        );
        $jadwal_ujian = $this->m_jadwal->get($where, true);
        if ( $jadwal_ujian != null )
        {
            if ( $jadwal_ujian['doc_status'] == self::DOC_STATUS_DRAFT )
            {
                $param["doc_status"] = $this->config->item("doc_status_publish");
                if ( $this->m_jadwal->save($param, $where, true))
                {
                    $data = $this->m_peserta->generate_peserta($jadwal_ujian);
                    $msg = "Ok";
                }
                if ( $data == null )
                {
                    $msg = "Terjadi kesalahan. Jadwal gagal dipublish.";
                    $param["doc_status"] = $this->config->item("doc_status_draft");
                    $this->m_jadwal->save($param, $where, true);
                    $this->set_alert($msg);
                }
                else
                {
                    $this->set_alert(self::ALERT_SUCCESS_PUBLISH);
                }
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
        }
        $this->to_index();
    }

    public function delete($id_jadwal_ujian=null)
    {
        if ( empty($id_jadwal_ujian) || $id_jadwal_ujian == null )
        {
            $this->to_index();
            return;
        }
        $where = array(
            'id_jadwal_ujian' => $id_jadwal_ujian,
            'id_guru' => $this->id_guru
        );
        $jadwal_ujian = $this->m_jadwal->get($where, true);
        if ( $jadwal_ujian != null )
        {
            if ( $this->m_jadwal->delete_all_ujian($where) )
            {
                $this->set_alert(self::ALERT_SUCCESS_DELETE);
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
        }
        $this->to_index();
    }

    public function index()
    {
        $data = array();
        $this->set_title('Jadwal Ujian');
        $this->set_subtitle('List');
        $this->template('elearning/asc_jadwal_ujian_list', $data , 'panel/elearning/asc_jadwal_ujian_js');
    }
    public function form($id_jadwal_ujian = null)
    {
        $data = array();
        $readonly = false;
        $doc_status = $this->config->item('doc_status_draft');
        if ( $id_jadwal_ujian == null )
        {
            $id_jadwal_ujian = $this->input->post('id_jadwal_ujian');
        }
        $jadwal_ujian = null;
        if ( $id_jadwal_ujian != null )
        {
            $where = array(
                't.id_guru' => $this->id_guru,
                't.id_jadwal_ujian' => $id_jadwal_ujian
            );
            $jadwal_ujian = $this->m_jadwal->single($where);
        }
        if ( $this->m_jadwal->validate() )
        {
            $tanggal = $this->input->post('tanggal');
            $jam = $this->input->post('jam');
            $durasi = $this->input->post('durasi');
            $waktu_mulai = $tanggal . ' ' . $jam;
            $date_mulai = new DateTime($waktu_mulai);
            $date_selesai = new DateTime($waktu_mulai);
            $date_selesai->modify('+' . $durasi . ' minutes');
            $param = array(
                    "id_ujian" => $this->input->post('id_ujian'),
                    "waktu_mulai" => $date_mulai->format('Y-m-d H:i:s'),
                    "waktu_selesai" => $date_selesai->format('Y-m-d H:i:s'),
                    "tampilkan_jawaban" => $this->input->post('tampilkan_jawaban'),
                    "id_kelas" => $this->input->post('id_kelas'),
                    "countdown" => $this->config->item("ujian_countdown"),
                    "is_manual_stop" => 'N',
                    "durasi" => $durasi,
                    "doc_status" => $this->input->post('status')
            );
            
            $where = null;
            if ( $jadwal_ujian != null )
            {
                $where = array(
                    'id_guru' => $this->id_guru,
                    'id_jadwal_ujian' => $id_jadwal_ujian
                );
            }
            else{
                $param['id_guru'] = $this->id_guru;
            }
            if ( $this->m_jadwal->save($param , $where, true) )
            {
                $this->set_alert(self::ALERT_SUCCESS_SAVE);
            }
            else
            {
                $this->set_alert_danger(self::ALERT_FAILED);
            }
            $this->to_index();
            return;
        }
        $judul_header = 'Jadwal Ujian';
        $data['jadwal_ujian'] = $jadwal_ujian;
        $data['readonly'] = $readonly;
        $data['id_jadwal_ujian'] = $id_jadwal_ujian;
        $data['judul_header'] = $judul_header;
        $data['doc_status'] = $doc_status;
        $this->set_title($judul_header);
        $this->set_subtitle('Baru');
        $this->template('elearning/asc_jadwal_ujian_form', $data , 'panel/elearning/asc_jadwal_ujian_js');
    }
}
