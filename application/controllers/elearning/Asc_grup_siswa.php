<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asc_grup_siswa extends Asc_base_controller {

    public function index()
    {
        $data = array();
        $this->set_title('Grup Siswa');
        $this->set_subtitle('List');
        $this->template('elearning/asc_grup_siswa_list', null , 'panel/elearning/asc_grup_siswa_js');
    }
}