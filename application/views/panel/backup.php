<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>

  <section class="content">
    <?php                       
        if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
        ?>                  
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>  
            </button>
        </div>
        <?php
        }
            $_SESSION['pesan'] = '';                        
                
        ?>
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Backup</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="panel/set_profil" method="post" enctype="multipart/form-data">
              <div class="box-body">
                Catatan : Pastikan anda menyimpan hasil backup database ini dengan benar.
                <div class="form-group">                  
                  <div class="col-sm-10">
                    <a href="backup/back">
                      <button type="button" class="btn btn-md bg-maroon" name="nama"><i class="fa fa-database"></i> Backup Database</button>
                    </a>
                  </div>
                </div>
                
              </div><!-- /.box-body -->              
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Restore</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="backup/restoredb" method="post" enctype="multipart/form-data">
              <div class="box-body">
                Catatan : Pastikan sebelum melakukan restore, anda sudah membuat database baru di dalam DBMS.
                <div class="form-group">                  
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="inputEmail3" name="db_file">
                    </div>                                    
                    <button type="submit" class="col-sm-4 btn btn-md bg-blue" name="restore"><i class="fa fa-folder-open"></i> Restore Database</button>

                </div>
                
              </div><!-- /.box-body -->              
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->
  </section>
</div>