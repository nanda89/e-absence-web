<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">          
          <div class="box-header with-border">                                    
            <br>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->          
          <div class="box-body with-border">
            <h4>Pilih Jenjang terlebih dulu untuk memulai proses kelulusan atau pendataan alumni.</h4> <br>
            <form class="form-horizontal" action="adm/kelulusan/set_kelas" method="get" enctype="multipart/form-data">          
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-4">
                  <select name="id_jenjang" required id="id_jenjang" class="form-control">                    
                    <option value=''>Pilih Jenjang</option>                    
                    <?php 
                    foreach($dt_jenjang->result() as $row) {                           
                    echo "
                    <option value='$row->id_jenjang'>$row->jenjang</option>";
                    } ?>
                  </select>
                </div>                           
                <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                <div class="col-sm-4">
                  <select name="id_ta" required id="id_tahun" class="form-control">                    
                    <option value=''>Pilih Tahun Ajaran</option>                    
                    <?php 
                    foreach($dt_ta->result() as $row) {                           
                    echo "
                    <option value='$row->id_ta'>$row->tahun_ajaran</option>";
                    } ?>
                  </select>
                </div>  
              </div>

              <div class="form-group">
                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Tingkat</label>
                <div class="col-sm-4">
                  <select name="id_tingkat" required id="tingkat" class="form-control">                    
                    <option value=''>Pilih Tingkat</option>                                       
                  </select>
                </div>                            -->
                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-4">
                  <select name="id_kelas" required id="id_kelas" class="form-control">                    
                    <option value=''>Pilih Kelas</option>                                       
                  </select>
                </div>  
              </div>
                          
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="submit" class="btn bg-maroon btn-flat margin">Mulai Proses Data Kelulusan <i class="fa fa-chevron-right"></i></button>              
                </div>             
              </div>  
            </form>  
          </div>
          <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>No.Induk</th>
              <th>Nama Siswa</th>              
              <th>Angkatan</th>
              <th>Tgl.Lulus</th>              
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1;
          $level = $this->session->userdata('level');
          $id_cabang = $this->session->userdata('id_cabang');
          if ($level=='operator cabang') {
            $dt_kelulusan = $this->db->query("SELECT * FROM tabel_kelulusan INNER JOIN tabel_siswa ON
              tabel_kelulusan.id_siswa=tabel_siswa.id_siswa WHERE tabel_siswa.id_cabang='$id_cabang' ORDER BY angkatan ASC");
          }else{
            $dt_kelulusan = $this->db->query("SELECT * FROM tabel_kelulusan INNER JOIN tabel_siswa ON
              tabel_kelulusan.id_siswa=tabel_siswa.id_siswa ORDER BY angkatan ASC");  
          }
          
          foreach($dt_kelulusan->result() as $row) {                 
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->id_siswa</td>      
              <td>$row->nama_lengkap</td>      
              <td>$row->angkatan</td>      
              <td>$row->tgl_lulus</td>            
            </tr>";  
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->          
        </div><!-- /.box -->        
      </div>     

    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  var tingkat = $("#tingkat").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang+"&tingkat="+tingkat,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>