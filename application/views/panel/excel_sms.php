<?php 
$d = date('dms'); 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_sms_".$judul."-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table id="example2" border="1" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="1%">No</th>
      <th width="5%">No.Induk</th>                        
      <th width="15%">Nama Lengkap</th>                    
      <th width="15%">No.HP</th>   
      <th>Pesan</th>                              
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1;
  $id_jenjang   = $this->input->get('id_jenjang');
  $id_kelas     = $this->input->get('id_kelas');
  $tgl_awal     = $this->input->get('tgl_awal');
  $tgl_akhir    = $this->input->get('tgl_akhir');
  $status       = $this->input->get('status');      
  $id           = $this->input->get('id');               
  foreach($dt_wali_kelas->result() as $row) {                           
    $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();              
    if($id == 2){
      $find     = array("nama_i","nis_i","tgl_i","jam_i");
      $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_pulang);
      $isi      = str_replace($find, $replace, $r->isi);
    }elseif($id == 1){
      $find     = array("nama_i","nis_i","tgl_i","jam_i");
      $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_pulang);
      $isi      = str_replace($find, $replace, $r->isi);
    }elseif($id == 3){
      $hadir    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                  AND absen_masuk = 'hadir' AND valid_masuk = 'valid'")->row();
      $alpa     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                  AND absen_masuk = 'alpha' AND valid_masuk = 'valid'")->row();
      $izin     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                  AND absen_masuk = 'izin' AND valid_masuk = 'valid'")->row();
      $sakit    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                  WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                  AND absen_masuk = 'sakit' AND valid_masuk = 'valid'")->row();
      $find     = array("nama_i","nis_i","tgl1_i","tgl2_i","hadir_i","alpa_i","izin_i","sakit_i");
      $replace  = array($row->nama_lengkap,$row->id_siswa,$tgl_awal,$tgl_akhir,$hadir->jum,$alpa->jum,$izin->jum,$sakit->jum);
      $isi      = str_replace($find, $replace, $r->isi);
    }
    $jum_c      = strlen($isi);

    echo "          
      <tr>
        <td>$no</td>
        <td>$row->id_siswa</td>      
        <td>$row->nama_lengkap</td>                     
        <td>$row->no_hp</td>
        <td>$isi</td>                                                  
        "; ?>                                 
      </tr>
    <?php
    $no++;
  }
  ?>
  </tbody>
</table>
