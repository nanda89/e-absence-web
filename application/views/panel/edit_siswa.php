<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<body>
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "edit") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      <?php
$row = $one_siswa->row();
    ?>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/siswa/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_siswa ?>" name="id">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl Daftar</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->tgl_daftar ?>"  placeholder="Tanggal Daftar" name="tgl_daftar" id="tanggal">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                  <div class="col-sm-4">
                    <select name="id_jenjang" id="jen" required class="form-control">
                      <option value="<?php echo $row->id_jenjang ?>">
                        <?php
$dt_cust = $this->m_admin->getByID("tabel_jenjang", "id_jenjang", $row->id_jenjang)->row();
    if (isset($dt_cust)) {
        echo $dt_cust->jenjang;
    } else {
        echo "Pilih";
    }
    ?>
                      </option>
                      <?php
$dt_jenjang = $this->m_admin->kondisiCond("tabel_jenjang", "id_jenjang != " . $row->id_jenjang);
    foreach ($dt_jenjang->result() as $row2) {
        echo "
                      <option value='$row2->id_jenjang'>$row2->jenjang</option>";
    }?>
                    </select>
                  </div>
                </div>
                <?php
$c = $this->db->query("SELECT * FROM tabel_siswakelas INNER JOIN tabel_tahunajaran ON tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta WHERE id_siswa = '$row->id_siswa'")->row();
    ?>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                  <div class="col-sm-4">
                    <select name="id_ta" id="id_ta" required class="form-control">
                      <option value="<?php echo $c->id_ta ?>">
                        <?php
$dt_cust = $this->m_admin->getByID("tabel_tahunajaran", "id_ta", $c->id_ta)->row();
    if (isset($dt_cust)) {
        echo $dt_cust->tahun_ajaran;
    } else {
        echo "Pilih";
    }
    ?>
                      </option>
                      <?php
$dt_ta = $this->m_admin->kondisiCond("tabel_tahunajaran", "id_ta != " . $c->id_ta);
    foreach ($dt_ta->result() as $ro) {
        echo "
                      <option value='$ro->id_ta'>$ro->tahun_ajaran</option>";
    }?>
                    </select>
                  </div>
                  <?php
$d = $this->db->query("SELECT * FROM tabel_siswakelas INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas WHERE id_siswa = '$row->id_siswa'")->row();
    ?>
                  <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                  <div class="col-sm-4">
                    <select name="id_kelas" id="kelas" required class="form-control" >
                      <option value="<?php echo $d->id_kelas ?>">
                        <?php
$dt_cust = $this->m_admin->getByID("tabel_kelas", "id_kelas", $c->id_kelas)->row();
    if (isset($dt_cust)) {
        echo $dt_cust->kelas;
    }
    ?>
                      </option>
<?php

    $kelas = $this->m_jenjang->getKelasByJenjangAndTa($row->id_jenjang, $c->id_ta);
    foreach ($kelas->result() as $rowkelas) {
        echo "<option value='$rowkelas->id_kelas'>$rowkelas->kelas</option>";
    }

    ?>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Induk</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->id_siswa ?>" required placeholder="No.Induk" name="id_siswa" id="kode_siswa">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">NISN</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->nisn ?>" id="inputEmail3" placeholder="Nomor Induk Siswa Nasional" name="nisn">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->nama_lengkap ?>" id="inputEmail3" required placeholder="Nama Lengkap" id="nama_s" name="nama_lengkap">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <?php
$j = $row->jenis_kelamin;
    if (strtolower($j) == 'perempuan') {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red"> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red" checked> Perempuan
                    </label>
                    <?php
} elseif (strpos(strtolower($j), 'laki') !== false) {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                    <?php
} else {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red"> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                    <?php
}
    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tempat_lahir ?>" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tgl_lahir ?>" class="form-control pull-right" id="tanggal2" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="<?php echo $row->agama ?>"><?php echo $row->agama ?></option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.HP</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->no_hp ?>" class="form-control pull-right" name="no_hp" placeholder="No Handphone">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $row->alamat ?>" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="email" value="<?php echo $row->email ?>" class="form-control"  id="inputEmail3" placeholder="Alamat Email Orang tua/wali yang aktif" name="email">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Asal Sekolah</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->asal_sekolah ?>" class="form-control" id="inputEmail3" placeholder="Asal Sekolah" name="asal_sekolah">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Ket Asal Sekolah</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->ket_asal ?>" class="form-control" id="inputEmail3" placeholder="Prestasi,jurusan,status,dll" name="ket_asal">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Ayah</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->nama_ayah ?>" class="form-control" id="inputEmail3" placeholder="Nama Ayah" name="nama_ayah">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Ibu</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->nama_ibu ?>" class="form-control" id="inputEmail3" placeholder="Nama Ibu" name="nama_ibu">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-4">
                    <input type="file" class="form-control" id="inputEmail3" name="gambar">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" onclick="return confirm('Pastikan data sudah lengkap, lanjutkan?')" value="edit" class="btn btn-info">Update All</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>


<script type="text/javascript">
function pilih_butuh(){
  var butuh = $("#butuh").val();
  if(butuh == 'ya'){
    $("#butuh_text").show();
    $("#butuh_text").focus();
  }else{
    $("#butuh_text").hide();
  }
}
function pilih_butuh_ay(){
  var butuh = $("#butuh_ay").val();
  if(butuh == 'ya'){
    $("#butuh_text_ay").show();
    $("#butuh_text_ay").focus();
  }else{
    $("#butuh_text_ay").hide();
  }
}
function pilih_butuh_ib(){
  var butuh = $("#butuh_ib").val();
  if(butuh == 'ya'){
    $("#butuh_text_ib").show();
    $("#butuh_text_ib").focus();
  }else{
    $("#butuh_text_ib").hide();
  }
}
function pilih_butuh_wl(){
  var butuh = $("#butuh_wl").val();
  if(butuh == 'ya'){
    $("#butuh_text_wl").show();
    $("#butuh_text_wl").focus();
  }else{
    $("#butuh_text_wl").hide();
  }
}
function  pilih(){
  var butuh_text = $("#butuh_text").val();
  if(butuh_text==''){
    $("#butuh_text").hide();
    $("#butuh").val('Tidak');
  }else{
    $("#butuh_text").show();
    $("#butuh").val('ya');
  }

  var butuh_text = $("#butuh_text_ay").val();
  if(butuh_text==''){
    $("#butuh_text_ay").hide();
    $("#butuh_ay").val('Tidak');
  }else{
    $("#butuh_text_ay").show();
    $("#butuh_ay").val('ya');
  }
  var butuh_text = $("#butuh_text_ib").val();
  if(butuh_text==''){
    $("#butuh_text_ib").hide();
    $("#butuh_ib").val('Tidak');
  }else{
    $("#butuh_text_ib").show();
    $("#butuh_ib").val('ya');
  }
  var butuh_text = $("#butuh_text_wl").val();
  if(butuh_text==''){
    $("#butuh_text_wl").hide();
    $("#butuh_wl").val('Tidak');
  }else{
    $("#butuh_text_wl").show();
    $("#butuh_wl").val('ya');
  }
}
window.onload=cek_jenjang();
function cek_jenjang(){
  var jenjang_js=document.getElem;entById("jen").value;
  if(jenjang_js=="1"){
    $("#no_seri_ijazah").hide();
    $("#no_ujian_nasional").hide();
    $("#no_seri_skhun").hide();
    $("#bank").show();
    $("#wn").show();
  }else if(jenjang_js=="2" || jenjang_js=="3" || jenjang_js=="4"){
    $("#no_seri_ijazah").show();
    $("#no_ujian_nasional").show();
    $("#no_seri_skhun").show();
    $("#bank").hide();
    $("#wn").hide();
  }
}
function jenjang(){
  var jenjang_js=$("#jen").val();
  if(jenjang_js=="1"){
    $("#no_seri_ijazah").hide();
    $("#no_ujian_nasional").hide();
    $("#no_seri_skhun").hide();
    $("#bank").show();
    $("#wn").show();
  }else if(jenjang_js=="2" || jenjang_js=="3" || jenjang_js=="4"){
    $("#no_seri_ijazah").show();
    $("#no_ujian_nasional").show();
    $("#no_seri_skhun").show();
    $("#bank").hide();
    $("#wn").hide();
  }
}
function auto(){
  var jenjang_js=document.getElementById("jen").value;
  if (jenjang_js=="") {
    alert("Pilih Jenjang Terlebih Dahulu...!");
    return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/siswa/cari_id') ?>",
        type:"POST",
        data:"jenjang="+jenjang_js,
        cache:false,
        success: function(msg){
          data=msg.split("|");
          $("#kode_siswa").val(data[0]);
        }
    })
  }
}
function hide_prestasi(){
    $("#tampil_prestasi").hide();
}
function kirim_data_prestasi(){
  $("#tampil_prestasi").show();
  var id_siswa = document.getElementById("kode_siswa").value;
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "id_siswa="+id_siswa;
     xhr.open("POST", "adm/siswa/t_prestasi", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_prestasi").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
function simpan_prestasi(){
    var id_siswa            = document.getElementById("kode_siswa").value;
    var jenis_prestasi_js   = $("#jenis_prestasi").val();
    var tingkat_js          = $("#tingkat").val();
    var nama_prestasi_js    = $("#nama_prestasi").val();
    var tahun_js            = $("#tahun").val();
    var penyelenggara_js    = $("#penyelenggara").val();
    if (id_siswa=="" || jenis_prestasi_js=="" || nama_prestasi_js == "") {
        alert("Isikan data dengan lengkap, pastikan No.Induk terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/siswa/save_prestasi') ?>",
            type:"POST",
            data:"id_siswa="+id_siswa+"&jenis_prestasi="+jenis_prestasi_js+"&tingkat="+tingkat_js+"&nama_prestasi="+nama_prestasi_js+"&tahun="+tahun_js+"&penyelenggara="+penyelenggara_js,
            cache:false,
            success:function(msg){
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_prestasi();
                    kosong();
                }else{
                    kirim_data_prestasi();
                    kosong();
                }
            }
        })
    }
}
function kosong(args){
  $("#jenis_prestasi").val("");
  $("#tingkat").val("");
  $("#nama_prestasi").val("");
  $("#tahun").val("");
  $("#penyelenggara").val("");
}
function hapus_prestasi(a,b){
    var id_prestasi_js  = a;
    var id_siswa_js     = b;
    $.ajax({
        url : "<?php echo site_url('adm/siswa/delete_prestasi') ?>",
        type:"POST",
        data:"id_prestasi="+id_prestasi_js+"&id_siswa="+id_siswa_js,
        cache:false,
        success:function(msg){
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_prestasi();
            }
        }
    })
}


function hide_beasiswa(){
    $("#tampil_beasiswa").hide();
}
function kirim_data_beasiswa(){
  $("#tampil_beasiswa").show();
  var id_siswa = document.getElementById("kode_siswa").value;
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "id_siswa="+id_siswa;
     xhr.open("POST", "adm/siswa/t_beasiswa", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_beasiswa").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
function simpan_beasiswa(){
    var id_siswa            = document.getElementById("kode_siswa").value;
    var jenis_beasiswa_js   = $("#jenis_beasiswa").val();
    var sumber_js           = $("#sumber").val();
    var tahun_mulai_js      = $("#tahun_mulai").val();
    var tahun_selesai_js    = $("#tahun_selesai").val();
    if (id_siswa=="" || jenis_beasiswa_js=="" || sumber_js == "") {
        alert("Isikan data dengan lengkap, pastikan No.Induk terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/siswa/save_beasiswa') ?>",
            type:"POST",
            data:"id_siswa="+id_siswa+"&jenis_beasiswa="+jenis_beasiswa_js+"&sumber="+sumber_js+"&tahun_mulai="+tahun_mulai_js+"&tahun_selesai="+tahun_selesai_js,
            cache:false,
            success:function(msg){
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_beasiswa();
                    kosong();
                }else{
                    kirim_data_beasiswa();
                    kosong();
                }
            }
        })
    }
}
function kosong(args){
  $("#jenis_beasiswa").val("");
  $("#sumber").val("");
  $("#tahun_mulai").val("");
  $("#tahun_selesai").val("");
}
function hapus_beasiswa(a,b){
    var id_beasiswa_js  = a;
    var id_siswa_js     = b;
    $.ajax({
        url : "<?php echo site_url('adm/siswa/delete_beasiswa') ?>",
        type:"POST",
        data:"id_beasiswa="+id_beasiswa_js+"&id_siswa="+id_siswa_js,
        cache:false,
        success:function(msg){
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_beasiswa();
            }
        }
    })
}
</script>
<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
function ambil_jenjang(){
  var id_jenjang = $("#jen").val();
  $.ajax({
    url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,
    cache:false,
    success:function(msg){
      $("#kelas").html(msg);
    }
  })
}
$("#jen").change(function(){
  var id_jenjang = $("#jen").val();
  $.ajax({
    url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,
    cache:false,
    success:function(msg){
      $("#kelas").html(msg);
    }
  })
});

$("#id_ta").change(function(){
  var id_ta = $("#id_ta").val();
  var jenjang = $("#jen").find(":selected").val();
  console.log("jejang : "+jenjang);
  $.ajax({
    url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
    type:"POST",
    data:{"id_ta":id_ta,"id_jenjang":jenjang},
    cache:false,
    success:function(msg){
      $("#kelas").html(msg);
    }
  })
});

</script>