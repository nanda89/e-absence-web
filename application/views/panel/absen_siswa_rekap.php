<script src="assets/panel/js_chart/jquery-1.9.1.min.js" type="text/javascript"></script>
<?php 
function tanggal(){
  $hari=$day=date("l");
  $tanggal=date("d");
  $bulan=$bl=$month=date("m");
  $tahun=date("Y");
  $jam=date("H");
  $menit=date("i");
  $detik=date("s");

  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }

  switch($bulan)
  {
    case"1":$bulan="Januari"; break;
    case"2":$bulan="Februari"; break;
    case"3":$bulan="Maret"; break;
    case"4":$bulan="April"; break;
    case"5":$bulan="Mei"; break;
    case"6":$bulan="Juni"; break;
    case"7":$bulan="Juli"; break;
    case"8":$bulan="Agustus"; break;
    case"9":$bulan="September"; break;
    case"10":$bulan="Oktober"; break;
    case"11":$bulan="November"; break;
    case"12":$bulan="Desember"; break;
  }

  $tglLengkap="$hari, $tanggal $bulan $tahun";
  return $tglLengkap;
}
?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">              
              <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>                                                                  
              <!-- <a href='adm/absen_siswa_rekap/export_all' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export Semua Data</a>         -->
            </h3>            
            <ul>
              <li>
                Anda bisa melakukan proses pencarian data dengan banyak kata kunci sekaligus, misal : kelas (spasi) jenjang (spasi) tanggal
              </li>
              <li>
                Anda bisa melakukan filter data rekap absen perhari atau perminggu atau perbulan dalam tiap kelas yang anda tentukan.
              </li>
              <li>
                Dengan menggunakan fasilitas filter, anda juga bisa menggunakan fitur pengiriman SMS harian atau mingguan.
              </li>
              <li>
                Anda juga bisa mencetak rekap absen siswa ini baik secara keseluruhan maupun perkelas.
              </li>
            </ul>              
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          
          <div class="box-body with-border">
            <form class="form-horizontal" action="adm/absen_siswa_rekap/filter" method="get" enctype="multipart/form-data">                                
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-4">
                  <select id="jenjang" name="id_jenjang" required class="form-control">                    
                    <option value=''>Pilih Jenjang</option>
                    <?php 
                    foreach ($dt_jenjang->result() as $s) {
                      echo"
                      <option value='$s->id_jenjang'>$s->jenjang</option>
                      ";
                    }
                    ?>
                  </select>
                </div>
                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-4">
                  <select id="kelas" name="id_kelas" required class="form-control">                    
                    <option value=''>Pilih Kelas</option>                    
                  </select>
                </div>                             
              </div>                                
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                <div class="col-sm-4">
                  <select id="mapel" name="id_mapel" class="form-control">                    
                    <option value=''>Pilih Mapel</option>                    
                  </select>
                </div>
                <label for="inputEmail3" class="col-sm-2 control-label">Status Kehadiran</label>
                <div class="col-sm-4">
                  <select name="status" class="form-control" required>
                    <option value="">Pilih Status</option>
                    <option value="semua">Semua</option>
                    <option value="hadir">Hadir</option>
                    <option value="tidak_hadir">Tidak Hadir</option>
                    <option value="alpha">Alpha</option>
                    <option value="izin">Izin</option>
                    <option value="sakit">Sakit</option>
                  </select>
                </div>                           
              </div>    
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Awal</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control pull-right" id="tanggal" name="tgl_awal" placeholder="Tanggal Awal" required="true">
                </div>     
                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Akhir</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control pull-right" id="tanggal2" name="tgl_akhir" placeholder="Tanggal Akhir" required="true">
                </div>         
              </div>

              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
                  <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>              
                </div>             
              </div>  
            </form>  
          </div> 
          <div class="box-body">
            <?php               
            if(isset($_GET['m'])){
              if($_GET['m'] == 'success'){
                $_SESSION['pesan'] = "Data Rekap Berhasil Terkirim ke Server Pusat";
                $_SESSION['tipe'] = "info";
              }else{
                $_SESSION['pesan'] = "Kode Aktivasi tidak Terdaftar Aktif Di Sistem Pusat";
                $_SESSION['tipe'] = "danger";
              }
            }

            if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
            ?>                  
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>  
                </button>
            </div>
            <?php
            }
                $_SESSION['pesan'] = '';                        
                    
            ?>
            <!-- <table id="table" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%"><input type="checkbox" id="check-all"></th>                                                  
                  <th width="1%">No</th>      
                  <th width="5%">NISN</th>      
                  <th width="5%">No.Induk</th>      
                  <th>Nama Lengkap</th>
                  <th>Mata Pelajaran</th>                                     
                  <th>Kelas</th>   
                  <th>Tanggal</th>       
                  <th width="15%">Jam Absen</th>                  
                </tr>
              </thead>
              <tbody>  
              
          
              </tbody>
            </table> -->


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>

    <!-- FILTER AREA -->
    <?php
        }elseif($set='filter'){
            $id_jenjang   = $this->input->get('id_jenjang');
            $id_kelas     = $this->input->get('id_kelas');
            $id_mapel   = $this->input->get('id_mapel');
            $tgl_awal     = $this->input->get('tgl_awal');
            $tgl_akhir    = $this->input->get('tgl_akhir');
            $status       = $this->input->get('status');
    ?>
    <div class="row">      
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">              
                        <a href='adm/absen_siswa_rekap' type='button' class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</a>          
                        <?php echo "<a target='_blank' href='adm/absen_siswa_rekap/export_filter?a=1&id_jenjang=$id_jenjang&id_kelas=$id_kelas&id_mapel=$id_mapel&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' class='btn btn-warning btn-flat margin'><i class='fa fa-print'></i> Cetak Data</a>";?>
                        <?php echo "<a target='_blank' href='adm/absen_siswa_rekap/export_filter?a=2&id_jenjang=$id_jenjang&id_kelas=$id_kelas&id_mapel=$id_mapel&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' class='btn btn-primary btn-flat margin'><i class='fa fa-file-excel-o'></i> Download Data</a>";?>
                    </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
           
                <div class="box-body">
                    <table data-ride="datatables" class="table table-bordered table-hovered">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th width="5%">NISN</th>
                                <th width="5%">No.Induk</th>
                                <th>Nama Lengkap</th>
                                <th>Mata Pelajaran</th>
                                <th>Kelas</th>
                                <th>Tanggal</th>
                                <th width="15%">Jam Absen</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        $('[data-ride="datatables"]').each(function() {
                            var oTable = $(this).dataTable( {
                                "bDestroy": true,
                                "processing": true,
                                "serverSide": true,
                                "sAjaxSource": "<?php echo $url_json?>",
                                "sPaginationType": "full_numbers",
                                "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
                                "aoColumns": [
                                    { "mData": "id_absen_mapel" },
                                    { "mData": "nisn" },
                                    { "mData": "id_siswa" },
                                    { "mData": "nama_lengkap" },
                                    { "mData": "mapel" },
                                    { "mData": "id_penempatan" },
                                    { "mData": "tgl" },
                                    { "mData": "id_penempatan" }
                                ],
                                "aaSorting": [[0, 'desc']],
                                "aoColumnDefs": [
                                    {
                                        "aTargets": [0],
                                        "mData":"ad",
                                        "mRender": function (data, type, full) {
                                            return full.no;
                                        }
                                    },{
                                        "aTargets": [5],
                                        "mData":"ad",
                                        "mRender": function (data, type, full) {
                                            return full.jenjang+" "+full.kelas;
                                        }
                                    },{
                                        "aTargets": [7],
                                        "mData":"ad",
                                        "mRender": function (data, type, full) {
                                            return full.jam+full.absen;
                                        }
                                    }
                                ],
                                "fnHeaderCallback": function( nHead, aData, iStart, iEnd, aiDisplay ) {
                                    $(nHead).children('th').addClass('text-center');
                                },
                                "fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
                                    $(nFoot).children('th').addClass('text-center');
                                },
                                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                                    $(nRow).children('td:nth-child(1),td:nth-child(2),td:nth-child(3),td:nth-child(5),td:nth-child(6),td:nth-child(7),td:nth-child(8)').addClass('text-center');
                                }

                            });
                        });
                    });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!-- END -->
    
    <?php
    }elseif($set='set_sms'){
      $id_jenjang   = $this->input->get('id_jenjang');
      $id_kelas     = $this->input->get('id_kelas');
      $tgl_awal     = $this->input->get('tgl_awal');
      $tgl_akhir    = $this->input->get('tgl_akhir');
      $status     = $this->input->get('status');
    ?>
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">              
              <a href='adm/absen/filter?id_jenjang=$id_jenjang&id_kelas=$id_kelas&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' type='button' class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</a>          
              <?php
              echo "
              <a href='adm/absen/export_filter?id_jenjang=$id_jenjang&id_kelas=$id_kelas&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' class='btn btn-warning btn-flat margin'><i class='fa fa-file-excel-o'></i> Export Data</a>        
              ";
              ?>                            
            </h3>                                      
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          
           
          <div class="box-body">
            
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>      
                  <th width="5%">No.Induk</th>      
                  <th>Nama Lengkap</th>
                  <th>Jenjang</th>  
                  <th>Kelas</th>   
                  <th>Mata Pelajaran</th>
                  <th>Tanggal</th>       
                  <th width="15%">Jam Absen Masuk</th>
                  <th width="15%">Jam Absen Pulang</th>                      
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_wali_kelas->result() as $row) { 
              $tanggal = date("d F Y", strtotime($row->tgl));
              $tgl = date('d-m-Y');
              $s = "SELECT * FROM tabel_absen_mapel RIGHT JOIN tabel_siswakelas 
                    ON tabel_absen_mapel.id_penempatan = tabel_siswakelas.id_penempatan LEFT JOIN tabel_jadwal
                    ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal INNER JOIN tabel_mapel
                    ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
                    WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen_mapel.tgl = '$row->tgl'
                    AND tabel_absen_mapel.valid='valid'";
              $d = $this->db->query($s);
              $ambil = $d->row();
              if($d->num_rows()>0){
                if($ambil->absen_masuk==''){
                  $absen_masuk = "-";
                  $jam_masuk = '';  
                }else{
                  $absen_m = $ambil->absen_masuk; 
                  if($absen_m=='alpha'){
                    $absen = "<span class='label label-danger'>$absen_m</span>";
                  }elseif($absen_m=='izin'){
                    $absen = "<span class='label label-warning'>$absen_m</span>";
                  }elseif($absen_m=='sakit'){
                    $absen = "<font class='label label-primary'>$absen_m</span>";
                  }elseif($absen_m=='hadir'){
                    $absen = "<font class='label label-success'>$absen_m</span>";
                  }
                  $jam_masuk = $ambil->jam_masuk." | ";  
                }
              }else{                
                $absen_masuk = "-";
                $jam_masuk = '';                
              }

              echo "          
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>      
                  <td>$row->nama_lengkap</td>  
                  <td>$row->jenjang</td>  
                  <td>$ambil->id_mapel</td>
                  <td>$row->kelas</td>
                  <td>$tanggal</td>                         
                  <td>$jam_masuk$absen_masuk</td>
                  <td>$jam_pulang$absen_pulang</td>
                  "; ?>                                 
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>    
    <?php  
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#jenjang").change(function(){
  var id_jenjang = $("#jenjang").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#kelas").html(msg); 
      get_mapel(id_jenjang);     
    }
  })  
});
function get_mapel(a){ 
  $.ajax({
    url : "<?php echo site_url('adm/absen_siswa_rekap/get_mapel')?>",
    type:"POST",
    data:"jenjang="+a,      
    cache:false,   
    success:function(msg){            
      $("#mapel").html(msg);
    }
  })
}
</script>

<script type="text/javascript">
function kosong(args){
  $("#tingkat").val('');  
  $("#id_jenjang").val('');  
  $("#id_tahun").val('');  
  $("#id_kelas").val(''); 
  $("#tampil_pendataan").hide();
}
function detail(a){ 
  var id        = a;     
  var s_process = 'detail';
  $.ajax({
    url : "<?php echo site_url('adm/siswa/process')?>",
    type:"POST",
    data:"id="+id+"&s_process="+s_process,
    cache:false,
  })
}

function hapus_penempatan(a){ 
  var id_penempatan     = a;     
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan')?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_pendataan();
          }
      }
  })
}

function kirim_data_pendataan(){    
  $("#tampil_pendataan").show();
  var id_tahun    = document.getElementById("id_tahun").value;     
  var id_kelas    = document.getElementById("id_kelas").value;     
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/pendataan/t_pendataan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_pendataan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>

<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/absen_siswa_rekap/ajax_bulk_delete')?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }                  
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>

<script type="text/javascript">

var table;

// $(document).ready(function() {
//     //datatables
//     table = $('#table').DataTable({

//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "order": [], //Initial no order.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo site_url('adm/absen_siswa_rekap/ajax_list')?>",
//             "type": "POST"
//         },

//         //Set column definition initialisation properties.
//         "columnDefs": [
//         {
//             "targets": [ 0 ], //first column / numbering column
//             "orderable": false, //set not orderable
//         },
//         ],
//     });
// });

</script>

