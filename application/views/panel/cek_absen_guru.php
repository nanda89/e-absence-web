<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $judul1; ?></li>
    </ol>
  </section>
  <section class="content">

    <!-- VIEW -->
    <?php
    if ($set == "view") {
      ?>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a href='adm/cek_absen_guru/export_now?a=1' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
            <a href='adm/cek_absen_guru/export_now?a=2' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
          </h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/cek_absen_guru/filter" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <?php
                $tgl1  = gmdate("d-m-Y", time() + 60 * 60 * 7);
                $hari  = hari();
                ?>
              <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
              <div class="col-sm-2">
                Dari :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
              </div>
              <div class="col-sm-2">
                Sampai :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
              </div>
              <div class="col-sm-2">
                Jenis Absen :
                <select class="form-control" name="jenis">
                  <option>Semua</option>
                  <option>Terlambat</option>
                  <option>Tidak Absen</option>
                </select>
              </div>
              <div class="col-sm-2">
                Guru :
                <select name="guru" class="form-control">
                  <?php
                    $data .= "<option value=''>Pilih Guru</option>";
                    foreach ($guru->result() as $row) {
                      $data .= "<option value='$row->id_guru'>$row->nama ($row->alias)</option>\n";
                    }
                    echo $data;
                    ?>
                </select>
              </div>
              <div class="col-sm-2">
                Mapel :
                <select name="mapel" class="form-control">
                  <?php
                    $nm_mapel .= "<option value=''>Pilih Mapel</option>";
                    foreach ($mapel->result() as $row) {
                      $nm_mapel .= "<option value='$row->id_mapel'>$row->mapel</option>\n";
                    }
                    echo $nm_mapel;
                    ?>
                </select>
              </div>
              <div class="col-sm-4" style="margin-top:10px;">
                <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
              </div>
            </div>
          </form>
        </div>
        <div class="box-body">
          <?php
            $args = array(
              'jenis'     => 'Semua',
              'id_guru'   => '',
              'tgl'       => date('Y-m-d'),
              'tgl_akhir' => date('Y-m-d')
            );
            $absensi = get_all_harian_absen_mengajar_guru($args); ?>
          <table id="example2" class="table table-bordered table-hovered">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Nama Guru</th>
                <th>Mengajar di Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Tanggal</th>
                <th>Jadwal Absen</th>
                <th>Validasi Absen</th>
                <th>Ket</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                foreach ($absensi as $absen) : ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $absen->nama; ?>
                  </td>
                  <td>
                    <?php echo $absen->jenjang; ?>
                    <?php echo $absen->kelas; ?>
                  </td>
                  <td>
                    <?php echo $absen->mapel; ?>
                  </td>
                  <td>
                    <?php echo $absen->tgl; ?>
                  </td>
                  <td>
                    <?php echo $absen->jadwal_absen; ?>
                  </td>
                  <td>
                    <?php
                        $keterangan = ucwords(str_replace('_', ' ', $absen->ket));
                        $validasi = ucwords(str_replace('_', ' ', $absen->validasi));
                        $ket_nihil = ['Nihil', 'Izin', 'Sakit', 'Tanpa Keterangan', 'Alpha'];
                        if ($absen->validasi == 'present' && !in_array($keterangan, $ket_nihil)) {
                          echo '<span class="label label-success"><i class="fa fa-close"></i> present</span> ' . $absen->jam_absen;
                        } else {
                          echo '<span class="label label-danger"><i class="fa fa-close"></i> nihil</span>';
                        }
                        ?>
                  </td>
                  <td>
                    <?php
                        // if( $validasi == 'Nihil' || $validasi == 'Alpha' )
                        // $keterangan = '';
                        if ($validasi == 'Alpha')
                          $keterangan = 'Tanpa Keterangan';
                        // if( $validasi == 'Nihil' || $keterangan == 'Izin' || $keterangan == 'Sakit' || $keterangan == 'Tanpa Keterangan'  ) {
                        //   echo '<a href="#" class="sinkron-keterangan-mengajar" data-type="select" data-url="adm/sinkron_absen_guru_rekap/absen_manual_mengajar" data-pk="'.$absen->id_jadwal.'" data-tgl="'.$absen->tgl.'" data-title="Pilih keterangan">';
                        //   echo $keterangan;
                        //   echo '</a>';
                        // } else {
                        echo $keterangan;
                        // }
                        ?>
                  </td>
                </tr>
              <?php $no++;
                endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <!-- ABSEN GURU FILTER -->
    <?php
    } elseif ($set == "filter") {
      ?>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a href='adm/cek_absen_guru' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>
            <a href='adm/cek_absen_guru/export_filter?a=1&id_guru=<?php echo $id_guru ?>&id_mapel=<?php echo $id_mapel ?>&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir; ?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
            <a href='adm/cek_absen_guru/export_filter?a=2&id_guru=<?php echo $id_guru ?>&id_mapel=<?php echo $id_mapel ?>&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir; ?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
          </h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/cek_absen_guru/filter" method="get" enctype="multipart/form-data">
            <div class="form-group">
              <?php
                $tgl1  = date("l", strtotime($tgl));
                $hari  = cek_hari($tgl1);
                ?>
              <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
              <div class="col-sm-2">
                Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
              </div>
              <div class="col-sm-2">
                Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
              </div>
              <div class="col-sm-2">
                Jenis Absen :
                <select class="form-control" disabled name="jenis">
                  <option><?php echo $jenis ?></option>
                </select>
              </div>
              <div class="col-sm-2">
                Guru :
                <select name="guru" disabled class="form-control">
                  <option value="<?php echo $id_guru ?>"><?php echo $nama_guru ?></option>
                </select>
              </div>
              <div class="col-sm-2">
                Mapel :
                <select name="mapel" disabled class="form-control">
                  <option value="<?php echo $id_mapel ?>"><?php echo $nama_mapel ?></option>
                </select>
              </div>

            </div>
          </form>
        </div>
        <div class="box-body">
          <?php
          
            $args = array(
              'jenis'     => $jenis,
              'id_guru'   => $id_guru,
              'tgl'       => $tgl,
              'tgl_akhir' => $tgl_akhir,
              'id_mapel'  => $id_mapel

            );
            $absensi = get_all_harian_absen_mengajar_guru($args); ?>
          <table id="example2" class="table table-bordered table-hovered">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Nama Guru</th>
                <th>Mengajar di Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Tanggal</th>
                <th>Jadwal Absen</th>
                <th>Validasi Absen</th>
                <th>Ket</th>
                <th>Hadir</th>
                <th>Tidak Hadir</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                $tgl2 = $tgl;
                foreach ($absensi as $absen) : ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $absen->nama; ?>
                  </td>
                  <td>
                    <?php echo $absen->jenjang; ?>
                    <?php echo $absen->kelas; ?>
                  </td>
                  <td>
                    <?php echo $absen->mapel; ?>
                  </td>
                  <td>
                    <?php echo $absen->tgl; ?>
                  </td>
                  <td>
                    <?php echo $absen->jadwal_absen; ?>
                  </td>
                  <td>
                    <?php
                        // echo '-='.$absen->ket.'|'.$absen->validasi.'=-';
                        $keterangan = ucwords(str_replace('_', ' ', $absen->ket));
                        $validasi = ucwords(str_replace('_', ' ', $absen->validasi));
                        $ket_nihil = ['Nihil', 'Izin', 'Sakit', 'Tanpa Keterangan', 'Alpha'];
                        if ($absen->validasi == 'present' && !in_array($keterangan, $ket_nihil)) {
                          echo '<span class="label label-success"><i class="fa fa-close"></i> present</span> ' . $absen->jam_absen;
                        } else {
                          echo '<span class="label label-danger"><i class="fa fa-close"></i> nihil</span>';
                        }
                        ?>
                  </td>
                  <td>
                    <?php
                        // if( $validasi == 'Nihil' || $validasi == 'Alpha' )
                        // $keterangan = '';
                        if ($validasi == 'Alpha')
                          $keterangan = 'Tanpa Keterangan';
                        // if( $validasi == 'Nihil' || $keterangan == 'Izin' || $keterangan == 'Sakit' || $keterangan == 'Tanpa Keterangan'  ) {
                        //   echo '<a href="#" class="sinkron-keterangan-mengajar" data-type="select" data-url="adm/sinkron_absen_guru_rekap/absen_manual_mengajar" data-pk="'.$absen->id_jadwal.'" data-tgl="'.$absen->tgl.'" data-title="Pilih keterangan">';
                        //   echo $keterangan;
                        //   echo '</a>';
                        // } else {
                        echo $keterangan;
                        // }
                        ?>
                  </td>
                  <td>
                    <?php echo $absen->hadir; ?>
                  </td>
                
                  <td>
                    <?php echo $absen->tidak_hadir; ?>
                  </td>
              
                </tr>
                
              <?php $no++;
                endforeach; ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>