<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">
        <?php echo $judul1; ?>
      </li>
    </ol>
  </section>
  <section class="content">

    <!-- VIEW -->
    <?php if ($set == "view"): ?>

    <div class="box">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">
          <a href='adm/sinkron_absen_guru_rekap/export_now?a=1' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
          <a href='adm/sinkron_absen_guru_rekap/export_now?a=2' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
        </h3> -->
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div class="box-body with-border">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
    ?>
          <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          </div>
          <?php
}
$_SESSION['pesan'] = '';

?>
            <form class="form-horizontal" action="adm/sinkron_absen_guru_rekap/filter" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <?php
$tgl1 = gmdate("d-m-Y", time() + 60 * 60 * 7);
$hari = hari();
?>
                  <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                  <div class="col-sm-2">
                    Dari :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                  </div>
                  <div class="col-sm-2">
                    Sampai :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                  </div>
                  <div class="col-sm-2">
                    Jenis Absen :
                    <select class="form-control" name="jenis">
                <option>Semua</option>
                <option>Terlambat</option>
                <option>Tidak Absen</option>
              </select>
                  </div>
                  <div class="col-sm-2">
                    Guru :
                    <select name="guru" class="form-control">
                <?php
$data .= "<option value=''>Pilih Guru</option>";
foreach ($guru->result() as $row) {
    $data .= "<option value='$row->id_guru'>$row->nama ($row->alias)</option>\n";
}
echo $data;
?>
              </select>
                  </div>
                  <div class="col-sm-2">
                    Jenjang :
                    <select name="jenjang" class="form-control">
                <?php
// $data1 .= "<option value=''>Pilih Jenjang</option>";
foreach ($jenjang->result() as $row) {
    $data1 .= "<option value='$row->id_jenjang'>$row->jenjang</option>\n";
}
echo $data1;
?>
              </select>
                  </div>
                  <div class="col-sm-2 hide">
                    Tahun Ajaran :
                    <select name="tahunajaran" class="form-control">
                <?php
// $data2 .= "<option value=''>Pilih Tahun Ajaran</option>";
foreach ($tahunajaran->result() as $row) {
    $data2 .= "<option value='$row->id_ta'>$row->tahun_ajaran</option>\n";
}
echo $data2;
?>
              </select>

                  </div>
                  <div class="col-sm-4" style="margin-top:10px;">
                    <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                    <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>

                    <!-- <a href="<?php echo base_url('adm/sinkron_absen_guru_rekap/sync') ?>" class="btn btn-flat margin btn-primary"><i class="fa fa-upload"></i> Sinkron data (testing)</a> -->
                  </div>
              </div>
            </form>
      </div>
      <div class="box-body">
        <?php
$args = array(
    'jenis' => 'Semua',
    'id_guru' => '',
    'tgl' => date('Y-m-d'),
    'tgl_akhir' => date('Y-m-d'),
);
$absensi = get_all_harian_absen_mengajar_guru($args);?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Nama Guru</th>
              <th>Mengajar di Kelas</th>
              <th>Mata Pelajaran</th>
              <th>Tanggal</th>
              <th>Jadwal Absen</th>
              <th>Validasi Absen</th>
              <th>Ket</th>
            </tr>
          </thead>
          <tbody>
            <?php
$no = 1;
foreach ($absensi as $absen): ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $absen->nama; ?>
              </td>
              <td>
                <?php echo $absen->jenjang; ?>
                <?php echo $absen->kelas; ?>
              </td>
              <td>
                <?php echo $absen->mapel; ?>
              </td>
              <td>
                <?php echo $absen->tgl; ?>
              </td>
              <td>
                <?php echo $absen->jadwal_absen; ?>
              </td>
              <td>
                <?php
$keterangan = ucwords(str_replace('_', ' ', $absen->ket));
$validasi = ucwords(str_replace('_', ' ', $absen->validasi));
$ket_nihil = ['Nihil', 'Izin', 'Sakit', 'Tanpa Keterangan', 'Alpha'];
if ($absen->validasi == 'present' && !in_array($keterangan, $ket_nihil)) {
    echo '<span class="label label-success"><i class="fa fa-close"></i> present</span> ' . $absen->jam_absen;
} else {
    echo '<span class="label label-danger"><i class="fa fa-close"></i> nihil</span>';
}
?>
              </td>
              <td>
                <?php
// if( $validasi == 'Nihil' || $validasi == 'Alpha' )
// $keterangan = '';
if ($validasi == 'Alpha') {
    $keterangan = 'Tanpa Keterangan';
}

// if( $validasi == 'Nihil' || $keterangan == 'Izin' || $keterangan == 'Sakit' || $keterangan == 'Tanpa Keterangan'  ) {
//   echo '<a href="#" class="sinkron-keterangan-mengajar" data-type="select" data-url="adm/sinkron_absen_guru_rekap/absen_manual_mengajar" data-pk="'.$absen->id_jadwal.'" data-tgl="'.$absen->tgl.'" data-title="Pilih keterangan">';
//   echo $keterangan;
//   echo '</a>';
// } else {
echo $keterangan;
// }
?>
              </td>
            </tr>
            <?php $no++;endforeach;?>
          </tbody>
          </table>
      </div>
    </div>

    <!-- ABSEN GURU FILTER -->
    <?php elseif ($set == "filter"): ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href='adm/sinkron_absen_guru_rekap' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>
          <a href='adm/sinkron_absen_guru_rekap/export_filter?a=1&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir; ?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>&id_jenjang=<?php echo $id_jenjang ?>&id_ta=<?php echo $id_ta ?>' onclick="return confirm('Anda yakin ingin melanjutkan?')"
            class="btn btn-warning btn-flat margin hide"><i class="fa fa-file-excel-o"></i> Export</a>
          <a href='adm/sinkron_absen_guru_rekap/export_filter?a=2&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir; ?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>&id_jenjang=<?php echo $id_jenjang ?>&id_ta=<?php echo $id_ta ?>' target="_blank"
            onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin hide"><i class="fa fa-print"></i> Cetak</a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div class="box-body with-border">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
    ?>
          <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          </div>
          <?php
}
$_SESSION['pesan'] = '';

?>
            <form class="form-horizontal" action="adm/sinkron_absen_guru_rekap/filter" method="get" enctype="multipart/form-data">
              <div class="form-group">
                <?php
$tgl1 = date("l", strtotime($tgl));
$hari = cek_hari($tgl1);
?>
                  <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                  <div class="col-sm-2">
                    Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                  </div>
                  <div class="col-sm-2">
                    Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                  </div>
                  <div class="col-sm-2">
                    Jenis Absen :
                    <select class="form-control" disabled name="jenis">
                <option><?php echo $jenis ?></option>
              </select>
                  </div>
                  <div class="col-sm-2">
                    Guru :
                    <select name="guru" disabled class="form-control">
                <option value="<?php echo $id_guru ?>"><?php echo $nama_guru ?></option>
              </select>
                  </div>
                  <div class="col-sm-2">
                    Jenjang :
                    <select name="jenjang" disabled class="form-control">
                <option value="<?php echo $id_jenjang ?>"><?php echo $jenjang ?></option>
              </select>
                  </div>
                  <div class="col-sm-2 hide">
                    Tahun Ajaran :
                    <select name="tahunajaran" disabled class="form-control">
                <option value="<?php echo $id_ta ?>"><?php echo $tahunajaran ?></option>
              </select>
                  </div>

              </div>
            </form>
            <input type="hidden" id="tgl" value="<?php echo $tgl ?>">
            <input type="hidden" id="tgl2" value="<?php echo $tgl_akhir ?>">
            <input type="hidden" id="jenis" value="<?php echo $jenis ?>">
            <input type="hidden" id="guru" value="<?php echo $id_guru ?>">
            <input type="hidden" id="jenjang" value="<?php echo $id_jenjang ?>">
            <input type="hidden" id="tahunajaran" value="<?php echo $id_ta ?>">

            <button type="button" onclick="kirim()" class="btn btn-flat margin btn-primary"><i class="fa fa-upload"></i> Sinkron data</button>
      </div>
      <div class="box-body">
        <?php
$args = array(
    'jenis' => $jenis,
    'id_guru' => $id_guru,
    'tgl' => $tgl,
    'tgl_akhir' => $tgl_akhir,
);
$absensi = get_all_harian_absen_mengajar_guru($args);?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Nama Guru</th>
              <th>Mengajar di Kelas</th>
              <th>Mata Pelajaran</th>
              <th>Tanggal</th>
              <th>Jadwal Absen</th>
              <th>Validasi Absen</th>
              <th>Ket</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
$no = 1;
$tgl2 = $tgl;
foreach ($absensi as $absen): ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $absen->nama; ?>
              </td>
              <td>
                <?php echo $absen->jenjang; ?>
                <?php echo $absen->kelas; ?>
              </td>
              <td>
                <?php echo $absen->mapel; ?>
              </td>
              <td>
                <?php echo $absen->tgl; ?>
              </td>
              <td>
                <?php echo $absen->jadwal_absen; ?>
              </td>
              <td>
                <?php
$keterangan = ucwords(str_replace('_', ' ', $absen->ket));
$validasi = ucwords(str_replace('_', ' ', $absen->validasi));
$ket_nihil = ['Nihil', 'Izin', 'Sakit', 'Tanpa Keterangan', 'Alpha'];
if ($absen->validasi == 'present' && !in_array($keterangan, $ket_nihil)) {
    echo '<span class="label label-success"><i class="fa fa-close"></i> present</span> ' . $absen->jam_absen;
} else {
    echo '<span class="label label-danger"><i class="fa fa-close"></i> nihil</span>';
}
// echo '('.$absen->ket.' | ' . $absen->validasi . ')';
?>
              </td>
              <td>
                <?php
// if( $validasi == 'Nihil' || $validasi == 'Alpha' )
//   $keterangan = '';
if ($validasi == 'Alpha') {
    $keterangan = 'Tanpa Keterangan';
}

// if( $validasi == 'Nihil' || $keterangan == 'Izin' || $keterangan == 'Sakit' || $keterangan == 'Tanpa Keterangan'  ) {
//   echo '<a href="#" class="sinkron-keterangan-mengajar" data-type="select" data-url="adm/sinkron_absen_guru_rekap/absen_manual_mengajar" data-pk="'.$absen->id_jadwal.'" data-tgl="'.$absen->tgl.'" data-title="Pilih keterangan">';
//   echo $keterangan;
//   echo '</a>';
// } else {
echo $keterangan;
// }
?>
              </td>
              <td>
                <button type="button" id="btn-ubah-absensi" class="btn btn-warning btn-xs"
                onclick="getAbsensiMapelByIdkelas(this)" data-target="#change-absence"
                data-jadwalId="<?php echo $absen->id_jadwal; ?>"
                data-jamAbsen="<?php echo $absen->jadwal_absen; ?>"
                data-kelas="<?php echo $absen->kelas; ?>"
                data-kelasId="<?php echo $absen->id_kelas; ?>"
                data-mapel="<?php echo $absen->mapel; ?>"
                data-mapelId="<?php echo $absen->id_mapel; ?>"
                data-tgl="<?php echo $absen->tgl; ?>"><i class="fa fa-pencil-square-o"></i> Ubah</button>
              </td>
            </tr>
            <?php $no++;endforeach;?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

  <!-- modal change absence -->
<div class="modal fade" id="change-absence" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ubah Absensi</h4>
              </div>
              <div class="modal-body">
                <div class="col-sm-6">                          
                  <div class="row">
                    <label for="mdl-hdr-kelas"><b>Kelas: </b></label>
                    <label id="mdl-hdr-kelas"></label>
                  </div>
                  <div class="row">
                    <label for="mdl-hdr-mapel"><b>Mapel: </b></label>
                    <label id="mdl-hdr-mapel"></label>
                  </div><br>
                </div>
                <form class="form-horizontal" id="form-absen" enctype="multipart/form-data" method="post">
                  <table id="tbl-daftar-siswa" class="table">
                      <thead id="head-tbl-daftar-siswa">
                        <th></th>
                        <th width="2%">No</th>
                        <th>Siswa</th>
                        <th>Jam</th>
                        <th>Absensi</th>
                        <th><button type="submit" class="btn btn-primary btn-xs" id="btn-send-all"><i class="fa fa-send"></i> Kirim Semua</button></th>
                      </thead>
                      <tbody id="body-tbl-daftar-siswa"></tbody>
                  </table>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Kirim</button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
</div>

    <?php endif;?>

  </section>
</div>
<script type="text/javascript">
  function kirim() {

    var value = {
      tanggal: $("#tanggal").val(),
      tgl_akhir: $("#tgl2").val(),
      jenis: $("#jenis").val(),
      id_guru: $("#guru").val(),
      id_jenjang: $("#jenjang").val(),
      id_ta: $("#tahunajaran").val()
    }
    $.ajax({
      type: "GET",
      url: "<?php echo site_url('adm/sinkron_absen_guru_rekap/sinkronisasi') ?>",
      data: value,
      beforeSend: function() {
        $('#loading-status').show();
      },
      cache: false,
      success: function(msg) {
        data = msg.split("|");
        if (data[0] == 'ok') {
          $('#loading-status').hide();
          window.location.href = "<?php echo site_url('adm/sinkron_absen_guru_rekap') ?>";
        }
      },
      statusCode: {
        500: function() {
          $('#loading-status').hide();
          alert("Something Wen't Wrong");
        }
      }
    });
  }

  function getAbsensiMapelByIdkelas(e){    
    const idJadwal = e.getAttribute('data-jadwalId');
    const jamAbsen = e.getAttribute('data-jamAbsen');
    const jamAbsArr = jamAbsen.split(" ");
    const tgl = e.getAttribute('data-tgl');
    const kelas = e.getAttribute('data-kelas');
    const mapel = e.getAttribute('data-mapel');
    var params = {
      id_kelas: e.getAttribute('data-kelasId'),
      id_jadwal: idJadwal,
			tgl: tgl   
    }
    $.ajax({
      method: "POST",
      url: "<?php echo site_url('adm/sinkron_absen_guru_rekap/get_absensi_mapel_by_id_kelas') ?>",
      data: params,
      dataType : 'json',
      // beforeSend: function() {
      //   // $('#loading-status').show();
      // },
      async: false,
      success: function(data) {
        var i, no;
        var tr = '';
        $('#tbl-daftar-siswa tbody').empty();
        for (i=0; i<data.length; i++) {           
          no=i+1;
          tr += '<tr>' +
                  '<td></td>'+
                  '<td>'+ no +'</td>'+
                  '<td>'+ data[i].nama_lengkap +'</td>';

          if(data[i].jam != null && data[i].jam != ''){
            tr += '<td><input class="form-control inputmask" style="width: 65px;" id="jam-absen-'+data[i].id_siswa+'" name="jam_absen['+i+']" placeholder="'+data[i].jam+'"></td>';
          } else {
            tr += '<td><input class="form-control inputmask" style="width: 65px;" id="jam-absen-'+data[i].id_siswa+'" name="jam_absen['+i+']" value="'+jamAbsArr[0]+'" placeholder="__:__"></td>';
          }

          tr += '<td> <select class="form-control" style="width: 100px;" id="slc-absen-'+data[i].id_siswa+'" name="absen['+i+']">';
          
          if(data[i].absen != null){
            if(data[i].absen=='izin'){
              tr += '<option value="hadir">Hadir</option>'+
                    '<option value="izin" selected="selected">Izin</option>'+
                    '<option value="sakit">Sakit</option>'+
                    '<option value="alpha">Alpha</option>';
                    
            } else if(data[i].absen=='sakit'){
              tr += '<option value="hadir">Hadir</option>'+
                    '<option value="izin">Izin</option>'+
                    '<option value="sakit" selected="selected">Sakit</option>'+
                    '<option value="alpha">Alpha</option>';
            } else if(data[i].absen=='alpha'){
              tr += '<option value="hadir">Hadir</option>'+
                    '<option value="izin">Izin</option>'+
                    '<option value="sakit">Sakit</option>'+
                    '<option value="alpha" selected="selected">Alpha</option>';
            } else {
              tr += '<option value="hadir">Hadir</option>'+
                    '<option value="izin">Izin</option>'+
                    '<option value="sakit">Sakit</option>'+
                    '<option value="alpha">Alpha</option>';
            }
            
          } else {
            tr += '<option value="hadir">Hadir</option>'+
                  '<option value="izin">Izin</option>'+
                  '<option value="sakit">Sakit</option>'+
                  '<option value="alpha">Alpha</option>';
          }
                      
          tr += '</select></td>'+
                '<td>'+
                '<button type="button" id="btn-kirim-'+data[i].id_siswa+'" class="btn btn-primary btn-xs" onclick="changeAbsence(this)"'+
                'data-idPenempatan="'+data[i].id_penempatan+'" data-idJadwal="'+idJadwal+'" data-tgl="'+tgl+'" data-idSiswa="'+data[i].id_siswa+'"'+
                'data-jamAbsen="'+jamAbsen+'"><i class="fa fa-send"></i> Kirim</button>';
            
          if(data[i].absen != null && data[i].absen != ''){
            tr += '&nbsp&nbsp<label class="control-label" for="updateSuccess"><i id="check-update'+data[i].id_siswa+'" class="fa fa-check"></i></label>';
          } else {
            tr += '&nbsp&nbsp<label class="control-label" for="updateSuccess"><i id="check-update'+data[i].id_siswa+'"></i></label>';
          }
          tr += '</td>'+
                '<td>'+
                '<input type="hidden" name="id_penempatan['+i+']" value="'+data[i].id_penempatan+'"/>'+
                '<input type="hidden" name="id_jadwal['+i+']" value="'+idJadwal+'"/>'+
                '<input type="hidden" name="tgl['+i+']" value="'+tgl+'"/>'+
                '</td>'+
                '</tr>';
            
        }

        $('#mdl-hdr-kelas').text(kelas); 
        $('#mdl-hdr-mapel').text(mapel);
        $('#tbl-daftar-siswa tbody').append(tr);
        $(".inputmask").mask("99:99");  
        $('#form-absen').attr("action", "adm/sinkron_absen_guru_rekap/change_absence_all/"+data.length);
        $('#change-absence').modal("show");
      },
      statusCode: {
        500: function() {
          $('#loading-status').hide();
          alert("Something Wen't Wrong");
        }
      }
    });
  };

  function changeAbsence(e){   
    var xId = e.getAttribute('data-idSiswa');
    var jamAbsenArr = e.getAttribute('data-jamAbsen').split(" s/d ");
    var jamAwalInt = jamAbsenArr[0].replace(":","");
    var jamAkhirInt = jamAbsenArr[1].replace(":","");
    var jamAbsen = $('#jam-absen-'+xId).val();
    
    if(jamAbsen!=""){
      var jamAbsenInt = jamAbsen.replace(":","");
      var hmJamAbsen = jamAbsen.split(":");
      if(hmJamAbsen[0]>24 || hmJamAbsen[1]>60){
        alert('Jam absen tidak valid');
        $('#jam-absen-'+xId).val("");
        return;
      } else if(jamAbsenInt<jamAwalInt || jamAbsenInt>jamAkhirInt){
        alert('Jam absen berada di luar Jam Absensi Jadwal');
        $('#jam-absen-'+xId).val("");
        return;
      }
    } else {
      jamAbsen = jamAbsenArr[0];
    }
    
    var absen = $('#slc-absen-'+xId).val(); 
    var params = {
      id_penempatan: e.getAttribute('data-idPenempatan'),
      id_jadwal: e.getAttribute('data-idJadwal'),
      jam_absen: jamAbsen,
      tgl: e.getAttribute('data-tgl'),
      absen: absen,      
    }
    $.ajax({
      type: "POST",
      url: "<?php echo site_url('adm/sinkron_absen_guru_rekap/change_absence') ?>",
      data: params,
      // beforeSend: function() {
      //   $('#loading-status').show();
      // },
      async : false,
      dataType : 'json',
      success: function(data) {
       
      },
      statusCode: {
        200: function() {
          $('#check-update'+xId).removeClass('fa fa-check'); 
          $('#check-update'+xId).addClass('fa fa-check'); 
        },
        500: function() {
          $('#loading-status').hide();
          alert("Something Wen't Wrong");
        }
      }
    });
  }
  
</script>
