<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="detail"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          
            <button type="button" onclick="back()" class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Lihat Data</button>            
          
        </h3>                
        <!--div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div-->
      <?php 
      $row_siswa = $one_siswa->row();       
      ?>
      </div><!-- /.box-header -->
      <input type="hidden" id="id_daftar" value="<?php $row_siswa->id_daftar ?>">
      <div class="box-body">
        <div class="row">
          <div class="col-md-9">            
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo strtoupper($row_siswa->nama_lengkap) ?> (<?php echo strtoupper($row_siswa->id_daftar) ?>)</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="box-group" id="accordion">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  
                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                          Identitas Peserta Didik
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="box-body">
                        <table width="100%" border="0">
                          <tr>
                            <td width="30%">
                              <label class="control-label">NISN</label>
                            </td>
                            <td><?php echo $row_siswa->nisn; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Jenjang</label>
                            </td>
                            <td>
                              <?php 
                              $r = $this->m_jenjang->get_one($row_siswa->id_jenjang);
                              $r_je = $r->row();
                              echo $r_je->jenjang;
                              ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Jenis Kelamin</label>
                            </td>
                            <td><?php echo $row_siswa->jenis_kelamin; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">No Seri Ijazah</label>
                            </td>
                            <td><?php echo $row_siswa->no_seri_ijazah; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">No Seri SKHUN</label>
                            </td>
                            <td><?php echo $row_siswa->no_seri_skhun; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">No Ujian Nasional</label>
                            </td>
                            <td><?php echo $row_siswa->no_ujian_nasional; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">NIK</label>
                            </td>
                            <td><?php echo $row_siswa->nik; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Tempat - Tgl Lahir</label>
                            </td>
                            <td><?php echo $row_siswa->tempat_lahir." - ".$row_siswa->tgl_lahir; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">AgamaK</label>
                            </td>
                            <td><?php echo $row_siswa->agama; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Berkebutuhan Khusus</label>
                            </td>
                            <td><?php echo $row_siswa->kebutuhan_khusus; ?></td>
                          </tr>
                          <tr>
                            <td valign="top">
                              <label class="control-label">Alamat Lengkap</label>
                            </td>
                            <td></td>
                          </tr>
                            <td>                      
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Jalan</label>                        
                            </td>
                            <td><?php echo $row_siswa->al_jalan; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Dusun</label>
                            </td>
                            <td><?php echo $row_siswa->al_dusun; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;RT/RW</label>
                            </td>
                            <td><?php echo $row_siswa->al_rtw; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Kodepos</label>
                            </td>
                            <td><?php echo $row_siswa->al_kodepos; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Kelurahan/Desa</label>
                            </td>
                            <td><?php echo $row_siswa->al_kel; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Kecamatan</label>
                            </td>
                            <td><?php echo $row_siswa->al_kec; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Kabupaten</label>
                            </td>
                            <td><?php echo $row_siswa->al_kab; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label"> &nbsp;&nbsp;&nbsp;&nbsp;Provinsi</label>
                            </td>
                            <td><?php echo $row_siswa->al_prov; ?></td>
                          </tr> 
                          <tr>
                            <td>
                              <label class="control-label">Alat Transportasi</label>
                            </td>
                            <td><?php echo $row_siswa->alat_transport; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Jenis Tinggal</label>
                            </td>
                            <td><?php echo $row_siswa->jenis_tinggal; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">No.Telp/HP</label>
                            </td>
                            <td><?php echo $row_siswa->no_telp." / ".$row_siswa->no_hp; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Email</label>
                            </td>
                            <td><?php echo $row_siswa->email; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">No.KPS</label>
                            </td>
                            <td><?php echo $row_siswa->no_kps; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Kewarganegaraan</label>
                            </td>
                            <td><?php echo $row_siswa->wn; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">PIP</label>
                            </td>
                            <td><?php echo $row_siswa->pip; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <label class="control-label">Bank - No.Rekening</label>
                            </td>
                            <td><?php echo $row_siswa->bank." - ".$row_siswa->no_rek; ?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-danger">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                          Data Ayah Kandung
                        </a>
                      </h4>
                    </div>
                    <?php 
                    $row_detail = $one_detail->row();       
                    ?>
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="box-body">
                        <table width="100%" border="0">
                          <tr>
                            <td width="30%">
                              <label class="control-label">Nama Ayah</label>
                            </td>
                            <td><?php echo $row_detail->ay_nama; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Tahun Lahir</label>
                            </td>
                            <td><?php echo $row_detail->ay_tahun_lahir; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Berkebutuhan Khusus</label>
                            </td>
                            <td><?php echo $row_detail->ay_kebutuhan_khusus; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pekerjaan</label>
                            </td>
                            <td><?php echo $row_detail->ay_kerja; ?></td>
                          </tr>                          
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pendidikan</label>
                            </td>
                            <td><?php echo $row_detail->ay_pendidikan; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Penghasilan</label>
                            </td>
                            <td><?php echo $row_detail->ay_penghasilan; ?></td>
                          </tr>                          
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-success">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                          Data Ibu Kandung
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="box-body">
                        <table width="100%" border="0">
                          <tr>
                            <td width="30%">
                              <label class="control-label">Nama Ibu</label>
                            </td>
                            <td><?php echo $row_detail->ib_nama; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Tahun Lahir</label>
                            </td>
                            <td><?php echo $row_detail->ib_tahun_lahir; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Berkebutuhan Khusus</label>
                            </td>
                            <td><?php echo $row_detail->ib_kebutuhan_khusus; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pekerjaan</label>
                            </td>
                            <td><?php echo $row_detail->ib_kerja; ?></td>
                          </tr>                          
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pendidikan</label>
                            </td>
                            <td><?php echo $row_detail->ib_pendidikan; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Penghasilan</label>
                            </td>
                            <td><?php echo $row_detail->ib_penghasilan; ?></td>
                          </tr>                          
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-warning">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                          Data Wali
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                      <div class="box-body">
                        <table width="100%" border="0">
                          <tr>
                            <td width="30%">
                              <label class="control-label">Nama Wali</label>
                            </td>
                            <td><?php echo $row_detail->wl_nama; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Tahun Lahir</label>
                            </td>
                            <td><?php echo $row_detail->wl_tahun_lahir; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Berkebutuhan Khusus</label>
                            </td>
                            <td><?php echo $row_detail->wl_kebutuhan_khusus; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pekerjaan</label>
                            </td>
                            <td><?php echo $row_detail->wl_kerja; ?></td>
                          </tr>                          
                          <tr>
                            <td width="30%">
                              <label class="control-label">Pendidikan</label>
                            </td>
                            <td><?php echo $row_detail->wl_pendidikan; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Penghasilan</label>
                            </td>
                            <td><?php echo $row_detail->wl_penghasilan; ?></td>
                          </tr>                          
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                          Data Periodik
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                      <div class="box-body">
                        <table width="100%" border="0">
                          <tr>
                            <td width="30%">
                              <label class="control-label">Tinggi Badan (cm)</label>
                            </td>
                            <td><?php echo $row_detail->tinggi; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Berat Badan (kg)</label>
                            </td>
                            <td><?php echo $row_detail->berat; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Jarak Tempat Tinggal ke Sekolah (km)</label>
                            </td>
                            <td><?php echo $row_detail->jarak; ?></td>
                          </tr>
                          <tr>
                            <td width="30%">
                              <label class="control-label">Waktu Tempuh Berangkat Sekolah (menit)</label>
                            </td>
                            <td><?php echo $row_detail->waktu; ?></td>
                          </tr>                          
                          <tr>
                            <td width="30%">
                              <label class="control-label">Jumlah Saudara Kandung</label>
                            </td>
                            <td><?php echo $row_detail->saudara_kandung; ?></td>
                          </tr>                                              
                        </table>
                      </div>
                    </div>
                  </div>

                  <!--div class="panel box box-info">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                          Data Prestasi
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">                            
                            <div class="col-sm-10">                             
                             <button type="button" onClick="kirim_data_prestasi()" class="btn btn-warning btn-flat"><i class="fa fa-eye"></i> Show</button>
                             <button type="button" onClick="hide_prestasi()" class="btn btn-warning btn-flat"><i class="fa fa-eye-slash"></i> Hide</button>
                            </div>                           
                        </div>                        
                        <div class="form-group">
                          <br>
                          <div id="tampil_prestasi"></div>
                        </div>                       
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-danger">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                          Data Beasiswa
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">                            
                            <div class="col-sm-10">                             
                             <button type="button" onClick="kirim_data_beasiswa()" class="btn btn-warning btn-flat"><i class="fa fa-eye"></i> Show</button>
                             <button type="button" onClick="hide_beasiswa()" class="btn btn-warning btn-flat"><i class="fa fa-eye-slash"></i> Hide</button>
                            </div>                           
                        </div>                        
                        <div class="form-group">
                          <br>
                          <div id="tampil_beasiswa"></div>
                        </div>                       
                      </div>
                    </div>
                  </div-->



                </div>
              </div><!-- /.box-body -->
            </div><!-- /.box -->            
          </div>

          <div class="col-md-3">
            <?php echo"<img src='assets/panel/images/$row_siswa->gambar' width='200px '>"; ?>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>


<script type="text/javascript">
function back(){
  history.go(-1);
}
</script>