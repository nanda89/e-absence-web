<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/pendaftar">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>No.Daftar</th>
              <th>Nama Lengkap</th>              
              <th>Asal Sekolah</th>                            
              <th>Jalur</th>              
              <th>Status</th>
              <th width="20%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_ta->result() as $row) {       
          if($row->jenis_kelamin=='laki-laki'){
            $j = "lk";
          }else{
            $j = "pr";
          }
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->id_daftar</td>
              <td>$row->nama_lengkap ($j)</td>               
              <td>$row->asal_sekolah</td>                            
              <td>$row->kelompok</td>
              <td>$row->status</td>
              <td>";
              ?>
              <form method="post" action="adm/pendaftar/process">
                <input type="hidden" name="id" value="<?php echo $row->id_daftar ?>" />          
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>          
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-primary btn-flat">Kelulusan</button>
                  <button type="button" class="btn btn-sm btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <?php 
                  $a = $_GET['id_daf_jenis'];
                  $b = $_GET['tahun'];
                  $c = $_GET['id_daf_jadwal'];
                  $d = $row->id_daftar;
                  echo "                  
                  <ul class='dropdown-menu' role='menu'>
                    <li><a href='adm/pendaftar/lulus1?a=$a&b=$b&c=$c&d=$d'>Lulus</a></li>
                    <li><a href='adm/pendaftar/t_lulus1?a=$a&b=$b&c=$c&d=$d'>Tidak Lulus</a></li>              
                  </ul>
                  ";
                  ?>
                </div>
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div>
    </div>
  </section>
  </section>