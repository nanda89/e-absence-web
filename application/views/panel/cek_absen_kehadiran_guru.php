<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">
        <?php echo $judul1; ?>
      </li>
    </ol>
  </section>
  <section class="content">

    <!-- VIEW -->
    <?php if($set == "view"){ ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href='adm/cek_absen_kehadiran_guru/export_now?a=1' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
          <a href='adm/cek_absen_kehadiran_guru/export_now?a=2' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/cek_absen_kehadiran_guru/filter" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <?php          
                    $tgl1  = gmdate("d-m-Y", time()+60*60*7);
                    $hari  = hari();
                ?>
            <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
            <div class="col-sm-2">
              Dari :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
            </div>
            <div class="col-sm-2">
              Sampai :<input type="text" class="form-control" value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
            </div>
            <div class="col-sm-2">
                          Jenis Absen :         
                          <select class="form-control" name="jenis">
                              <option>Semua</option>
                              <option>Hadir</option>
                              <option>Izin</option>
                              <option>Sakit</option>
                              <option>Tidak Absen</option>
                          </select>
                      </div>
            <div class="col-sm-3">
                          Guru : 
                          <select name="guru" class="form-control">
                              <?php
                                  $data .= "<option value=''>Pilih Guru</option>";
                                  foreach ($guru->result() as $row) {
                                      $data .= "<option value='$row->id_guru'>$row->nama ($row->alias)</option>\n";
                                  }
                                  echo $data;
                              ?>
                          </select>
                      </div>
            <div class="col-sm-3" style="margin-top:10px;">
              <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
              <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body">
      <?php
            $args = array(
              'tgl' => date('Y-m-d'),
              'tgl_akhir' => date('Y-m-d')
            );
            $absensi = get_all_harian_absen_kehadiran_guru($args);
            ?>
            <table id="example2" class="table table-bordered table-hovered">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Nama Guru</th>
                        <th>Tanggal</th>
                        <th>Jam masuk</th>
                        <th>Jam pulang</th>
                        <th>Validasi jam masuk</th>
                        <th>Validasi jam pulang</th>
                        <th>Keterangan jam masuk</th>
                        <th>Keterangan jam pulang</th>         
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $list_ket = get_keterangan_absen();
                  $no=1; foreach( $absensi as $absen ) : ?>
                  <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $absen->nama; ?></td>
                      <td><?php echo $absen->tgl; ?></td>     
                      <td><?php echo $absen->jam_masuk_kerja; ?></td>
                      <td><?php echo $absen->jam_pulang_kerja; ?></td>                                         
                      <td><?php echo $absen->validasi_jam_masuk; ?></td>
                      <td><?php echo $absen->validasi_jam_pulang; ?></td>  
                      <td><?php echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk; ?> </td>
                      <td><?php echo $absen->keterangan_jam_pulang; ?></td>
                  </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
            </table>
      </div>
    </div>

    <!-- ABSEN GURU FILTER -->
    <?php
    }elseif($set == "filter"){
?>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <a href='adm/cek_absen_kehadiran_guru' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>
            <a href='adm/cek_absen_kehadiran_guru/export_filter?a=1&guru=<?php echo $id_guru ?>&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
            <a href='adm/cek_absen_kehadiran_guru/export_filter?a=2&guru=<?php echo $id_guru ?>&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
          </h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/cek_absen_kehadiran_guru/filter" method="get" enctype="multipart/form-data">
            <div class="form-group">
              <?php
                    $tgl1  = date("l", strtotime($tgl));
                    $hari  = cek_hari($tgl1);
                ?>
                <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                <div class="col-sm-2">
                  Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                </div>
                <div class="col-sm-2">
                  Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                </div>
                <div class="col-sm-2">   
                        Jenis Absen :                    
                        <select class="form-control" disabled name="jenis">
                            <option><?php echo $jenis ?></option>                
                        </select>
                </div>
                <div class="col-sm-3">
                  Guru : 
                  <select name="guru" disabled class="form-control">
                    <option value="<?php 
                              if($id_guru == ''){
                                echo "Pilih Guru";
                              }else{
                                echo $id_guru;
                              }
                            ?>"><?php 
                            if($id_guru == ''){
                              echo "Pilih Guru";
                            }else{
                              echo $nama_guru;
                            }
                            
                            ?></option>
                  </select>
                </div> 

            </div>
          </form>
        </div>
        <div class="box-body">
        
        <?php 
          $args = array(
            'tgl' => $tgl,
            'tgl_akhir' => $tgl_akhir,
            'id_guru'   => $id_guru,
            'jenis'     => $jenis
          );
          $absensi = get_all_harian_absen_kehadiran_guru($args);
        
        ?>
            <table id="example2" class="table table-bordered table-hovered">
            <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Nama Guru</th>
                        <th>Tanggal</th>
                        <th>Jam masuk</th>
                        <th>Jam pulang</th>
                        <th>Validasi jam masuk</th>
                        <th>Validasi jam pulang</th>
                        <th>Keterangan jam masuk</th>
                        <th>Keterangan jam pulang</th>         
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $list_ket = get_keterangan_absen();
                  $no=1; foreach( $absensi as $absen ) : ?>
                  <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $absen->nama; ?></td>
                      <td><?php echo $absen->tgl; ?></td>     
                      <td><?php echo $absen->jam_masuk_kerja; ?></td>
                      <td><?php echo $absen->jam_pulang_kerja; ?></td>                                         
                      <td><?php echo $absen->validasi_jam_masuk; ?></td>
                      <td><?php echo $absen->validasi_jam_pulang; ?></td>  
                      <td><?php echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk; ?> </td>
                      <td><?php echo $absen->keterangan_jam_pulang; ?></td>
                  </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <?php
    }
    ?>
  </section>
</div>