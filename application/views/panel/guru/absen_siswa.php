
<div class="content-wrapper">
<!-- Content Header (Page header) -->


<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section> 

  <section class="content">
    
    <?php
    if($set=="view"){
    ?>    
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
             <?php 
             if($dt_absen_kelas->num_rows()>0){                           
              $row = $dt_absen_kelas->row();

              $id_kelas  = $this->uri->segment(4);
              $id_jadwal  = $this->uri->segment(5);
              $sql = $this->db->query("SELECT * FROM tabel_jadwal INNER JOIN tabel_jenjang 
                      ON tabel_jadwal.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_mapel
                      ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel WHERE tabel_jadwal.id_jadwal = '$id_jadwal'")->row();

              ?>
            <h3 class="box-title">
              <a href="guru/absen_siswa">
                <button onclick="return confirm('Anda Yakin Ingin Kembali?')" class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
              </a>
              <a href="guru/absen/hadir?id=<?php echo $id_kelas ?>&j=<?php echo $id_jadwal ?>">
                <button onclick="return confirm('Anda Yakin Ingin Absen Hadir Semua?')" class="btn btn-success btn-flat margin"><i class="fa fa-users"></i> Absen Hadir Semua</button>
              </a>              
              <a href="guru/absen/validasi?id=<?php echo $id_kelas ?>&j=<?php echo $id_jadwal ?>">
                <button onclick="return confirm('Anda Yakin Ingin Validasi Absen?')" class="btn btn-primary btn-flat margin"><i class="fa fa-check"></i> Validasi Absen</button>
              </a>              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->

          <div class="box-body with-border">
            <table class="table table-bordered">
             
              <tr>
                <td width='20%'><b>Jenjang, Kelas</td>
                <td><?php echo $sql->jenjang ?>, <?php echo $row->kelas ?></td>
              </tr>
              <tr>
                <td width='20%'><b>Mata Pelajaran</td>
                <td><?php echo $sql->mapel." <span class='label label-success'>$sql->jam_awal s/d $sql->jam_akhir</span>"; ?></td>
              </tr>
              <tr>
                <td><b>Tahun Ajaran</td>
                <td><?php echo $row->tahun_ajaran ?></td>
              </tr>              
              <tr>
                <td><b>Hari, Jam, Tanggal</td>
                <td>
                  <?php                                              
                  $har   = hari();
                  $jam    = gmdate("H:i", time()+60*60*7);
                  $tgl1   = gmdate("d F Y", time()+60*60*7);
                  echo $jam.", ".$har.", ".$tgl1; 
                  ?>
                </td>
              </tr>
            </table>
          </div> 
          <div class="box-body">
            
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>      
                  <th width="15%">No.Induk</th>      
                  <th>Nama Lengkap</th>            
                  <th width="20%">Jam Absen</th>
                  <th width="5%">Aksi</th>                      
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_absen_kelas->result() as $row) {

              $tgl  = gmdate("d-m-Y", time()+60*60*7);              
              $s = "SELECT tabel_absen_mapel.* FROM tabel_absen_mapel INNER JOIN tabel_siswakelas ON 
                    tabel_absen_mapel.id_penempatan = tabel_siswakelas.id_penempatan
                    WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen_mapel.tgl = '$tgl'
                    AND tabel_absen_mapel.id_jadwal = '$id_jadwal'";
              $d = $this->db->query($s);
              $ambil = $d->row();
              if($d->num_rows()>0){
                if($ambil->absen==''){
                  $absen = "-";
                  $jam = '';  
                }else{
                  $absen = $ambil->absen; 
                  $jam = $ambil->jam." | ";  
                }                
                $id_absen = $ambil->id_absen_mapel;

                if($ambil->valid=='valid'){
                  $va = 'disabled';
                }else{
                  $va = '';
                }
                if($ambil->valid=='valid'){
                  $ve = 'disabled';
                }else{
                  $ve = '';
                }

              }else{
                $id_absen = '';
                $absen = "-";
                $jam = '';
                $ve = '';
                $va = '';
              }

              
              echo "          
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>      
                  <td>$row->nama_lengkap</td>                         
                  <td>"; ?>             
                    <div class="btn-group">
                      <button <?php echo $va ?> type="button" class="btn btn-sm btn-info btn-flat"><?php echo $jam.$absen ?></button>
                      <button <?php echo $va ?> type="button" class="btn btn-sm btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="guru/absen/ket_absen?k=<?php echo $id_kelas ?>&h=<?php echo $id_jadwal ?>&g=<?php echo $sql->status ?>&v=hadir&id=<?php echo $row->id_penempatan ?>">Hadir</a></li>
                        <li><a href="guru/absen/ket_absen?k=<?php echo $id_kelas ?>&h=<?php echo $id_jadwal ?>&g=<?php echo $sql->status ?>&v=alpha&id=<?php echo $row->id_penempatan ?>">Alpha</a></li> 
                        <li><a href="guru/absen/ket_absen?k=<?php echo $id_kelas ?>&h=<?php echo $id_jadwal ?>&g=<?php echo $sql->status ?>&v=izin&id=<?php echo $row->id_penempatan ?>">Izin</a></li>              
                        <li><a href="guru/absen/ket_absen?k=<?php echo $id_kelas ?>&h=<?php echo $id_jadwal ?>&g=<?php echo $sql->status ?>&v=sakit&id=<?php echo $row->id_penempatan ?>">Sakit</a></li>                     
                      </ul>
                    </div>                    
                  </td>                  
                  <td>
                    <form action="guru/absen/hapus_absen" method="post">
                      <input type="hidden" name="id_absen" value="<?php echo $id_absen ?>"> 
                      <input type="hidden" name="id_kelas" value="<?php echo $id_kelas ?>"> 
                      <button onclick="return confirm('Anda Yakin Ingin Menghapusnya?')" <?php echo $va ?> type="submit" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></button>
                    </form>
                  </td>
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>
      <?php }else{ ?>
      Data tidak ditemukan

      <?php } ?>
    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_tingkat')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#tingkat").html(msg);      
    }
  })  
});
$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  var tingkat = $("#tingkat").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang+"&tingkat="+tingkat,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>

<script type="text/javascript">
function kosong(args){
  $("#tingkat").val('');  
  $("#id_jenjang").val('');  
  $("#id_tahun").val('');  
  $("#id_kelas").val(''); 
  $("#tampil_pendataan").hide();
}
function detail(a){ 
  var id        = a;     
  var s_process = 'detail';
  $.ajax({
    url : "<?php echo site_url('adm/siswa/process')?>",
    type:"POST",
    data:"id="+id+"&s_process="+s_process,
    cache:false,
  })
}

function hapus_penempatan(a){ 
  var id_penempatan     = a;     
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan')?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_pendataan();
          }
      }
  })
}

function kirim_data_pendataan(){    
  $("#tampil_pendataan").show();
  var id_tahun    = document.getElementById("id_tahun").value;     
  var id_kelas    = document.getElementById("id_kelas").value;     
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/pendataan/t_pendataan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_pendataan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>