<?php 
function tanggal(){
  $hari=$day=date("l");
  $tanggal=date("d");
  $bulan=$bl=$month=date("m");
  $tahun=date("Y");
  $jam=date("H");
  $menit=date("i");
  $detik=date("s");

  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }

  switch($bulan)
  {
    case"1":$bulan="Januari"; break;
    case"2":$bulan="Februari"; break;
    case"3":$bulan="Maret"; break;
    case"4":$bulan="April"; break;
    case"5":$bulan="Mei"; break;
    case"6":$bulan="Juni"; break;
    case"7":$bulan="Juli"; break;
    case"8":$bulan="Agustus"; break;
    case"9":$bulan="September"; break;
    case"10":$bulan="Oktober"; break;
    case"11":$bulan="November"; break;
    case"12":$bulan="Desember"; break;
  }

  $tglLengkap="$hari, $tanggal $bulan $tahun";
  return $tglLengkap;
}
?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
             <?php 
             if($dt_wali_kelas->num_rows()>0){                           
              $row = $dt_wali_kelas->row();
              ?>
            <h3 class="box-title">
              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body with-border">
            <h5>Anda bisa melakukan proses pencarian data dengan banyak kata kunci sekaligus, misal : kelas (spasi) jenjang (spasi) tanggal</h5>
          </div> 
          <div class="box-body">
            
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>      
                  <th width="5%">No.Induk</th>      
                  <th>Nama Lengkap</th>
                  <th>Jenjang</th>  
                  <th>Kelas</th>   
                  <th>Tanggal</th>       
                  <th width="15%">Jam Absen Masuk</th>
                  <th width="15%">Jam Absen Pulang</th>                      
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_wali_kelas->result() as $row) { 
              $tanggal = date("d F Y", strtotime($row->tgl));
              $tgl = date('d-m-Y');
              $s = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                    WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$row->tgl'";
              $d = $this->db->query($s);
              $ambil = $d->row();
              if($d->num_rows()>0){
                if($ambil->absen_masuk==''){
                  $absen_masuk = "-";
                  $jam_masuk = '';  
                }else{
                  $absen_masuk = $ambil->absen_masuk; 
                  $jam_masuk = $ambil->jam_masuk." | ";  
                }

                if($ambil->absen_pulang==''){
                  $absen_pulang = "-";  
                  $jam_pulang = '';
                }else{
                  $absen_pulang = $ambil->absen_pulang; 
                  $jam_pulang = $ambil->jam_pulang." | "; 
                }
              }else{
                $absen_pulang = "-";
                $absen_masuk = "-";
                $jam_masuk = '';
                $jam_pulang = '';
              }

             
              echo "          
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>      
                  <td>$row->nama_lengkap</td>  
                  <td>$row->jenjang</td>  
                  <td>$row->kelas</td>
                  <td>$tanggal</td>                         
                  <td>$jam_masuk$absen_masuk</td>
                  <td>$jam_pulang$absen_pulang</td>
                  "; ?>                                 
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

       <?php }else{ ?>
      Data tidak ditemukan

      <?php } ?>

    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_tingkat')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#tingkat").html(msg);      
    }
  })  
});
$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();  
  var tingkat = $("#tingkat").val();  
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang+"&tingkat="+tingkat,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>

<script type="text/javascript">
function kosong(args){
  $("#tingkat").val('');  
  $("#id_jenjang").val('');  
  $("#id_tahun").val('');  
  $("#id_kelas").val(''); 
  $("#tampil_pendataan").hide();
}
function detail(a){ 
  var id        = a;     
  var s_process = 'detail';
  $.ajax({
    url : "<?php echo site_url('adm/siswa/process')?>",
    type:"POST",
    data:"id="+id+"&s_process="+s_process,
    cache:false,
  })
}

function hapus_penempatan(a){ 
  var id_penempatan     = a;     
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan')?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_pendataan();
          }
      }
  })
}

function kirim_data_pendataan(){    
  $("#tampil_pendataan").show();
  var id_tahun    = document.getElementById("id_tahun").value;     
  var id_kelas    = document.getElementById("id_kelas").value;     
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/pendataan/t_pendataan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_pendataan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>