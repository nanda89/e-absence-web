<?php
function tanggal()
{
    $hari = $day = date("l");
    $tanggal = date("d");
    $bulan = $bl = $month = date("m");
    $tahun = date("Y");
    $jam = date("H");
    $menit = date("i");
    $detik = date("s");

    switch ($hari) {
        case "Sunday":$hari = "Minggu";
            break;
        case "Monday":$hari = "Senin";
            break;
        case "Tuesday":$hari = "Selasa";
            break;
        case "Wednesday":$hari = "Rabu";
            break;
        case "Thursday":$hari = "Kamis";
            break;
        case "Friday":$hari = "Jumat";
            break;
        case "Saturday":$hari = "Sabtu";
            break;
    }

    switch ($bulan) {
        case "1":$bulan = "Januari";
            break;
        case "2":$bulan = "Februari";
            break;
        case "3":$bulan = "Maret";
            break;
        case "4":$bulan = "April";
            break;
        case "5":$bulan = "Mei";
            break;
        case "6":$bulan = "Juni";
            break;
        case "7":$bulan = "Juli";
            break;
        case "8":$bulan = "Agustus";
            break;
        case "9":$bulan = "September";
            break;
        case "10":$bulan = "Oktober";
            break;
        case "11":$bulan = "November";
            break;
        case "12":$bulan = "Desember";
            break;
    }

    $tglLengkap = "$hari, $tanggal $bulan $tahun";
    return $tglLengkap;
}
?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "view") {
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
             <?php
if ($dt_wali_kelas->num_rows() > 0) {
        $row = $dt_wali_kelas->row();
        ?>
            <h3 class="box-title">
              <a href="guru/absen/validasi_masuk?id=<?php echo $row->id_kelas ?>">
                <button onclick="return confirm('Anda Yakin Ingin Validasi Absen Masuk ?')" class="btn bg-maroon btn-flat margin"><i class="fa fa-check"></i> Validasi Absen Masuk</button>
              </a>
              <a href="guru/absen/validasi_pulang?id=<?php echo $row->id_kelas ?>">
                <button onclick="return confirm('Anda Yakin Ingin Validasi Absen Pulang ?')" class="btn bg-maroon btn-flat margin"><i class="fa fa-check"></i> Validasi Absen Pulang</button>
              </a>
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->

          <div class="box-body with-border">
            <table class="table table-bordered">

              <tr>
                <td width='20%'><b>Jenjang, Kelas</td>
                <td><?php echo $row->jenjang ?>, <?php echo $row->kelas ?></td>
              </tr>
              <tr>
                <td width='20%'><b>Wali Kelas</td>
                <td><?php echo $row->nama ?></td>
              </tr>
              <tr>
                <td><b>Tahun Ajaran</td>
                <td><?php echo $row->tahun_ajaran ?></td>
              </tr>
              <tr>
                <td><b>Hari, Tanggal</td>
                <td>
                  <?php
$tgl1 = gmdate("d F Y", time() + 60 * 60 * 7);
        echo $tgl1;
        ?>
                </td>
              </tr>
            </table>
          </div>
          <div class="box-body">

            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>
                  <th width="15%">No.Induk</th>
                  <th>Nama Lengkap</th>
                  <th width="20%">Jam Absen Masuk</th>
                  <th width="20%">Jam Absen Pulang</th>
                  <th width="5%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
$no = 1;
        foreach ($dt_wali_kelas->result() as $row) {

            $tgl = gmdate("d-m-Y", time() + 60 * 60 * 7);
            $s = "SELECT tabel_absen.* FROM tabel_absen INNER JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                    WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$tgl'";
            $d = $this->db->query($s);
            $ambil = $d->row();
            if ($d->num_rows() > 0) {
                if ($ambil->absen_masuk == '') {
                    $absen_masuk = "-";
                    $jam_masuk = '';
                } else {
                    $absen_masuk = $ambil->absen_masuk;
                    $jam_masuk = $ambil->jam_masuk . " | ";
                }

                if ($ambil->absen_pulang == '') {
                    $absen_pulang = "-";
                    $jam_pulang = '';
                } else {
                    $absen_pulang = $ambil->absen_pulang;
                    $jam_pulang = $ambil->jam_pulang . " | ";
                }
                $id_absen = $ambil->id_absen;

                if ($ambil->valid_masuk == 'valid') {
                    $va = 'disabled';
                } else {
                    $va = '';
                }
                if ($ambil->valid_pulang == 'valid') {
                    $ve = 'disabled';
                } else {
                    $ve = '';
                }

            } else {
                $id_absen = '';
                $absen_pulang = "-";
                $absen_masuk = "-";
                $jam_masuk = '';
                $jam_pulang = '';
                $ve = '';
                $va = '';
            }

            echo "
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>
                  <td>$row->nama_lengkap</td>
                  <td>";?>
                    <div class="btn-group">
                      <button <?php echo $va ?> type="button" class="btn btn-sm btn-info btn-flat"><?php echo $jam_masuk . $absen_masuk ?></button>
                      <button <?php echo $va ?> type="button" class="btn btn-sm btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="guru/absen/ket?g=masuk&v=hadir&id=<?php echo $row->id_penempatan ?>">Hadir</a></li>
                        <li><a href="guru/absen/ket?g=masuk&v=alpha&id=<?php echo $row->id_penempatan ?>">Alpha</a></li>
                        <li><a href="guru/absen/ket?g=masuk&v=izin&id=<?php echo $row->id_penempatan ?>">Izin</a></li>
                        <li><a href="guru/absen/ket?g=masuk&v=sakit&id=<?php echo $row->id_penempatan ?>">Sakit</a></li>
                      </ul>
                    </div>
                  </td>
                  <td>
                    <div class="btn-group">
                      <button <?php echo $ve ?> type="button" class="btn btn-sm btn-info btn-flat"><?php echo $jam_pulang . $absen_pulang ?></button>
                      <button <?php echo $ve ?> type="button" class="btn btn-sm btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="guru/absen/ket?g=pulang&v=hadir&id=<?php echo $row->id_penempatan ?>">Hadir</a></li>
                        <li><a href="guru/absen/ket?g=pulang&v=alpha&id=<?php echo $row->id_penempatan ?>">Alpha</a></li>
                        <li><a href="guru/absen/ket?g=pulang&v=izin&id=<?php echo $row->id_penempatan ?>">Izin</a></li>
                        <li><a href="guru/absen/ket?g=pulang&v=sakit&id=<?php echo $row->id_penempatan ?>">Sakit</a></li>
                      </ul>
                    </div>
                  </td>
                  <td>
                    <form action="guru/absen/hapus" method="post">
                      <input type="hidden" name="id_absen" value="<?php echo $id_absen ?>" name="hapus">
                      <button type="submit" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></button>
                    </form>
                  </td>
                </tr>
              <?php
$no++;
        }
        ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
      <?php } else {?>
      Data tidak ditemukan

      <?php }?>
    </div>

    <?php
}
?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_tingkat') ?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,
    cache:false,
    success:function(msg){
      $("#tingkat").html(msg);
    }
  })
});
$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  var tingkat = $("#tingkat").val();
  $.ajax({
    url : "<?php echo site_url('adm/pendataan/get_kelas') ?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang+"&tingkat="+tingkat,
    cache:false,
    success:function(msg){
      $("#id_kelas").html(msg);
    }
  })
});
</script>

<script type="text/javascript">
function kosong(args){
  $("#tingkat").val('');
  $("#id_jenjang").val('');
  $("#id_tahun").val('');
  $("#id_kelas").val('');
  $("#tampil_pendataan").hide();
}
function detail(a){
  var id        = a;
  var s_process = 'detail';
  $.ajax({
    url : "<?php echo site_url('adm/siswa/process') ?>",
    type:"POST",
    data:"id="+id+"&s_process="+s_process,
    cache:false,
  })
}

function hapus_penempatan(a){
  var id_penempatan     = a;
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan') ?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan,
      cache:false,
      success:function(msg){
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_pendataan();
          }
      }
  })
}

function kirim_data_pendataan(){
  $("#tampil_pendataan").show();
  var id_tahun    = document.getElementById("id_tahun").value;
  var id_kelas    = document.getElementById("id_kelas").value;
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/pendataan/t_pendataan", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_pendataan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
</script>