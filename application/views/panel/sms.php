<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>     
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">        
           
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php                       
        if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
        ?>                  
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>  
            </button>
        </div>
        <?php
        }
            $_SESSION['pesan'] = '';                        
                
        ?>
        <table id="" class="table table-bordered table-hovered">
          <thead>
            <tr>              
              <th width="10%">Fungsi</th>
              <th>Status</th>              
            </tr>
          </thead>
          <tbody>            
            <tr>
              <td align="right">
                <a href="http://localhost/web_absence/gammu/cek.php" aonclick="cek()" id="cek" class="btn bg-maroon btn-flat"> <i class="fa fa-check"></i> Cek</a>
              </td>
              <td><input class="form-control" readonly type="text" id="isi_cek"></td>
            </tr>         
            <tr>
              <td align="right">
                <button onclick="install()" class="btn bg-maroon btn-flat"> <i class="fa fa-check"></i> Install</button>
              </td>
              <td><input class="form-control" readonly type="text" id="isi_install"></td>
            </tr> 
            <tr>
              <td align="right">
                <button onclick="uninstall()" class="btn bg-maroon btn-flat"> <i class="fa fa-check"></i> Uninstall</button>
              </td>
              <td><input class="form-control" readonly type="text" id="isi_uninstall"></td>
            </tr>
            <tr>
              <td align="right">
                <button onclick="start()" class="btn bg-maroon btn-flat"> <i class="fa fa-check"></i> Start</button>
              </td>
              <td><input class="form-control" readonly type="text" id="isi_start"></td>
            </tr>
            <tr>
              <td align="right">
                <button onclick="stop()" class="btn bg-maroon btn-flat"> <i class="fa fa-check"></i> Stop</button>
              </td>
              <td><input class="form-control" readonly type="text" id="isi_stop"></td>
            </tr>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    <?php
    }
    ?>    
  </section>
</div>

<script type="text/javascript">
function cek(){
  var cek=document.getElementById("cek").value;
  
  $("#isi_cek").val("Bismillah");                                        
}
</script>