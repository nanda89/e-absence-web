<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Anda bisa lakukan perubahan jadwal pengiriman sms dengan fitur ini</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <?php 
      $re = $setting->row();
      ?>
      <div class="box-body">
        <?php                       
        if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
        ?>                  
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>  
            </button>
        </div>
        <?php
        }
            $_SESSION['pesan'] = '';                        
                
        ?>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordered table-hovered">
              <thead>
                <tr>                  
                  <th>Jadwal</th>                
                  <th>Filter</th>  
                  <th>Status</th>
                  <th width="5%">Aksi</th>                          
                </tr>
              </thead>
              <tbody>            
              <?php 
              foreach ($setting->result() as $k) {
                echo "
                <tr>
                  <form action='adm/gateway/update' method='post'>
                    <td>
                      <input type='text' class='form-control' readonly value='$k->tipe_sms'>
                      <input type='hidden' name='id_schedule' value='$k->id_schedule'>
                    </td>                  
                    <td>
                      <select name='aturan' class='form-control'>
                        <option value='$k->aturan'>$k->aturan</option>
                        <option>hadir</option>
                        <option>tidak hadir</option>
                        <option>semua</option>
                      </select>
                    </td>
                    <td>
                      <select name='status' class='form-control'>
                        <option value='$k->status'>$k->status</option>
                        <option>aktif</option>                        
                        <option>tidak aktif</option>                        
                      </select>
                    </td>
                    <td><button type='submit' class='btn bg-maroon btn-flat'>Simpan</button></td>                  
                  </form>
                </tr>
                ";
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div><!-- /.box -->
  </section>
</div>