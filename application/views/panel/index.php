<!--?php 
$tgl  = date('d');
if($tgl=='01' or $tgl=='10'){
  echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/tunggakan/mailku'>";
}
?-->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="assets/panel/#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">                        

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                <?php                              
                $d  = "SELECT COUNT(tabel_kelas.id_kelas) as jum FROM tabel_kelas";                
                $in = $this->db->query($d);
                $re  = $in->row();
                ?>
                  <h3><?php echo $re->jum ?> Kelas</h3>
                  <p>Aktif</p>
                </div>
                <div class="icon">
                  <i class="fa fa-flag"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/jenjang" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                <?php
                $level = $this->session->userdata('level');
                $id_cabang = $this->session->userdata('id_cabang');
                if ($level=='operator cabang') { 
                  $d  = "SELECT COUNT(tabel_siswa.id_siswa) as jum FROM tabel_siswa
                      WHERE tabel_siswa.id_cabang='$id_cabang'";
                }else{
                  $d  = "SELECT COUNT(tabel_siswa.id_siswa) as jum FROM tabel_siswa";
                }
                $in = $this->db->query($d);
                $re  = $in->row();
                ?>
                  <h3><?php echo $re->jum ?> orang</h3>
                  <p>Siswa Aktif</p>
                </div>
                <div class="icon">
                  <i class="fa fa-group"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/pendataan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <?php 
                  $e  = "SELECT COUNT(tabel_guru.id_guru) as jum FROM tabel_guru";                    
                  $in = $this->db->query($e);
                  $re  = $in->row();
                  ?>
                  <h3><?php echo $re->jum  ?> orang</h3>
                  <p>Guru</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/guru" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->
            
            <!--div class="col-lg-3 col-xs-6">
              <div class="small-box bg-red">
                <?php 
                  $f  = "SELECT COUNT(sentitems.ID) as jum FROM sentitems";                    
                  $in = $this->db->query($f);
                  $re  = $in->row();
                  ?>
                <div class="inner">
                  <h3><?php echo $re->jum ?> SMS</h3>
                  <p>Terkirim</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/gateway/sent" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div--><!-- ./col -->

            <!--div class="col-lg-3 col-xs-6">
              <div class="small-box bg-aqua">
                <?php 
                  $f  = "SELECT COUNT(outbox.ID) as jum FROM outbox";                    
                  $in = $this->db->query($f);
                  $re  = $in->row();
                  ?>
                <div class="inner">
                  <h3><?php echo $re->jum ?> SMS</h3>
                  <p>Tidak Terkirim</p>
                </div>
                <div class="icon">
                  <i class="fa fa-envelope"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/gateway/draft" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->

            <!--div class="col-lg-3 col-xs-6">
              <div class="small-box bg-blue">
                <?php 
                  $f  = "SELECT COUNT(inbox.ID) as jum FROM inbox";                    
                  $in = $this->db->query($f);
                  $re  = $in->row();
                  ?>
                <div class="inner">
                  <h3><?php echo $re->jum ?> SMS</h3>
                  <p>Masuk</p>
                </div>
                <div class="icon">
                  <i class="fa fa-envelope"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/gateway/inbox" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->

            <!--div class="col-lg-3 col-xs-6">
              <div class="small-box bg-green">
                <?php 
                // jalankan perintah cek pulsa via gammu
                $d = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
                exec("c:\C:\xampp\htdocs\web_absence\gammu\gammu -c c:\C:\xampp\htdocs\web_absence\gammu\gammurc getussd ".$d->cek_pulsa."", $hasil);
                 
                // proses filter hasil output
                for ($i=0; $i<=count($hasil)-1; $i++)
                {
                   if (substr_count($hasil[$i], 'Service reply') > 0) $index = $i;
                }

                if($i <>  0){
                  $has = $hasil[$index];
                }else{
                  $has = $i;
                }
                 
                // menampilkan sisa pulsa
                

                  ?>
                <div class="inner">
                  <h3>Rp. <?php echo $has ?></h3>
                  <p>Sisa Pulsa <?php echo $d->kartu_gsm ?></p>
                </div>
                <div class="icon">
                  <i class="fa fa-money"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/gateway/draft" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <?php 
                  $f  = "SELECT COUNT(outbox.ID) as jum FROM outbox";                    
                  $in = $this->db->query($f);
                  $re  = $in->row();
                  ?>
                <div class="inner">
                  <h3>Jadwal SMS</h3>
                  <p>                  
                      <?php 
                      $dt = $this->db->query('SELECT * FROM tabel_schedule');
                      if($dt->num_rows() > 0){
                        foreach ($dt->result() as $k) {
                          if($k->status=='aktif'){
                            $t = "<i class='fa fa-check'></i>";
                          }else{
                            $t = "<i class='fa fa-minus'></i>";
                          }
                          echo "
                          $k->tipe_sms $t                
                          ";
                        }
                      }else{
                        echo "tidak ada";
                      }
                      ?>                                  
                  </p>
                </div>
                <div class="icon">
                  <i class="fa fa-upload"></i>
                </div>
                <?php
                $level = $this->session->userdata('level');
                if($level=='administrator' or $level=='super admin'){ 
                ?>
                <a href="adm/gateway/schedule" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>
              </div>
            </div><!-- ./col -->

          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <section class="col-lg-12 connectedSortable">
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-graphic"></i>
                  <h3 class="box-title">Infografis Siswa</h3>
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>                    
                  </div>
                </div>
                <div class="box-body chat" id="chat-box">
                  <div id="container" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                </div>
              </div>
            </section>
          </div>

          <!--div class="row">
            <section class="col-lg-6 connectedSortable">
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-graphic"></i>
                  <h3 class="box-title">Infografis Absensi</h3>
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>                    
                  </div>
                </div>
                <div class="box-body chat" id="chat-box">
                  <div id="container2" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                </div>
              </div>
            </section>
          
            <section class="col-lg-6 connectedSortable">
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-graphic"></i>
                  <h3 class="box-title">Infografis Absensi</h3>
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>                    
                  </div>
                </div>
                <div class="box-body chat" id="chat-box">
                  <div id="container3" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                </div>
              </div>
            </section>
          </div-->

        </div>
      </section>
    </div>

<script src="assets/panel/js_chart/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="assets/panel/js_chart/highcharts.js" type="text/javascript"></script>
<script src="assets/panel/js_chart/exporting.js" type="text/javascript"></script>
<script type="text/javascript">
    var chart1; // globally available
$(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container',
            type: 'column'
         },   
         title: {
            text: 'Jumlah siswa aktif'
         },
         xAxis: {
            categories: ['Kelas']
         },
         yAxis: {
            title: {
               text: ''
            }
         },
              series:             
            [
    <?php     
    $level = $this->session->userdata('level');      
      $sql   = "SELECT tabel_jenjang.jenjang,tabel_kelas.kelas, COUNT(tabel_siswakelas.id_siswa) AS jumlah
        FROM tabel_siswakelas RIGHT JOIN tabel_siswa
        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
        INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas=tabel_kelas.id_kelas              
        INNER JOIN tabel_jenjang ON tabel_kelas.id_jenjang=tabel_jenjang.id_jenjang
        GROUP BY tabel_kelas.id_kelas ORDER BY tabel_kelas.id_kelas ASC";
    $cek = $this->db->query($sql);
    foreach ($cek->result() as $r) {    
        $kelas=$r->jenjang." ".$r->kelas;
        $jumlah=$r->jumlah;                    
        ?>
        {
          name: '<?php echo $kelas; ?>',
          data: [<?php echo $jumlah; ?>]
        },
        <?php } ?>
]
});
}); 
</script>
<script type="text/javascript">
    var chart1; // globally available
$(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container2',
            type: 'column'
         },   
         title: {
            <?php $dat = date("d-m-Y"); ?>
            text: 'Grafik Kehadiran <?php echo $dat ?>'
         },
         xAxis: {
            categories: ['Kelas']
         },
         yAxis: {
            title: {
               text: ''
            }
         },
              series:             
            [
    <?php     
    $level = $this->session->userdata('level');      
      $sql   = "SELECT tabel_jenjang.jenjang,tabel_kelas.kelas, COUNT(tabel_absen.id_penempatan) AS jumlah
        FROM tabel_siswakelas RIGHT JOIN tabel_siswa
        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
        INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas=tabel_kelas.id_kelas
        INNER JOIN tabel_jenjang ON tabel_kelas.id_jenjang=tabel_jenjang.id_jenjang
        LEFT JOIN tabel_absen ON tabel_siswakelas.id_penempatan=tabel_absen.id_penempatan
        WHERE tabel_absen.tgl='$dat' AND tabel_absen.absen_masuk='hadir'             
        GROUP BY tabel_kelas.id_kelas ORDER BY tabel_kelas.id_kelas ASC";
    $cek = $this->db->query($sql);
    foreach ($cek->result() as $r) {    
        $kelas=$r->jenjang." ".$r->kelas;
        $jumlah=$r->jumlah;                    
        ?>
        {
          name: '<?php echo $kelas; ?>',
          data: [<?php echo $jumlah; ?>]
        },
        <?php } ?>
]
});
}); 
</script>
<script type="text/javascript">
    var chart1; // globally available
$(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container3',
            type: 'column'
         },   
         title: {
            <?php $dat = date("d-m-Y"); ?>
            text: 'Grafik Ketidakhadiran <?php echo $dat ?>'
         },
         xAxis: {
            categories: ['Kelas']
         },
         yAxis: {
            title: {
               text: ''
            }
         },
              series:             
            [
    <?php     
    $level = $this->session->userdata('level');      
      $sql   = "SELECT tabel_jenjang.jenjang,tabel_kelas.kelas, COUNT(tabel_absen.id_penempatan) AS jumlah
        FROM tabel_siswakelas RIGHT JOIN tabel_siswa
        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa)
        INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas=tabel_kelas.id_kelas
        INNER JOIN tabel_jenjang ON tabel_kelas.id_jenjang=tabel_jenjang.id_jenjang
        LEFT JOIN tabel_absen ON tabel_siswakelas.id_penempatan=tabel_absen.id_penempatan
        WHERE tabel_absen.tgl='$dat' AND tabel_absen.absen_masuk<>'hadir'             
        GROUP BY tabel_kelas.id_kelas ORDER BY tabel_kelas.id_kelas ASC";
    $cek = $this->db->query($sql);
    foreach ($cek->result() as $r) {    
        $kelas=$r->jenjang." ".$r->kelas;
        $jumlah=$r->jumlah;                    
        ?>
        {
          name: '<?php echo $kelas; ?>',
          data: [<?php echo $jumlah; ?>]
        },
        <?php } ?>
]
});
}); 
</script>