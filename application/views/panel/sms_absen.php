<?php 
function tanggal(){
  $hari=$day=date("l");
  $tanggal=date("d");
  $bulan=$bl=$month=date("m");
  $tahun=date("Y");
  $jam=date("H");
  $menit=date("i");
  $detik=date("s");

  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }

  switch($bulan)
  {
    case"1":$bulan="Januari"; break;
    case"2":$bulan="Februari"; break;
    case"3":$bulan="Maret"; break;
    case"4":$bulan="April"; break;
    case"5":$bulan="Mei"; break;
    case"6":$bulan="Juni"; break;
    case"7":$bulan="Juli"; break;
    case"8":$bulan="Agustus"; break;
    case"9":$bulan="September"; break;
    case"10":$bulan="Oktober"; break;
    case"11":$bulan="November"; break;
    case"12":$bulan="Desember"; break;
  }

  $tglLengkap="$hari, $tanggal $bulan $tahun";
  return $tglLengkap;
}
?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">    
    <?php
    if($set='set_sms'){
      $id_jenjang   = $this->input->get('id_jenjang');
      $id_kelas     = $this->input->get('id_kelas');
      $tgl_awal     = $this->input->get('tgl_awal');
      $tgl_akhir    = $this->input->get('tgl_akhir');
      $status       = $this->input->get('status');      
      $id           = $this->input->get('id');
    ?>
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">              
              <?php
              echo "
              <a href='adm/absen/filter?id_jenjang=$id_jenjang&id_kelas=$id_kelas&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' type='button' class='btn bg-maroon btn-flat margin'><i class='fa fa-chevron-left'></i> Kembali</a>          
              ";
              ?>
              <a onclick="return confirm('Anda yakin ingin melanjutkan?')" <?php echo "href='adm/absen/export_kirim?id=$id&id_jenjang=$id_jenjang&id_kelas=$id_kelas&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' class='btn btn-warning btn-flat margin'><i class='fa fa-file-excel-o'></i> Export Data</a>        
              ";
              ?>
              <a onclick="return confirm('Anda yakin ingin melanjutkan?')" <?php echo "href='adm/absen/sms_kirim?id=$id&id_jenjang=$id_jenjang&id_kelas=$id_kelas&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&status=$status' class='btn btn-info btn-flat margin'><i class='fa fa-envelope'></i> Kirim Pesan</a>        
              ";
              ?>              
            </h3>                                      
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          
           
          <div class="box-body">
            
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>
                  <th width="5%">No.Induk</th>                        
                  <th width="15%">Nama Lengkap</th>                    
                  <th width="15%">No.HP</th>   
                  <th>Pesan</th> 
                  <th width="5%">Char</th>                        
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1;               
              foreach($dt_wali_kelas->result() as $row) {                           
                $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '$id'")->row();              
                if($id == 2){
                  if($row->absen_pulang == 'hadir'){
                    $find     = array("nama_i","nis_i","tgl_i","jam_i");
                    $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_pulang);
                    $isi      = str_replace($find, $replace, $r->isi);
                  }else{
                    $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
                    $find     = array("nama_i","nis_i","tgl_i","absen_i");
                    $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->absen_masuk);
                    $isi      = str_replace($find, $replace, $as->isi);
                  }                  
                }elseif($id == 1){
                  if($row->absen_pulang == 'hadir'){
                    $find     = array("nama_i","nis_i","tgl_i","jam_i");
                    $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->jam_masuk);
                    $isi      = str_replace($find, $replace, $r->isi);
                  }else{
                    $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
                    $find     = array("nama_i","nis_i","tgl_i","absen_i");
                    $replace  = array($row->nama_lengkap,$row->id_siswa,$row->tgl,$row->absen_masuk);
                    $isi      = str_replace($find, $replace, $as->isi);
                  }
                }elseif($id == 3){
                  $hadir    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                              WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                              AND absen_masuk = 'hadir' AND valid_masuk = 'valid'")->row();
                  $alpa     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                              WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                              AND absen_masuk = 'alpha' AND valid_masuk = 'valid'")->row();
                  $izin     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                              WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                              AND absen_masuk = 'izin' AND valid_masuk = 'valid'")->row();
                  $sakit    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                              WHERE id_penempatan = '$row->id_penempatan' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                              AND absen_masuk = 'sakit' AND valid_masuk = 'valid'")->row();
                  $find     = array("nama_i","nis_i","tgl1_i","tgl2_i","hadir_i","alpa_i","izin_i","sakit_i");
                  $replace  = array($row->nama_lengkap,$row->id_siswa,$tgl_awal,$tgl_akhir,$hadir->jum,$alpa->jum,$izin->jum,$sakit->jum);
                  $isi      = str_replace($find, $replace, $r->isi);
                }
                $jum_c      = strlen($isi);

                echo "          
                  <tr>
                    <td>$no</td>
                    <td>$row->id_siswa</td>      
                    <td>$row->nama_lengkap</td>                     
                    <td>$row->no_hp</td>
                    <td>$isi</td>
                    <td>$jum_c kata</td>                                           
                    "; ?>                                 
                  </tr>
                <?php
                $no++;
              }
              ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>
    
    <?php  
    }
    ?>
  </section>
</div>


