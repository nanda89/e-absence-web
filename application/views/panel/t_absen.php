<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">          
            <?php 
              $id_tahun = $_GET['id_tahun'];
              $id_kelas = $_GET['id_kelas'];              
              $bulan = $_GET['bulan'];
              $tahun = $_GET['tahun'];                
              $tanggal = $_GET['tanggal'];
                                          
              ?>
              <a href="adm/laporan/absensi">
                <button class="btn btn-sm bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
              </a>
              <?php
              if($dt_wali_kelas=='nihil'){
                echo "<script>alert('Pilih parameter yang benar');</script>";
                echo "<meta http-equiv='refresh' content='0; url=".base_url()."adm/laporan/absensi'>";
              }else{
                $r = $dt_wali_kelas->num_rows();   
              }
              
              if($r>0){
              echo "
              <a href='adm/laporan/absensi_pdf?id_tahun=$id_tahun&id_kelas=$id_kelas&tanggal=$tanggal&bulan=$bulan&tahun=$tahun'>
                <button class='btn btn-sm bg-primary btn-flat margin'><i class='fa fa-file-pdf-o'></i> Export As PDF</button>
              </a>
              
              <a href='adm/laporan/absensi_excel?id_tahun=$id_tahun&id_kelas=$id_kelas&tanggal=$tanggal&bulan=$bulan&tahun=$tahun'>
                <button class='btn btn-sm btn-warning btn-flat margin'><i class='fa fa-file-excel-o'></i> Export As Excel</button>
              </a>
              ";
              } ?>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="box-body with-border">
        </div>
        <?php    
            if($dt_wali_kelas->num_rows()>0){
                $r = $dt_wali_kelas->row();
                //$id_jenjang = $r->id_jenjang;   
        ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%">No</th>      
              <th width="5%">No.Induk</th>      
              <th>Nama Lengkap</th>
              <th>Jenjang</th>  
              <th>Kelas</th>   
              <th>Tanggal</th>       
              <th width="15%">Jam Absen Masuk</th>
              <th width="15%">Jam Absen Pulang</th>                      
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_wali_kelas->result() as $row) { 
          $tanggal = date("d F Y", strtotime($row->tgl));
          $tgl = date('d-m-Y');
          $s = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$row->tgl'";
          $d = $this->db->query($s);
          $ambil = $d->row();
          if($d->num_rows()>0){
            if($ambil->absen_masuk==''){
              $absen_masuk = "-";
              $jam_masuk = '';  
            }else{
              $absen_m = $ambil->absen_masuk; 
              if($absen_m=='alpha'){
                $absen_masuk = "<font color='red'>$absen_m</font>";
              }elseif($absen_m=='izin'){
                $absen_masuk = "<font color='blue'>$absen_m</font>";
              }elseif($absen_m=='sakit'){
                $absen_masuk = "<font color='green'>$absen_m</font>";
              }elseif($absen_m=='hadir'){
                $absen_masuk = "<font color='black'>$absen_m</font>";
              }
              $jam_masuk = $ambil->jam_masuk." | ";  
            }

            if($ambil->absen_pulang==''){
              $absen_pulang = "-";  
              $jam_pulang = '';
            }else{
              $absen_p = $ambil->absen_pulang; 
              if($absen_p=='alpha'){
                $absen_pulang = "<font color='red'>$absen_p</font>";
              }elseif($absen_p=='izin'){
                $absen_pulang = "<font color='blue'>$absen_p</font>";
              }elseif($absen_p=='sakit'){
                $absen_pulang = "<font color='green'>$absen_p</font>";
              }elseif($absen_p=='hadir'){
                $absen_pulang = "<font color='black'>$absen_p</font>";
              } 
              $jam_pulang = $ambil->jam_pulang." | "; 
            }
          }else{
            $absen_pulang = "-";
            $absen_masuk = "-";
            $jam_masuk = '';
            $jam_pulang = '';
          }

         
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->id_siswa</td>      
              <td>$row->nama_lengkap</td>  
              <td>$row->jenjang</td>  
              <td>$row->kelas</td>
              <td>$tanggal</td>                         
              <td>$jam_masuk$absen_masuk</td>
              <td>$jam_pulang$absen_pulang</td>
              "; ?>                                 
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
 
<?php 
}else{
    echo "<b>Data tidak ditemukan</b>";
} ?>

   </div><!-- /.box -->    
  </section>
</div>