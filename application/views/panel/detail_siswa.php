<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="detail"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>            
          </a>          
        </h3>                
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      <?php 
      $row = $one_siswa->row();       
      ?>
      </div><!-- /.box-header -->
      <input type="hidden" id="kode_siswa" value="<?php $row->id_siswa ?>">
      <div class="box-body">
        <div class="row">
          <form class="form-horizontal" action="adm/siswa/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_siswa; ?>" name="id">
          <div class="col-md-12">            
              <div class="box-body">
                <table width="100%" border="0">
                  <tr>
                    <td width="20%">
                      <label class="control-label">NISN</label>
                    </td>
                    <td>                      
                      <?php echo $row->nisn; ?>
                    </td>
                    <td width="20%" align="right" rowspan="10">
                      <?php 
                      if($row->gambar==''){
                        if($row->jenis_kelamin=='Laki-laki' or $row->jenis_kelamin=='laki-laki'){
                          $gambar = 'admin-lk.png'; 
                        }else{
                          $gambar = 'admin-pr.png';
                        }                        
                      }else{
                        $gambar = $row->gambar;
                      }
                      ?>
                      <img src="assets/panel/images/<?php echo $gambar; ?>" width="200px">
                    </td>                    
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Nama</label>
                    </td>
                    <?php 
                    $r = $this->m_jenjang->get_one($row->id_jenjang);
                    $r_je = $r->row();
                    $n = $r_je->jenjang;
                    ?>
                    <td><?php echo $row->nama_lengkap; ?> [ <?php echo $n; ?> ]</td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">No.Induk</label>
                    </td>
                    <td><?php echo $row->id_siswa; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Jenis Kelamin</label>
                    </td>
                    <td><?php echo $row->jenis_kelamin; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tempat, Tgl.Lahir</label>
                    </td>
                    <td><?php echo $row->tempat_lahir." , ".$row->tgl_lahir; ?></td>
                  </tr>                  
                  <tr>
                    <td>
                      <label class="control-label">Agama</label>
                    </td>
                    <td><?php echo $row->agama; ?></td>
                  </tr>                                                      
                  <tr>
                    <td>
                      <label class="control-label">No.HP</label>
                    </td>
                    <td><?php echo $row->no_hp; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Email</label>
                    </td>
                    <td><?php echo $row->email; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Alamat Lengkap</label>
                    </td>
                    <td><?php echo $row->alamat; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Asal Sekolah</label>
                    </td>
                    <td><?php echo $row->asal_sekolah; ?></td>
                  </tr>                  
                  <tr>
                    <td>
                      <label class="control-label">Keterangan</label>
                    </td>
                    <td><?php echo $row->ket_asal; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Nama Ayah</label>
                    </td>
                    <td><?php echo $row->nama_ayah; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Nama Ibu</label>
                    </td>
                    <td><?php echo $row->nama_ibu; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tgl.Daftar</label>
                    </td>
                    <td><?php echo $row->tgl_daftar; ?></td>
                  </tr>
                                     
                </table>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="ubah" class="btn btn-info">Edit</button>
                <a href="adm/guru">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>                
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>


<script type="text/javascript">
window.onload=cek_jenjang();
function cek_jenjang(){
  var jenjang_js=document.getElem;entById("jen").value;
  if(jenjang_js=="1"){
    $("#no_seri_ijazah").hide();
    $("#no_ujian_nasional").hide();
    $("#no_seri_skhun").hide();
    $("#bank").show();
    $("#wn").show();
  }else if(jenjang_js=="2" || jenjang_js=="3" || jenjang_js=="4"){
    $("#no_seri_ijazah").show();
    $("#no_ujian_nasional").show();
    $("#no_seri_skhun").show();
    $("#bank").hide();
    $("#wn").hide();
  }
}
function jenjang(){
  var jenjang_js=$("#jen").val();
  if(jenjang_js=="1"){
    $("#no_seri_ijazah").hide();
    $("#no_ujian_nasional").hide();
    $("#no_seri_skhun").hide();
    $("#bank").show();
    $("#wn").show();
  }else if(jenjang_js=="2" || jenjang_js=="3" || jenjang_js=="4"){
    $("#no_seri_ijazah").show();
    $("#no_ujian_nasional").show();
    $("#no_seri_skhun").show();
    $("#bank").hide();
    $("#wn").hide();
  }
}
function auto(){
  var jenjang_js=document.getElementById("jen").value;
  if (jenjang_js=="") {    
    alert("Pilih Jenjang Terlebih Dahulu...!");
    return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/siswa/cari_id')?>",
        type:"POST",
        data:"jenjang="+jenjang_js,   
        cache:false,   
        success: function(msg){ 
          data=msg.split("|");
          $("#kode_siswa").val(data[0]);                        
        }        
    })
  }
}
function hide_prestasi(){
    $("#tampil_prestasi").hide();
}
function kirim_data_prestasi(){    
  $("#tampil_prestasi").show();
  var id_siswa = document.getElementById("kode_siswa").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_siswa="+id_siswa;                           
     xhr.open("POST", "adm/siswa/t_prestasi", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_prestasi").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
function simpan_prestasi(){
    var id_siswa            = document.getElementById("kode_siswa").value;   
    var jenis_prestasi_js   = $("#jenis_prestasi").val();
    var tingkat_js          = $("#tingkat").val();
    var nama_prestasi_js    = $("#nama_prestasi").val();    
    var tahun_js            = $("#tahun").val();    
    var penyelenggara_js    = $("#penyelenggara").val();        
    if (id_siswa=="" || jenis_prestasi_js=="" || nama_prestasi_js == "") {    
        alert("Isikan data dengan lengkap, pastikan No.Induk terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/siswa/save_prestasi')?>",
            type:"POST",
            data:"id_siswa="+id_siswa+"&jenis_prestasi="+jenis_prestasi_js+"&tingkat="+tingkat_js+"&nama_prestasi="+nama_prestasi_js+"&tahun="+tahun_js+"&penyelenggara="+penyelenggara_js,
            cache:false,
            success:function(msg){            
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_prestasi();
                    kosong();                
                }else{
                    kirim_data_prestasi();
                    kosong();                      
                }                
            }
        })    
    }
}
function kosong(args){
  $("#jenis_prestasi").val("");
  $("#tingkat").val("");
  $("#nama_prestasi").val("");    
  $("#tahun").val("");    
  $("#penyelenggara").val(""); 
}
function hapus_prestasi(a,b){ 
    var id_prestasi_js  = a;   
    var id_siswa_js     = b;       
    $.ajax({
        url : "<?php echo site_url('adm/siswa/delete_prestasi')?>",
        type:"POST",
        data:"id_prestasi="+id_prestasi_js+"&id_siswa="+id_siswa_js,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_prestasi();
            }
        }
    })
}


function hide_beasiswa(){
    $("#tampil_beasiswa").hide();
}
function kirim_data_beasiswa(){    
  $("#tampil_beasiswa").show();
  var id_siswa = document.getElementById("kode_siswa").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_siswa="+id_siswa;                           
     xhr.open("POST", "adm/siswa/t_beasiswa", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_beasiswa").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
function simpan_beasiswa(){
    var id_siswa            = document.getElementById("kode_siswa").value;   
    var jenis_beasiswa_js   = $("#jenis_beasiswa").val();
    var sumber_js           = $("#sumber").val();
    var tahun_mulai_js      = $("#tahun_mulai").val();    
    var tahun_selesai_js    = $("#tahun_selesai").val();        
    if (id_siswa=="" || jenis_beasiswa_js=="" || sumber_js == "") {    
        alert("Isikan data dengan lengkap, pastikan No.Induk terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/siswa/save_beasiswa')?>",
            type:"POST",
            data:"id_siswa="+id_siswa+"&jenis_beasiswa="+jenis_beasiswa_js+"&sumber="+sumber_js+"&tahun_mulai="+tahun_mulai_js+"&tahun_selesai="+tahun_selesai_js,
            cache:false,
            success:function(msg){            
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_beasiswa();
                    kosong();                
                }else{
                    kirim_data_beasiswa();
                    kosong();                      
                }                
            }
        })    
    }
}
function kosong(args){
  $("#jenis_beasiswa").val("");
  $("#sumber").val("");
  $("#tahun_mulai").val("");    
  $("#tahun_selesai").val("");      
}
function hapus_beasiswa(a,b){ 
    var id_beasiswa_js  = a;   
    var id_siswa_js     = b;       
    $.ajax({
        url : "<?php echo site_url('adm/siswa/delete_beasiswa')?>",
        type:"POST",
        data:"id_beasiswa="+id_beasiswa_js+"&id_siswa="+id_siswa_js,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_beasiswa();
            }
        }
    })
}
</script>