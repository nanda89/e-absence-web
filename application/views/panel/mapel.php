<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "view") {
    ?>
    <div class="row">
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" data-toggle="modal"
                onclick="add_kat()">
                <i class="fa fa-plus"></i> Tambah Data Kategori Mapel</button>
              </a>
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
            <table id="" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Ketegori Mapel</th>
                  <th width="33%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
$no = 1;
    foreach ($dt_kat_mapel->result() as $row) {
        $id = $row->id_kat_mapel;
        echo "
                <tr>
                  <td>$no</td>
                  <td>$row->kategori_mapel</td>
                  <td>";
        ?>
                  <form method="post" action="adm/mapel/process">
                    <input type="hidden" name="id" value="<?php echo $row->id_kat_mapel ?>" />
                    <button onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                      <a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-primary btn-flat" data-toggle="tooltip modal"
                    onclick="edit_kat(<?php echo $id ?>)"><i class='fa fa-edit'></i></a>
                  </td>
                  </form>
                </tr>
              <?php
$no++;
    }
    ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>

      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" data-toggle="modal"
                onclick="add_mapel()">
                <i class="fa fa-plus"></i> Tambah Data Mata Pelajaran</button>
              </a>
              <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>

            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <?php
if (isset($_SESSION['pesan2']) && $_SESSION['pesan2'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe2'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan2'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan2'] = '';

    ?>
            <table id="example3" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%"><input type="checkbox" id="check-all"></th>
                  <th width="5%">No</th>
                  <th>Jenjang</th>
                  <th>Kategori</th>
                  <th>Mapel</th>
                  <th>KKM</th>
                  <th>Jenis</th>
                  <th width="16%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
$no = 1;
    foreach ($dt_mapel->result() as $row) {
        $id = $row->id_mapel;
        echo "
                <tr>
                  <td><input type='checkbox' class='data-check' value='$row->id_mapel'></td>
                  <td>$no</td>
                  <td>$row->jenjang</td>
                  <td>$row->kategori_mapel</td>
                  <td>$row->mapel</td>
                  <td>$row->kkm</td>
                  <td>$row->jenis</td>
                  <td>";
        ?>
                  <form method="post" action="adm/mapel/process_mapel">
                    <input type="hidden" name="id" value="<?php echo $row->id_mapel ?>" />
                    <button onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                      <a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-primary btn-flat" data-toggle="tooltip modal"
                    onclick="edit_mapel(<?php echo $id ?>)"><i class='fa fa-edit'></i></a>
                  </td>
                  </form>
                  </td>
                </tr>
              <?php
$no++;
    }
    ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>

    </div>

    <?php
}
?>
  </section>
</div>


<div class="modal fade" id="tambah_kat">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Kategori Mapel</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/mapel/save" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kategori Mapel</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Kategori Mapel" name="kategori_mapel" required>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
              <a href="adm/mapel">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_kat">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Kategori Mapel</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/mapel/process" method="post" enctype="multipart/form-data">
            <input type="hidden" class="form-control" id="inputEmail3" name="id">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kategori Mapel</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Kategori Mapel" name="kategori_mapel" required>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
              <a href="adm/mapel">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_mapel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/mapel/save_mapel" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-10">
                  <select name="id_jenjang" class="form-control">
                    <?php
foreach ($dt_jenjang->result() as $dt_jenjang) {
    echo "
                    <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Mata Pelajaran" name="mapel" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
                <div class="col-sm-10">
                  <select name="id_kat_mapel" class="form-control">
                    <?php
foreach ($dt_kat_mapel->result() as $dt_kat_mapel) {
    echo "
                    <option value='$dt_kat_mapel->id_kat_mapel'>$dt_kat_mapel->kategori_mapel</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">KKM</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" placeholder="KKM" name="kkm" required>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
              <a href="adm/mapel">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_mapel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Data Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/mapel/process_mapel" method="post" enctype="multipart/form-data">
            <input type="hidden" class="form-control" id="inputEmail3" name="id_mapel">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-10">
                  <select name="id_jenjang" class="form-control">
                    <?php
foreach ($dt_jenjang2->result() as $dt_jenjang) {
    echo "
                    <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Mata Pelajaran" name="mapel" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
                <div class="col-sm-10">
                  <select name="id_kat_mapel" class="form-control">
                    <?php
foreach ($dt_kat->result() as $dt_kat) {
    echo "
                    <option value='$dt_kat->id_kat_mapel'>$dt_kat->kategori_mapel</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">KKM</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" placeholder="KKM" name="kkm" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                <div class="col-sm-10">
                  <select name="id_jenis_mapel" class="form-control">
                    <option value="reguler">Reguler</option>
                    <option value="tambahan">Tambahan</option>
                  </select>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="s_process" value="edit" class="btn btn-info">Save</button>
              <a href="adm/mapel">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function edit_kat(id){
  //Ajax Load data from ajax
  $.ajax({
      url : "<?php echo site_url('adm/mapel/cari') ?>",
      type:"POST",
      data:"id="+id,
      success: function(msg)
      {
          data=msg.split("|");
          $('[name="id"]').val(data[0]);
          $('[name="kategori_mapel"]').val(data[1]);
          $('[name="kategori_mapel"]').autofocus;
          $('#modal_kat').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit Kategori Mapel'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}
function edit_mapel(id){
  //Ajax Load data from ajax
  $.ajax({
      url : "<?php echo site_url('adm/mapel/cari_mapel') ?>",
      type:"POST",
      data:"id="+id,
      success: function(msg)
      {
          data=msg.split("|");
          $('[name="id_mapel"]').val(data[0]);
          $('[name="mapel"]').val(data[1]);
          $('[name="id_kat_mapel"]').val(data[2]);
          $('[name="kkm"]').val(data[3]);
          $('[name="id_jenjang"]').val(data[4]);
          $('[name="mapel"]').autofocus;
          $('[name="id_jenis_mapel"]').val();
          $('#modal_mapel').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit Mata Pelajaran'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}
function add_kat(){
  $('[name="id"]').val('');
  $('[name="kategori_mapel"]').val('');
  $('[name="kategori_mapel"]').autofocus;
  $('#tambah_kat').modal('show'); // show bootstrap modal when complete loaded
  $('.modal-title').text('Tambah Kategori Mapel'); // Set title to Bootstrap modal title
}
function add_mapel(){
  $('[name="mapel"]').val('');
  $('[name="mapel"]').autofocus;
  $('[name="id_kat_mapel"]').val('');
  $('[name="kkm"]').val('');
  $('[name="id_jenjang"]').val('');
  $('#tambah_mapel').modal('show'); // show bootstrap modal when complete loaded
  $('.modal-title').text('Tambah Mata Pelajaran'); // Set title to Bootstrap modal title
}
</script>




<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/mapel/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            window . location . reload();

          },
          error: function (jqXHR, textStatus, errorThrown){
            // alert('Error deleting data');
            window . location . reload();

          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>