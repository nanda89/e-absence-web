<?php date_default_timezone_set("Asia/Jakarta");?>
<?php 
$d = date('dms'); 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table border="1" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="1%">No</th>      
      <th width="5%">NISN</th>      
      <th width="5%">No.Induk</th>      
      <th>Nama Lengkap</th>
      <th>Mata Pelajaran</th>                                     
      <th>Kelas</th>   
      <th>Tanggal</th>       
      <th width="15%">Jam Absen</th>                  
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1; 
  foreach($dt_wali_kelas->result() as $row) { 
  $tanggal = date("d F Y", strtotime($row->tgl));
  $tgl = date('d-m-Y');
  $s = "SELECT * FROM tabel_absen_mapel RIGHT JOIN tabel_siswakelas 
        ON tabel_absen_mapel.id_penempatan = tabel_siswakelas.id_penempatan LEFT JOIN tabel_jadwal
        ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal INNER JOIN tabel_mapel
        ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
        WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen_mapel.tgl = '$row->tgl'
        AND tabel_absen_mapel.valid='valid'";
  $d = $this->db->query($s);
  $ambil = $d->row();
  if($d->num_rows()>0){
    if($ambil->absen==''){
      $absen = "-";
      $jam = '';  
    }else{
      $absen_m = $ambil->absen; 
      if($absen_m=='alpha'){
        $absen = "<span class='label label-danger'>$absen_m</span>";
      }elseif($absen_m=='izin'){
        $absen = "<span class='label label-warning'>$absen_m</span>";
      }elseif($absen_m=='sakit'){
        $absen = "<font class='label label-primary'>$absen_m</span>";
      }elseif($absen_m=='hadir'){
        $absen = "<font class='label label-success'>$absen_m</span>";
      }
      $jam = $ambil->jam." ";  
    }
  }else{                
    $absen_masuk = "-";
    $jam_masuk = '';                
  }

  echo "          
    <tr>
      <td>$no</td>
      <td>$row->nisn</td>      
      <td>$row->id_siswa</td>      
      <td>$row->nama_lengkap</td>  
      <td>$ambil->mapel</td>                    
      <td>$row->jenjang $row->kelas</td>
      <td>$tanggal</td>                         
      <td>$jam$absen</td>                  
      "; ?>                                 
    </tr>
  <?php
  $no++;
  }
  ?>
  </tbody>
  </table>