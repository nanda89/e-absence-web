<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
     
    <div class="box">      
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa">
            <button type='button' class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>          
          </a>          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      
      <div class="box-body">       
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>No.Induk</th>
              <th>NISN</th>
              <th>Nama Lengkap</th>              
              <th>No.Hp</th>
              <th>Email</th>
              <th>Alamat</th>              
              <th width="12%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_ta->result() as $row) {       
            if($row->jenis_kelamin=='laki-laki'){
              $j = "lk";
            }else{
              $j = "pr";
            }
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->id_siswa</td>
              <td>$row->nisn</td>
              <td>$row->nama_lengkap ($j)</td>              
              <td>$row->no_hp</td>
              <td>$row->email</td>
              <td>$row->alamat</td>              
              <td>";
              ?>
              <form method="post" action="adm/siswa/process">
                <input type="hidden" name="id" value="<?php echo $row->id_siswa ?>" />
                <input type="hidden" name="nisn" value="<?php echo $row->nisn ?>" />
                <input type="hidden" name="nama" value="<?php echo $row->nama_lengkap ?>" />
                <input type="hidden" name="jk" value="<?php echo $row->jenis_kelamin ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>                                
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
</div>

<script type="text/javascript">
function  back(){
  history.go(-1);
}
</script>