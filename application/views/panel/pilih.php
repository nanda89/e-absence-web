<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">                                    
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->          
          <div class="box-body with-border">
            <h4>Pilih Jenjang terlebih dulu untuk memulai proses penempatan kelas.</h4> <br>
            <form class="form-horizontal" action="adm/penempatan/set_jenjang" method="get" enctype="multipart/form-data">          
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-1 control-label">Jenjang</label>
                <div class="col-sm-11">
                  <select name="id_jenjang" class="form-control">                    
                    <option value=''>Pilih Jenjang</option>                    
                    <?php 
                    foreach($dt_jenjang->result() as $row) {                           
                    echo "
                    <option value='$row->id_jenjang'>$row->jenjang</option>";
                    } ?>
                  </select>
                </div>                           
              </div>
                          
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-1 control-label"></label>
                <div class="col-sm-2">
                  <button type="submit" class="btn bg-maroon btn-flat margin">Mulai Proses Penempatan <i class="fa fa-chevron-right"></i></button>              
                </div>             
              </div>  
            </form>  
          </div>          
        </div><!-- /.box -->        
      </div>     

    </div>

    <?php
    }
    ?>
  </section>
</div>


