<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?> 
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php 
              $id_ta = $_GET['id_tahun'];
              $id_kelas = $_GET['id_kelas'];
              $id_jenjang = $_GET['id_jenjang'];              
              ?>
              <a href="adm/pendataan">
                <button class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
              </a>
              <?php
              $r = $dt_penempatan->num_rows(); 
              if($r>0){
              echo "
                            
              <a href='adm/pendataan/excel_siswa_filter?id_ta=$id_ta&id_kelas=$id_kelas'>
                <button class='btn btn-warning btn-flat margin'><i class='fa fa-file-excel-o'></i> Export Filter Data</button>
              </a>
              <a href='adm/pendataan/import_filter?id_jenjang=$id_jenjang&id_tahun=$id_ta&id_kelas=$id_kelas' class='btn btn-primary btn-flat margin'><i class='fa fa-upload'></i> Import Filter Data</a>
              ";
              } ?>              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->        
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="2%">No</th>      
                  <th>No.Induk</th>      
                  <th>NISN</th>
                  <th>Nama Lengkap</th>            
                  <th>Jenjang</th>
                  <th>Tingkat/Kelas</th>                      
                  <th width="5%">Aksi</th>
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_penempatan->result() as $row) {       
              echo "          
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>      
                  <td>$row->nisn</td>      
                  <td>$row->nama_lengkap</td>              
                  <td>$row->jenjang</td>      
                  <td>$row->kelas</td>                            
                  <td>"; ?>
                  <form method="post" action="adm/siswa/process">
                    <input type="hidden" name="id" value="<?php echo $row->id_siswa ?>" >
                    <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>
                  </form>
                  </td>
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>

          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>
    </div>

    <?php 
    }elseif($set=="import"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <?php 
          $id_ta = $_GET['id_tahun'];
          $id_kelas = $_GET['id_kelas'];
          $id_jenjang = $_GET['id_jenjang'];
          $tingkat = $_GET['tingkat'];
          echo "
          <a href='adm/pendataan/filter?id_jenjang=$id_jenjang&id_tahun=$id_ta&tingkat=$tingkat&id_kelas=$id_kelas'>
            <button class='btn bg-maroon btn-flat margin'><i class='fa fa-chevron-left'></i> Kembali</button>
          </a>";
          ?>          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/pendataan/importcsv" method="post" enctype="multipart/form-data">              
              <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Browse File CSV</label>
                    <div class="col-sm-4">
                      <input type="file" name="userfile">
                    </div>
                  </div>                                  
              </div>                            
              <div class="box-footer">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-10">
                    <button type="submit" name="submit"  value="UPLOAD" class="btn btn-info btn-flat"><i class="fa fa-upload"></i> Upload</button>                    
                  </div>               
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }
    ?>
  </section>
</div>


