<?php date_default_timezone_set("Asia/Jakarta"); ?>
<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=excel_jadwal.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
  body {
    width: 8.5in;
    height: 13.0in;
    font-family: arial;
    margin: 0mm 0mm 0mm 0mm;
  }

  @page {
    size: 8.5in 13.0in
  }

  .table {
    float: left;
    margin-left: 0px;
    border-collapse: collapse;
  }

  .table2 {
    float: left;
    margin-left: 5px;
    border-collapse: collapse;
  }

  #tabel-absen thead tr th {
    font-size: 14px;
    padding: 7px;
  }

  #tabel-absen tbody tr td {
    font-size: 12px;
  }
</style>
<?php

function helpIndoMonth($num)
{
  $monthArray = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
  if (array_key_exists($num, $monthArray)) {
    return $monthArray[$num];
  } else {
    return 'Undefined';
  }
}


?>

<?php

$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>



<?php if (file_exists('assets/panel/images/' . $sql->kop_surat)) { ?>
  <img src="<?php echo base_url() ?>assets/panel/images/<?php echo $sql->kop_surat ?>" width="550" height="100">
<?php } ?>



<table>
  <!-- 
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr>
    <td colspan="7" style="text-align: center;"><h2>LAPORAN VALIDASI ABSENSI GURU</h2></td>
  </tr>
  <tr>
    <td colspan="7" style="text-align: center;"><b>Hari, Tanggal : <?php echo $hari . ", " . $tgl ?> </b></td>
  </tr>
  <tr>
    <td colspan="7" style="text-align: center;"><b>Tahun Ajaran : <u style="text-decoration: none;"><?php echo $tahun_ajaran ?></u></td>
  </tr>
  <tr><td colspan="7">&nbsp;</td></tr>
</table>
<center> -->

  <center>
    <table style="width: 97%;text-align: center;border-top: #000 2px solid;border-bottom: #000 2px solid;">
    <tr>
          <td colspan="7" style="text-align: center;">
            <h3>DAFTAR HADIR KELAS <?php echo $kelas ?> <br>TAHUN AJARAN <?php echo $ta ?></h3>
            <!-- <h3></h3> -->
          </td>
        </tr>
    </table>

  </center>
  <table>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>

  <table >
      <tr>
        <td style="width: 15%;">MATA DIKLAT</td>
        <td style="width: 1%;"> :</td>
        <td style="width: 30%;"><?php echo $mapel ?></td>
      </tr>
      <tr>
        <td style="width: 15%;">PENGAJAR</td>
        <td style="width: 1%;"> :</td>
        <td style="width: 30%;"><?php echo $guru ?></td>
      </tr>
      <tr>
        <td style="width: 15%;">KELAS</td>
        <td style="width: 1%;"> :</td>
        <td style="width: 30%;"><?php echo $kelas ?></td>
      </tr>
    </table>

  <table>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <center>
  <?php
      echo $tabel_1;
      ?>
      <table>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <?php
      echo $tabel_2;
      ?>
    <table>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>

    <table id="example2" class="table table-bordered table-hovered" border="0" width="80%" style="margin-top: 12px;">
      <tfoot>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <td colspan="2" style="text-align: right;">&nbsp;<?php echo date("d F Y") ?></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <?php
          $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
          ?>
          <td colspan="2" style="text-align: right;">Kepala <?php echo $re->logo_besar ?></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <td colspan="2" style="text-align: right;">
            <?php
            $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
            ?>
            <u><?php echo $re->pimpinan ?></u>
          </td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <?php
          $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
          ?>
          <td colspan="2" style="text-align: right;"><?php echo $re->nik ?></td>
        </tr>
      </tfoot>
    </table>

  </center>