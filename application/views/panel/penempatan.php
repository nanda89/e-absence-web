<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?> di Jenjang <?php $t = $dt_ke->row(); echo $t->jenjang ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">            
            <a href="adm/penempatan">
              <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>              
            </a>
            <br>
              <b>Petunjuk :</b>
              <ul>                                
                <li>Tentukan kelas yag menjadi tujuan</li>
                <li>Klik tombol '>' untuk memindahkan siswa</li>
                <li>Bisa lakukan pencarian dengan keyword apapun yang berkaitan dengan 
                  data siswa [misal:nama (spasi) tahun masuk (spasi) no.induk]</li>
                <li>Siswa dengan tombol berwarna <b>merah</b> berarti telah memiliki kelas, dan <b>biru</b>
                  berarti belum memiliki kelas</li>
              </ul>
            
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>No.Induk</th>              
                  <th>NISN</th>                      
                  <th>Nama</th>                                    
                  <th width="3%">Aksi</th>
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_pen->result() as $row) {                       
                if($row->status=='aktif'){
                  $r = 'btn btn-danger btn-flat fa fa-chevron-right';
                }else{
                  $r = 'btn btn-primary btn-flat fa fa-chevron-right';
                }                                
              echo "          
                <tr>
                  <td>$no</td>                  
                  <td>$row->id_siswa</td>
                  <td>$row->nisn</td>
                  <td>$row->nama_lengkap</td>                                    
                  <td>";
                  ?>
                  <button title="Set Kelas"
                    class="<?php echo $r ?>" type="button" 
                    onClick="set_kelas('<?php echo $row->id_siswa; ?>')"></button>
                  </td>
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">          
            <input type="hidden" value="<?php echo $dt_j ?>" id="id_jenjang">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Angkatan</label>
              <div class="col-sm-4">
                <select id="angkatan" required class="form-control">                    
                  <option value=''>Pilih Angkatan</option>
                  <?php 
                  for ($i=2005; $i<=2025 ; $i++) { 
                    echo"
                    <option>$i</option>               
                    ";
                  }
                  ?>
                </select>
              </div>    
              <label for="inputEmail3" class="col-sm-2 control-label">T.Ajaran</label>
              <div class="col-sm-4">
                <select id="id_tahun" required class="form-control">                    
                  <option value=''>Pilih T.Ajaran</option>  
                  <?php 
                  foreach ($dt_ta->result() as $r) {
                    echo"
                    <option value='$r->id_ta'>$r->tahun_ajaran</option>
                    ";
                  }
                  ?>              
                </select>
              </div>        
            </div>
          
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Tingkat</label>
              <div class="col-sm-4">
                <select id="tingkat" required class="form-control">                    
                  <option value=''>Pilih Tingkat</option>
                  <?php 
                  foreach ($dt_kelas->result() as $s) {
                    echo"
                    <option>$s->tingkat</option>
                    ";
                  }
                  ?>
                </select>
              </div>     
              <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
              <div class="col-sm-4">
                <select id="id_kelas" required class="form-control">                    
                  <option value=''>Pilih Kelas</option>
                </select>
              </div>         
            </div>    
                  
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <button type="button" onclick="kirim_data_penempatan()" class="btn bg-maroon btn-flat margin"><i class="fa fa-check"></i> Set Kelas Tujuan</button>              
              </div>             
            </div>  
          </form>  
        </div> 
          <div class="box-body">
            <div id="tampil_penempatan">
            </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>

$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  var tingkat = $("#tingkat").val();
  $.ajax({
    url : "<?php echo site_url('adm/penempatan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang+"&tingkat="+tingkat,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>

<script type="text/javascript">
function hapus_penempatan(a,b){ 
  var id_penempatan     = a;     
  var id_siswa          = b;     
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan')?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan+"&id_siswa="+id_siswa,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_penempatan();
          }
      }
  })
}
function set_kelas(id){
  var id_tahun    = document.getElementById("id_tahun").value;   
  var id_kelas    = $("#id_kelas").val();
  var angkatan    = $("#angkatan").val();
  var id_siswa    = id; 

  if(id_tahun=="" || id_kelas=="" || angkatan==""){
      alert("Pastikan kelas tujuan sudah di-set...!");
      return false;
  }else{
      $.ajax({
          url : "<?php echo site_url('adm/penempatan/save')?>",
          type:"POST",
          data:"id_tahun="+id_tahun+"&angkatan="+angkatan+"&id_kelas="+id_kelas+"&id_siswa="+id_siswa,
          cache:false,
          success:function(msg){            
              data=msg.split("|");
              if(data[0]=="nihil"){
                kirim_data_penempatan();                                
              }else{
                alert("Siswa ini telah memiliki kelas");
              }                
          }
      })    
  }
}
function kirim_data_penempatan(){    
  $("#tampil_penempatan").show();
  var id_tahun    = document.getElementById("id_tahun").value;     
  var id_kelas    = document.getElementById("id_kelas").value;   
  var angkatan    = document.getElementById("angkatan").value;   
  if(id_tahun=="" || id_kelas=="" || angkatan==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&angkatan="+angkatan+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/penempatan/t_penempatan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_penempatan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>