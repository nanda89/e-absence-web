
<table class="table table-bordered responsive-utilities jambo_table bulk_action" id="jasa">
    <thead>
        <tr class="headings">                                                
            <th width="5%">No </th>            
            <th>Jenis Prestasi </th>
            <th>Tingkat </th>                                                                                                                                
            <th>Nama Prestasi </th>                                                                    
            <th>Tahun </th>                                                                    
            <th>Penyelenggara </th>
            <th width="7%">Aksi </th>                                                                                                                
        </tr>
    </thead>
    <tbody> 
    <?php 
      $no=1; 
      foreach($dt_prestasi->result() as $row) {       
      echo "          
        <tr>
          <td>$no</td>
          <td>$row->jenis_prestasi</td>
          <td>$row->tingkat</td>
          <td>$row->nama_prestasi</td>   
          <td>$row->tahun</td>   
          <td>$row->penyelenggara</td>   
          <td>"; ?>
            <button title="Hapus Data"
                class="btn btn-sm btn-danger fa fa-trash-o" type="button" 
                onClick="hapus_prestasi('<?php echo $row->id_prestasi; ?>','<?php echo $row->id_siswa; ?>')"></button>
          </td>
        </tr>
        <?php
        $no++;
        }
    ?>   
    </tbody>
</table>
