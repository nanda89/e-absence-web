<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/pendaftar/filter" method="get" enctype="multipart/form-data">          
          <div class="form-group">            
            <label for="inputEmail3" class="col-sm-2 control-label">Penerimaan</label>
            <div class="col-sm-4">
              <select id="id_daf_jenis" required name="id_daf_jenis" class="form-control">                    
                <option value=''>Pilih Proses Penerimaan</option>
                <?php 
                foreach($dt_konfigurasi ->result() as $row2) {                           
                echo "
                <option value='$row2->id_daf_jenis'>$row2->nama_proses</option>";
                } ?>
                </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Tahun Pendaftaran</label>
            <div class="col-sm-3">
              <select id="tahun" required name="tahun" class="form-control">                    
                <option value=''>Pilih Tahun Pendaftaran</option>               
                <?php 
                for ($i=2010; $i<=2050 ; $i++) { 
                  echo"
                  <option>$i</option>               
                  ";
                }
                ?>
              </select>
            </div>           
          </div> 
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Gelombang</label>
            <div class="col-sm-4">
              <select id="id_daf_jadwal" name="id_daf_jadwal" class="form-control">                    
                <option value=''>Pilih Gelombang Pendaftaran</option>                               
              </select>
            </div>
                     
          </div>

                        
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
              <button type="reset" class="btn bg-blue btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>              
            </div>             
          </div>  
        </form>  
      </div> 
      <div class="box-body">
  <table id="example2" class="table table-bordered table-hovered">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>No.Daftar</th>
        <th>Nama Lengkap</th>        
        <th>Asal Sekolah</th>      
        <th>Jalur</th>
        <th>Cabang</th>
        <th>Tgl Daftar</th>  
        <th>Status</th>                    
        <th width="20%">Aksi</th>
      </tr>
    </thead>
    <tbody>            
    <?php 
    $no=1; 
    foreach($dt_ta->result() as $row) {       
    if($row->jenis_kelamin=='laki-laki'){
      $j = "lk";
    }else{
      $j = "pr";
    }
    echo "          
      <tr>
        <td>$no</td>
        <td>$row->id_daftar</td>
        <td>$row->nama_lengkap ($j)</td>        
        <td>$row->asal_sekolah</td>        
        <td>$row->kelompok</td>
        <td>$row->nama_cabang</td>
        <td>$row->tgl_daftar</td>
        <td>$row->status</td>      
        <td>";
        ?>
        <form method="post" action="adm/pendaftar/process">
          <input type="hidden" name="id" value="<?php echo $row->id_daftar ?>" />          
          <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>          
          <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>
          <div class="btn-group">
            <button type="button" class="btn btn-sm btn-primary btn-flat">Kelulusan</button>
            <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="adm/pendaftar/lulus?v=<?php echo $row->id_daftar ?>">Lulus</a></li>
              <li><a href="adm/pendaftar/t_lulus?v=<?php echo $row->id_daftar ?>">Tidak Lulus</a></li>              
            </ul>
          </div>
        </form>
        </td>
      </tr>
    <?php
    $no++;
    }
    ?>
    </tbody>
  </table>
</div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_daf_jenis").change(function(){
  var id_daf_jenis = $("#id_daf_jenis").val();
  $.ajax({
    url : "<?php echo site_url('adm/pendaftar/get_jadwal')?>",
    type:"POST",
    data:"id_daf_jenis="+id_daf_jenis,      
    cache:false,   
    success:function(msg){            
      $("#id_daf_jadwal").html(msg);      
    }
  })  
});
</script>