<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<body onload="pilih()">
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "insert") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/siswa/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl Daftar</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo date("d-m-Y") ?>"  placeholder="Tanggal Daftar" name="tgl_daftar" id="tanggal">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                  <div class="col-sm-4">
                    <select name="id_jenjang" id="jen" required class="form-control">
                      <option value="">Pilih</option>
                      <?php
foreach ($dt_jenjang->result() as $row) {
        echo "
                      <option value='$row->id_jenjang'>$row->jenjang</option>";
    }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                  <div class="col-sm-4">
                    <select name="id_ta" id="id_ta" required class="form-control">
                      <option value="">Pilih</option>
                      <?php
foreach ($dt_ta->result() as $ro) {
        echo "
                      <option value='$ro->id_ta'>$ro->tahun_ajaran</option>";
    }?>
                    </select>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                  <div class="col-sm-4">
                    <select name="id_kelas" id="kelas" required class="form-control">
                      <option value="">Pilih</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Induk</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" required placeholder="No.Induk" name="id_siswa" id="kode_siswa">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-info btn-flat" onclick="auto()">Otomatis</button>
                  </div>
                  <label for="inputEmail3" class="col-sm-1 control-label">NISN</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nomor Induk Siswa Nasional" name="nisn" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="Nama Lengkap" id="nama_s" name="nama_lengkap" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir" autocomplete="off">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal2" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="">Pilih</option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.HP</label>
                  <div class="col-sm-4">
                    <input type="number" class="form-control pull-right" name="no_hp" placeholder="No Handphone (+628xxx)" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="email" class="form-control"  id="inputEmail3" placeholder="Alamat Email Orang tua/wali yang aktif" name="email" autocomplete="off">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Asal Sekolah</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Asal Sekolah" name="asal_sekolah" autocomplete="off">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Ket Asal Sekolah</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Prestasi,jurusan,status,dll" name="ket_asal" autocomplete="off">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Ayah</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama Ayah" name="nama_ayah" autocomplete="off">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Ibu</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama Ibu" name="nama_ibu" autocomplete="off">
                  </div>
                </div>
                <div class="form-group" id="bank">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-4">
                    <input type="file" class="form-control" id="inputEmail3" name="gambar">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" onclick="return confirm('Pastikan data sudah lengkap, lanjutkan?')" value="save" class="btn btn-info">Save All</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "import") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
          </a>
          <a href="adm/siswa/download">
            <button class="btn btn-primary btn-flat margin"><i class="fa fa-download"></i> Download Template</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/siswa/importcsv" method="post" enctype="multipart/form-data">
              <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Browse File CSV</label>
                    <div class="col-sm-4">
                      <input type="file" name="userfile" id="userfile" required accept=".csv"/>
                    </div>
                  </div>
              </div>
              <div class="box-footer">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-10">
                    <button type="submit" name="submit"  value="UPLOAD" class="btn btn-info btn-flat"><i class="fa fa-upload"></i> Upload</button>
                  </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "view") {
    ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/siswa/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
          <a href="adm/siswa/import">
            <button
            class="btn bg-blue btn-flat margin"><i class="fa fa-download"></i> Import Data</button>
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/siswa/filter" method="get" enctype="multipart/form-data">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
            <div class="col-sm-4">
              <select id="id_jenjang" required name="id_jenjang" class="form-control">
                <option value=''>Pilih Jenjang</option>
                <?php
foreach ($dt_jenjang->result() as $row) {
        echo "
                <option value='$row->id_jenjang'>$row->jenjang</option>";
    }?>
              </select>
            </div>

          </div>


          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
              <button type="reset" class="btn bg-blue btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body">

        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $_SESSION['pesan'] ?>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';
    ?>

        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>
              <th>No.Induk</th>
              <th>NISN</th>
              <th>Nama Lengkap</th>
              <th>No.Hp</th>
              <th>Kelas</th>
              <th width="12%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php
$no = 1;
    foreach ($dt_siswa->result() as $row) {
        if (strtolower($row->jenis_kelamin) == 'laki-laki') {
            $j = "lk";
        } else {
            $j = "pr";
        }
        $er = $this->db->query("SELECT * FROM tabel_siswakelas INNER JOIN tabel_kelas ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas
              INNER JOIN tabel_jenjang ON tabel_kelas.id_jenjang = tabel_jenjang.id_jenjang
              INNER JOIN tabel_tahunajaran ON tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta
              WHERE tabel_siswakelas.id_siswa = '$row->id_siswa'");
        $r = $er->row();
        if ($er->num_rows() > 0) {
            $kelas = $r->jenjang . "-" . $r->kelas;
        } else {
            $kelas = "";
        }

        echo "
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_siswa'></td>
              <td>$no</td>
              <td>$row->id_siswa</td>
              <td>$row->nisn</td>
              <td>$row->nama_lengkap ($j)</td>
              <td>$row->no_hp</td>
              <td>$kelas</td>
              <td>";
        ?>
              <form method="post" action="adm/siswa/process">
                <input type="hidden" name="id" value="<?php echo $row->id_siswa ?>" />
                <input type="hidden" name="nama" value="<?php echo $row->nama_lengkap ?>" />
                <input type="hidden" name="jk" value="<?php echo $row->jenis_kelamin ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-danger btn-flat btn-sm'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-edit'></i></button>
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-info btn-flat btn-sm'><i class='fa fa-eye'></i></button>
                <?php
$id = $row->id_siswa;
        $c = $this->m_admin->cek_user($id);
        if ($c->num_rows() > 0) {
            ?>
                <button data-toggle='tooltip' title='Set Akun Default' name="s_process" type="submit" value="set_default" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-key'></i></button>
                <?php
}
        ?>
              </form>
              </td>
            </tr>
          <?php
$no++;
    }
    ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>

<script type="text/javascript">
function auto(){
  var jenjang_js=document.getElementById("jen").value;
  if (jenjang_js=="") {
    alert("Pilih Jenjang Terlebih Dahulu...!");
    return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/siswa/cari_id') ?>",
        type:"POST",
        data:"jenjang="+jenjang_js,
        cache:false,
        success: function(msg){
          data=msg.split("|");
          $("#kode_siswa").val(data[0]);
        }
    })
  }
}
</script>
<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#jen").change(function(){
  var id_jenjang = $("#jen").val();
  var id_ta = $("#id_ta").val();
  if(id_ta != ""){
    getKelas(id_jenjang , id_ta);
  }else{
    $("#jen").prop('selectedIndex',0);
    alert("harap pilih tahun ajaran");
  }
});

$("#id_ta").change(function(){

  $("#jen").prop('selectedIndex',0);

});

function getKelas(id_jenjang , id_ta){
  $.ajax({
        url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
        type:"POST",
        data:{"id_jenjang" : id_jenjang ,"id_ta":id_ta},
        cache:false,
        success:function(msg){
          $("#kelas").html(msg);
        }
      })
}
</script>



<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/siswa/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>
