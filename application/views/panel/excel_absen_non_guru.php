<?php date_default_timezone_set("Asia/Jakarta");?>
<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=excel_absen_non_guru.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html>
<html>

<head>
  <title>Cetak</title>
  <style type="text/css">
    body {
      width: 8.5in;
      height: 13.0in;
      font-family: arial;
      margin: 0mm 0mm 0mm 0mm;
    }

    @page {
      size: 8.5in 13.0in
    }

    .table {
      float: left;
      margin-left: 0px;
      border-collapse: collapse;
    }

    .table2 {
      float: left;
      margin-left: 5px;
      border-collapse: collapse;
    }

    #tabel-absen thead tr th {
      font-size: 14px;
      padding: 7px;
    }

    #tabel-absen tbody tr td {
      font-size: 12px;
    }

    @media print {
      .kertas {
        page-break-after: always;
      }
    }
  </style>
</head>

<body onload="window.print()">
  <?php 
  function helpIndoMonth($num)
  {
    $monthArray = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    if(array_key_exists($num, $monthArray)){
      return $monthArray[$num];
    }else{
      return 'Undefined';
    }
  }
      
    $tgl1  = date("l", strtotime($tgl));
    $hari  = cek_hari($tgl1);

    $tgl_akhir_1 = date("l", strtotime($tgl_akhir));
    $tgl_akhir_day  = cek_hari($tgl_akhir_1);

    $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();      

    $args = array(
      'tgl' => $tgl,
      'tgl_akhir' => $tgl_akhir
    );
    $absensi = get_all_harian_absen_kehadiran_non_guru($args);
    $hasil = [];
    foreach($absensi as $absen) {
      $key = str_replace("-", "_", $absen->tgl);
      if( !isset( $hasil[$key] ) ) {
        $hasil[$key] = [];
      }
      $hasil[$key][] = $absen;
    }
    // debugji($hasil, true);
?>

  <?php
  $tgl_keterangan1 = $tgl;
  $tgl_keterangan1 = explode('-', $tgl_keterangan1);
  $t_awal = $tgl_keterangan1[2].'-'.$tgl_keterangan1[1].'-'.$tgl_keterangan1[0];

  $tgl_keterangan2 = $tgl_akhir;
  $tgl_keterangan2 = explode('-', $tgl_keterangan2);
  $t_akhir = $tgl_keterangan2[2].'-'.$tgl_keterangan2[1].'-'.$tgl_keterangan2[0];

  $tgl_awal1 = $tgl;
  $tgl_akhir1 = $tgl_akhir;

  $tgl_awal = date('Y-m-d',strtotime($t_awal));
  $tgl_akhir = date('Y-m-d',strtotime($t_akhir));

  $end = date('Y-m-d', strtotime($tgl_akhir . ' +1 day'));

  $hari_libur = [];
  $kp = get_kalender_pendidikan(array(
    'start_date'	=> $tgl_awal,
    'end_date'		=> $tgl_akhir
  ));

  if( count( $kp ) > 0 ) {
    $jml_hari = ($jml_hari-(count($kp)));
    $jml_hari = ( $jml_hari > 0 ) ? $jml_hari : 0;
    foreach( $kp as $agenda ) {
      if ( $agenda->libur == 1 ) {
        $hari_libur[] = date('d-m-Y', strtotime($agenda->tanggal));
      }   
    }
  }
  
  $period = new DatePeriod(
       new DateTime($tgl_awal),
       new DateInterval('P1D'),
       new DateTime($end)
  );

  foreach ( $hasil as $key => $data ) {

    // if ($value->format("l") == "Sunday") continue;
?>
    <div class="kertas" style="page-break-before: always">
      <?php if(file_exists('assets/panel/images/'.$sql->kop_surat)){ ?>
      <img src="<?php echo base_url()?>assets/panel/images/<?php echo $sql->kop_surat ?>" width="800" height="100">
      <?php }?>

      <?php
$tahun_ajaran = '';
if($jenis == "Semua"){
  $t = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas) INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru) INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) WHERE SUBSTR(tabel_jadwal.hari,3) = '$hari' ORDER BY hari,jam_awal ASC");
  foreach($t->result() as $row){
    $my_q = $this->db->query('SELECT * FROM tabel_tahunajaran WHERE id_ta = "'.$row->id_ta.'"')->row_array();
    $tahun_ajaran = $my_q['tahun_ajaran'];
  }
}elseif($jenis == 'Terlambat'){    
  $t = $this->db->query("SELECT DISTINCT(tabel_absen_mapel.id_jadwal) FROM tabel_jadwal INNER JOIN tabel_absen_mapel ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal WHERE SUBSTR(tabel_jadwal.hari,3) = '$hari' AND tabel_absen_mapel.tgl = '$tgl2' ORDER BY hari,jam_awal ASC");
  foreach($t->result() as $row) {
    $my_q = $this->db->query('SELECT * FROM tabel_tahunajaran WHERE id_ta = "'.$row->id_ta.'"')->row_array();
    $tahun_ajaran = $my_q['tahun_ajaran'];
  }
}else{
  $t = $this->db->query("SELECT * FROM tabel_kelas INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas) INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru) INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) WHERE SUBSTR(tabel_jadwal.hari,3) = '$hari' ORDER BY hari,jam_awal ASC");
  foreach($t->result() as $row) {
    $my_q = $this->db->query('SELECT * FROM tabel_tahunajaran WHERE id_ta = "'.$row->id_ta.'"')->row_array();
    $tahun_ajaran = $my_q['tahun_ajaran'];
  }
}

?>

        <center>
          <table style="width: 97%;text-align: center;border-top: #000 2px solid;border-bottom: #000 2px solid;">
            <tr>
              <td>
                <h2>LAPORAN HARIAN VALIDASI<br> KEHADIRAN NON GURU</h2>
              </td>
            </tr>
          </table>

        </center>
        <table>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <b style="font-size: 12px;font-weight: normal;">
<?php
  $tgl_keterangan1 = $tgl;
  $tgl_keterangan1 = explode('-', $tgl_keterangan1);
  $tgl_keterangan1 = $tgl_keterangan1[0].'-'.helpIndoMonth(str_replace('0', '', $tgl_keterangan1[1]) - 1).'-'.$tgl_keterangan1[2];

  $tgl_keterangan2 = $tgl_akhir;
  $tgl_keterangan2 = explode('-', $tgl_keterangan2);
  $tgl_keterangan2 = $tgl_keterangan2[2].'-'.helpIndoMonth(str_replace('0', '', $tgl_keterangan2[1]) - 1).'-'.$tgl_keterangan2[0];
?>
<u style="text-decoration: none;font-size: 15px;" ><b>Dari tgl</b> :
        <?php echo $tgl_awal1 ?> s.d
        <?php echo $tgl_akhir1 ?>
        </u>
        </b>

        <center>



          <table id="tabel-absen" class="table table-bordered table-hovered" border="1" width="100%">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Nama Non Guru</th>
                <th>Tanggal</th>
                <th>Jam masuk</th>
                <th>Jam pulang</th>
                <th>Validasi jam masuk</th>
                <th>Validasi jam pulang</th>
                <th>Keterangan jam masuk</th>
                <th>Keterangan jam pulang</th>
              </tr>
            </thead>
            <tbody>
                  <?php 
                  $list_ket = get_keterangan_absen();
                  $no=1; foreach( $data as $absen ) : ?>
                  <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $absen->nama; ?></td>
                      <td align="center"><?php echo $absen->tgl; ?></td>     
                      <td align="center"><?php echo $absen->jam_masuk_kerja; ?></td>
                      <td align="center"><?php echo $absen->jam_pulang_kerja; ?></td>                                         
                      <td align="center"><?php echo $absen->validasi_jam_masuk; ?></td>
                      <td align="center"><?php echo $absen->validasi_jam_pulang; ?></td>  
                      <td align="center"><?php echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk; ?> </td>
                      <td align="center"><?php echo $absen->keterangan_jam_pulang; ?></td>
                  </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
          </table>
          <table>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>



          <table id="example2" class="table table-bordered table-hovered" border="0" width="100%" style="margin-top: 12px;">
            <tfoot>
              <?php $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();?>
              <tr>
                <td width="70%"></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <?php echo $re->kota . ', ' . date("d M Y") ?>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>Kepala
                  <?php echo $re->logo_besar ?>
                </td>
              </tr>
              <tr>
                <?php $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();?>
                <td><br><br><br><br><br><br></td>
                <td valign="bottom"><u style="text-decoration-skip-ink: none;"><?php echo $re->pimpinan ?></u> <br>
                  <?php echo $re->nik ?>
                </td>
              </tr>
            </tfoot>
          </table>
        </center>

    </div>
    <?php }?>
</body>

</html>