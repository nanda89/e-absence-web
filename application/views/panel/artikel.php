<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="insert"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/artikel">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/artikel/save" method="post" enctype="multipart/form-data">              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Judul" name="judul">
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kategori</label>
                  <div class="col-sm-4">
                    <select name="id_kategori" class="form-control">                    
                      <option value="">Pilih Kategori</option>
                      <?php 
                      foreach($dt_kategori->result() as $row) {                           
                      echo "
                      <option value='$row->id_kategori'>$row->kategori</option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Penulis</label>
                  <div class="col-sm-10">
                    <?php 
                    $id_cabang = $this->session->userdata('id_cabang');
                    $level = $this->session->userdata('level');
                    if ($level=='operator cabang') {
                      $f = $this->m_semua->getById("tabel_cabang","id_cabang",$id_cabang)->row();
                    }else{
                      $f = "Administrator";
                    }
                    ?>
                    <input type="text" readonly class="form-control" id="inputEmail3" placeholder="Penulis" name="penulis" value="<?php echo $f->nama_cabang ?>">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Gambar Header <br>(744 x 302 px)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" name="gambar">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Isi Artikel</label>
                  <div class="col-sm-10">
                    <textarea name="isi" id="textarea" class="form-control" style="height: 400px">
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>                  
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" name="status" value="publish" class="flat-red" checked> Publish
                    </label> &nbsp;&nbsp;&nbsp;
                    <label>
                      <input type="radio" name="status" value="draft" class="flat-red"> Draft
                    </label>                    
                  </div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="edit"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/artikel">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/artikel/process" method="post" enctype="multipart/form-data">              
              <input type="hidden" name="id" value="<?php echo $row->id_artikel ?>">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->judul ?>" placeholder="Judul" name="judul">
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kategori</label>
                  <div class="col-sm-4">
                    <select name="id_kategori" class="form-control">                    
                      <option value="<?php echo $row->id_kategori ?>">
                        <?php 
                        $r = $this->m_kategori->get_one($row->id_kategori);
                        $r_je = $r->row();
                        echo $r_je->kategori;
                        ?>
                      </option>

                      <?php 
                      foreach($dt_kategori->result() as $row2) {                           
                      echo "
                      <option value='$row2->id_kategori'>$row2->kategori</option>";
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Penulis</label>
                  <div class="col-sm-10">
                    <input type="text" readonly class="form-control" id="inputEmail3" placeholder="Penulis" name="penulis" value="<?php echo $row->penulis ?>">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Gambar Header <br>(744 x 302 px)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" name="gambar">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Isi Artikel</label>
                  <div class="col-sm-10">
                    <textarea name="isi" id="textarea" class="form-control" style="height: 300px">
                      <?php echo $row->isi ?>
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>                  
                  <div class="col-sm-4">
                    <?php 
                    if($row->status=='publish'){
                    ?>
                    <div class="radio3 radio-success radio-check radio-inline">                                  
                      <input type="radio" class="flat-red" id="radio1" name="status" value="publish" checked>
                      <label for="radio1">
                        Publish
                      </label>
                    </div>
                    <div class="radio3 radio-success radio-check radio-inline">
                      <input type="radio" class="flat-red" id="radio2" name="status" value="draft">
                      <label for="radio2">
                        Draft
                      </label>
                    </div>
                    <?php 
                    }elseif($row->status=='draft'){
                    ?>
                    <div class="radio3 radio-success radio-check radio-inline">                                  
                      <input type="radio" class="flat-red" id="radio1" name="status" value="publish">
                      <label for="radio1">
                        Publish
                      </label>
                    </div>
                    <div class="radio3 radio-success radio-check radio-inline">
                      <input type="radio" class="flat-red" id="radio2" name="status" value="draft" checked>
                      <label for="radio2">
                        Draft
                      </label>
                    </div>
                    <?php 
                    } 
                    ?>                    
                  </div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }elseif($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/artikel/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
          <a href="adm/kategori">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Tambah Kategori</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Judul</th>
              <th>Kategori</th>
              <th>Dibaca</th>
              <th>Waktu</th>
              <th>Status</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_ka->result() as $row) {       
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->judul</td>
              <td>$row->kategori</td>
              <td>$row->dibaca</td>
              <td>$row->waktu</td>              
              <td>";
              ?>
              <form method="post" action="adm/artikel/process">
                <input type="hidden" name="id" value="<?php echo $row->id_artikel ?>" />
                <?php 
                $st = $row->status;                
                if($st=='publish'){
                ?>
                <button data-toggle='tooltip' title='Set Draft' name="s_process" type="submit" value="publish" class='btn btn-sm btn-primary btn-flat'>Publish</button>
                <?php
                }else{
                ?>
                <button data-toggle='tooltip' title='Set Publish' name="s_process" type="submit" value="draft" class='btn btn-sm btn-warning btn-flat'>Draft</button>
                <?php 
                }
                ?>                
              </td>
              <td>
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>                
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>

