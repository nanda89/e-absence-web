
<meta http-equiv="refresh" content="5">
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <table class="table table-bordered">                               
          <tr>
            <td width="10%"><b>Tanggal</td>
            <td>
              <?php                            
              $tgl1  = gmdate("d F Y", time()+60*60*7);
              $hari = hari();
              echo $hari.", ".$tgl1;
              ?>
            </td>
          </tr>
          <tr>
            <td><b>Jadwal SMS</b></td>
            <td>
              <?php 
              $dt = $this->db->query('SELECT * FROM tabel_schedule');
              foreach ($dt->result() as $k) {
                if($k->status=='aktif'){
                  $t = "<i class='fa fa-check'></i>";
                }else{
                  $t = "<i class='fa fa-minus'></i>";
                }
                echo "
                $k->tipe_sms $t                
                ";
              }
              ?>
            </td>
          </tr>
        </table>
      </div>
      <div class="box-body">        
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Jenjang</th>
              <th>Kelas</th>
              <th>Wali Kelas</th>
              <th width="15%">Validasi Masuk</th>
              <th width="15%">Validasi Pulang</th>              
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          $tgl2 = gmdate("d-m-Y", time()+60*60*7);
          $t = $this->m_jenjang->get_all_kelas();
          foreach($t->result() as $row) {                   
            $cek = $this->db->query("SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                    WHERE tabel_siswakelas.id_kelas='$row->id_kelas' and tabel_absen.tgl = '$tgl2'
                    AND tabel_absen.valid_masuk='valid'");
            if($cek->num_rows()>0){
              $val = "<a class='btn bg-maroon btn-flat btn-sm'>Tervalidasi <i class='fa fa-check'></i></a>";
              $dt = $this->db->query('SELECT * FROM tabel_schedule');
              foreach ($dt->result() as $s) {
                if($s->tipe_sms=='Harian' AND $s->status=='aktif'){
                  $filter = $s->aturan;

                  if($filter=='semua'){
                    $kondisi = "";
                  }elseif($filter=='hadir'){
                    $kondisi = "AND tabel_absen.absen_masuk='hadir'";
                  }elseif($filter=='tidak hadir'){
                    $kondisi = "AND tabel_absen.absen_masuk<>'hadir'";
                  }
                  $sql = "SELECT tabel_absen.id_absen,tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
                        tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp,
                        tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
                        tabel_absen.jam_masuk,tabel_absen.jam_pulang
                        FROM tabel_siswakelas LEFT JOIN tabel_siswa
                        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
                        ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
                        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
                        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
                        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
                        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)   
                        WHERE tabel_kelas.id_kelas='$row->id_kelas' AND tabel_absen.tgl='$tgl2' 
                        AND tabel_absen.sms_masuk NOT LIKE '%kirim%'            
                        ".$kondisi."
                        ORDER BY tabel_absen.id_absen DESC";

                  $dt_wali_kelas = $this->db->query($sql);
                  foreach($dt_wali_kelas->result() as $ro) {                           
                    $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '1'")->row(); 

                    if($ro->absen_masuk == 'hadir'){
                      $find     = array("nama_i","nis_i","tgl_i","jam_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->jam_masuk);
                      $isi      = str_replace($find, $replace, $r->isi);
                    }else{
                      $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
                      $find     = array("nama_i","nis_i","tgl_i","absen_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->absen_masuk);
                      $isi      = str_replace($find, $replace, $as->isi);
                    }

                    // $find     = array("nama_i","nis_i","tgl_i","jam_i");
                    // $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->jam_masuk);
                    // $isi      = str_replace($find, $replace, $r->isi);
                    
                    $waktu = gmdate("Y-m-d H:i:s", time()+60*60*7);
                    //$date=date_create($waktu);
                    $date = strtotime($waktu."+ 5 minutes");
                    $h = date('Y-m-d H:i:s',$date);


                    $myArray = array();
                    $d  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();              
                    $tujuan = $d->link_tujuan;        

                    if($d->metode == 'Sewa Server'){
                      $a = $d->kode_aktivasi;
                      $b = $h;
                      $c = $isi;
                      $d = $row->no_hp;
                      $e = $r->jenis;
                      $f = base_url()."adm/cek_absen";                            
                      $myArray[]  = array("kode" => $a, "waktu" => $b,"isi" => $c,"no_hp" => $d,"jenis" => $e,"link_lagi" => $f);             

                      $url = $tujuan.'assets/json_file/simpan.php/';
                      $ch = curl_init( $url );
                        
                      $payload = json_encode(array("rekap"=>$myArray));
                      curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                      //curl_setopt( $ch, CURLOPT_HEADER, true);
                      curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                      # Return response instead of printing.
                      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                      # Send request.
                      $result = curl_exec($ch);
                      curl_close($ch);
                      # Print response.
                      echo "<pre>$result</pre>";

                      
                      $de['sms_masuk'] = 'kirim';
                      $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);                      

                    }elseif($d->metode == 'Kirim Sendiri'){
                      $dat['DestinationNumber']  = $ro->no_hp;
                      $dat['SendingDateTime']    = $h;
                      $dat['TextDecoded']        = $isi;
                      $this->db->insert("outbox",$dat);

                      $de['sms_masuk'] = 'kirim';
                      $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);                      
                    }                    
                  }
                }
                

                
                if($s->tipe_sms=='Mingguan' AND $s->status=='aktif'){
                  $hari = gmdate("l", time()+60*60*7);
                  if($hari=='Saturday'){
                    $filter = $s->aturan;

                    if($filter=='semua'){
                      $kondisi = "";
                    }elseif($filter=='hadir'){
                      $kondisi = "AND tabel_absen.absen_masuk='hadir'";
                    }elseif($filter=='tidak_hadir'){
                      $kondisi = "AND tabel_absen.absen_masuk<>'hadir'";
                    }

                    $waktu = gmdate("d-m-Y", time()+60*60*7);
                    //$date=date_create($waktu);
                    //date_sub($date,date_interval_create_from_date_string("6 days"));
                    //$tgl1 = date_format($date,'d-m-Y');
                    
                    $date = strtotime($waktu."+ 6 days");
                    $tgl1 = date('d-m-Y',$date);

                    $sql = "SELECT tabel_absen.id_absen,tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
                          tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp,
                          tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
                          tabel_absen.jam_masuk,tabel_absen.jam_pulang
                          FROM tabel_siswakelas LEFT JOIN tabel_siswa
                          ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
                          ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
                          ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
                          ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
                          ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
                          ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)   
                          WHERE tabel_kelas.id_kelas='$row->id_kelas' AND tabel_absen.tgl BETWEEN '$tgl1' AND '$tgl2'
                          AND tabel_absen.sms_masuk NOT LIKE '%rekap%'            
                          ".$kondisi."
                          ORDER BY tabel_absen.id_absen DESC";

                    $dt_wali_kelas = $this->db->query($sql);
                    foreach($dt_wali_kelas->result() as $ro) {                           
                      $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '3'")->row(); 

                      $hadir    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'hadir' AND valid_masuk = 'valid'")->row();
                      $alpa     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'alpha' AND valid_masuk = 'valid'")->row();
                      $izin     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'izin' AND valid_masuk = 'valid'")->row();
                      $sakit    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'sakit' AND valid_masuk = 'valid'")->row();
                      $find     = array("nama_i","nis_i","tgl1_i","tgl2_i","hadir_i","alpa_i","izin_i","sakit_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$tgl1,$tgl2,$hadir->jum,$alpa->jum,$izin->jum,$sakit->jum);
                      $isi      = str_replace($find, $replace, $r->isi);
                      
                      $waktu = gmdate("Y-m-d H:i:s", time()+60*60*7);
                      //$date=date_create($waktu);
                      $date = strtotime($waktu."+ 5 minutes");
                      $h = date('Y-m-d H:i:s',$date);

                      $myArray = array();
                      $d  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();              
                      $tujuan = $d->link_tujuan;        

                      if($d->metode == 'Sewa Server'){
                        $a = $d->kode_aktivasi;
                        $b = $h;
                        $c = $isi;
                        $d = $row->no_hp;
                        $e = $r->jenis;
                        $f = base_url()."adm/cek_absen";                            
                        $myArray[]  = array("kode" => $a, "waktu" => $b,"isi" => $c,"no_hp" => $d,"jenis" => $e,"link_lagi" => $f);             

                        $url = $tujuan.'assets/json_file/simpan.php/';
                        $ch = curl_init( $url );
                          
                        $payload = json_encode(array("rekap"=>$myArray));
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                        //curl_setopt( $ch, CURLOPT_HEADER, true);
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                        # Return response instead of printing.
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                        # Send request.
                        $result = curl_exec($ch);
                        curl_close($ch);
                        # Print response.
                        echo "<pre>$result</pre>";

                        
                        $ce = $this->db->query("SELECT * FROM tabel_absen WHERE id_absen = '$ro->id_absen'")->row();
                        if($ce->sms_masuk == '-'){
                          $de['sms_masuk'] = '- | rekap';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }elseif($ce->sms_masuk == 'kirim'){
                          $de['sms_masuk'] = 'kirim | rekap';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }                      

                      }elseif($d->metode == 'Kirim Sendiri'){

                        $dat['DestinationNumber']  = $ro->no_hp;
                        $dat['SendingDateTime']    = $h;
                        $dat['TextDecoded']        = $isi;
                        $this->db->insert("outbox",$dat);
                        
                        $ce = $this->db->query("SELECT * FROM tabel_absen WHERE id_absen = '$ro->id_absen'")->row();
                        if($ce->sms_masuk == '-'){
                          $de['sms_masuk'] = '- | rekap';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }elseif($ce->sms_masuk == 'kirim'){
                          $de['sms_masuk'] = 'kirim | rekap';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }         
                      }                                   

                    }                
                  }
                } 

                
                if($s->tipe_sms=='Bulanan' AND $s->status=='aktif'){
                  $tg = gmdate("d", time()+60*60*7);
                  if($tg=='28'){
                    $filter = $s->aturan;

                    if($filter=='semua'){
                      $kondisi = "";
                    }elseif($filter=='hadir'){
                      $kondisi = "AND tabel_absen.absen_masuk='hadir'";
                    }elseif($filter=='tidak_hadir'){
                      $kondisi = "AND tabel_absen.absen_masuk<>'hadir'";
                    }

                    $waktu = gmdate("d-m-Y", time()+60*60*7);
                    //$date=date_create($waktu);
                    $date = strtotime($waktu."+ 30 days");
                    $tgl1 = date('d-m-Y',$date);

                    $sql = "SELECT tabel_absen.id_absen,tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
                          tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp,
                          tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
                          tabel_absen.jam_masuk,tabel_absen.jam_pulang
                          FROM tabel_siswakelas LEFT JOIN tabel_siswa
                          ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
                          ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
                          ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
                          ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
                          ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
                          ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)   
                          WHERE tabel_kelas.id_kelas='$row->id_kelas' AND tabel_absen.tgl BETWEEN '$tgl1' AND '$tgl2'
                          AND tabel_absen.sms_masuk NOT LIKE '%bulan%'            
                          ".$kondisi."
                          ORDER BY tabel_absen.id_absen DESC";

                    $dt_wali_kelas = $this->db->query($sql);
                    foreach($dt_wali_kelas->result() as $ro) {                           
                      $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '3'")->row(); 

                      $hadir    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'hadir' AND valid_masuk = 'valid'")->row();
                      $alpa     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'alpha' AND valid_masuk = 'valid'")->row();
                      $izin     = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'izin' AND valid_masuk = 'valid'")->row();
                      $sakit    = $this->db->query("SELECT COUNT(id_absen) as jum FROM tabel_absen 
                                  WHERE id_penempatan = '$ro->id_penempatan' AND tgl BETWEEN '$tgl1' AND '$tgl2'
                                  AND absen_masuk = 'sakit' AND valid_masuk = 'valid'")->row();
                      $find     = array("nama_i","nis_i","tgl1_i","tgl2_i","hadir_i","alpa_i","izin_i","sakit_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$tgl1,$tgl2,$hadir->jum,$alpa->jum,$izin->jum,$sakit->jum);
                      $isi      = str_replace($find, $replace, $r->isi);
                      
                      $waktu = gmdate("Y-m-d H:i:s", time()+60*60*7);
                      //$date=date_create($waktu);
                      $date = strtotime($waktu."+ 5 minutes");
                      $h = date('Y-m-d H:i:s',$date);

                      $myArray = array();
                      $d  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();              
                      $tujuan = $d->link_tujuan;        

                      if($d->metode == 'Sewa Server'){
                        $a = $d->kode_aktivasi;
                        $b = $h;
                        $c = $isi;
                        $d = $row->no_hp;
                        $e = $r->jenis;
                        $f = base_url()."adm/cek_absen";                            
                        $myArray[]  = array("kode" => $a, "waktu" => $b,"isi" => $c,"no_hp" => $d,"jenis" => $e,"link_lagi" => $f);             

                        $url = $tujuan.'assets/json_file/simpan.php/';
                        $ch = curl_init( $url );
                          
                        $payload = json_encode(array("rekap"=>$myArray));
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                        //curl_setopt( $ch, CURLOPT_HEADER, true);
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                        # Return response instead of printing.
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                        # Send request.
                        $result = curl_exec($ch);
                        curl_close($ch);
                        # Print response.
                        echo "<pre>$result</pre>";

                        
                        $ce = $this->db->query("SELECT * FROM tabel_absen WHERE id_absen = '$ro->id_absen'")->row();
                        if($ce->sms_masuk == '- | rekap'){
                          $de['sms_masuk'] = '- | rekap | bulan';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }elseif($ce->sms_masuk == '- | rekap | bulan'){
                          $de['sms_masuk'] = 'kirim | rekap | bulan';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }

                      }elseif($d->metode == 'Kirim Sendiri'){

                        $dat['DestinationNumber']  = $ro->no_hp;
                        $dat['SendingDateTime']    = $h;
                        $dat['TextDecoded']        = $isi;
                        $this->db->insert("outbox",$dat);
                        
                        $ce = $this->db->query("SELECT * FROM tabel_absen WHERE id_absen = '$ro->id_absen'")->row();
                        if($ce->sms_masuk == '- | rekap'){
                          $de['sms_masuk'] = '- | rekap | bulan';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }elseif($ce->sms_masuk == '- | rekap | bulan'){
                          $de['sms_masuk'] = 'kirim | rekap | bulan';  
                          $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                        }                                            
                      }

                    }                
                  }  
                }
                
              }
            }else{
              $val = "<a class='btn btn-warning btn-flat btn-sm'><i class='fa fa-minus'></i></a>";
            }

            $cek2 = $this->db->query("SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                    WHERE tabel_siswakelas.id_kelas='$row->id_kelas' and tabel_absen.tgl = '$tgl2'
                    AND tabel_absen.valid_pulang='valid'");
            if($cek2->num_rows()>0){
              $vala = "<a class='btn bg-maroon btn-flat btn-sm'>Tervalidasi <i class='fa fa-check'></i></a>";
              $dt = $this->db->query('SELECT * FROM tabel_schedule');
              foreach ($dt->result() as $s) {
                if($s->tipe_sms=='Harian' AND $s->status=='aktif'){
                  $filter = $s->aturan;

                  if($filter=='semua'){
                    $kondisi = "";
                  }elseif($filter=='hadir'){
                    $kondisi = "AND tabel_absen.absen_pulang='hadir'";
                  }elseif($filter=='tidak_hadir'){
                    $kondisi = "AND tabel_absen.absen_pulang<>'hadir'";
                  }
                  $sql = "SELECT tabel_absen.id_absen,tabel_siswakelas.id_penempatan,tabel_siswa.nama_lengkap,tabel_siswa.id_siswa,tabel_jenjang.jenjang,
                    tabel_kelas.kelas,tabel_siswa.nama_ayah,tabel_siswa.nama_ibu,tabel_siswa.no_hp,
                    tabel_tahunajaran.tahun_ajaran,tabel_absen.tgl,tabel_absen.absen_masuk,tabel_absen.absen_pulang,tabel_guru.nama,
                    tabel_absen.jam_masuk,tabel_absen.jam_pulang
                    FROM tabel_siswakelas LEFT JOIN tabel_siswa
                        ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang
                        ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas 
                        ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru 
                        ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran
                        ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) INNER JOIN tabel_absen
                        ON (tabel_siswakelas.id_penempatan = tabel_absen.id_penempatan)   
                        WHERE tabel_kelas.id_kelas='$row->id_kelas' AND tabel_absen.tgl='$tgl2' 
                        AND tabel_absen.sms_pulang NOT LIKE '%kirim%'            
                        ".$kondisi."
                        ORDER BY tabel_absen.id_absen DESC";

                  $dt_wali_kelas = $this->db->query($sql);
                  foreach($dt_wali_kelas->result() as $ro) {                           
                    $r = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = '2'")->row();              
                    
                    if($ro->absen_pulang == 'hadir'){
                      $find     = array("nama_i","nis_i","tgl_i","jam_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->jam_pulang);
                      $isi      = str_replace($find, $replace, $r->isi);
                    }else{
                      $as = $this->db->query("SELECT * FROM sms_auto WHERE id_pesan = 4")->row();                                      
                      $find     = array("nama_i","nis_i","tgl_i","absen_i");
                      $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->absen_pulang);
                      $isi      = str_replace($find, $replace, $as->isi);
                    }

                    // $find     = array("nama_i","nis_i","tgl_i","jam_i");
                    // $replace  = array($ro->nama_lengkap,$ro->id_siswa,$ro->tgl,$ro->jam_pulang);
                    // $isi      = str_replace($find, $replace, $r->isi);
                    
                    $waktu = gmdate("Y-m-d H:i:s", time()+60*60*7);
                    //$date=date_create($waktu);
                    $date = strtotime($waktu."+ 5 minutes");
                    $h = date('Y-m-d H:i:s',$date);
                    $myArray = array();
                    $d  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();              
                    $tujuan = $d->link_tujuan;        

                    if($d->metode == 'Sewa Server'){
                      $a = $d->kode_aktivasi;
                      $b = $h;
                      $c = $isi;
                      $d = $row->no_hp;
                      $e = $r->jenis;
                      $f = base_url()."adm/cek_absen";                            
                      $myArray[]  = array("kode" => $a, "waktu" => $b,"isi" => $c,"no_hp" => $d,"jenis" => $e,"link_lagi" => $f);             

                      $url = $tujuan.'assets/json_file/simpan.php/';
                      $ch = curl_init( $url );
                        
                      $payload = json_encode(array("rekap"=>$myArray));
                      curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                      //curl_setopt( $ch, CURLOPT_HEADER, true);
                      curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                      # Return response instead of printing.
                      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                      # Send request.
                      $result = curl_exec($ch);
                      curl_close($ch);
                      # Print response.
                      echo "<pre>$result</pre>";

                      
                      $de['sms_pulang'] = 'kirim';
                      $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);                      

                    }elseif($d->metode == 'Kirim Sendiri'){

                      $dat['DestinationNumber']  = $ro->no_hp;
                      $dat['SendingDateTime']    = $h;
                      $dat['TextDecoded']        = $isi;
                      $this->db->insert("outbox",$dat);

                      $de['sms_pulang'] = 'kirim';
                      $this->m_semua->update('tabel_absen',$de,'id_absen',$ro->id_absen);
                    }
                  }
                }
                
              }
            }else{
              $vala = "<a class='btn btn-warning btn-flat btn-sm'><i class='fa fa-minus'></i></a>";
            }

            
            echo "          
              <tr>
                <td>$no</td>
                <td>$row->jenjang</td>
                <td>$row->kelas</td>
                <td>$row->nama</td>
                <td>$val</td>
                <td>$vala</td>                          
              </tr>";          
            $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>

<script type="text/javascript">
    var auto_refresh = setInterval(
    function () {
        $('#load_content').load('adm/cek_absen').fadeIn("slow");
    }, 500); // refresh setiap 5000 milliseconds
</script>
