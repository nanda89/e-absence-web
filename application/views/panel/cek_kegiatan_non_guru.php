<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $judul1; ?>
            <small><?php echo $judul2; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $judul1; ?></li>
        </ol>
    </section>
    <section class="content">

      <!-- VIEW -->
      <?php if ($set == "view"): ?>
      <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title">
                  <a href='adm/cek_kegiatan_non_guru/export_filter?a=1&nonGuru=&tgl=<?php echo date('d-m-Y') ?>&tgl_akhir=<?php echo date('d-m-Y') ?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
                  
                  <a class="btn btn-primary btn-flat margin cetak-data" data-toggle="modal"
                   data-target="#cetakModal" 
                   data-tgl="<?php echo date('d-m-Y') ?>"
                   data-tgl_akhir="<?php echo date('d-m-Y') ?>"
                   data-g=""
                   data-a="2"><i class="fa fa-print"></i> Cetak</a>
                  
                </h3>
              <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
          </div>
          <div class="box-body with-border">
              <form class="form-horizontal" action="adm/cek_kegiatan_non_guru/filter" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                      <?php
$tgl1 = gmdate("d-m-Y", time() + 60 * 60 * 7);
$hari = hari();
?>
                      <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                      <div class="col-sm-2">
                          Dari :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-2">
                          Sampai :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-3">
                          Non Guru :<select name="nonGuru" class="form-control">
                              <?php
$non_guru = $this->db->query("SELECT * FROM tabel_non_guru");
$data .= "<option value=''>Pilih Non Guru</option>";
foreach ($non_guru->result() as $row) {
    $data .= "<option value='$row->id_non_guru'>$row->nama</option>\n";
}
echo $data;
?>
                          </select>
                      </div>
                      <div class="col-sm-4" style="margin-top:10px;">
                          <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                          <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
                      </div>
                  </div>
              </form>
          </div>
          <div class="box-body">
                <div class="row">
                <table id="example3" class="table table-bordered table-hovered">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Non Guru</th>
                            <th>Jabatan</th>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th>Kegiatan</th>
                            <th>File</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
$args = array(
    'tgl' => date('d-m-Y'),
    'tgl_akhir' => date('d-m-Y'),
    'nonGuru' => '',
);
$listKegiatan = get_kegiatan($args);
$no = 1;foreach ($listKegiatan as $kegiatan): ?>
                      <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $kegiatan->nama; ?></td>
                    <td><?php echo $kegiatan->jabatan; ?></td>
                    <td><?php echo $kegiatan->tanggal; ?></td>
                    <td><?php echo $kegiatan->jam; ?></td>
                    <td><?php echo $kegiatan->uraian; ?></td>
                    <td>
                    <?php if ($kegiatan->type == "p"): ?>
                        <a class="btn btn-social-icon btn-instagram send-data" data-toggle="modal" data-target="#myModal" data-name="<?php echo $kegiatan->img; ?>" data-type="<?php echo $kegiatan->type; ?>">
                        <i class="fa fa-instagram"></i></a>
                    <?php elseif ($kegiatan->type == "v"): ?>
                        <a class="btn btn-social-icon btn-flickr send-data" data-toggle="modal" data-target="#myModal" data-name="<?php echo $kegiatan->img; ?>" data-type="<?php echo $kegiatan->type; ?>">
                        <i class="fa fa-file-video-o"></i></a>
                    <?php endif;?>
                    </td>
                  </tr>
                  <?php $no++;endforeach;?>
                    </tbody>
                </table>
                </div>
          </div>
      </div>


      <!-- MODAL CETAK -->
      <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="cetakModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
      <p>Apakah anda akan mencetak dengan gambar kegiatan ?</p>
      <p>klik <b>Ya</b> cetak menggunakan foto atau <br>klik <b>Tidak</b> cetak tanpa foto</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modal-btn-si">Ya</button>
        <button type="button" class="btn btn-default" id="modal-btn-no">Tidak</button>
      </div>
    </div>
  </div>
</div>

      <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">GAMBAR KEGIATAN</h4>
      </div>
      <div class="modal-body">
      <div class="row text-center" id="gam">
        <img id="myImg" src="" style="width:100%;max-width:300px">
      </div>
      <div class="row text-center" id="vid">
        <video width="400" id="v_kegiatan" controls>
            <source src="" type="video/mp4">
        </video>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

      <!-- ABSEN GURU FILTER -->
      <?php elseif ($set == "filter"): ?>
      <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title">
                  <a href='adm/cek_kegiatan_non_guru' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>
                  <a href='adm/cek_kegiatan_non_guru/export_filter?a=1&nonGuru=<?php echo $nonGuru ?>&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir; ?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>
                  
                  <a class="btn btn-primary btn-flat margin cetak-data" data-toggle="modal"
                   data-target="#cetakModal" 
                   data-tgl="<?php echo $tgl ?>"
                   data-tgl_akhir="<?php echo $tgl_akhir ?>"
                   data-g="<?php echo $nonGuru ?>"
                   data-a="2"><i class="fa fa-print"></i> Cetak</a>

              </h3>
              <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
          </div>
          <div class="box-body with-border">
              <form class="form-horizontal" action="adm/cek_absen_non_guru/filter" method="get" enctype="multipart/form-data">
                  <div class="form-group">
                      <?php
$tgl1 = date("l", strtotime($tgl));
$hari = cek_hari($tgl1);
?>
                      <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                      <div class="col-sm-2">
                          Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-2">
                          Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-3">
                          Non Guru :<select name="nonGuru" class="form-control" disabled>
                          <?php
$dataNonGuru = $nama_non_guru == '' ? 'semua' : $nama_non_guru;
echo "<option value='$dataNonGuru'>$dataNonGuru</option>";
?>

                          </select>
                      </div>

                  </div>
              </form>
          </div>
          <div class="box-body">

            <div class="row">
            <table id="example3" class="table table-bordered table-hovered">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Non Guru</th>
                        <th>Jabatan</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                        <th>Kegiatan</th>
                        <th>File</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
$args = array(
    'tgl' => $tgl,
    'tgl_akhir' => $tgl_akhir,
    'nonGuru' => $nonGuru,
);
$listKegiatan = get_kegiatan($args);
$no = 1;foreach ($listKegiatan as $kegiatan): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $kegiatan->nama; ?></td>
                    <td><?php echo $kegiatan->jabatan; ?></td>
                    <td><?php echo $kegiatan->tanggal; ?></td>
                    <td><?php echo $kegiatan->jam; ?></td>
                    <td><?php echo $kegiatan->uraian; ?></td>
                    <td>
                    <?php if ($kegiatan->type == "p"): ?>
                        <a class="btn btn-social-icon btn-instagram send-data" data-toggle="modal" data-target="#myModal" data-name="<?php echo $kegiatan->img; ?>" data-type="<?php echo $kegiatan->type; ?>">
                        <i class="fa fa-instagram"></i></a>
                    <?php elseif ($kegiatan->type == "v"): ?>
                        <a class="btn btn-social-icon btn-flickr send-data" data-toggle="modal" data-target="#myModal" data-name="<?php echo $kegiatan->img; ?>" data-type="<?php echo $kegiatan->type; ?>">
                        <i class="fa fa-file-video-o"></i></a>
                    <?php endif;?>
                    </td>
                  </tr>
                  <?php $no++;endforeach;?>
                </tbody>
            </table>
            </div>

            </div><!-- /.box-body -->
          </div><!-- /.box -->

      <?php endif;?>

  </section>
</div>
<!-- MODAL CETAK -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="cetakModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
      <p>Apakah anda akan mencetak dengan gambar kegiatan ?</p>
      <p>klik <b>Ya</b> cetak menggunakan foto atau <br>klik <b>Tidak</b> cetak tanpa foto</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modal-btn-si">Ya</button>
        <button type="button" class="btn btn-default" id="modal-btn-no">Tidak</button>
      </div>
    </div>
  </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">GAMBAR KEGIATAN</h4>
      </div>
      <div class="modal-body">
      <div class="row text-center" id="gam">
        <img id="myImg" src="" style="width:100%;max-width:300px">
      </div>
      <div class="row text-center" id="vid">
        <video width="400" id="v_kegiatan" controls>
            <source src="" type="video/mp4">
        </video>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
