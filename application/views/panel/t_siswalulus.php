
<table id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="2%">No</th>      
      <th>No.Induk</th>      
      <th>Nama Lengkap</th>            
      <th width="10%">Aksi</th>
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1; 
  foreach($dt_kenaikan->result() as $row) {       
  echo "          
    <tr>
      <td>$no</td>
      <td>$row->id_siswa</td>      
      <td>$row->nama_lengkap</td>            
      <td>"; ?>
        <button title="Luluskan"
            class="btn btn-sm btn-primary btn-flat fa fa-chevron-right" type="button" 
            onClick="luluskan('<?php echo $row->id_penempatan; ?>','<?php echo $row->id_siswa; ?>')"></button>
      </td>
    </tr>
  <?php
  $no++;
  }
  ?>
  </tbody>
</table>

