petunjuk instalasi

- pastikan anda telah instal Appserv 1.5.9 atau XAMPP 1.7.1
- unzip file gammu-php-sample ini ke xampp/htdocs atau Appserv/htdocs
- buat database dengan nama smsd dan import file sql yang berada di folder gammu/sql
  caranya :
  - buka http://localhost/phpmyadmin
  - Create new database smsd
  - klik smsd
  - klik import
  - browse file sql.sql di folder gammu/sql
  
- buka file db.php pastikan password dan nama database sudah benar
- buka file gammurc di folder gammu, pastikan bahwa PORT modem sudah benar
- buka file smsdrc di folder gammu, pastikan bahwa PORT modem sudah benar dan password dan nama database sudah benar
- buka http://localhost/gammu-php/inbox.php lalu Klik SMS Gateway
  - klik install
  - klik start
  - klik INBOX
- klik Kirim SMS untuk tes mengirim SMS




###############

  Menggunakan GAMMU versi 1.2.5
  
  Kode digenerate by PROCODEGEN 2010
  By Nixerco Technologies
  www.nixsms.com
  www.software-pilkada.com
  www.nixsms-primagama.com
  www.sms-online.web.id
  
 
Skrip ini digunakan untuk membantu anda memahami cara kerja gammu, dan contoh kode yang bisa langsung digunakan
setelah berhasil silahkan kembangkan sendiri menjadi sms center atau integrasi dengan software lain
semoga bermanfaat

Bekasi, 3 Agustus 2010

Aswandi
  
#################