
<style type="text/css">
.table {
    float:left;    
    margin-left:0px;
    border-collapse:collapse;
}
.table2 {
    float:left;    
    margin-left:5px;
    border-collapse:collapse;
}
</style>
<base href="<?php echo base_url(); ?>" />
<body onload="window.print()">

<?php 

 $d = date('dms'); 

// header("Content-type: application/octet-stream");
// header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
// header("Pragma: no-cache");
// header("Expires: 0");

// $ro = $dt_wali_kelas->row(); 
$tglnow = gmdate("d-m-Y", time()+60*60*7);
$tglcantik = gmdate("d M Y", time()+60*60*7);
$harinow = hari();   
$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>
<!--table border="0" width="100%">
  <tr>
    <td width="10%">
      <img src="assets/panel/images/<?php echo $sql->logo_sekolah ?>" width="100%">
    </td>
    <td align="center" valign="top">      
        <h3>
          PEMERINTAH KOTA MAKASSAR <br>
          DINAS PENDIDIKAN DAN KEBUDAYAAN <br>
          <?php echo strtoupper($sql->logo_besar) ?>         
        </h3>      
        <?php echo $sql->alamat ?> <br>
        <?php echo $sql->akun_gmail." - ".$sql->website ?> 
    </td>
    <td width="10%">    
      <img src="assets/panel/images/<?php echo $sql->logo_sekolah ?>" width="100%">
    </td>
  </tr>
</table-->
<div class="kertas" style="page-break-before: always">
<?php if(file_exists('assets/panel/images/'.$sql->kop_surat)){ ?>
<img src="<?php echo base_url()?>assets/panel/images/<?php echo $sql->kop_surat ?>" width="800" height="100" style="page-break-before: always">
<?php }?>

<hr style="border-collapse:collapse;">
  <p align="center"><b>REKAPITULASI VALIDASI ABSEN GURU</b></p>
<hr style="border-collapse:collapse;">
<table border="0">
  <tr>
    <td><b>Hari/Tanggal</b></td>
    <td>: <?php echo $harinow.", ".$tglnow ?></td>
  </tr>
  <tr>
    <td><b>Tahun Ajaran</b></td>
    <td>: <?php echo $tahunajaran ?></td>
  </tr>
  <tr>
    <td><b>Jenjang</b></td>
    <td>: <?php echo $jenjang ?></td>
  </tr>
  <tr>
    <td><b>Guru</b></td>
    <td>: <?php echo $nama_guru ?></td>
  </tr>
  <tr>
    <td><b>Rekap</b></td>
    <td>: <?php echo $tgl." s/d ".$tgl_akhir ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<table border="1" width="100%" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
    <th>NAMA</th>
      <th>MATA PELAJARAN</th>
      <th>HADIR</th>
      <th>SAKIT</th>
      <th>IZIN</th>
      <th>NIHIL</th>  
      <th>TANPA KET.</th>                        
    </tr>
  </thead>
  <tbody>            
  <?php 
    $guru_sql = "select * from tabel_guru ";
if ($id_guru != "") $guru_sql .= " where id_guru = '$id_guru'";

$guru = $this->db->query($guru_sql);

$data = array();

foreach ($guru->result_array() as $row1) {
  $id_guru = $row1['id_guru'];
  $sql = "select distinct(tabel_mapel.id_mapel) from tabel_mapel 
          inner join tabel_jadwal on (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) 
          inner join tabel_jenjang on (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) 
          inner join tabel_guru on (tabel_guru.id_guru = tabel_jadwal.id_guru) 
          inner join tabel_tahunajaran on (tabel_jadwal.id_ta = tabel_tahunajaran.id_ta) 
          where tabel_jenjang.id_jenjang = '$id_jenjang' and tabel_tahunajaran.id_ta = '$id_ta' ";

  if ($id_guru != "") $sql .= " and tabel_guru.id_guru = '$id_guru'";
  $t = $this->db->query($sql);
  foreach ($t->result_array() as $row) {
      $hadir = 0;
      $tidak_absen = 0;
      $sakit = 0;
      $izin = 0;
      $tanpa_keterangan = 0;
      $id_mapel = $row['id_mapel'];

      $begin = new DateTime($tgl);
      $end = new DateTime($tgl_akhir);
      $end->modify('+1 day');
      
      $interval = DateInterval::createFromDateString('1 day');
      $period = new DatePeriod($begin, $interval, $end);

      foreach ($period as $dt) {
          $tgltgl = $dt->format("d-m-Y");
          $day = $dt->format("l");
          $harihari = cek_hari($day);
          $harilengkap = hari_lengkap($day);

          // if ($harihari != substr($row['hari'], 2)) {
          //     continue;
          // }
          // if ($day == "Sunday") continue;

          // $banyak = mysqli_query($con, "SELECT * FROM tabel_jadwal where id_kelas = '$id_kelas' 
          // and hari = '$harilengkap' and tabel_jadwal.id_mapel = '$id_mapel' and tabel_jadwal.id_guru='$id_guru'");

          // $cek = mysqli_query($con, "SELECT * FROM tabel_absen_mapel 
          // inner join tabel_jadwal on tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal 
          // WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel' 
          // and tabel_jadwal.id_kelas = '$id_kelas' 
          // AND tabel_absen_mapel.valid = 'valid'  and tabel_jadwal.id_guru='$id_guru'");

          $banyak = $this->db->query("SELECT * FROM tabel_jadwal where hari = '$harilengkap' 
          and tabel_jadwal.id_mapel = '$id_mapel' and tabel_jadwal.id_guru='$id_guru'");

          $cek = $this->db->query("SELECT distinct(tabel_absen_mapel.id_jadwal), tabel_absen_mapel.keterangan 
          FROM tabel_absen_mapel 
          inner join tabel_jadwal on tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal 
          WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel' 
          AND tabel_absen_mapel.valid = 'valid'  and tabel_jadwal.id_guru='$id_guru'");

          $ada_absen = 0;

          // while($row_absen = mysqli_fetch_array($cek)) {
          foreach($cek->result_array() as $row_absen) {
              $keterangan = $row_absen['keterangan'];
              if ($keterangan == "") {
                  $hadir++; $ada_absen++;
              }
              else if ($keterangan == "Sakit") { 
                  $sakit++; $ada_absen++;
              }
              else if ($keterangan == "Izin") { 
                  $izin++; $ada_absen++;
              }
              else if ($keterangan == "Tanpa keterangan") {
                  $tanpa_keterangan++; $ada_absen++;
              }
          }

          $banyak = $banyak->num_rows();

          $tidak_absen += ($banyak - $ada_absen);
      }

      // $mapel = mysqli_query($con, "select * from tabel_mapel where id_mapel = '$id_mapel'");
      $mapel = $this->db->query("select * from tabel_mapel where id_mapel = '$id_mapel'");
      $mapel = $mapel->row_array();
      
      // $jenjang = mysqli_query($con, "select * from tabel_jenjang where id_jenjang = '$id_jenjang'");
      $jenjang = $this->db->query("select * from tabel_jenjang where id_jenjang = '$id_jenjang'");
      $jenjang = $jenjang->row_array();

      $data[] = array("nama"=>$row1['nama'],"jenjang"=>$jenjang['jenjang'],
                      "mapel"=>$mapel['mapel'], "hadir"=>$hadir, "tidak_absen" => $tidak_absen, 
                      "sakit" => $sakit, "izin" => $izin, "tanpa_keterangan" => $tanpa_keterangan, 
                      "nik" => $row1['nik']);    
                      
        $nama = $row1['nama'];
        $mapel = $mapel['mapel'];
                      
        echo "
        <tr>
          <td>$nama</td>
          <td>$mapel</td>
          <td align='center'>$hadir</td>
          <td align='center'>$sakit</td>
          <td align='center'>$izin</td>
          <td align='center'>$tidak_absen</td>
          <td align='center'>$tanpa_keterangan</td>
          </tr>
        "; 
    
    
  }
}
    ?>         
  </tbody>
</table>
<br><br><br>
<table border="0" align="right">
  <?php
    $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
  ?>
  <tr>
    <td><br></td>
  </tr>
  <tr>
    <td colspan="7"><b><?php echo $sql->kota . ', ' . $tglcantik; ?></b></td>    
  </tr>
  <tr>
    <td colspan="7"><b>Mengetahui, </b></td>    
  </tr>
  <tr>
    <td colspan="7"><b>Kepala Sekolah, </b></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>    
    <?php 
    
    if($sql->ttd != ""){
    ?>
    <td colspan"7"><?php if(file_exists('assets/panel/images/'.$sql->ttd)){ ?><img src="assets/panel/images/<?php echo $sql->ttd ?>" width="100px"><?php }?></td>       
    <?php 
    } 
    ?>
  </tr>
  <tr>
    <td><br><br><br><br></td>
  </tr>
  <tr>    
    <td colspan="7"><b><u><?php echo $sql->pimpinan ?></b></u></td>    
  </tr>  
  <tr>
    <td colspan="7"><?php echo $sql->nik ?></td>    
      
  </tr>
</table>
</div>