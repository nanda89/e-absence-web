
<style type="text/css">
.table {
    float:left;    
    margin-left:0px;
    border-collapse:collapse;
}
.table2 {
    float:left;    
    margin-left:5px;
    border-collapse:collapse;
}
</style>
<base href="<?php echo base_url(); ?>" />
<body onload="window.print()">
<?php 

 $d = date('dms'); 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

$ro = $dt_wali_kelas->row(); 
$tgl = gmdate("d-m-Y", time()+60*60*7); 
$hari = hari();   
$tgl_awal = $_GET['tgl_awal'];
$tgl_akhir = $_GET['tgl_akhir'];
$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>
<table border="0">
  <tr>
    <td><b>Hari/Tanggal</b></td>
    <td>: <?php echo $hari.", ".$tgl ?></td>
  </tr>
  <tr>
    <td><b>Tahun Ajaran</b></td>
    <td>: <?php echo $ro->tahun_ajaran ?></td>
  </tr>
    <td><b>Jenjang/Kelas</b></td>
    <td>: <?php echo $ro->jenjang." ".$ro->kelas ?></td>
  </tr>
  </tr>
    <td><b>Rekap</b></td>
    <td>: <?php echo $tgl_awal." s/d ".$tgl_akhir ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<?php 
$jadwal = $this->db->query("SELECT DISTINCT(tabel_jadwal.id_mapel),tabel_mapel.mapel FROM tabel_jadwal INNER JOIN tabel_mapel ON tabel_jadwal.id_mapel=tabel_mapel.id_mapel 
    WHERE id_kelas = '$ro->id_kelas' ORDER BY tabel_mapel.id_mapel ASC");
$al = $jadwal->row();
$ju = $jadwal->num_rows();
$wali = $this->db->query("SELECT * FROM tabel_guru INNER JOIN tabel_kelas ON tabel_guru.id_guru=tabel_kelas.id_guru
        WHERE tabel_kelas.id_kelas = '$ro->id_kelas'")->row();
?>
<table border="1" width="100%" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th colspan="3">Nomor</th>
      <th rowspan="2">Nama Siswa</th>      
      <th rowspan="2" width="2%">L/P</th>
      <th rowspan="2" width="2%">AK</th>
      <?php       
      foreach ($jadwal->result() as $isi) {        
      echo "
        <th colspan='4'>$isi->mapel</th>";      
      }
      ?>
    </tr>
    <tr>
      <th width="1%">No</th>      
      <th width="5%">NISN</th>      
      <th width="5%">No.Induk</th>            
      <?php       
      for ($i=1; $i <= $ju; $i++) {       
        for($a=1; $a <= 4; $a++){
          if($a==1){
            $ab = 'h';
          }elseif($a==2){
            $ab = 'i';
          }elseif($a==3){
            $ab = 's';
          }elseif($a==4){
            $ab = 'a';
          }
          else if ($a == 5) {
            $ab = 'nihil';
          }
          echo "
          <th>$ab</th>";      
        }      
      }
      ?>                  
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1;
  $jum_lk = 0; 
  $jum_pr = 0; 
  $jum = 0;  
  foreach($dt_wali_kelas->result() as $row) {
    $s = "SELECT * FROM tabel_absen_mapel RIGHT JOIN tabel_siswakelas 
          ON tabel_absen_mapel.id_penempatan = tabel_siswakelas.id_penempatan LEFT JOIN tabel_jadwal
          ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal INNER JOIN tabel_mapel
          ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
          WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen_mapel.tgl = '$tgl'
          AND tabel_absen_mapel.valid='valid'";
    $d = $this->db->query($s);
    $ambil = $d->row();
    if($d->num_rows()>0){
      if($ambil->absen==''){
        $absen = "-";
        $jam = '';  
      }else{
        $absen_m = $ambil->absen; 
        if($absen_m=='alpha'){
          $absen = "<span class='label label-danger'>$absen_m</span>";
        }elseif($absen_m=='izin'){
          $absen = "<span class='label label-warning'>$absen_m</span>";
        }elseif($absen_m=='sakit'){
          $absen = "<font class='label label-primary'>$absen_m</span>";
        }elseif($absen_m=='hadir'){
          $absen = "<font class='label label-success'>$absen_m</span>";
        }
        $jam = $ambil->jam." ";  
      }
    }else{                
      $absen_masuk = "-";
      $jam_masuk = '';                
    }
    
    if(strtolower($row->jenis_kelamin) == 'laki-laki'){
      $jk = "L";
      $jum_lk = $jum_lk + 1;
      $jum++;
    }else{
      $jk = "P";
      $jum_pr = $jum_pr + 1;
      $jum++;
    }

    if($row->agama == 'Islam'){
      $ag = "1";
    }elseif($row->agama == 'Kristen'){
      $ag = "2";
    }elseif($row->agama == 'Katholik'){
      $ag = "3";
    }elseif($row->agama == 'Hindu'){
      $ag = "4";
    }elseif($row->agama == 'Buddha'){
      $ag = "5";
    }elseif($row->agama == 'Kong Hu Chu'){
      $ag = "6";
    }elseif($row->agama == 'Tidak Diisi'){
      $ag = "7";
    }elseif($row->agama == ''){
      $ag = "-";
    }else{
      $ag = "-";
    }

    $wali = $this->db->query("SELECT * FROM tabel_guru INNER JOIN tabel_kelas ON tabel_guru.id_guru=tabel_kelas.id_guru
        WHERE tabel_kelas.id_kelas = '$row->id_kelas'")->row();
    echo "          
      <tr>
        <td>$no</td>
        <td>$row->nisn</td>      
        <td>$row->id_siswa</td>      
        <td>$row->nama_lengkap</td>  
        <td align='center'>$jk</td>
        <td align='center'>$ag</td>";

        foreach($jadwal->result() as $as){
          // $harihari = $as->hari;
          for($a=1; $a <= 4; $a++){
            if($a==1){
              $kondisi = "AND tabel_absen_mapel.absen = 'hadir'";
            }elseif($a==2){
              $kondisi = "AND tabel_absen_mapel.absen = 'izin'";              
            }elseif($a==3){
              $kondisi = "AND tabel_absen_mapel.absen = 'sakit'";
            }elseif($a==4){
              $kondisi = "AND tabel_absen_mapel.absen = 'alpha'";
            }elseif($a==5) {
              $kondisi = "";
            }

            $begin = new DateTime($tgl_awal);
            $end = new DateTime($tgl_akhir);
            $end->modify('+1 day');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            $banyak = 0;

            foreach ($period as $dt) {
                $tgltgl = $dt->format("d-m-Y");
                $day = $dt->format("l");
                $dayday = cek_hari($day);

                $cek = $this->db->query("SELECT COUNT(id_absen_mapel) as jum FROM tabel_absen_mapel INNER JOIN tabel_jadwal
                ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal
                WHERE tabel_jadwal.id_mapel = '$as->id_mapel' 
                AND tabel_absen_mapel.id_penempatan = '$row->id_penempatan' 
                AND tabel_absen_mapel.valid = 'valid'
                AND tabel_absen_mapel.tgl = '$tgltgl'
                ".$kondisi."");
              $isi = $cek->row();  
              if ($a != 5) $banyak += $isi->jum;
              else { 
                $cek1 = $this->db->query("SELECT COUNT(id_jadwal) as jum FROM tabel_jadwal 
                where hari = '$dayday' and id_mapel = '$as->id_mapel' and id_kelas = '$ro->id_kelas'")->row();
                $nihil = ($cek1->jum - $isi->jum);
                $banyak += $nihil;
              }
            }
            
                       
              echo "
              <td align='center'>$banyak</td>";      
          }                        
        }
        ?>                                
      </tr>
    <?php
    $no++;
  }
  ?>  
  </tbody>
</table>
<br><br><br>
<table border="0" width="100%" align="center">
  <tr>
    <td colspan="3"><b>Mengetahui, </b></td>    
  </tr>
  <tr>
    <td width="40%"><b>Kepala Sekolah, </b></td>
    <td width="40%"><b>Rekap: </b></td>
    <td width="20%"><b>Wali Kelas, </b></td>
  </tr>
  <tr>
    <td></td>
    <td>
      Laki : <?php echo $jum_lk ?> Orang <br>
      Perempuan : <?php echo $jum_pr ?> Orang <br>
      Jumlah : <?php echo $jum ?> Orang
    </td>    
    <td></td>
  </tr>
  <tr>
    <td colspan="3"></td>    
  </tr>
  <tr>
    <td colspan="2"><b><u><?php echo $sql->pimpinan ?></b></u></td>    
    <td><b><u><?php echo $wali->nama ?></b></u></td>    
  </tr>
  <tr>
    <td colspan="2"><?php echo $sql->nik ?></td>    
    <td><?php echo $wali->nik ?></td>    
  </tr>
</table>