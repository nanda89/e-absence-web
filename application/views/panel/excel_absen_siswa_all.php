
<style type="text/css">
.table {
    float:left;    
    margin-left:0px;
    border-collapse:collapse;
}
.table2 {
    float:left;    
    margin-left:5px;
    border-collapse:collapse;
}
</style>
<base href="<?php echo base_url(); ?>" />
<body onload="window.print()">
<?php 

 $d = date('dms'); 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

$ro   = $dt_wali_kelas->row(); 
if($tgl_awal == ''){
  $tgl1   = gmdate("d-m-Y", time()+60*60*7); 
  $tgl    = strtotime($tgl1);
  $tgl_h  = date("l",$tgl); 
  $hari   = hari($tgl_h);     
}else{
  $tgl1   = $tgl_awal; 
  $tgl    = strtotime($tgl1); 
  $tgl_h  = date("l",$tgl); 
  $hari   = hari($tgl_h);     
}

$sql  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>
<?php 
$begin = new DateTime($tgl_awal);
$end = new DateTime($tgl_akhir);
$end->modify('+1 day');

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);

foreach ($period as $dt) {
    $tgltgl = $dt->format("d-m-Y");
    $harihari = $dt->format("l");
    $tanggal_ = strtotime($tgltgl);
    // $hariharihari = hari($harihari);
    // echo $harihari;
    ?>
<table border="0">
  <tr>
    <td><b>Hari/Tanggal</b></td>
    <td>: <?php echo $hariharihari.", ".date('d-m-Y',strtotime($tgltgl)) ?></td>
  </tr>
  <tr>
    <td><b>Tahun Ajaran</b></td>
    <td>: <?php echo $ro->tahun_ajaran ?></td>
  </tr>
    <td><b>Jenjang/Kelas</b></td>
    <td>: <?php echo $ro->jenjang." ".$ro->kelas ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<?php 
$hariharihari = hari($harihari);
$hariIni = date('D', $tanggal_);
$h = convert_hari($hariIni);
$jadwal = $this->db->query("SELECT * FROM tabel_jadwal INNER JOIN tabel_mapel ON tabel_jadwal.id_mapel=tabel_mapel.id_mapel 
    WHERE id_kelas = '$ro->id_kelas' AND SUBSTR(tabel_jadwal.hari,3) = '$h' ORDER BY hari,jam_awal ASC");
$al = $jadwal->row();
$ju = $jadwal->num_rows();
$wali = $this->db->query("SELECT * FROM tabel_guru INNER JOIN tabel_kelas ON tabel_guru.id_guru=tabel_kelas.id_guru
        WHERE tabel_kelas.id_kelas = '$ro->id_kelas'")->row();
?>
<table border="1" width="85%" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th colspan="3">Nomor</th>
      <th rowspan="2">Nama Siswa</th>      
      <th rowspan="2" width="2%">L/P</th>
      <th rowspan="2" width="2%">AK</th>
      <th colspan="<?php echo $ju ?>">Jam Ke-</th>
    </tr>
    <tr>
      <th width="1%">No</th>      
      <th width="5%">NISN</th>      
      <th width="5%">No.Induk</th>            
      <?php       
      for ($i=1; $i <= $ju; $i++) {       
      echo "
        <th width='5%'>$i</th>";      
      }
      ?>                  
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1;
  $jum_lk = 0; 
  $jum_pr = 0; 
  $jum =  0;  
  foreach($dt_wali_kelas->result() as $row) {
    if (hari_apa($row->hari) != $harihari) continue;
    $s = "SELECT * FROM tabel_absen_mapel RIGHT JOIN tabel_siswakelas 
          ON tabel_absen_mapel.id_penempatan = tabel_siswakelas.id_penempatan LEFT JOIN tabel_jadwal
          ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal INNER JOIN tabel_mapel
          ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel
          WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen_mapel.tgl = '$tgltgl'
          AND tabel_absen_mapel.valid='valid'";
    $d = $this->db->query($s);
    $ambil = $d->row();
    if($d->num_rows()>0){
      if($ambil->absen==''){
        $absen = "-";
        $jam = '';  
      }else{
        $absen_m = $ambil->absen; 
        if($absen_m=='alpha'){
          $absen = "<span class='label label-danger'>$absen_m</span>";
        }elseif($absen_m=='izin'){
          $absen = "<span class='label label-warning'>$absen_m</span>";
        }elseif($absen_m=='sakit'){
          $absen = "<font class='label label-primary'>$absen_m</span>";
        }elseif($absen_m=='hadir'){
          $absen = "<font class='label label-success'>$absen_m</span>";
        }
        $jam = $ambil->jam." ";  
      }
    }else{                
      $absen_masuk = "-";
      $jam_masuk = '';                
    }
    
    if(strtolower($row->jenis_kelamin) == 'laki-laki'){
      $jk = "L";
      $jum_lk = $jum_lk + 1;
      $jum++;
    }else{
      $jk = "P";
      $jum_pr = $jum_pr + 1;
      $jum++;
    }
    
    if($row->agama == 'Islam'){
      $ag = "1";
    }elseif($row->agama == 'Kristen'){
      $ag = "2";
    }elseif($row->agama == 'Katholik'){
      $ag = "3";
    }elseif($row->agama == 'Hindu'){
      $ag = "4";
    }elseif($row->agama == 'Buddha'){
      $ag = "5";
    }elseif($row->agama == 'Kong Hu Chu'){
      $ag = "6";
    }elseif($row->agama == 'Tidak Diisi'){
      $ag = "7";
    }elseif($row->agama == ''){
      $ag = "-";
    }else{
      $ag = "-";
    }

    $wali = $this->db->query("SELECT * FROM tabel_guru INNER JOIN tabel_kelas ON tabel_guru.id_guru=tabel_kelas.id_guru
        WHERE tabel_kelas.id_kelas = '$row->id_kelas'")->row();
    echo "          
      <tr>
        <td>$no</td>
        <td>$row->nisn</td>      
        <td>$row->id_siswa</td>      
        <td>$row->nama_lengkap</td>  
        <td align='center'>$jk</td>
        <td align='center'>$ag</td>";
        foreach($jadwal->result() as $as){
          $cek = $this->db->query("SELECT * FROM tabel_absen_mapel WHERE id_jadwal = '$as->id_jadwal' 
            AND id_penempatan = '$row->id_penempatan' AND valid = 'valid' and tabel_absen_mapel.tgl='$tgltgl'");
          if($cek->num_rows() > 0){
            $isi = $cek->row();
            if($isi->absen == 'hadir'){
              $ab = "*";            
            }elseif($isi->absen == 'alpha'){
              $ab = "a";            
            }elseif($isi->absen == 'izin'){
              $ab = "i";
            }elseif($isi->absen == 'sakit'){
              $ab = "s";
            }
          }else{
            $ab = "";
          }
        echo "
          <td align='center'>$ab</td>";      
        }               
        ?>                                 
      </tr>
    <?php
    $no++;
  }
  ?>  
  </tbody>
</table>
<table class="table2" width="12%" border="1">
  <tr>
    <th>Keterangan</th>
  </tr>  
  <tr>
    <td>Mapel:</td>
  </tr>
  <?php 
  $no=1;
  foreach ($jadwal->result() as $k) {
    echo "
    <tr>
      <td>$no. $k->mapel</td>
    </tr>
    ";
    $no++;
  }
  ?>  
  <tr>
    <td>Paraf Guru</td>
  </tr>
  <?php 
  $no=1;
  foreach ($jadwal->result() as $k) {
    echo "
    <tr>
      <td>$no._________</td>
    </tr>
    ";
    $no++;
  }
  ?>  
</table>
<br><br><br>
<table border="0" width="100%" align="center">
  <tr>
    <td colspan="3">&nbsp;</td>    
  </tr>
  <tr>
    <td colspan="3"><b>Mengetahui, </b></td>    
  </tr>
  <tr>
    <td width="35%"><b>Kepala Sekolah, </b></td>
    <td width="30%"><b>Rekap: </b></td>
    <td width="35%"><b>Wali Kelas, </b></td>
  </tr>
  <tr>
    <td></td>
    <td>
      Laki : <?php echo $jum_lk ?> Orang <br>
      Perempuan : <?php echo $jum_pr ?> Orang <br>
      Jumlah : <?php echo $jum ?> Orang
    </td>    
    <td></td>
  </tr>
  <tr>
    <td colspan="3"></td>    
  </tr>
  <tr>
    <td colspan="2"><b><u><?php echo $sql->pimpinan ?></b></u></td>    
    <td><b><u><?php echo $wali->nama ?></b></u></td>    
  </tr>
  <tr>
    <td colspan="2"><?php echo $sql->nik ?></td>    
    <td><?php echo $wali->nik ?></td>    
  </tr>
</table>
<?php } ?>