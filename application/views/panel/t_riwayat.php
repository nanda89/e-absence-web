<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<b>Riwayat Pembayaran</b>
<table class="table table-bordered responsive-utilities jambo_table bulk_action" id="jasa">
    <thead>
        <tr class="headings">                                                
            <th width="5%">No </th>            
            <th>Tgl Bayar </th>
            <th>Biaya </th>                                                                                                                                
            <th>Nominal </th>                                                                    
            <th>Keterangan </th>                                                                                
            <th width="7%">Aksi </th>                                                                                                                
        </tr>
    </thead>
    <tbody> 
    <?php 
    $no=1;
    foreach ($dt_riwayat->result() as $row){
        $tgl2 = date("d F Y", strtotime($row->tgl_bayar));
        $nom = mata_uang($row->nominal);
        echo "
        <tr>
            <td>$no</td>
            <td>$row->tgl_bayar</td>
            <td>$row->biaya</td>
            <td>$nom</td>
            <td>$row->ket</td>
            <td>"; ?>
            <button title="Hapus Data"
                class="btn btn-sm btn-danger fa fa-trash-o" type="button" 
                onClick="hapus_riwayat('<?php echo $row->id_bayar; ?>','<?php echo $row->id_penempatan; ?>')"></button>
          </td>
        </tr>
        <?php        
        $no++;
    }
    ?>
    </tbody>
</table>
