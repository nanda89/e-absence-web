<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="insert"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/jam_masuk">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php                       
            if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
            ?>                  
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>  
                </button>
            </div>
            <?php
            }
                $_SESSION['pesan'] = '';                        
                    
            ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/jam_masuk/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Hari</label>
                  <div class="col-sm-4">
                    <select name="hari" id="hari" class="form-control" required>
                      <option value="0 Minggu">Minggu</option>
                      <option value="1 Senin">Senin</option>
                      <option value="2 Selasa">Selasa</option>
                      <option value="3 Rabu">Rabu</option>
                      <option value="4 Kamis">Kamis</option>
                      <option value="5 Jumat">Jumat</option>
                      <option value="6 Sabtu">Sabtu</option>
                    </select>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Jam</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="jam" name="jam">
                  </div>
                </div>        

                <div class="form-group">
                  <div class="col-sm-2">
                  </div>  
                  <div class="col-sm-10">
                    <div id="tampil_mapel"></div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="edit"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/jam_masuk">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php                       
            if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
            ?>                  
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>  
                </button>
            </div>
            <?php
            }
                $_SESSION['pesan'] = '';                        
                    
            ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/jam_masuk/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_jam_masuk ?>" name="id">
              <div class="box-body">
              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Hari</label>
                  <div class="col-sm-4">
                    <select name="hari" id="hari" required class="form-control">
                      <option value="<?php echo $row->hari ?>">
                        <?php
                          if ($row->hari == "1 Senin") echo "Senin";
                          else if ($row->hari == "2 Selasa") echo "Selasa";
                          else if ($row->hari == "3 Rabu") echo "Rabu";
                          else if ($row->hari == "4 Kamis") echo "Kamis";
                          else if ($row->hari == "5 Jumat") echo "Jumat";
                          else if ($row->hari == "6 Sabtu") echo "Sabtu";
                          else if ($row->hari == "0 Minggu") echo "Minggu";
                        ?>
                      </option>
                      <option value="0 Minggu">Minggu</option>                      
                      <option value="1 Senin">Senin</option>
                      <option value="2 Selasa">Selasa</option>
                      <option value="3 Rabu">Rabu</option>
                      <option value="4 Kamis">Kamis</option>
                      <option value="5 Jumat">Jumat</option>
                      <option value="6 Sabtu">Sabtu</option>
                    </select>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Jam</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="jam" name="jam" value="<?php echo $row->jam ?>">
                  </div>
                </div>  

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="detail"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/jam_masuk">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php                       
            if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
            ?>                  
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>  
                </button>
            </div>
            <?php
            }
                $_SESSION['pesan'] = '';                        
                    
            ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/jam_masuk/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_jam_masuk; ?>" name="id">
              <div class="box-body">
                <table width="100%" border="0">
                  <tr>
                    <td width="20%">
                      <label class="control-label">Hari</label>
                    </td>
                    <td>
                      <?php 
                        if ($row->hari == "1 Senin") echo "Senin";
                        else if ($row->hari == "2 Selasa") echo "Selasa";
                        else if ($row->hari == "3 Rabu") echo "Rabu";
                        else if ($row->hari == "4 Kamis") echo "Kamis";
                        else if ($row->hari == "5 Jumat") echo "Jumat";
                        else if ($row->hari == "6 Sabtu") echo "Sabtu";
                        else if ($row->hari == "0 Minggu") echo "Minggu";
                      ?></td>                   
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Jam</label>
                    </td>
                    <td><?php echo $row->jam; ?></td>
                  </tr>
                                     
                </table>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="ubah" class="btn btn-info">Edit</button>
                <a href="adm/jam_masuk">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>                
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }elseif($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
                      
          <a href="adm/jam_masuk/add">
            <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
            <!--button onclick="return confirm('Anda Yakin Ingin Menghapus Semua Data yang Ditandai ?')" type="button" class="btn bg-red btn-flat margin"><i class="fa fa-trash-o"></i> Hapus Banyak</button-->              
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>                            
          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php                       
            if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
            ?>                  
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>  
                </button>
            </div>
            <?php
            }
                $_SESSION['pesan'] = '';                        
                    
            ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>              
              <th width="5%">No</th>
              
              <th>Hari</th>
              <th>Jam</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_jam_masuk->result() as $row) {   
            if ($row->hari == "1 Senin") $hari = "Senin";
            else if ($row->hari == "2 Selasa") $hari = "Selasa";
            else if ($row->hari == "3 Rabu") $hari = "Rabu";
            else if ($row->hari == "4 Kamis") $hari = "Kamis";
            else if ($row->hari == "5 Jumat") $hari = "Jumat";
            else if ($row->hari == "6 Sabtu") $hari = "Sabtu";
            else if ($row->hari == "0 Minggu") $hari = "Minggu";
          echo "          
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_jam_masuk'></td>            
              <td>$no</td>              
              <td>$hari</td>
              <td>$row->jam</td>
              <td>";
              ?>
                <form method="post" action="adm/jam_masuk/process">
                <input type="hidden" name="id" value="<?php echo $row->id_jam_masuk ?>" />
                <input type="hidden" name="hari" value="<?php echo $row->hari ?>" />
                <input type="hidden" name="jam" value="<?php echo $row->jam ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-danger btn-flat btn-sm'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-edit'></i></button>
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-info btn-flat btn-sm'><i class='fa fa-eye'></i></button>

              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>        
        
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>






<script type="text/javascript">

function milih(id_mapel){
  $.ajax({
      url:"<?php echo site_url('adm/guru/cari_mapel')?>",
      type:"POST",
      data:"id_mapel="+id_mapel,
      cache:false,
      success:function(msg){                
          data=msg.split("|");
          $("#mapel").val(data[1]);
          $("#id_mapel").val(data[0]);                          
      }
  })
  $("#Amodal").modal("hide");
}
function hide_mapel(){
    $("#tampil_mapel").hide();
}
function kirim_data_mapel(){    
  $("#tampil_mapel").show();
  var nik = document.getElementById("nik_id").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "nik="+nik;                           
     xhr.open("POST", "adm/guru/t_mapel", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_mapel").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
function simpan_mapel(){
    var id_mapel            = document.getElementById("id_mapel").value;   
    var nik_js              = $("#nik_id").val();            
    if (id_mapel=="" || nik_js=="") {    
        alert("Isikan data dengan lengkap, pastikan Mapel terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/guru/save_mapel')?>",
            type:"POST",
            data:"id_mapel="+id_mapel+"&nik="+nik_js,
            cache:false,
            success:function(msg){            
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_mapel();
                    kosong();                
                }else{
                    alert('Mapel ini sudah ditambahkan');
                    kosong();                      
                }                
            }
        })    
    }
}
function kosong(args){
  $("#id_mapel").val("");
  $("#mapel").val("");   
}
function hapus_mapel(a,b){ 
    var id_ampu_js  = a;   
    var nik_js     = b;       
    $.ajax({
        url : "<?php echo site_url('adm/guru/delete_mapel')?>",
        type:"POST",
        data:"id_ampu="+id_ampu_js,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_mapel();
            }
        }
    })
}
</script>



<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/jam_masuk/ajax_bulk_delete')?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }                  
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>