<?php date_default_timezone_set("Asia/Jakarta"); ?>
<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=excel_jadwal.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
  body {
    width: 8.5in;
    height: 13.0in;
    font-family: arial;
    margin: 0mm 0mm 0mm 0mm;
  }

  @page {
    size: 8.5in 13.0in
  }

  .table {
    float: left;
    margin-left: 0px;
    border-collapse: collapse;
  }

  .table2 {
    float: left;
    margin-left: 5px;
    border-collapse: collapse;
  }

  #tabel-absen thead tr th {
    font-size: 14px;
    padding: 7px;
  }

  #tabel-absen tbody tr td {
    font-size: 12px;
  }
</style>
<?php

function helpIndoMonth($num)
{
  $monthArray = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
  if (array_key_exists($num, $monthArray)) {
    return $monthArray[$num];
  } else {
    return 'Undefined';
  }
}


?>

<?php

$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>



<?php if (file_exists('assets/panel/images/' . $sql->kop_surat)) { ?>
  <img src="<?php echo base_url() ?>assets/panel/images/<?php echo $sql->kop_surat ?>" width="550" height="100">
<?php } ?>



<table>
  <!-- 
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr><td colspan="7">&nbsp;</td></tr>
  <tr>
    <td colspan="7" style="text-align: center;"><h2>LAPORAN VALIDASI ABSENSI GURU</h2></td>
  </tr>
  <tr>
    <td colspan="7" style="text-align: center;"><b>Hari, Tanggal : <?php echo $hari . ", " . $tgl ?> </b></td>
  </tr>
  <tr>
    <td colspan="7" style="text-align: center;"><b>Tahun Ajaran : <u style="text-decoration: none;"><?php echo $tahun_ajaran ?></u></td>
  </tr>
  <tr><td colspan="7">&nbsp;</td></tr>
</table>
<center> -->

  <center>
    <table style="width: 97%;text-align: center;border-top: #000 2px solid;border-bottom: #000 2px solid;">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="7" style="text-align: center;">
          <h2>JADWAL MENGAJAR</h2>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>

  </center>
  <table>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <center>


    <table id="example2" class="table table-bordered table-hovered" border="1">
      <thead>
        <tr>
          <th width="2%">No</th>
          <th width="10%">Hari</th>
          <th width="15%">Jam</th>
          <th>Mapel</th>
          <th>Kelas</th>
          <th>Guru</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        foreach ($dt_jadwal->result() as $row) {

          echo "          
    <tr>
      <td>$no</td>
      <td>$row->hari (pekan ke-$row->minggu_ke)</td>
      <td>$row->jam_awal - $row->jam_akhir</td>
      <td>$row->mapel</td>
      <td>$row->nama_kelas</td>
      <td>$row->nama ($row->alias)</td>
      </tr>"; ?>
        <?php
          $no++;
        }
        ?>
      </tbody>
    </table>


    <table>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>

    <table id="example2" class="table table-bordered table-hovered" border="0" width="80%" style="margin-top: 12px;">
      <tfoot>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <td colspan="2" style="text-align: right;">&nbsp;<?php echo date("d F Y") ?></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <?php
          $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
          ?>
          <td colspan="2" style="text-align: right;">Kepala <?php echo $re->logo_besar ?></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="7"></td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <td colspan="2" style="text-align: right;">
            <?php
            $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
            ?>
            <u><?php echo $re->pimpinan ?></u>
          </td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <?php
          $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
          ?>
          <td colspan="2" style="text-align: right;"><?php echo $re->nik ?></td>
        </tr>
      </tfoot>
    </table>

  </center>