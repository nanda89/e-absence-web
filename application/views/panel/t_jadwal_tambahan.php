<table id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="2%">No</th>
      <th width="10%">Hari</th>
      <th width="15%">Jam</th>
      <th>Mapel</th>
      <th>Guru</th>
      <th width="10%">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
    foreach ($dt_jadwal->result() as $row) {
      if ($row->keterangan == 'masuk') {
        $status = "<span class='label label-primary pull-right'>$row->keterangan</span>";
      } elseif ($row->keterangan == 'pulang') {
        $status = "<span class='label label-danger pull-right'>$row->keterangan</span>";
      } else {
        $status = "";
      }
      echo "          
    <tr>
      <td>$no</td>
      <td>$row->hari</td>
      <td>$row->jam_awal - $row->jam_akhir $status</td>
      <td>$row->mapel (kelas $row->nama_kelas)</td>
      <td>$row->nama ($row->alias)</td>
      <td>"; ?>
      <a href="<?php echo site_url('adm/jadwal_tambahan/kelas_tambahan') .
                    "?jdwl_tambhn=$row->id_jadwal_tambahan" ?>" title="Edit Kelas" class="btn btn-sm btn-primary fa fa-edit btn-flat" type="button"></a>
      <button title="Hapus Jadwal" class="btn btn-sm btn-danger fa fa-trash-o btn-flat btn-del-kelas" type="button" data-idjdwl="<?php echo $row->id_jadwal_tambahan; ?>"></button>
      </td>
      </tr>
    <?php
      $no++;
    }
    ?>
  </tbody>
</table>