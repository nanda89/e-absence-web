<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $judul1; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/jadwal_tambahan/reset">
            <button onclick="alert('Anda yakin ingin mereset jadwal?')" class="btn bg-red btn-flat margin"><i class="fa fa-refresh"></i> Reset Jadwal</button>
          </a>
          <!--button class="btn bg-blue btn-flat margin"><i class="fa fa-download"></i> Export to Excel</button-->
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
            <div class="col-sm-4">
              <select id="id_tahun" class="form-control">
                <option value=''>Pilih Tahun Ajaran</option>
                <?php
foreach ($dt_tahun->result() as $dt_tahun) {
    echo "
                <option value='$dt_tahun->id_ta'>$dt_tahun->tahun_ajaran</option>";
}?>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 2</label>
            <div class="col-sm-4">
              <select id="id_pembimbing2" name="id_pembimbing2" class="form-control">
                <option value=''>Pilih Pembimbing 2</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
            <div class="col-sm-4">
              <select id="id_jenjang" name="jenjang" class="form-control">
                <option value=''>Pilih Jenjang</option>
                <?php
foreach ($dt_jenjang->result() as $dt_jenjang) {
    echo "
                <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
}?>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 3</label>
            <div class="col-sm-4">
              <select id="id_pembimbing3" name="id_pembimbing3" class="form-control">
                <option value=''>Pilih Pembimbing 3</option>
              </select>
            </div>

          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
            <div class="col-sm-4">
              <input class="form-control" id="nama_kelas" placeholder="Kelas" autocomplete="off">
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
            <div class="col-sm-2">
              <input class="form-control" id="tanggal" placeholder="Mulai">
            </div>
            <div class="col-sm-2">
              <input class="form-control" id="tanggal2" placeholder="Selesai">
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Mapel Tambahan</label>
            <div class="col-sm-4">
              <select id="id_mapel" name="id_mapel" class="form-control">
                <option value=''>Pilih Mapel Tambahan</option>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Hari / Pekan ke-</label>
            <div class="col-sm-2">
              <select id="hari" class="form-control">
                <option value=''>Pilih Hari</option>
                <option value="Minggu">Minggu</option>
                <option value="Senin">Senin</option>
                <option value="Selasa">Selasa</option>
                <option value="Rabu">Rabu</option>
                <option value="Kamis">Kamis</option>
                <option value="Jumat">Jumat</option>
                <option value="Sabtu">Sabtu</option>
              </select>
            </div>

            <div class="col-sm-2">
              <select id="pekan" class="form-control">
                <option value=''>Pilih Pekan</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
              </select>
            </div>

          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Guru</label>
            <div class="col-sm-4">
              <select id="id_guru" name="id_guru" class="form-control">
                <option value=''>Pilih Guru</option>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Jam</label>
            <div class="col-sm-2">
              <input class="form-control" id="jam_awal" placeholder="mulai jam">
            </div>
            <div class="col-sm-2">
              <input class="form-control" id="jam_akhir" placeholder="sampai jam">
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 1</label>
            <div class="col-sm-4">
              <select id="id_pembimbing1" name="id_pembimbing1" class="form-control">
                <option value=''>Pilih Pembimbing 1</option>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
            <div class="col-sm-2">
              <select id="isi" class="form-control">
                <option value=''>Pilih Status</option>
                <option value="masuk">Masuk</option>
                <option value="pulang">Pulang</option>
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button type="button" onclick="kirim_data_jadwal()" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
              <!-- <h3 class="box-title">                           -->
              <button type="button" onclick="excel()" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</button>
              <button type="button" onclick="print()" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</button>
              <!-- </h3> -->
            </div>
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <!-- <div class="col-sm-4">

            </div> -->
            <div class="col-sm-4">
              <button type="button" onclick="simpan_jadwal()" class="btn btn-info btn-flat">Save</button>
              <!--button type="button" onclick="tes()" class="btn btn-info btn-flat">Tes</button-->
              <button type="button" onclick="re()" class="btn btn-default pull-right btn-flat">Reset</button>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body">
        <div id="tampil_jadwal">
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
  $("#id_jenjang").change(function() {
    var id_jenjang = $("#id_jenjang").val();
    get_guru(id_jenjang);
    get_mapel(id_jenjang);
  });


  $(document).on('click', '.btn-del-kelas', function() {
    if (window.confirm('Apakah Anda Yakin Untuk Menghapus Jadwal Tambahan ini ?')) {

      var id_jadwal = $(this).data('idjdwl');

      $.ajax({
        url: "<?php echo site_url('adm/jadwal_tambahan/delete_jadwal') ?>",
        type: "POST",
        data: "id_jadwal=" + id_jadwal,
        cache: false,
        success: function(msg) {
          data = msg.split("|");
          if (data[0] == "nihil") {
            alert("Jadwal Tambahan Berhasil Dihapus.");
            kirim_data_jadwal();
          } else {
            alert("Jadwal Tambahan Gagal Dihapus.");
          }
        }
      })
    } else {
      kirim_data_jadwal();
    }
  });

  function simpan_jadwal() {
    var id_tahun = document.getElementById("id_tahun").value;
    var id_jenjang = $("#id_jenjang").val();
    var nama_kelas = $("#nama_kelas").val();
    var id_mapel = $("#id_mapel").val();
    var id_guru = $("#id_guru").val();
    var id_pendamping1 = $("#id_pembimbing1").val();
    var id_pendamping2 = $("#id_pembimbing2").val();
    var id_pendamping3 = $("#id_pembimbing3").val();
    var tgl_mulai = $("#tanggal").val();
    var tgl_akhir = $("#tanggal2").val();
    var hari = $("#hari").val();
    var pekan = $("#pekan").val();
    var jam_awal = $("#jam_awal").val();
    var jam_akhir = $("#jam_akhir").val();
    var isi = $("#isi").val();

    if (id_tahun == "" || id_jenjang == "" || nama_kelas == "" || id_mapel == "" ||
      id_guru == "" || id_pembimbing1 == "" || id_pembimbing2 == "" ||
      id_pembimbing3 == "" || tgl_mulai == "" || tgl_akhir == "" ||
      pekan == "" || hari == "" || jam_awal == "" || jam_akhir == "") {
      alert("Isikan data dengan lengkap...!");
      return false;
    } else {
      $.ajax({
        url: "<?php echo site_url('adm/jadwal_tambahan/save') ?>",
        type: "POST",
        data: "&id_tahun=" + id_tahun + "&id_jenjang=" + id_jenjang +
          "&nama_kelas=" + nama_kelas + "&id_mapel=" + id_mapel + "&id_guru=" + id_guru +
          "&id_pendamping1=" + id_pendamping1 + "&id_pendamping2=" + id_pendamping2 +
          "&id_pendamping3=" + id_pendamping3 + "&tgl_mulai=" + tgl_mulai +
          "&tgl_akhir=" + tgl_akhir + "&pekan=" + pekan +
          "&hari=" + hari + "&jam_awal=" + jam_awal + "&jam_akhir=" + jam_akhir +
          "&isi=" + isi,
        cache: false,
        success: function(msg) {
          data = msg.split("|");
          if (data[0] == "nihil") {
            kirim_data_jadwal();
            kosong_jadwal();
          } else {
            alert(data[0]);
            kosong_jadwal();
          }
        }
      })
    }
  }

  function tes() {
    var isi = $("#isi").val();
    alert(isi);
  }

  function re(args) {
    $("#hari").val("");
    $("#jam_awal").val("");
    $("#jam_akhir").val("");
    $("#id_mapel").val("");
    $("#id_guru").val("");
    $("#id_tahun").val("");
    $("#nama_kelas").val("");
    $("#id_jenjang").val("");
    $("#tingkat").val("");
    $("#isi").val("");
    $("#tampil_jadwal").hide();
  }

  function kosong_jadwal(args) {
    $("id_tahun").val("");
    $("#id_jenjang").val("");
    $("#nama_kelas").val("");
    $("#id_mapel").val("");
    $("#id_guru").val("");
    $("#id_pembimbing1").val("");
    $("#id_pembimbing2").val("");
    $("#id_pembimbing3").val("");
    $("#tanggal").val("");
    $("#tanggal2").val("");
    $("#hari").val("");
    $("#pekan").val("");
    $("#jam_awal").val("");
    $("#jam_akhir").val("");
    $("#isi").val("");

  }

  function kirim_data_jadwal() {
    $("#tampil_jadwal").show();
    var id_tahun = document.getElementById("id_tahun").value;
    var id_jenjang = document.getElementById("id_jenjang").value;
    var nama_kelas = document.getElementById("nama_kelas").value;
    var id_mapel = document.getElementById("id_mapel").value;
    var id_guru = document.getElementById("id_guru").value;
    var id_pembimbing1 = document.getElementById("id_pembimbing1").value;
    var xhr;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
      xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 8 and older
      xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //var data = "birthday1="+birthday1_js;
    var data = "id_tahun=" + id_tahun + "&id_jenjang=" + id_jenjang +
      "&nama_kelas=" + nama_kelas + "&id_mapel=" + id_mapel + "&id_guru=" + id_guru +
      "&id_pembimbing1=" + id_pembimbing1;
    xhr.open("POST", "adm/jadwal_tambahan/t_jadwal", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(data);
    xhr.onreadystatechange = display_data;

    function display_data() {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          document.getElementById("tampil_jadwal").innerHTML = xhr.responseText;
        } else {
          alert('There was a problem with the request.');
        }
      }
    }
  }

  function excel() {
    var id_tahun = document.getElementById("id_tahun").value;
    var id_jenjang = document.getElementById("id_jenjang").value;
    var nama_kelas = document.getElementById("nama_kelas").value;
    var id_mapel = document.getElementById("id_mapel").value;
    var id_guru = document.getElementById("id_guru").value;
    var id_pembimbing1 = document.getElementById("id_pembimbing1").value;

    window.open("<?php echo site_url('adm/jadwal_tambahan/export_now') ?>" + "?a=1&id_tahun=" + id_tahun + "&id_jenjang=" + id_jenjang +
      "&nama_kelas=" + nama_kelas + "&id_guru=" + id_guru + "&id_mapel=" + id_mapel +
      "&id_pembimbing1=" + id_pembimbing1);
  }

  function print() {
    var id_tahun = document.getElementById("id_tahun").value;
    var id_jenjang = document.getElementById("id_jenjang").value;
    var nama_kelas = document.getElementById("nama_kelas").value;
    var id_guru = document.getElementById("id_guru").value;
    window.open("<?php echo site_url('adm/jadwal_tambahan/export_now') . "?a=2&id_tahun=" ?>" + id_tahun + "&id_jenjang=" + id_jenjang + "&nama_kelas=" + nama_kelas + "&id_guru=" + id_guru)
  }

  function get_guru(id_jenjang) {
    $.ajax({
      url: "<?php echo site_url('adm/jadwal_tambahan/get_guru') ?>",
      type: "POST",
      data: "id_jenjang=" + id_jenjang,
      cache: false,
      success: function(msg) {

        var msg1 = "<option value = ''> Pilih Guru </option>" + msg;
        var msg2 = "<option value = ''> Pilih Pendamping 1 </option>" + msg;
        var msg3 = "<option value = ''> Pilih Pendamping 2 </option>" + msg;
        var msg4 = "<option value = ''> Pilih Pendamping 3 </option>" + msg;

        $("#id_guru").html(msg1);
        $("#id_pembimbing1").html(msg2);
        $("#id_pembimbing2").html(msg3);
        $("#id_pembimbing3").html(msg4);
      }
    })
  }

  function get_mapel(id_jenjang) {
    $.ajax({
      url: "<?php echo site_url('adm/jadwal_tambahan/get_mapel') ?>",
      type: "POST",
      data: "id_jenjang=" + id_jenjang,
      cache: false,
      success: function(msg) {
        $("#id_mapel").html(msg);
      }
    })
  }
</script>