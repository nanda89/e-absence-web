<?php date_default_timezone_set("Asia/Jakarta");?>
<?php 
  $d = date('dms'); 
  header("Content-type: application/octet-stream");
  header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
  header("Pragma: no-cache");
  header("Expires: 0");
?>
<table border="1" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="1%">No</th>      
      <th width="5%">NISN</th>      
      <th width="5%">No.Induk</th>      
      <th>Nama Lengkap</th>
      <th>Kelas</th>   
      <th>Tanggal</th>       
      <th width="15%">Jam Absen Masuk</th>
      <th width="15%">Jam Absen Pulang</th>                      
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1; 
  foreach($dt_wali_kelas->result() as $row) { 
  $tanggal = date("d F Y", strtotime($row->tgl));
  $tgl = date('d-m-Y');
  $s = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
        WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$row->tgl'
        AND tabel_absen.valid_masuk='valid'";
  $d = $this->db->query($s);
  $ambil = $d->row();
  if($d->num_rows()>0){
    if($ambil->absen_masuk==''){
      $absen_masuk = "-";
      $jam_masuk = '';  
    }else{
      $absen_m = $ambil->absen_masuk; 
      if($absen_m=='alpha'){
        $absen_masuk = "<span class='label label-danger'>$absen_m</span>";
      }elseif($absen_m=='izin'){
        $absen_masuk = "<span class='label label-warning'>$absen_m</span>";
      }elseif($absen_m=='sakit'){
        $absen_masuk = "<span class='label label-primary'>$absen_m</span>";
      }elseif($absen_m=='hadir'){
        $absen_masuk = "<span class='label label-success'>$absen_m</span>";
      }
      $jam_masuk = $ambil->jam_masuk." ";  
    }
  }else{                
    $absen_masuk = "-";
    $jam_masuk = '';                
  }


  $sa = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
        WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$row->tgl'
        AND tabel_absen.valid_pulang='valid'";
  $da = $this->db->query($sa);
  $ambil = $da->row();
  if($da->num_rows()>0){
    if($ambil->absen_pulang==''){
      $absen_pulang = "-";  
      $jam_pulang = '';
    }else{                  
      $absen_p = $ambil->absen_pulang; 
      if($absen_p=='alpha'){
        $absen_pulang = "<span class='label label-danger'>$absen_p</span>";
      }elseif($absen_p=='izin'){
        $absen_pulang = "<span class='label label-warning'>$absen_p</span>";
      }elseif($absen_p=='sakit'){
        $absen_pulang = "<span class='label label-primary'>$absen_p</span>";
      }elseif($absen_p=='hadir'){
        $absen_pulang = "<span class='label label-success'>$absen_p</span>";
      }
      $jam_pulang = $ambil->jam_pulang." "; 
    }
  }else{
    $absen_pulang = "-";                
    $jam_pulang = '';
  }


  echo "          
    <tr>
      <td>$no</td>
      <td>$row->id_siswa</td>      
      <td>$row->nisn</td>      
      <td>$row->nama_lengkap</td>  
      <td>$row->jenjang $row->kelas</td>                  
      <td>$tanggal</td>                         
      <td>$jam_masuk$absen_masuk</td>
      <td>$jam_pulang$absen_pulang</td>
      "; ?>                                 
    </tr>
  <?php
  $no++;
  }
  ?>
  </tbody>
  </table>