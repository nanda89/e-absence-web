<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "insert") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/tahunajaran">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/tahunajaran/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Tahun Ajaran" name="tahun_ajaran">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Tgl Awal </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal" placeholder="Tanggal Awal" name="tgl_awal" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl Akhir</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal2" placeholder="Tanggal Akhir" name="tgl_akhir" autocomplete="off">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "edit") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/tahunajaran">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/tahunajaran/process" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $row->id_ta ?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->tahun_ajaran; ?>" placeholder="Tahun Ajaran" name="tahun_ajaran">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Tgl Akhir</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal2" value="<?php echo $row->tgl_akhir; ?>" placeholder="Tanggal Akhir" name="tgl_akhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl Awal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal" value="<?php echo $row->tgl_awal; ?>" placeholder="Tanggal Awal" name="tgl_awal">
                  </div>
                </div>
                <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                  <select name="status" id="status" class="form-control">
                    <option value="<?php echo $row->status; ?>"><?php echo $row->status; ?></option>
                    <?php
if ($row->status == "Aktif") {
        echo "<option value='Tidak Aktif'>Tidak Aktif</option>";

    } else {
        echo "<option value='Aktif'>Aktif</option>";
    }
    ?>
                  </select>
                </div>
              </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <a href="adm/tahunajaran">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "view") {
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/tahunajaran/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>
              <th>Tahun Ajaran</th>
              <th>Status</th>
              <th>Tgl Awal</th>
              <th>Tgl Akhir</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php
$no = 1;
    foreach ($dt_ta->result() as $row) {
        $tgl = date("d F Y", strtotime($row->tgl_awal));
        $tgl2 = date("d F Y", strtotime($row->tgl_akhir));
        echo "
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_ta'></td>
              <td>$no</td>
              <td>$row->tahun_ajaran</td>";
        if ($row->status == "Aktif") {
            echo "<td class='text-center'><span class='label label-success'>$row->status</span></td>";
        } else {
            echo "<td class='text-center'><span class='label label-danger'>$row->status</span></td>";
        }
        echo "<td>$row->tgl_awal</td>
              <td>$row->tgl_akhir</td>
              <td>";
        ?>
              <form method="post" action="adm/tahunajaran/process">
                <input type="hidden" name="id" value="<?php echo $row->id_ta ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
              </form>
              </td>
            </tr>
          <?php
$no++;
    }
    ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>


<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/tahunajaran/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            window . location . reload();

          },
          error: function (jqXHR, textStatus, errorThrown){
            // alert('Error deleting data');
            window . location . reload();

          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>