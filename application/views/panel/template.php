<?php
$d = date('dms'); 
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=template-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>      
        <b>
            Catatan : <br>
            1. Save As file ini dengan tipe (*.csv) sebelum upload ke sistem dan hapus baris ini. <br>
            2. Sesuaikan field id_jenjang, id_kelas dan id_ta dengan tabel jenjang yg tersedia.
        </b>
        <table class="table table-hover table-striped" border="1">
            <thead>
            <tr>                
                <!-- <th>id_siswa</th>                             -->
                <th>id_jenjang</th>
                <th>id_ta</th>
                <th>id_kelas</th>
                <th>nama_lengkap</th>
                <th>nisn</th>
                <th>jenis_kelamin</th>                                            
                <th>tempat_lahir</th>
                <th>tgl_lahir</th>                                                                                                                                   
                <th>agama</th>
                <th>alamat</th>
                <th>no_hp</th>
                <th>email</th>
                <th>asal_sekolah</th>
                <th>ket_asal</th>
                <th>nama_ayah</th>
                <th>nama_ibu</th>
                <th>tgl_daftar</th>                
            </tr>
            </thead>
            <tbody>
                <tr>
                    <!-- <td></td> -->
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>                                           
            </tbody>
        </table>
        <br><br><br>
        <table border="1">
            <tr>
                <td>ID Jenjang</td>
                <td>Jenjang</td>
            </tr>
            <?php
            $t = $this->m_jenjang->get_all();
            foreach ($t->result() as $k) {
                 echo "
                    <tr>
                        <td>$k->id_jenjang</td>
                        <td>$k->jenjang</td>
                    </tr>
                 ";
             }             
            ?>            
        </table>
        <br><br>
        <table border="1">
            <tr>
                <td>ID Kelas</td>
                <td>Kelas</td>
            </tr>
            <?php
            $d = $this->m_jenjang->get_all_kelas_by_last_ta();
            foreach ($d->result() as $w) {
                 echo "
                    <tr>
                        <td>$w->id_kelas</td>
                        <td>$w->kelas</td>
                    </tr>
                 ";
             }             
            ?>            
        </table>
        <br><br>
        <table border="1">
            <tr>
                <td>ID Ta</td>
                <td>Tahun Ajaran</td>
            </tr>
            <?php
            $q = $this->m_tahunajaran->get_last();
            foreach ($q->result() as $s) {
                 echo "
                    <tr>
                        <td>$s->id_ta</td>
                        <td>$s->tahun_ajaran</td>
                    </tr>
                 ";
             }             
            ?>            
        </table>
    
