<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $judul1; ?></li>
    </ol>
  </section>
  <section class="content">
    <?php
if ($set == "insert") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/kalender">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/kalender/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="tanggal-agenda" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal-agenda" placeholder="Tanggal" name="tanggal">
                  </div>
                </div>
                <div class="form-group">
                  <label for="warna" class="col-sm-2 control-label">Warna</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control colorpicker1" id="warna" placeholder="#fff" name="warna">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Agenda</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Agenda" name="agenda">
                  </div>
                </div>
                <div class="form-group">
                  <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="keterangan" placeholder="Keterangan Tambahan" name="keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="libur" value="1"> Libur
                      </label>
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "edit") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/kalender">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/kalender/process" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $row->id_kalender ?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tanggal" value="<?php echo date('d-m-Y', strtotime($row->tanggal)); ?>" placeholder="Tanggal" name="tanggal">
                  </div>
                </div>
                <div class="form-group">
                  <label for="warna" class="col-sm-2 control-label">Warna</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control colorpicker1" id="warna" placeholder="#fff" value="<?php echo $row->warna; ?>" name="warna">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Agenda</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->agenda; ?>" placeholder="Agenda" name="agenda">
                  </div>
                </div>
                <div class="form-group">
                  <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="keterangan" value="<?php echo $row->keterangan; ?>" placeholder="Keterangan Tambahan" name="keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="libur" value="1" <?php echo ($row->libur) ? 'checked' : ''; ?>> Libur
                      </label>
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <a href="kalender/view">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "view") {
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/kalender/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>
              <th>Tanggal</th>
              <th>Agenda</th>
              <th>Keterangan</th>
              <th>Libur</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php $no = 1;
    foreach ($dt_ka->result() as $row): ?>
            <tr>
              <td><input type="checkbox" class="data-check" value="<?php echo $row->id_kalender; ?>"></td>
              <td><?php echo $no; ?></td>
              <td><?php echo $row->tanggal; ?></td>
              <td><?php echo $row->agenda; ?></td>
              <td><?php echo $row->keterangan; ?></td>
              <td><?php echo ($row->libur) ? 'Ya' : 'Tidak'; ?></td>
              <td>
              <form method="post" action="adm/kalender/process">
                <input type="hidden" name="id" value="<?php echo $row->id_kalender ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
              </form>
              </td>
            </tr>
          <?php $no++;
    endforeach;?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>

<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/kalender/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>
