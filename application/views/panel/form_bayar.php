<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
            <td colspan="2">
              <b>Masukkan Data Siswa</b>
            </td>
          </tr>
          <tr>          
            <td>
              No.Induk
            </td>
            <td>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="id_siswa" placeholder="Masukkan No.Induk Siswa">
              </div>
              <div class="col-sm-6">
                <button onclick="cek_nisn()" class="btn bg-maroon btn-flat">Cek</button>
              </div>
            </td>
          </tr>
          <tr>
            <td width='20%'>
              Nama
            </td>
            <td>
              <div class="col-sm-12">
                <input type="text" class="form-control" readonly id="nama" placeholder="Nama Lengkap">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              Jenjang
            </td>
            <td>
              <div class="col-sm-4">
                <input type="text" readonly class="form-control" id="jenjang" placeholder="Jenjang">
              </div>              
              <div class="col-sm-4">
                <input type="text" readonly class="form-control" id="kelas" placeholder="Kelas">
              </div> 
              <div class="col-sm-4">
                <input type="text" readonly class="form-control" id="tahun_ajaran" placeholder="Tahun Ajaran">
              </div> 
              <input type="hidden" class="form-control" id="id_penempatan">
            </td>
          </tr>
        </table>

        <table class="table table-bordered">
          <tr>
            <td colspan="2">
              <b>Masukkan Data Pembayaran</b>
            </td>
          </tr>
          <tr>          
            <td width='20%'>
              Jenis Pembayaran
            </td>
            <td>
              <div class="col-sm-6">
                <select id="id_biaya" name="id_biaya" class="form-control" onchange="cek()">                    
                  <option value=''>Pilih Jenis Pembayaran</option>
                  <!--?php 
                  foreach($dt_biaya->result() as $dt_biaya) {                           
                  echo "
                  <option value='$dt_biaya->id_biaya'>$dt_biaya->biaya</option>";
                  } ?-->
                </select>
              </div>              
            </td>
          </tr>  
          <tr>
            <td>
              Nominal
            </td>
            <td>
              <div class="col-sm-6">
                <input readonly type="text" class="form-control" id="nominal" placeholder="Nominal">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              Uraian
            </td>
            <td>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="uraian" placeholder="Uraian">
              </div>
            </td>
          </tr> 
          <tr>
            <td>
              Tgl Bayar
            </td>
            <td>
              <div class="col-sm-4">
                <input type="text" class="form-control" value="<?php echo date('m/d/Y') ?>" id="tanggal" placeholder="Tanggal Bayar">
              </div>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>              
              <div class="col-sm-12">
                <button type="button" onclick="simpan_bayar()" class="btn btn-info">Save</button>
                <button type="button" onclick="re()" class="btn btn-default">Reset</button>
                <button type="button" onclick="cetak()" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print</button>                              
              </div>
            </td>
          </tr>       
        </table>
        <br>
        <div id="tampil_riwayat">
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->    
  </section>
</div>

<script type="text/javascript">
function cek_nisn(){
  var nisn_js=document.getElementById("id_siswa").value;
  if (nisn_js=="") {    
    alert("Masukkan NISN Terlebih Dahulu...!");
    return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/form_bayar/cari_nisn')?>",
        type:"POST",
        data:"nisn="+nisn_js,   
        cache:false,   
        success: function(msg){ 
          data=msg.split("|");
          if(data[0]=="nihil"){
            $("#nama").val(data[1]);                        
            $("#jenjang").val(data[2]);          
            $("#kelas").val(data[4]);          
            $("#tahun_ajaran").val(data[5]);          
            $("#id_penempatan").val(data[6]);          
            cek_biaya(data[3]);
            kirim_data_riwayat(data[6]);
          }else{
            alert(data[0]);
            re();
          }
        }        
    })
  }
}
function cek_biaya(a){
 var id_jenjang=a;  
  $.ajax({
      url : "<?php echo site_url('adm/form_bayar/cari_biaya')?>",
      type:"POST",
      data:"id_jenjang="+id_jenjang,   
      cache:false,   
      success: function(msg){ 
        $("#id_biaya").html(msg);
      }        
  })  
}
function cetak(){
  var id_siswa  = document.getElementById("id_siswa").value;
  var tanggal   = document.getElementById("tanggal").value;
  if(id_siswa=="" || tanggal == ""){
    alert("Pastikan data ID Siswa dan tanggal sudah terisi dengan benar");
    return false;
  }else{
    window.location.href= "adm/form_bayar/cetak?id="+id_siswa+"&tanggal="+tanggal;
  } 
}
function cek(){
  var id_biaya=document.getElementById("id_biaya").value;
  $.ajax({
      url : "<?php echo site_url('adm/form_bayar/cek_bi')?>",
      type:"POST",
      data:"id_biaya="+id_biaya,   
      cache:false,   
      success: function(msg){ 
        data=msg.split("|");                
        $("#nominal").val(data[0]);                                        
        $("#uraian").focus();

      }        
  })
}
function simpan_bayar(){
    var nisn            = document.getElementById("id_siswa").value;   
    var uraian          = $("#uraian").val();
    var id_biaya        = $("#id_biaya").val();
    var tanggal         = $("#tanggal").val();    
    var id_penempatan   = $("#id_penempatan").val();    
    
    if (nisn=="" || tanggal=="" || id_biaya == "") {    
        alert("Isikan data dengan lengkap...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/form_bayar/save')?>",
            type:"POST",
            data:"nisn="+nisn+"&uraian="+uraian+"&id_biaya="+id_biaya+"&tanggal="+tanggal+"&id_penempatan="+id_penempatan,
            cache:false,
            success:function(msg){            
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_riwayat(id_penempatan);
                    kelar();                
                }else{
                    alert(data[0]);
                }                
            }
        })    
    }
}
function re(){
  kosong();
  $("#tampil_riwayat").hide();
}
function kosong(args){
  $("#id_siswa").val("");
  $("#nama").val("");
  $("#jenjang").val("");    
  $("#nominal").val("");      
  $("#uraian").val("");            
  $("#id_biaya").val("");   
  $("#jenjang").val("");   
  $("#tahun_ajaran").val("");   
  $("#id_penempatan").val("");   
}
function kelar(args){  
  $("#nominal").val("");      
  $("#uraian").val("");           
  $("#id_biaya").val("");   
}
function hapus_riwayat(a,b){ 
    var id_bayar  = a;     
    $.ajax({
        url : "<?php echo site_url('adm/form_bayar/delete_riwayat')?>",
        type:"POST",
        data:"id_bayar="+id_bayar,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_riwayat(b);
            }
        }
    })
}
function kirim_data_riwayat(id){    
  $("#tampil_riwayat").show();
  //var nisn = document.getElementById("nisn").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id="+id;                           
     xhr.open("POST", "adm/form_bayar/t_riwayat", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_riwayat").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>
<script type="text/javascript">
$('document').ready(function(){
  $('#id_siswa').keypress(function(e){
    if(e.which == 13){//Enter key pressed
      cek_nisn();//Trigger search button click event
    }
  });
  
});
</script>