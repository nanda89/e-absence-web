<base href="<?php echo base_url(); ?>" />
<link rel="stylesheet" href="assets/panel/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/panel/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
body{
	margin: 30px;
}
</style>
<body onload="window.print()">
<?php $r=$data_cetak->row(); ?>
<table width="90%">
	<tr>
		<td width="15%">Nama</td>
		<td>: <?php echo $r->nama_lengkap ?></td>
	</tr>
	<tr>
		<td>Jenjang - Kelas</td>
		<td>: <?php echo $r->jenjang ?>  <?php echo $r->kelas ?></td>
	</tr>
	<tr>
		<td>Tahun Ajaran</td>
		<td>: <?php echo $r->tahun_ajaran ?></td>
	</tr>
</table>
<hr>
<table width="30px" class="table table-bordered table-hovered">
	<thead>
		<tr>
			<th width="2%">No</th>
			<th>Jenis Pembayaran</th>
			<th>Nominal</th>
			<th>Tgl.Bayar</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no=1; 
		foreach ($data_cetak->result() as $row) {
			$tgl = date("d F Y", strtotime($row->tgl_bayar));
			echo"
			<tr>
				<td>$no</td>
				<td>$row->biaya</td>
				<td>$row->nominal</td>
				<td>$tgl</td>
				<td>$row->ket</td>
			</tr>
			";
			$no++;
		}
		$tg = date("d F Y");
		?>
	</tbody>
</table>

Jambi, <?php echo $tg ?> <br>
Bagian Keuangan <br><br><br>
(........................)

</body>