<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">            
            <a href="adm/kelulusan">
              <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>              
            </a>
            <br>
              <b>Petunjuk :</b>
              <ul>                                
                <li>Tentukan siswa yang telah lulus</li>
                <li>Klik tombol '>' untuk menyatakan kelulusan siswa</li>                                
              </ul>
              <?php 
              $r = $dt_siswa->row();
              if($dt_siswa->num_rows()>0){                
              ?>              
              <b>Jenjang : <?php echo $r->jenjang; ?></b> <br>
              <b>Kelas Asal : <?php echo $r->kelas; ?></b> <br>
              <b>Tahun Ajaran : <?php echo $r->tahun_ajaran; ?></b> <br>
              <input type="hidden" id="ke" value="<?php echo $r->id_kelas ?>">
              <input type="hidden" id="ta" value="<?php echo $r->id_ta ?>">              
              <?php 
              }else{                
              ?>
              <b>Data tidak ditemukan</b> <br>
              <?php 
              }
              ?>              
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div id="tampil_siswakelas"></div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">          
            <input type="hidden" value="<?php echo $dt_j ?>" id="id_jenjang">
            <div class="form-group">
              <!-- <label for="inputEmail3" class="col-sm-2 control-label">Angkatan</label>
              <div class="col-sm-4">
                <select id="angkatan" required class="form-control">                    
                  <option value=''>Pilih Angkatan</option>
                  <?php 
                  for ($i=2005; $i<=2025 ; $i++) { 
                    echo"
                    <option>$i</option>               
                    ";
                  }
                  ?>
                </select>
              </div>     -->
              <label for="inputEmail3" class="col-sm-2 control-label">Tgl.Lulus</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="tanggal" name="tgl_lulus" placeholder="Tanggal Lulus">
              </div>        
            </div>                      
                  
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <button type="button" onclick="kirim_data_lulus()" class="btn bg-maroon btn-flat margin"><i class="fa fa-check"></i> Set Data Kelulusan</button>              
              </div>             
            </div>  
          </form>  
        </div> 
          <div class="box-body">
            <div id="tampil_kelulusan">
            </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>

$("#tingkat").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  $.ajax({
    url : "<?php echo site_url('adm/kenaikan/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>

<script type="text/javascript">
function hapus_kenaikan(a){ 
  var id_kenaikan     = a;     
  $.ajax({
      url : "<?php echo site_url('adm/kenaikan/delete_kenaikan')?>",
      type:"POST",
      data:"id_kenaikan="+id_kenaikan,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_kenaikan();
          }
      }
  })
}
function set_kelas(id){
  var id_tahun    = document.getElementById("id_tahun").value;   
  var id_kelas    = $("#id_kelas").val();
  var id_siswa    = id; 

  if(id_tahun=="" || id_kelas=="" ){
      alert("Pastikan kelas tujuan sudah di-set...!");
      return false;
  }else{
      $.ajax({
          url : "<?php echo site_url('adm/kenaikan/save')?>",
          type:"POST",
          data:"id_tahun="+id_tahun+"&id_kelas="+id_kelas+"&id_siswa="+id_siswa,
          cache:false,
          success:function(msg){            
              data=msg.split("|");
              if(data[0]=="nihil"){
                kirim_data_kenaikan();                                
              }else{
                alert("Siswa ini telah memiliki kelas");
              }                
          }
      })    
  }
}
function kirim_data_lulus(){    
 $("#tampil_kelulusan").show();  
  var tanggal    = document.getElementById("tanggal").value;     
  if(tanggal==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;        
     xhr.open("POST", "adm/kelulusan/t_kelulusan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");     
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_kelulusan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}

function kirim_data_siswa(){    
  $("#tampil_siswakelas").show();
  var id_tahun    = document.getElementById("ta").value;     
  var id_kelas    = document.getElementById("ke").value;     
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/kelulusan/t_siswakelas", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_siswakelas").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
window.onload=kirim_data_siswa();
</script>
<script type="text/javascript">
function luluskan(a,b){ 
  var id_penempatan   = a;     
  var id_siswa        = b;  
  var id_ta           = $("#ta").val();
  var id_kelas        = $("#ke").val();  
  var tanggal         = $("#tanggal").val();  

  if(tanggal==""){
      alert("Pastikan dan tanggal lulus sudah di-set...!");
      return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/kelulusan/luluskan')?>",
        type:"POST",
        data:"id_penempatan="+id_penempatan+"&id_siswa="+id_siswa+"&id_ta="+id_ta+"&id_kelas="+id_kelas+"&tanggal="+tanggal,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_lulus();
              kirim_data_siswa();              
            }else{
              alert(data[0]);
            }
        }
    })
  } 
}
function turunkan(a,b){ 
  var id_penempatan   = a;     
  var id_siswa        = b;  

  var id_tahun    = $("#ta").val();
  var id_kelas    = $("#ke").val();

  if(id_tahun=="" || id_kelas==""){
      alert("Pastikan kelas tujuan sudah di-set...!");
      return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/kenaikan/turunkan')?>",
        type:"POST",
        data:"id_penempatan="+id_penempatan+"&id_siswa="+id_siswa+"&id_tahun="+id_tahun+"&id_kelas="+id_kelas,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_siswa();
              kirim_data_kenaikan();
            }else{
              alert(data[0]);
            }
        }
    })
  } 
}
</script>