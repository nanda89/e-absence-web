<body onload="metode_a();cek_sinkron();">
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Anda bisa lakukan perubahan komponen web dengan fitur ini</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <?php
$re = $setting->row();
?>
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
    ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
$_SESSION['pesan'] = '';

?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="panel/set_setting" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Logo Kecil (di pojok kiri atas panel)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->logo_mini; ?>" name="logo_mini">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Logo Gambar (ukuran 250 x 80 px *.png)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" name="gambar" placeholder="Kosongkan bila tidak dirubah">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Logo Favicon (*.ico)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" name="gambar1" placeholder="Kosongkan bila tidak dirubah">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Kartu GSM</label>
                  <div class="col-sm-4">
                    <select name="kartu_gsm" class="form-control">
                      <option value="<?php echo $re->kartu_gsm ?>"><?php echo $re->kartu_gsm ?></option>
                      <option>Telkomsel</option>
                      <option>3 (Tri)</option>
                      <option>Indosat</option>
                      <option>XL</option>
                      <option>Axis</option>
                      <option>Flexi</option>
                      <option>Esia</option>
                      <option>Smartfren</option>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Cara Cek Pulsa</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->cek_pulsa; ?>" placeholder="Cek Pulsa" name="cek_pulsa">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Metode Pengiriman SMS</label>
                  <div class="col-sm-4">
                    <select onchange="metode_a()" id="metode" name="metode" class="form-control">
                      <option value="Sewa Server">Sewa Server</option>
                    </select>
                  </div>

                  <label for="inputEmail3" id="kode1" class="col-sm-2 control-label">Kode Aktivasi</label>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="kode" value="<?php echo $re->kode_aktivasi; ?>" placeholder="Kode Aktivasi" name="kode_aktivasi">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" id="link1" class="col-sm-2 control-label">Link Tujuan</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="link" value="<?php echo $re->link_tujuan; ?>" placeholder="Link Tujuan, ex : http://namaweb.com/" name="link_tujuan">
                  </div>
                  <label for="inputEmail3" id="link1" class="col-sm-2 control-label">Toleransi Terlambat (menit)</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="link" value="<?php echo $re->telat; ?>" placeholder="Toleransi Terlambat" name="telat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Toleransi Jam Akhir (menit)</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="link" value="<?php echo $re->toleransi_jam_akhir; ?>" placeholder="Toleransi Jam Akhir" name="toleransi_jam_akhir">
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Sekolah</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->logo_besar; ?>" name="logo_besar">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Kepala Sekolah</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->pimpinan; ?>" placeholder="Nama Pimpinan" name="pimpinan">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">NIK</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->nik; ?>" placeholder="NIK" name="nik">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Akun Gmail</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" value="<?php echo $re->akun_gmail; ?>" placeholder="Akun Gmail" name="akun_gmail">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Website</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->website; ?>" placeholder="Website" name="website">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $re->alamat; ?>" placeholder="Alamat Lengkap" name="alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Logo Sekolah (ukuran 250 x 80 px *.png)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" name="logo_sekolah" placeholder="Kosongkan bila tidak dirubah">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kop Surat (ukuran 600 x 110 px *.jpg)</label>
                  <div class="col-sm-4">
                    <input type="file" class="form-control" name="kop_surat" placeholder="Kosongkan bila tidak dirubah">
                  </div>
                  <label for="inputPassword3" class="col-sm-2 control-label">Ttd Kepsek (*.png)</label>
                  <div class="col-sm-4">
                    <input type="file" class="form-control" name="ttd" placeholder="Kosongkan bila tidak dirubah">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Koordinat sekolah</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="lat" placeholder="latitude" value="<?php echo $re->lat; ?>">
                  </div>
                  <div class="col-sm-4">
                    <input class="form-control" name="lng" placeholder="longitude" value="<?php echo $re->lng; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Radius</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="radius" placeholder="dalam satuan meter" value="<?php echo $re->radius; ?>">
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jam masuk</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="jam_masuk" value="<?php echo $re->jam_masuk; ?>">
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jam pulang</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="jam_pulang" value="<?php echo $re->jam_pulang; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status Absen Pulang</label>
                  <div class="col-sm-4">
                    <select name="status_absen_pulang" id="status_absen_pulang" class="form-control">
                      <option value="<?php echo $re->status_absen_pulang; ?>"><?php if ($re->status_absen_pulang == "1") {
    echo "Ya";
} else {
    echo "Tidak";
}
?></option>
                      <option value="1">Ya</option>
                      <option value="0">Tidak</option>
                    </select>
                  </div>
                  <label for="inputPassword3" class="col-sm-2 control-label">Kapasitas Kelas</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="kapasitas" value="<?php echo $re->kapasitas_kelas; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kota</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="kota" value="<?php echo $re->kota; ?>">
                  </div>
                  <label for="inputPassword3" class="col-sm-2 control-label">Link Sinkron</label>
                  <div class="col-sm-4">
                    <input class="form-control" name="link_sinkron" value="<?php echo $re->link_sinkron; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Sinkron Web Monitoring</label>
                  <div class="col-sm-4">
                    <select name="sinkron" id="sinkron" class="form-control" onchange="cek_sinkron()">
                      <option value="" <?php if ($re->sinkron == "") {
    echo "selected";
}
?>>- choose -</option>
                      <option value="1" <?php if ($re->sinkron == 1) {
    echo "selected";
}
?>>Auto</option>
                      <option value="0" <?php if ($re->sinkron == 0) {
    echo "selected";
}
?>>Manual</option>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <select name="hari" id="hari" class="form-control">
                      <option value="mon" <?php if ($re->hari == "mon") {
    echo "selected";
}
?>>Senin</option>
                      <option value="tue" <?php if ($re->hari == "tue") {
    echo "selected";
}
?>>Selasa</option>
                      <option value="wed" <?php if ($re->hari == "wed") {
    echo "selected";
}
?>>Rabu</option>
                      <option value="thu" <?php if ($re->hari == "thu") {
    echo "selected";
}
?>>Kamis</option>
                      <option value="fri" <?php if ($re->hari == "fri") {
    echo "selected";
}
?>>Jumat</option>
                      <option value="sat" <?php if ($re->hari == "sat") {
    echo "selected";
}
?>>Sabtu</option>
                    </select>
                  </div>
                  <div class="col-sm-2">
                    <input class="form-control" id="jam" type="time" name="jam" value="<?php echo $re->jam; ?>">
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->
  </section>
</div>
<script type="text/javascript">
function metode_a(){
  var metode = $("#metode").val();
  if(metode == 'Sewa Server'){
    $("#kode").show();
    $("#link").show();
    $("#kode1").show();
    $("#link1").show();
    $("#kode").focus();
  }else{
    $("#kode").hide();
    $("#link").hide();
    $("#kode1").hide();
    $("#link1").hide();
  }
}
function cek_sinkron(){
  var sinkron = $("#sinkron").val();
  if(sinkron == 1){
    $("#hari").show();
    $("#jam").show();
  }else{
    $("#hari").hide();
    $("#jam").hide();
  }
}

</script>