<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="insert"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/penjadwalan">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/penjadwalan/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Proses</label>
                  <div class="col-sm-4">
                    <select name="id_daf_jenis" required class="form-control">                    
                      <option value="">Pilih</option>
                      <?php 
                      foreach($dt_konfigurasi->result() as $row) {                           
                      echo "
                      <option value='$row->id_daf_jenis'>$row->nama_proses</option>";
                      } ?>
                    </select>
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kelompok Pendaftaran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Macam-macam kelompok pendaftaran (Gelombang Pendaftaran)" name="kelompok">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kapasistas</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Batasan calon siswa yang diterima (opsional)" name="kapasitas">
                  </div>
                </div>                              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Pendaftaran</label>
                  <div class="col-sm-4">
                    <input type="text" id="tanggal" class="form-control" id="inputEmail3" placeholder="Dibuka Mulai" name="tgl_awal">
                  </div>

                  <div class="col-sm-4">
                    <input type="text" id="tanggal2" class="form-control" id="inputEmail3" placeholder="Hingga Tanggal" name="tgl_akhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Uraian Lengkap</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="uraian">
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Persyaratan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="syarat">
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Fasilitas</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="fasilitas">
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="kegiatan">
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Feedback Pendaftaran</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="keterangan">
                    </textarea>
                  </div>
                </div> 
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="edit"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/penjadwalan">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/penjadwalan/add" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $row->id_daf_jadwal ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Proses</label>
                  <div class="col-sm-4">
                    <select name="id_daf_jenis" required class="form-control">                    
                      <option value="<?php echo $row->id_daf_jenis ?>">
                        <?php 
                        $r = $this->m_konfigurasi->get_one($row->id_daf_jenis);
                        $r_je = $r->row();
                        echo $r_je->nama_proses;
                        ?>
                      </option>
                      <?php 
                      foreach($dt_konfigurasi->result() as $row2) {                           
                      echo "
                      <option value='$row2->id_daf_jenis'>$row2->nama_proses</option>";
                      } ?>
                    </select>
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kelompok Pendaftaran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->kelompok ?>" id="inputEmail3" placeholder="Macam-macam kelompok pendaftaran (Gelombang Pendaftaran)" name="kelompok">
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kapasistas</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->kapasitas ?>" class="form-control" id="inputEmail3" placeholder="Batasan calon siswa yang diterima (opsional)" name="kapasitas">
                  </div>
                </div>                              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Pendaftaran</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tgl_awal ?>" id="tanggal" class="form-control" id="inputEmail3" placeholder="Dibuka Mulai" name="tgl_awal">
                  </div>

                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tgl_akhir ?>" id="tanggal2" class="form-control" id="inputEmail3" placeholder="Hingga Tanggal" name="tgl_akhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Uraian Lengkap</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="uraian">
                      <?php echo $row->uraian ?>
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Persyaratan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="syarat">
                      <?php echo $row->syarat ?>
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Fasilitas</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="fasilitas">
                      <?php echo $row->fasilitas ?>
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="kegiatan">
                      <?php echo $row->kegiatan ?>
                    </textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Feedback Pendaftaran</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="keterangan">
                      <?php echo $row->keterangan ?>
                    </textarea>
                  </div>
                </div> 
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }elseif($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/penjadwalan/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Nama Proses</th>
              <th>Kelompok</th>
              <th>Kapasitas</th>
              <th>Pendaftar</th>
              <th>Tgl Pendaftaran</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_ta->result() as $row) { 
            $c      = "SELECT COUNT(tabel_calon_siswa.id_daftar) as jum FROM tabel_daf_jenis
                    INNER JOIN tabel_daf_jadwal 
                        ON (tabel_daf_jenis.id_daf_jenis = tabel_daf_jadwal.id_daf_jenis)
                    INNER JOIN tabel_calon_siswa 
                        ON (tabel_calon_siswa.id_jenjang = tabel_daf_jenis.id_jenjang)
                    WHERE tabel_daf_jenis.id_jenjang = '$row->id_jenjang'";
            $in = $this->db->query($c);
            $re  = $in->row();
            $j = $re->jum;

            $tgl_akhir = $row->tgl_akhir;
            $tgl_awal  = $row->tgl_awal;            
            $tgl = date("d/m/Y");

            $kap = $row->kapasitas;            
            if($tgl_awal<=$tgl and $tgl_akhir>=$tgl){
              $status = "Dibuka";           
            }else{
              $status = "Ditutup";            
            }                          
            
            

            if($status=='Ditutup'){
              $r = "<span class='label label-danger'>$status</span>";
            }else{
              $r = "<span class='label label-success'>$status</span>";
            } 
            $tgl1 = date("d F Y", strtotime($tgl_awal));
            $tgl2 = date("d F Y", strtotime($tgl_akhir));
            
           
            

          echo "          
            <tr>
              <td>$no</td>
              <td>$row->nama_proses $r</td>
              <td>$row->kelompok</td>
              <td>$row->kapasitas orang</td>
              <td>$j orang</td>
              <td>$tgl_awal - $tgl_akhir</td>
              <td>";
              ?>
              <form method="post" action="adm/penjadwalan_edit/process">
                <input type="hidden" name="id" value="<?php echo $row->id_daf_jadwal ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>

