
<table class="table table-bordered responsive-utilities jambo_table bulk_action" id="jasa">
    <thead>
        <tr class="headings">                                                
            <th width="5%">No </th>            
            <th>Kategori </th>
            <th>Mapel </th>                                                                                                                                
            <th>KKM </th>                                                                                
            <th width="7%">Aksi </th>                                                                                                                
        </tr>
    </thead>
    <tbody> 
    <?php 
      $no=1; 
      foreach($dt_mapel->result() as $row) {       
      echo "          
        <tr>
          <td>$no</td>
          <td>$row->kategori_mapel</td>
          <td>$row->mapel</td>
          <td>$row->kkm</td>                
          <td>"; ?>
            <button title="Hapus Data"
                class="btn btn-sm btn-danger fa fa-trash-o" type="button" 
                onClick="hapus_mapel('<?php echo $row->id_pengampu; ?>','<?php echo $row->nik; ?>')"></button>
          </td>
        </tr>
        <?php
        $no++;
        }
    ?>   
    </tbody>
</table>
