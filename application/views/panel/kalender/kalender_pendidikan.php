<base href="<?php echo base_url(); ?>" />
<!-- <body onload="window.print()"> -->
<body>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>

  <section class="content">
    
    <div class="box box-default">
      <div class="box-header with-border">
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-inline">
          <label>Tahun Ajaran</label>
            <select id="ta-select" class="form-control">
            <?php
              $ta_list = $this->db->query("SELECT * FROM tabel_tahunajaran");
              foreach( $ta_list->result() as $ta ) {
                echo '<option value="kalender/kalender_pendidikan?ta=' . $ta->tahun_ajaran . '"';
                echo ( $ta->tahun_ajaran == $selected_ta ) ? " selected" : "";
                echo '>';
                echo $ta->tahun_ajaran;
                echo '</option>';
              }
            ?>
            </select>
            <a href="kalender/kalender_pendidikan?print=1&ta=<?php echo $selected_ta; ?>" class="btn btn-danger" target="_blank"><i class="fa fa-print"></i> Cetak</a>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php 
            $tahun = substr( $selected_ta, 0,4 );
            $bulan = 7;
            $hk = [];
            $hl = [];
            $he = [];
            $bulan_per_semester = 6;
            $jml_hari = 31; 
            $grand_total_hk = 0; $grand_total_hl = 0; $grand_total_he = 0;
            ?><br><br>
            <div class="table-responsive">
            <table class="table kp-table">              
              <thead>
                <tr>
                  <th rowspan="2" class="text-center">THN</th>
                  <th rowspan="2" class="text-center">BLN</th>
                  <th colspan="31" class="text-center">TANGGAL</th>
                  <th colspan="3" class="text-center">JML HARI</th>
                </tr>
                <tr>
                  <?php 
                  for ($i=1; $i <= $jml_hari; $i++) { 
                    echo '<th width="50" class="text-center">' . $i . '</th>';
                  }
                  ?>
                  <th class="text-center">HK</th>
                  <th class="text-center">HL</th>
                  <th class="text-center">HE</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                // SMT 1
                $total_hk = 0; $total_hl = 0; $total_he = 0;
                for ($i=7; $i <= 12; $i++) : ?>
                <tr>
                  <?php if( $i == 7 ) : ?>
                  <td align="center" rowspan="6"><?php echo $tahun; ?></td>
                  <?php endif; ?>
                  <td align="center">
                    <?php
                    $dt_param = $tahun . '-' . mb_strimwidth($i, 0, 2, '0') . '-01';
                    $m = new \DateTime($dt_param);
                    echo strtoupper($m->format('M'));
                    ?>
                  </td>  
                  <?php 
                  $bulan_key = strtolower($m->format('M'));
                  $hk[$bulan_key] = cal_days_in_month(CAL_GREGORIAN, $i, $tahun);
                  $hl[$bulan_key] = 0;
                  $he[$bulan_key] = 0;
                  for ($j=1; $j <= $jml_hari; $j++) { 
                    $warna_bg = '';
                    $libur = false;
                    $teks = '';
                    $date = new \DateTime($tahun . '-' . $m->format('m') . '-' . $j);
                    if ( $date->format('N') == 7 ) {
                      $libur = true;
                      $teks = 'M';
                      $warna_bg = 'red';
                    }
                    // cek hari libur
                    $kalender = $this->db->query("SELECT * FROM tabel_kalender WHERE tanggal='".$date->format('d-m-Y')."' LIMIT 0,1");
                    // var_dump( ( isset($kalender->row()->libur) ?$kalender->row()->libur : '' ) );
                    if( !is_null($kalender->row()) ) {
                      if ( $kalender->row()->libur )
                        $libur = true;
                      if ( !empty( trim( $kalender->row()->warna ) ) )
                        $warna_bg = trim( $kalender->row()->warna );
                      $teks = $kalender->row()->agenda;
                    }
                    echo '<td align="center"';
                    echo ' style="background:' . $warna_bg . ' !important"';
                    echo '>';
                    echo $teks;
                    // echo ( isset($value['agenda'][$j]) ) ? $value['agenda'][$j] : '';
                    echo '</td>';


                    if ( $libur ) {
                      $hl[$bulan_key]++;
                    } else {
                      $he[$bulan_key]++;
                    }

                  }
                  ?>
                  <td align="center"><?php echo $hk[$bulan_key]; ?></td>
                  <td align="center"><?php echo $hl[$bulan_key]; ?></td>
                  <td align="center"><?php echo $he[$bulan_key]; ?></td>
                </tr>
                <?php
                $total_hk += $hk[$bulan_key]; $total_hl += $hl[$bulan_key]; $total_he += $he[$bulan_key];
                endfor;
                $grand_total_hk += $total_hk; $grand_total_hl += $total_hl; $grand_total_he += $total_he;
                ?>
                <tr>
                  <th style="background:<?php echo $color_footer_semester; ?>;color:#fff" colspan="<?php echo $jml_hari+2; ?>" class="text-center">JUMLAH PADA SEMESTER I</th>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hk; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hl; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_he; ?></td>
                </tr>


                <?php
                // SMT 2
                $total_hk = 0; $total_hl = 0; $total_he = 0;
                for ($i=1; $i <= 7; $i++) :
                  $warna_bg = '';
                  $libur = false;
                  // $value = $kalender_pendidikan[$i]; ?>
                <tr>
                  <?php if( $i == 1 ) : ?>
                  <td align="center" rowspan="7"><?php echo $tahun+1; ?></td>
                  <?php endif; ?>
                  <td align="center">
                    <?php
                    $dt_param = ($tahun+1) . '-' . mb_strimwidth($i, 0, 2, '0') . '-01';
                    $m = new \DateTime($dt_param);
                    echo strtoupper($m->format('M'));
                    ?>
                  </td>  
                  <?php 
                  $bulan_key = strtolower($m->format('M'));
                  $hk[$bulan_key] = cal_days_in_month(CAL_GREGORIAN, $i, $tahun+1);
                  $hl[$bulan_key] = 0;
                  $he[$bulan_key] = 0;
                  for ($j=1; $j <= $jml_hari; $j++) { 
                    $warna_bg = '';
                    $libur = false;
                    $teks = '';
                    $date = new \DateTime(($tahun+1) . '-' . $m->format('m') . '-' . $j);
                    if ( $date->format('N') == 7 ) {
                      $libur = true;
                      $teks = 'M';
                      $warna_bg = 'red';
                    }
                    // cek hari libur
                    $kalender = $this->db->query("SELECT * FROM tabel_kalender WHERE tanggal='".$date->format('d-m-Y')."' LIMIT 0,1");
                    if( !is_null($kalender->row()) ) {
                      if ( $kalender->row()->libur )
                        $libur = true;
                      if ( !empty( trim( $kalender->row()->warna ) ) )
                        $warna_bg = trim( $kalender->row()->warna );
                      $teks = $kalender->row()->agenda;
                    }
                    echo '<td align="center"';
                    echo ' style="background:' . $warna_bg . ' !important"';
                    echo '>';
                    echo $teks;
                    echo '</td>';


                    if ( $libur ) {
                      $hl[$bulan_key]++;
                    } else {
                      $he[$bulan_key]++;
                    }

                    $date->add( new \DateInterval('P1D') );

                  }
                  ?>
                  <td align="center"><?php echo $hk[$bulan_key]; ?></td>
                  <td align="center"><?php echo $hl[$bulan_key]; ?></td>
                  <td align="center"><?php echo $he[$bulan_key]; ?></td>
                </tr>
                <?php
                $total_hk += $hk[$bulan_key]; $total_hl += $hl[$bulan_key]; $total_he += $he[$bulan_key];
                endfor;
                $grand_total_hk += $total_hk; $grand_total_hl += $total_hl; $grand_total_he += $total_he;
                ?>
                <tr>
                  <th style="background:<?php echo $color_footer_semester; ?>;color:#fff" colspan="<?php echo $jml_hari+2; ?>" class="text-center">JUMLAH PADA SEMESTER II</th>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hk; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hl; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_he; ?></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th style="background: <?php echo $color_bg_foot; ?>" class="text-center" colspan="<?php echo $jml_hari+2; ?>">JUMLAH SELAMA TAHUN PELAJARAN INI</th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_hk; ?></th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_hl; ?></th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_he; ?></th>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">
          Keterangan
        </h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <style type="text/css">
          .kp-code {
            border: 1px solid #ddd;
            padding: .5em;
          }
        </style>
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <p>
              <span class="kp-code" style="color: #fff;background-color:red">M</span> Minggu / Libur
            </p>   
          </div>
          <?php
          $agenda = $this->db->query("SELECT DISTINCT agenda FROM tabel_kalender");
          $num_rows = $agenda->num_rows();
          foreach( $agenda->result() as $kalender ) : ?>
                <?php
                $ket_q = $this->db->query("SELECT keterangan,warna FROM tabel_kalender WHERE agenda='{$kalender->agenda}' LIMIT 1");
                $ket = $ket_q->result();
                if( isset( $ket[0] ) ) : ?>
                <div class="col-sm-6 col-md-4">
                  <p>
                    <span class="kp-code" style="background-color:<?php echo $ket[0]->warna; ?>"><?php echo $kalender->agenda; ?></span>
                    <?php echo $ket[0]->keterangan; ?>
                  </p>
                </div>
                <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div><!-- /.box -->
    
  </section>
</div>
</body>