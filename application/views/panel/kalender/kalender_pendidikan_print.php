<?php date_default_timezone_set("Asia/Jakarta");
$s = "select * from tabel_setting where id_setting = 1";
$r = $this->db->query($s)->row(); ?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="assets/web/images/<?php echo $r->logo_favicon ?>">
    <title><?php echo $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <link rel="stylesheet" href="assets/panel/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/panel/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/panel/dist/ionicons.min.css">
    <link rel="stylesheet" href="assets/panel/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <?php if( isset($stylesheets) ) :?>
    <style type="text/css">
      <?php echo $stylesheets; ?>
    </style>
    <?php endif; ?>



</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">

    <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    </section>

    
        <div class="row">
          <div class="col-md-12">
            <?php 
            $tahun = substr( $selected_ta, 0,4 );
            $bulan = 7;
            $hk = [];
            $hl = [];
            $he = [];
            $bulan_per_semester = 6;
            $color_tb_border = '#777';
            $color_bg_head = '#47F269';
            $color_footer_semester = '#1B76FE';
            $color_bg_foot = '#97FE65';
            $jml_hari = 31; 
            $grand_total_hk = 0; $grand_total_hl = 0; $grand_total_he = 0;
            ?><br><br>
            <style type="text/css">
              .kp-table, .kp-table tbody tr td {
                border: 1px solid <?php echo $color_tb_border; ?>;
              }
              .kp-table thead tr th {
                background-color: <?php echo $color_bg_head; ?>;
                border: 1px solid <?php echo $color_tb_border; ?>;
              }
              .kp-table tr th, .kp-table tr td {
                vertical-align: middle !important;
              }
            </style>
            <br><br>
            <div class="table-responsive">
            <table class="table kp-table">              
              <thead>
                <tr>
                  <th rowspan="2" class="text-center">THN</th>
                  <th rowspan="2" class="text-center">BLN</th>
                  <th colspan="31" class="text-center">TANGGAL</th>
                  <th colspan="3" class="text-center">JML HARI</th>
                </tr>
                <tr>
                  <?php 
                  for ($i=1; $i <= $jml_hari; $i++) { 
                    echo '<th width="50" class="text-center">' . $i . '</th>';
                  }
                  ?>
                  <th class="text-center">HK</th>
                  <th class="text-center">HL</th>
                  <th class="text-center">HE</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                // SMT 1
                $total_hk = 0; $total_hl = 0; $total_he = 0;
                for ($i=7; $i <= 12; $i++) : ?>
                <tr>
                  <?php if( $i == 7 ) : ?>
                  <td align="center" rowspan="6"><?php echo $tahun; ?></td>
                  <?php endif; ?>
                  <td align="center">
                    <?php
                    $dt_param = $tahun . '-' . mb_strimwidth($i, 0, 2, '0') . '-01';
                    $m = new \DateTime($dt_param);
                    echo strtoupper($m->format('M'));
                    ?>
                  </td>  
                  <?php 
                  $bulan_key = strtolower($m->format('M'));
                  $hk[$bulan_key] = cal_days_in_month(CAL_GREGORIAN, $i, $tahun);
                  $hl[$bulan_key] = 0;
                  $he[$bulan_key] = 0;
                  for ($j=1; $j <= $jml_hari; $j++) { 
                    $warna_bg = '';
                    $libur = false;
                    $teks = '';
                    $date = new \DateTime($tahun . '-' . $m->format('m') . '-' . $j);
                    if ( $date->format('N') == 7 ) {
                      $libur = true;
                      $teks = 'M';
                      $warna_bg = 'red';
                    }
                    // cek hari libur
                    $kalender = $this->db->query("SELECT * FROM tabel_kalender WHERE tanggal='".$date->format('d-m-Y')."' LIMIT 0,1");
                    // var_dump( ( isset($kalender->row()->libur) ?$kalender->row()->libur : '' ) );
                    if( !is_null($kalender->row()) ) {
                      if ( $kalender->row()->libur )
                        $libur = true;
                      if ( !empty( trim( $kalender->row()->warna ) ) )
                        $warna_bg = trim( $kalender->row()->warna );
                      $teks = $kalender->row()->agenda;
                    }
                    echo '<td align="center"';
                    echo ' style="background:' . $warna_bg . ' !important"';
                    echo '>';
                    echo $teks;
                    // echo ( isset($value['agenda'][$j]) ) ? $value['agenda'][$j] : '';
                    echo '</td>';


                    if ( $libur ) {
                      $hl[$bulan_key]++;
                    } else {
                      $he[$bulan_key]++;
                    }

                  }
                  ?>
                  <td align="center"><?php echo $hk[$bulan_key]; ?></td>
                  <td align="center"><?php echo $hl[$bulan_key]; ?></td>
                  <td align="center"><?php echo $he[$bulan_key]; ?></td>
                </tr>
                <?php
                $total_hk += $hk[$bulan_key]; $total_hl += $hl[$bulan_key]; $total_he += $he[$bulan_key];
                endfor;
                $grand_total_hk += $total_hk; $grand_total_hl += $total_hl; $grand_total_he += $total_he;
                ?>
                <tr>
                  <th style="background:<?php echo $color_footer_semester; ?>;color:#fff" colspan="<?php echo $jml_hari+2; ?>" class="text-center">JUMLAH PADA SEMESTER I</th>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hk; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hl; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_he; ?></td>
                </tr>


                <?php
                // SMT 2
                $total_hk = 0; $total_hl = 0; $total_he = 0;
                for ($i=1; $i <= 7; $i++) :
                  $warna_bg = '';
                  $libur = false;
                  // $value = $kalender_pendidikan[$i]; ?>
                <tr>
                  <?php if( $i == 1 ) : ?>
                  <td align="center" rowspan="7"><?php echo $tahun+1; ?></td>
                  <?php endif; ?>
                  <td align="center">
                    <?php
                    $dt_param = ($tahun+1) . '-' . mb_strimwidth($i, 0, 2, '0') . '-01';
                    $m = new \DateTime($dt_param);
                    echo strtoupper($m->format('M'));
                    ?>
                  </td>  
                  <?php 
                  $bulan_key = strtolower($m->format('M'));
                  $hk[$bulan_key] = cal_days_in_month(CAL_GREGORIAN, $i, $tahun+1);
                  $hl[$bulan_key] = 0;
                  $he[$bulan_key] = 0;
                  for ($j=1; $j <= $jml_hari; $j++) { 
                    $warna_bg = '';
                    $libur = false;
                    $teks = '';
                    $date = new \DateTime(($tahun+1) . '-' . $m->format('m') . '-' . $j);
                    if ( $date->format('N') == 7 ) {
                      $libur = true;
                      $teks = 'M';
                      $warna_bg = 'red';
                    }
                    // cek hari libur
                    $kalender = $this->db->query("SELECT * FROM tabel_kalender WHERE tanggal='".$date->format('d-m-Y')."' LIMIT 0,1");
                    if( !is_null($kalender->row()) ) {
                      if ( $kalender->row()->libur )
                        $libur = true;
                      if ( !empty( trim( $kalender->row()->warna ) ) )
                        $warna_bg = trim( $kalender->row()->warna );
                      $teks = $kalender->row()->agenda;
                    }
                    echo '<td align="center"';
                    echo ' style="background:' . $warna_bg . ' !important"';
                    echo '>';
                    echo $teks;
                    echo '</td>';


                    if ( $libur ) {
                      $hl[$bulan_key]++;
                    } else {
                      $he[$bulan_key]++;
                    }

                    $date->add( new \DateInterval('P1D') );

                  }
                  ?>
                  <td align="center"><?php echo $hk[$bulan_key]; ?></td>
                  <td align="center"><?php echo $hl[$bulan_key]; ?></td>
                  <td align="center"><?php echo $he[$bulan_key]; ?></td>
                </tr>
                <?php
                $total_hk += $hk[$bulan_key]; $total_hl += $hl[$bulan_key]; $total_he += $he[$bulan_key];
                endfor;
                $grand_total_hk += $total_hk; $grand_total_hl += $total_hl; $grand_total_he += $total_he;
                ?>
                <tr>
                  <th style="background:<?php echo $color_footer_semester; ?>;color:#fff" colspan="<?php echo $jml_hari+2; ?>" class="text-center">JUMLAH PADA SEMESTER II</th>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hk; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_hl; ?></td>
                  <td align="center" style="background: <?php echo $color_bg_foot; ?>"><?php echo $total_he; ?></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th style="background: <?php echo $color_bg_foot; ?>" class="text-center" colspan="<?php echo $jml_hari+2; ?>">JUMLAH SELAMA TAHUN PELAJARAN INI</th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_hk; ?></th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_hl; ?></th>
                  <th class="text-center" style="background:#64FEFE"><?php echo $grand_total_he; ?></th>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
        </div>

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">
          Keterangan
        </h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <style type="text/css">
          .kp-code {
            border: 1px solid #ddd;
            padding: .5em;
          }
        </style>
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <p>
              <span class="kp-code" style="color: #fff;background-color:red">M</span> Minggu / Libur
            </p>   
          </div>
          <?php
          $agenda = $this->db->query("SELECT DISTINCT agenda FROM tabel_kalender");
          $num_rows = $agenda->num_rows();
          foreach( $agenda->result() as $kalender ) : ?>
                <?php
                $ket_q = $this->db->query("SELECT keterangan,warna FROM tabel_kalender WHERE agenda='{$kalender->agenda}' LIMIT 1");
                $ket = $ket_q->result();
                if( isset( $ket[0] ) ) : ?>
                <div class="col-sm-6 col-md-4">
                  <p>
                    <span class="kp-code" style="background-color:<?php echo $ket[0]->warna; ?>"><?php echo $kalender->agenda; ?></span>
                    <?php echo $ket[0]->keterangan; ?>
                  </p>
                </div>
                <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div><!-- /.box -->
    

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="assets/panel/bootstrap/js/bootstrap.min.js"></script>
<?php if( isset($scripts) ) :?>
    <script>
    $(document).ready(function(){
      <?php echo $scripts; ?>
    });
    </script>
    <?php endif; ?>

</body>
</html>