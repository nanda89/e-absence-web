<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/tunggakan/mailku">
            <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Kirim Pesan Tunggakan</button>                       
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="box-body with-border">
          <form class="form-horizontal" action="adm/tunggakan/t_tunggakan" method="get" enctype="multipart/form-data">          
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
              <div class="col-sm-4">
                <select id="id_jenjang" required name="id_jenjang" class="form-control">                    
                  <option value=''>Pilih Jenjang</option>
                  <?php 
                  foreach($dt_jenjang->result() as $dt_jenjang) {                           
                  echo "
                  <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
                  } ?>
                </select>
              </div>    
              <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
              <div class="col-sm-4">
                <select id="id_tahun" name="id_tahun" required class="form-control">                    
                  <option value=''>Pilih Tahun Ajaran</option>
                  <?php 
                  foreach($dt_tahun->result() as $dt_tahun) {                           
                  echo "
                  <option value='$dt_tahun->id_ta'>$dt_tahun->tahun_ajaran</option>";
                  } ?>
                </select>
              </div>              
            </div> 

            <!--div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jenis Biaya</label>
              <div class="col-sm-4">
                <select id="id_biaya" required class="form-control">                    
                  <option value=''>Pilih Jenis Biaya</option>
                </select>
              </div>            
            </div-->

            
                  
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
              </div>               
            </div>  
          </form>  
        </div>         
        <b>Riwayat Pembayaran</b>        
        <table class="table table-bordered responsive-utilities jambo_table bulk_action" id="example2">
            <thead>
                <tr class="headings">                                                
                    <th width="5%">No </th>
                    <th>No.Induk</th>            
                    <th>Nama Siswa</th>
                    <th>Kelas</th>    
                    <th>Biaya</th>
                    <th>Nominal</th>
                    <th>Tgl.Bayar</th>
                </tr>
            </thead>        
            <tbody> 
            <?php 
            $no=1;
            foreach ($dt_tu->result() as $s) {                
              $nom = mata_uang($s->nominal);
              $tgl = date("d F Y", strtotime($s->tgl_bayar));              
            echo "  
                <tr>
                    <td>$no</td>
                    <td>$s->id_siswa</td>
                    <td>$s->nama_lengkap</td>
                    <td>$s->kelas</td>
                    <td>$s->biaya</td>
                    <td>$nom</td>
                    <td>$s->tgl_bayar</td>                                                 
                </tr>
            ";
            $no++;
            }    
            ?>
            </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->    
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  $.ajax({
    url : "<?php echo site_url('adm/jadwal/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);      
    }
  })  
});
</script>

<script type="text/javascript">
function cek_nisn(){
  var nisn_js=document.getElementById("nisn").value;
  if (nisn_js=="") {    
    alert("Masukkan NISN Terlebih Dahulu...!");
    return false;
  }else{
    $.ajax({
        url : "<?php echo site_url('adm/form_bayar/cari_nisn')?>",
        type:"POST",
        data:"nisn="+nisn_js,   
        cache:false,   
        success: function(msg){ 
          data=msg.split("|");
          $("#nama").val(data[0]);                        
          $("#jenjang").val(data[1]);          
          cek_biaya(1);
          kirim_data_riwayat(nisn_js);
        }        
    })
  }
}
function cek_biaya(a){
 var id_jenjang=a;  
  $.ajax({
      url : "<?php echo site_url('adm/form_bayar/cari_biaya')?>",
      type:"POST",
      data:"id_jenjang="+id_jenjang,   
      cache:false,   
      success: function(msg){ 
        $("#id_biaya").html(msg);
      }        
  })  
}
function cek(){
  var id_biaya=document.getElementById("id_biaya").value;
  $.ajax({
      url : "<?php echo site_url('adm/form_bayar/cek_bi')?>",
      type:"POST",
      data:"id_biaya="+id_biaya,   
      cache:false,   
      success: function(msg){ 
        data=msg.split("|");        
        $("#nominal").val(data[0]);                                        
        $("#uraian").focus();
      }        
  })
}
function simpan_bayar(){
    var nisn            = document.getElementById("nisn").value;   
    var uraian          = $("#uraian").val();
    var id_biaya        = $("#id_biaya").val();
    var tanggal         = $("#tanggal").val();    
    
    if (nisn=="" || tanggal=="" || id_biaya == "") {    
        alert("Isikan data dengan lengkap...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/form_bayar/save')?>",
            type:"POST",
            data:"nisn="+nisn+"&uraian="+uraian+"&id_biaya="+id_biaya+"&tanggal="+tanggal,
            cache:false,
            success:function(msg){            
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_riwayat(nisn);
                    kosong();                
                }else{
                    alert("Data gagal tersimpan, silahkan ulangi");
                }                
            }
        })    
    }
}
function re(){
  kosong();
  $("#tampil_riwayat").hide();
}
function kosong(args){
  $("#nisn").val("");
  $("#nama").val("");
  $("#jenjang").val("");    
  $("#nominal").val("");      
  $("#uraian").val("");      
  $("#tanggal").val("");      
  $("#id_biaya").val("");   
}
function hapus_riwayat(a,b){ 
    var id_bayar  = a;     
    $.ajax({
        url : "<?php echo site_url('adm/form_bayar/delete_riwayat')?>",
        type:"POST",
        data:"id_bayar="+id_bayar,
        cache:false,
        success:function(msg){            
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_riwayat(b);
            }
        }
    })
}
function kirim_data_tunggakan(){    
  $("#tampil_tunggakan").show();  
  var id_tahun = document.getElementById("id_tahun").value;   
  var id_jenjang = document.getElementById("id_jenjang").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_jenjang="+id_jenjang+"&id_tahun="+id_tahun;
     xhr.open("POST", "adm/tunggakan/t_tunggakan", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_tunggakan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
</script>
<script type="text/javascript">
$('document').ready(function(){
  $('#nisn').keypress(function(e){
    if(e.which == 13){//Enter key pressed
      cek_nisn();//Trigger search button click event
    }
  });
  
});
</script>