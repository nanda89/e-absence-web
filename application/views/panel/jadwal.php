<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/jadwal/reset">
            <button onclick="alert('Anda yakin ingin mereset jadwal?')" class="btn bg-red btn-flat margin"><i class="fa fa-refresh"></i> Reset Jadwal</button>
          </a>
          <!--button class="btn bg-blue btn-flat margin"><i class="fa fa-download"></i> Export to Excel</button-->
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
            <div class="col-sm-4">
              <select id="id_tahun" class="form-control">
                <option value=''>Pilih Tahun Ajaran</option>
                <?php
foreach ($dt_tahun->result() as $dt_tahun) {
    echo "
                <option value='$dt_tahun->id_ta'>$dt_tahun->tahun_ajaran</option>";
}?>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Mapel</label>
            <div class="col-sm-4">
              <select id="id_mapel" name="id_mapel" class="form-control">
                <option value=''>Pilih Mapel</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
            <div class="col-sm-4">
              <select id="id_jenjang" name="jenjang" class="form-control">
                <option value=''>Pilih Jenjang</option>
                <?php
foreach ($dt_jenjang->result() as $dt_jenjang) {
    echo "
                <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
}?>
              </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Guru</label>
            <div class="col-sm-4">
              <select id="id_guru" name="id_guru" class="form-control">
                <option value=''>Pilih Guru</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
            <div class="col-sm-4">
              <select id="id_kelas" class="form-control">
                <option value=''>Pilih Kelas</option>
              </select>
            </div>

            <label for="inputEmail3" class="col-sm-2 control-label">Hari / Jam</label>
            <div class="col-sm-2">
              <select id="hari" class="form-control">
                <option value=''>Pilih Hari</option>
                <option value="0 Minggu">Minggu</option>
                <option value="1 Senin">Senin</option>
                <option value="2 Selasa">Selasa</option>
                <option value="3 Rabu">Rabu</option>
                <option value="4 Kamis">Kamis</option>
                <option value="5 Jumat">Jumat</option>
                <option value="6 Sabtu">Sabtu</option>
              </select>
            </div>
            <div class="col-sm-1">
              <input class="form-control" id="jam_awal" placeholder="mulai jam">
            </div>
            <div class="col-sm-1">
              <input class="form-control" id="jam_akhir" placeholder="sampai jam">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-6">
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
            <div class="col-sm-2">
              <select id="isi" class="form-control">
                <option value=''>Pilih Status</option>
                <option value="masuk">Masuk</option>
                <option value="pulang">Pulang</option>
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button type="button" onclick="kirim_data_jadwal()" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
              <!-- <h3 class="box-title">                           -->
                <button type="button" onclick="excel()" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</button>
                <button type="button" onclick="print()" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</button>
              <!-- </h3> -->
            </div>
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <!-- <div class="col-sm-4">

            </div> -->
            <div class="col-sm-4">
              <button type="button" onclick="simpan_jadwal()" class="btn btn-info btn-flat">Save</button>
              <!--button type="button" onclick="tes()" class="btn btn-info btn-flat">Tes</button-->
              <button type="button" onclick="re()" class="btn btn-default pull-right btn-flat">Reset</button>
            </div>
          </div>
        </form>
      </div>
      <div class="box-body">
        <div id="tampil_jadwal">
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$("#id_jenjang").change(function(){

  var id_jenjang = $("#id_jenjang").val();
  var id_ta = $("#id_tahun").val();
  if(id_ta != ""){
    getKelas(id_jenjang , id_ta);
  } else{
    $("#id_jenjang").prop('selectedIndex',0);
    alert("harap pilih tahun ajaran");
  }
});

function getKelas(id_jenjang , id_ta){
$.ajax({
    url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
    type:"POST",
    data:{"id_jenjang" : id_jenjang ,"id_ta":id_ta},
    cache:false,
    success:function(msg){
      $("#id_kelas").html(msg);
      get_mapel(id_jenjang);
    }
  })
}
$("#id_tahun").change(function(){

  $("#id_jenjang").prop('selectedIndex',0);

});

$("#id_mapel").change(function(){
  var idMapel = this.value;
  var idJenjang = $('#id_jenjang option:selected').val();
  // alert(idJenjang);
  get_guru(idJenjang,idMapel);
});


function hapus_jadwal(a,b,c,d){
  var id_jadwal     = a;
  var id_ta         = b;
  var id_jenjang    = c;
  var id_kelas      = d;
  $.ajax({
      url : "<?php echo site_url('adm/jadwal/delete_jadwal') ?>",
      type:"POST",
      data:"id_jadwal="+id_jadwal,
      cache:false,
      success:function(msg){
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_jadwal();
          }
      }
  })
}
function simpan_jadwal(){
  var id_tahun            = document.getElementById("id_tahun").value;
  var id_jenjang   = $("#id_jenjang").val();
  var id_kelas          = $("#id_kelas").val();
  var id_mapel      = $("#id_mapel").val();
  var id_guru    = $("#id_guru").val();
  var hari    = $("#hari").val();
  var jam_awal    = $("#jam_awal").val();
  var jam_akhir    = $("#jam_akhir").val();
  var isi    = $("#isi").val();
  if (id_tahun=="" || id_jenjang=="" || id_kelas == "" || id_mapel == ""|| id_guru == ""|| hari == "" || jam_awal == "" || jam_akhir == "") {
      alert("Isikan data dengan lengkap...!");
      return false;
  }else{
      $.ajax({
          url : "<?php echo site_url('adm/jadwal/save') ?>",
          type:"POST",
          data:"isi="+isi+"&id_tahun="+id_tahun+"&id_jenjang="+id_jenjang+"&id_kelas="+id_kelas+"&id_mapel="+id_mapel+"&id_guru="+id_guru+"&hari="+hari+"&jam_awal="+jam_awal+"&jam_akhir="+jam_akhir,
          cache:false,
          success:function(msg){
              data=msg.split("|");
              if(data[0]=="nihil"){
                  kirim_data_jadwal();
                  kosong_jadwal();
              }else{
                  alert(data[0]);
                  kosong_jadwal();
              }
          }
      })
  }
}


function tes(){
  var isi    = $("#isi").val();
  alert(isi);
}
function re(args){
  $("#hari").val("");
  $("#jam_awal").val("");
  $("#jam_akhir").val("");
  $("#id_mapel").val("");
  $("#id_guru").val("");
  $("#id_tahun").val("");
  $("#id_kelas").val("");
  $("#id_jenjang").val("");
  $("#tingkat").val("");
  $("#isi").val("");
  $("#tampil_jadwal").hide();
}
function kosong_jadwal(args){
  $("#hari").val("");
  $("#jam_awal").val("");
  $("#jam_akhir").val("");
  $("#id_mapel").val("");
  $("#id_guru").val("");
  $("#isi").val("");
  $("#tingkat").val("");
}
function kirim_data_jadwal(){
  $("#tampil_jadwal").show();
  var id_tahun = document.getElementById("id_tahun").value;
  var id_jenjang = document.getElementById("id_jenjang").value;
  var id_kelas = document.getElementById("id_kelas").value;
  var id_guru = document.getElementById("id_guru").value;
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "id_tahun="+id_tahun+"&id_jenjang="+id_jenjang+"&id_kelas="+id_kelas+"&id_guru="+id_guru;
     xhr.open("POST", "adm/jadwal/t_jadwal", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_jadwal").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
function excel() {
  var id_tahun = document.getElementById("id_tahun").value;
  var id_jenjang = document.getElementById("id_jenjang").value;
  var id_kelas = document.getElementById("id_kelas").value;
  var id_guru = document.getElementById("id_guru").value;
  window.open("<?php echo site_url('adm/jadwal/export_now') . "?a=1&id_tahun=" ?>" + id_tahun + "&id_jenjang="+ id_jenjang + "&id_kelas=" + id_kelas + "&id_guru=" + id_guru)
}
function print() {
  var id_tahun = document.getElementById("id_tahun").value;
  var id_jenjang = document.getElementById("id_jenjang").value;
  var id_kelas = document.getElementById("id_kelas").value;
  var id_guru = document.getElementById("id_guru").value;
  window.open("<?php echo site_url('adm/jadwal/export_now') . "?a=2&id_tahun=" ?>" + id_tahun + "&id_jenjang="+ id_jenjang + "&id_kelas=" + id_kelas + "&id_guru=" + id_guru)
}
function get_guru(id_jenjang,idMapel){
  $.ajax({
    url : "<?php echo site_url('adm/jadwal/get_guru') ?>",
    type:"POST",
    data:{"id_jenjang": id_jenjang,"id_mapel":idMapel},
    cache:false,
    success:function(msg){
      $("#id_guru").html(msg);
    }
  })
}
function get_mapel(id_jenjang){
  $.ajax({
    url : "<?php echo site_url('adm/jadwal/get_mapel') ?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,
    cache:false,
    success:function(msg){
      $("#id_mapel").html(msg);
    }
  })
}
</script>