<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?php if (!$readonly) : ?>
                    <a href="elearning/asc_pertanyaan/form/<?php echo $ujian['id_ujian']; ?>">
                    <button class="btn bg-success btn-flat margin"><i class="fa fa-plus"></i> Pertanyaan Baru</button>
                    </a>
                    <?php endif;?>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <td>UjianID</td>
                            <td>ID</td>
                            <td>No. Urut</td>
                            <td>Pertanyaan</td>
                            <td>Jawaban</td>
                            <td  style="column-width: 20%;">Tindakan</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>