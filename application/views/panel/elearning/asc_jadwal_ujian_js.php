<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    { "targets" : 1, "searchable" : false },
                    { "targets" : 7, "searchable" : false },
                    {
                        "targets" : 6,
                        "render" : function(data, type, row)
                        {
                            let col = row[6]; 
                            if ( row[6] == "publish" )
                            {
                                col = "<h2 class='label bg-green'>publish</h2>";
                            }
                            else if ( row[6] == "done" )
                            {
                                col = "<h2 class='label bg-blue'>done</h2>";
                            }
                            return col;
                        }
                    }
                ];
                init_datatable("#table", 'jadwal_ujian', columnDefs);
            <?php else: ?>
                // JS Form
                init_select2("#id_kelas", "kelas_select");
                init_select2("#id_ujian", "ujian_select");
                $("#tanggal").datepicker({
                      autoclose: true
                });
                var timepicker = new TimePicker('asc_jam', {
                  lang: 'en',
                  theme: 'dark'
                });

                timepicker.on('change', function(evt) {
                  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
                  evt.element.value = value;
                  var h = document.getElementById("jam");
                  h.value = value;
                });
            <?php endif; ?>
        }
    );
</script>