<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    { "targets" : 1, "visible" : false },
                    { "targets" : 2, "visible" : false },
                    { "targets" : 5, "searchable" : false }
                ];
                init_datatable("#table", 'pertanyaan/' + <?php echo $this->uri->segment(4,0); ?>, columnDefs);
            <?php else: ?>
                // JS Form
                init_select2("#mapel", "mapel_select");
            <?php endif; ?>
        }
    );
</script>