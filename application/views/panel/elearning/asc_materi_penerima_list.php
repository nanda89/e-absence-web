<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <a href="elearning/asc_materi_penerima/form?type=kelas&<?php echo $_SERVER['QUERY_STRING']; ?>">
                    <button class="btn bg-success btn-flat margin"><i class="fa fa-plus"></i> Tambah Penerima</button>
                    </a>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Tipe</td>
                            <td>Penerima</td>
                            <td>Tindakan</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>