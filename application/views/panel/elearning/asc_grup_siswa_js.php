<script type="text/javascript">
    $(document).ready(
        function()
        {
        let table = $('#table').DataTable
        ({
          'select'      : true,
          'paging'      : true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          "scrollX"     : true,
          "scrollY"     : "400px",
          'autoWidth'   : true,
          "scrollCollapse" : false,
          "processing"  : true,
          "serverSide"  : true,
          "order"       : [],
          "colReorder"  : false,
          "ajax"        :
          {
            "url"   : "<?php echo base_url().'elearning/asc_ajax_dt/grup_siswa?'.$_SERVER['QUERY_STRING']; ?>",
            "type"  : "POST"
          }
        }); // datatable init
        }
    );
</script>