<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <a href="elearning/asc_jadwal_ujian/form">
                    <button class="btn bg-success btn-flat margin"><i class="fa fa-plus"></i> Jadwal Baru</button>
                    </a>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Mapel</td>
                            <td>Judul Ujian</td>
                            <td>Deskripsi</td>
                            <td>Waktu Mulai</td>
                            <td>Waktu Selesai</td>
                            <td>Status</td>
                            <td style="column-width: 20%;">Tindakan</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>