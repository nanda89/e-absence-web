<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php echo $judul_header; ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <a href="elearning/asc_submission/index/<?php echo $id_jadwal_ujian;?>">
                            <button class="btn btn-box-tool"><i class="fa fa-arrow-left"></i> Kembali</button>
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">NISN</label>
                      <div class="col-sm-4"><?php echo $peserta['nisn']; ?></div>
                    </div>
                    <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                      <div class="col-sm-4"><?php echo $ujian['judul']; ?></div>
                    </div>
                    <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi</label>
                      <div class="col-sm-4"><?php echo $ujian['deskripsi']; ?></div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <?php if ( $jawaban != null ) : ?>
      <?php $index = 0; ?>
      <form id="form_sub" name="form_sub"  class="form-horizontal" action="elearning/asc_submission/form/<?php echo $id_jadwal_ujian .'/'.$id_peserta_ujian; ?>" method="post" enctype="multipart/form-data">
      <?php foreach ($jawaban as $row) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php echo $row['no_urut'] . ') ' . $row['pertanyaan']; ?>
                        </h3>
                        <br><br>
                        <?php 
                        /*
 Array ( [id_submission] => 30 [id_pertanyaan] => 54 [tipe] => Essay [jawaban] => kepala [gambar] => [suara] => [koreksi] => k11 [nilai] => 30 [pertanyaan] => Rangka paling atas adalah [pertanyaan_gambar] => 118-20180616030732_2.bmp [pertanyaan_suara] => [jawaban_guru] => [no_urut] => 1 [pilihan] => )
                        */

                        if ($row['pertanyaan_gambar'] != null ): ?>
                          <img style="max-width: 60%;" src="<?php echo base_url() . 'assets/upload/' . $row['pertanyaan_gambar'];?>">
                        <?php endif; ?>
                    </div><!-- /.box-header -->
                    <div class="box-body with-border">
                        <?php if ( $row['pilihan'] != null ) : ?>
                          <?php foreach ($row['pilihan'] as $choice) : ?>
                            <?php
                              $class = '';
                              $icon = '';
                              if ( $choice['huruf'] == $row['jawaban'] && $choice['huruf'] == $row['jawaban_guru'] )
                              {
                                $class = "bg bg-olive";
                                $icon = 'fa fa-check';
                              }
                              elseif ( $choice['huruf'] == $row['jawaban'] )
                              {
                                $class = "bg bg-orange";
                                $icon = 'fa fa-close';
                              }
                              elseif ( $choice['huruf'] == $row['jawaban_guru'] )
                              {
                                $icon = 'fa fa-check';
                              }
                            ?>
                            <div class="row form-group">
                              <label for="inputEmail3" class="col-sm-1 control-label"><?php echo $choice['huruf'] . ')'; ?></label>
                              <div class="<?php echo $class; ?> col-sm-9">
                                <?php echo $choice['text']; ?>
                                <?php 
                                if ($choice['gambar'] != null && $choice['gambar'] != "null"): ?>
                                  <br><br>
                                  <img style="max-width: 60%;" src="<?php echo base_url() . 'assets/upload/' . $choice['gambar'];?>">
                                <?php endif; ?>
                              </div>
                              <div class="col-sm-1"><span class="<?php echo $icon; ?>"></span></div>
                            </div>
                          <?php endforeach; ?>
                        <?php else: //pilihan is null?>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Jawaban</label>
                            <div class="col-sm-10">
                              <?php
                                  $id = 'jawaban' . $index;
                                  $value = $row['jawaban'];
                                  $array_data = array(
                                    'id' => $id,
                                    'name' => $id,
                                    'class' => 'form-control',
                                    'value' => set_value($id, $value),
                                    'type' => 'text',
                                    'readonly' => true
                                  );
                                  echo form_textarea($array_data);
                                  if ($row['gambar'] != null && $row['gambar'] != "null" ): ?>
                                    <br><br>
                                    <img style="max-width: 60%;" src="<?php echo base_url() . 'assets/upload/' . $row['gambar'];?>">
                                <?php endif;
                              ?>
                            </div>
                          </div>
                          <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">Nilai</label>
                              <div class="col-sm-10">
                                <?php
                                    $id = 'nilai' . $index;
                                    $value = $row['nilai'];
                                    $array_data = array(
                                      'id' => $id,
                                      'name' => $id,
                                      'class' => 'form-control',
                                      'value' => set_value($id, $value),
                                      'type' => 'number',
                                      'min' => 0
                                    );
                                    echo form_input($array_data);
                                    echo form_error($id);
                                    echo form_hidden('id_submission' . $index, $row['id_submission']);
                                ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">Koreksi</label>
                              <div class="col-sm-10">
                                <?php
                                    $id = 'koreksi' . $index;
                                    $value = $row['koreksi'];
                                    $array_data = array(
                                      'id' => $id,
                                      'name' => $id,
                                      'class' => 'form-control',
                                      'value' => set_value($id, $value),
                                      'type' => 'text'
                                    );
                                    echo form_input($array_data);
                                    echo form_error($id);
                                ?>
                              </div>
                            </div>
                        <?php endif; //pilihan is null?>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
      <?php $index++; ?>
      <?php endforeach; ?>
      </form>
      <?php if (!$readonly) : ?>
        <button type="submit" name="save" value="save" class="btn btn-info" form="form_sub">Save</button>
      <?php endif;?>
    <?php endif; ?>
</section>