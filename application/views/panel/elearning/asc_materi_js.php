<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    { 
                        "targets" : 5,
                        "searchable" : false
                    },
                    {
                        "targets" : 4,
                        "render" : function(data, type, row)
                        {
                            let col = row[4]; 
                            if ( row[4] == "publish" )
                            {
                                col = "<h2 class='label bg-green'>publish</h2>";
                            }
                            return col;
                        }
                    }
                ];
                init_datatable("#table", 'materi', columnDefs);
            <?php else: ?>
                // JS Form
                init_select2("#mapel", "mapel_select");
            <?php endif; ?>
        }
    );
</script>