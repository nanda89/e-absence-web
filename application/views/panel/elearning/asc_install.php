<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body with-border modal-body">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>Version</td>
                            <th>Batch</td>
                            <th>SQL File</td>
                            <th>Description</td>
                            <th>Action</td>
                        </tr>
                    </thead>
                    <tbody>
<?php
foreach($data as $row) {
$cmd = "
<form method='post' action='elearning/asc_install/doUpdate'>
<input type='hidden' name='v' value='".$row['version']."' />
<input type='hidden' name='b' value='".$row['batch']."' />
<input type='hidden' name='f' value='".$row['sql']."' />
<button data-toggle='tooltip' title='Update' name='s_process' type='submit' value='ubah' class='btn btn-primary btn-flat btn-sm'><i class='fa fa-check'></i></button>
</form>
";
echo "<tr><td>".$row['version']."</td><td>".$row['batch']."</td><td>".$row['sql'].
     "</td><td>".$row['description']."</td><td>".$cmd."</td></tr>";
}
?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
