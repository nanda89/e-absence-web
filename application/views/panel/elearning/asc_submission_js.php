<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    { "targets" : 0, "visible" : false },
                    { "targets" : 1, "visible" : false },
                    { "targets" : 8, "searchable" : false },
                    {
                        "targets" : 7,
                        "render" : function(data, type, row)
                        {
                            let col = row[7]; 
                            if ( row[7] == "publish" )
                            {
                                col = "<h2 class='label bg-green'>publish</h2>";
                            }
                            else if ( row[7] == "join" )
                            {
                                col = "<h2 class='label bg-navy'>join</h2>";
                            }
                            else if ( row[7] == "submit" )
                            {
                                col = "<h2 class='label bg-blue'>submit</h2>";
                            }
                            else if ( row[7] == "koreksi" )
                            {
                                col = "<h2 class='label bg-yellow'>koreksi</h2>";
                            }
                            return col;
                        }
                    }
                ];
                init_datatable("#table", "submission/" + <?php echo $id_jadwal_ujian; ?>, columnDefs);
            <?php else: ?>
                // JS Form
                init_select2("#mapel", "mapel_select");
            <?php endif; ?>
        }
    );
</script>