<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    { "targets" : 3, "searchable" : false }
                ];
                init_datatable("#table", "materi_penerima?id_materi=" + <?php echo $this->input->get('id_materi'); ?>, columnDefs );
            <?php else: ?>
                // JS Form
                init_select2("#penerima", "kelas_select");
            <?php endif; ?>
        }
    );
</script>