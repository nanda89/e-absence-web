<script type="text/javascript">
    $(document).ready(
        function()
        {
            <?php if ( $this->uri->segment(3, '') != 'form' ) : ?>
                // JS index
                const columnDefs = [
                    {
                        "targets" : 8,
                        "searchable" : false
                    },
                    {
                        "targets" : 7,
                        "render" : function(data, type, row)
                        {
                            let col = row[7]; 
                            if ( row[7] == "publish" )
                            {
                                col = "<h2 class='label bg-green'>publish</h2>";
                            }
                            return col;
                        }
                    }
                ];
                init_datatable("#table", 'ujian/', columnDefs);
            <?php else: ?>
                // JS Form
                init_select2("#mapel", "mapel_select");
            <?php endif; ?>
        }
    );
</script>