<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
    <?php
        $alert = $this->session->flashdata('alert');
        if ( $alert != null ) :
            $this->session->set_flashdata('alert', null);
    ?>
        <div class="alert <?php echo $alert['type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $alert['message']; ?>
        </div>
    <?php endif; ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
</section>
    <?php echo $page; ?>
</div>