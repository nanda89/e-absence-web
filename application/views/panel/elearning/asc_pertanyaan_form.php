<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        No. <?php echo $no_urut . ' ' , $judul; ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <a href="elearning/asc_pertanyaan/index/<?php echo $ujian['id_ujian']; ?>">
                            <button class="btn btn-box-tool"><i class="fa fa-arrow-left"></i> Kembali</button>
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <form class="form-horizontal" action="elearning/asc_pertanyaan/form/<?php echo $this->uri->segment(4,'') .'/'. $this->uri->segment(5,''); ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body with-border">
                        <?php echo form_hidden('tipe', $ujian['tipe']); ?>
                        <?php echo form_hidden('jumlah_pilihan', $ujian['jumlah_pilihan']); ?>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">No. Urut</label>
                          <div class="col-sm-10">
                            <?php
                                $array_data = array(
                                  'id' => 'no_urut',
                                  'name' => 'no_urut',
                                  'class' => 'form-control',
                                  'value' => set_value('no_urut', $no_urut),
                                  'readonly' => true
                                );
                                echo form_input($array_data);
                                echo form_error('no_urut');
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Pertanyaan</label>
                          <div class="col-sm-10">
                            <?php
                                $value = '';
                                if ( $pertanyaan != null )
                                {
                                    $value = $pertanyaan['pertanyaan'];
                                }
                                $array_data = array(
                                  'id' => 'pertanyaan',
                                  'name' => 'pertanyaan',
                                  'class' => 'form-control',
                                  'value' => set_value('pertanyaan', $value)
                                );
                                echo form_textarea($array_data);
                                echo form_error('pertanyaan');
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
                          <div class="col-sm-10">
                            <input type="file" class="form-control" id="gambar" name="gambar">
                          </div>
                        </div>
                        <?php if ( $ujian['tipe'] == "Pilihan Ganda" ) : ?>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Jawaban</label>
                            <div class="col-sm-10">
                              <?php
                                $array_data = array(
                                  'id' => 'jawaban',
                                  'name' => 'jawaban',
                                  'class' => 'form-control'
                                );
                                $option_list = explode(" ", $this->config->item('pilihan_abjad'));
                                $options = array();
                                for($i=0; $i<$ujian['jumlah_pilihan']; $i++)
                                {
                                  $huruf = $option_list[$i] ;
                                  $options[$huruf] = $huruf;
                                }
                                $selected = null;
                                if ( $pertanyaan != null )
                                {
                                    $selected = $pertanyaan['jawaban'];
                                    $options[$selected] = $pertanyaan['jawaban'];
                                }
                                echo form_dropdown('jawaban', $options, $selected, $array_data);
                                echo form_error('jawaban');
                            ?>
                            </div>
                          </div>
                          <?php for($i=0; $i<$ujian['jumlah_pilihan']; $i++): ?>
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                  <h3 class="box-title">
                                      Pilihan <?php echo $option_list[$i]; ?>
                                      <?php echo form_hidden('huruf_' . $i, $option_list[$i] ); ?>
                                  </h3>
                              </div><!-- /.box-header -->
                              <div class="box-body with-border">
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Text</label>
                                  <div class="col-sm-10">
                                    <?php
                                        $pilihan = null;
                                        if ( $list_pilihan != null )
                                        {
                                          $pilihan = $list_pilihan[$i];
                                        }
                                        if ( $pilihan != null )
                                        {
                                            $value = $pilihan['text'];
                                        }
                                        $name_control = 'pilihan_' . $i;
                                        $array_data = array(
                                          'id' => $name_control,
                                          'name' => $name_control,
                                          'class' => 'form-control',
                                          'type' => 'text',
                                          'value' => set_value($name_control, $value)
                                        );
                                        echo form_input($array_data);
                                        echo form_error($name_control);
                                    ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                                  <div class="col-sm-10">
                                    <input type="file" class="form-control" id="<?php echo 'gambar_' . $i; ?>" name="<?php echo 'gambar_' . $i; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          <?php endfor; ?>
                        <?php endif; ?>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php if (!$readonly) : ?>
                        <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                        <?php endif;?>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>