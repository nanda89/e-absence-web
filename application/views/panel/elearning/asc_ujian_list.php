<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <a href="elearning/asc_ujian/form/0/p">
                    <button class="btn bg-success btn-flat margin"><i class="fa fa-plus"></i> Ujian Baru Pilihan Ganda</button>
                    </a>
                    <a href="elearning/asc_ujian/form/0/e">
                    <button class="btn bg-success btn-flat margin"><i class="fa fa-plus"></i> Ujian Baru Essay</button>
                    </a>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Kode Ujian</td>
                            <td>Mata Pelajaran</td>
                            <td>Judul</td>
                            <td>Deskripsi</td>
                            <td>Tipe</td>
                            <td>Pertanyaan</td>
                            <td>Status</td>
                            <td style="column-width: 20%;">Tindakan</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>