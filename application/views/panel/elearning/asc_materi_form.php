<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php echo $judul_header; ?>
                    </h3>
                </div><!-- /.box-header -->
                <form class="form-horizontal" action="elearning/asc_materi/form/<?php echo $this->uri->segment(4,''); ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body with-border">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
                          <div class="col-sm-10">
                            <?php
                                $value = '';
                                if ( $materi != null )
                                {
                                    $value = $materi['judul'];
                                }
                                $array_data = array(
                                  'id' => 'judul',
                                  'name' => 'judul',
                                  'class' => 'form-control',
                                  'value' => set_value('judul', $value),
                                  'type' => 'text'
                                );
                                echo form_input($array_data);
                                echo form_error('judul');
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Mata Pelajaran</label>
                          <div class="col-sm-4">
                            <?php
                                $array_data = array(
                                  'id' => 'mapel',
                                  'name' => 'mapel',
                                  'class' => 'form-control'
                                );
                                $options = array();
                                $selected = null;
                                if ( $materi != null )
                                {
                                    $selected = $materi['id_mapel'];
                                    $options[$selected] = $materi['mapel'];
                                }
                                echo form_dropdown('mapel', $options, $selected, $array_data);
                                echo form_error('mapel');
                            ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Isi Materi</label>
                          <div class="col-sm-10">
                            <?php
                                $value = '';
                                if ( $materi != null )
                                {
                                    $value = $materi['materi'];
                                }
                                $array_data = array(
                                  'id' => 'materi',
                                  'name' => 'materi',
                                  'class' => 'form-control',
                                  'value' => set_value('materi', $value)
                                );
                                echo form_textarea($array_data);
                                echo form_error('materi');
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
                          <div class="col-sm-10">
                            <input type="file" class="form-control" id="gambar" name="gambar">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                          <div class="col-sm-4">
                            <?php
                                $draft_check = true;
                                if ( $materi != null )
                                {
                                    $draft_check = ($materi['doc_status'] == 'draft');
                                }
                            ?>
                            <label>
                              <input type="radio" name="status" value="publish" class="flat-red" <?php echo ($draft_check ? '' : 'checked'); ?> > Publish
                            </label> &nbsp;&nbsp;&nbsp;
                            <label>
                              <input type="radio" name="status" value="draft" class="flat-red" <?php echo ($draft_check ? 'checked' : ''); ?> > Draft
                            </label>
                          </div>
                          </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>