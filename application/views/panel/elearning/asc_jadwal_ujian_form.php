<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php echo $judul_header . validation_errors(); ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <a href="elearning/asc_jadwal_ujian/index">
                            <button class="btn btn-box-tool"><i class="fa fa-arrow-left"></i> Kembali</button>
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <form class="form-horizontal" action="elearning/asc_jadwal_ujian/form" method="post" enctype="multipart/form-data">
                    <div class="box-body with-border">
                        <?php echo form_hidden('id_jadwal_ujian', $id_jadwal_ujian); ?>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Ujian</label>
                          <div class="col-sm-4">
                            <?php
                                $array_data = array(
                                  'id' => 'id_ujian',
                                  'name' => 'id_ujian',
                                  'class' => 'form-control'
                                );
                                $options = array();
                                $selected = null;
                                if ( $jadwal_ujian != null )
                                {
                                    $selected = $jadwal_ujian['id_ujian'];
                                    $options[$selected] = $jadwal_ujian['judul'];
                                }
                                echo form_dropdown('id_ujian', $options, $selected, $array_data);
                                echo form_error('id_ujian');
                            ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Kelas Penerima</label>
                          <div class="col-sm-4">
                            <?php
                                $array_data = array(
                                  'id' => 'id_kelas',
                                  'name' => 'id_kelas',
                                  'class' => 'form-control'
                                );
                                $options = array();
                                $selected = null;
                                if ( $jadwal_ujian != null )
                                {
                                    $selected = $jadwal_ujian['id_kelas'];
                                    $options[$selected] = $jadwal_ujian['kelas'];
                                }
                                echo form_dropdown('id_kelas', $options, $selected, $array_data);
                                echo form_error('id_kelas');
                            ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Tanggal</label>
                          <div class="col-sm-4">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <?php
                                $value = '';
                                if ( $jadwal_ujian != null )
                                {
                                    $arr = explode(" ",$jadwal_ujian['waktu_mulai']);
                                    $value = $arr[0];
                                    $date = new DateTime($value);
                                    $value = $date->format('d-m-Y');
                                }
                                $array_data = array(
                                  'id' => 'tanggal',
                                  'name' => 'tanggal',
                                  'class' => 'form-control pull-right',
                                  'value' => set_value('tanggal', $value),
                                  'autocomplete' => 'off'
                                );
                                echo form_input($array_data);
                                echo form_error('tanggal');
                                ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Jam</label>
                          <div class="col-sm-4">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              <?php
                                $value = date("H:i");
                                if ( $jadwal_ujian != null )
                                {
                                    $arr = explode(" ",$jadwal_ujian['waktu_mulai']);
                                    if ( count($arr) > 1)
                                    {
                                      $value = $arr[1];
                                    }
                                }
                                $array_data = array(
                                  'id' => 'asc_jam',
                                  'name' => 'asc_jam',
                                  'class' => 'form-control pull-right',
                                  'type' => 'text',
                                  'value' => set_value('asc_jam', $value),
                                  'autocomplete' => 'off'
                                );
                                echo form_input($array_data);
                                echo form_error('asc_jam');
                                ?>
                            </div>
                          </div>
                        </div>
<?php
$array_data = array(
  'id' => 'jam',
  'name' => 'jam',
  'type' => 'hidden',
  'value' => set_value('jam', $value)
);
echo form_input($array_data);
?>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Durasi</label>
                          <div class="col-sm-3">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              <?php
                                $value = '1';
                                if ( $jadwal_ujian != null )
                                {
                                    $value = $jadwal_ujian['durasi'];
                                }
                                $array_data = array(
                                  'id' => 'durasi',
                                  'name' => 'durasi',
                                  'class' => 'form-control',
                                  'type' => 'number',
                                  'min' => "1",
                                  'value' => set_value('durasi', $value)
                                );
                                echo form_input($array_data);
                                echo form_error('jam');
                                ?>
                            </div>
                          </div> Menit
                        </div>
                        
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Tampilkan Jawaban</label>
                          <div class="col-sm-4">
                            <?php
                                $tampilkan_jawaban = true;
                                if ( $jadwal_ujian != null )
                                {
                                    $tampilkan_jawaban = ($jadwal_ujian['tampilkan_jawaban'] == 'Y');
                                }
                                echo form_error('tampilkan_jawaban');
                            ?>
                            <label>
                              <input type="radio" name="tampilkan_jawaban" value="Y" class="flat-red" <?php echo ($tampilkan_jawaban ? '' : 'checked'); ?> > Iya
                            </label> &nbsp;&nbsp;&nbsp;
                            <label>
                              <input type="radio" name="tampilkan_jawaban" value="N" class="flat-red" <?php echo (!$tampilkan_jawaban ? 'checked' : ''); ?> > Tidak
                            </label>
                          </div>
                          </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                          <div class="col-sm-4">
                            <?php echo form_hidden('status', $doc_status); ?>
                            <label class="control-label"><span></span><h5><?php echo $doc_status;?></h5></label>
                          </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <?php if ( !$readonly ) : ?>
            <button type="submit" name="s_process" value="edit" class="btn btn-info">Save</button>
            <?php endif; ?>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>
