<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?php if ( $jadwal_ujian['doc_status'] != $this->config->item("doc_status_done")) : ?>
                    <a href="elearning/asc_submission/publish/<?php echo $id_jadwal_ujian; ?>">
                    <button class="btn bg-green btn-flat margin" onclick="return confirm('Anda yakin menghapus data ini ?');"><i class="fa fa-check"></i> Publish hasil ujian</button>
                    </a>
                    <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body with-border">
                    <table id="table" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Jadwal</td>
                            <td>NISN</td>
                            <td>Nama</td>
                            <td>Mulai</td>
                            <td>Mengumpulkan</td>
                            <td>Nilai</td>
                            <td>Status</td>
                            <td>Jawaban Siswa</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>