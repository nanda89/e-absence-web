<section class="content">    
<div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php echo validation_errors(); ?>
                    </h3>
                </div><!-- /.box-header -->
      <form class="form-horizontal" action="<?php echo base_url('elearning/asc_ujian/form/' . $this->uri->segment(4,'0') . '/' . $this->uri->segment(5,'e')); ?>" method="post" enctype="multipart/form-data">        
        <input type="hidden" class="form-control" id="inputEmail3" name="id">
          <div class="box-body">
          <?php echo form_hidden('id_ujian', $id_ujian); ?>
          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Kode Soal</h5></label>
              <div class="col-sm-9">
              <?php
                    $value = '';
                    if ( $ujian != null )
                    {
                        $value = $ujian['kode'];
                    }
                    $array_data = array(
                      'id' => 'kode',
                      'name' => 'kode',
                      'class' => 'form-control',
                      'value' => set_value('kode', $value),
                      'readonly' => true,
                      'type' => 'text'
                    );
                    echo form_input($array_data);
                    echo form_error('kode');
                ?>
              </div>
          </div>

          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Judul Soal</h5></label>
              <div class="col-sm-9">
              <?php
                  $value = '';
                  if ( $ujian != null )
                  {
                      $value = $ujian['judul'];
                  }
                  $array_data = array(
                    'id' => 'judul',
                    'name' => 'judul',
                    'class' => 'form-control',
                    'value' => set_value('judul', $value),
                    'type' => 'text',
                    'placeholder' => "Judul"
                  );
                  echo form_input($array_data);
                  echo form_error('judul');
              ?>
              </div>
          </div>

          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Deskripsi</h5></label>
              <div class="col-sm-9">
              <?php
                  $value = '';
                  if ( $ujian != null )
                  {
                      $value = $ujian['deskripsi'];
                  }
                  $array_data = array(
                    'id' => 'deskripsi',
                    'name' => 'deskripsi',
                    'class' => 'form-control',
                    'value' => set_value('deskripsi', $value),
                    'type' => 'text',
                    'placeholder' => "Penjelasan tentang ujian"
                  );
                  echo form_textarea($array_data);
                  echo form_error('deskripsi');
              ?>
              </div>
          </div>

          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Mata Pelajaran</h5></label>
              <div class="col-sm-9">
              <?php
                  $array_data = array(
                    'id' => 'mapel',
                    'name' => 'mapel',
                    'class' => 'form-control',
                    'required' => true
                  );
                  $options = array();
                  $selected = null;
                  if ( $ujian != null )
                  {
                      $selected = $ujian['id_mapel'];
                      $options[$selected] = $ujian['mapel'];
                  }
                  echo form_dropdown('mapel', $options, $selected, $array_data);
                  echo form_error('mapel');
              ?>
              </div>
          </div>
          <?php if ( $ujian_tipe == $this->config->item('ujian_tipe')['p'] ) : ?>
          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Jumlah Pilihan</h5></label>
              <div class="col-sm-9">
              <?php
                  $value = '';
                  if ( $ujian != null )
                  {
                      $value = $ujian['jumlah_pilihan'];
                  }
                  $array_data = array(
                    'id' => 'jumlah_pilihan',
                    'name' => 'jumlah_pilihan',
                    'class' => 'form-control',
                    'value' => set_value('jumlah_pilihan', $value),
                    'type' => 'number',
                    'min' => 1,
                    'placeholder' => "Jumlah Pilihan",
                    'required' => true
                  );
                  echo form_input($array_data);
                  echo form_error('jumlah_pilihan');
              ?>
              </div>
          </div>
          <?php else : ?>
            <?php echo form_hidden('jumlah_pilihan', 0); ?>
          <?php endif; ?>

          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Tipe</h5></label>
              <div class="col-sm-5">
                <?php echo form_hidden('tipe', $ujian_tipe); ?>
                <label class="control-label"><span></span><h5><?php echo $ujian_tipe;?></h5></label>
              </div>
          </div>

          <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label"><h5>Status</h5></label>
              <div class="col-sm-5">
                <?php echo form_hidden('status', $doc_status); ?>
                <label class="control-label"><span></span><h5><?php echo $doc_status;?></h5></label>
              </div>
          </div>

         </div><!-- /.box-body -->
         <div class="box-footer">
            <?php if ( !$readonly ) : ?>
            <button type="submit" name="s_process" value="edit" class="btn btn-info">Save</button>
            <?php endif; ?>
            <a href="<?php echo base_url('elearning/asc_ujian');?>">
            <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button> 
            </a>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
 </section>