<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php echo $judul_header; ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <a href="elearning/asc_materi_penerima/index?id_materi=<?php echo $id_materi; ?>">
                            <button class="btn btn-box-tool"><i class="fa fa-arrow-left"></i> Kembali</button>
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <form class="form-horizontal" action="elearning/asc_materi_penerima/form?<?php echo $_SERVER['QUERY_STRING']; ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body with-border">
                        <?php echo form_hidden('id_materi', $id_materi); ?>
                        <?php echo form_hidden('type', $type); ?>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-2 control-label">Penerima</label>
                          <div class="col-sm-4">
                            <?php
                                $array_data = array(
                                  'id' => 'penerima',
                                  'name' => 'penerima',
                                  'class' => 'form-control'
                                );
                                $options = array();
                                $selected = null;
                                echo form_dropdown('penerima', $options, $selected, $array_data);
                                echo form_error('penerima');
                            ?>
                          </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>