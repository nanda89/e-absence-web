<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $judul1; ?></li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="">
            <!-- <button id="btn-back" class="btn bg-red btn-flat margin"><i class="fa fa-refresh"></i> Back To Jadwal </button> -->
          </a>
          <!--button class="btn bg-blue btn-flat margin"><i class="fa fa-download"></i> Export to Excel</button-->
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="nama_kelas" class="col-sm-2 control-label">Kelas Tambahan</label>
            <div class="col-sm-4">
              <input class="form-control" id="nama_kelas" disabled value="<?php echo $dt_kls_tmbh->nama_kelas ?>">
            </div>
          </div>

          <div class="form-group">
            <label for="id_kelas_reg" class="col-sm-2 control-label">Kelas Reguler</label>
            <div class="col-sm-4">
              <select id="id_kelas_reg" class="form-control">
                <option value=''>Pilih Kelas Reguler</option>
                <?php
                foreach ($dt_kelas_reg->result() as $dt_kelas_reg) {
                  echo "<option value='$dt_kelas_reg->id_kelas'>$dt_kelas_reg->kelas</option>";
                } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Kelas Tambahan</label>
            <div class="col-sm-4">
              <select id="id_kelas_tmbhn" name="jenjang" class="form-control">
                <option value=''>Pilih Kelas Tambahan</option>
                <?php
                foreach ($dt_kelas_tambahan->result() as $dt_kelas_tambahan) {
                  echo "
                <option value='$dt_kelas_tambahan->id_kelas'>$dt_kelas_tambahan->nama_kelas</option>";
                } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label"></label>
            <div class="col-sm-6">
              <button type="button" onclick="show_students()" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Show Murid</button>
              <button type="button" id="btn-show" class="btn btn-primary btn-flat margin"><i class="fa fa-edit"></i> Show Kelas</button>
              <button type="button" id="btn-add" class="btn btn-warning btn-flat margin" style="display: none;"><i class="fa fa-plus"></i> Add Selected Siswa</button>
            </div>

          </div>
        </form>
      </div>
      <div class="box-body">
        <div id="tampil_siswa">
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
  $(document).on('change', '#check-all', function() {
    if ($(this).is(":checked")) {
      $(".data-check").each(function(i, e) {
        $(e).prop('checked', true);
      });
    } else {
      $(".data-check").each(function(i, e) {
        $(e).prop('checked', false);
      });
    }
  });

  $(document).on('click', '#btn-back', function() {
    window.location = "<?php echo site_url('adm/jadwal_tambahan') ?>";
  });

  $(document).on('click', '#btn-add', function() {
    var id_jadwal = "<?php echo $dt_kls_tmbh->id_jadwal_tambahan ?>";
    var id_siswa = [];
    $(".data-check").each(function(i, e) {
      if ($(e).is(":checked")) {
        id_siswa.push($(e).val());
      };
    });

    if (id_siswa.length == 0) {
      alert("Anda Belum Memilih Siswa Yang Akan Ditambahkan");
    } else {
      $.ajax({
        url: "<?php echo site_url('adm/jadwal_tambahan/add_siswa') ?>",
        type: "POST",
        data: "&id_jadwal=" + id_jadwal + "&id_siswa=" + id_siswa,
        cache: false,
        success: function(msg) {
          data = msg.split("|");
          if (data[0] == "nihil") {
            show_kelas(id_jadwal);
          }
        }
      });
    }
  });

  function show_students() {
    $("#tampil_siswa").show();
    var id_kelas_reg = document.getElementById("id_kelas_reg").value;
    var id_kelas_tmbhn = document.getElementById("id_kelas_tmbhn").value;

    var xhr;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
      xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 8 and older
      xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var data = "id_kelas_reg=" + id_kelas_reg + "&id_kelas_tmbhn=" + id_kelas_tmbhn;
    xhr.open("POST", "adm/jadwal_tambahan/t_siswa", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(data);
    xhr.onreadystatechange = display_data;

    function display_data() {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          document.getElementById("tampil_siswa").innerHTML = xhr.responseText;
        } else {
          alert('There was a problem with the request.');
        }
      }
    }

    $('#btn-add').css('display', 'inline');
  }

  function show_kelas(id_jadwal) {
    var xhr;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
      xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 8 and older
      xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var data = "id_jadwal=" + id_jadwal;
    xhr.open("POST", "adm/jadwal_tambahan/show_siswa", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(data);
    xhr.onreadystatechange = display_data;

    function display_data() {
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          document.getElementById("tampil_siswa").innerHTML = "";
          document.getElementById("tampil_siswa").innerHTML = xhr.responseText;
        } else {
          alert('There was a problem with the request.');
        }
      }
    }

  }

  $(document).on('click', '#btn-show', function() {
    var id_jadwal = "<?php echo $dt_kls_tmbh->id_jadwal_tambahan ?>";
    show_kelas(id_jadwal);
  });

  $(document).on('click', '.btn-del-siswa', function() {
    var id_jadwal = $(this).data('idjdwl');
    var id_siswa = $(this).data('idsiswa');
    var nama_siswa = $(this).data('namasiswa');

    if (window.confirm('Apakah Anda Yakin Akan Menghapus Siswa' + nama_siswa + "Dari Kelas Tambahan Ini?")) {
      $.ajax({
        url: "<?php echo site_url('adm/jadwal_tambahan/delete_siswa') ?>",
        type: "POST",
        data: "id_jadwal=" + id_jadwal + "&id_siswa=" + id_siswa,
        cache: false,
        success: function(msg) {
          data = msg.split("|");
          if (data[0] == "nihil") {
            alert("Jadwal Tambahan Berhasil Dihapus.");
            show_kelas(id_jadwal);
          } else {
            alert("Jadwal Tambahan Gagal Dihapus.");
          }
        }
      })
    } else {
      show_kelas(id_jadwal);
    }
  });
</script>