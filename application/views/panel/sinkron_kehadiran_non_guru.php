<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo $judul1; ?>
			<small><?php echo $judul2; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
			<li class="active"><?php echo $judul1; ?></li>
		</ol>
	</section>
	<section class="content">

    <!-- VIEW -->
    <?php if($set == "view") : ?>
    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">                          
          <!-- <a href='adm/sinkron_kehadiran_non_guru/export_now?a=1' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>                          
          <a href='adm/sinkron_kehadiran_non_guru/export_now?a=2' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a> -->
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/sinkron_kehadiran_non_guru/filter" method="post" enctype="multipart/form-data">                                
          <div class="form-group">
            <?php          
              $tgl1  = gmdate("d-m-Y", time()+60*60*7);
              $hari  = hari();
            ?>
            <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
            <div class="col-sm-2">              
              Dari :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
            </div>
            <div class="col-sm-2">              
              Sampai :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
            </div> 
            <div class="col-sm-4" style="margin-top:10px;">
              <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
              <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>              
            </div>          
          </div>
        </form>  
      </div> 
      <div class="box-body">        
        <?php
        if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
        ?>                  
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>  
            </button>
        </div>
        <?php
        }
            $_SESSION['pesan'] = '';                        

        ?>
        <?php 
        $args = array(
          'tgl'       => date('Y-m-d'),
          'tgl_akhir' => date('Y-m-d')
        );
        $absensi = get_all_harian_absen_kehadiran_non_guru($args);
        //debugji($absensi); ?>
        <table id="example2" class="table table-bordered table-hovered">
        <thead>
            <tr>
              <th width="5%">No</th>
              <th>Nama Non Guru</th>
              <th>Tanggal</th>
              <th>Jam masuk</th>
              <th>Jam pulang</th>
              <th>Validasi jam masuk</th>
              <th>Validasi jam pulang</th>
              <th>Keterangan jam masuk</th>
              <th>Keterangan jam pulang</th>         
            </tr>
          </thead>
          <tbody>
            <?php $list_ket = get_keterangan_absen();
            $no=1; foreach( $absensi as $absen ) : ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $absen->nama; ?></td>
              <td><?php echo $absen->tgl; ?></td>
              <td><?php echo $absen->jam_masuk_kerja; ?></td>
              <td><?php echo $absen->jam_pulang_kerja; ?></td>
              <td><?php echo $absen->validasi_jam_masuk; ?></td>
              <td><?php echo $absen->validasi_jam_pulang; ?></td>
              <td><?php echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk; ?>
              </td>
              <td><?php echo $absen->keterangan_jam_pulang; ?></td>
            </tr>
            <?php $no++; endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>

    <!-- ABSEN GURU FILTER -->
    <?php elseif($set == "filter") : ?>
    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href='adm/sinkron_kehadiran_non_guru' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>        
          <a href='adm/sinkron_kehadiran_non_guru/export_filter?a=1&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin hide"><i class="fa fa-file-excel-o"></i> Export</a>        
          <a href='adm/sinkron_kehadiran_non_guru/export_filter?a=2&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin hide"><i class="fa fa-print"></i> Cetak</a>
          <!-- <a href='adm/sinkron_kehadiran_non_guru/sinkronisasi?a=2&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-success btn-flat margin"><i class="fa fa-refresh"></i> Sinkronisasi</a>         -->			
          <input type="hidden" id="tgl" value="<?php echo $tgl ?>">
          <input type="hidden" id="tgl_akhir" value="<?php echo $tgl_akhir ?>">
          <input type="hidden" id="a" value="2">
          <button onclick="kirim()" class="btn btn-success btn-flat"><i class="fa fa-refresh" type="button"></i> Sinkronisasi</button>        
          <!-- <a href='adm/sinkron_kehadiran_non_guru/sinkronisasi?a=2&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-success btn-flat margin"><i class="fa fa-refresh"></i> Sinkronisasi</a>         -->
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <div class="box-body with-border">
        <form class="form-horizontal" action="adm/sinkron_kehadiran_non_guru/filter" method="get" enctype="multipart/form-data">                                
          <div class="form-group">
            <?php
              $tgl1  = date("l", strtotime($tgl));
              $hari  = cek_hari($tgl1);
            ?>
            <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
            <div class="col-sm-2">              
              Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
            </div>
            <div class="col-sm-2">              
              Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
            </div> 

          </div>
        </form>  
      </div> 
      <div class="box-body">
        <?php 
        $tgl_param = explode('-', $tgl);
        $tgl_akhir_param = explode('-', $tgl_akhir);
        $args = array(
          'tgl'       => $tgl_param[2] . '-' . $tgl_param[1] . '-' . $tgl_param[0],
          'tgl_akhir' => $tgl_akhir_param[2] . '-' . $tgl_akhir_param[1] . '-' . $tgl_akhir_param[0]
        );
        $absensi = get_all_harian_absen_kehadiran_non_guru($args);
        //debugji($absensi); ?>
        <table id="example2" class="table table-bordered table-hovered">
        <thead>
            <tr>
              <th width="5%">No</th>
              <th>Nama Non Guru</th>
              <th>Tanggal</th>
              <th>Jam masuk</th>
              <th>Jam pulang</th>
              <th>Validasi jam masuk</th>
              <th>Validasi jam pulang</th>
              <th>Keterangan jam masuk</th>
              <th>Keterangan jam pulang</th>         
            </tr>
          </thead>
          <tbody>
            <?php $list_ket = get_keterangan_absen();
            $no=1; foreach( $absensi as $absen ) : ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $absen->nama; ?></td>
              <td><?php echo $absen->tgl; ?></td>
              <td><?php echo $absen->jam_masuk_kerja; ?></td>
              <td><?php echo $absen->jam_pulang_kerja; ?></td>
              <td><?php echo $absen->validasi_jam_masuk; ?></td>
              <td><?php echo $absen->validasi_jam_pulang; ?></td>
              <td>
                <?php 
                if( $absen->validasi_jam_masuk == 'nihil' ) {
                  echo '<a href="#" class="sinkron-ket-kehadiran-non-guru" data-type="select" data-url="adm/sinkron_kehadiran_non_guru/absen_manual" data-pk="'.$absen->id_non_guru.'" data-tgl="'.$absen->tgl.'" data-title="Pilih keterangan">'; 
                  echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk;
                  echo '</a>';
                } else {
                  echo ( isset($list_ket[$absen->keterangan_jam_masuk]) ) ? $list_ket[$absen->keterangan_jam_masuk] : $absen->keterangan_jam_masuk;   
                }
                ?>
              </td>
              <td><?php echo $absen->keterangan_jam_pulang; ?></td>
            </tr>
            <?php $no++; endforeach; ?>
          </tbody>
        </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

	  <?php endif; ?>
    
  </section>
</div>


<script type="text/javascript">
function kirim()
{	
  var value={
    tgl:$("#tanggal").val(),
    tgl_akhir:$("#tgl_akhir").val(),
    a:$("#a").val()
  };
  $.ajax({  	
       beforeSend: function() { $('#loading-status').show(); },
       url:"<?php echo site_url('adm/sinkron_kehadiran_non_guru/sinkronisasi')?>",
       type:"GET",
       data:value,
       cache:false,
       success:function(msg){      
        data=msg.split("|");  
        if(data[0] == 'ok'){
	        $('#loading-status').hide();          
	        window.location.href = "<?php echo site_url('adm/sinkron_kehadiran_non_guru')?>";
	      }
       },
       statusCode: {
    500: function() {
      $('#loading-status').hide();
      alert("Something Wen't Wrong");
    }
  }
  });
}
</script>