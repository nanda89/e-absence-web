<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "view") {
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>
              <a href='adm/pendataan/export_all' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export Semua Data</a>
              <a href='adm/pendataan/import_all' class="btn btn-primary btn-flat margin"><i class="fa fa-upload"></i> Import Semua Data</a>
            </h3>
            <br>
            <p>Fitur ini dapat digunakan untuk merubah data <b>jenjang, tingkat dan kelas </b> siswa secara keseluruhan. Anda
              dapat mengunduh versi excel, kemudian ubah data-data yang diperlukan, simpan dan ubah tipe datanya menjadi <b>*.csv</b>
              kemudian upload ke sistem melalui tombol <b>Import Semua Data</b>.
              Untuk merubah data per-kelas anda dapat menggunakan fitur <b>Filter Data</b>. </p>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body with-border">
            <form class="form-horizontal" action="adm/pendataan/filter" method="get" enctype="multipart/form-data">
              <div class="form-group">

<label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                <div class="col-sm-4">
                  <select id="id_tahun" name="id_tahun" required class="form-control">
                    <option value=''>Pilih Tahun Ajaran</option>
                    <?php
foreach ($dt_ta->result() as $t) {
        echo "
                      <option value='$t->id_ta'>$t->tahun_ajaran</option>
                      ";
    }
    ?>
                  </select>
                </div>
<label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-4">
                  <select id="kelas" name="id_kelas" required class="form-control">
                    <option value=''>Pilih Kelas</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-4">
                  <select id="jenjang" name="id_jenjang" required class="form-control">
                    <option value=''>Pilih Jenjang</option>
                    <?php
foreach ($dt_jenjang->result() as $s) {
        echo "
                      <option value='$s->id_jenjang'>$s->jenjang</option>
                      ";
    }
    ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="button" class="btn bg-maroon btn-flat margin" onclick="searchByFilter()"><i class="fa fa-list"></i> Filter</button>
                  <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                  <button type="button" onclick="kosong()" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
                </div>
              </div>
            </form>
          </div>
          <div class="box-body" style="display:none;">
            <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $_SESSION['pesan'] ?>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';
    ?>
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <!-- <th width="1%"><input type="checkbox" id="check-all"></th> -->
                  <th width="2%">No</th>
                  <th>No.Induk</th>
                  <th>NISN</th>
                  <th>Nama Lengkap</th>
                  <th>Jenjang</th>
                  <th>Kelas</th>
                  <th width="5%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
/*$no = 1;
    foreach ($dt_penempatan->result() as $row) {
    echo "
    <tr>
    <td><input type='checkbox' class='data-check' value='$row->id_penempatan'></td>
    <td>$no</td>
    <td>$row->id_siswa</td>
    <td>$row->nisn</td>
    <td>$row->nama_lengkap</td>
    <td>$row->jenjang</td>
    <td>$row->kelas</td>
    <td>";?>
    <form method="post" action="adm/siswa/process">
    <input type="hidden" name="id" value="<?php echo $row->id_siswa ?>" >
    <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>
    </form>
    </td>
    </tr>
    <?php
    $no++;
    }*/
    ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>

    </div>

    <?php
} elseif ($set == "import") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/pendataan">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/pendataan/importcsv" method="post" enctype="multipart/form-data">
              <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Browse File CSV</label>
                    <div class="col-sm-4">
                      <input type="file" name="userfile">
                    </div>
                  </div>
              </div>
              <div class="box-footer">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-10">
                    <button type="submit" name="submit"  value="UPLOAD" class="btn btn-info btn-flat"><i class="fa fa-upload"></i> Upload</button>
                  </div>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
// $("#jenjang").change(function(){
//   var id_jenjang = $("#jenjang").val();
//   $.ajax({
//     url : "<?php echo site_url('adm/pendataan/get_kelas') ?>",
//     type:"POST",
//     data:"id_jenjang="+id_jenjang,
//     cache:false,
//     success:function(msg){
//       $("#kelas").html(msg);
//     }
//   })
// });
$("#jenjang").change(function(){

  var id_jenjang = $("#jenjang").val();
  var id_ta = $("#id_tahun").val();
  if(id_ta != ""){
    getKelas(id_jenjang , id_ta);
  } else{
    $("#jenjang").prop('selectedIndex',0);
    alert("harap pilih tahun ajaran");
  }
});
function getKelas(id_jenjang , id_ta){
$.ajax({
    url : "<?php echo site_url('adm/siswa/get_kelas') ?>",
    type:"POST",
    data:{"id_jenjang" : id_jenjang ,"id_ta":id_ta},
    cache:false,
    success:function(msg){
      $("#kelas").html(msg);
    }
  })
}
$("#id_tahun").change(function(){

  $("#jenjang").prop('selectedIndex',0);

});
</script>

<script type="text/javascript">
function kosong(args){
  $("#tingkat").val('');
  $("#id_jenjang").val('');
  $("#id_tahun").val('');
  $("#id_kelas").val('');
  $("#tampil_pendataan").hide();
}
function detail(a){
  var id        = a;
  var s_process = 'detail';
  $.ajax({
    url : "<?php echo site_url('adm/siswa/process') ?>",
    type:"POST",
    data:"id="+id+"&s_process="+s_process,
    cache:false,
  })
}

function hapus_penempatan(a){
  var id_penempatan     = a;
  $.ajax({
      url : "<?php echo site_url('adm/penempatan/delete_penempatan') ?>",
      type:"POST",
      data:"id_penempatan="+id_penempatan,
      cache:false,
      success:function(msg){
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_pendataan();
          }
      }
  })
}

function kirim_data_pendataan(){
  $("#tampil_pendataan").show();
  var id_tahun    = document.getElementById("id_tahun").value;
  var id_kelas    = document.getElementById("id_kelas").value;
  if(id_tahun=="" || id_kelas==""){
    alert("Isikan semua data dengan lengkap");
    return false;
  }
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "id_tahun="+id_tahun+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/pendataan/t_pendataan", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_pendataan").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
</script>

<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/pendataan/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}

function searchByFilter(){
    var params = {
      id_ta : $('#id_tahun').val(),
      id_jenjang : $('#jenjang').val(),
	  	id_kelas : $('#kelas').val()
    }
    $.ajax({
      type: "POST",
      url: "<?php echo site_url('adm/pendataan/search_by_filter') ?>",
      data: params,
      async : false,
      dataType : 'json',
      success: function(data) {
        $('#example2 tbody').empty();
        var no;
        var tr = '';
        for (i=0; i<data.length; i++) {
          no = i+1;
          tr += '<tr>'+
                  // '<td><input type="checkbox" class="data-check" value="'+data[i].id_penempatan+'"></td>'+
                  '<td>'+no+'</td>'+
                  '<td>'+data[i].id_siswa+'</td>'+
                  '<td>'+data[i].nisn+'</td>'+
                  '<td>'+data[i].nama_lengkap+'</td>'+
                  '<td>'+data[i].jenjang+'</td>'+
                  '<td>'+data[i].kelas+'</td>'+
                  '<td>'+
                    '<form method="post" action="adm/siswa/process">'+
                      '<input type="hidden" name="id" value="'+data[i].id_siswa+'" >'+
                      '<button data-toggle="tooltip" title="Detail" name="s_process" type="submit" value="detail" class="btn btn-sm btn-info btn-flat"><i class="fa fa-eye"></i></button>'+
                    '</form>'+
                  '</td>'+
                '</tr>';
        }
        $('#example2 tbody').append(tr);
        $('.box-body').removeAttr("style");
      },
      statusCode: {
        200: function() {

        },
        500: function() {
          alert("Something Wen't Wrong");
        }
      }
    });
  }
</script>