<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">          
            <?php 
              $id_ta = $_GET['id_tahun'];
              $id_jenjang = $_GET['id_jenjang'];
              $bulan = $_GET['bulan'];
              $tahun = $_GET['tahun'];
              ?>
              <a href="adm/laporan/keuangan">
                <button class="btn btn-sm bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>
              </a>
              <?php
              $r = $dt_tu->num_rows(); 
              if($r>0){
              echo "
              <a href='adm/laporan/keuangan_pdf?id_ta=$id_ta&id_jenjang=$id_jenjang&bulan=$bulan&tahun=$tahun'>
                <button class='btn btn-sm bg-primary btn-flat margin'><i class='fa fa-file-pdf-o'></i> Export As PDF</button>
              </a>
              
              <a href='adm/laporan/keuangan_excel?id_ta=$id_ta&id_jenjang=$id_jenjang&bulan=$bulan&tahun=$tahun'>
                <button class='btn btn-sm btn-warning btn-flat margin'><i class='fa fa-file-excel-o'></i> Export As Excel</button>
              </a>
              ";
              } ?>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="box-body with-border">
        </div>
        <?php    
            if($dt_tu->num_rows()>0){
                $r = $dt_tu->row();
                $id_jenjang = $r->id_jenjang;   
        ?>
        <b>Riwayat Pembayaran</b>
        <table class="table table-bordered responsive-utilities jambo_table bulk_action" id="example2">
            <thead>
                <tr class="headings">                                                
                    <th width="5%">No </th>
                    <th>No.Induk</th>            
                    <th>Nama Siswa</th>
                    <th>Jenjang</th>
                    <th>Kelas</th>    
                    <th>Biaya</th>
                    <th>Nominal</th>
                    <th>Tgl.Bayar</th>
                </tr>
            </thead>        
            <tbody> 
            <?php 
            $total = 0;
            $no=1;
            foreach ($dt_tu->result() as $s) {                
              $h = $s->nominal;
              $nom = mata_uang($h);
              $tgl = date("d F Y", strtotime($s->tgl_bayar));              
            echo "  
                <tr>
                    <td>$no</td>
                    <td>$s->id_siswa</td>
                    <td>$s->nama_lengkap</td>
                    <td>$s->jenjang</td>
                    <td>$s->kelas</td>
                    <td>$s->biaya</td>
                    <td>Rp. $nom</td>
                    <td>$tgl</td>                                                 
                </tr>
            ";
            $total = $total + $h;
            $t = mata_uang($total);
            $no++;
            }    
            ?>            
            </tbody>
            <tbody>
                <tr>
                <td>&nbsp;</td>
                <td colspan="4" align="right"><b>Total</b></td>
                <td><b>Rp. <?php echo $t ?></b></td>
                <td>&nbsp;</td>
            </tr>
            </tbody> 
        </table>
        </div><!-- /.box-body -->
 
<?php 
}else{
    echo "<b>Data tidak ditemukan</b>";
} ?>

   </div><!-- /.box -->    
  </section>
</div>