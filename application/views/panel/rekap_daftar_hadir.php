<script src="assets/panel/js_chart/jquery-1.9.1.min.js" type="text/javascript"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $judul1; ?>
      <small><?php echo $judul2; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $judul1; ?></li>
    </ol>
  </section>
  <section class="content">

    <!-- HALAMAN VIEW ABSENSI SISWA HARIAN -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <!-- <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button> -->
              <!-- <a href='adm/absen_siswa/export_all' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export Semua Data</a> -->
            </h3>
            <!-- <ul>
                <li>Anda bisa melakukan proses pencarian data dengan banyak kata kunci sekaligus, misal : kelas (spasi) jenjang (spasi) tanggal</li>
                <li>Anda bisa melakukan filter data rekap absen perhari atau perminggu atau perbulan dalam tiap kelas yang anda tentukan.</li>
                <li>Dengan menggunakan fasilitas filter, anda juga bisa menggunakan fitur pengiriman SMS harian atau mingguan.</li>
                <li>Anda juga bisa mencetak rekap absen siswa ini baik secara keseluruhan maupun perkelas.</li>
              </ul> -->
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div>

          <div class="box-body with-border">
            <form class="form-horizontal" method="get" enctype="multipart/form-data">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenis Mapel<font color="red">*</font></label>
                <div class="col-sm-4">
                  <select id="jenis" name="jenis" required class="form-control">
                    <option value='' selected disabled>Pilih Jenis</option>
                    <option value='reguler'>Reguler</option>
                    <option value='tambahan'>Tambahan</option>
                  </select>
                </div>
              </div>

              <div id="div-reguler" style="display: none">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenjang<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="jenjang" name="id_jenjang" required class="form-control">
                      <option value=''>Pilih Jenjang</option>
                      <?php
                      foreach ($dt_jenjang->result() as $s) {
                        echo "<option value='$s->id_jenjang'>$s->jenjang</option>";
                      }
                      ?>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Guru<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="guru" name="id_guru" class="form-control" required>
                      <option value="">Pilih Guru</option>
                    </select>
                  </div>

                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                  <div class="col-sm-4">
                    <select id="mapel" name="id_mapel" class="form-control">
                      <option value=''>Pilih Mapel</option>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Kelas<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="kelas" name="id_kelas" required class="form-control">
                      <option value=''>Pilih Kelas
                      </option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="ta" name="id_ta" class="form-control" required>
                      <option value="">Pilih Tahun</option>
                      <?php
                      foreach ($dt_ta->result() as $ta) {
                        echo "<option value='$ta->id_ta'>$ta->tahun_ajaran</option>";
                      }
                      ?>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Awal<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal" name="tgl_awal" placeholder="Tanggal Awal" autocomplete="off">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-6">
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Akhir<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal2" name="tgl_akhir" placeholder="Tanggal Akhir" autocomplete="off">
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                    <button type="button" id="btn-filter" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                    <button type="button" id="btn-excel" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</button>
                    <button type="button" id="btn-cetak" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</button>
                    <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
                    <br>
                    <font color="red"><i>* Wajib diisi</i></font>
                  </div>
                </div>
              </div>

              <div id="div-tambahan" style="display: none">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenjang<font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="jenjang-tambahan" name="id_jenjang" required class="form-control">
                      <option value=''>Pilih Jenjang</option>
                      <?php
                      foreach ($dt_jenjang->result() as $s) {
                        echo "<option value='$s->id_jenjang'>$s->jenjang</option>";
                      }
                      ?>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 1</label>
                  <div class="col-sm-4">
                    <select id="pembimbing1" name="id_pembimbing1" class="form-control" required>
                      <option value="">Pilih Pembimbing 1</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran <font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="mapel-tambahan" name="id_mapel" class="form-control">
                      <option value=''>Pilih Mapel Tambahan</option>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 2</label>
                  <div class="col-sm-4">
                    <select id="pembimbing2" name="id_pembimbing2" class="form-control" required>
                      <option value="">Pilih Pembimbing 2</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran <font color="red">*</font></label>
                  <div class="col-sm-4">
                    <select id="ta-tambahan" name="id_ta" class="form-control" required>
                      <option value="">Pilih Tahun</option>
                      <?php
                      foreach ($dt_ta->result() as $ta) {
                        echo "<option value='$ta->id_ta'>$ta->tahun_ajaran</option>";
                      }
                      ?>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Pembimbing 3</label>
                  <div class="col-sm-4">
                    <select id="pembimbing3" name="id_pembimbing3" class="form-control" required>
                      <option value="">Pilih Pembimbing 3</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Guru</label>
                  <div class="col-sm-4">
                    <select id="guru-tambahan" name="id_guru" class="form-control" required>
                      <option value="">Pilih Guru</option>
                    </select>
                  </div>

                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Awal</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal3" name="tgl_awal" placeholder="Tanggal Awal" autocomplete="off">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-6">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Akhir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal4" name="tgl_akhir" placeholder="Tanggal Akhir" autocomplete="off">
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                    <button type="button" id="btn-filter" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>
                    <button type="button" id="btn-excel" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</button>
                    <button type="button" id="btn-cetak" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</button>
                    <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>
                    <br>
                    <font color="red"><i>* Wajib diisi</i></font>
                  </div>
                </div>
              </div>

            </form>
          </div>

          <div class="box-body">
            <div id="list-siswa" style="overflow-x:auto; width: 100%;"></div>

            <script type="text/javascript">
              $(document).on('change', '#jenis', function() {
                var jenis = $('#jenis').val();
                if (jenis === "tambahan") {
                  $('#div-reguler').css('display', 'none');
                  $('#div-tambahan').css('display', 'block');
                } else if (jenis === "reguler") {
                  $('#div-tambahan').css('display', 'none');
                  $('#div-reguler').css('display', 'block');
                }
              });

              $("#jenjang").change(function() {
                var id_jenjang = $("#jenjang").val();
                $.ajax({
                  url: "<?php echo site_url('adm/pendataan/get_kelas') ?>",
                  type: "POST",
                  data: "id_jenjang=" + id_jenjang,
                  cache: false,
                  success: function(msg) {
                    //$("#kelas").html(msg);
                    get_mapel(id_jenjang);
                    get_guru(id_jenjang);
                  }
                })
              });

              $("#jenjang-tambahan").change(function() {
                var id_jenjang = $("#jenjang-tambahan").val();
                get_mapel_tambahan(id_jenjang);
                get_guru(id_jenjang);
              });

              $("#guru").change(function() {
                var id_guru = $('#guru').val();

                $.ajax({
                  url: "<?php echo site_url('adm/rekap_daftar_hadir/get_kelas_reg_by_guru') ?>",
                  type: "POST",
                  data: "id_guru=" + id_guru,
                  cache: false,
                  success: function(msg) {
                    console.log(msg);
                    $("#kelas").html(msg);
                  }
                })
              });

              $("#guru-tambahan").change(function() {
                if ($(this).val() == "") {
                  $('#pembimbing1').attr('disabled', false);
                  $('#pembimbing2').attr('disabled', false);
                  $('#pembimbing3').attr('disabled', false);
                } else {
                  $('#pembimbing1').attr('disabled', true);
                  $('#pembimbing2').attr('disabled', true);
                  $('#pembimbing3').attr('disabled', true);
                }

              });

              $("#pembimbing1").change(function() {
                if ($(this).val() == "") {
                  $('#guru-tambahan').attr('disabled', false);
                  $('#pembimbing2').attr('disabled', false);
                  $('#pembimbing3').attr('disabled', false);
                } else {
                  $('#guru-tambahan').attr('disabled', true);
                  $('#pembimbing2').attr('disabled', true);
                  $('#pembimbing3').attr('disabled', true);
                }

              });

              $("#pembimbing2").change(function() {
                if ($(this).val() == "") {
                  $('#guru-tambahan').attr('disabled', false);
                  $('#pembimbing1').attr('disabled', false);
                  $('#pembimbing3').attr('disabled', false);
                } else {
                  $('#guru-tambahan').attr('disabled', true);
                  $('#pembimbing1').attr('disabled', true);
                  $('#pembimbing3').attr('disabled', true);
                }

              });

              $("#pembimbing3").change(function() {
                if ($(this).val() == "") {
                  $('#guru-tambahan').attr('disabled', false);
                  $('#pembimbing1').attr('disabled', false);
                  $('#pembimbing2').attr('disabled', false);
                } else {
                  $('#guru-tambahan').attr('disabled', true);
                  $('#pembimbing1').attr('disabled', true);
                  $('#pembimbing2').attr('disabled', true);
                }

              });

              $(document).on('click', '#btn-filter', function() {
                var jenis = $('#jenis').val();
                if (jenis === "tambahan") {
                  get_data_rekap_tambahan();
                } else if (jenis === "reguler") {
                  get_data_rekap_reguler();
                }

              });

              $(document).on('click', '#btn-excel', function() {
                var jenis = $('#jenis').val();
                if (jenis === "tambahan") {
                  export_data_tambahan("excel");
                } else if (jenis === "reguler") {
                  export_data_reguler("excel");
                }

              });

              $(document).on('click', '#btn-cetak', function() {
                var jenis = $('#jenis').val();
                if (jenis === "tambahan") {
                  export_data_tambahan("cetak");
                } else if (jenis === "reguler") {
                  export_data_reguler("cetak");
                }
              });

              function get_mapel(a) {
                $.ajax({
                  url: "<?php echo site_url('adm/rekap_daftar_hadir/get_mapel') ?>",
                  type: "POST",
                  data: "jenjang=" + a,
                  cache: false,
                  success: function(msg) {
                    $("#mapel").html(msg);
                  }
                })
              }

              function get_guru(a) {
                $.ajax({
                  url: "<?php echo site_url('adm/jadwal_tambahan/get_guru') ?>",
                  type: "POST",
                  data: "id_jenjang=" + a,
                  cache: false,
                  success: function(msg) {
                    var msg1 = "<option value = ''> Pilih Guru </option>" + msg;
                    var msg2 = "<option value = ''> Pilih Pembimbing 1 </option>" + msg;
                    var msg3 = "<option value = ''> Pilih Pembimbing 2 </option>" + msg;
                    var msg4 = "<option value = ''> Pilih Pembimbing 3 </option>" + msg;

                    $("#guru").html(msg1);
                    $("#guru-tambahan").html(msg1);
                    $("#pembimbing1").html(msg2);
                    $("#pembimbing2").html(msg3);
                    $("#pembimbing3").html(msg4);
                  }
                })
              }

              function get_mapel_tambahan(id_jenjang) {
                $.ajax({
                  url: "<?php echo site_url('adm/jadwal_tambahan/get_mapel') ?>",
                  type: "POST",
                  data: "id_jenjang=" + id_jenjang,
                  cache: false,
                  success: function(msg) {
                    $("#mapel-tambahan").html(msg);
                  }
                })
              }

              function get_data_rekap_reguler() {
                var id_kelas = $('#kelas').val();
                var id_jenjang = $('#jenjang').val();
                var id_mapel = $('#mapel').val();
                var id_guru = $('#guru').val();
                var id_ta = $('#ta').val();
                var tgl_awal = $('#tanggal').val();
                var tgl_akhir = $('#tanggal2').val();
                var html = "";

                $('#list-siswa').empty();

                if (id_jenjang == "" || id_guru == "" ||
                  id_ta == "" || id_kelas == "") {
                  alert("Isikan data dengan lengkap...!");
                } else {
                  $.ajax({
                    url: "<?php echo site_url('assets/file/daftar_hadir.php') ?>",
                    type: "GET",
                    data: "id_kelas=" + id_kelas + "&id_mapel=" + id_mapel +
                      "&id_jenjang=" + id_jenjang + "&dari=" + tgl_awal +
                      "&ke=" + tgl_akhir + "&guru=" + id_guru + "&id_ta=" + id_ta +
                      "&jenis=Reguler",
                    cache: false,
                    success: function(msg) {

                      var msgJSON = JSON.parse(msg);
                      var tgl = msgJSON.tanggal;
                      var siswa = msgJSON.siswa;

                      if (tgl.length == 0 || siswa.length == 0) {
                        alert("Data yang dicari tidak ditemukan.");
                      } else {
                        html += '<table id="table" class="table table-bordered table-hovered">' +
                          '<thead><tr><th width="1%" style="text-align:center;">No</th><th width="3%" style="text-align:center;">STB</th>' +
                          '<th width="15%" style="text-align:center;">Nama Siswa</th><th width="1%" style="text-align:center;">JK</th>';

                        for (var i = 0; i < tgl.length; i++) {
                          html += '<th width="1%" style="text-align:center;">' + tgl[i].tgl + '</th>';
                        }
                        html += '<td width="1%" style="text-align:center;">Hadir</td><td width="2%" style="text-align:center;">Tdk. Hdr</td>';
                        html += '</tr></thead><tbody>';

                        for (var i = 0; i < siswa.length; i++) {
                          html += '<tr><td>' + siswa[i].no + '</td><td style="text-align:center;">' + siswa[i].stb + '</td><td>' +
                            siswa[i].nama + '</td><td style="text-align:center;">' + siswa[i].sex + '</td>';

                          var absen = siswa[i].absen;
                          for (var j = 0; j < absen.length; j++) {
                            html += '<td style="text-align:center;">' + absen[j] + '</td>';
                          }
                          html += '<td style="text-align:center;">' + siswa[i].hadir + '</td><td style="text-align:center;">' + siswa[i].alpha + '</td>';
                          html += '</tr>';
                        }
                        html += '</tbody></table> <br>';

                        html += '<table> <tr><td style="width: 20%;"><b>Total Siswa<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_siswa + '</td></tr>' +
                          '<tr><td style="width: 20%;"><b>Total Tatap Muka<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_tatap_muka + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Hadir<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_hadir + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Sakit<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_sakit + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Izin<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_izin + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Alpha<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_alpha + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Nihil<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%">' +
                          msgJSON.total_nihil + '</td></tr>' +
                          '</table><br>';

                        var att = msgJSON.attachment;
                        if (typeof msgJSON.attachment != 'undefined') {
                          if (att.length > 0) {
                            html += '<table id="table-attach" class="table table-bordered table-hovered">' +
                              '<thead><tr><th width="5%">No Urut</th><th width="10%">Tanggal Tatap Muka</th>' +
                              '<th width="45%">Standar Kompetensi/Kompetensi Dasar/Indikator</th><th width="40%">Keterangan</th></tr></thead>';

                            for (var i = 0; i < att.length; i++) {
                              html += '<tbody><tr><td>' + att[i].no + '</td><td>' + att[i].tgl_tatap_muka +
                                '</td><td>' + att[i].kompetensi + '</td>' +
                                '<td>' + att[i].keterangan + '</td></tr>';
                            }

                            html += '</table>';
                          }
                        }
                        $('#list-siswa').append(html);
                      }
                    }
                  });
                }
              }

              function get_data_rekap_tambahan() {
                var id_jenjang = $('#jenjang-tambahan').val();
                var id_mapel = $('#mapel-tambahan').val();
                var id_guru_tambahan = $('#guru-tambahan').val();
                var id_pembimbing1 = $('#pembimbing1').val();
                var id_pembimbing2 = $('#pembimbing2').val();
                var id_pembimbing3 = $('#pembimbing3').val();
                var id_ta = $('#ta-tambahan').val();
                var tgl_awal = $('#tanggal3').val();
                var tgl_akhir = $('#tanggal4').val();
                var html = "";

                $('#list-siswa').empty();

                if (id_guru_tambahan == "") {
                  if (id_pembimbing1 != "") {
                    id_guru_tambahan = id_pembimbing1;
                  } else if (id_pembimbing2 != "") {
                    id_guru_tambahan = id_pembimbing2;
                  } else if (id_pembimbing3 != "") {
                    id_guru_tambahan = id_pembimbing3;
                  }
                }

                if (id_jenjang == "" || id_mapel == "" || id_ta == "") {
                  alert("Isikan data dengan lengkap...!");
                } else if (id_guru_tambahan == "") {
                  alert("Isikan data guru atau penbimbing 1 atau pembimbing 2 atau pembimbing 3...!");
                } else {
                  $.ajax({
                    url: "<?php echo site_url('assets/file/daftar_hadir.php') ?>",
                    type: "GET",
                    data: "id_mapel=" + id_mapel +
                      "&id_jenjang=" + id_jenjang + "&dari=" + tgl_awal +
                      "&ke=" + tgl_akhir + "&guru=" + id_guru_tambahan + "&id_ta=" + id_ta +
                      "&jenis=Tambahan",
                    cache: false,
                    success: function(msg) {

                      var msgJSON = JSON.parse(msg);
                      var tgl = msgJSON.tanggal;
                      var siswa = msgJSON.siswa;

                      if (tgl.length == 0 || siswa.length == 0) {
                        alert("Data yang dicari tidak ditemukan.");
                      } else {
                        html += '<table id="table" class="table table-bordered table-hovered">' +
                          '<thead><tr><th width="1%" style="text-align:center;">No</th><th width="3%" style="text-align:center;">STB</th>' +
                          '<th width="15%" style="text-align:center;">Nama Siswa</th><th width="1%" style="text-align:center;">JK</th>';

                        for (var i = 0; i < tgl.length; i++) {
                          html += '<th width="1%" style="text-align:center;">' + tgl[i].tgl + '</th>';
                        }
                        html += '<td width="1%" style="text-align:center;">Hadir</td><td width="2%" style="text-align:center;">Tdk. Hdr</td>';
                        html += '</tr></thead><tbody>';

                        for (var i = 0; i < siswa.length; i++) {
                          html += '<tr><td>' + siswa[i].no + '</td><td style="text-align:center;">' + siswa[i].stb + '</td><td>' +
                            siswa[i].nama + '</td><td style="text-align:center;">' + siswa[i].sex + '</td>';

                          var absen = siswa[i].absen;
                          for (var j = 0; j < absen.length; j++) {
                            html += '<td style="text-align:center;">' + absen[j] + '</td>';
                          }
                          html += '<td style="text-align:center;">' + siswa[i].hadir + '</td><td style="text-align:center;">' + siswa[i].alpha + '</td>';
                          html += '</tr>';
                        }
                        html += '</tbody></table> <br>';

                        html += '<table> <tr><td style="width: 20%;"><b>Total Siswa<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_siswa + '</td></tr>' +
                          '<tr><td style="width: 20%;"><b>Total Tatap Muka<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_tatap_muka + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Hadir<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_hadir + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Sakit<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_sakit + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Izin<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_izin + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Alpha<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%;">' +
                          msgJSON.total_alpha + '</td></tr>' +
                          '<tr><td style="width: 20%"><b>Total Nihil<b></td>' +
                          '<td style="width: 5%;">:</td><td style="width: 20%">' +
                          msgJSON.total_nihil + '</td></tr>' +
                          '</table><br>';

                        var att = msgJSON.attachment;
                        if (typeof msgJSON.attachment != 'undefined') {
                          if (att.length > 0) {
                            html += '<table id="table-attach" class="table table-bordered table-hovered">' +
                              '<thead><tr><th width="5%">No Urut</th><th width="10%">Tanggal Tatap Muka</th>' +
                              '<th width="45%">Standar Kompetensi/Kompetensi Dasar/Indikator</th><th width="40%">Keterangan</th></tr></thead>';

                            for (var i = 0; i < att.length; i++) {
                              html += '<tbody><tr><td>' + att[i].no + '</td><td>' + att[i].tgl_tatap_muka +
                                '</td><td>' + att[i].kompetensi + '</td>' +
                                '<td>' + att[i].keterangan + '</td></tr>';
                            }

                            html += '</table>';
                          }
                        }
                        $('#list-siswa').append(html);
                      }
                    }
                  });
                }
              }

              function export_data_reguler(jenis_download) {
                var a = 1;

                if (jenis_download === "cetak") {
                  a = 2;
                }
                var id_kelas = $('#kelas').val();
                var id_jenjang = $('#jenjang').val();
                var id_mapel = $('#mapel').val();
                var id_guru = $('#guru').val();
                var id_ta = $('#ta').val();
                var tgl_awal = $('#tanggal').val();
                var tgl_akhir = $('#tanggal2').val();

                var kelas = $('#kelas option:selected').text();
                var guru = $('#guru option:selected').text();
                var ta = $('#ta option:selected').text();
                var mapel = $('#mapel option:selected').text();
                var html = "";

                if (id_jenjang == "" || id_mapel == "" || id_guru == "" ||
                  id_ta == "") {
                  alert("Isikan data dengan lengkap...!");
                } else {

                  window.open("<?php echo site_url('adm/rekap_daftar_hadir/export_now') ?>" +
                    "?a=" + a + "&id_kelas=" + id_kelas + "&id_jenjang=" + id_jenjang +
                    "&id_mapel=" + id_mapel + "&id_guru=" + id_guru +
                    "&id_ta=" + id_ta +
                    "&tgl_awal=" + tgl_awal + "&tgl_akhir=" + tgl_akhir +
                    "&ta=" + ta + "&mapel=" + mapel +
                    "&kelas=" + kelas + "&guru=" + guru + "&jenis=reguler");
                }
              }

              function export_data_tambahan(jenis_download) {
                var a = 1;

                if (jenis_download === "cetak") {
                  a = 2;
                }
                var id_jenjang = $('#jenjang-tambahan').val();
                var id_mapel = $('#mapel-tambahan').val();
                var id_guru_tambahan = $('#guru-tambahan').val();
                var id_pembimbing1 = $('#pembimbing1').val();
                var id_pembimbing2 = $('#pembimbing2').val();
                var id_pembimbing3 = $('#pembimbing3').val();
                var id_ta = $('#ta-tambahan').val();
                var tgl_awal = $('#tanggal3').val();
                var tgl_akhir = $('#tanggal4').val();

                var guru_tambahan = $('#guru-tambahan option:selected').text();
                var pembimbing1 = $('#pembimbing1 option:selected').text();
                var pembimbing2 = $('#pembimbing2 option:selected').text();
                var pembimbing3 = $('#pembimbing3 option:selected').text();
                var ta = $('#ta-tambahan option:selected').text();
                var mapel = $('#mapel-tambahan option:selected').text();

                if (id_guru_tambahan == "") {
                  if (id_pembimbing1 != "") {
                    id_guru_tambahan = id_pembimbing1;
                    guru_tambahan = pembimbing1;
                  } else if (id_pembimbing2 != "") {
                    id_guru_tambahan = id_pembimbing2;
                    guru_tambahan = pembimbing2;
                  } else if (id_pembimbing3 != "") {
                    id_guru_tambahan = id_pembimbing3;
                    guru_tambahan = pembimbing3;
                  }
                }


                if (id_jenjang == "" || id_mapel == "" || id_ta == "") {
                  alert("Isikan data dengan lengkap...!");
                } else if (id_guru_tambahan == "") {
                  alert("Isikan data guru atau penbimbing 1 atau pembimbing 2 atau pembimbing 3...!");
                } else {
                  window.open("<?php echo site_url('adm/rekap_daftar_hadir/export_now') ?>" +
                    "?a=" + a + "&id_jenjang=" + id_jenjang +
                    "&id_mapel=" + id_mapel + "&id_guru=" + id_guru_tambahan +
                    "&id_ta=" + id_ta +
                    "&tgl_awal=" + tgl_awal + "&tgl_akhir=" + tgl_akhir +
                    "&ta=" + ta + "&mapel=" + mapel + "&guru=" + guru_tambahan + "&jenis=tambahan");
                }
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <!-- END -->
  </section>
</div>

<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>