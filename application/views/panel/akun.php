<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "view") {
    ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <?php
$level = $this->session->userdata('level');
    if ($level == 'super admin' or $level == 'administrator') {
        ?>
           <a href="adm/akun/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Tambah Data</button>
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>
          <?php }?>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>
              <th>Username</th>
              <th>Nama Lengkap</th>
              <th>Level</th>
              <th>Tgl.Daftar</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php
$no = 1;
    $dt_admin = $this->db->query("SELECT * FROM tabel_user");
    foreach ($dt_admin->result() as $row) {
        echo "
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_user'></td>
              <td>$no</td>
              <td>$row->username</td>
              <td>$row->nama</td>
              <td>$row->level</td>
              <td>$row->tgl_daftar</td>
              <td>";
        ?>
              <form method="post" action="adm/akun/process">
                <input type="hidden" name="id" value="<?php echo $row->id_user ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
              </form>
              </td>
            </tr>
          <?php
$no++;
    }
    ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    <?php
} elseif ($set == 'insert') {
    ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
           <a href="adm/akun">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/akun/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" name="nama" placeholder="Nama Lengkap" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Username" name="username" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Level</label>
                  <div class="col-sm-10">
                    <select name="level" class="form-control">
                      <option value="">Pilih</option>
                      <option>administrator</option>
                      <option>guru</option>
                      <option>non guru</option>
					            <option>siswa</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Avatar</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" name="avatar">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php
} elseif ($set == 'edit') {
    $r = $one_post->row();
    ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
           <a href="adm/akun">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/akun/process" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
                  <div class="col-sm-10">
                    <input type="hidden" value="<?php echo $r->id_user ?>" name="id">
                    <input type="text" value="<?php echo $r->nama ?>" class="form-control" id="inputEmail3" name="nama" placeholder="Nama Lengkap">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $r->username ?>" class="form-control" id="inputEmail3" placeholder="Username" name="username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputImei1" class="col-sm-2 control-label">Imei 1</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $r->imei_1 ?>" class="form-control" id="inputImei1" placeholder="Imei 1" name="imei1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputImei2" class="col-sm-2 control-label">Imei 2</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $r->imei_2 ?>" class="form-control" id="inputImei2" placeholder="Imei 2" name="imei2">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Level</label>
                  <div class="col-sm-10">
                    <select name="level" class="form-control">
                      <option value="<?php echo $r->level ?>"><?php echo $r->level ?></option>
                      <option>super admin</option>
                      <option>administrator</option>
                      <option>operator cabang</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Avatar</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" name="avatar">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" value="edit" name="s_process" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
      <?php }?>
  </section>
</div>


<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/akun/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>