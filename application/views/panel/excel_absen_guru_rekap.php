<style type="text/css">
.table {
    float:left;    
    margin-left:0px;
    border-collapse:collapse;
}
.table2 {
    float:left;    
    margin-left:5px;
    border-collapse:collapse;
}
</style>
<base href="<?php echo base_url(); ?>" />
<body onload="window.print()">
<?php 
$d = date('dms'); 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

// $ro = $dt_wali_kelas->row(); 
$tglnow = gmdate("d-m-Y", time()+60*60*7); 
$tglcantik = gmdate("d M Y", time()+60*60*7); 
$harinow = hari();   
// $tgl_awal = $_GET['tgl_awal'];
// $tgl_akhir = $_GET['tgl_akhir'];
$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>
<hr style="border-collapse:collapse;">
  <p align="center"><b>REKAPITULASI VALIDASI ABSEN GURU</b></p>
<hr style="border-collapse:collapse;">
<table border="0">
  <tr>
    <td><b>Hari/Tanggal</b></td>
    <td>: <?php echo $harinow.", ".$tglnow ?></td>
  </tr>
  <tr>
    <td><b>Tahun Ajaran</b></td>
    <td>: <?php echo $tahunajaran ?></td>
  </tr>
  <tr>
    <td><b>Jenjang</b></td>
    <td>: <?php echo $jenjang ?></td>
  </tr>
  <tr>
    <td><b>Guru</b></td>
    <td>: <?php echo $nama_guru ?></td>
  </tr>
  <tr>
    <td><b>Rekap</b></td>
    <td>: <?php echo $tgl." s/d ".$tgl_akhir ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
        <?php
          $rekaps = array();
          $sql = "SELECT id_guru FROM tabel_guru";
          if ($id_guru != "") 
            $sql .= " WHERE id_guru='$id_guru'";

          $guru = $this->db->query($sql);
          foreach( $guru->result() as $row1 ) {

            $tgl_mulai_param = explode('-', $tgl);
            $tgl_akhir_param = explode('-', $tgl_akhir);
            $args = array(
              'id_guru'     => $row1->id_guru,
              'id_jenjang'  => $id_jenjang,
              'id_ta'       => $id_ta,
              'tgl_mulai'   => $tgl_mulai_param[2] . '-' . $tgl_mulai_param[1] . '-' . $tgl_mulai_param[0],
              'tgl_akhir'   => $tgl_akhir_param[2] . '-' . $tgl_akhir_param[1] . '-' . $tgl_akhir_param[0]
            );
            $rekaps[] = get_one_rekap_absen_mengajar_guru($args);
          }
    //       debugji($rekaps, true);

      ?>
    <table border="1" width="100%" id="example2" class="table table-bordered table-hovered">
      <thead>
        <tr>
        <th>NAMA</th>
          <th>MATA PELAJARAN</th>
          <th>HADIR</th>
          <th>SAKIT</th>
          <th>IZIN</th>
          <th>NIHIL</th>  
          <th>TANPA KET.</th>                        
        </tr>
      </thead>
      <tbody>            
        <?php foreach ( $rekaps as $rekap ) : ?>         
          <tr>
            <td><?php echo $rekap->nama; ?></td>
            <td><?php echo $rekap->mapel; ?></td>
            <td align='center'><?php echo $rekap->hadir; ?></td>
            <td align='center'><?php echo $rekap->sakit; ?></td>
            <td align='center'><?php echo $rekap->izin; ?></td>
            <td align='center'><?php echo $rekap->nihil ?></td>
            <td align='center'><?php echo $rekap->tanpa_keterangan; ?></td>
          </tr>    
        <?php endforeach; ?>
      </tbody>
    </table>
<br><br><br>
<table border="0"  align="right">
  <?php
    $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
  ?>
  <tr>
    <td><br></td>
  </tr>
  <tr>
    <td colspan="7" align="right"><b><?php echo $sql->kota . ', ' . $tglcantik; ?></b></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b>Mengetahui, </b></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b>Kepala Sekolah, </b></td>
  </tr>
  <tr>
    <td><br><br><br></td>
  </tr>
  <tr>
    <td colspan="7" align="right"></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b><u><?php echo $sql->pimpinan ?></u></b></td>      
  </tr>
  <tr>
    <td colspan="7" align="right"><?php echo $sql->nik ?></td>    
  </tr>
</table>
</body>