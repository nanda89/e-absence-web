<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $judul1; ?>
            <small><?php echo $judul2; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
            <li class="active"><?php echo $judul1; ?></li>
        </ol>
    </section>
    <section class="content">

      <!-- VIEW -->
      <?php if($set == "view") : ?>
      
      <div class="box">
          <div class="box-header with-border">
              <!-- <h3 class="box-title">                          
                  <a href='adm/cek_absen_kehadiran_non_guru_rekap/export_now?a=1' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>                          
                  <a href='adm/cek_absen_kehadiran_non_guru_rekap/export_now?a=2' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>
              </h3> -->
              <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
          </div>
          <div class="box-body with-border">
              <form class="form-horizontal" action="adm/cek_absen_kehadiran_non_guru_rekap/filter" method="post" enctype="multipart/form-data">                                
                  <div class="form-group">
                      <?php          
                          $tgl1  = gmdate("d-m-Y", time()+60*60*7);
                          $hari  = hari();
                      ?>
                      <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                      <div class="col-sm-2">              
                          Dari :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-2">              
                          Sampai :<input type="text" class="form-control"  value="<?php echo $tgl1; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                      </div>
                      <div class="col-sm-2">
                          Jenis Absen :         
                          <select class="form-control" name="jenis">
                              <option>Semua</option>
                              <option>Terlambat</option>
                              <option>Tidak Absen</option>
                          </select>
                      </div>    
                      <div class="col-sm-3">
                          Non Guru : 
                          <select name="guru" class="form-control">
                              <?php
                                  $non_guru = $this->db->query("SELECT * FROM tabel_non_guru");
                                  $data .= "<option value=''>Pilih Non Guru</option>";
                                  foreach ($non_guru->result() as $row) {
                                      $data .= "<option value='$row->id_non_guru'>$row->nama</option>\n";
                                  }
                                  echo $data;
                              ?>
                          </select>
                      </div>
                      <div class="col-sm-3">
                          Jenjang : 
                          <select name="jenjang" class="form-control">
                              <?php
                                  // $data1 .= "<option value=''>Pilih Jenjang</option>";
                                  foreach ($jenjang->result() as $row) {
                                      $data1 .= "<option value='$row->id_jenjang'>$row->jenjang</option>\n";
                                  }
                                  echo $data1;
                              ?>
                          </select>
                      </div>
                      <div class="col-sm-4" style="margin-top:10px;">
                          <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
                          <button type="reset" class="btn bg-s btn-flat margin"><i class="fa fa-refresh"></i> Reset</button>              
                      </div>          
                  </div>
              </form>  
          </div> 
          
      </div>

      <!-- ABSEN GURU FILTER -->
      <?php elseif($set == "filter") : ?>
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href='adm/cek_absen_kehadiran_non_guru_rekap' class="btn bg-maroon btn-flat"><i class="fa fa-chevron-left"></i> Kembali</a>        
                <a href='adm/cek_absen_kehadiran_non_guru_rekap/export_filter?a=1&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>&id_jenjang=<?php echo $id_jenjang ?>&id_ta=<?php echo $id_ta ?>' onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-warning btn-flat margin"><i class="fa fa-file-excel-o"></i> Export</a>        
                <a href='adm/cek_absen_kehadiran_non_guru_rekap/export_filter?a=2&tgl=<?php echo $tgl ?>&tgl_akhir=<?php echo $tgl_akhir;?>&jenis=<?php echo $jenis ?>&id_guru=<?php echo $id_guru ?>&id_jenjang=<?php echo $id_jenjang ?>&id_ta=<?php echo $id_ta ?>' target="_blank" onclick="return confirm('Anda yakin ingin melanjutkan?')" class="btn btn-primary btn-flat margin"><i class="fa fa-print"></i> Cetak</a>        
            </h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body with-border">
            <form class="form-horizontal" action="adm/cek_absen_kehadiran_non_guru_rekap/filter" method="get" enctype="multipart/form-data">                                
                <div class="form-group">
                    <?php
                        $tgl1  = date("l", strtotime($tgl));
                        $hari  = cek_hari($tgl1);
                    ?>
                    <!-- <label class="col-sm-1 control-label"><?php echo $hari; ?></label>             -->
                    <div class="col-sm-2">              
                        Dari :<input type="text" class="form-control" disabled value="<?php echo $tgl; ?>" id="tanggal" name="tgl" placeholder="Tanggal">
                    </div>
                    <div class="col-sm-2">              
                        Sampai :<input type="text" class="form-control" disabled value="<?php echo $tgl_akhir; ?>" id="tanggal2" name="tgl2" placeholder="Tanggal">
                    </div>
                    <div class="col-sm-2">   
                        Jenis Absen :                    
                        <select class="form-control" disabled name="jenis">
                            <option><?php echo $jenis ?></option>                
                        </select>
                    </div>  
                    <div class="col-sm-3">
                        Guru : 
                        <select name="guru" disabled class="form-control">
                            <option value="<?php echo $id_guru ?>"><?php echo $nama_guru ?></option>
                        </select>
                    </div> 
                    <div class="col-sm-3">
                        Jenjang : 
                        <select name="jenjang" disabled class="form-control">
                            <option value="<?php echo $id_jenjang ?>"><?php echo $jenjang ?></option>
                        </select>
                    </div> 

                </div>
            </form>  
        </div> 
        <div class="box-body">

            <?php $rekaps=array(); 

            $sql = "SELECT * FROM tabel_non_guru";
            if ($id_guru != "") 
              $sql .= " WHERE id_non_guru = '$id_guru'";
            $guru = $this->db->query($sql);

            $tmp_tgl_mulai = explode('-', $tgl);
            $tmp_tgl_akhir = explode('-', $tgl_akhir);
            $tgl_mulai = $tmp_tgl_mulai[2] . '-' . $tmp_tgl_mulai[1] . '-' . $tmp_tgl_mulai[0];
            $tgl_akhir = $tmp_tgl_akhir[2] . '-' . $tmp_tgl_akhir[1] . '-' . $tmp_tgl_akhir[0];
            ?>
            <?php foreach ($guru->result() as $row1) :
            $args = array(
              'id_non_guru'   => $row1->id_non_guru,
              'tgl_mulai'     => $tgl_mulai,
              'tgl_akhir'     => $tgl_akhir
            );
            $rekap = get_one_rekap_absen_kehadiran_non_guru($args);
            $rekap['nama'] = $row1->nama;
            $rekap['jabatan'] = $row1->jabatan;
            $rekaps[] = $rekap;
            endforeach; ?>
          
            <table id="example2" class="table table-bordered table-hovered">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Val. Datang</th>
                        <th>Nihil</th>
                        <th>Val. Pulang</th>
                        <th>Nihil</th>
                        <th>Sakit</th>
                        <th>Izin</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($rekaps as $rekap) : ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rekap['nama']; ?></td>
                    <td><?php echo $rekap['jabatan']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['datang']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['nihil_dtg']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['pulang']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['nihil_plg']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['sakit']; ?></td>
                    <td align='center'><?php echo $rekap['absensi']['izin']; ?></td>
                  </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->

        <?php endif; ?>
    </section>
</div>
