
<table class="table table-bordered responsive-utilities jambo_table bulk_action" id="jasa">
    <thead>
        <tr class="headings">                                                
            <th width="5%">No </th>            
            <th>Jenis Beasiswa </th>
            <th>Sumber </th>                                                                                                                                
            <th>Tahun Mulai </th>                                                                    
            <th>Tahun Selesai </th>                                                                                
            <th width="7%">Aksi </th>                                                                                                                
        </tr>
    </thead>
    <tbody> 
    <?php 
      $no=1; 
      foreach($dt_beasiswa->result() as $row) {       
      echo "          
        <tr>
          <td>$no</td>
          <td>$row->jenis_beasiswa</td>
          <td>$row->sumber</td>
          <td>$row->tahun_mulai</td>   
          <td>$row->tahun_selesai</td>             
          <td>"; ?>
            <button title="Hapus Data"
                class="btn btn-sm btn-danger fa fa-trash-o" type="button" 
                onClick="hapus_beasiswa('<?php echo $row->id_beasiswa; ?>','<?php echo $row->id_siswa; ?>')"></button>
          </td>
        </tr>
        <?php
        $no++;
        }
    ?>   
    </tbody>
</table>
