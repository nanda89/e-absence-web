<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "insert") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/non_guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">

        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/non_guru/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="nip_id" placeholder="NIP" name="nip" autocomplete="off">
                  </div>

                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="Nama Lengkap" name="nama" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Alias</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="inputEmail3" placeholder="Kode Penamaan" name="alias" autocomplete="off">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.KTP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="No KTP" name="no_ktp" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin *</label>
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;&nbsp;&nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="">Pilih</option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir" autocomplete="off">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Unit Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Unit Kerja" name="unit_kerja" autocomplete="off">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl.Mulai Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Tgl.Mulai Kerja" id="tanggal2" name="tmt" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="ijazah">
                      <option value="">Pilih</option>
                      <option value="SD">SD</option>
                      <option value="SLTP">SLTP</option>
                      <option value="SLTA">SLTA</option>
                      <option value="D1">D1</option>
                      <option value="D2">D2</option>
                      <option value="D3">D3</option>
                      <option value="D4">D4</option>
                      <option value="S1">S1</option>
                      <option value="S2">S2</option>
                      <option value="S3">S3</option>
                    </select>
                  </div>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama Sekolah / PT / Universitas" name="pend_terakhir" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Hp *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="No. HP" name="no_hp" autocomplete="off">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" name="email" placeholder="Alamat Email" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" placeholder="Pas Foto" name="gambar">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Jabatan" name="jabatan" autocomplete="off">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Ijinkan akses aplikasi</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="login_access" onchange="access(this.value)" id="login_access">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                </div>
                <div class="form-group hidden" id="form-user">
                  <label for="inputEmail3" class="col-sm-2 control-label">password</label>
                  <div class="col-sm-4">
                    <input type="password" class="form-control pull-right" name="password" placeholder="password" maxlength="15" autocomplete="off">
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "edit") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/non_guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/non_guru/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_non_guru ?>" name="id">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="nik_id" value="<?php echo $row->nip ?>" placeholder="NIP" name="nip">
                  </div>

                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->nama ?>" id="inputEmail3" required placeholder="Nama Lengkap" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Alias</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->alias ?>" required id="inputEmail3" placeholder="Kode Penamaan" name="alias">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.KTP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->no_ktp ?>" id="inputEmail3" required placeholder="No KTP" name="no_ktp">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin *</label>
                  <div class="col-sm-4">
                    <?php
$j = $row->jenis_kelamin;
    if ($j == 'perempuan') {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red"> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red" checked> Perempuan
                    </label>
                    <?php
} elseif ($j == 'laki-laki') {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                    <?php
}
    ?>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="<?php echo $row->agama ?>"><?php echo $row->agama ?></option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tempat_lahir ?>" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tgl_lahir ?>" class="form-control pull-right" id="tanggal" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Unit Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->unit_kerja ?>" class="form-control" id="inputEmail3" placeholder="Unit Kerja" name="unit_kerja">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl.Mulai Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tmt ?>" class="form-control" placeholder="Tgl.Mulai Kerja" id="tanggal2" name="tmt">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="ijazah">
                      <option value="<?php echo $row->ijazah ?>"><?php echo $row->ijazah ?></option>
                      <option value="SD">SD</option>
                      <option value="SLTP">SLTP</option>
                      <option value="SLTA">SLTA</option>
                      <option value="D1">D1</option>
                      <option value="D2">D2</option>
                      <option value="D3">D3</option>
                      <option value="D4">D4</option>
                      <option value="S1">S1</option>
                      <option value="S2">S2</option>
                      <option value="S3">S3</option>
                    </select>
                  </div>
                  <div class="col-sm-8">
                    <input type="text" value="<?php echo $row->pend_terakhir ?>" class="form-control" id="inputEmail3" placeholder="Nama Sekolah / PT / Universitas" name="pend_terakhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Hp *</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->no_hp ?>" class="form-control" id="inputEmail3" required placeholder="No. HP" name="no_hp">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->email ?>" class="form-control pull-right" name="email" placeholder="Alamat Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $row->alamat ?>" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" placeholder="Pas Foto" name="gambar">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $row->jabatan ?>" class="form-control" id="inputEmail3" placeholder="Jabatan" name="jabatan">
                  </div>
                </div>


              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "detail") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/non_guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/non_guru/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_non_guru; ?>" name="id">
              <div class="box-body">
                <table width="100%" border="0">
                  <tr>
                    <td width="20%">
                      <label class="control-label">NIP</label>
                    </td>
                    <td>
                      <input type="hidden" id="nik_id" value="<?php echo $row->nip; ?>">
                      <?php echo $row->nip; ?></td>
                    <td width="20%" align="right" rowspan="10">
                      <img src="assets/panel/images/<?php echo $row->gambar; ?>" width="200px">
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <label class="control-label">No.KTP</label>
                    </td>
                    <td><?php echo $row->no_ktp; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Jenis Kelamin</label>
                    </td>
                    <td><?php echo $row->jenis_kelamin; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tempat, Tgl.Lahir</label>
                    </td>
                    <td><?php echo $row->tempat_lahir . " , " . $row->tgl_lahir; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Agama</label>
                    </td>
                    <td><?php echo $row->agama; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Unit Kerja</label>
                    </td>
                    <td><?php echo $row->unit_kerja; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tgl.Mulai Kerja</label>
                    </td>
                    <td><?php echo $row->tmt; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Pendidikan Terakhir</label>
                    </td>
                    <td><?php echo $row->ijazah . " - " . $row->pend_terakhir; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">No.HP</label>
                    </td>
                    <td><?php echo $row->no_hp; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Email</label>
                    </td>
                    <td><?php echo $row->email; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Alamat Lengkap</label>
                    </td>
                    <td><?php echo $row->alamat; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Jabatan</label>
                    </td>
                    <td><?php echo $row->jabatan; ?></td>
                  </tr>


                </table>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="ubah" class="btn btn-info">Edit</button>
                <a href="adm/non_guru">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "view") {
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">

          <a href="adm/non_guru/add">
            <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
            <!--button onclick="return confirm('Anda Yakin Ingin Menghapus Semua Data yang Ditandai ?')" type="button" class="btn bg-red btn-flat margin"><i class="fa fa-trash-o"></i> Hapus Banyak</button-->
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>

        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div id="alert-info" class="alert alert-danger alert-dismissable hidden">
                <strong>data berhasil dihapus</strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>

              <th>NIP</th>
              <th>Nama Lengkap/Alias</th>
              <th>No.KTP</th>
              <th>No.Hp</th>
              <th>Pass. sync</th>
              <th width="16%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php
$no = 1;
    foreach ($dt_non_guru->result() as $row) {
        echo "
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_non_guru'></td>
              <td>$no</td>
              <td>$row->nip</td>
              <td>$row->nama / $row->alias</td>
              <td>$row->no_ktp</td>
              <td>$row->no_hp</td>
              <td>$row->pass_sync</td>
              <td>";
        ?>
                <form method="post" action="adm/non_guru/process">
                <input type="hidden" name="id" value="<?php echo $row->id_non_guru ?>" />
                <input type="hidden" name="nip" value="<?php echo $row->nip ?>" />
                <input type="hidden" name="nama" value="<?php echo $row->nama ?>" />
                <input type="hidden" name="jk" value="<?php echo $row->jenis_kelamin ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-danger btn-flat btn-sm'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-edit'></i></button>
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-info btn-flat btn-sm'><i class='fa fa-eye'></i></button>
                <?php
$id = $row->nip;
        $c = $this->m_admin->cek_user($id);
        if ($c->num_rows() > 0) {
            ?>
                <button data-toggle='tooltip' title='Set Akun Default' name="s_process" type="submit" value="set_default" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-key'></i></button>
                <?php
} else {
            ?>
                <button data-toggle='tooltip' title='Set Akun' name="s_process" type="submit" value="set_akun" class='btn btn-warning btn-flat btn-sm'><i class='fa fa-key'></i></button>
                <?php
}
        ?>
              </form>
              </td>
            </tr>
          <?php
$no++;
    }
    ?>
          </tbody>
        </table>

      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>




<div class="modal fade" id="Amodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cari Data Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover" id="example2">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Kategori</th>
            <th>Mapel</th>
            <th>KKM</th>
            <th width="5%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/non_guru/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            window . location . reload();
          },
          error: function (jqXHR, textStatus, errorThrown){
            window . location . reload();

          }
        });
      }
    }else{
      alert('no data selected');
  }
}
function access(value){
  if(value == "1"){
    showFormLogin();
  }else{
    hideFormLogin();
  }
}

function showFormLogin(){
  $("#form-user").removeClass("hidden");
}
function hideFormLogin(){
  $("#form-user").addClass("hidden");
}
</script>