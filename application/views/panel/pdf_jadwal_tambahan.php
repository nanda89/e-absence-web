<?php date_default_timezone_set("Asia/Jakarta"); ?>

<!DOCTYPE html>
<html>

<head>
  <title>Cetak</title>
  <style type="text/css">
    body {
      width: 8.5in;
      height: 13.0in;
      font-family: arial;
      margin: 0mm 0mm 0mm 0mm;
    }

    @page {
      size: 8.5in 13.0in
    }

    .table {
      float: left;
      margin-left: 0px;
      border-collapse: collapse;
    }

    .table2 {
      float: left;
      margin-left: 5px;
      border-collapse: collapse;
    }

    #tabel-absen thead tr th {
      font-size: 14px;
      padding: 7px;
    }

    #tabel-absen tbody tr td {
      font-size: 12px;
    }

    @media print {
      .kertas {
        page-break-after: always;
      }
    }
  </style>
</head>

<body onload="window.print()">
  <?php
  function helpIndoMonth($num)
  {
    $monthArray = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    if (array_key_exists($num, $monthArray)) {
      return $monthArray[$num];
    } else {
      return 'Undefined';
    }
  }

  $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
  ?>

  <?php

  $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
  ?>



  <?php if (file_exists('assets/panel/images/' . $sql->kop_surat)) { ?>
    <img src="<?php echo base_url() ?>assets/panel/images/<?php echo $sql->kop_surat ?>" width="800" height="100">
  <?php } ?>



  <table>
    <!-- 
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr><td colspan="7">&nbsp;</td></tr>
       <tr>
         <td colspan="7" style="text-align: center;"><h2>LAPORAN VALIDASI ABSENSI GURU</h2></td>
       </tr>
       <tr>
         <td colspan="7" style="text-align: center;"><b>Hari, Tanggal : <?php echo $hari . ", " . $tgl ?> </b></td>
       </tr>
       <tr>
         <td colspan="7" style="text-align: center;"><b>Tahun Ajaran : <u style="text-decoration: none;"><?php echo $tahun_ajaran ?></u></td>
       </tr>
       <tr><td colspan="7">&nbsp;</td></tr>
     </table>
     <center> -->

    <center>
      <table style="width: 97%;text-align: center;border-top: #000 2px solid;border-bottom: #000 2px solid;">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="7" style="text-align: center;">
            <h2>JADWAL MENGAJAR</h2>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>

    </center>
    <table>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>
    <!-- <b style="font-size: 12px;font-weight: normal;">
     <u style="text-decoration: none;font-size: 15px;" ><b>Tahun </b> : <?php echo $tahun ?></u><br>
     <u style="text-decoration: none;font-size: 15px;" ><b>Jenjang </b> : <?php echo $jenjang ?></u><br>
     <u style="text-decoration: none;font-size: 15px;" ><b>Kelas </b> : <?php echo $kelas ?></u><br>
     <u style="text-decoration: none;font-size: 15px;" ><b>Guru </b> : <?php echo $guru ?></u><br>
     
     </b> -->

    <center>


      <table id="example2" class="table table-bordered table-hovered" border="1">
        <thead>
          <tr>
            <th width="2%">No</th>
            <th width="20%">Hari</th>
            <th width="15%">Jam</th>
            <th>Mapel</th>
            <th>Kelas</th>
            <th>Guru</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          foreach ($dt_jadwal->result() as $row) {
            echo "          
          <tr>
            <td>$no</td>
            <td>$row->hari (pekan ke-$row->minggu_ke)</td>
            <td>$row->jam_awal - $row->jam_akhir</td>
            <td>$row->mapel</td>
            <td>$row->nama_kelas</td>
            <td>$row->nama ($row->alias)</td>
          </tr>"; ?>
          <?php
            $no++;
          }
          ?>
        </tbody>
      </table>


      <table>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>

      <table id="example2" class="table table-bordered table-hovered" border="0" width="100%" style="margin-top: 12px;">
        <tfoot>
          <tr>
            <td width="70%"></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td><?php echo date("d F Y") ?></td>
          </tr>
          <tr>
            <?php $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row(); ?>
            <td></td>
            <td>Kepala <?php echo $re->logo_besar ?></td>
          </tr>
          <tr>
            <?php $re  = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row(); ?>
            <td><br><br><br><br><br><br></td>
            <td valign="bottom"><u style="text-decoration-skip-ink: none;"><?php echo $re->pimpinan ?></u> <br> <?php echo $re->nik ?></td>
          </tr>
        </tfoot>
      </table>

    </center>