<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "view") {
    ?>
    <div class="row">
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" data-toggle="modal"
                onclick="add_jen()">
                <i class="fa fa-plus"></i> Tambah Data Jenjang</button>
              </a>
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
            <table id="" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Jenjang</th>
                  <th>Jadwal</th>
                  <th width="33%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
$no = 1;
    foreach ($dt_jenjang->result() as $row) {
        $id = $row->id_jenjang;
        echo "
                <tr>
                  <td>$no</td>
                  <td>$row->jenjang</td>
                  <td>$row->jam_masuk s/d $row->jam_pulang</td>
                  <td>";
        ?>
                  <form method="post" action="adm/jenjang/process">
                    <input type="hidden" name="id" value="<?php echo $row->id_jenjang ?>" />
                    <button onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                      <a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-primary btn-flat" data-toggle="tooltip modal"
                    onclick="edit_jen(<?php echo $id ?>)"><i class='fa fa-edit'></i></a>
                  </td>
                  </form>
                </tr>
              <?php
$no++;
    }
    ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>

      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" data-toggle="modal"
                onclick="add_kelas()">
                <i class="fa fa-plus"></i> Tambah Data Kelas</button>
              </a>
              <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" data-toggle="modal"
                onclick="copy_kelas()">
                <i class="fa fa-copy"></i> Duplikasi Data Kelas</button>
              </a>
              <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>

            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <?php
if (isset($_SESSION['pesan2']) && $_SESSION['pesan2'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe2'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan2'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan2'] = '';

    ?>
            <table id="example3" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%"><input type="checkbox" id="check-all"></th>
                  <th width="5%">No</th>
                  <th>Jenjang</th>
                  <th>Kelas</th>
                  <th>Wali Kelas</th>
                  <th>Tahun Ajaran</th>
                  <th width="16%">Aksi</th>
                </tr>
              </thead>
              <tbody>
              <?php
$no = 1;
    foreach ($dt_kelas->result() as $row) {
        $id = $row->id_kelas;
        echo "
                <tr>
                  <td><input type='checkbox' class='data-check' value='$row->id_kelas'></td>
                  <td>$no</td>
                  <td>$row->jenjang</td>
                  <td>$row->kelas</td>
                  <td>$row->nama ($row->alias)</td>
                  <td>$row->tahun_ajaran</td>
                  <td>";
        ?>
                  <form method="post" action="adm/jenjang/process_kelas ">
                    <input type="hidden" name="id" value="<?php echo $row->id_kelas ?>" />
                    <button onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                      <a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-primary btn-flat" data-toggle="tooltip modal"
                    onclick="edit_kelas(<?php echo $id ?>)"><i class='fa fa-edit'></i></a>
                  </td>
                  </form>
                  </td>
                </tr>
              <?php
$no++;
    }
    ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>

    </div>

    <?php
}
?>
  </section>
</div>


<div class="modal fade" id="tambah_jenjang">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Jenjang</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/jenjang/save" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jenjang</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Jenjang" name="jenjang" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jam Masuk</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control timepicker" id="inputEmail3" placeholder="Jam Masuk" name="jam_masuk" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jam Pulang</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control timepicker" id="inputEmail3" autofocus placeholder="Jam Pulang" name="jam_pulang" required>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
              <a href="adm/jenjang">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_jenjang">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Data Jenjang</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/jenjang/process" method="post" enctype="multipart/form-data">
            <input type="hidden" class="form-control" id="inputEmail3" name="id">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jenjang</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Jenjang" name="jenjang" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jam Masuk</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputEmail3" placeholder="Jam Masuk" name="jam_masuk" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Jam Pulang</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Jam Pulang" name="jam_pulang" required>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
              <a href="adm/jenjang">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="copy_kelas">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelcopy">Tambah Data Kelas</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/jenjang/duplicate_kelas" method="post" enctype="multipart/form-data">
            <div class="box-body">
            <div class="callout callout-info">
              <h4>Informasi!</h4>
              <div class="box-body">
              <ol>
                <li>Mode ini adalah mode tambah kelas dengan cara cepat dengan menduplikasi data sebelumnya</li>
                <li>Pastikan anda sudah membuat <span class="label label-warning">tahun ajaran baru</span></li>
                <li>Pilih data kelas dari tahun & jenjang <code>sumber</code></li>
                <li>pilih data tahun & jenjang <code>tujuan</code></li>
                <li>klik <span class="btn btn-danger btn-xs"> Duplikat Data</span></li>
                <li>Data kelas akan secara otomatis terduplikasi beserta <span class="label label-primary">wali kelas</span>nya</li>
                <li>Tambahkan <span class="label label-primary">murid untuk setiap kelas</span> untuk data yang baru dibuat</li>
              </ol>
            </div>
            </div>



              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Tahun Ajaran Sumber</label>
                <div class="col-sm-8">
                  <select name="id_ta_sumber" class="form-control">
                    <?php
foreach ($dt_tahun_ajaran->result() as $dt_tahun_ajarancopy) {
    echo "
                    <option value='$dt_tahun_ajarancopy->id_ta'>$dt_tahun_ajarancopy->tahun_ajaran</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Jenjang Sumber</label>
                <div class="col-sm-8">
                  <select name="id_jenjang_sumber" class="form-control">
<?php
foreach ($dt_jenjang->result() as $dt_jenjangcopy) {
    echo "
                    <option value='$dt_jenjangcopy->id_jenjang'>$dt_jenjangcopy->jenjang</option>";
}?>

                  </select>
                </div>
              </div>
              <hr>
               <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Tahun Ajaran Tujuan</label>
                <div class="col-sm-8">
                  <select name="id_ta_tujuan" class="form-control">
                    <?php
foreach ($dt_tahun_ajaran->result() as $dt_tahun_ajarancopy_tujuan) {
    echo "
                    <option value='$dt_tahun_ajarancopy_tujuan->id_ta'>$dt_tahun_ajarancopy_tujuan->tahun_ajaran</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Jenjang Tujuan</label>
                <div class="col-sm-8">
                  <select name="id_jenjang_tujuan" class="form-control">
<?php
foreach ($dt_jenjang->result() as $dt_jenjangcopy_tujuan) {
    echo "
                    <option value='$dt_jenjangcopy_tujuan->id_jenjang'>$dt_jenjangcopy_tujuan->jenjang</option>";
}?>

                  </select>
                </div>
              </div>

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="save" value="save" class="btn btn-info">Duplikat Data</button>
              <a href="adm/jenjang">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="tambah_kelas">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Kelas</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/jenjang/save_kelas" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Kelas" name="kelas" autocomplete="off" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Tahun Ajaran</label>
                <div class="col-sm-10">
                  <select name="id_ta" class="form-control">
                    <?php
foreach ($dt_tahun_ajaran->result() as $dt_tahun_ajaran) {
    echo "
                    <option value='$dt_tahun_ajaran->id_ta'>$dt_tahun_ajaran->tahun_ajaran</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-10">
                  <select name="id_jenjang" class="form-control">
                    <?php
foreach ($dt_jenjang->result() as $dt_jenjang) {
    echo "
                    <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Wali Kelas</label>
                <div class="col-sm-10">
                  <select name="id_guru" class="form-control">
                    <option value=""></option>
                    <?php
foreach ($dt_guru->result() as $dt_guru) {
    echo "
                    <option value='$dt_guru->id_guru'>$dt_guru->nama ($dt_guru->alias)</option>";
}?>
                  </select>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
              <a href="adm/jenjang">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modal_kelas">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Data Kelas</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="adm/jenjang/process_kelas" method="post" enctype="multipart/form-data">
            <input type="hidden" class="form-control" id="inputEmail3" name="id_kelas">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" autofocus placeholder="Kelas" name="kelas" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-10">
                  <select name="id_jenjang" class="form-control">
                    <?php
foreach ($dt_jen->result() as $dt_jen) {
    echo "
                    <option value='$dt_jen->id_jenjang'>$dt_jen->jenjang</option>";
}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Wali Kelas</label>
                <div class="col-sm-10">
                  <select name="id_guru" id="id_guru" class="form-control">
                    <?php
foreach ($dt_g->result() as $dt_g) {
    echo "
                    <option value='$dt_g->id_guru'>$dt_g->nama ($dt_g->alias)</option>";
}?>
                  </select>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="s_process" value="edit" class="btn btn-info">Save</button>
              <a href="adm/jenjang">
                <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Cancel</button>
              </a>
            </div><!-- /.box-footer -->
          </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function edit_jen(id){
  //Ajax Load data from ajax
  $.ajax({
      url : "<?php echo site_url('adm/jenjang/cari') ?>",
      type:"POST",
      data:"id="+id,
      success: function(msg)
      {
          data=msg.split("|");
          $('[name="id"]').val(data[0]);
          $('[name="jenjang"]').val(data[1]);
          $('[name="jam_masuk"]').val(data[2]);
          $('[name="jam_pulang"]').val(data[3]);
          $('[name="jenjang"]').autofocus;
          $('#modal_jenjang').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit Jenjang'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}
function edit_kelas(id){
  //Ajax Load data from ajax
  $.ajax({
      url : "<?php echo site_url('adm/jenjang/cari_kelas') ?>",
      type:"POST",
      data:"id="+id,
      success: function(msg)
      {

          data=msg.split("|");
          $('[name="id_kelas"]').val(data[0]);
          $('[name="kelas"]').val(data[1]);
          $('[name="id_jenjang"]').val(data[4]).change();
          $('#id_guru').val(data[5]).change();

          console . log(data[2]);

          $('[name="kelas"]').autofocus;
          $('#modal_kelas').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit Kelas'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}
function add_jen(){
  $('[name="id"]').val('');
  $('[name="jenjang"]').val('');
   $('[name="jam_masuk"]').val('');
    $('[name="jam_pulang"]').val('');
  $('[name="jenjang"]').autofocus;
  $('#tambah_jenjang').modal('show'); // show bootstrap modal when complete loaded
  $('.modal-title').text('Tambah Jenjang'); // Set title to Bootstrap modal title
}
function add_kelas(){
  $('[name="id_kelas"]').val('');
  $('[name="kelas"]').autofocus;
  $('[name="jenjang"]').val('');
  $('[name="wali_kelas"]').val('');
  $('#tambah_kelas').modal('show'); // show bootstrap modal when complete loaded
  $('.modal-title').text('Tambah Kelas'); // Set title to Bootstrap modal title
}
function copy_kelas(){
  $('#copy_kelas').modal('show'); // show bootstrap modal when complete loaded
  $('.modal-title').text('Duplikasi Data Kelas'); // Set title to Bootstrap modal title
}
</script>



<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/jenjang/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            window . location . reload();

          },
          error: function (jqXHR, textStatus, errorThrown){
            // alert('Error deleting data');
            window . location . reload();

          }
        });
      }
    }else{
      alert('no data selected');
  }
}
$('.timepicker').timepicker({
  template: 'modal'
});

</script>