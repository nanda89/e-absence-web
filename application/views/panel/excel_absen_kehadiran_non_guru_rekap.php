
<style type="text/css">
.table {
    float:left;    
    margin-left:0px;
    border-collapse:collapse;
}
.table2 {
    float:left;    
    margin-left:5px;
    border-collapse:collapse;
}
</style>
<base href="<?php echo base_url(); ?>" />
<body onload="window.print()">
<?php 

 $d = date('dms'); 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_absen_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

// $ro = $dt_wali_kelas->row(); 
$tglnow = gmdate("d-m-Y", time()+60*60*7); 
$tglcantik = gmdate("d M Y", time()+60*60*7); 
$harinow = hari();   
// $tgl_awal = $_GET['tgl_awal'];
// $tgl_akhir = $_GET['tgl_akhir'];
$sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
?>
<hr style="border-collapse:collapse;">
  <p align="center"><b>REKAPITULASI VALIDASI ABSEN NON GURU</b></p>
<hr style="border-collapse:collapse;">
<table border="0">
  <tr>
    <td><b>Hari/Tanggal</b></td>
    <td>: <?php echo $harinow.", ".$tglnow ?></td>
  </tr>
  <tr>
    <td><b>Jenjang</b></td>
    <td>: <?php echo $jenjang ?></td>
  </tr>
  <?php if( $nama_guru ) : ?>
  <tr>
    <td><b>Guru</b></td>
    <td>: <?php echo $nama_guru ?></td>
  </tr>
  <?php endif; ?>
  <tr>
    <td><b>Rekap</b></td>
    <td>: <?php echo $tgl." s/d ".$tgl_akhir ?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<?php $rekaps=array(); 

$sql = "SELECT * FROM tabel_non_guru";
if ($id_guru != "") 
  $sql .= " WHERE id_guru = '$id_guru'";
$guru = $this->db->query($sql);

$tmp_tgl_mulai = explode('-', $tgl);
$tmp_tgl_akhir = explode('-', $tgl_akhir);
$tgl_mulai = $tmp_tgl_mulai[2] . '-' . $tmp_tgl_mulai[1] . '-' . $tmp_tgl_mulai[0];
$tgl_akhir = $tmp_tgl_akhir[2] . '-' . $tmp_tgl_akhir[1] . '-' . $tmp_tgl_akhir[0];
?>
<?php foreach ($guru->result() as $row1) :
$args = array(
  'id_non_guru' => $row1->id_non_guru,
  'tgl_mulai'   => $tgl_mulai,
  'tgl_akhir'   => $tgl_akhir
);
$rekap = get_one_rekap_absen_kehadiran_non_guru($args);
$rekap['nama'] = $row1->nama;
$rekap['jabatan'] = $row1->jabatan;
$rekaps[] = $rekap;
endforeach; ?>
<table border="1" width="100%" id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th>NAMA</th>
      <th>JABATAN</th>
      <th>VAL. DATANG</th>
      <th>NIHIL</th>
      <th>VAL. PULANG</th>
      <th>NIHIL</th>
      <th>SAKIT</th>
      <th>IZIN</th>
    </tr>
  </thead>
  <tbody>            
    <?php foreach ($rekaps as $rekap) : ?>
    <tr>
      <td><?php echo $rekap['nama']; ?></td>
      <td><?php echo $rekap['jabatan']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['datang']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['nihil_dtg']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['pulang']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['nihil_plg']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['sakit']; ?></td>
      <td align='center'><?php echo $rekap['absensi']['izin']; ?></td>
      </tr>
    <?php endforeach; ?>         
  </tbody>
</table>
<br><br><br>
<table border="0"  align="right">
  <?php
    $sql = $this->db->query("SELECT * FROM tabel_setting WHERE id_setting = 1")->row();
  ?>
  <tr>
    <td><br></td>
  </tr>
  <tr>
    <td colspan="7" align="right"><b><?php echo $sql->kota . ', ' . $tglcantik; ?></b></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b>Mengetahui, </b></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b>Kepala Sekolah, </b></td>
  </tr>
  <tr>
    <td><br><br><br></td>
  </tr>
  <tr>
    <td colspan="7" align="right"></td>    
  </tr>
  <tr>
    <td colspan="7" align="right"><b><u><?php echo $sql->pimpinan ?></u></b></td>      
  </tr>
  <tr>
    <td colspan="7" align="right"><?php echo $sql->nik ?></td>    
  </tr>
</table>