
<table id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="2%">No</th>
      <th width="10%">Hari</th>
      <th width="15%">Jam</th>
      <th>Mapel</th>
      <th>Guru</th>      
      <th width="5%">Aksi</th>
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1; 
  foreach($dt_jadwal->result() as $row) {       
    $hari = substr_replace($row->hari,"",0,2);
    if($row->status == 'masuk'){
      $status = "<span class='label label-primary pull-right'>$row->status</span>";
    }elseif($row->status == 'pulang'){
      $status = "<span class='label label-danger pull-right'>$row->status</span>";      
    }else{
      $status = "";
    }
  echo "          
    <tr>
      <td>$no</td>
      <td>$hari</td>
      <td>$row->jam_awal - $row->jam_akhir $status</td>
      <td>$row->mapel (kelas $row->kelas)</td>
      <td>$row->nama ($row->alias)</td>
      <td>"; ?>
        <button title="Hapus Data"
            class="btn btn-sm btn-danger fa fa-trash-o btn-flat" type="button" 
            onClick="hapus_jadwal('<?php echo $row->id_jadwal; ?>','<?php echo $row->id_ta; ?>','<?php echo $row->id_jenjang; ?>','<?php echo $row->id_kelas; ?>')"></button>
      </td>
    </tr>
  <?php
  $no++;
  }
  ?>
  </tbody>
</table>

