<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">    
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">          
            <a href="adm/tunggakan">
                <button onclick="back()" class="btn bg-maroon btn-flat margin"><i class="fa fa-chevron-left"></i> Kembali</button>          
            </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="box-body with-border">
        </div>
        <?php    
            if($dt_tu->num_rows()>0){
                $r = $dt_tu->row();
                $id_jenjang = $r->id_jenjang;   
        ?>
        <b>Riwayat Pembayaran</b>
        <table class="table table-bordered responsive-utilities jambo_table bulk_action" id="example2">
            <thead>
                <tr class="headings">                                                
                    <th width="5%">No </th>
                    <th>No.Induk</th>            
                    <th>Nama Siswa</th>
                    <th>Kelas</th>    
                    <?php        
                    $j = $_GET['id_jenjang'];
                    $c = $this->m_biaya->get_biaya($j);
                    $jum_kol = $c->num_rows();
                    foreach ($c->result() as $row){
                        echo"
                        <th>$row->biaya</th>
                        ";
                    }
                    ?>
                    
                </tr>
            </thead>        
            <tbody> 
            <?php 
            $no=1;
            foreach ($dt_tu->result() as $s) { 
                $siswa = $s->id_siswa;                       
            echo "  
                <tr>
                    <td>$no</td>
                    <td>$s->id_siswa</td>
                    <td>$s->nama_lengkap</td>
                    <td>$s->kelas</td>";
                    $jum_kol = $c->num_rows();
                    if($jum_kol>0){
                        foreach ($c->result() as $k) {            
                            $nom = mata_uang($k->nominal);                            
                            $tu = 0;
                            $sq = "SELECT * FROM tabel_bayar WHERE id_penempatan = '$s->id_penempatan' AND id_biaya = '$k->id_biaya' ORDER BY id_biaya ASC";
                            $cek = $this->db->query($sq);
                            if($cek->num_rows()>0){
                                if($k->frekuensi=='Perbulan' or $k->frekuensi=='Persemester' or $k->frekuensi=='Pertahun' or $k->frekuensi=='Satu Kali'){
                                    echo "<td>";
                                    $q = "SELECT * FROM tabel_bayar INNER JOIN tabel_biaya ON tabel_bayar.id_biaya = tabel_biaya.id_biaya 
                                            WHERE tabel_bayar.id_penempatan= '$s->id_penempatan' AND tabel_bayar.id_biaya = '$k->id_biaya'";
                                    $co = $this->db->query($q);
                                    foreach ($co->result() as $amb) {                                        
                                        $bulan = date("m-Y", strtotime($amb->tgl_bayar));
                                        $nomi = mata_uang($amb->nominal);                            
                                        echo "
                                        $nomi ($bulan)<br>                                        
                                        ";
                                    }
                                    echo "</td>";                                                                                                    
                                }else{
                                    echo "<td>$nom</td>";
                                }                                   
                            }else{
                                echo"<td>0</td>";
                                $tu = $tu + $k->nominal;
                                $tun = mata_uang($tu);
                            }                            
                        }    
                    }else{
                        $r = $dt_tu->row();
                        $id_jenjang = $r->id_jenjang;
                        $c = $this->m_biaya->get_biaya($id_jenjang);
                        foreach ($c->result() as $row){
                            echo"<td>0</td>";
                        }
                    }            
                    echo"                                                 
                </tr>
            ";
            $no++;
            }    
            ?>
            </tbody>
        </table>
        </div><!-- /.box-body -->
 
<?php 
}else{
    echo "<b>Data tidak ditemukan</b>";
} ?>

   </div><!-- /.box -->    
  </section>
</div>