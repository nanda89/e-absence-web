<?php 
function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="insert"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/biaya">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/biaya/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Jenis Pembayaran" name="biaya">
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenjang</label>
                  <div class="col-sm-10">
                    <select id="id_jenjang" name="id_jenjang" class="form-control">                    
                      <option value=''>Pilih Jenjang</option>
                      <?php 
                      foreach($dt_jenjang->result() as $dt_jenjang) {                           
                      echo "
                      <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
                      } ?>
                    </select>
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nominal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nominal" name="nominal">
                  </div>
                </div>                              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Uraian</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Uraian tentang biaya" name="uraian">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Frekuensi Pembayaran</label>
                  <div class="col-sm-10">
                    <select id="id_jenjang" name="frekuensi" class="form-control">                    
                      <option value=''>Pilih Frekuensi</option>
                      <option>Perbulan</option>
                      <option>Persemester</option>
                      <option>Pertahun</option>
                      <option>Satu Kali</option>
                    </select>
                  </div>
                </div>  
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="edit"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/biaya">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/biaya/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_biaya ?>" name="id">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->biaya ?>" placeholder="Jenis Pembayaran" name="biaya">
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenjang</label>
                  <div class="col-sm-10">
                    <select id="id_jenjang" name="id_jenjang" class="form-control">                    
                      <?php 
                      $isi=$this->m_jenjang->get_one($row->id_jenjang);
                      $row2 = $isi->row();                      
                      echo "<option value='$row2->id_jenjang'>$row2->jenjang</option>";
                      foreach($dt_jenjang->result() as $dt_jenjang) {                           
                      echo "
                      <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
                      } ?>
                    </select>
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nominal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->nominal ?>" id="inputEmail3" placeholder="Nominal" name="nominal">
                  </div>
                </div>                              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Uraian</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->uraian ?>" id="inputEmail3" placeholder="Uraian tentang biaya" name="uraian">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Frekuensi Pembayaran</label>
                  <div class="col-sm-10">
                    <select id="id_jenjang" name="frekuensi" class="form-control">                    
                      <option><?php echo $row->frekuensi ?></option>
                      <option>Perbulan</option>
                      <option>Persemester</option>
                      <option>Pertahun</option>
                      <option>Satu Kali</option>
                    </select>
                  </div>
                </div>  
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }elseif($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/biaya/add">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Jejang</th>
              <th>Biaya</th>
              <th>Nominal</th>
              <th>Uraian</th>
              <th>Frekuensi Pembayaran</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_biaya->result() as $row) {       
          $r = mata_uang($row->nominal);
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->jenjang</td>
              <td>$row->biaya</td>
              <td>Rp.$r</td>
              <td>$row->uraian</td>
              <td>$row->frekuensi</td>
              <td>";
              ?>
              <form method="post" action="adm/biaya/process">
                <input type="hidden" name="id" value="<?php echo $row->id_biaya ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-sm btn-primary btn-flat'><i class='fa fa-edit'></i></button>
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>

