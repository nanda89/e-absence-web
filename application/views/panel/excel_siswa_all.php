<?php 
$d = date('dms'); 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=data_siswa_keseluruhan-".$d.".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<b>
    Catatan : <br>
    1. Save As file ini dengan tipe (*.csv) sebelum upload ke sistem dan hapus baris ini. <br>
    2. Sesuaikan field id_ta dan id_kelas dengan tabel yg tersedia.
</b>
<table id="example2" border="1" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="2%">no</th>      
      <th>id_siswa</th>      
      <th>nisn</th>
      <th>nama_lengkap</th>   
      <th>id_ta</th>                           
      <th>id_kelas</th>
    </tr>
  </thead>
  <tbody>            
  <?php 
  $no=1; 
  foreach($dt_penempatan->result() as $row) {       
  echo "          
    <tr>
      <td>$no</td>
      <td>$row->id_siswa</td>      
      <td>$row->nisn</td>      
      <td>$row->nama_lengkap</td>
      <td>$row->tahun_ajaran</td>
      <td>$row->kelas</td>                                           
    </tr>";
  $no++;
  }
  ?>
  </tbody>
</table>

<br><br><br>
<table border="1">
    <tr>
        <th>ID Ta</th>
        <th>Tahun Ajaran</th>
    </tr>
    <?php
    $t = $this->m_tahunajaran->get_all();
    foreach ($t->result() as $k) {
         echo "
            <tr>
                <td>$k->id_ta</td>
                <td>$k->tahun_ajaran</td>
            </tr>
         ";
     }             
    ?>            
</table>

<br><br><br>
<table border="1">
    <tr>
        <th>ID Kelas</th>
        <th>Jenjang</th>        
        <th>Kelas</th>        
    </tr>
    <?php
    $te = $this->m_jenjang->get_all_kelas();
    foreach ($te->result() as $ke) {
         echo "
            <tr>
                <td>$ke->id_kelas</td>
                <td>$ke->jenjang</td>                
                <td>$ke->kelas</td>
            </tr>
         ";
     }             
    ?>            
</table>