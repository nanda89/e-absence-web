<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Insert Installation Code</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="assets/panel/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"-->
    <!-- Ionicons -->
    
    <link rel="stylesheet" href="assets/panel/dist/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/panel/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="assets/panel/plugins/iCheck/square/blue.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>  

  <body background="assets/web/bg-3.jpg">    
    <div class="login-box">
      <div class="login-logo">              
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Masukkan Installation Code untuk Mengaktifkan Aplikasi ini.</p>
        <form action="getcode/submit" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="code" required class="form-control" placeholder="Installation Code">
            <span class="glyphicon glyphicon-key form-control-feedback"></span>
          </div>          
          <div class="row">   
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Proses</button>              
            </div><!-- /.col -->          
            <div class="col-xs-4">
              <a href="web">
                <button type="reset" class="btn btn-danger btn-block btn-flat">Reset</button>              
              </a>
            </div><!-- /.col -->            
          </div>


          <?php 
          
          ?>

        </form>      
        <br><br>
        <!--button class="btn btn-info btn-flat btn-block" data-toggle="modal" data-target="#forgot_pass">Forgot Your Password ?</button-->          
      </div><!-- /.login-box-body -->     
      <br>
        <?php                       
        if (isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {                    
        ?>                  
        <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
            <strong><?php echo $_SESSION['pesan'] ?></strong>
            <button class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>  
            </button>
        </div>
        <?php
        }
            $_SESSION['pesan'] = '';                        
                
        ?>
      <!--div class="col-xs-12">
        <div class="alert alert-danger alert-dismissable" id="msg">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
          Kombinasi password dan username anda salah!
        </div>
      </div>
    </div--><!-- /.login-box -->
                      

    <!-- jQuery 2.1.4 -->
    <script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="assets/panel/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="assets/panel/plugins/iCheck/icheck.min.js"></script> 
    
  </body>
</html>
