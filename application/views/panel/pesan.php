<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php 
    if($set=="insert"){
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/kategori">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form class="form-horizontal" action="adm/kategori/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Kategori" name="kategori">
                  </div>
                </div>                                                            
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>                
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php 
    }elseif($set=="edit"){
      $row = $one_post->row(); 
    ?>

    <div class="box box-default">
      <div class="box-header with-border">        
        <h3 class="box-title">
          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <form class="form-horizontal" action="adm/pesan/process" method="GET" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $row->id_pesan ?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->nama; ?>" placeholder="Kategori" name="nama">
                  </div>
                </div>                                                            
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->email; ?>" placeholder="Kategori" name="email">
                  </div>
                </div>              
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Asal</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" value="<?php echo $row->asal; ?>" placeholder="Kategori" name="asal">
                  </div>
                </div>                                                                                                          
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pesan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control">                      
                      <?php echo $row->pesan ?>
                    </textarea>
                  </div>
                </div>  
                <a href="adm/kategori">
                  <button type="submit" value="edit" name="s_process"  class="btn bg-maroon btn-flat margin"> Kembali</button>
                </a>                                                          
              </div><!-- /.box-body -->              
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
    }elseif($set=="view"){
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th>Pengirim</th>              
              <th>Asal</th>
              <th>Email</th>
              <th>Waktu</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody>            
          <?php 
          $no=1; 
          foreach($dt_ta->result() as $row) {   
          if($row->status=='unread'){
            $r = "<span class='label label-warning'>$row->status</span>";
          }else{
            $r = "<span class='label label-success'>$row->status</span>";
          }    
          echo "          
            <tr>
              <td>$no</td>
              <td>$row->nama $r</td>              
              <td>$row->asal</td>              
              <td>$row->email</td>              
              <td>$row->waktu</td>              
              <td>";
              ?>
              <form method="GET" action="adm/pesan/process">
                <input type="hidden" name="id" value="<?php echo $row->id_pesan ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-sm btn-danger btn-flat'><i class='fa fa-trash-o'></i></button>                
                <button data-toggle='tooltip' title='Baca Detail' name="s_process" type="submit" value="lihat" class='btn btn-sm btn-info btn-flat'><i class='fa fa-eye'></i></button>
              </form>
              </td>
            </tr>
          <?php
          $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
    }
    ?>
  </section>
</div>

