<?php 
function tanggal(){
  $hari=$day=date("l");
  $tanggal=date("d");
  $bulan=$bl=$month=date("m");
  $tahun=date("Y");
  $jam=date("H");
  $menit=date("i");
  $detik=date("s");

  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }

  switch($bulan)
  {
    case"1":$bulan="Januari"; break;
    case"2":$bulan="Februari"; break;
    case"3":$bulan="Maret"; break;
    case"4":$bulan="April"; break;
    case"5":$bulan="Mei"; break;
    case"6":$bulan="Juni"; break;
    case"7":$bulan="Juli"; break;
    case"8":$bulan="Agustus"; break;
    case"9":$bulan="September"; break;
    case"10":$bulan="Oktober"; break;
    case"11":$bulan="November"; break;
    case"12":$bulan="Desember"; break;
  }

  $tglLengkap="$hari, $tanggal $bulan $tahun";
  return $tglLengkap;
}
?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i
     class="fa fa-dashboard"></i> Home</a></li>    
    <li class="active"><?php echo $judul1; ?></li>
  </ol>  
  </section>
  <section class="content">
    <?php
    if($set=="view"){
    ?>    
    <div class="row">      
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              
            </h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body with-border">
            <h5>Anda dapat melakukan pencetakan laporan harian dan bulanan. Silahkan masukkan data-data yang dibutuhkan.</h5><br>
            <form class="form-horizontal" action="adm/laporan/t_absen" method="get" enctype="multipart/form-data">   
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Jenjang</label>
                <div class="col-sm-4">
                  <select id="id_jenjang" required name="id_jenjang" class="form-control">                    
                    <option value=''>Pilih Jenjang</option>
                    <?php 
                    foreach($dt_jenjang->result() as $dt_jenjang) {                           
                    echo "
                    <option value='$dt_jenjang->id_jenjang'>$dt_jenjang->jenjang</option>";
                    } ?>
                  </select>
                </div>     

                <label for="inputEmail3" required class="col-sm-2 control-label">Tahun Ajaran</label>
                <div class="col-sm-4">
                  <select id="id_tahun" name="id_tahun" class="form-control">                    
                    <option value=''>Pilih Tahun Ajaran</option>
                    <?php 
                    foreach($dt_tahun->result() as $dt_tahun) {                           
                    echo "
                    <option value='$dt_tahun->id_ta'>$dt_tahun->tahun_ajaran</option>";
                    } ?>
                  </select>
                </div>                
              </div> 

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-4">
                  <select id="id_kelas" name="id_kelas" required class="form-control">                    
                    <option value=''>Pilih Kelas</option>
                  </select>
                </div> 
                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal (dd-MM-YYYY)</label>
                <div class="col-sm-4">
                  <input type="text" name="tanggal" class="form-control">
                </div>               
              </div>   

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Bulan</label>
                <div class="col-sm-4">
                  <select id="id_tahun" name="bulan" class="form-control">                    
                    <option value=''>Pilih Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>  
                <label for="inputEmail3" class="col-sm-2 control-label">Tahun</label>
                <div class="col-sm-4">
                  <select id="id_tahun" name="tahun" class="form-control">                    
                    <option value=''>Pilih Tahun</option>
                    <?php 
                    for ($i=2010; $i < 2025; $i++) { 
                      echo "<option>$i</option>
                      ";
                    }
                    ?>
                  </select>
                </div>              
              </div>       
                        
         
                    
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="submit" class="btn bg-maroon btn-flat margin"><i class="fa fa-list"></i> Filter Data</button>              
                </div>                 
              </div>  
            </form>  
          </div> 
          <div class="box-body">
            
            <table id="example2" class="table table-bordered table-hovered">
              <thead>
                <tr>
                  <th width="1%">No</th>      
                  <th width="5%">No.Induk</th>      
                  <th>Nama Lengkap</th>
                  <th>Jenjang</th>  
                  <th>Kelas</th>   
                  <th>Tanggal</th>       
                  <th width="15%">Jam Absen Masuk</th>
                  <th width="15%">Jam Absen Pulang</th>                      
                </tr>
              </thead>
              <tbody>            
              <?php 
              $no=1; 
              foreach($dt_wali_kelas->result() as $row) { 
              $tanggal = date("d F Y", strtotime($row->tgl));
              $tgl = date('d-m-Y');
              $s = "SELECT * FROM tabel_absen RIGHT JOIN tabel_siswakelas 
                    ON tabel_absen.id_penempatan = tabel_siswakelas.id_penempatan
                    INNER JOIN tabel_siswa ON tabel_siswakelas.id_siswa = tabel_siswa.id_siswa
                    WHERE tabel_siswakelas.id_penempatan='$row->id_penempatan' and tabel_absen.tgl = '$row->tgl'";
              $d = $this->db->query($s);
              $ambil = $d->row();
              if($d->num_rows()>0){
                if($ambil->absen_masuk==''){
                  $absen_masuk = "-";
                  $jam_masuk = '';  
                }else{
                  $absen_masuk = $ambil->absen_masuk; 
                  $jam_masuk = $ambil->jam_masuk." | ";  
                }

                if($ambil->absen_pulang==''){
                  $absen_pulang = "-";  
                  $jam_pulang = '';
                }else{
                  $absen_pulang = $ambil->absen_pulang; 
                  $jam_pulang = $ambil->jam_pulang." | "; 
                }
              }else{
                $absen_pulang = "-";
                $absen_masuk = "-";
                $jam_masuk = '';
                $jam_pulang = '';
              }

             
              echo "          
                <tr>
                  <td>$no</td>
                  <td>$row->id_siswa</td>      
                  <td>$row->nama_lengkap</td>  
                  <td>$row->jenjang</td>  
                  <td>$row->kelas</td>
                  <td>$tanggal</td>                         
                  <td>$jam_masuk$absen_masuk</td>
                  <td>$jam_pulang$absen_pulang</td>
                  "; ?>                                 
                </tr>
              <?php
              $no++;
              }
              ?>
              </tbody>
            </table>


          </div><!-- /.box-body -->
        </div><!-- /.box -->        
      </div>

    </div>

    <?php
    }
    ?>
  </section>
</div>


<script src="assets/panel/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>

$("#id_jenjang").change(function(){
  var id_jenjang = $("#id_jenjang").val();
  $.ajax({
    url : "<?php echo site_url('adm/jadwal/get_kelas')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#id_kelas").html(msg);
      get_guru(id_jenjang);
    }
  })  
});

function hapus_jadwal(a,b,c,d){ 
  var id_jadwal     = a;   
  var id_ta         = b;       
  var id_jenjang    = c;   
  var id_kelas      = d;       
  $.ajax({
      url : "<?php echo site_url('adm/jadwal/delete_jadwal')?>",
      type:"POST",
      data:"id_jadwal="+id_jadwal,
      cache:false,
      success:function(msg){            
          data=msg.split("|");
          if(data[0]=="nihil"){
            kirim_data_jadwal();
          }
      }
  })
}
function simpan_jadwal(){
  var id_tahun            = document.getElementById("id_tahun").value;   
  var id_jenjang   = $("#id_jenjang").val();
  var id_kelas          = $("#id_kelas").val();
  var id_mapel      = $("#id_mapel").val();    
  var id_guru    = $("#id_guru").val();        
  var hari    = $("#hari").val();        
  var jam_awal    = $("#jam_awal").val();        
  var jam_akhir    = $("#jam_akhir").val();        
  if (id_tahun=="" || id_jenjang=="" || id_kelas == "" || id_mapel == ""|| id_guru == ""|| hari == "" || jam_awal == "" || jam_akhir == "") {    
      alert("Isikan data dengan lengkap...!");
      return false;
  }else{
      $.ajax({
          url : "<?php echo site_url('adm/jadwal/save')?>",
          type:"POST",
          data:"id_tahun="+id_tahun+"&id_jenjang="+id_jenjang+"&id_kelas="+id_kelas+"&id_mapel="+id_mapel+"&id_guru="+id_guru+"&hari="+hari+"&jam_awal="+jam_awal+"&jam_akhir="+jam_akhir,
          cache:false,
          success:function(msg){            
              data=msg.split("|");
              if(data[0]=="nihil"){
                  kirim_data_jadwal();
                  kosong_jadwal();                
              }else{
                  alert("Data gagal tersimpan");
              }                
          }
      })    
  }
}
function re(args){
  $("#hari").val("");
  $("#jam_awal").val("");
  $("#jam_akhir").val("");
  $("#id_mapel").val("");
  $("#id_guru").val("");
  $("#id_tahun").val("");
  $("#id_kelas").val("");
  $("#id_jenjang").val("");
  $("#tampil_jadwal").hide();
}
function kosong_jadwal(args){
  $("#hari").val("");
  $("#jam_awal").val("");
  $("#jam_akhir").val("");
  $("#id_mapel").val("");
  $("#id_guru").val("");
}
function kirim_data_jadwal(){    
  $("#tampil_jadwal").show();
  var id_tahun = document.getElementById("id_tahun").value;   
  var id_jenjang = document.getElementById("id_jenjang").value;   
  var id_kelas = document.getElementById("id_kelas").value;   
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  } 
   //var data = "birthday1="+birthday1_js;          
    var data = "id_tahun="+id_tahun+"&id_jenjang="+id_jenjang+"&id_kelas="+id_kelas;
     xhr.open("POST", "adm/jadwal/t_jadwal", true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {       
                document.getElementById("tampil_jadwal").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    } 
}
function get_guru(id_jenjang){ 
  $.ajax({
    url : "<?php echo site_url('adm/jadwal/get_guru')?>",
    type:"POST",
    data:"id_jenjang="+id_jenjang,      
    cache:false,   
    success:function(msg){            
      $("#id_guru").html(msg);
    }
  })
}
</script>