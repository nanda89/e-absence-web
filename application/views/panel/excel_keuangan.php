<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");

function mata_uang($a){
    return number_format($a, 0, ',', '.');
}
?>
        <b>Data Pembayaran Keseluruhan</b>        
        <table class="table table-bordered responsive-utilities jambo_table bulk_action" id="example2">
            <thead>
                <tr class="headings">                                                
                    <th width="5%">No </th>
                    <th>No.Induk</th>            
                    <th>Nama Siswa</th>
                    <th>Jenjang</th>
                    <th>Kelas</th>    
                    <th>Biaya</th>
                    <th>Nominal</th>
                    <th>Tgl.Bayar</th>
                </tr>
            </thead>        
            <tbody> 
            <?php 
            $total = 0;
            $no=1;
            foreach ($dt_tu->result() as $s) {
              $h = $s->nominal;                
              $nom = mata_uang($h);
              $tgl = date("d F Y", strtotime($s->tgl_bayar));              
            echo "  
                <tr>
                    <td>$no</td>
                    <td>$s->id_siswa</td>
                    <td>$s->nama_lengkap</td>
                    <td>$s->jenjang</td>
                    <td>$s->kelas</td>
                    <td>$s->biaya</td>
                    <td>$nom</td>
                    <td>$tgl</td>                                                 
                </tr>
            ";
            $total = $total + $h;
            $t = mata_uang($total);
            $no++;
            }    
            ?>
            </tbody>
            <tbody>
                <tr>
                <td>&nbsp;</td>
                <td colspan="4" align="right"><b>Total</b></td>
                <td><b>Rp. <?php echo $t ?></b></td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
    
