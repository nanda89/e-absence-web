<base href="<?php echo base_url(); ?>" />
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul1; ?>
    <small><?php echo $judul2; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="panel/home"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul1; ?></li>
  </ol>
  </section>
  <section class="content">
    <?php
if ($set == "insert") {
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-eye"></i> Lihat Data</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/guru/save" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIK *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="nik_id" placeholder="NIK" name="nik">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Penempatan *</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="id_jenjang" required>
                      <option value="">Pilih</option>
                      <?php
foreach ($dt_jenjang->result() as $row) {
        echo "
                      <option value='$row->id_jenjang'>$row->jenjang</option>";
    }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="Nama Lengkap" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Alias</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="inputEmail3" placeholder="Kode Penamaan" name="alias">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.KTP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="No KTP" name="no_ktp">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin *</label>
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;&nbsp;&nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="">Pilih</option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" id="tanggal" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Unit Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Unit Kerja" name="unit_kerja">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl.Mulai Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Tgl.Mulai Kerja" id="tanggal2" name="tmt">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="ijazah">
                      <option value="">Pilih</option>
                      <option value="SD">SD</option>
                      <option value="SLTP">SLTP</option>
                      <option value="SLTA">SLTA</option>
                      <option value="D1">D1</option>
                      <option value="D2">D2</option>
                      <option value="D3">D3</option>
                      <option value="D4">D4</option>
                      <option value="S1">S1</option>
                      <option value="S2">S2</option>
                      <option value="S3">S3</option>
                    </select>
                  </div>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama Sekolah / PT / Universitas" name="pend_terakhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Hp *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputEmail3" required placeholder="No. HP" name="no_hp">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control pull-right" name="email" placeholder="Alamat Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" placeholder="Pas Foto" name="gambar">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                  <div class="col-sm-8">
                   <input type="text" id="mapel" class="form-control" placeholder="Mata Pelajaran yang Diampu" name="mapel">
                   <input type="hidden" id="id_mapel" class="form-control" placeholder="Mata Pelajaran yang Diampu" name="mapel">
                  </div>
                  <div class="col-sm-2">
                   <button type="button" data-toggle="modal" data-target="#Amodal" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Cari Mapel</button>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="col-sm-2 control-label">Status guru BK</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="status_guru_bk">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>

                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="col-sm-2 control-label">Status kepsek</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="status_kepsek">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>

                </div>
<div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="col-sm-2 control-label">Ijinkan akses aplikasi</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="login_access" onchange="access(this.value)" id="login_access">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group hidden" id="form-user">
                  <label for="inputEmail3" class="col-sm-2 control-label">password</label>
                  <div class="col-sm-4">
                    <input type="password" class="form-control pull-right" name="password" placeholder="password" maxlength="15">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-8">
                   <button type="button" onClick="simpan_mapel()" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Add</button>
                   <button type="button" onClick="kirim_data_mapel()" class="btn btn-warning btn-flat"><i class="fa fa-eye"></i> Show</button>
                   <button type="button" onClick="hide_mapel()" class="btn btn-warning btn-flat"><i class="fa fa-eye-slash"></i> Hide</button>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-10">
                    <div id="tampil_mapel"></div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="save" value="save" class="btn btn-info">Save</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "edit") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/guru/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_guru ?>" name="id">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIK *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" required id="nik_id" value="<?php echo $row->nik ?>" placeholder="NIK" name="nik">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Penempatan *</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="id_jenjang" required>
                      <option value="<?php echo $row->id_jenjang ?>">
                      <?php
$r = $this->m_jenjang->get_one($row->id_jenjang);
    $r_je = $r->row();
    echo $r_je->jenjang;
    ?>
                    </option>
                      <?php
foreach ($dt_jenjang->result() as $row2) {
        echo "
                      <option value='$row2->id_jenjang'>$row2->jenjang</option>";
    }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $row->nama ?>" id="inputEmail3" required placeholder="Nama Lengkap" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Alias</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->alias ?>" required id="inputEmail3" placeholder="Kode Penamaan" name="alias">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">No.KTP *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php echo $row->no_ktp ?>" id="inputEmail3" required placeholder="No KTP" name="no_ktp">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin *</label>
                  <div class="col-sm-4">
                    <?php
$j = $row->jenis_kelamin;
    if ($j == 'perempuan') {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red"> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red" checked> Perempuan
                    </label>
                    <?php
} elseif ($j == 'laki-laki') {
        ?>
                    <label>
                      <input type="radio" name="jenis_kelamin" value="laki-laki" class="flat-red" checked> Laki-laki
                    </label> &nbsp;
                    <label>
                      <input type="radio" name="jenis_kelamin" value="perempuan" class="flat-red"> Perempuan
                    </label>
                    <?php
}
    ?>
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Agama</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="agama">
                      <option value="<?php echo $row->agama ?>"><?php echo $row->agama ?></option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Katholik">Katholik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                      <option value="Konghucu">Konghucu</option>
                      <option value="Tidak Diisi">Tidak Diisi</option>
                      <option value="Lainnya">Lainnya</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tempat - Tgl.Lahir</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tempat_lahir ?>" class="form-control" id="inputEmail3" placeholder="Tempat Lahir" name="tempat_lahir">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tgl_lahir ?>" class="form-control pull-right" id="tanggal" name="tgl_lahir" placeholder="Tanggal Lahir (tgl-bulan-tahun)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Unit Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->unit_kerja ?>" class="form-control" id="inputEmail3" placeholder="Unit Kerja" name="unit_kerja">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Tgl.Mulai Kerja</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->tmt ?>" class="form-control" placeholder="Tgl.Mulai Kerja" id="tanggal2" name="tmt">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="ijazah">
                      <option value="<?php echo $row->ijazah ?>"><?php echo $row->ijazah ?></option>
                      <option value="SD">SD</option>
                      <option value="SLTP">SLTP</option>
                      <option value="SLTA">SLTA</option>
                      <option value="D1">D1</option>
                      <option value="D2">D2</option>
                      <option value="D3">D3</option>
                      <option value="D4">D4</option>
                      <option value="S1">S1</option>
                      <option value="S2">S2</option>
                      <option value="S3">S3</option>
                    </select>
                  </div>
                  <div class="col-sm-8">
                    <input type="text" value="<?php echo $row->pend_terakhir ?>" class="form-control" id="inputEmail3" placeholder="Nama Sekolah / PT / Universitas" name="pend_terakhir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.Hp *</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->no_hp ?>" class="form-control" id="inputEmail3" required placeholder="No. HP" name="no_hp">
                  </div>
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-4">
                    <input type="text" value="<?php echo $row->email ?>" class="form-control pull-right" name="email" placeholder="Alamat Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo $row->alamat ?>" class="form-control" id="inputEmail3" placeholder="Alamat Lengkap" name="alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pas Foto (3x4 Warna)</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="inputEmail3" placeholder="Pas Foto" name="gambar">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Mata Pelajaran</label>
                  <div class="col-sm-8">
                   <input type="text" id="mapel" class="form-control" placeholder="Mata Pelajaran yang Diampu" name="mapel">
                   <input type="hidden" id="id_mapel" class="form-control" placeholder="Mata Pelajaran yang Diampu" name="mapel">
                  </div>
                  <div class="col-sm-2">
                   <button type="button" data-toggle="modal" data-target="#Amodal" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Cari Mapel</button>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="col-sm-2 control-label">Status guru BK</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="status_guru_bk">
                      <option value="<?php echo $row->status_guru_bk ?>"><?php
if ($row->status_guru_bk == "1") {
        echo "Ya";
    } else {
        echo "Tidak";
    }

    ?>
                        </option>
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>

                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                  <label for="inputEmail3" class="col-sm-2 control-label">Status kepsek</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="status_kepsek">
                      <option value="<?php echo $row->status_kepsek ?>"><?php
if ($row->status_kepsek == "1") {
        echo "Ya";
    } else {
        echo "Tidak";
    }

    ?>
                        </option>
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                  </div>

                </div>
                <div class="form-group">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-8">
                   <button type="button" onClick="simpan_mapel()" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Add</button>
                   <button type="button" onClick="kirim_data_mapel()" class="btn btn-warning btn-flat"><i class="fa fa-eye"></i> Show</button>
                   <button type="button" onClick="hide_mapel()" class="btn btn-warning btn-flat"><i class="fa fa-eye-slash"></i> Hide</button>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-10">
                    <div id="tampil_mapel"></div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="edit" class="btn btn-info">Update</button>
                <button type="reset" class="btn btn-default pull-right">Reset</button>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "detail") {
    $row = $one_post->row();
    ?>

    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">
          <a href="adm/guru">
            <button class="btn bg-maroon btn-flat margin"><i class="fa fa-arrow-left"></i> Kembali</button>
          </a>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal" action="adm/guru/process" method="post" enctype="multipart/form-data">
              <input type="hidden" value="<?php echo $row->id_guru; ?>" name="id">
              <div class="box-body">
                <table width="100%" border="0">
                  <tr>
                    <td width="20%">
                      <label class="control-label">NIK</label>
                    </td>
                    <td>
                      <input type="hidden" id="nik_id" value="<?php echo $row->nik; ?>">
                      <?php echo $row->nik; ?></td>
                    <td width="20%" align="right" rowspan="10">
                      <img src="assets/panel/images/<?php echo $row->gambar; ?>" width="200px">
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Nama</label>
                    </td>
                    <?php
$r = $this->m_jenjang->get_one($row->id_jenjang);
    $r_je = $r->row();
    $n = $r_je->jenjang;
    ?>
                    <td><?php echo $row->nama; ?> [ <?php echo $row->alias; ?> - <?php echo $n; ?> ]</td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">No.KTP</label>
                    </td>
                    <td><?php echo $row->no_ktp; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Jenis Kelamin</label>
                    </td>
                    <td><?php echo $row->jenis_kelamin; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tempat, Tgl.Lahir</label>
                    </td>
                    <td><?php echo $row->tempat_lahir . " , " . $row->tgl_lahir; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Agama</label>
                    </td>
                    <td><?php echo $row->agama; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Unit Kerja</label>
                    </td>
                    <td><?php echo $row->unit_kerja; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Tgl.Mulai Kerja</label>
                    </td>
                    <td><?php echo $row->tmt; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Pendidikan Terakhir</label>
                    </td>
                    <td><?php echo $row->ijazah . " - " . $row->pend_terakhir; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">No.HP</label>
                    </td>
                    <td><?php echo $row->no_hp; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Email</label>
                    </td>
                    <td><?php echo $row->email; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <label class="control-label">Alamat Lengkap</label>
                    </td>
                    <td><?php echo $row->alamat; ?></td>
                  </tr>
                  <tr>
                    <td valign="top"><label class="control-label"> Mata Pelajaran yg diampu</label></td>
                    <td><ul>
                      <?php
$ce = "SELECT * FROM tabel_pengampu INNER JOIN tabel_mapel ON tabel_pengampu.id_mapel=tabel_mapel.id_mapel
                        INNER JOIN tabel_kat_mapel ON tabel_mapel.id_kat_mapel = tabel_kat_mapel.id_kat_mapel
                        WHERE tabel_pengampu.nik = '$row->nik'";
    $s = $this->db->query($ce);
    foreach ($s->result() as $k) {
        echo "<li>$k->mapel</li>";
    }
    ?> </ul>
                    </td>
                  </tr>
                  <tr>
                      <td>
                        <label class="control-label">Status guru BK</label>
                      </td>
                      <td>
                        <?php
if ($row->status_guru_bk == "1") {
        echo "ya";
    } else {
        echo "tidak";
    }

    ?>
                      </td>
                  </tr>
                  <tr>
                      <td>
                        <label class="control-label">Status kepsek</label>
                      </td>
                      <td>
                        <?php
if ($row->status_kepsek == "1") {
        echo "ya";
    } else {
        echo "tidak";
    }

    ?>
                      </td>
                  </tr>
                </table>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" name="s_process" value="ubah" class="btn btn-info">Edit</button>
                <a href="adm/guru">
                  <button type="button" class="btn btn-default pull-right">Cancel</button>
                </a>
              </div><!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.box -->

    <?php
} elseif ($set == "view") {
    ?>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">

          <a href="adm/guru/add">
            <button type="button" class="btn bg-maroon btn-flat margin"><i class="fa fa-plus"></i> Tambah Data</button>
            <!--button onclick="return confirm('Anda Yakin Ingin Menghapus Semua Data yang Ditandai ?')" type="button" class="btn bg-red btn-flat margin"><i class="fa fa-trash-o"></i> Hapus Banyak</button-->
          </a>
          <button class="btn bg-maroon btn-flat margin" onclick="bulk_delete()"><i class="fa fa-trash"></i> Bulk Delete</button>

        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <?php
if (isset($_SESSION['pesan']) && $_SESSION['pesan'] != '') {
        ?>
            <div class="alert alert-<?php echo $_SESSION['tipe'] ?> alert-dismissable">
                <strong><?php echo $_SESSION['pesan'] ?></strong>
                <button class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <?php
}
    $_SESSION['pesan'] = '';

    ?>
        <table id="example2" class="table table-bordered table-hovered">
          <thead>
            <tr>
              <th width="1%"><input type="checkbox" id="check-all"></th>
              <th width="5%">No</th>

              <th>NIK</th>
              <th>Nama Lengkap/Alias</th>
              <th>No.KTP</th>
              <th>Penempatan</th>
              <th>No.Hp</th>
              <th>Pass.Sync</th>
              <th>Status guru BK</th>
              <th width="16%">Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php
$no = 1;
    foreach ($dt_guru->result() as $row) {
        $r = $this->m_jenjang->get_one($row->id_jenjang);
        $r_je = $r->row();
        if (!$r_je) {$n = null;} else {
            $n = $r_je->jenjang;
        }

        $status_guru_bk = ($row->status_guru_bk == "1") ? "ya" : "tidak";
        echo "
            <tr>
              <td><input type='checkbox' class='data-check' value='$row->id_guru'></td>
              <td>$no</td>
              <td>$row->nik</td>
              <td>$row->nama / $row->alias</td>
              <td>$row->no_ktp</td>
              <td>$n</td>
              <td>$row->no_hp</td>
              <td>$row->pass_sync</td>
              <td>
                  $status_guru_bk
              </td>
              <td>";
        ?>
                <form method="post" action="adm/guru/process">
                <input type="hidden" name="id" value="<?php echo $row->id_guru ?>" />
                <input type="hidden" name="nik" value="<?php echo $row->nik ?>" />
                <input type="hidden" name="nama" value="<?php echo $row->nama ?>" />
                <input type="hidden" name="jk" value="<?php echo $row->jenis_kelamin ?>" />
                <button data-toggle='tooltip' onclick="return confirm('Anda Yakin Ingin Menghapus Data Ini ?')" title='Hapus' name="s_process" type="submit" value="hapus" class='btn btn-danger btn-flat btn-sm'><i class='fa fa-trash-o'></i></button>
                <button data-toggle='tooltip' title='Edit' name="s_process" type="submit" value="ubah" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-edit'></i></button>
                <button data-toggle='tooltip' title='Detail' name="s_process" type="submit" value="detail" class='btn btn-info btn-flat btn-sm'><i class='fa fa-eye'></i></button>
                <?php
$id = $row->nik;
        $c = $this->m_admin->cek_user($id);
        if ($c->num_rows() > 0) {
            ?>
                <button data-toggle='tooltip' title='Set Akun Default' name="s_process" type="submit" value="set_default" class='btn btn-primary btn-flat btn-sm'><i class='fa fa-key'></i></button>
                <?php
} else {
            ?>
                <button data-toggle='tooltip' title='Set Akun' name="s_process" type="submit" value="set_akun" class='btn btn-warning btn-flat btn-sm'><i class='fa fa-key'></i></button>
                <?php
}
        ?>
              </form>
              </td>
            </tr>
          <?php
$no++;
    }
    ?>
          </tbody>
        </table>

      </div><!-- /.box-body -->
    </div><!-- /.box -->

    <?php
}
?>
  </section>
</div>




<div class="modal fade" id="Amodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cari Data Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered table-hover" id="example2">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Kategori</th>
            <th>Mapel</th>
            <th>KKM</th>
            <th width="5%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        <?php
$no = 1;
foreach ($dt_mapel->result() as $re) {
    echo "<tr>
              <td align='center'>$no</td>
              <td>$re->kategori_mapel</td>
              <td>$re->mapel</td>
              <td>$re->kkm</td>";?>
              <td class="center">
                <button title="Pilih Data" onclick="milih('<?php echo $re->id_mapel; ?>')" class="btn btn-success">Pilih</button>
              </td>
              </tr>
          <?php
$no++;
}
?>
        </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function milih(id_mapel){
  $.ajax({
      url:"<?php echo site_url('adm/guru/cari_mapel') ?>",
      type:"POST",
      data:"id_mapel="+id_mapel,
      cache:false,
      success:function(msg){
          data=msg.split("|");
          $("#mapel").val(data[1]);
          $("#id_mapel").val(data[0]);
      }
  })
  $("#Amodal").modal("hide");
}
function hide_mapel(){
    $("#tampil_mapel").hide();
}
function kirim_data_mapel(){
  $("#tampil_mapel").show();
  var nik = document.getElementById("nik_id").value;
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    xhr = new XMLHttpRequest();
  }else if (window.ActiveXObject) { // IE 8 and older
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
   //var data = "birthday1="+birthday1_js;
    var data = "nik="+nik;
     xhr.open("POST", "adm/guru/t_mapel", true);
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
     xhr.send(data);
     xhr.onreadystatechange = display_data;
     function display_data() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                document.getElementById("tampil_mapel").innerHTML = xhr.responseText;
            }else{
                alert('There was a problem with the request.');
            }
        }
    }
}
function simpan_mapel(){
    var id_mapel            = document.getElementById("id_mapel").value;
    var nik_js              = $("#nik_id").val();
    if (id_mapel=="" || nik_js=="") {
        alert("Isikan data dengan lengkap, pastikan Mapel terisi...!");
        return false;
    }else{
        $.ajax({
            url : "<?php echo site_url('adm/guru/save_mapel') ?>",
            type:"POST",
            data:"id_mapel="+id_mapel+"&nik="+nik_js,
            cache:false,
            success:function(msg){
                data=msg.split("|");
                if(data[0]=="nihil"){
                    kirim_data_mapel();
                    kosong();
                }else{
                    alert('Mapel ini sudah ditambahkan');
                    kosong();
                }
            }
        })
    }
}
function kosong(args){
  $("#id_mapel").val("");
  $("#mapel").val("");
}
function hapus_mapel(a,b){
    var id_ampu_js  = a;
    var nik_js     = b;
    $.ajax({
        url : "<?php echo site_url('adm/guru/delete_mapel') ?>",
        type:"POST",
        data:"id_ampu="+id_ampu_js,
        cache:false,
        success:function(msg){
            data=msg.split("|");
            if(data[0]=="nihil"){
              kirim_data_mapel();
            }
        }
    })
}
function access(value){
  if(value == "1"){
    showFormLogin();
  }else{
    hideFormLogin();
  }
}

function showFormLogin(){
  $("#form-user").removeClass("hidden");
}
function hideFormLogin(){
  $("#form-user").addClass("hidden");
}
</script>



<script type="text/javascript">
function bulk_delete(){
  var list_id = [];
  $(".data-check:checked").each(function() {
    list_id.push(this.value);
  });
  if(list_id.length > 0){
    if(confirm('Are you sure delete this '+list_id.length+' data?'))
      {
        $.ajax({
          type: "POST",
          data: {id:list_id},
          url: "<?php echo site_url('adm/guru/ajax_bulk_delete') ?>",
          dataType: "JSON",
          success: function(data)
          {
            if(data.status){
              window.location.reload();
            }else{
              alert('Failed.');
            }
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error deleting data');
          }
        });
      }
    }else{
      alert('no data selected');
  }
}
</script>