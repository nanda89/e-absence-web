<div class="row">
  <div class="col-md-2"><b>Tahun</b></div>
  <div class="col-md-1" style="text-align: right;"><b>:</b></div>
  <div class="col-md-9"><?php echo $dt_jdwl_tmbh->tahun_ajaran ?></div>
</div>
<div class="row">
  <div class="col-md-2"><b>Jenjang</b></div>
  <div class="col-md-1" style="text-align: right;"><b>:</b></div>
  <div class="col-md-9"><?php echo $dt_jdwl_tmbh->nama_kelas ?></div>
</div>
<div class="row">
  <div class="col-md-2"><b>Nama Kelas Tambahan</b></div>
  <div class="col-md-1" style="text-align: right;"><b>:</b></div>
  <div class="col-md-9"><?php echo $dt_jdwl_tmbh->nama_kelas ?></div>
</div>
<div class="row">
  <div class="col-md-2"><b>Guru</b></div>
  <div class="col-md-1" style="text-align: right;"><b>:</b></div>
  <div class="col-md-9"><?php echo $dt_jdwl_tmbh->nama ?></div>
</div>

<table id="example2" class="table table-bordered table-hovered">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th>NISN</th>
      <th>Nama Lengkap</th>
      <th>Kelas</th>
      <th>No.Hp</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
    foreach ($dt_siswa->result() as $row) {
      echo "          
    <tr>
      <td>$no</td>
      <td>$row->nisn</td>
      <td>$row->nama_lengkap</td>
      <td>$row->kelas</td>
      <td>$row->no_hp</td>"; ?>
      <td><button title="Hapus Siswa" class="btn btn-sm btn-danger fa fa-trash-o btn-flat btn-del-siswa" type="button" data-idjdwl="<?php echo $row->id_jadwal_tambahan; ?>" data-namasiswa="<?php echo $row->nama_lengkap; ?>" data-idsiswa="<?php echo $row->id_siswa; ?>"></button></td>
      </button></td>
      </tr>
    <?php
      $no++;
    }
    ?>
  </tbody>
</table>