<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <?php
        $id_user = $this->session->userdata('id_user');
        $m    = $this->m_admin->get_one($id_user);
        $ambil = $m->row();
        echo "<img src='assets/panel/icon/" . (($ambil->avatar == NULL) ? 'siswa-pr2.png' : $ambil->avatar) . "' width='18px' class='img-circle' alt='User Image'>";
        ?>
      </div>
      <div class="pull-left info">
        <p>
          <?php echo $name = $this->session->userdata('nama');  ?>
        </p>
        <a href="panel/home"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <?php
    $level = $this->session->userdata('level');
    if ($level == 'administrator' or $level == 'super admin') {
      ?>
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="
            <?php
              if ($isi == 'dashboard') {
                echo "active";
              }
              ?>
            treeview">
          <a href="panel/home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="
            <?php
              if (
                $isi == 'kalender' or $isi == 'jadwal' or $isi == 'kalender_pendidikan'
                or $isi == 'jadwal_tambahan'
              ) {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#"><i class="fa fa-pie-chart"></i><span>Kurikulum</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'jadwal') {
                            echo "active";
                          }
                          ?>"><a href="adm/jadwal"><i class="fa fa-circle-o"></i> Penjadwalan</a></li>
            <li class="<?php
                          if ($isi == 'jadwal_tambahan') {
                            echo "active";
                          }
                          ?>"><a href="adm/jadwal_tambahan"><i class="fa fa-circle-o"></i> Penjadwalan Tambahan</a></li>
            <li class="<?php
                          if ($isi == 'kalender') {
                            echo "active";
                          }
                          ?>"><a href="adm/kalender"><i class="fa fa-circle-o"></i> Kalender Pendidikan</a></li>
            <li class="<?php
                          if ($isi == 'kalender_pendidikan') {
                            echo "active";
                          }
                          ?>"><a href="kalender/kalender_pendidikan"><i class="fa fa-circle-o"></i> Cetak</a></li>
          </ul>
        </li>
        <li class="<?php
                      if (
                        $isi == 'penempatan' or $isi == 'pendataan' or $isi == 'kelulusan' or $isi == 'kenaikan' or
                        $isi == 'absen' or $isi == 'cek_absen' or $isi == 'absen_siswa' or $isi == 'cek_absen_siswa' or $isi == 'cek_absen_semua'
                        or $isi == 'cek_absen_guru' or $isi == 'cek_absen_guru_rekap' or $isi == 'cek_absen_kehadiran_guru'
                        or $isi == 'cek_absen_non_guru' or $isi == 'cek_kegiatan_non_guru' or $isi == 'dashboard' or $isi == 'cek_absen_kehadiran_guru_rekap' or $isi == 'cek_absen_kehadiran_non_guru_rekap'
                      ) {
                        echo "active";
                      }
                      ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-edit"></i>
            <span>Akademik</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="
                  <?php
                    if ($isi == 'penempatan' or $isi == 'pendataan' or $isi == 'kelulusan' or $isi == 'kenaikan') {
                      echo "active";
                    }
                    ?>
                  ">
              <a href="#"><i class="fa fa-circle-o"></i> Laporan Kesiswaan <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="
                    <?php
                      if ($isi == 'pendataan') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/pendataan"><i class="fa fa-circle-o"></i> Data Siswa Aktif</a></li>

                <li class="
                    <?php
                      if ($isi == 'kelulusan') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/kelulusan"><i class="fa fa-circle-o"></i> Data Kelulusan</a></li>
              </ul>
            </li>
            <li class="
                  <?php
                    if ($isi == 'absen' or $isi == 'cek_absen') {
                      echo "active";
                    }
                    ?>
                  ">
              <a href="#"><i class="fa fa-circle-o"></i> Laporan Absensi Kelas<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="
                    <?php
                      if ($isi == 'cek_absen') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/cek_absen"><i class="fa fa-circle-o"></i> Cek Absen Kelas</a></li>
                <li class="
                    <?php
                      if ($isi == 'absen') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/absen"><i class="fa fa-circle-o"></i> Rekap Absensi Kelas</a></li>
              </ul>
            </li>
            <li class="
                  <?php
                    if ($isi == 'absen_siswa' or $isi == 'cek_absen_siswa' or $isi == 'cek_absen_semua') {
                      echo "active";
                    }
                    ?>
                  ">
              <a href="#"><i class="fa fa-circle-o"></i> Laporan Absensi Siswa<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="
                    <?php
                      if ($isi == 'cek_absen_siswa') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/cek_absen_siswa"><i class="fa fa-circle-o"></i> Cek Absen Siswa</a></li>
                <li class="
                    <?php
                      if ($isi == 'cek_absen_semua') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/cek_absen_semua"><i class="fa fa-circle-o"></i> Cek Absen Semua</a></li>
                <li class="
                    <?php
                      if ($isi == 'absen_siswa') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/absen_siswa"><i class="fa fa-circle-o"></i> Absensi Siswa Harian</a></li>
                <li class="
                    <?php
                      if ($isi == 'absen_siswa_rekap') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/absen_siswa_rekap"><i class="fa fa-circle-o"></i> Rekap Absensi Siswa</a></li>

              </ul>
            </li>
            <li class="
                  <?php
                    if ($isi == 'cek_absen_guru' or $isi == 'cek_absen_guru_rekap') {
                      echo "active";
                    }
                    ?>
                  ">
              <a href="#"><i class="fa fa-circle-o"></i> Lap. Mengajar Guru<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="
                    <?php
                      if ($isi == 'cek_absen_guru') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/cek_absen_guru"><i class="fa fa-circle-o"></i> Laporan Harian</a></li>
                <li class="
                    <?php
                      if ($isi == 'cek_absen_guru_rekap') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/cek_absen_guru_rekap"><i class="fa fa-circle-o"></i> Rekap Mengajar Guru</a></li>

                <li class="
                    <?php
                      if ($isi == 'rekap_daftar_hadir') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/rekap_daftar_hadir"><i class="fa fa-circle-o"></i> Rekap Daftar Hadir</a></li>

              </ul>
            </li>
            <li class="<?php echo ($isi == 'cek_absen_kehadiran_guru' or $isi == 'cek_absen_non_guru' or $isi == 'cek_absen_kehadiran_guru_rekap' or $isi == 'cek_absen_kehadiran_non_guru_rekap' or $isi == 'cek_kegiatan_non_guru') ? "active" : ""; ?>">
              <a href="#"><i class="fa fa-circle-o"></i> Lap. Datang & Pulang<i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="
                      <?php
                        if ($isi == 'cek_absen_kehadiran_guru') {
                          echo "active";
                        }
                        ?>
                      "><a href="adm/cek_absen_kehadiran_guru"><i class="fa fa-circle-o"></i> Laporan Guru</a></li>
                <li class="<?php echo ($isi == 'cek_absen_kehadiran_guru_rekap') ? "active" : ""; ?>"><a href="adm/cek_absen_kehadiran_guru_rekap"><i class="fa fa-circle-o"></i> Lap. Rekap Hadir Guru</a></li>
                <li class="
                      <?php
                        if ($isi == 'cek_absen_non_guru') {
                          echo "active";
                        }
                        ?>
                      "><a href="adm/cek_absen_non_guru"><i class="fa fa-circle-o"></i> Laporan Non Guru</a></li>
                <li class="
                      <?php
                        if ($isi == 'cek_absen_kehadiran_non_guru_rekap') {
                          echo "active";
                        }
                        ?>
                      "><a href="adm/cek_absen_kehadiran_non_guru_rekap"><i class="fa fa-circle-o"></i> Lap. Rekap Hadir Non Guru</a></li>
                <li class="
                      <?php
                        if ($isi == 'cek_kegiatan_non_guru') {
                          echo "active";
                        }
                        ?>
                      "><a href="adm/cek_kegiatan_non_guru"><i class="fa fa-circle-o"></i> Lap Keg. Non. Guru</a></li>
              </ul>
            </li>


            <!--li><a href="assets/panel/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Leger Nilai</a></li-->
          </ul>
        </li>
        <li class="
            <?php
              if ($isi == 'pesan_masuk' or $isi == 'pesan_terkirim' or $isi == 'draft' or $isi == 'sms_auto' or $isi == 'schedule') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-envelope"></i> <span>SMS Gateway</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!--li class="
                <?php
                  if ($isi == 'pesan_masuk') {
                    echo "active";
                  }
                  ?>
                "><a href="adm/gateway/inbox"><i class="fa fa-circle-o"></i> Pesan Masuk</a></li>
                <li class="
                <?php
                  if ($isi == 'pesan_terkirim') {
                    echo "active";
                  }
                  ?>
                "><a href="adm/gateway/sent"><i class="fa fa-circle-o"></i> Pesan Terkirim</a></li>
                <li class="
                <?php
                  if ($isi == 'draft') {
                    echo "active";
                  }
                  ?>
                "><a href="adm/gateway/draft"><i class="fa fa-circle-o"></i> Draft</a></li-->
            <li class="
                <?php
                  if ($isi == 'sms_auto') {
                    echo "active";
                  }
                  ?>
                "><a href="adm/gateway/auto"><i class="fa fa-circle-o"></i> Isi Pesan Auto</a></li>
            <li class="
                <?php
                  if ($isi == 'schedule') {
                    echo "active";
                  }
                  ?>
                "><a href="adm/gateway/schedule"><i class="fa fa-circle-o"></i> Jadwal SMS</a></li>
          </ul>
        </li>
        <!--li class="
            <?php
              if ($isi == 'lap_absensi' or $isi == 'lap_keuangan') {
                echo "active";
              }
              ?>
            treeview">
              <a href="assets/panel/#">
                <i class="fa fa-print"></i> <span>Cetak Laporan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="
                <?php
                  if ($isi == 'lap_keuangan') {
                    echo "active";
                  }
                  ?>
                "><a href="#"><i class="fa fa-circle-o"></i> Laporan Data Siswa</a></li>
                <li class="
                <?php
                  if ($isi == 'lap_keuangan') {
                    echo "active";
                  }
                  ?>
                "><a href="#"><i class="fa fa-circle-o"></i> Laporan Data Guru</a></li>
              </ul>
            </li-->
        <li class="
            <?php
              if ($isi == 'sinkron_absen_guru_rekap' or $isi == 'sinkron_kehadiran_guru' or $isi == 'sinkron_kehadiran_non_guru') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-refresh"></i> <span>Sinkronisasi</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'sinkron_absen_guru_rekap') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/sinkron_absen_guru_rekap"><i class="fa fa-circle-o"></i> Sinkron Mengajar Guru</a></li>
            <li class="<?php
                          if ($isi == 'sinkron_kehadiran_guru') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/sinkron_kehadiran_guru"><i class="fa fa-circle-o"></i> Sinkron Kehadiran Guru</a></li>
            <li class="<?php
                          if ($isi == 'sinkron_kehadiran_non_guru') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/sinkron_kehadiran_non_guru"><i class="fa fa-circle-o"></i> Sinkron Kehadiran Non-Guru</a></li>
          </ul>
        </li>
        <li class="header">REFERSENCES</li>
        <li class="
            <?php
              if ($isi == 'tahun_ajaran' or $isi == 'guru' or $isi == 'mapel' or $isi == 'kelas' or $isi == 'siswa') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-group"></i> <span>Administrasi</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'siswa') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/siswa"><i class="fa fa-circle-o"></i> Siswa</a></li>
            <li class="<?php
                          if ($isi == 'guru') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/guru"><i class="fa fa-circle-o"></i> Guru</a></li>
            <li class="<?php
                          if ($isi == 'non guru') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/non_guru"><i class="fa fa-circle-o"></i> Non guru</a></li>
            <li class="<?php
                          if ($isi == 'mapel') {
                            echo "active";
                          }
                          ?>"><a href="adm/mapel"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
            <li class="<?php
                          if ($isi == 'kelas') {
                            echo "active";
                          }
                          ?>"><a href="adm/jenjang"><i class="fa fa-circle-o"></i> Jenjang & Kelas</a></li>
            <li class="<?php
                          if ($isi == 'tahun_ajaran') {
                            echo "active";
                          }
                          ?> "><a href="adm/tahunajaran"><i class="fa fa-circle-o"></i> Tahun Ajaran</a></li>
            <li class="
                    <?php
                      if ($isi == 'kenaikan') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/kenaikan"><i class="fa fa-circle-o"></i> Kenaikan Kelas</a></li>
            <li class="
                    <?php
                      if ($isi == 'jam_masuk') {
                        echo "active";
                      }
                      ?>
                    "><a href="adm/jam_masuk"><i class="fa fa-circle-o"></i> Jam Masuk</a></li>
          </ul>
        </li>
        <!--li class="treeview">
              <a href="assets/panel/#">
                <i class="fa fa-envelope"></i> <span>SMS Gateway</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="assets/panel/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Setup Notifikasi</a></li>
              </ul>
            </li-->
        <li class="
            <?php
              if ($isi == 'akun' or $isi == 'umum' or $isi == 'backup') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-gear"></i> <span>Setting</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'umum') {
                            echo "active";
                          }
                          ?>
                "><a href="panel/umum"><i class="fa fa-circle-o"></i> Umum</a></li>
            <li class="<?php
                          if ($isi == 'akun') {
                            echo "active";
                          }
                          ?>
                "><a href="adm/akun"><i class="fa fa-circle-o"></i> Akun</a></li>
            <li class="<?php
                          if ($isi == 'backup') {
                            echo "active";
                          }
                          ?>
                "><a href="backup"><i class="fa fa-circle-o"></i> Backup & Restore</a></li>
            <!--li
                class="<?php
                          if ($isi == 'sms') {
                            echo "active";
                          }
                          ?>
                "><a href="<?php echo base_url(); ?>gammu/menu.php"><i class="fa fa-circle-o"></i> SMS Gateway</a></li-->
          </ul>
        </li>
      </ul>


    <?php
    } elseif ($level == 'siswa') {
      ?>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="
            <?php
              if ($isi == 'dashboard') {
                echo "active";
              }
              ?>
            treeview">
          <a href="panel/home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="
            <?php
              if ($isi == 'absen' or $isi == 'rekap_absen') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-edit"></i>
            <span>Akademik</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'rekap_absen') {
                            echo "active";
                          }
                          ?>
                "><a href="siswa/rekap_absen"><i class="fa fa-circle-o"></i> Rekap Absen Siswa</a></li>
            <!--li><a href="assets/panel/pages/UI/icons.html"><i class="fa fa-circle-o"></i> Input Nilai</a></li>
                <li><a href="assets/panel/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Kenaikan Kelas</a></li>
                <li><a href="assets/panel/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Leger Nilai</a></li-->
          </ul>
        </li>
      </ul>


    <?php
    } elseif ($level == 'guru') {
      ?>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="
            <?php
              if ($isi == 'dashboard') {
                echo "active";
              }
              ?>
            treeview">
          <a href="panel/home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="
            <?php
              if ($isi == 'absen' or $isi == 'rekap_absen' or $isi == 'rekap_nilai' or $isi == 'siswa') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-edit"></i>
            <span>Wali Kelas</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'siswa') {
                            echo "active";
                          }
                          ?>
                "><a href="guru/siswa"><i class="fa fa-circle-o"></i> Daftar Siswa</a></li>
            <li class="<?php
                          if ($isi == 'absen') {
                            echo "active";
                          }
                          ?>
                "><a href="guru/absen"><i class="fa fa-circle-o"></i> Absensi Kelas</a></li>
            <li class="<?php
                          if ($isi == 'rekap_absen') {
                            echo "active";
                          }
                          ?>
                "><a href="guru/rekap_absen"><i class="fa fa-circle-o"></i> Rekap Absen Kelas</a></li>
            <!--li class="<?php
                            if ($isi == 'rekap_nilai') {
                              echo "active";
                            }
                            ?>
                "><a href=""><i class="fa fa-circle-o"></i> Rekap Nilai Kelas</a></li-->
            <!--li><a href="assets/panel/pages/UI/icons.html"><i class="fa fa-circle-o"></i> Input Nilai</a></li>
                <li><a href="assets/panel/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Kenaikan Kelas</a></li>
                <li><a href="assets/panel/pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Leger Nilai</a></li-->
          </ul>
        </li>
        <li class="
            <?php
              if ($isi == 'absen_siswa' or $isi == 'rekap_siswa') {
                echo "active";
              }
              ?>
            treeview">
          <a href="assets/panel/#">
            <i class="fa fa-book"></i>
            <span>Akademik</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php
                          if ($isi == 'absen_siswa') {
                            echo "active";
                          }
                          ?>
                "><a href="guru/absen_siswa"><i class="fa fa-circle-o"></i> Absensi Siswa</a></li>
            <!--li class="<?php
                            if ($isi == 'rekap_siswa') {
                              echo "active";
                            }
                            ?>
                "><a href="guru/rekap_siswa"><i class="fa fa-circle-o"></i> Rekap Absen Siswa</a></li-->
          </ul>
        </li>
        <!-- ASC Elearning start -->
        <li class="treeview <?php echo ($this->uri->segment(1, '') == 'elearning' ? 'active' : ''); ?>">
          <a href="assets/panel/#">
            <i class="fa fa-graduation-cap"></i>
            <span>Elearning</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="elearning/asc_ujian"><i class="fa fa-circle-o"></i> Ujian</a></li>
            <li class=""><a href="elearning/asc_jadwal_ujian"><i class="fa fa-circle-o"></i> Jadwal Ujian</a></li>
            <li class=""><a href="elearning/asc_grup_siswa"><i class="fa fa-circle-o"></i> Group Siswa</a></li>
            <li class=""><a href="elearning/asc_materi"><i class="fa fa-circle-o"></i> Materi</a></li>
          </ul>
        </li>
        <!-- ASC Elearning end -->
      </ul>

    <?php
    }
    ?>


  </section>
  <!-- /.sidebar -->
</aside>