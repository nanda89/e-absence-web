<?php date_default_timezone_set("Asia/Jakarta");
$s = "select * from tabel_setting where id_setting = 1";
$r = $this->db->query($s)->row();?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="assets/web/images/<?php echo $r->logo_favicon ?>">
    <title><?php echo $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="assets/panel/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/panel/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/panel/dist/ionicons.min.css">
    <link rel="stylesheet" href="assets/panel/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/panel/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/panel/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="assets/panel/plugins/morris/morris.css">
    <link rel="stylesheet" href="assets/panel/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="assets/panel/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="assets/panel/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="assets/panel/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="assets/panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="assets/panel/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="assets/panel/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="assets/panel/plugins/iCheck/all.css">
    <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"> -->
    <link rel="stylesheet" href="assets/panel/timepicker/timepicker.min.css"/>
    <link rel="stylesheet" href="assets/panel/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="assets/panel/x-editable/bootstrap3-editable/css/bootstrap-editable.css"/>
    <style type="text/css">

.bootstrap-timepicker-widget.dropdown-menu {
    z-index: 99999!important;
}

      #loading-status{
        position: fixed;
        top: 50%;
        left: 50%;
        margin: -50px 0px 0px -50px;

          -moz-border-radius: 5px;
          -webkit-border-radius: 5px;
          z-index:3000;
          display:none;
      }


      .disabled-select {
        background-color: #d5d5d5;
        opacity: 0.5;
        border-radius: 3px;
        cursor: not-allowed;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
      }

      select[readonly].select2-hidden-accessible + .select2-container {
        pointer-events: none;
        touch-action: none;
      }

      select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
        background: #eee;
        box-shadow: none;
      }

      select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
      select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
        display: none;
      }

    </style>
    <?php if (isset($stylesheets)): ?>
    <style type="text/css">
      <?php echo $stylesheets; ?>
    </style>
    <?php endif;?>
</head>
<body class="hold-transition skin-green sidebar-mini">
    <div id="loading-status">
     <table>
        <tr>
           <td><img src='<?php echo base_url("assets/panel/ajax-load.gif") ?>' class="img-responsive" style="width: 30%" /></td>
        </tr>
     </table>
    </div>
    <div class="wrapper">
        <header class="main-header">

            <!-- Logo -->
            <a href="panel/home" class="logo">
              <!-- mini logo for sidebar mini 50x50 pixels -->
              <span class="logo-mini"><b><?php echo $r->logo_mini ?></b></span>
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg"><b><?php echo $r->logo_besar ?></b></span>
            </a>

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">

                <!-- Sidebar toggle button-->
                <a href="assets/panel/#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <?php
$id_user = $this->session->userdata('id_user');
$m = $this->m_admin->get_one($id_user);
$ambil = $m->row();
$tgl1 = $ambil->tgl_daftar;
$tgl = date("F Y", strtotime($tgl1));
?>
                        <li class="dropdown user user-menu">
                            <a href="assets/panel/#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo "<img src='assets/panel/icon/" . (($ambil->avatar == null) ? 'siswa-pr2.png' : $ambil->avatar) . "' width='18px' class='img-circle' alt='User Image'>"; ?>
                                <span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>
                            </a>

                            <ul class="dropdown-menu">

                                <!-- User image -->
                                <li class="user-header">
                                    <?php echo "<img src='assets/panel/icon/" . (($ambil->avatar == null) ? 'siswa-pr2.png' : $ambil->avatar) . "' class='img-circle' alt='User Image'>"; ?>
                                    <p>
                                    <?php
echo $name = $this->session->userdata('nama');
echo " <br> ";
echo $name = $this->session->userdata('level');
echo "<small>Registered since $tgl</small>";
?>
                                    </p>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"><a href="panel/profil" class="btn btn-default btn-flat">Profile</a></div>
                                    <div class="pull-right"><a href="panel/logout" class="btn btn-default btn-flat">Sign out</a></div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
      </header>