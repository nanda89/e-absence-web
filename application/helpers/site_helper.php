<?php

if (!function_exists('get_one_rekap_absen_mengajar_guru')) {
    function get_one_rekap_absen_mengajar_guru($params)
    {
        $CI = &get_instance();

        $data = array();
        $id_guru = intval($params['id_guru']);
        $id_jenjang = $params['id_jenjang'];
        $id_ta = $params['id_ta'];
        $tgl_mulai = new \DateTime($params['tgl_mulai']); // format mesti Y-m-d
        $tgl_akhir = new \DateTime($params['tgl_akhir']);
        $tgl_akhir->modify('+1 day');

        $interval = $tgl_mulai->diff($tgl_akhir);
        $jml_hari = intval($interval->format('%R%a'));

        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl_mulai->format('Y-m-d'),
            'end_date' => $tgl_akhir->format('Y-m-d'),
        ));

        if (count($kp) > 0) {
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        $jml_hadir = 0;
        $jml_sakit = 0;
        $jml_izin = 0;
        $jml_nihil = 0; // nihil
        $jml_tanpa_keterangan = 0;

        $list_ket = get_keterangan_absen();
        $guru = $CI->db->query("SELECT * FROM tabel_guru WHERE id_guru=$id_guru")->row();

        $sql = "
      SELECT DISTINCT(tabel_mapel.id_mapel)
      FROM tabel_mapel
      INNER JOIN tabel_jadwal ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
      INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
      INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
      INNER JOIN tabel_tahunajaran ON (tabel_jadwal.id_ta = tabel_tahunajaran.id_ta)
      WHERE tabel_jenjang.id_jenjang = '$id_jenjang' AND tabel_tahunajaran.id_ta = '$id_ta'
    ";

        if ($id_guru != "") {
            $sql .= " AND tabel_guru.id_guru = '$id_guru'";
        }

        $t = $CI->db->query($sql);

        foreach ($t->result() as $mapel) {
            $id_mapel = intval($mapel->id_mapel);

            $begin = new DateTime($params['tgl_mulai']);
            $end = new DateTime($params['tgl_akhir']);
            $end->modify('+1 day');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            // $hari_libur = rtrim($hari_libur, ', ');

            foreach ($period as $dt) {

                $tgltgl = $dt->format("d-m-Y");
                $day = $dt->format("l");
                $harihari = cek_hari($day);
                $harilengkap = hari_lengkap($day);

                if (!in_array($tgltgl, $hari_libur)) {

                    // if ($harihari != substr($row['hari'], 2)) {
                    //     continue;
                    // }
                    // if ($day == "Sunday") continue;

                    // $banyak = mysqli_query($con, "SELECT * FROM tabel_jadwal where id_kelas = '$id_kelas'
                    // and hari = '$harilengkap' and tabel_jadwal.id_mapel = '$id_mapel' and tabel_jadwal.id_guru='$id_guru'");

                    // $cek = mysqli_query($con, "SELECT * FROM tabel_absen_mapel
                    // inner join tabel_jadwal on tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal
                    // WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel'
                    // and tabel_jadwal.id_kelas = '$id_kelas'
                    // AND tabel_absen_mapel.valid = 'valid'  and tabel_jadwal.id_guru='$id_guru'");

                    $banyak = $CI->db->query("
              SELECT * FROM tabel_jadwal
              WHERE hari='$harilengkap'
              AND tabel_jadwal.id_mapel=$id_mapel and tabel_jadwal.id_guru=$id_guru
            ");
                    $jml_nihil += $banyak->num_rows() * 2; // dikali 2 karna dihitug datang dan pulang
                    $cek = $CI->db->query("
              SELECT distinct(tabel_absen_mapel.id_jadwal), tabel_absen_mapel.keterangan, tabel_absen_mapel.jam
              FROM tabel_absen_mapel
              INNER JOIN tabel_jadwal ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal
              WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel'
              AND tabel_absen_mapel.valid = 'valid' and tabel_jadwal.id_guru='$id_guru'
            ");

                    foreach ($cek->result() as $absen) {

                        if (null !== $absen->jam && !empty($absen->jam)) {

                            $jml_hadir++;
                            $jml_nihil--;
                        } else {

                            $keterangan = strtolower(str_replace(' ', '_', $absen->keterangan));
                            if ($keterangan == "sakit") {
                                $jml_sakit++;
                                $jml_nihil--;
                            } elseif ($keterangan == "izin") {
                                $jml_izin++;
                                $jml_nihil--;
                            } else {
                                $jml_tanpa_keterangan++;
                                $jml_nihil--;
                            }
                        }
                    }
                }
            }

            // $mapel = mysqli_query($con, "select * from tabel_mapel where id_mapel = '$id_mapel'");
            $mapel = $CI->db->query("select * from tabel_mapel where id_mapel = '$id_mapel'");
            $mapel = $mapel->row_array();

            // $jenjang = mysqli_query($con, "select * from tabel_jenjang where id_jenjang = '$id_jenjang'");
            $jenjang = $CI->db->query("select * from tabel_jenjang where id_jenjang = '$id_jenjang'");
            $jenjang = $jenjang->row_array();
        }

        $data = (object) array(
            "nama" => $guru->nama,
            "nik" => $guru->nik,
            "jenjang" => (isset($jenjang['jenjang'])) ? $jenjang['jenjang'] : '',
            "mapel" => (isset($mapel['mapel'])) ? $mapel['mapel'] : '',
            "hadir" => $jml_hadir,
            "sakit" => $jml_sakit,
            "izin" => $jml_izin,
            "nihil" => $jml_nihil,
            'tanpa_keterangan' => $jml_tanpa_keterangan,
        );

        return $data;
    }
}

if (!function_exists('get_one_rekap_absen_kehadiran_guru')) {
    function get_one_rekap_absen_kehadiran_guru($params)
    {
        $CI = &get_instance();

        $id_guru = $params['id_guru'];
        $tgl_mulai = new \DateTime($params['tgl_mulai']);
        $tgl_akhir = new \DateTime($params['tgl_akhir']);
        $tgl_akhir->modify('+1 day');

        $interval = $tgl_mulai->diff($tgl_akhir);
        $jml_hari = intval($interval->format('%R%a'));

        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl_mulai->format('Y-m-d'),
            'end_date' => $tgl_akhir->format('Y-m-d'),
        ));

        if (count($kp) > 0) {
            // $jml_hari = ($jml_hari-(count($kp)));
            // $jml_hari = ( $jml_hari > 0 ) ? $jml_hari : 0;
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = $agenda->tanggal; //date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        $jml_absen_nhl_dtg = $jml_hari;
        $jml_absen_nhl_plg = $jml_hari;
        $jml_absen_dtg = 0;
        $jml_absen_plg = 0;
        $jml_absen_sakit = 0;
        $jml_absen_izin = 0;

        $tanggal = new \DateTime($params['tgl_mulai']);

        for ($i = 0; $i < $jml_hari; $i++) {

            if ($tanggal->format('N') == 7) {

                // $jml_absen_dtg--;
                $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                // $jml_absen_plg--;
                $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
            } elseif (in_array($tanggal->format('d-m-Y'), $hari_libur)) {

                // $jml_absen_dtg--;
                $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                // $jml_absen_plg--;
                $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
            } else {

                $sql = "
        SELECT id_absen_kehadiran_guru
        FROM tabel_absen_kehadiran_guru
        WHERE id_guru=" . $id_guru . "
        AND tgl='" . $tanggal->format('d-m-Y') . "'
        ";

                $absensi = $CI->db->query($sql);
                $num_rows = $absensi->num_rows();

                if ($num_rows >= 1) {

                    $jml_absen_dtg++;
                    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                    $jml_absen_plg++;
                    $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
                } elseif ($absensi->num_rows() == 1) {

                    $jml_absen_dtg++;
                    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                }

                if ($num_rows != 0) {
                    $sakit_sql = $sql;
                    $sakit_sql .= " AND keterangan='sakit'";
                    $cek_sakit = $CI->db->query($sakit_sql);
                    if ($cek_sakit->num_rows() > 0) {
                        $jml_absen_sakit++;
                        $jml_absen_dtg = $jml_absen_dtg - 1;
                        //$jml_absen_nhl_dtg++;
                    }
                    $izin_sql = $sql;
                    $izin_sql .= " AND keterangan='izin'";
                    $cek_izin = $CI->db->query($izin_sql);
                    if ($cek_izin->num_rows() > 0) {
                        $jml_absen_izin++;
                        $jml_absen_dtg = $jml_absen_dtg - 1;
                        //$jml_absen_nhl_dtg++;
                    }

                    $tidak_absen_sql = $sql;
                    $tidak_absen_sql .= " AND keterangan='tidak_absen'";
                    $cek_tidak_absen = $CI->db->query($tidak_absen_sql);
                    if ($cek_tidak_absen->num_rows() > 0) {
                        // $jml_absen_izin++;
                        $jml_absen_dtg = $jml_absen_dtg - 1;
                        $jml_absen_nhl_dtg++;
                    }
                }
            }

            $tanggal->add(new \DateInterval('P1D'));
        }

        $data = array(
            'id_guru' => intval($id_guru),
            'absensi' => array(
                'datang' => $jml_absen_dtg,
                'nihil_dtg' => $jml_absen_nhl_dtg, //(($jml_absen_nhl-$jml_absen_dtg)-$jml_absen_plg)*2
                'pulang' => $jml_absen_plg,
                'nihil_plg' => $jml_absen_nhl_plg,
                'sakit' => $jml_absen_sakit,
                'izin' => $jml_absen_izin,
            ),
        );
        return $data;
    }
}

if (!function_exists('get_one_rekap_absen_kehadiran_non_guru')) {
    function get_one_rekap_absen_kehadiran_non_guru($params)
    {
        $CI = &get_instance();

        $id_non_guru = $params['id_non_guru'];
        $tgl_mulai = new \DateTime($params['tgl_mulai']);
        $tgl_akhir = new \DateTime($params['tgl_akhir']);
        $tgl_akhir->modify('+1 day');

        $interval = $tgl_mulai->diff($tgl_akhir);
        $jml_hari = intval($interval->format('%R%a'));

        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl_mulai->format('Y-m-d'),
            'end_date' => $tgl_akhir->format('Y-m-d'),
        ));

        if (count($kp) > 0) {
            // $jml_hari = ($jml_hari-(count($kp)));
            // $jml_hari = ( $jml_hari > 0 ) ? $jml_hari : 0;
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        $jml_absen_nhl_dtg = $jml_hari;
        $jml_absen_nhl_plg = $jml_hari;
        $jml_absen_dtg = 0;
        $jml_absen_plg = 0;
        $jml_absen_sakit = 0;
        $jml_absen_izin = 0;

        $tanggal = new \DateTime($params['tgl_mulai']);

        for ($i = 0; $i < $jml_hari; $i++) {

            if ($tanggal->format('N') == 7) {

                // $jml_absen_dtg--;
                $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                // $jml_absen_plg--;
                $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
            } elseif (in_array($tanggal->format('d-m-Y'), $hari_libur)) {

                // $jml_absen_dtg--;
                $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                // $jml_absen_plg--;
                $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
            } else {

                $sql = "
        SELECT id_absen_non_guru
        FROM tabel_absen_non_guru
        WHERE id_non_guru=" . $id_non_guru . "
        AND tgl='" . $tanggal->format('d-m-Y') . "'
        ";

                $absensi = $CI->db->query($sql);
                $num_rows = $absensi->num_rows();

                if ($num_rows >= 2) {

                    $jml_absen_dtg++;
                    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                    $jml_absen_plg++;
                    $jml_absen_nhl_plg = $jml_absen_nhl_plg - 1;
                } elseif ($absensi->num_rows() == 1) {

                    $jml_absen_dtg++;
                    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg - 1;
                }

                $sakit_sql = $sql;
                $sakit_sql .= " AND keterangan='sakit'";
                $cek_sakit = $CI->db->query($sakit_sql);
                if ($cek_sakit->num_rows() > 0) {
                    $jml_absen_sakit++;
                    $jml_absen_dtg = $jml_absen_dtg - 1;
                }
                $izin_sql = $sql;
                $izin_sql .= " AND keterangan='izin'";
                $cek_izin = $CI->db->query($izin_sql);
                if ($cek_izin->num_rows() > 0) {
                    $jml_absen_izin++;
                    $jml_absen_dtg = $jml_absen_dtg - 1;
                }
            }

            $tanggal->add(new \DateInterval('P1D'));
        }

        $data = array(
            'id_non_guru' => intval($id_non_guru),
            'absensi' => array(
                'datang' => $jml_absen_dtg,
                'nihil_dtg' => $jml_absen_nhl_dtg, //(($jml_absen_nhl-$jml_absen_dtg)-$jml_absen_plg)*2
                'pulang' => $jml_absen_plg,
                'nihil_plg' => $jml_absen_nhl_plg,
                'sakit' => $jml_absen_sakit,
                'izin' => $jml_absen_izin,
            ),
        );
        return $data;
    }
}

if (!function_exists('get_all_harian_absen_mengajar_guru')) {
    function get_all_harian_absen_mengajar_guru($params = array())
    {
        $CI = &get_instance();

        $data = array();
        $jenis = (!empty($params['jenis'])) ? $params['jenis'] : 'Semua';
        $id_guru = (!empty($params['id_guru'])) ? $params['id_guru'] : '';
        $id_mapel = (!empty($params['id_mapel'])) ? $params['id_mapel'] : '';
        $tgl = $params['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
        $tgl_akhir = $params['tgl_akhir'];
        $list_ket = get_keterangan_absen();

        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl,
            'end_date' => $tgl_akhir,
        ));

        if (count($kp) > 0) {
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = $agenda->tanggal; //date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        $sql = "
      SELECT *
      FROM tabel_kelas
      INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas)
      INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
      INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
      INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
    ";
        if ($id_guru != '') {
            $sql .= " where tabel_jadwal.id_guru = '$id_guru'";
        }

        if ($id_mapel != '' && $id_guru == '') {
            $sql .= " where tabel_mapel.id_mapel = '$id_mapel'";
        } else if ($id_mapel != '' && $id_guru != '') {
            $sql .= " and tabel_mapel.id_mapel = '$id_mapel'";
        }

        $sql .= " ORDER BY hari,jam_awal ASC";
        $t_kelas = $CI->db->query($sql);

        $begin = new DateTime($tgl);
        $end = new DateTime($tgl_akhir);
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        if ($jenis == "Semua") {

            foreach ($period as $dt):

                $tgltgl = $dt->format("d-m-Y");
                $harihari = $dt->format("l");

                if (in_array($tgltgl, $hari_libur)) {
                    continue;
                }

                // validasi cuma antara nihil atau present
                // jika present, ada jam absennya
                foreach ($t_kelas->result() as $row) {

                    $jadwal_absen = "$row->jam_awal s/d $row->jam_akhir";
                    $jam_absen = '';
                    $val = 'nihil';
                    $ket = 'tanpa_keterangan';
                    $dif = "";
                    $totHadir = "-";
                    $totTidakHadir = "-";

                    $hari_ = hari_apa($row->hari);

                    if ($harihari != $hari_) {
                        continue;
                    }

                    $cek = $CI->db->query("SELECT * FROM tabel_absen_mapel WHERE tgl = '$tgltgl' AND id_jadwal = '$row->id_jadwal' AND valid = 'valid'");

                    if ($cek->num_rows() > 0) {

                        //menghitung jumlah siswa yang hadir dan tidak hadir
                        $tot = $CI->db->query("SELECT count(*) AS absen FROM tabel_absen_mapel WHERE absen = 'hadir' AND id_jadwal = '$row->id_jadwal' AND tgl ='$tgltgl'
																												            UNION
																												            SELECT count(*) AS absen FROM tabel_absen_mapel WHERE absen <> 'hadir' AND id_jadwal = '$row->id_jadwal' AND tgl = '$tgltgl'");
                        $no = 1;
                        foreach ($tot->result() as $resultTot) {
                            if ($no == 1) {
                                $totHadir = $resultTot->absen;
                            } else {
                                $totTidakHadir = $resultTot->absen;
                            }
                            $no = $no + 1;
                        }

                        $isi = $cek->row();
                        $jam_absen = $isi->jam;
                        $ket = $isi->keterangan;
                        // $val        = ( $isi->absen != 'hadir' ) ? $isi->absen : "present";
                        $val = 'present';

                        $ket_sesuai = ucwords(str_replace('_', ' ', $ket));
                        if ($ket_sesuai == 'Sakit' || $ket_sesuai == 'Izin' || $ket_sesuai == 'Alpha' || $ket_sesuai == 'Tanpa Keterangan') {

                            $val = 'nihil';
                            $ket = $ket_sesuai;
                        } else {

                            $awal = date("H:i", strtotime($row->jam_awal));
                            $deadline = date("H:i", strtotime($isi->jam));
                            $a = datediff($awal, $deadline);
                            $dif = $a['minutes_total'];

                            $re = $CI->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
                            $telat = $re->telat;
                            if ($telat < $dif) {
                                // $val = "<span class='label label-success'><i class='fa fa-check'></i> present</span> $isi->jam";
                                $ket = "terlambat $dif menit";
                            } else {
                                // $val = "<span class='label label-success'><i class='fa fa-check'></i> present</span> $isi->jam";
                                $ket = "";
                            }

                            if ($ket == "") {
                                if ($row->status == "pulang") {
                                    $ket = "absen_sesuai";
                                } else {
                                    if ($dif > $telat) {
                                        $dif = $dif - $telat;
                                        $ket = "terlambat $dif menit";
                                    } else {
                                        $ket = "absen_sesuai";
                                    }

                                }
                            }
                        }

                        $id_absen = $isi->id_absen_mapel;
                    } else {
                        // $data_absen_mapel = array(
                        //   'id_jadwal'   => $row->id_jadwal,
                        //   'tgl'         => $tgltgl,
                        //   'absen'       => $val,
                        //   'valid'       => 'valid',
                        //   'keterangan'  => $ket
                        // );
                        // $CI->db->insert("tabel_absen_mapel",$data_absen_mapel);
                        // $id_absen = $CI->db->insert_id();
                    }

                    $data[] = (object) array(
                        'nama' => $row->nama,
                        'jenjang' => trim($row->jenjang),
                        'kelas' => $row->kelas,
                        'mapel' => $row->mapel,
                        'tgl' => $tgltgl,
                        'jadwal_absen' => $jadwal_absen,
                        'jam_absen' => $jam_absen,
                        'validasi' => $val,
                        'ket' => $ket,
                        'id_jenjang' => $row->id_jenjang,
                        'id_guru' => intval($row->id_guru),
                        'id_jadwal' => $row->id_jadwal,
                        'id_kelas' => $row->id_kelas,
                        'id_mapel' => $row->id_mapel,
                        'hadir' => $totHadir,
                        'tidak_hadir' => $totTidakHadir,
                        'id_absen' => $id_absen,
                        // 'semua'       => $row
                    );
                }

            endforeach;
        } elseif ($jenis == "Terlambat") {

            foreach ($period as $dt) {
                $tgltgl = $dt->format("d-m-Y");
                $hari = $dt->format("l");
                $hariLengkap = hari_lengkap($hari);

                $sqlTerlambat = "SELECT *
        FROM tabel_jadwal
        INNER JOIN tabel_absen_mapel ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal
        WHERE tabel_jadwal.hari = '$hariLengkap'
        AND tabel_absen_mapel.tgl = '$tgltgl'";

                if ($id_guru != '') {
                    $sqlTerlambat .= " AND tabel_jadwal.id_guru = '$id_guru'";
                }

                if ($id_mapel != '') {
                    $sqlTerlambat .= " AND tabel_jadwal.id_mapel = '$id_mapel'";
                }

                $sqlTerlambat .= "GROUP BY tabel_jadwal.id_jadwal";

                $t = $CI->db->query($sqlTerlambat);
                foreach ($t->result() as $row) {
                    $totHadir = "-";
                    $totTidakHadir = "-";

                    //menghitung jumlah siswa yang hadir dan tidak hadir
                    $tot = $CI->db->query("SELECT count(*) AS absen FROM tabel_absen_mapel WHERE absen = 'hadir' AND id_jadwal = '$row->id_jadwal' AND tgl ='$tgltgl'
          UNION
          SELECT count(*) AS absen FROM tabel_absen_mapel WHERE absen <> 'hadir' AND id_jadwal = '$row->id_jadwal' AND tgl ='$tgltgl'");
                    $no = 1;
                    foreach ($tot->result() as $resultTot) {
                        if ($no == 1) {
                            $totHadir = $resultTot->absen;
                        } else {
                            $totTidakHadir = $resultTot->absen;
                        }
                        $no = $no + 1;
                    }

                    $se = $CI->db->query("SELECT * FROM tabel_jadwal INNER JOIN tabel_absen_mapel ON tabel_jadwal.id_jadwal = tabel_absen_mapel.id_jadwal
          WHERE tabel_jadwal.id_jadwal = '$row->id_jadwal'")->row();

                    $jadwal_absen = "$se->jam_awal s/d $se->jam_akhir";
                    $jam_absen = $row->jam;
                    $awal = date("H:i", strtotime($se->jam_awal));
                    $deadline = date("H:i", strtotime($se->jam));
                    $a = datediff($awal, $deadline);
                    $dif = $a['minutes_total'];

                    $re = $CI->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
                    $telat = $re->telat;
                    if ($telat < $dif && $se->keterangan != "offline") {
                        //if($se->num_rows() == 0){

                        $t = $CI->db->query("SELECT * FROM tabel_jadwal
            INNER JOIN tabel_kelas ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas
            INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
            INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
            INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
            WHERE tabel_jadwal.id_jadwal = '$row->id_jadwal'")->row();

                        // $val = "<span class='label label-success'><i class='fa fa-check'></i> present</span> $se->jam";
                        // $ket = "terlambat $dif menit";

                        $ket = $row->keterangan;
                        $val = 'present';
                        $ket_sesuai = ucwords(str_replace('_', ' ', $ket));
                        if ($ket_sesuai == 'Sakit' || $ket_sesuai == 'Izin' || $ket_sesuai == 'Alpha' || $ket_sesuai == 'Tanpa Keterangan') {

                            $val = 'nihil';
                            $ket = $ket_sesuai;
                        } else {

                            $awal = date("H:i", strtotime($row->jam_awal));
                            $deadline = date("H:i", strtotime($isi->jam));
                            $a = datediff($awal, $deadline);
                            $dif = $a['minutes_total'];

                            $re = $CI->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
                            $telat = $re->telat;
                            if ($telat < $dif) {
                                // $val = "<span class='label label-success'><i class='fa fa-check'></i> present</span> $isi->jam";
                                $ket = "terlambat $dif menit";
                            } else {
                                // $val = "<span class='label label-success'><i class='fa fa-check'></i> present</span> $isi->jam";
                                $ket = "";
                            }

                        }

                        if ($t != null) {
                            $data[] = (object) array(
                                'nama' => $t->nama,
                                'jenjang' => $t->jenjang,
                                'kelas' => $t->kelas,
                                'mapel' => $t->mapel,
                                'tgl' => $tgltgl,
                                'jadwal_absen' => $jadwal_absen,
                                'jam_absen' => $jam_absen,
                                'validasi' => $val,
                                'ket' => $ket,
                                'id_jenjang' => $row->id_jenjang,
                                'id_guru' => $row->id_guru,
                                'id_jadwal' => $row->id_jadwal,
                                'id_kelas' => $row->id_kelas,
                                'id_mapel' => $row->id_mapel,
                                'hadir' => $totHadir,
                                'tidak_hadir' => $totTidakHadir,
                            );
                        }
                    }
                }
            }
        } else {

            foreach ($period as $dt) {
                $tgltgl = $dt->format("d-m-Y");
                $hari = $dt->format("l");
                $hariLengkap = hari_lengkap($hari);

                $qJadwal = "SELECT * FROM tabel_jadwal WHERE hari = '$hariLengkap'";
                if ($id_guru != '') {
                    $qJadwal .= " AND id_guru = '$id_guru'";
                }

                if ($id_mapel != '') {
                    $qJadwal .= " AND id_mapel = '$id_mapel'";
                }

                $jadwal = $CI->db->query($qJadwal);

                if ($jadwal->num_rows() == 0) {
                    continue;
                }
                foreach ($jadwal->result() as $dataJadwal) {

                    $qAbsen = "SELECT * FROM tabel_absen_mapel a
          INNER JOIN tabel_jadwal j ON j.id_jadwal = a.id_jadwal
          INNER JOIN tabel_guru g ON g.id_guru = j.id_guru
          INNER JOIN tabel_mapel m ON m.id_mapel = j.id_mapel
          INNER JOIN tabel_jenjang jj ON jj.id_jenjang = j.id_jenjang
          INNER JOIN tabel_kelas k ON k.id_kelas = j.id_kelas
          WHERE a.id_jadwal = '$dataJadwal->id_jadwal' AND a.tgl = '$tgltgl' AND a.valid ='valid'";

                    $totAbsen = $CI->db->query($qAbsen);

                    if ($totAbsen->num_rows() > 0) {
                        continue;
                    }
                    // var_dump("id : ".$dataJadwal->id_jadwal." total : ".$totAbsen->num_rows());
                    $qAbsen1 = "SELECT * FROM  tabel_jadwal j
          INNER JOIN tabel_guru g ON g.id_guru = j.id_guru
          INNER JOIN tabel_mapel m ON m.id_mapel = j.id_mapel
          INNER JOIN tabel_jenjang jj ON jj.id_jenjang = j.id_jenjang
          INNER JOIN tabel_kelas k ON k.id_kelas = j.id_kelas
          WHERE j.id_jadwal = '$dataJadwal->id_jadwal'";
                    // var_dump($qAbsen1);
                    $totAbsen1 = $CI->db->query($qAbsen1);
                    foreach ($totAbsen1->result() as $tk) {

                        $jadwal_absen = "$tk->jam_awal s/d $tk->jam_akhir";

                        $data[] = (object) array(
                            'nama' => $tk->nama,
                            'jenjang' => $tk->jenjang,
                            'kelas' => $tk->kelas,
                            'mapel' => $tk->mapel,
                            'tgl' => $tgltgl,
                            'jadwal_absen' => $jadwal_absen,
                            'jam_absen' => '-',
                            'validasi' => '-',
                            'ket' => '-',
                            'id_jenjang' => $tk->id_jenjang,
                            'id_guru' => $tk->id_guru,
                            'id_jadwal' => $tk->id_jadwal,
                            'id_kelas' => $tk->id_kelas,
                            'id_mapel' => $tk->id_mapel,
                            'hadir' => '-',
                            'tidak_hadir' => '-',
                        );

                    }
                }

            }
        }

        return $data;
    }
}

if (!function_exists('get_all_harian_absen_kehadiran_guru')) {
    function get_all_harian_absen_kehadiran_guru($params = array())
    {
        $CI = &get_instance();

        $data = array();
        $tgl = $params['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
        $tgl_akhir = $params['tgl_akhir'];
        $id_guru = $params['id_guru'];
        $jenis = $params['jenis'];

        $re = $CI->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
        $jam_pulang_kerja = $re->jam_pulang;

        $begin = new DateTime($tgl);
        $end = new DateTime($tgl_akhir);
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        //apakah tanggal dalam period ada yang termasuk hari libur,jika ada masukan kedalam array $hari_libur
        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl,
            'end_date' => $tgl_akhir,
        ));

        if (count($kp) > 0) {
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        //nilai = 1
        $status_absen_pulang = $re->status_absen_pulang;

        foreach ($period as $dt) {
            $tgl = $dt->format("d-m-Y");

            //apakah tanggal ini termasuk hari libur ?
            if (in_array($tgl, $hari_libur)) {
                continue;
            }

            //apakah tanggal ini hari minggu ?
            if ($dt->format('N') == 7) {
                continue;
            }

            //ubah tanggal kedalam bentuk hari
            $harihari = cek_hari($dt->format("l"));

            //mengambil jam masuk berdasarkan hari
            $jam_masuk = $CI->db->query("SELECT * FROM tabel_jam_masuk WHERE SUBSTRING(tabel_jam_masuk.hari, 3) = '$harihari'")->row();
            $jam_masuk_kerja = $jam_masuk->jam;

            //mengambil data guru
            $qGuru = "SELECT * FROM tabel_guru";
            if ($id_guru != '') {
                $qGuru .= " WHERE id_guru = '$id_guru'";
            }
            $t = $CI->db->query($qGuru);

            //looping untuk setiap data guru
            foreach ($t->result() as $row) {
                $cek = $CI->db->query("SELECT * FROM tabel_absen_kehadiran_guru
            WHERE id_guru = '$row->id_guru' AND tgl = '$tgl' order by jam");

                if ($status_absen_pulang == "1") {
                    if ($cek->num_rows() == 0) {
                        //guru tidak hadir pada hari ini
                        $validasi_jam_masuk = "nihil";
                        $validasi_jam_pulang = "nihil";
                        $keterangan_jam_masuk = "tidak absen";
                        $keterangan_jam_pulang = "tidak absen";
                    } else if ($cek->num_rows() == 1) {
                        //guru sudah absen hadir
                        $validasi_jam_masuk = $cek->row()->jam;
                        $validasi_jam_pulang = "nihil";
                        $keterangan_jam_masuk = $cek->row()->keterangan;
                        $keterangan_jam_pulang = "tidak absen";

                        if (strtolower($keterangan_jam_masuk) == 'izin' || strtolower($keterangan_jam_masuk) == 'sakit') {

                            //jika keteranga izin atau sakit maka jam masuk diubah menjadi nihil
                            $validasi_jam_masuk = 'nihil';
                        } else {
                            $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                            $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                            $keterangan_jam_masuk = "";
                            if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                                $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                            } else {
                                $keterangan_jam_masuk .= "absen sesuai - ";
                            }
                            // $keterangan_jam_masuk .= " ";
                            // $keterangan_jam_masuk .= $cek->row()->keterangan;
                            if (!empty($cek->row()->keterangan)) {
                                $keterangan_jam_masuk = $cek->row()->keterangan;
                            }
                        }

                        // $keterangan_jam_pulang = "tidak absen";
                    } else {
                        //guru sudah absen hadir dan pulang
                        $validasi_jam_masuk = $cek->row()->jam;
                        $keterangan_jam_masuk = $cek->row()->keterangan;

                        // $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                        // $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                        // $keterangan_jam_masuk = "";
                        // if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                        //     $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                        // } else {
                        //     $keterangan_jam_masuk .= "absen sesuai - ";
                        // }

                        $cek = $CI->db->query("select * from tabel_absen_kehadiran_guru
                where id_guru = '$row->id_guru' and tgl = '$tgl' order by jam desc");
                        $validasi_jam_pulang = $cek->row()->jam;
                        $keterangan_jam_pulang = $cek->row()->keterangan;

                        // $sampai = "17:10";
                        // if (strtotime($validasi_jam_pulang) <= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
                        //     $jam_pulang_diff = datediff($validasi_jam_pulang, $jam_pulang_kerja);
                        //     $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
                        //     $keterangan_jam_pulang = "";
                        //     if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
                        //         // $keterangan_jam_pulang .= "lebih cepat $jam_pulang_diff menit -";
                        //         $keterangan_jam_pulang .= $cek->row()->keterangan;

                        //     } else {
                        //         $keterangan_jam_pulang .= "absen sesuai - ";
                        //     }
                        // } else {
                        //     $validasi_jam_pulang = $cek->row()->jam;
                        //     $keterangan_jam_pulang = $cek->row()->keterangan;

                        // }
                    }
                } else {
                    if ($cek->num_rows() == 0) {
                        $validasi_jam_masuk = "nihil";
                        $keterangan_jam_masuk = "tidak absen";
                    } else if ($cek->num_rows() == 1) {
                        $validasi_jam_masuk = $cek->row()->jam;
                        $keterangan_jam_masuk = $cek->row()->keterangan;

                        if (strtolower($keterangan_jam_masuk) == 'izin' || strtolower($keterangan_jam_masuk) == 'sakit') {
                            $validasi_jam_masuk = 'nihil';
                        } else {

                            $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                            $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                            $keterangan_jam_masuk = "";
                            if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                                $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                            } else {
                                $keterangan_jam_masuk .= "absen sesuai - ";
                            }
                            $keterangan_jam_masuk .= " ";
                            $keterangan_jam_masuk .= $cek->row()->keterangan;
                        }
                    } else {
                        $validasi_jam_masuk = $cek->row()->jam;

                        $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                        $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                        $keterangan_jam_masuk = "";
                        if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                            $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                        } else {
                            $keterangan_jam_masuk .= "absen sesuai - ";
                        }
                        // $keterangan_jam_masuk .= " ";
                        // $keterangan_jam_masuk .= $cek->row()->keterangan;
                        if (!empty($cek->row()->keterangan)) {
                            $keterangan_jam_masuk = $cek->row()->keterangan;
                        }
                    }
                    // echo $row->id_guru . " " . $harihari;
                    $absenGuru = $CI->db->query("select * from tabel_jadwal where id_guru = '$row->id_guru' and substring(hari, 3) = '$harihari' order by jam_awal desc");

                    if ($absenGuru->num_rows() > 0) {

                        $jadwal = $absenGuru->row();

                        $absenMapel = $CI->db->query("select * from tabel_absen_mapel where tgl = '$tgl' and id_jadwal='$jadwal->id_jadwal'");

                        if ($absenMapel->num_rows() == 0) {
                            $validasi_jam_pulang = "nihil";
                            $keterangan_jam_pulang = "tidak absen";
                        } else {
                            $validasi_jam_pulang = $absenMapel->row()->jam;

                            // echo $validasi_jam_pulang;

                            $sampai = "17:00";
                            if (strtotime($validasi_jam_pulang) >= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
                                $jam_pulang_diff = datediff($jam_pulang_kerja, $validasi_jam_pulang);
                                $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
                                $keterangan_jam_pulang = "";
                                if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
                                    $keterangan_jam_pulang .= "terlambat $jam_pulang_diff menit -";
                                } else {
                                    $keterangan_jam_pulang .= "absen sesuai - ";
                                }
                                $keterangan_jam_pulang .= " ";
                                $keterangan_jam_pulang .= $cek->row()->keterangan;
                            } else {
                                $validasi_jam_pulang = "nihil";
                                $keterangan_jam_pulang = "tidak absen";
                            }
                        }
                    } else {
                        $validasi_jam_pulang = "nihil";
                        $keterangan_jam_pulang = "tidak absen";
                    }
                }

                $data[] = (object) array(
                    'id_guru' => $row->id_guru,
                    'nama' => $row->nama,
                    'tgl' => $tgl,
                    'jam_masuk_kerja' => $jam_masuk_kerja,
                    'jam_pulang_kerja' => $jam_pulang_kerja,
                    'validasi_jam_masuk' => (is_null($validasi_jam_masuk)) ? "nihil" : $validasi_jam_masuk,
                    'validasi_jam_pulang' => $validasi_jam_pulang,
                    'keterangan_jam_masuk' => $keterangan_jam_masuk,
                    'keterangan_jam_pulang' => $keterangan_jam_pulang,
                );
            }
        }
        // var_dump($data);
        return $data;
    }
}

if (!function_exists('get_all_harian_absen_kehadiran_non_guru')) {
    function get_all_harian_absen_kehadiran_non_guru($params = array())
    {
        $CI = &get_instance();

        $data = array();
        $tgl = $params['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
        $tgl_akhir = $params['tgl_akhir'];
        $nonGuru = !empty($params['nonGuru']) ? $params['nonGuru'] : '';

        $re = $CI->db->query("SELECT * FROM tabel_setting WHERE id_setting = '1'")->row();
        $jam_pulang_kerja = $re->jam_pulang;
        $begin = new DateTime($tgl);
        $end = new DateTime($tgl_akhir);
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $hari_libur = [];
        $kp = get_kalender_pendidikan(array(
            'start_date' => $tgl,
            'end_date' => $tgl_akhir,
        ));

        if (count($kp) > 0) {
            foreach ($kp as $agenda) {
                if ($agenda->libur == 1) {
                    $hari_libur[] = date('d-m-Y', strtotime($agenda->tanggal));
                }
            }
        }

        foreach ($period as $dt) {
            $tgl = $dt->format("d-m-Y");

            if (in_array($tgl, $hari_libur)) {
                continue;
            }

            $harihari = cek_hari($dt->format("l"));

            $jam_masuk = $CI->db->query("select * from tabel_jam_masuk where SUBSTRING(tabel_jam_masuk.hari, 3) = '$harihari'")->row();
            $jam_masuk_kerja = $jam_masuk->jam;

            $qNonGuru = "select * from tabel_non_guru";
            if ($nonGuru != '') {
                $qNonGuru .= " where id_non_guru = '$nonGuru'";
            }
            $t = $CI->db->query($qNonGuru);

            foreach ($t->result() as $row) {
                $cek = $CI->db->query("select * from tabel_absen_non_guru
        where id_non_guru = '$row->id_non_guru' and tgl = '$tgl' order by jam");

                if ($cek->num_rows() == 0) {
                    $validasi_jam_masuk = "nihil";
                    $validasi_jam_pulang = "nihil";
                    $keterangan_jam_masuk = "tidak absen";
                    $keterangan_jam_pulang = "tidak absen";
                } else if ($cek->num_rows() == 1) {
                    $validasi_jam_masuk = $cek->row()->jam;
                    $validasi_jam_pulang = "nihil";

                    $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                    $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                    $keterangan_jam_masuk = "";
                    if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                    } else {
                        $keterangan_jam_masuk .= "absen sesuai - ";
                    }
                    // $keterangan_jam_masuk .= " ";
                    // $keterangan_jam_masuk .= $cek->row()->keterangan;
                    if (!empty($cek->row()->keterangan)) {
                        $keterangan_jam_masuk = $cek->row()->keterangan;
                    }

                    $keterangan_jam_pulang = "tidak absen";
                } else {
                    $validasi_jam_masuk = $cek->row()->jam;

                    $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                    $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                    $keterangan_jam_masuk = "";
                    if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                    } else {
                        $keterangan_jam_masuk .= "absen sesuai - ";
                    }
                    // $keterangan_jam_masuk .= " ";
                    // $keterangan_jam_masuk .= $cek->row()->keterangan;
                    if (!empty($cek->row()->keterangan)) {
                        $keterangan_jam_masuk = $cek->row()->keterangan;
                    }

                    $cek = $CI->db->query("select * from tabel_absen_non_guru
          where id_non_guru = '$row->id_non_guru' and tgl = '$tgl' order by jam desc");
                    $validasi_jam_pulang = $cek->row()->jam;
                    $sampai = "17:00";
                    // var_dump("(".$validasi_jam_pulang." >= ".$jam_pulang_kerja.")&&(".$validasi_jam_pulang." <= ".$sampai);
                    if (strtotime($validasi_jam_pulang) >= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
                        $jam_pulang_diff = datediff($jam_pulang_kerja, $validasi_jam_pulang);
                        $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
                        $keterangan_jam_pulang = "";
                        if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
                            $keterangan_jam_pulang .= "terlambat $jam_pulang_diff menit -";
                        } else {
                            $keterangan_jam_pulang .= "absen sesuai - ";
                        }
                        // $keterangan_jam_masuk .= " ";
                        // $keterangan_jam_masuk .= $cek->row()->keterangan;
                        if (!empty($cek->row()->keterangan)) {
                            $keterangan_jam_masuk = $cek->row()->keterangan;
                        }
                    } else {
                        $validasi_jam_pulang = "nihil";
                        $keterangan_jam_pulang = "tidak absen";
                    }
                }

                $data[] = (object) array(
                    'id_non_guru' => $row->id_non_guru,
                    'nama' => $row->nama,
                    'tgl' => $tgl,
                    'jam_masuk_kerja' => $jam_masuk_kerja,
                    'jam_pulang_kerja' => $jam_pulang_kerja,
                    'validasi_jam_masuk' => (!empty(trim($validasi_jam_masuk))) ? $validasi_jam_masuk : "nihil",
                    'validasi_jam_pulang' => $validasi_jam_pulang,
                    'keterangan_jam_masuk' => $keterangan_jam_masuk,
                    'keterangan_jam_pulang' => $keterangan_jam_pulang,
                );
            }
        }
        // var_dump($data);
        return $data;
    }
}
if (!function_exists('get_kegiatan')) {
    function get_kegiatan($params = array())
    {
        $CI = &get_instance();
        $data = array();

        $tgl = $params['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
        $tgl_akhir = $params['tgl_akhir'];
        $nonGuru = !empty($params['nonGuru']) ? $params['nonGuru'] : '';

        $q = "SELECT t.*,n.nama,n.jabatan FROM tabel_kegiatan_non_guru t
    JOIN tabel_non_guru n
    ON t.id_non_guru = n.id_non_guru AND t.tanggal BETWEEN '$tgl' AND '$tgl_akhir'";
        if ($nonGuru != '') {
            $q .= " AND t.id_non_guru = '$nonGuru'";
        }

        $q .= " ORDER BY id_kegiatan DESC";
        $kegiatan = $CI->db->query($q);

        if ($kegiatan->num_rows() > 0) {
            foreach ($kegiatan->result() as $row) {

                $data[] = (object) array(
                    'id_non_guru' => intval($row->id_non_guru),
                    'tanggal' => $row->tanggal,
                    'jam' => $row->jam,
                    'type' => $row->type_file,
                    'uraian' => $row->uraian,
                    'img' => $row->file,
                    'nama' => $row->nama,
                    'jabatan' => $row->jabatan,
                );
            }
        }

        return $data;
    }
}

if (!function_exists('get_keterangan_absen')) {
    function get_keterangan_absen()
    {
        return array(
            'izin' => 'izin',
            'sakit' => 'sakit',
            'tanpa_keterangan' => 'tanpa keterangan',
            'absen_sesuai' => 'absen sesuai',
            'tidak_absen' => 'tidak absen',
        );
    }
}

if (!function_exists('get_kalender_pendidikan')) {
    function get_kalender_pendidikan($args = array())
    {

        $CI = &get_instance();
        $data = array();

        $date = new \DateTime($args['start_date']); // format standard
        $end = new \DateTime($args['end_date']); // format standard
        $interval = $date->diff($end);
        $count = intval($interval->format('%R%a'));
        $result = [];
        $tmp_result = [];
        if ($count > 0) {
            for ($i = 0; $i <= $count; $i++) {

                $tmp_data = array(
                    'tanggal' => $date->format('Y-m-d'),

                );
                $agenda = $CI->db->query("SELECT * FROM tabel_kalender WHERE tanggal='{$date->format('d-m-Y')}'");
                if ($agenda->num_rows() > 0) {

                    //$tmp_data['agenda'] = $agenda->result_array();
                    // $tmp_data['agenda']     = $agenda->row()->agenda;
                    // $tmp_data['warna']      = $agenda->row()->warna;
                    // $tmp_data['keterangan'] = $agenda->row()->keterangan;
                    // $tmp_data['libur']      = $agenda->row()->libur;
                    $data[] = $agenda->row();
                }

                // $bulan_key = $date->format('n');

                $date->add(new \DateInterval('P1D'));
            }
        }

        return $data;
    }
}

if (!function_exists('get_nama_bulan')) {
    function get_nama_bulan()
    {
        return array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
    }
}

if (!function_exists('cek_hari')) {
    function cek_hari($tgl)
    {
        $hari = $day = $tgl;
        switch ($hari) {
            case "Sunday":
                $hari = "Minggu";
                break;
            case "Monday":
                $hari = "Senin";
                break;
            case "Tuesday":
                $hari = "Selasa";
                break;
            case "Wednesday":
                $hari = "Rabu";
                break;
            case "Thursday":
                $hari = "Kamis";
                break;
            case "Friday":
                $hari = "Jumat";
                break;
            case "Saturday":
                $hari = "Sabtu";
                break;
        }
        return $hari;
    }
}

if (!function_exists('hari')) {
    function hari()
    {
        $hari = $day = gmdate("l", time() + 60 * 60 * 7);
        switch ($hari) {
            case "Sunday":
                $hari = "Minggu";
                break;
            case "Monday":
                $hari = "Senin";
                break;
            case "Tuesday":
                $hari = "Selasa";
                break;
            case "Wednesday":
                $hari = "Rabu";
                break;
            case "Thursday":
                $hari = "Kamis";
                break;
            case "Friday":
                $hari = "Jumat";
                break;
            case "Saturday":
                $hari = "Sabtu";
                break;
        }
        return $hari;
    }
}

if (!function_exists('hari_apa')) {
    function hari_apa($s)
    {
        if ($s == "0 Minggu") {
            return "Sunday";
        } else if ($s == "1 Senin") {
            return "Monday";
        } else if ($s == "2 Selasa") {
            return "Tuesday";
        } else if ($s == "3 Rabu") {
            return "Wednesday";
        } else if ($s == "4 Kamis") {
            return "Thursday";
        } else if ($s == "5 Jumat") {
            return "Friday";
        } else if ($s == "6 Sabtu") {
            return "Saturday";
        }
    }
}

if (!function_exists('datediff')) {
    function datediff($tgl1, $tgl2)
    {
        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        return array("years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff));
    }
}

if (!function_exists('hari_lengkap')) {
    function hari_lengkap($tgl)
    {
        $hari = $day = $tgl;
        switch ($hari) {
            case "Sunday":
                $hari = "0 Minggu";
                break;
            case "Monday":
                $hari = "1 Senin";
                break;
            case "Tuesday":
                $hari = "2 Selasa";
                break;
            case "Wednesday":
                $hari = "3 Rabu";
                break;
            case "Thursday":
                $hari = "4 Kamis";
                break;
            case "Friday":
                $hari = "5 Jumat";
                break;
            case "Saturday":
                $hari = "6 Sabtu";
                break;
        }
        return $hari;
    }
}

if (!function_exists('convert_hari')) {
    function convert_hari($hari)
    {
        if ($hari == 'Sun') {
            return "Minggu";
        } elseif ($hari == 'Mon') {
            return "Senin";
        } elseif ($hari == 'Tue') {
            return "Selasa";
        } elseif ($hari == 'Wed') {
            return "Rabu";
        } elseif ($hari == 'Thu') {
            return "Kamis";
        } elseif ($hari == 'Fri') {
            return "Jumat";
        } elseif ($hari == 'Sat') {
            return "Sabtu";
        }
    }
}

if (!function_exists('__autoload')) {
    function __autoload($class)
    { //die($class);
        if (substr($class, 0, 3) !== 'MY_') {
            if (file_exists($file = APPPATH . 'core/' . $class . '.php')) {
                include $file;
            }
        }
    }
}

if (!function_exists('debugji')) {
    function debugji($var, $exit = false)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        if ($exit) {
            exit;
        }

    }
}
