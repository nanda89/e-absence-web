<?php
function datediff($tgl1, $tgl2){
    $tgl1 = strtotime($tgl1);
    $tgl2 = strtotime($tgl2);
    $diff_secs = abs($tgl1 - $tgl2);
    $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
    return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
  }
function hari_apa($s) {
    if ($s == "0 Minggu") {
      return "Sunday";
    }
    else if ($s == "1 Senin") {
      return "Monday";
    }
    else if ($s == "2 Selasa") {
      return "Tuesday";
    }
    else if ($s == "3 Rabu") {
      return "Wednesday";
    }
    else if ($s == "4 Kamis") {
      return "Thursday";
    }
    else if ($s == "5 Jumat") {
      return "Friday";
    }
    else if ($s == "6 Sabtu") {
      return "Saturday";
    }
  }
  function getDay($tgl){
    $timestamp = strtotime($tgl);
    $day = date('D', $timestamp);
    if ($day == "Sun") {
        return "Minggu";
      }
      else if ($day == "Mon") {
        return "Senin";
      }
      else if ($day == "Tue") {
        return "Selasa";
      }
      else if ($day == "Wed") {
        return "Rabu";
      }
      else if ($day == "Thu") {
        return "Kamis";
      }
      else if ($day == "Fri") {
        return "Jumat";
      }
      else if ($day == "Sat") {
        return "Sabtu";
      }
  }
function get_keterangan_absen() {
    return array(
      'izin'            => 'izin',
      'sakit'           => 'sakit',
      'tanpa_keterangan'=> 'tanpa keterangan',
      'absen_sesuai'    => 'absen sesuai',
      'tidak_absen'    => 'tidak absen',
    );
  }
  function getKalenderPendidikan($con,$args){
    $data = array();
    $date = new DateTime($args['start_date']); // format standard
	$end = new DateTime($args['end_date']); // format standard
	$interval = $date->diff($end);
	$count = intval( $interval->format('%R%a') );
	$result = [];
    $tmp_result = [];
    if ( $count > 0 ) {
        for ($i=0; $i <= $count; $i++) { 

            $tmp_data = array(
                'tanggal' => $date->format('Y-m-d'),
            );

            $agenda = mysqli_query($con,"SELECT * FROM tabel_kalender WHERE tanggal='{$date->format('d-m-Y')}'");
            $agenda = mysqli_fetch_array($agenda);
            if( count($agenda) > 0 ) {
                $data[] = array(
                    "id_kalender"       =>$agenda['id_kalender'],
                    "tanggal"           =>$agenda['tanggal'],
                    "warna"             =>$agenda['warna'],
                    "agenda"            =>$agenda['agenda'],
                    "keterangan"        =>$agenda['keterangan'],
                    "libur"             =>$agenda['libur']
                );
            }
            $date->add( new DateInterval('P1D') );
   
        }
    }
    return $data;
}
include('koneksi.php');
  
$data = array();
$jenis = ( !empty( $_GET['jenis'] ) ) ? $_GET['jenis'] : 'Semua';
$id_guru= ( !empty( $_GET['id_guru'] ) ) ? $_GET['id_guru'] : '';
$tgl = $_GET['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
$tgl_akhir = $_GET['tgl_akhir'];
$mapel = $_GET['mapel'];
$list_ket = get_keterangan_absen();
  
$hari_libur = [];
$kp = getKalenderPendidikan($con,array(
  'start_date'	=> $tgl,
  'end_date'		=> $tgl_akhir
));
  
if( count( $kp ) > 0 ) {
  foreach( $kp as $agenda ) {
    if ( $agenda['libur'] == 1 ) {
      $hari_libur[] = $agenda['tanggal']; //date('d-m-Y', strtotime($agenda->tanggal));
    }   
  }
}
  
$sql = "SELECT * FROM tabel_kelas 
    INNER JOIN tabel_jadwal ON (tabel_kelas.id_kelas = tabel_jadwal.id_kelas) 
    INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) 
    INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru) 
    INNER JOIN tabel_mapel ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
    INNER JOIN tabel_tahunajaran ON (tabel_jadwal.id_ta = tabel_tahunajaran.id_ta)";
    // WHERE tabel_mapel.mapel ='$mapel'"

if ($id_guru != ''){
    $sql .= " WHERE tabel_jadwal.id_guru = '$id_guru' OR tabel_guru.nik = '$id_guru'";
} 
    
$sql .= " ORDER BY hari,jam_awal ASC";
// $t_kelas = mysqli_query($con,$sql);
$begin = new DateTime($tgl);
$end = new DateTime($tgl_akhir);
$end->modify('+1 day');

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);
  
if( $jenis == "Semua" ) {

    foreach ($period as $dt) :
        
        $tgltgl = $dt->format("d-m-Y");
        $harihari = $dt->format("l");
        if( in_array( $tgltgl, $hari_libur ) ) 
          continue;

        $t_kelas = mysqli_query($con,$sql);
        while($row = mysqli_fetch_array($t_kelas)){

            if($mapel != ''){
                if($row['mapel'] != $mapel){
                    continue;
                }
            }
            

            $awal           = $row['jam_awal'];
            $akhir          = $row['jam_akhir'];
            $jadwal_absen   = "$awal s/d $akhir";
            $jam_absen      = '';
            $val            = 'nihil';
            $ket            = 'tanpa_keterangan';
            $dif            = "";

            $hari_ = hari_apa($row['hari']);

            if ($harihari != $hari_) 
              continue;

            $idJadwal = $row['id_jadwal'];
            $q = "SELECT * FROM tabel_absen_mapel WHERE tgl = '$tgltgl' AND id_jadwal = '$idJadwal' AND valid = 'valid'";
           
            $cek = mysqli_query($con,$q);
            $cek = mysqli_fetch_array($cek);
            if( count($cek) > 1 ) {
              
                $jam_absen  = $cek['jam'];
                $ket        = $cek['keterangan'];
                $val        = 'present';
              
                $ket_sesuai = ucwords( str_replace('_', ' ', $ket) );
                if( $ket_sesuai == 'Sakit' || $ket_sesuai == 'Izin' || $ket_sesuai == 'Alpha' || $ket_sesuai == 'Tanpa Keterangan' ) {
                  
                  $val = 'nihil';
                  $ket = $ket_sesuai;
                  
                } else {
                  
                  $awal     = date("H:i", strtotime($row['jam_awal']));
                  $deadline = date("H:i", strtotime($cek['jam']));              
                  $a        = datediff($awal,$deadline);      
                  $dif      = $a['minutes_total']; 

                  $re  = mysqli_query($con,"SELECT * FROM tabel_setting WHERE id_setting = '1'");
                  $re = mysqli_fetch_array($re);
                  $telat = $re['telat'];
                  if($telat < $dif){
                      $ket = "terlambat $dif menit";
                  }else{
                      $ket = "";
                  }
                 
                  if ( $ket == "" ) {
                      if ($row['status'] == "pulang") {
                        $ket = "absen_sesuai";  
                      } else {
                          if ($dif > $telat) {
                              $dif = $dif - $telat;
                              $ket = "terlambat $dif menit";
                          }
                          else $ket = "absen_sesuai";
                      }
                  }
                  
                }
              
                $id_absen = $cek['id_absen_mapel'];
              
            }

            $data[] = (object) array(
              'nama'        => $row['nama'],
              'jenjang'     => trim($row['jenjang']),
              'kelas'       => $row['kelas'],
              'mapel'       => $row['mapel'],
              'tgl'         => $tgltgl,
              'jadwal_absen'=> $jadwal_absen,
              'jam_absen'   => $jam_absen,
              'validasi'    => $val,
              'ket'         => $ket,
              'id_jenjang'  => intval($row['id_jenjang']),
              'id_guru'     => intval($row['id_guru']),
              'id_jadwal'   => intval($row['id_jadwal']),
              'id_kelas'    => intval($row['id_kelas']),
              'id_mapel'    => intval($row['id_mapel']),
              'tahun_ajaran'    => $row['tahun_ajaran'],
            );
      }
    endforeach;
}
  
  echo json_encode($data);
?>