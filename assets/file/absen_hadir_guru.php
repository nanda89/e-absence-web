<?php 
    include('koneksi.php');

    $id_guru = $_GET['id_guru'];
    $tgl        = gmdate("d-m-Y", time()+60*60*7);    
    $jam        = gmdate("H:i", time()+60*60*7);

    $hari = date("l");
if ($hari == "Sunday") $hari = "Minggu";
else if ($hari == "Monday") $hari = "Senin";
else if ($hari == "Tuesday") $hari = "Selasa";
else if ($hari == "Wednesday") $hari = "Rabu";
else if ($hari == "Thursday") $hari = "Kamis";
else if ($hari == "Friday") $hari = "Jumat";
else if ($hari == "Saturday") $hari = "Sabtu";

$cek = mysqli_query($con,"SELECT * from tabel_absen_kehadiran_guru where id_guru = '$id_guru' and tgl = '$tgl' and keterangan not like '%pulang%'");
$c=mysqli_num_rows($cek);

$setting = mysqli_query($con,"SELECT * from tabel_setting");
$setting = mysqli_fetch_array($setting);
$jam_pulang_kerja = $setting['jam_pulang'];

$jam_masuk = mysqli_query($con, "select * from tabel_jam_masuk where substring(hari, 3) = '$hari'");
$jam_masuk = mysqli_fetch_array($jam_masuk);
$jam_masuk_kerja = $jam_masuk['jam'];

$hari_libur = mysqli_query($con, "select * from tabel_kalender where libur = 1 and tanggal = '$tgl'");
$libur=mysqli_num_rows($hari_libur);

/**pengecekan apakah hari ini hari libur */
if ($libur == 0) {
/**pengecekan apakah sudah absen masuk */
    if ($c == 0) {
        $sampai = strtotime($jam_pulang_kerja) - 1800;
        $sampai = date('H:i', $sampai);
        if (strtotime($jam) <= strtotime($sampai)) {
            
            $jam_masuk_diff = ceil((strtotime($jam) - strtotime($jam_masuk_kerja)) / 60);
            $keterangan_jam_masuk = "";
            if ($jam_masuk_diff > 0) {
                $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit";
            }
            else {
                $keterangan_jam_masuk .= "absen sesuai";
            }
            // echo $id_guru." tanggal ".$tgl;
            mysqli_query($con,"INSERT INTO tabel_absen_kehadiran_guru(id_guru,tgl,jam,keterangan) VALUES ('$id_guru','$tgl','$jam','$keterangan_jam_masuk')");

            echo json_encode(array("status"=>"ok", "keterangan"=>$keterangan_jam_masuk));
        }
        else {
            echo json_encode(array("status"=>"error", "keterangan"=>"tidak bisa absen masuk lagi"));
        }
    }
    else {
        echo json_encode(array("status"=>"error", "keterangan"=>"hari ini anda sudah absen"));
    }
}else{
    echo json_encode(array("status"=>"error", "keterangan"=>"Absen gagal, hari ini hari libur"));
}

?>