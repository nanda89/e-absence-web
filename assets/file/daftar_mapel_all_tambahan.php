<?php 
function hari(){
  $hari=$day=gmdate("l", time()+60*60*7);		
  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }      
  $hariLengkap="$hari";
  return $hariLengkap;
}


include('koneksi.php');
$getguru 	= $_GET['idguru'];
$tgl 			= gmdate("d-m-Y", time()+60*60*7);
$jam  		= gmdate("H:i", time()+60*60*7);
$hari 		= hari();

$thisTame = strtotime($tgl);
$newDate = date('d-m-Y',$thisTame);
$firstOfMonth = date("01-m-Y", strtotime($tgl));
$currentWeek = intval(date("W", strtotime($tgl))) - intval(date("W", strtotime($firstOfMonth))) + 1;
if($currentWeek < 0){
  $currentWeek = 6;
}

$queryGuru = mysqli_query($con,"select * from tabel_guru where nik = '$getguru'");
$queryGuru = mysqli_fetch_array($queryGuru,MYSQLI_ASSOC);
$statusGuruBK = $queryGuru['status_guru_bk'];


if ($statusGuruBK == "1") 
{
  $queryKelas="SELECT 
  -- DISTINCT(tabel_kelas_tambahan.id_kelas_tambahan),
  tabel_jenjang.jenjang,tabel_mapel.mapel, tabel_jadwal_tambahan.nama_kelas,
  tabel_jadwal_tambahan.jam_awal, tabel_jadwal_tambahan.jam_akhir,tabel_jadwal_tambahan.id_jadwal_tambahan , tabel_guru.nama
  FROM tabel_jadwal_tambahan 
  INNER JOIN tabel_jenjang ON tabel_jadwal_tambahan.id_jenjang = tabel_jenjang.id_jenjang 
  -- LEFT JOIN tabel_kelas_tambahan ON  tabel_kelas_tambahan.id_jadwal_tambahan = tabel_jadwal_tambahan.id_jadwal_tambahan
  INNER JOIN tabel_guru ON tabel_jadwal_tambahan.id_guru = tabel_guru.id_guru 
  INNER JOIN tabel_mapel ON tabel_jadwal_tambahan.id_mapel = tabel_mapel.id_mapel
  WHERE tabel_jadwal_tambahan.hari = '$hari' AND STR_TO_DATE(tabel_jadwal_tambahan.end,'%d-%m-%Y') > STR_TO_DATE('$tgl','%d-%m-%Y')
  AND tabel_jadwal_tambahan.minggu_ke = '$currentWeek'
  ORDER BY tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal ASC";
}else{

  $queryKelas="SELECT 
  -- DISTINCT(tabel_kelas_tambahan.id_kelas_tambahan),
  tabel_jenjang.jenjang, tabel_mapel.mapel,
  tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal, tabel_jadwal_tambahan.jam_akhir,
  tabel_jadwal_tambahan.id_jadwal_tambahan,tabel_guru.nama
  FROM tabel_jadwal_tambahan 
  INNER JOIN tabel_jenjang ON tabel_jadwal_tambahan.id_jenjang = tabel_jenjang.id_jenjang
  -- LEFT JOIN tabel_kelas_tambahan ON  tabel_kelas_tambahan.id_jadwal_tambahan = tabel_jadwal_tambahan.id_jadwal_tambahan
  INNER JOIN tabel_guru ON (tabel_jadwal_tambahan.id_guru = tabel_guru.id_guru OR tabel_jadwal_tambahan.id_pembimbing_1 = tabel_guru.id_guru  OR tabel_jadwal_tambahan.id_pembimbing_2 = tabel_guru.id_guru OR tabel_jadwal_tambahan.id_pembimbing_3 = tabel_guru.id_guru)  
  INNER JOIN tabel_mapel ON tabel_jadwal_tambahan.id_mapel = tabel_mapel.id_mapel
  WHERE tabel_guru.nik = '$getguru' AND tabel_jadwal_tambahan.hari = '$hari' AND tabel_jadwal_tambahan.minggu_ke = '$currentWeek'
  AND STR_TO_DATE(tabel_jadwal_tambahan.end,'%d-%m-%Y') > STR_TO_DATE('$tgl','%d-%m-%Y')
  ORDER BY tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal ASC";
  
}
 
$result = mysqli_query($con,$queryKelas);
$data = array();

$setting = mysqli_query($con,"select * from tabel_setting where id_setting = '1'");
$setting = mysqli_fetch_array($setting,MYSQLI_ASSOC);
$detik = $setting['toleransi_jam_akhir'] * 60;

while($row = mysqli_fetch_array($result)){

  
//	$data[]= $row;
  // $status = $row['status'];
  $id_jadwal = $row['id_jadwal_tambahan'];
  $jam_awal = $row['jam_awal'];
  $jam_akhir = $row['jam_akhir'];
  $jam_akhir1 = strtotime($jam_akhir) + $detik;
  $jam_akhir1 = date('H:i', $jam_akhir1);
  $jam_akhir2 = strtotime($jam_akhir) - $detik;
  $jam_akhir2 = date('H:i', $jam_akhir2);

  $cek = mysqli_query($con,"SELECT * FROM tabel_jadwal_tambahan WHERE id_jadwal_tambahan = '$id_jadwal' AND 
                          '$jam' BETWEEN '$jam_awal' AND '$jam_akhir'");
  $ac = mysqli_num_rows($cek);
  if($ac > 0){
    $waktu = "ada"; 
  }else{
    $waktu = "";
  }
  $data[] = array(
    "id_kelas"      =>1,
    "jenjang"       =>$row['jenjang'],
    "kelas"         =>$row['nama_kelas'],
    "mapel"         =>$row['mapel'],
    "jam_awal"      =>$row['jam_awal'],
    "jam_akhir"     =>$row['jam_akhir'],
    "id_jadwal"     =>$row['id_jadwal_tambahan'],
    "waktu"         =>$waktu,
    "jam"           =>$jam, 
    "nama"          =>$row['nama'],
    "status_guru_bk" =>$statusGuruBK);
}
echo json_encode($data);
?>
