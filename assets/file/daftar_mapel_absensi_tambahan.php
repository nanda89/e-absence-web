<?php 
function hari(){
  $hari=$day=gmdate("l", time()+60*60*7);		
  switch($hari)
  {
    case"Sunday":$hari="Minggu"; break;
    case"Monday":$hari="Senin"; break;
    case"Tuesday":$hari="Selasa"; break;
    case"Wednesday":$hari="Rabu"; break;
    case"Thursday":$hari="Kamis"; break;
    case"Friday":$hari="Jumat"; break;
    case"Saturday":$hari="Sabtu"; break;
  }      
  $hariLengkap="$hari";
  return $hariLengkap;
}
include('koneksi.php');
$jadwal = $_GET['jadwalTambahan'];
$jam  		= gmdate("H:i", time()+60*60*7);
$hari 			= hari();
$tgl = gmdate("d-m-Y", time()+60*60*7);

$sql = mysqli_query($con,"SELECT * FROM tabel_absen_mapel_tambahan WHERE tgl = '$tgl' ANd id_jadwal_tambahan = '$jadwal'");
$cek = mysqli_num_rows($sql);

$queryKelasTambahan = "SELECT sk.id_penempatan,ts.nama_lengkap,ts.gambar,ts.id_siswa,tjt.nama_kelas,
    tg.nama,jt.tahun_ajaran,tjt.id_jadwal_tambahan AS id_kelas,amt.valid,amt.absen,tj.jenjang
    FROM tabel_kelas_tambahan kt
    INNER JOIN tabel_siswa ts ON ts.id_siswa = kt.id_siswa
    INNER JOIN tabel_siswakelas sk ON sk.id_siswa = ts.id_siswa
    INNER JOIN tabel_jadwal_tambahan tjt ON tjt.id_jadwal_tambahan = kt.id_jadwal_tambahan
    INNER JOIN tabel_guru tg ON tg.id_guru = tjt.id_guru
    INNER JOIN tabel_tahunajaran jt ON jt.id_ta = sk.id_ta
    LEFT JOIN tabel_absen_mapel_tambahan amt ON amt.id_kelas_tambahan = kt.id_kelas_tambahan
    INNER JOIN tabel_jenjang tj on tj.id_jenjang = tjt.id_jenjang
    WHERE kt.id_jadwal_tambahan 
    IN (SELECT id_jadwal_tambahan FROM tabel_jadwal_tambahan WHERE id_jadwal_tambahan = '$jadwal')";

$result = mysqli_query($con,$queryKelasTambahan);

$data = array();
  $no=1;
  while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){  
    //$data[]= $row;  
    $data[]  = array(
        "no"            => $no,
        "id_penempatan" => $row['id_penempatan'],
        "nama_lengkap"  => $row['nama_lengkap'],
        "gambar"        => $row['gambar'],
        "id_siswa"      => $row['id_siswa'],
        "jenjang"       => $row['jenjang'],
        "kelas"         => $row['nama_kelas'],
        "tahun_ajaran"  => $row['tahun_ajaran'],
        "nama"          => $row['nama'],
        "id_kelas"      => $row['id_kelas'],
        "valid"         => $row['valid'],
        "status"        => $hari,
        "absen"         => $row['absen'],
        "cek"           => $cek,
        "valid2"        => "");  // tidak faham buat apa
      $no++;             
  }

echo json_encode($data);
?>
