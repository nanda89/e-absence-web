<?php

$host = "localhost";
$db_user = "test";
$pass = "test";
$db_name = "dbs_absence_now";

if ($con = mysqli_connect($host, $db_user, $pass, $db_name)) {
    $con->set_charset("utf8");

} else {
    throw new Exception('Unable to connect');
}
