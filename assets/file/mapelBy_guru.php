<?php
include('koneksi.php');
include('all_function.php');
$nikGuru          = $_GET['guru'];
// $idJenjang        = $_GET['id_jenjang'];

$idGuru = getIdGuru($con , $nikGuru);

$q = "SELECT m.* FROM tabel_jadwal j JOIN tabel_mapel m ON m.id_mapel = j.id_mapel WHERE j.id_guru =$idGuru group by id_mapel";

$daftarMapel = mysqli_query($con , $q);
$mapel = array();
while($mpl = mysqli_fetch_array($daftarMapel)) {
    $mapel[] =array(
       "id_mapel"   => intval($mpl['id_mapel']),
       "mapel"      => $mpl['mapel'],
       "kkm"        => $mpl['kkm'],
       "jenis"      => $mpl['jenis'],
       "id_jenjang"      => intval($mpl['id_jenjang']),
       "id_kat_mapel"      => intval($mpl['id_kat_mapel'])
    );
}

echo json_encode($mapel);
?>