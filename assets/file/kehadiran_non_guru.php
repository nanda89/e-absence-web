<?php
function datediff($tgl1, $tgl2){
    $tgl1 = strtotime($tgl1);
    $tgl2 = strtotime($tgl2);
    $diff_secs = abs($tgl1 - $tgl2);
    $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
    return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
  }
include('koneksi.php');
include('all_function.php');

$idNonGuru  = $_GET['id_non_guru'];
$tgl        = $_GET['tgl'];
$tgl_akhir  = $_GET['tgl_akhir'];


$data = array();
$re  = mysqli_query($con,"SELECT * FROM tabel_setting WHERE id_setting = '1'");
$re = mysqli_fetch_array($re);
$jam_pulang_kerja = $re['jam_pulang'];
$begin = new DateTime($tgl);
$end = new DateTime($tgl_akhir);
$end->modify('+1 day');

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);

$hari_libur = [];
$kp = getKalenderPendidikan($con,array(
  'start_date'  => $tgl,
  'end_date'    => $tgl_akhir
));

if (count($kp) > 0) {
  foreach ($kp as $agenda) {
    if ($agenda['libur'] == 1) {
      $hari_libur[] = date('d-m-Y', strtotime($agenda['tanggal']));
    }
  }
}

foreach ($period as $dt) {
  $tgl = $dt->format("d-m-Y");

  if (in_array($tgl, $hari_libur))
    continue;

  $harihari = cek_hari($dt->format("l"));

  $jam_masuk = mysqli_query($con,"SELECT * FROM tabel_jam_masuk WHERE SUBSTRING(tabel_jam_masuk.hari, 3) = '$harihari'");
  $jam_masuk = mysqli_fetch_array($jam_masuk);
  $jam_masuk_kerja = $jam_masuk['jam'];

  $t = mysqli_query($con,"SELECT * FROM tabel_non_guru WHERE id_non_guru = '$idNonGuru'");

  while ($row = mysqli_fetch_array($t)) {

    $id = $row['id_non_guru'];
    $cek = mysqli_query($con,"SELECT * FROM tabel_absen_non_guru 
    WHERE id_non_guru = '$id' and tgl between '$tgl' and '$tgl_akhir' order by jam");

    if (mysqli_num_rows($cek) == 0) {
      $validasi_jam_masuk = "nihil";
      $validasi_jam_pulang = "nihil";
      $keterangan_jam_masuk = "tidak absen";
      $keterangan_jam_pulang = "tidak absen";
    } else if (mysqli_num_rows($cek) == 1) {
      $dataCek = mysqli_fetch_array($cek);
      $validasi_jam_masuk = $dataCek['jam'];
      $validasi_jam_pulang = "nihil";

      $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
      $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
      $keterangan_jam_masuk = "";
      if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
      } else {
        $keterangan_jam_masuk .= "absen sesuai - ";
      }
      // $keterangan_jam_masuk .= " ";
      // $keterangan_jam_masuk .= $cek->row()->keterangan;
      if (!empty($dataCek['keterangan'])) {
        $keterangan_jam_masuk = $dataCek['keterangan'];
      }

      $keterangan_jam_pulang = "tidak absen";
    } else {
        $dataCek = mysqli_fetch_array($cek);
      $validasi_jam_masuk = $dataCek['jam'];

      $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
      $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
      $keterangan_jam_masuk = "";
      if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
      } else {
        $keterangan_jam_masuk .= "absen sesuai - ";
      }
      // $keterangan_jam_masuk .= " ";
      // $keterangan_jam_masuk .= $cek->row()->keterangan;
      if (!empty($dataCek['keterangan'])) {
        $keterangan_jam_masuk = $dataCek['keterangan'];
      }

      $cek = mysqli_query($con,"SELECT * FROM tabel_absen_non_guru 
      WHERE id_non_guru = '$id' AND tgl between '$tgl' and '$tgl_akhir' order by jam desc");
      $cek = mysqli_fetch_array($cek);
      $validasi_jam_pulang = $cek['jam'];
      $sampai = "17:00";
      if (strtotime($validasi_jam_pulang) >= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
        $jam_pulang_diff = datediff($jam_pulang_kerja, $validasi_jam_pulang);
        $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
        $keterangan_jam_pulang = "";
        if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
          $keterangan_jam_pulang .= "terlambat $jam_pulang_diff menit -";
        } else {
          $keterangan_jam_pulang .= "absen sesuai - ";
        }
        // $keterangan_jam_masuk .= " ";
        // $keterangan_jam_masuk .= $cek->row()->keterangan;
        if (!empty($cek['keterangan'])) {
          $keterangan_jam_masuk = $cek['keterangan'];
        }
      } else {
        $validasi_jam_pulang = "nihil";
        $keterangan_jam_pulang = "tidak absen";
      }
    }

    $data[] = (object) array(
      'id_non_guru'         => intval($id),
      'nama'                => $row['nama'],
      'tgl'                 => $tgl,
      'jam_masuk_kerja'     => $jam_masuk_kerja,
      'jam_pulang_kerja'    => $jam_pulang_kerja,
      'validasi_jam_masuk'  => (!empty(trim($validasi_jam_masuk))) ? $validasi_jam_masuk : "nihil",
      'validasi_jam_pulang' => $validasi_jam_pulang,
      'keterangan_jam_masuk' => $keterangan_jam_masuk,
      'keterangan_jam_pulang' => $keterangan_jam_pulang
    );
  }
}

echo json_encode($data);
?>