<?php
include('koneksi.php');
include('all_function.php');
$nikGuru          = $_GET['guru'];
// $idJenjang        = $_GET['id_jenjang'];

$idGuru = getIdGuru($con , $nikGuru);

$q = "SELECT j.*,k.kelas,g.nama FROM tabel_jadwal j JOIN tabel_kelas k ON k.id_kelas = j.id_kelas JOIN tabel_guru g ON g.id_guru = j.id_guru
WHERE j.id_guru = $idGuru GROUP BY k.kelas";
//  AND j.id_jenjang =$idJenjang";
$daftarKelas = mysqli_query($con , $q);
$kelas = array();
while($kls = mysqli_fetch_array($daftarKelas)) {
    $kelas[] =array(
       "id_kelas"  => intval($kls['id_kelas']),
       "kelas"     => $kls['kelas'],
       "jam_awal"  => $kls['jam_awal'],
       "jam_akhir" => $kls['jam_akhir'],
       "hari"      => $kls['hari'],
       "nama"      => $kls['nama']
    );
}

echo json_encode($kelas);
?>