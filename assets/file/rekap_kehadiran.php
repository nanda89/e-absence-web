<?php 

function getKalenderPendidikan($con,$args){
    $data = array();
    $date = new DateTime($args['start_date']); // format standard
	$end = new DateTime($args['end_date']); // format standard
	$interval = $date->diff($end);
	$count = intval( $interval->format('%R%a') );
	$result = [];
    $tmp_result = [];
    if ( $count > 0 ) {
        for ($i=0; $i <= $count; $i++) { 

            $tmp_data = array(
                'tanggal' => $date->format('Y-m-d'),
            );

            $agenda = mysqli_query($con,"SELECT * FROM tabel_kalender WHERE tanggal='{$date->format('d-m-Y')}'");
            $agenda = mysqli_fetch_array($agenda);
            if( count($agenda) > 0 ) {
                $data[] = array(
                    "id_kalender"       =>$agenda['id_kalender'],
                    "tanggal"           =>$agenda['tanggal'],
                    "warna"             =>$agenda['warna'],
                    "agenda"            =>$agenda['agenda'],
                    "keterangan"        =>$agenda['keterangan'],
                    "libur"             =>$agenda['libur']
                );
            }
            $date->add( new DateInterval('P1D') );
   
        }
    }
    return $data;
}
function get_keterangan_absen(){
    return array(
        'izin'            => 'izin',
        'sakit'           => 'sakit',
        'tanpa_keterangan'=> 'tanpa keterangan',
        'absen_sesuai'    => 'absen sesuai',
        'tidak_absen'    => 'tidak absen',
      );
}
function cek_hari($tgl){
    $hari=$day= $tgl;
    switch($hari){
      case"Sunday":$hari="Minggu"; break;
      case"Monday":$hari="Senin"; break;
      case"Tuesday":$hari="Selasa"; break;
      case"Wednesday":$hari="Rabu"; break;
      case"Thursday":$hari="Kamis"; break;
      case"Friday":$hari="Jumat"; break;
      case"Saturday":$hari="Sabtu"; break;
    }
    return $hari;
  }
  function hari_lengkap($tgl) 
  {
      $hari=$day= $tgl;
    switch($hari){
        case"Sunday":$hari="0 Minggu"; break;
        case"Monday":$hari="1 Senin"; break;
        case"Tuesday":$hari="2 Selasa"; break;
        case"Wednesday":$hari="3 Rabu"; break;
        case"Thursday":$hari="4 Kamis"; break;
        case"Friday":$hari="5 Jumat"; break;
        case"Saturday":$hari="6 Sabtu"; break;
    }  
    return $hari;
  }
            
include('koneksi.php');
$dari           = $_GET['dari'];
// $ke             = $_GET['ke'];
$id_jenjang     = $_GET['id_jenjang'];
$id_guru        = $_GET['id_guru'];
$id_ta          = $_GET['id_ta'];

$dataGuru = mysqli_query($con , "SELECT * FROM tabel_guru WHERE id_guru = '$id_guru' or nik = '$id_guru'");
$dataGuru = mysqli_fetch_array($dataGuru);

$tgl_mulai  = new \DateTime( $_GET['dari'] );
$tgl_akhir  = new \DateTime( $_GET['ke'] );
$tgl_akhir->modify('+1 day');

$interval = $tgl_mulai->diff($tgl_akhir);
$jml_hari = intval( $interval->format('%R%a') );

$hari_libur = [];
$kp = getKalenderPendidikan($con,array(
  'start_date'	=> $tgl_mulai->format('Y-m-d'),
  'end_date'		=> $tgl_akhir->format('Y-m-d')
));

if( count( $kp ) > 0 ) {
  foreach( $kp as $agenda ) {
    if ( $agenda['libur'] == 1 ) {
      $hari_libur[] = $agenda['tanggal']; //date('d-m-Y', strtotime($agenda->tanggal));
    }   
  }
}

$jml_absen_nhl_dtg = $jml_hari;
$jml_absen_nhl_plg = $jml_hari;
$jml_absen_dtg = 0;
$jml_absen_plg = 0;
$jml_absen_sakit = 0;
$jml_absen_izin = 0;

$tanggal = new \DateTime( $_GET['dari'] );

for( $i=0; $i < $jml_hari; $i++ ) {

  if ( $tanggal->format('N') == 7 ) {

    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg-1;
    $jml_absen_nhl_plg = $jml_absen_nhl_plg-1;

  } elseif( in_array( $tanggal->format('d-m-Y'), $hari_libur ) ) {

    $jml_absen_nhl_dtg = $jml_absen_nhl_dtg-1;
    $jml_absen_nhl_plg = $jml_absen_nhl_plg-1;

  } else {


    $sql = "SELECT id_absen_kehadiran_guru FROM tabel_absen_kehadiran_guru WHERE id_guru='$id_guru' AND tgl='$dari'";
    // echo $sql;
    $absensi = mysqli_query($con,$sql);
    $num_rows = count($absensi);
    
    if( $num_rows >= 2 ) {

      $jml_absen_dtg++;
      $jml_absen_nhl_dtg = $jml_absen_nhl_dtg-1;
      $jml_absen_plg++;
      $jml_absen_nhl_plg = $jml_absen_nhl_plg-1;

      // echo "111111A ".$jml_absen_dtg.'<br>';

    } elseif( count($absensi) == 2 ) {

      $jml_absen_dtg++;
      $jml_absen_nhl_dtg = $jml_absen_nhl_dtg-1;
      // echo "111111B ".$jml_absen_dtg.'<br>';
    } 
    
    if( $num_rows != 1 ) {
      $sakit_sql = $sql;
      $sakit_sql .= " AND keterangan='sakit'";
      $cek_sakit = mysqli_query($con,$sakit_sql);
      if( count($cek_sakit) > 1 ) {
        $jml_absen_sakit++;
        $jml_absen_dtg = $jml_absen_dtg-1;
        // echo "22222A ".$jml_absen_dtg.'<br>';
      }
      $izin_sql = $sql;
      $izin_sql .= " AND keterangan='izin'";
      $cek_izin = mysqli_query($con,$izin_sql);
      if( count($cek_izin) > 1 ) {
        $jml_absen_izin++;
        $jml_absen_dtg = $jml_absen_dtg-1;
        // echo "222222B ".$jml_absen_dtg.'<br>';
      }

      $tidak_absen_sql = $sql;
      $tidak_absen_sql .= " AND keterangan='tidak_absen'";
      $cek_tidak_absen = mysqli_query($con,$tidak_absen_sql);
      if( count($cek_tidak_absen) > 1 ) {
        // $jml_absen_izin++;
        $jml_absen_dtg = $jml_absen_dtg-1;
        $jml_absen_nhl_dtg++;
        // echo "333333A ".$jml_absen_dtg.'<br>';
      }
    }

  }

  $tanggal->add( new \DateInterval('P1D') ); 
}

$data[] = array(
  'id_guru' => intval($id_guru),
  'nama'    => $dataGuru['nama'],
  'absensi' => array(
    'datang'      => $jml_absen_dtg,
    'nihil_dtg'   => $jml_absen_nhl_dtg,//(($jml_absen_nhl-$jml_absen_dtg)-$jml_absen_plg)*2
    'pulang'      => $jml_absen_plg,
    'nihil_plg'   => $jml_absen_nhl_plg,
    'sakit'       => $jml_absen_sakit,
    'izin'        => $jml_absen_izin
  )
);
echo json_encode($data);

?>
