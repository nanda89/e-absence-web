<?php
function getIdGuru($con ,$nikGuru){
    $dataGuru = mysqli_query($con , "SELECT * FROM tabel_guru WHERE id_guru = '$nikGuru' or nik = '$nikGuru'");
    $dataGuru = mysqli_fetch_array($dataGuru);
    $idGuru = $dataGuru['id_guru'];
    return $idGuru;
}
function hari_lengkap($tgl) 
{
    $hari=$day= $tgl;
  switch($hari){
      case"Sunday":$hari="0 Minggu"; break;
      case"Monday":$hari="1 Senin"; break;
      case"Tuesday":$hari="2 Selasa"; break;
      case"Wednesday":$hari="3 Rabu"; break;
      case"Thursday":$hari="4 Kamis"; break;
      case"Friday":$hari="5 Jumat"; break;
      case"Saturday":$hari="6 Sabtu"; break;
  }  
  return $hari;
}
function cek_hari($tgl){
    $hari=$day= $tgl;
    switch($hari){
      case"Sunday":$hari="Minggu"; break;
      case"Monday":$hari="Senin"; break;
      case"Tuesday":$hari="Selasa"; break;
      case"Wednesday":$hari="Rabu"; break;
      case"Thursday":$hari="Kamis"; break;
      case"Friday":$hari="Jumat"; break;
      case"Saturday":$hari="Sabtu"; break;
    }
    return $hari;
  }
  function getKalenderPendidikan($con,$args){
    $data = array();
    $date = new DateTime($args['start_date']); // format standard
	$end = new DateTime($args['end_date']); // format standard
	$interval = $date->diff($end);
	$count = intval( $interval->format('%R%a') );
	$result = [];
    $tmp_result = [];
    if ( $count > 0 ) {
        for ($i=0; $i <= $count; $i++) { 

            $tmp_data = array(
                'tanggal' => $date->format('Y-m-d'),
            );

            $agenda = mysqli_query($con,"SELECT * FROM tabel_kalender WHERE tanggal='{$date->format('d-m-Y')}'");
            $agenda = mysqli_fetch_array($agenda);
            if( count($agenda) > 0 ) {
                $data[] = array(
                    "id_kalender"       =>$agenda['id_kalender'],
                    "tanggal"           =>$agenda['tanggal'],
                    "warna"             =>$agenda['warna'],
                    "agenda"            =>$agenda['agenda'],
                    "keterangan"        =>$agenda['keterangan'],
                    "libur"             =>$agenda['libur']
                );
            }
            $date->add( new DateInterval('P1D') );
   
        }
    }
    return $data;
}
function get_keterangan_absen() {
    return array(
      'izin'            => 'izin',
      'sakit'           => 'sakit',
      'tanpa_keterangan'=> 'tanpa keterangan',
      'absen_sesuai'    => 'absen sesuai',
      'tidak_absen'    => 'tidak absen',
    );
  }
function get_week_of_month($date,$firstDayOfMonth) {
    $w1 = date("W", $firstDayOfMonth->getTimestamp());
    $w2 = date("W", $date->getTimestamp());
    $weekNum = $w2 - $w1 + 1;
    return $weekNum;
   }
?>