<?php 
include('koneksi.php');
$tgl = gmdate("d-m-Y", time()+60*60*7);

$query = "SELECT * FROM tabel_jadwal_tambahan WHERE STR_TO_DATE(end,'%d-%m-%Y') > STR_TO_DATE('$tgl','%d-%m-%Y')";
$result = mysqli_query($con,$query);
$valid = mysqli_num_rows($result);

$data= array();
if($valid > 0){

    while($row = mysqli_fetch_array($result)){
        $data[] = array(
            "id_jadwal_tambahan"    =>$row['id_jadwal_tambahan'],
            "nama_kelas"            =>$row['nama_kelas'],
            "id_jenjang"            =>$row['id_jenjang'],
            "id_guru"               =>$row['id_guru'],
            "id_mapel"              =>$row['id_mapel'],
            "id_pembimbing_1"       =>$row['id_pembimbing_1'],
            "id_pembimbing_2"       =>$row['id_pembimbing_2'],
            "id_pembimbing_3"       =>$row['id_pembimbing_3'],
            "star_date"                 =>$row['start'],
            "end_date"                   =>$row['end'],
            "hari"                  =>$row['hari'],
            "jam_awal"              =>$row['jam_awal'],
            "jam_akhir"             =>$row['jam_akhir'],
            "minggu_ke"             =>$row['minggu_ke']
        );
    }
    echo json_encode($data);
}else{
    echo json_encode($data);
}

?>