<?php
include 'koneksi.php';
include 'all_function.php';

$tgl = $_GET['tgl'];
$jenjang = $_GET['id_jenjang'];
$ta = $_GET['id_ta'];
$kelas = $_GET['id_kelas'];
$guru = $_GET['id_guru'];

// total siswa
$q_total_siswa = "select * from tabel_siswakelas where id_kelas ='$kelas'";
$qTotal = mysqli_query($con, $q_total_siswa);
$totalRow = mysqli_num_rows($qTotal);

//ubah tanggal ke hari
$dt = new DateTime($tgl);
$harihari = cek_hari($dt->format("l"));

//loop mata pelajaran
$q_mapel = "select j.*,m.mapel from tabel_jadwal j join tabel_mapel m on j.id_mapel = m.id_mapel where j.id_kelas = '$kelas' and j.hari like '%$harihari' and j.id_ta = '$ta' and j.id_jenjang = '$jenjang'";
$q_mapel_list = mysqli_query($con, $q_mapel);
while ($row = mysqli_fetch_array($q_mapel_list)) {

    $mapel = '';
    $hadir = 0;
    $sakit = 0;
    $izin = 0;
    $nihil = 0;

    $id_jadwal = $row['id_jadwal'];

    $mapel = $row['mapel'];

    $cek = mysqli_query($con, "select * from tabel_absen_mapel where tgl = '$tgl' and id_jadwal = '$id_jadwal'");
    if (mysqli_num_rows($cek) == 0) {
        $data[] = array(
            'mapel' => $mapel,
            'nihil' => $totalRow,
            'hadir' => $hadir,
            'sakit' => $sakit,
            'izin' => $izin,
        );

    } else {

        while ($rowSiswa = mysqli_fetch_array($cek)) {

            if ($rowSiswa['absen'] == 'alpha') {
                $nihil++;
            } else if ($rowSiswa['absen'] == 'hadir') {
                $hadir++;
            } else if ($rowSiswa['absen'] == 'izin') {
                $izin++;
            } else if ($rowSiswa['absen'] == 'sakit') {
                $sakit++;
            }
        }

        $data[] = array(
            'mapel' => $mapel,
            'nihil' => $nihil,
            'hadir' => $hadir,
            'sakit' => $sakit,
            'izin' => $izin,
        );

    }
}

echo json_encode($data);
