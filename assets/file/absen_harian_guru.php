<?php
function datediff($tgl1, $tgl2)
{
    $tgl1 = strtotime($tgl1);
    $tgl2 = strtotime($tgl2);
    $diff_secs = abs($tgl1 - $tgl2);
    $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
    return array("years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff));
}
include 'koneksi.php';
include 'all_function.php';
$data = array();
$tgl = $_GET['tgl']; // format mesti Y-m-d atau Y-m-d H:i:s
$tgl_akhir = $_GET['tgl_akhir'];
$guru = $_GET['id_guru'];

$re = mysqli_query($con, "SELECT * FROM tabel_setting WHERE id_setting = '1'");
$re = mysqli_fetch_array($re);
$jam_pulang_kerja = $re['jam_pulang'];
$begin = new DateTime($tgl);
$end = new DateTime($tgl_akhir);
$end->modify('+1 day');

$id_guru = getIdGuru($con, $guru);

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);

$hari_libur = [];
$kp = getKalenderPendidikan($con, array(
    'start_date' => $tgl,
    'end_date' => $tgl_akhir,
));

if (count($kp) > 0) {
    foreach ($kp as $agenda) {
        if ($agenda['libur'] == 1) {
            $hari_libur[] = date('d-m-Y', strtotime($agenda['tanggal']));
        }
    }
}

$status_absen_pulang = $re['status_absen_pulang'];

foreach ($period as $dt) {
    $tgl = $dt->format("d-m-Y");

    if (in_array($tgl, $hari_libur)) {
        continue;
    }

    if ($dt->format('N') == 7) {
        continue;
    }

    $harihari = cek_hari($dt->format("l"));

    $jam_masuk = mysqli_query($con, "SELECT * FROM tabel_jam_masuk WHERE SUBSTRING(tabel_jam_masuk.hari, 3) = '$harihari'");
    $jam_masuk = mysqli_fetch_array($jam_masuk);
    $jam_masuk_kerja = $jam_masuk['jam'];

    $qGuru = "SELECT * FROM tabel_guru";
    if ($id_guru != '') {
        $qGuru .= " WHERE id_guru = '$id_guru'";
    }

    $t = mysqli_query($con, $qGuru);

    while ($row = mysqli_fetch_array($t)) {
        $id = $row['id_guru'];
        $cek = mysqli_query($con, "SELECT * FROM tabel_absen_kehadiran_guru
    WHERE id_guru = $id AND tgl = '$tgl' order by jam");

        if ($status_absen_pulang == "1") {
            if (mysqli_num_rows($cek) == 0) {
                //guru tidak hadir pada hari ini
                $validasi_jam_masuk = "nihil";
                $validasi_jam_pulang = "nihil";
                $keterangan_jam_masuk = "tidak absen";
                $keterangan_jam_pulang = "tidak absen";
            } else if (mysqli_num_rows($cek) == 1) {
                //guru sudah absen hadir
                $cek = mysqli_fetch_array($cek);
                $validasi_jam_masuk = $cek['jam'];
                $validasi_jam_pulang = "nihil";
                $keterangan_jam_masuk = $cek['keterangan'];
                $keterangan_jam_pulang = "tidak absen";

                if (strtolower($keterangan_jam_masuk) == 'izin' || strtolower($keterangan_jam_masuk) == 'sakit') {
                    $validasi_jam_masuk = 'nihil';
                } else {
                    $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                    $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                    $keterangan_jam_masuk = "";
                    if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                    } else {
                        $keterangan_jam_masuk .= "absen sesuai - ";
                    }
                    // $keterangan_jam_masuk .= " ";
                    // $keterangan_jam_masuk .= $cek->row()->keterangan;
                    if (!empty($cek['keterangan'])) {
                        $keterangan_jam_masuk = $cek['keterangan'];
                    }
                }

                $keterangan_jam_pulang = "tidak absen";
            } else {
                //guru sudah absen hadir dan pulang
                $cek = mysqli_fetch_array($cek);
                $validasi_jam_masuk = $cek['jam'];

                $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                $keterangan_jam_masuk = "";
                if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                    $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                } else {
                    $keterangan_jam_masuk .= "absen sesuai - ";
                }
                // $keterangan_jam_masuk .= " ";
                // $keterangan_jam_masuk .= $cek->row()->keterangan;
                if (!empty($cek['keterangan'])) {
                    $keterangan_jam_masuk = $cek['keterangan'];
                }

                $cek = mysqli_query($con, "select * from tabel_absen_kehadiran_guru
            where id_guru = $id and tgl = '$tgl' order by jam desc");
                $cek = mysqli_fetch_array($cek);
                $validasi_jam_pulang = $cek['jam'];

                $sampai = "17:00";
                if (strtotime($validasi_jam_pulang) >= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
                    $jam_pulang_diff = datediff($jam_pulang_kerja, $validasi_jam_pulang);
                    $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
                    $keterangan_jam_pulang = "";
                    if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
                        $keterangan_jam_pulang .= "terlambat $jam_pulang_diff menit -";
                    } else {
                        $keterangan_jam_pulang .= "absen sesuai - ";
                    }
                    // $keterangan_jam_masuk .= " ";
                    // $keterangan_jam_masuk .= $cek->row()->keterangan;
                    if (!empty($cek['keterangan'])) {
                        $keterangan_jam_masuk = $cek['keterangan'];
                    }
                } else {
                    $validasi_jam_pulang = "nihil";
                    $keterangan_jam_pulang = "tidak absen";
                }
            }
        } else {
            if (mysqli_num_rows($cek) == 0) {
                $validasi_jam_masuk = "nihil";
                $keterangan_jam_masuk = "tidak absen";
            } else if (mysqli_num_rows($cek) == 1) {
                $validasi_jam_masuk = $cek['jam'];
                $keterangan_jam_masuk = $cek['keterangan'];

                if (strtolower($keterangan_jam_masuk) == 'izin' || strtolower($keterangan_jam_masuk) == 'sakit') {
                    $validasi_jam_masuk = 'nihil';
                } else {

                    $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                    $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                    $keterangan_jam_masuk = "";
                    if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                        $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                    } else {
                        $keterangan_jam_masuk .= "absen sesuai - ";
                    }
                    $keterangan_jam_masuk .= " ";
                    $keterangan_jam_masuk .= $cek['keterangan'];
                }
            } else {
                $validasi_jam_masuk = $cek['jam'];

                $jam_masuk_diff = datediff($validasi_jam_masuk, $jam_masuk_kerja);
                $jam_masuk_diff = $jam_masuk_diff['minutes_total'];
                $keterangan_jam_masuk = "";
                if (strtotime($validasi_jam_masuk) > strtotime($jam_masuk_kerja)) {
                    $keterangan_jam_masuk .= "terlambat $jam_masuk_diff menit - ";
                } else {
                    $keterangan_jam_masuk .= "absen sesuai - ";
                }
                // $keterangan_jam_masuk .= " ";
                // $keterangan_jam_masuk .= $cek->row()->keterangan;
                if (!empty($cek['keterangan'])) {
                    $keterangan_jam_masuk = $cek['keterangan'];
                }
            }
            // echo $row->id_guru . " " . $harihari;
            $absenGuru = mysqli_query($con, "select * from tabel_jadwal where id_guru = $id and substring(hari, 3) = '$harihari' order by jam_awal desc");

            if (mysqli_num_rows($absenGuru) > 0) {

                $jadwal = mysqli_fetch_array($absenGuru);
                $idJadwal = $jadwal['id_jadwal'];
                $absenMapel = mysqli_query($con, "select * from tabel_absen_mapel where tgl = '$tgl' and id_jadwal='$idJadwal'");

                if (mysqli_num_rows($absenMapel) == 0) {
                    $validasi_jam_pulang = "nihil";
                    $keterangan_jam_pulang = "tidak absen";
                } else {
                    $validasi_jam_pulang = $absenMapel['jam'];

                    // echo $validasi_jam_pulang;

                    $sampai = "17:00";
                    if (strtotime($validasi_jam_pulang) >= strtotime($jam_pulang_kerja) && strtotime($validasi_jam_pulang) <= strtotime($sampai)) {
                        $jam_pulang_diff = datediff($jam_pulang_kerja, $validasi_jam_pulang);
                        $jam_pulang_diff = $jam_pulang_diff['minutes_total'];
                        $keterangan_jam_pulang = "";
                        if (strtotime($jam_pulang_kerja) > strtotime($validasi_jam_pulang)) {
                            $keterangan_jam_pulang .= "terlambat $jam_pulang_diff menit -";
                        } else {
                            $keterangan_jam_pulang .= "absen sesuai - ";
                        }
                        $keterangan_jam_pulang .= " ";
                        $keterangan_jam_pulang .= $cek->$row()->keterangan;
                    } else {
                        $validasi_jam_pulang = "nihil";
                        $keterangan_jam_pulang = "tidak absen";
                    }
                }
            } else {
                $validasi_jam_pulang = "nihil";
                $keterangan_jam_pulang = "tidak absen";
            }
        }
        $data[] = (object) array(
            'id_guru' => $row['id_guru'],
            'nama' => $row['nama'],
            'tgl' => $tgl,
            'jam_masuk_kerja' => $jam_masuk_kerja,
            'jam_pulang_kerja' => $jam_pulang_kerja,
            'validasi_jam_masuk' => (is_null($validasi_jam_masuk)) ? "nihil" : $validasi_jam_masuk,
            'validasi_jam_pulang' => $validasi_jam_pulang,
            'keterangan_jam_masuk' => $keterangan_jam_masuk,
            'keterangan_jam_pulang' => $keterangan_jam_pulang,
        );
    }
}

echo json_encode($data);
