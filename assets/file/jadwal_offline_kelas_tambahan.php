<?php
include 'koneksi.php';
$daftarJadwal = $_GET['id_jadwal'];
$jadwal = str_replace("'", "", $daftarJadwal);
$query = "SELECT * FROM tabel_kelas_tambahan WHERE id_jadwal_tambahan in($daftarJadwal)";
$result = mysqli_query($con, $query);
$valid = mysqli_num_rows($result);

if ($valid > 0) {
    $data = array();
    while ($row = mysqli_fetch_array($result)) {
        $data[] = array(
            "id_kelas_tambahan" => $row['id_kelas_tambahan'],
            "id_jadwal_tambahan" => $row['id_jadwal_tambahan'],
            "id_siswa" => $row['id_siswa'],
        );
    }
    echo json_encode($data);
} else {
    $data = array();
    echo json_encode($data);

}
