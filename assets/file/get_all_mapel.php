<?php
include('koneksi.php');

$query = "SELECT * FROM tabel_mapel";
$dataMapel = mysqli_query($con,$query);
$data = array();

while($row = mysqli_fetch_array($dataMapel)){

    $data[] = (object) array(
        'id_mapel'              => intval($row['id_mapel']),
        'id_jenjang'            => intval($row['id_jenjang']),
        'id_kat_mapel'          => intval($row['id_kat_mapel']),
        'mapel'                 => $row['mapel'],
        'kkm'                   => $row['kkm'],
        'jenis'                 => $row['jenis'],
      );
}

echo json_encode($data);
?>