<?php
/**
 * API ini digunakan untuk mengambil semua data absesi kehadiran dan kepulang
 * semua guru kecuali data kepala sekolah tidak ditampilkan
 */

include 'koneksi.php';

$tanggal = $_GET['tgl'];
$idJenjang = $_GET['id_jenjang'];
$idTa = $_GET['id_ta'];

// cek apakah tanggal ini hari libur
$totalHariLibur = mysqli_query($con, "SELECT * FROM tabel_kalender where libur='1' and tanggal = '$tanggal'");
$totalHariLibur = mysqli_num_rows($totalHariLibur);
$data = array();

if ($totalHariLibur > 0) {
    //tanggal ini tanggal libur
} else {
    //ambil data guru selain kepala sekolah dan non guru
    $daftarGuru = mysqli_query($con, "SELECT * FROM tabel_guru where status_kepsek = 0 and id_jenjang = '$idJenjang'");

    while ($row = mysqli_fetch_array($daftarGuru)) {

        //cek absensi
        $idGuru = $row['id_guru'];
        $dataAbsensi = mysqli_query($con, "SELECT * FROM tabel_absen_kehadiran_guru WHERE id_guru = $idGuru and tgl='$tanggal' order by time('jam') desc");

        $namaGuru = $row['nama'];
        $jamMasuk = '-';
        $ketMasuk = '-';
        $tglMasuk = '-';

        $jamPulang = '-';
        $ketPulang = '-';
        $tglPulang = '-';

        if (mysqli_num_rows($dataAbsensi) == 1) {
            //baru absen masuk
            while ($absensi = mysqli_fetch_array($dataAbsensi)) {
                $jamMasuk = $absensi['jam'];
                $ketMasuk = $absensi['keterangan'];
                $tglMasuk = $absensi['tgl'];
            }

        } else if (mysqli_num_rows($dataAbsensi) == 2) {
            //sudah absen masuk dan pulang
            $isMasuk = 1;
            while ($absensi = mysqli_fetch_array($dataAbsensi)) {
                if ($isMasuk == 1) {
                    $jamMasuk = $absensi['jam'];
                    $ketMasuk = $absensi['keterangan'];
                    $tglMasuk = $absensi['tgl'];

                } else {
                    $jamPulang = $absensi['jam'];
                    $ketPulang = $absensi['keterangan'];
                    $tglPulang = $absensi['tgl'];
                }
                $isMasuk++;
            }

        }

        $data[] = array(
            "nama" => $namaGuru,
            "jam_masuk" => $jamMasuk,
            "tgl_masuk" => $tglMasuk,
            "keterangan_masuk" => $ketMasuk,
            "jam_pulang" => $jamPulang,
            "tgl_pulang" => $tglPulang,
            "keterangan_pulang" => $ketPulang,
        );

    }
    echo json_encode($data);

}
