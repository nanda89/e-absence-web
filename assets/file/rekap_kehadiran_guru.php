<?php 
function cek_hari($tgl){  
  $hari=$day= $tgl;
  switch($hari){
      case"Sunday":$hari="Minggu"; break;
      case"Monday":$hari="Senin"; break;
      case"Tuesday":$hari="Selasa"; break;
      case"Wednesday":$hari="Rabu"; break;
      case"Thursday":$hari="Kamis"; break;
      case"Friday":$hari="Jumat"; break;
      case"Saturday":$hari="Sabtu"; break;
  }  
  return $hari;
}

function hari(){  
  $hari=$day= gmdate("l", time()+60*60*7);
  switch($hari){
      case"Sunday":$hari="Minggu"; break;
      case"Monday":$hari="Senin"; break;
      case"Tuesday":$hari="Selasa"; break;
      case"Wednesday":$hari="Rabu"; break;
      case"Thursday":$hari="Kamis"; break;
      case"Friday":$hari="Jumat"; break;
      case"Saturday":$hari="Sabtu"; break;
  }  
  return $hari;
}

function hari_lengkap($tgl) {
    $hari=$day= $tgl;
  switch($hari){
      case"Sunday":$hari="0 Minggu"; break;
      case"Monday":$hari="1 Senin"; break;
      case"Tuesday":$hari="2 Selasa"; break;
      case"Wednesday":$hari="3 Rabu"; break;
      case"Thursday":$hari="4 Kamis"; break;
      case"Friday":$hari="5 Jumat"; break;
      case"Saturday":$hari="6 Sabtu"; break;
  }  
  return $hari;
}

function datediff($tgl1, $tgl2){
  $tgl1 = strtotime($tgl1);
  $tgl2 = strtotime($tgl2);
  $diff_secs = abs($tgl1 - $tgl2);
  $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
  $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
  return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
}

function kehadiranReguler($con,$dari,$ke,$id_jenjang,$id_ta,$id_guru,$tgl,$jam,$tgl1,$hari){
    $guru_sql = "select * from tabel_guru ";
    if ($id_guru != "") $guru_sql .= " where id_guru = '$id_guru' or nik = '$id_guru'";
    
    $guru = mysqli_query($con, $guru_sql);
    
    $data = array();
    
    while ($row1 = mysqli_fetch_array($guru)) {
        $id_guru = $row1['id_guru'];
        $sql = "select distinct(tabel_mapel.id_mapel) from tabel_mapel 
                inner join tabel_jadwal on (tabel_mapel.id_mapel = tabel_jadwal.id_mapel) 
                inner join tabel_jenjang on (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang) 
                inner join tabel_guru on (tabel_guru.id_guru = tabel_jadwal.id_guru) 
                inner join tabel_tahunajaran on (tabel_jadwal.id_ta = tabel_tahunajaran.id_ta) 
                where tabel_jenjang.id_jenjang = '$id_jenjang' and tabel_tahunajaran.id_ta = '$id_ta' ";
    
        if ($id_guru != "") $sql .= " and tabel_guru.id_guru = '$id_guru'";
        $t = mysqli_query($con, $sql) or die(mysqli_error($con));
        while ($row = mysqli_fetch_array($t)) {
            $hadir = 0;
            $tidak_absen = 0;
            $sakit = 0;
            $izin = 0;
            $tanpa_keterangan = 0;
            $id_mapel = $row['id_mapel'];
    
            $begin = new DateTime($dari);
            $end = new DateTime($ke);
            $end->modify('+1 day');
            
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);
    
            
    
            foreach ($period as $dt) {
                $tgltgl = $dt->format("d-m-Y");
                $day = $dt->format("l");
                $harihari = cek_hari($day);
                $harilengkap = hari_lengkap($day);
    
            // echo $tgltgl." == ".$day." == ".$harihari." == ".$harilengkap."<br>";
    
                $banyak = mysqli_query($con, "SELECT * FROM tabel_jadwal where hari = '$harilengkap' 
                and tabel_jadwal.id_mapel = '$id_mapel' and tabel_jadwal.id_guru='$id_guru'");
    
                $cek = mysqli_query($con, "SELECT distinct(tabel_absen_mapel.id_jadwal), tabel_absen_mapel.keterangan
                 FROM tabel_absen_mapel 
                inner join tabel_jadwal on tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal 
                WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel' 
                AND tabel_absen_mapel.valid = 'valid'  and tabel_jadwal.id_guru='$id_guru'");
    
                $ada_absen = 0;
    
                while($row_absen = mysqli_fetch_array($cek)) {
                    $keterangan = $row_absen['keterangan'];
                    if ($keterangan == "") {
                        $hadir++; $ada_absen++;
                    }
                    else if ($keterangan == "Sakit") { 
                        $sakit++; $ada_absen++;
                    }
                    else if ($keterangan == "Izin") { 
                        $izin++; $ada_absen++;
                    }
                    else if ($keterangan == "Tanpa keterangan") {
                        $tanpa_keterangan++; $ada_absen++;
                    }
                }
    
                $banyak = mysqli_num_rows($banyak);
    
                $tidak_absen += ($banyak - $ada_absen);
            }
    
            $mapel = mysqli_query($con, "select * from tabel_mapel where id_mapel = '$id_mapel'");
            $mapel = mysqli_fetch_array($mapel);
            
            $jenjang = mysqli_query($con, "select * from tabel_jenjang where id_jenjang = '$id_jenjang'");
            $jenjang = mysqli_fetch_array($jenjang);
    
            $data[] = array(
                "nama"          =>$row1['nama'],
                "jenjang"       =>$jenjang['jenjang'],
                "mapel"         =>$mapel['mapel'],
                "hadir"         =>$hadir,
                "tidak_absen"   => $tidak_absen, 
                "sakit"         => $sakit,
                "izin"          => $izin,
                "tanpa_keterangan" => $tanpa_keterangan, 
                "nik"           => $row1['nik'],
                "type"          =>"reguler"
            );      
        }
    }
    return $data;
}


include('koneksi.php');
//$nik = $_GET['nik'];
$dari           = $_GET['dari'];
$ke             = $_GET['ke'];
$id_jenjang     = $_GET['id_jenjang'];
$id_ta          = $_GET['id_ta'];
$id_guru        = $_GET['id_guru'];
$tgl 			= gmdate("d-m-Y", time()+60*60*7);
$jam  		    = gmdate("H:i", time()+60*60*7);
$tgl1           = date("l", strtotime($dari));
$hari           = cek_hari($tgl1);
$data = array();
$data = kehadiranReguler($con,$dari,$ke,$id_jenjang,$id_ta,$id_guru,$tgl,$jam,$tgl1,$hari);

// $guru_sql = "select * from tabel_guru ";
// if ($id_guru != "") $guru_sql .= " where id_guru = '$id_guru' or nik = '$id_guru'";

// $guru = mysqli_query($con, $guru_sql);
// while ($row1 = mysqli_fetch_array($guru)) {
//     $id_guru = $row1['id_guru'];
//         $sql = "select distinct(tabel_mapel.id_mapel) 
//         from tabel_mapel 
//         inner join tabel_jadwal_tambahan on (tabel_mapel.id_mapel = tabel_jadwal_tambahan.id_mapel) 
//         inner join tabel_jenjang on (tabel_jenjang.id_jenjang = tabel_jadwal_tambahan.id_jenjang) 
//         inner join tabel_guru on (tabel_guru.id_guru = tabel_jadwal_tambahan.id_guru or tabel_guru.id_guru = tabel_jadwal_tambahan.id_pembimbing_1 or tabel_guru.id_guru = tabel_jadwal_tambahan.id_pembimbing_2 or tabel_guru.id_guru = tabel_jadwal_tambahan.id_pembimbing_3) 
//         inner join tabel_tahunajaran on (tabel_jadwal_tambahan.id_ta = tabel_tahunajaran.id_ta) 
//         where tabel_jenjang.id_jenjang = '$id_jenjang'
//         and tabel_tahunajaran.id_ta = '$id_ta'";

//         if ($id_guru != "") $sql .= " and tabel_guru.id_guru = '$id_guru'";
//         $t = mysqli_query($con, $sql) or die(mysqli_error($con));
//         // berapa banyak jenis mapel
//         while ($row = mysqli_fetch_array($t)) {
//             $hadir = 0;
//             $tidak_absen = 0;
//             $sakit = 0;
//             $izin = 0;
//             $tanpa_keterangan = 0;
//             $id_mapel = $row['id_mapel'];
    
//             $begin = new DateTime($dari);
//             $end = new DateTime($ke);
//             $end->modify('+1 day');
            
//             $interval = DateInterval::createFromDateString('1 day');
//             $period = new DatePeriod($begin, $interval, $end);
//             foreach ($period as $dt) {
//                 $tgltgl = $dt->format("d-m-Y");
//                 $day = $dt->format("l");
//                 $harihari = cek_hari($day);
//                 $harilengkap = hari_lengkap($day);
    
//             // echo $tgltgl." == ".$day." == ".$harihari." == ".$harilengkap."<br>";
//                 $banyak = mysqli_query($con, "SELECT * FROM tabel_jadwal_tambahan where hari = '$harihari' 
//                 and id_mapel = '$id_mapel' and id_guru='$id_guru'");

//                 $cek = mysqli_query($con, "SELECT distinct(tabel_absen_mapel.id_jadwal), tabel_absen_mapel.keterangan
//                 FROM tabel_absen_mapel 
//                 inner join tabel_jadwal on tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal 
//                 WHERE tabel_absen_mapel.tgl = '$tgltgl' AND tabel_jadwal.id_mapel = '$id_mapel' 
//                 AND tabel_absen_mapel.valid = 'valid'  and tabel_jadwal.id_guru='$id_guru'");

//             }

//         }
        
// }

echo json_encode($data);
?>
