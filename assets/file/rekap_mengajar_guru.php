<?php
include 'koneksi.php';
include 'all_function.php';
$data = array();

$idguru = $_GET['id_guru'];
$id_jenjang = $_GET['id_jenjang'];
$id_ta = $_GET['id_ta'];
$tgl_mulai = new \DateTime($_GET['tgl_mulai']); // format mesti Y-m-d
$tgl_akhir = new \DateTime($_GET['tgl_akhir']);
$tgl_akhir->modify('+1 day');

$id_guru = getIdGuru($con, $idguru);

$interval = $tgl_mulai->diff($tgl_akhir);
$jml_hari = intval($interval->format('%R%a'));
// var_dump($jml_hari);
$hari_libur = [];
$kp = getKalenderPendidikan($con, array(
    'start_date' => $tgl_mulai->format('Y-m-d'),
    'end_date' => $tgl_akhir->format('Y-m-d'),
));

if (count($kp) > 0) {
    foreach ($kp as $agenda) {
        if ($agenda['libur'] == 1) {
            $hari_libur[] = date('d-m-Y', strtotime($agenda['tanggal']));
        }
    }
}

// $jml_hadir = 0;
// $jml_sakit = 0;
// $jml_izin = 0;
// $jml_nihil = 0; // nihil
// $jml_tanpa_keterangan = 0;

$list_ket = get_keterangan_absen();
$guru = mysqli_query($con, "SELECT * FROM tabel_guru WHERE id_guru = $id_guru");
$guru = mysqli_fetch_array($guru);

$sql = "SELECT DISTINCT(tabel_mapel.id_mapel),tabel_mapel.mapel FROM tabel_mapel
  INNER JOIN tabel_jadwal ON (tabel_mapel.id_mapel = tabel_jadwal.id_mapel)
  INNER JOIN tabel_jenjang ON (tabel_jenjang.id_jenjang = tabel_jadwal.id_jenjang)
  INNER JOIN tabel_guru ON (tabel_guru.id_guru = tabel_jadwal.id_guru)
  INNER JOIN tabel_tahunajaran ON (tabel_jadwal.id_ta = tabel_tahunajaran.id_ta)
  WHERE tabel_jenjang.id_jenjang = '$id_jenjang' AND tabel_tahunajaran.id_ta = '$id_ta'";

if ($id_guru != "") {
    $sql .= " AND tabel_guru.id_guru = '$id_guru'";
}

$t = mysqli_query($con, $sql);
$mpl = '';
while ($mapel = mysqli_fetch_array($t)) {

    $jml_hadir = 0;
    $jml_sakit = 0;
    $jml_izin = 0;
    $jml_nihil = 0; // nihil
    $jml_tanpa_keterangan = 0;

    $id_mapel = intval($mapel['id_mapel']);

    $mpl .= $mapel['mapel'] . "\n";
    $begin = new DateTime($_GET['tgl_mulai']);
    $end = new DateTime($_GET['tgl_akhir']);
    $end->modify('+1 day');

    $interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($begin, $interval, $end);

    foreach ($period as $dt) {

        $tgltgl = $dt->format("d-m-Y");
        $day = $dt->format("l");
        $harihari = cek_hari($day);
        $harilengkap = hari_lengkap($day);

        if (!in_array($tgltgl, $hari_libur)) {

            $banyak = mysqli_query($con, "SELECT * FROM tabel_jadwal WHERE hari='$harilengkap'
      AND tabel_jadwal.id_mapel=$id_mapel and tabel_jadwal.id_guru=$id_guru");

            $jml_nihil += mysqli_num_rows($banyak) * 2; // dikali 2 karna dihitug datang dan pulang
            $cek = mysqli_query($con, "SELECT distinct(tabel_absen_mapel.id_jadwal), tabel_absen_mapel.keterangan, tabel_absen_mapel.jam
          FROM tabel_absen_mapel
          INNER JOIN tabel_jadwal ON tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal
          WHERE tabel_absen_mapel.tgl = '$tgltgl'
          AND tabel_jadwal.id_mapel = '$id_mapel'
          AND tabel_absen_mapel.valid = 'valid'
          AND tabel_jadwal.id_guru='$id_guru'");

            while ($absen = mysqli_fetch_array($cek)) {

                if (null != $absen['jam'] && !empty($absen['jam'])) {

                    $jml_hadir++;
                    $jml_nihil--;
                } else {

                    $keterangan = strtolower(str_replace(' ', '_', $absen['keterangan']));
                    if ($keterangan == "sakit") {
                        $jml_sakit++;
                        $jml_nihil--;
                    } elseif ($keterangan == "izin") {
                        $jml_izin++;
                        $jml_nihil--;
                    } else {
                        $jml_tanpa_keterangan++;
                        $jml_nihil--;
                    }
                }
            }
        }
    }

    $mapel = mysqli_query($con, "SELECT * FROM tabel_mapel WHERE id_mapel = '$id_mapel'");
    $mapel = mysqli_fetch_array($mapel);
    $jenjang = mysqli_query($con, "SELECT * FROM tabel_jenjang WHERE id_jenjang = '$id_jenjang'");
    $jenjang = mysqli_fetch_array($jenjang);

    $data[] = array(
        "nama" => $guru['nama'],
        "nik" => $guru['nik'],
        "jenjang" => (isset($jenjang['jenjang'])) ? $jenjang['jenjang'] : '',
        "mapel" => (isset($mapel['mapel'])) ? $mapel['mapel'] : '',
        "hadir" => $jml_hadir,
        "sakit" => $jml_sakit,
        "izin" => $jml_izin,
        "nihil" => $jml_nihil,
        "tanpa_keterangan" => $jml_tanpa_keterangan,
    );
}

echo json_encode($data);
