<?php
include('koneksi.php');
$target_dir = "upload/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$idNonGuru  = $_POST["idNonGuru"];
$tanggal    = $_POST["tanggal"];
$jam        = $_POST["jam"];
$uraian     = $_POST["uraian"];
$type       = $_POST["type"];
$fileName   = basename($_FILES["fileToUpload"]["name"]);

$data = array();

if(file_exists($target_dir) == false){
    mkdir($target_dir , true);
}

if ($uploadOk == 0) {
    $data['code']       = 500;
    $data['success']    = false;
    $data['message']    = "File tidak boleh kosong";
    
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $q = "INSERT INTO tabel_kegiatan_non_guru(id_non_guru,tanggal,jam,uraian,type_file,file) VALUES('$idNonGuru','$tanggal','$jam','$uraian','$type','$fileName')";
        mysqli_query($con , $q);
        $data['code']       = 200;
        $data['success']    = true;
        $data['message']    = "File berhasil diupload";

    } else {
        $data['code']       = 500;
        $data['success']    = false;
        $data['message']    = "Error dalam pemindahan file";
        
    }
}
echo json_encode($data);
?>