<?php
include('all_function.php');
include('koneksi.php');
$idKelas        = isset($_GET['id_kelas']) ? $_GET['id_kelas'] : '';
$idJenjang      = $_GET['id_jenjang'];
$nikGuru        = $_GET['guru'];
$idMapel        = $_GET['id_mapel'];
$idTA           = $_GET['id_ta'];
$tglMulai       = new \DateTime($_GET['dari']);
$tglAkhir       = new \DateTime($_GET['ke']);
$jenis          = isset($_GET['jenis']) ? $_GET['jenis'] : '';
$tglAkhir->modify('+1 day');

$idGuru = getIdGuru($con, $nikGuru);

// $interval = $tglMulai->diff($tglAkhir);
// $jml_hari = intval( $interval->format('%R%a') );


// total siswa pada kelas = ?
if ($jenis == 'Reguler') {
    $q = "SELECT k.*,s.nama_lengkap,s.nisn,s.jenis_kelamin 
    FROM tabel_siswakelas k
    INNER JOIN tabel_siswa s ON s.id_siswa = k.id_siswa 
    WHERE k.id_kelas = '$idKelas'";
} else {
    $tgl_Awal = $tglMulai->format('d-m-Y');
    $tgl_Akhir = $tglAkhir->format('d-m-Y');

    $q = "SELECT k.id_kelas_tambahan,t.start,t.end,sk.id_penempatan,t.id_ta,t.id_jadwal_tambahan,t.id_jenjang,k.id_siswa,s.nama_lengkap,s.nisn,s.jenis_kelamin
    FROM tabel_jadwal_tambahan t
    INNER JOIN tabel_kelas_tambahan k ON t.id_jadwal_tambahan = k.id_jadwal_tambahan 
    INNER JOIN tabel_siswa s ON s.id_siswa = k.id_siswa
    INNER JOIN tabel_siswakelas sk ON sk.id_siswa = k.id_siswa
    WHERE t.id_ta = '$idTA' AND t.id_jenjang = '$idJenjang' 
    AND (t.id_guru = '$idGuru' OR t.id_pembimbing_1 = '$idGuru' OR t.id_pembimbing_2 = '$idGuru' OR t.id_pembimbing_3 ='$idGuru')
    AND t.id_mapel = '$idMapel'
    -- AND STR_TO_DATE(t.start,'%d-%m-%Y') >= STR_TO_DATE('$tgl_Awal','%d-%m-%Y')
    -- AND STR_TO_DATE(t.end,'%d-%m-%Y') <= STR_TO_DATE('$tgl_Akhir','%d-%m-%Y')
    GROUP BY k.id_siswa";
}


$kelas = mysqli_query($con, $q);
$totalSiswa = mysqli_num_rows($kelas);
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($tglMulai, $interval, $tglAkhir);

$hari_libur = [];
$kp = getKalenderPendidikan($con, array(
    'start_date'    => $tglMulai->format('Y-m-d'),
    'end_date'        => $tglAkhir->format('Y-m-d')
));

if (count($kp) > 0) {
    foreach ($kp as $agenda) {
        if ($agenda['libur'] == 1) {
            $hari_libur[] = date('d-m-Y', strtotime($agenda['tanggal']));
        }
    }
}

$response = array();
$tanggal = array();
$dataSiswa = array();
$dataAttachment = array();
$noAttach = 1;
foreach ($period as $dt) {
    $tgltgl       = $dt->format("d-m-Y");
    $startTgl     = $dt->format("1-m-Y");
    $tgl          = $dt->format("d/m");
    $day          = $dt->format("l");
    $hari         = cek_hari($day);
    $harilengkap  = hari_lengkap($day);

    $weekOfMonth = get_week_of_month(new \DateTime($tgltgl), new \DateTime($startTgl));

    if ($hari == "Minggu") {
        continue;
    }
    if (in_array($tgltgl, $hari_libur)) {
        continue;
    }

    if ($jenis == 'Reguler') {
        $query = "SELECT * FROM tabel_jadwal WHERE id_ta = '$idTA' AND id_jenjang = '$idJenjang' 
        AND id_kelas = '$idKelas' AND id_guru = '$idGuru' AND hari = '$harilengkap'";

        if ($idMapel != "") {
            $query .= " AND id_mapel = '$idMapel'";
        }
    } else {
        $query = "SELECT * FROM tabel_jadwal_tambahan WHERE id_ta = '$idTA' AND id_jenjang = '$idJenjang' AND 
        (id_guru = '$idGuru' OR id_pembimbing_1 = '$idGuru' OR id_pembimbing_2 = '$idGuru' OR id_pembimbing_3 = '$idGuru') AND hari = '$hari' AND minggu_ke = '$weekOfMonth'";
    }

    $checkJadwal = mysqli_query($con, $query);

    if (mysqli_num_rows($checkJadwal) == 0) {
        continue;
    } else {
        $tanggal[] = (object) array(
            "tgl"        => $tgl,
        );
        $checkJadwal = mysqli_fetch_array($checkJadwal);
        if ($jenis == 'Reguler') {
            $idJad = $checkJadwal['id_jadwal'];
            $qAttachment = "SELECT * FROM tabel_attachment WHERE id_guru = '$idGuru' AND tanggal = '$tgltgl' AND id_jadwal ='$idJad' AND jenis ='reguler'";
        } else {
            $idJad = $checkJadwal['id_jadwal_tambahan'];
            $qAttachment = "SELECT * FROM tabel_attachment WHERE id_guru = '$idGuru' AND tanggal = '$tgltgl' AND id_jadwal ='$idJad' AND jenis ='tambahan'";
        }

        $attach = mysqli_query($con, $qAttachment);
        if (mysqli_num_rows($attach) > 0) {
            $attach = mysqli_fetch_array($attach);

            $dataAttachment[] = (object) array(
                "no"                  => $noAttach,
                "id"                  => intval($attach['id_attachment']),
                "tgl_tatap_muka"      => $attach['tanggal'],
                "kompetensi"          => $attach['materi'],
                "keterangan"          => $attach['keterangan'],
            );

            $noAttach++;
        }
    }

    // echo "pada tanggal ".$tgl ." tot : ".$totJadwal."<br>";
}

$totHadir = 0;
$totIzin  = 0;
$totSakit = 0;
$totAlpha = 0;
$totNihil = 0;

$no = 1;
while ($siswa = mysqli_fetch_array($kelas)) {
    $sex = "-";
    if (strpos(strtolower($siswa['jenis_kelamin']), 'laki') !== false) {
        $sex = "L";
    } else if (strtolower($siswa['jenis_kelamin']) == 'perempuan') {
        $sex = "P";
    }

    $absen = array();

    foreach ($period as $dt) {
        $tgltgl       = $dt->format("d-m-Y");
        $startTgl       = $dt->format("1-m-Y");
        $day          = $dt->format("l");
        $hari         = cek_hari($day);
        $harilengkap  = hari_lengkap($day);

        if ($hari == "Minggu") {
            continue;
        }
        if (in_array($tgltgl, $hari_libur)) {
            continue;
        }

        $weekOfMonth = get_week_of_month(new \DateTime($tgltgl), new \DateTime($startTgl));

        if ($jenis == 'Reguler') {
            $query = "SELECT * FROM tabel_jadwal WHERE id_ta = '$idTA' AND id_jenjang = '$idJenjang' 
            AND id_kelas = '$idKelas' AND id_guru = '$idGuru' AND hari = '$harilengkap'";

            if ($idMapel != "") {
                $query .= " AND id_mapel = '$idMapel'";
            }
        } else {
            $query = "SELECT * FROM tabel_jadwal_tambahan WHERE id_ta = '$idTA' AND id_jenjang = '$idJenjang' AND 
            (id_guru = '$idGuru' OR id_pembimbing_1 = '$idGuru' OR id_pembimbing_2 = '$idGuru' OR id_pembimbing_3 = '$idGuru') AND hari = '$hari' AND minggu_ke = '$weekOfMonth'";
        }

        $checkJadwal = mysqli_query($con, $query);
        $totJadwal = mysqli_num_rows($checkJadwal);

        if ($totJadwal == 0) {
            continue;
        }

        if ($totJadwal > 0) {
            $checkJadwal = mysqli_fetch_array($checkJadwal);
            if ($jenis == 'Reguler') {
                $idJadwal = $checkJadwal['id_jadwal'];
                $idPenempatan = $siswa['id_penempatan'];
                $qq = "SELECT * FROM tabel_absen_mapel WHERE id_penempatan = $idPenempatan AND id_jadwal = $idJadwal AND tgl = '$tgltgl'";
            } else {
                $idJadwal = $checkJadwal['id_jadwal_tambahan'];
                $idPenempatan = $siswa['id_kelas_tambahan'];
                $qq = "SELECT * FROM tabel_absen_mapel_tambahan WHERE id_kelas_tambahan =$idPenempatan AND id_jadwal_tambahan = $idJadwal AND tgl = '$tgltgl'";
            }

            $absenMapel = mysqli_query($con, $qq);

            if (mysqli_num_rows($absenMapel) > 0) {
                $absenMapel = mysqli_fetch_array($absenMapel);

                if ($absenMapel['absen'] == 'hadir') {
                    array_push($absen, "H");
                    $totHadir = $totHadir + 1;
                } else if ($absenMapel['absen'] == 'sakit') {
                    array_push($absen, "S");
                    $totSakit = $totSakit + 1;
                } else if ($absenMapel['absen'] == 'izin') {
                    array_push($absen, "I");
                    $totIzin = $totIzin + 1;
                } else if ($absenMapel['absen'] == 'alpha') {
                    array_push($absen, "TK");
                    $totAlpha = $totAlpha + 1;
                }
            } else {
                array_push($absen, "N");
                $totNihil = $totNihil + 1;
            }
        }
    }
    // $counts = array_count_values($absen);
    $hadir = count(array_keys($absen, "H"));
    $alpha = count(array_keys($absen, "N")) + count(array_keys($absen, "I")) + count(array_keys($absen, "S")) + count(array_keys($absen, "TK"));
    $dataSiswa[] = (object) array(
        "no"        => $no,
        "nama"      => $siswa['nama_lengkap'],
        "stb"       => $siswa['nisn'],
        "sex"       => $sex,
        "absen"     => $absen,
        "hadir"     => $hadir,
        "alpha"     => $alpha,
    );
    $no++;
}

$response['tanggal']               = $tanggal;
$response['siswa']                 = $dataSiswa;
$response['total_siswa']           = $totalSiswa;
$response['total_tatap_muka']      = sizeof($tanggal);
$response['total_hadir']           = $totHadir;
$response['total_sakit']           = $totSakit;
$response['total_izin']            = $totIzin;
$response['total_alpha']           = $totAlpha;
$response['total_nihil']           = $totNihil;
$response['attachment']            = $dataAttachment;
//echo $q;
echo json_encode($response);
