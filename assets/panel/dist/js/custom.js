(function ($) {

  'use strict';

  $.WebAbsence = {};
  $.WebAbsence.modul = {
    kalender: {
      agenda: {
        form_init: function() {
          $('#tanggal-agenda').daterangepicker();

          
          
          $('.colorpicker1').colorpicker()
        }
      },
      cetak: function() {
        $("#ta-select").on("change", function(){
          var url = this.value;
          if( url )
            window.location = url;
          return false;
        });
      }
    },
    lap_kehadiran: {
      manual: function() {

        $('.select-ket-jam-pulang').editable({
            value: 'tidak absen',    
            source: [
                  {value: 'tidak absen', text: 'tidak absen'},
                  {value: 'izin', text: 'izin'},
                  {value: 'sakit', text: 'sakit'}
               ],
            params: function(params) {

                // add additional params from data-attributes of trigger element

                params.tgl = $(this).editable().data('tgl');
                return params;

            },

            success: function(response, newValue) {

                alert("Translate updated successfully.");

            }
        });

      }
    },
    sinkron: {
      mengajar: function() {
        $('.sinkron-keterangan-mengajar').editable({
            value: 'tanpa_keterangan',    
            source: [
                  {value: 'tanpa_keterangan', text: 'Tanpa Keterangan'},
                  {value: 'izin', text: 'Izin'},
                  {value: 'sakit', text: 'Sakit'}
               ],
            params: function(params) {

                // add additional params from data-attributes of trigger element

                params.tgl = $(this).editable().data('tgl');

                return params;

            }
        });
      },
      kehadiran_guru: function() {
        $('.sinkron-ket-kehadiran-guru').editable({
            value: 'tidak_absen',    
            source: [
                  {value: 'tidak_absen', text: 'tidak absen'},
                  {value: 'izin', text: 'izin'},
                  {value: 'sakit', text: 'sakit'},
				  {value: 'hadir', text: 'hadir'}
               ],
            params: function(params) {

                // add additional params from data-attributes of trigger element

                params.tgl = $(this).editable().data('tgl');

                return params;

            }
        });
      },
      kehadiran_non_guru: function() {
        $('.sinkron-ket-kehadiran-non-guru').editable({
            value: 'tanpa_keterangan',    
            source: [
                  {value: 'tanpa_keterangan', text: 'tidak absen'},
                  {value: 'izin', text: 'izin'},
                  {value: 'sakit', text: 'sakit'}
               ],
            params: function(params) {

                // add additional params from data-attributes of trigger element

                params.tgl = $(this).editable().data('tgl');

                return params;

            }
        });
      }
    }
  };

})(jQuery);